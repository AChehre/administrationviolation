﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Reflection;
using System.ComponentModel;

namespace DAL
{
    internal class SqlHelper
    {
        /// <summary>
		///میکند تولید کامند ال کیو اس نوع از شی یک متد این 
		///<para>مینماید باز را کانکشن سپس و داده تخصیص آن به کانکشن یک سپس </para>
		///<para>شده پاس آن به تکست کامند بعنوان که پروسجری داخل از سپس </para>
		///<para>داده تشخیص را نیاز مورد پارامترهای لیست </para>
		///<para>پروسجر نیاز مورد پارامترهای تعداد به سپس </para>
		///<para>افزاید می پارامتر کامند ال کیو اس به </para>
		///</summary>
        /// <param name="CommandText">نام پروسجر</param>
        /// <returns>شی اس کیو ال کامند</returns>
        internal static SqlCommand GetSqlCommand(string CommandText)
        {
            SqlCommand Cmd = new SqlCommand();
            Cmd.Disposed += new EventHandler(Cmd_Disposed);
            Cmd.Connection = new SqlConnection(ConnectionString);
            Cmd.CommandText = CommandText;
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Connection.Open();
            SqlCommandBuilder.DeriveParameters(Cmd);
            return Cmd;
        }

        /// <summary>
		///میکند تولید کامند ال کیو اس نوع از شی یک متد این 
		///<para>مینماید باز را کانکشن سپس و داده تخصیص آن به کانکشن یک سپس </para>
		///<para>شده پاس آن به تکست کامند بعنوان که کوئری یا پروسجر داخل از سپس </para>
		///<para>داده تشخیص را نیاز مورد پارامترهای لیست </para>
		///<para>پروسجر نیاز مورد پارامترهای تعداد به سپس </para>
		///<para>افزاید می پارامتر کامند ال کیو اس به </para>
		///<para>میگردد استفاده کلاسیک بصورت داده پایگاه با کار برای متد این </para>
		///</summary>
        /// <param name="CommandText">نام پروسجر</param>
        /// <returns>شی اس کیو ال کامند</returns>
        internal static SqlCommand GetSqlCommand(string CommandText, CommandType Type)
        {
            SqlCommand Cmd = new SqlCommand();
            Cmd.Disposed += new EventHandler(Cmd_Disposed);
            Cmd.Connection = new SqlConnection(ConnectionString);
            Cmd.CommandText = CommandText;
            Cmd.CommandType = Type;
            Cmd.Connection.Open();
            SqlCommandBuilder.DeriveParameters(Cmd);
            return Cmd;
        }

        /// <summary>
		///برود بین از کامند ال کیو اس شی وقتی 
		///<para>میگردد متد این وارد رفتن بین از ار  قبل </para>
		///<para>ببندیم حودکار آنرا کانکشن ما و </para>
		///</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Cmd_Disposed(object sender, EventArgs e)
        {
            ((SqlCommand)sender).Connection.Close();
        }

        public static void SetValue(object inputObject, string propertyName, object propertyVal)
        {
            //find out the type
            Type type = inputObject.GetType();

            //get the property information based on the type
            System.Reflection.PropertyInfo propertyInfo = type.GetProperty(propertyName);

            //find the property type
            Type propertyType = propertyInfo.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable, we need to get the underlying type of the property
            var targetType = IsNullableType(propertyInfo.PropertyType) ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) : propertyInfo.PropertyType;

            //Returns an System.Object with the specified System.Type and whose value is
            //equivalent to the specified object.
            propertyVal = Convert.ChangeType(propertyVal, targetType);

            //Set the value of the property
            propertyInfo.SetValue(inputObject, propertyVal, null);

        }
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        /// <summary>
		///استرینگ کانکشن متن خواندن برای پروپرتی 
		///</summary>
        internal static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["UIConnectionString"].ConnectionString;
            }
        }

        /// <summary>
		///کامند ال کیو اس های پارامتر با متناظر مقادیر تخصسص برای متدی 
		///</summary>
        /// <param name="cmd">شی اس کیو ال کامند</param>
        /// <param name="obj">شی کلاس دیتا برای تخصیص مقادیر به پارامترهای مورد نیاز اس کیو ال کامند</param>
        internal static void SetParamValue(SqlCommand cmd, object obj)
        {
            // این متد نام فیلدهای کلاس را استخراج مینماید
            // نام فیلد ها دقیقا همنام ستونهای جداول بانک اطلاعاتی میباشند
            PropertyInfo[] props = obj.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                string strParam = "@" + prop.Name;
                if (cmd.Parameters.Contains(strParam))
                {
                    object val = prop.GetValue(obj, null);
                    cmd.Parameters[strParam].Value = (val == null) ? DBNull.Value : val;
                }
            }
        }

        /// <summary>
		///برمیگرداند را سلکت کوئری از حاصل اطلاعات 
		///</summary>
        /// <typeparam name="T">هر نوع داده ای از نوع کلاس دیتا</typeparam>
        /// <param name="obj">کلاس دیتا بهنوان پارامتر پروسجر یا کوئری استرینگ</param>
        /// <param name="commandtext">پروسجر یا کامند تکست</param>
        /// <returns>لیست جنریک از نوع کلاس دیتا حاوی رکوردها استخراج شده</returns>
        public static List<T> GetDataField<T>(T obj, string commandtext)
        {
            try
            {
                List<T> lstDataList = new List<T>();
                using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
                {
                    SqlHelper.SetParamValue(Cmd, obj);
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    PropertyInfo[] props = obj.GetType().GetProperties();
                    if (Reader.HasRows)
                    {
                        long RowNumber = 0;
                        while (Reader.Read())
                        {
                            var c = (T)Activator.CreateInstance(typeof(T));
                            props = c.GetType().GetProperties();
                            RowNumber++; // تولید شمارنده ردیف رکورد
                            // مقدار دادن به فیلد کلاس دیتا
                            props[0].SetValue(c, RowNumber, null);
                            for (int i = 0; i < Reader.FieldCount; i++)
                            {
                                foreach (PropertyInfo prop in props)
                                {
                                    if (prop.Name == Reader.GetName(i))
                                    {
                                        if (Reader[i] == DBNull.Value)
                                            continue;
                                        //prop.SetValue(c, Reader[i], null);
                                        SetValue(c, prop.Name, Reader[i]);
                                    }
                                }
                            }
                            lstDataList.Add(c);
                        }
                    }
                    return lstDataList;

                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
		/// برگرداندن مقادیر تک مقداری محاسباتی
		///</summary>
        /// <typeparam name="T">هر نوع داده ای از نوع کلاس دیتا</typeparam>
        /// <param name="obj">کلاس دیتا بهنوان پارامتر پروسجر یا کوئری استرینگ</param>
        /// <param name="commandtext">پروسجر یا کامند تکست</param>
        /// <returns>نتیجه بولین</returns>
        internal static object GetScalerValue<T>(T obj, string commandtext)
        {
            using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
            {
                SqlHelper.SetParamValue(Cmd, obj);
                return Cmd.ExecuteScalar();
            }
        }

        /// <summary>
		///مینماید جدول وارد را رکورد یک متد این 
		///</summary>
        /// <param name="obj">شی حاوی فیلدهای رکورد ورودی</param>
        /// <param name="commandtext">دستور اسکیول کامند</param>
        /// <returns>نتیجه بولین</returns>
        internal static bool InsertDataField(object obj, string commandtext)
        {
            using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
            {
                PropertyInfo[] props = obj.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    string strParam = "@" + prop.Name;
                    if (Cmd.Parameters.Contains(strParam))
                    {
                        object val = prop.GetValue(obj, null);
                        Cmd.Parameters[strParam].Value = (val == null) ? DBNull.Value : val; 
                    }
                }
                return Cmd.ExecuteNonQuery() > 0;
            }
        }

        /// <summary>
        ///مینماید جدول وارد را رکورد یک متد این 
        ///</summary>
        /// <param name="obj">شی حاوی فیلدهای رکورد ورودی</param>
        /// <param name="commandtext">دستور اسکیول کامند</param>
        /// <returns>نتیجه بولین</returns>
        internal static object InsertDataField(object obj, string commandtext , bool returnCustomValue)
        {
            using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
            {
                PropertyInfo[] props = obj.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    string strParam = "@" + prop.Name;
                    if (Cmd.Parameters.Contains(strParam))
                    {
                        object val = prop.GetValue(obj, null);
                        Cmd.Parameters[strParam].Value = (val == null) ? DBNull.Value : val;
                    }
                }
                Cmd.ExecuteNonQuery();
                return Cmd.Parameters["@RETURN_VALUE"].Value;
            }
        }

        /// <summary>
        /// لیستی از رکورد ها را یکجا وارد جدول مینماید
        /// </summary>
        /// <param name="objectList">لیست رکوردها</param>
        /// <param name="commandtext">دستور اسکیوال کامند</param>
        /// <returns>نتیجه بولین</returns>
        internal static bool InsertDataFieldList<T>(List<T> objectList, string commandtext)
        {
            using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
            {
                foreach (object obj in objectList)
                {
                    PropertyInfo[] props = obj.GetType().GetProperties();
                    foreach (PropertyInfo prop in props)
                    {
                        string strParam = "@" + prop.Name;
                        if (Cmd.Parameters.Contains(strParam))
                        {
                            object val = prop.GetValue(obj, null);
                            Cmd.Parameters[strParam].Value = (val == null) ? DBNull.Value : val; 
                        }
                    }
                    Cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        /// <summary>
        ///برمیگرداند را سلکت کوئری از حاصل اطلاعات 
        ///</summary>
        /// <typeparam name="T">هر نوع داده ای از نوع کلاس دیتا</typeparam>
        /// <param name="obj">کلاس دیتا بهنوان پارامتر پروسجر یا کوئری استرینگ</param>
        /// <param name="commandtext">پروسجر یا کامند تکست</param>
        /// <returns>لیست جنریک از نوع کلاس دیتا حاوی رکوردها استخراج شده</returns>
        public static List<Return> GetDataField<Return, Input>(Input obj, string commandtext)
        {
            List<Return> lstDataList = new List<Return>();
            using (SqlCommand Cmd = SqlHelper.GetSqlCommand(commandtext))
            {
                SqlHelper.SetParamValue(Cmd, obj);
                SqlDataReader Reader = Cmd.ExecuteReader();

                PropertyInfo[] props = obj.GetType().GetProperties();
                if (Reader.HasRows)
                {
                    long RowNumber = 0;
                    while (Reader.Read())
                    {
                        var c = (Return)Activator.CreateInstance(typeof(Return));
                        props = c.GetType().GetProperties();
                        RowNumber++; // تولید شمارنده ردیف رکورد
                        // مقدار دادن به فیلد کلاس دیتا
                        props[0].SetValue(c, RowNumber, null);
                        for (int i = 0; i < Reader.FieldCount; i++)
                        {
                            foreach (PropertyInfo prop in props)
                            {
                                if (prop.Name == Reader.GetName(i))
                                {
                                    if (Reader[i] == DBNull.Value)
                                        continue;
                                    //prop.SetValue(c, Reader[i], null);
                                    SetValue(c, prop.Name, Reader[i]);
                                }
                            }
                        }
                        lstDataList.Add(c);
                    }
                }
                return lstDataList;
            }
        }
    }
}

