﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserGroupMap;
using System.Data.SqlClient;

namespace DAL.Config.tblUserGroupMap
{
    public class Methods : DTO.Config.tblUserGroupMap.IMethods
    {
        public List<DTO.Config.tblUserGroupMap.tblUserGroupMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblUserGroupMap.tblUserGroupMap> SelectByUserID(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserGroupMap_SelectByUserID");
        }

        public DTO.Config.tblUserGroupMap.tblUserGroupMap SelectByID(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroupMap_Insert");
        }

        public bool Insert(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lst)
        {
            return SqlHelper.InsertDataFieldList(lst, "tblUserGroupMap_Insert");
        }

        public bool Update(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// پاک کردن عضویت یک کاربر از تمام گروه های کاربری
        /// </summary>
        /// <param name="obj">شی از نوع tblUserGroupMap</param>
        /// <returns>ترو یعنی عملیات موفقیت آمیز</returns>
        public bool DeleteByUserID(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroupMap_DeleteByUserID");
        }

        public bool CheckUserAccessToGroup(DTO.Config.tblUserGroupMap.tblUserGroupMap obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblUserGroupMap_CheckUserAccessToGroup"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@AccessResult"].Value.ToString().StringToBool();
            }
        }
    }
}
