﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblChangeFileBoard;
using System.Data.SqlClient;

namespace DAL.Board.tblChangeFileBoard
{
    public class Methods : DTO.Board.tblChangeFileBoard.IMethods
    {
        public List<DTO.Board.tblChangeFileBoard.tblChangeFileBoard> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblChangeFileBoard.tblChangeFileBoard SelectByID(DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblChangeFileBoard_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long[] Insert(DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblChangeFileBoard_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@ID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj)
        {
            throw new NotImplementedException();
        }

        public long SelectByRequestDataID(DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblChangeFileBoard_SelectBy_RequestDataID");
            if (lst.Any())
            {
                return lst[0].ID;
            }
            return 0;
        }
    }
}
