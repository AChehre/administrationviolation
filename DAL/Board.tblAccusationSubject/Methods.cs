﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblAccusationSubject;

namespace DAL.Board.tblAccusationSubject
{
    public class Methods : DTO.Board.tblAccusationSubject.IMethods
    {
        public List<DTO.Board.tblAccusationSubject.tblAccusationSubject> SelectAll()
        {
            DTO.Board.tblAccusationSubject.tblAccusationSubject obj = new DTO.Board.tblAccusationSubject.tblAccusationSubject();
            return SqlHelper.GetDataField(obj, "tblAccusationSubject_SelectAll");
        }

        public DTO.Board.tblAccusationSubject.tblAccusationSubject SelectByID(DTO.Board.tblAccusationSubject.tblAccusationSubject obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblAccusationSubject_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblAccusationSubject.tblAccusationSubject obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblAccusationSubject.tblAccusationSubject obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblAccusationSubject.tblAccusationSubject obj)
        {
            throw new NotImplementedException();
        }
    }
}
