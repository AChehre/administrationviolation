﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblEjectionNotification;
using System.Data.SqlClient;

namespace DAL.Board.tblEjectionNotification
{
    public class Methods : DTO.Board.tblEjectionNotification.IMethods
    {
        public List<DTO.Board.tblEjectionNotification.tblEjectionNotification> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblEjectionNotification.tblEjectionNotification SelectByID(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblEjectionNotification_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long[] Insert(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblEjectionNotification_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@NotificationID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            return SqlHelper.InsertDataField(obj, "tblEjectionNotification_Update");
        }

        public bool Delete(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            throw new NotImplementedException();
        }

        public long SelectByRequestDataID(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblEjectionNotification_SelectBy_RequestDataID");
            return lst.Any() ? lst[0].NotificationID : 0;
        }

        public DTO.Board.tblEjectionNotification.tblEjectionNotification SelectByFileID(DTO.Board.tblEjectionNotification.tblEjectionNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblEjectionNotification_SelectByFileID");
            return lst.Any() ? lst[0] : obj;
        }
    }
}
