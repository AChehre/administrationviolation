﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblHelp;

namespace DAL.Config.tblHelp
{
    public class Methods : DTO.Config.tblHelp.IMethods
    {
        public List<DTO.Config.tblHelp.tblHelp> SelectAll()
        {
            DTO.Config.tblHelp.tblHelp obj = new DTO.Config.tblHelp.tblHelp();
            return SqlHelper.GetDataField(obj, "tblHelp_SelectAll");
        }

        public DTO.Config.tblHelp.tblHelp SelectByID(DTO.Config.tblHelp.tblHelp obj)
        {
            List<DTO.Config.tblHelp.tblHelp> rlst = new List<DTO.Config.tblHelp.tblHelp>();

            rlst = SqlHelper.GetDataField(obj, "tblHelp_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblHelp.tblHelp SelectByType(DTO.Config.tblHelp.tblHelp obj)
        {
            List<DTO.Config.tblHelp.tblHelp> rlst = new List<DTO.Config.tblHelp.tblHelp>();

            rlst = SqlHelper.GetDataField(obj, "tblHelp_SelectByType");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public bool Insert(DTO.Config.tblHelp.tblHelp obj)
        {
            return SqlHelper.InsertDataField(obj, "tblHelp_Insert");
        }

        public bool Update(DTO.Config.tblHelp.tblHelp obj)
        {
            return SqlHelper.InsertDataField(obj, "tblHelp_Update");
        }

        public bool Delete(DTO.Config.tblHelp.tblHelp obj)
        {
            return SqlHelper.InsertDataField(obj, "tblHelp_Delete");
        }

        public bool Save(DTO.Config.tblHelp.tblHelp obj)
        {
            return SqlHelper.InsertDataField(obj, "tblHelp_Save");
        }
    }
}
