using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblPermissionMap;

namespace DAL.Config.tblPermissionMap
{
    public class Methods : DTO.Config.tblPermissionMap.IMethods
    {
        public List<DTO.Config.tblPermissionMap.tblPermissionMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblPermissionMap.tblPermissionMap SelectByID(DTO.Config.tblPermissionMap.tblPermissionMap obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblPermissionMap.tblPermissionMap> SelectByPageIDActionCode(DTO.Config.tblPermissionMap.tblPermissionMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblPermissionMap_SelectByPageIDActionCode");
        }

        public bool Insert(DTO.Config.tblPermissionMap.tblPermissionMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPermissionMap_Insert");
        }

        public bool Update(DTO.Config.tblPermissionMap.tblPermissionMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblPermissionMap.tblPermissionMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPermissionMap_Delete");
        }
    }
}
