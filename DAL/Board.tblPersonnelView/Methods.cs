﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblPersonnelView;

namespace DAL.Board.tblPersonnelView
{
    public class Methods : DTO.Board.tblPersonnelView.IMethods
    {
        public List<DTO.Board.tblPersonnelView.tblPersonnelView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblPersonnelView.tblPersonnelView SelectByID(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblPersonnelView.tblPersonnelView> SelectByLikePersonnelCode(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            return SqlHelper.GetDataField(obj, "tblPersonnelView_SelectByLikePersonnelCode");
        }

        public List<DTO.Board.tblPersonnelView.tblPersonnelView> SelectByLikeName(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            return SqlHelper.GetDataField(obj, "tblPersonnelView_SelectByLikeName");
        }

        public bool Insert(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            throw new NotImplementedException();
        }


        public DTO.Board.tblPersonnelView.tblPersonnelView SelectByPersonnelCode(DTO.Board.tblPersonnelView.tblPersonnelView obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblPersonnelView_SelectByLikePersonnelCode_SerachByPersonnerCode");
            return lst.Any() ? lst[0] : obj;
        }
    }
}
