﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblNewsGroupMap;

namespace DAL.Admin.tblNewsGroupMap
{
    public class Methods : DTO.Admin.tblNewsGroupMap.IMethods
    {
        public List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> SelectByNewsID(DTO.Admin.tblNewsGroupMap.tblNewsGroupMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblNewsGroupMap_SelectBy_NewsID");
        }

        public bool Insert(List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> lst)
        {
            return SqlHelper.InsertDataFieldList(lst,"tblNewsGroupMap_Insert");
        }

        public bool Update(DTO.Admin.tblNewsGroupMap.tblNewsGroupMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Admin.tblNewsGroupMap.tblNewsGroupMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblNewsGroupMap_Delete");
        }
    }
}
