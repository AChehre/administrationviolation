﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblBoardMemberMap;
using System.Data.SqlClient;

namespace DAL.Board.tblBoardMemberMap
{
    public class Methods : DTO.Board.tblBoardMemberMap.IMethods
    {
        public List<DTO.Board.tblBoardMemberMap.tblBoardMemberMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblBoardMemberMap.tblBoardMemberMap> SelectByMemberUserID(DTO.Board.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoardMemberMap_SelectByMemberUserID");
        }

        public DTO.Board.tblBoardMemberMap.tblBoardMemberMap SelectByID(DTO.Board.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }

        public long Insert(DTO.Board.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }
    }
}
