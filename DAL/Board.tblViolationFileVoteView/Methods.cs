﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFileVoteView;

namespace DAL.Board.tblViolationFileVoteView
{
    public class Methods : DTO.Board.tblViolationFileVoteView.IMethods
    {
        public List<DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView SelectByID(DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView obj)
        {
            throw new NotImplementedException();
        }


        public bool CheckVoteNumberExists(DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectBy_BoardID_AND_Number").ObjectToByte() > 0;
        }
    }
}
