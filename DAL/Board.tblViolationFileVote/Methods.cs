﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFileVote;

namespace DAL.Board.tblViolationFileVote
{
    public class Methods : DTO.Board.tblViolationFileVote.IMethods
    {
        public List<DTO.Board.tblViolationFileVote.tblViolationFileVote> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationFileVote.tblViolationFileVote SelectByViolationFileID(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFileVote_SelectByViolationFileID");
            return lst.Any() ? lst[0] : obj;
        }

        public DTO.Board.tblViolationFileVote.tblViolationFileVote SelectByID(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFileVote_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileVote_Insert");
        }

        public bool Update(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileVote_Update");
        }

        public bool Delete(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationFileVote.tblViolationFileVote getVoteDate(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFileVote_GetVoteDate");
            return lst.Any() ? lst[0] : obj;
        }

        public long getPenaltyTypeCount(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_GetPenaltyCount").ObjectToLong();
        }

        public long getVoteCountByEducationalGrade(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectByEducationalGrade").ObjectToLong();
        }

        public long getVoteCountByGender(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectByGender").ObjectToLong();
        }

        public long getVoteCountByOrganizationPosition(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectByOrganizationPosition").ObjectToLong();
        }

        public long getVoteCountByOstanCode(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectByOstanCode").ObjectToLong();
        }

        public long getVoteCountByActivityYear(DTO.Board.tblViolationFileVote.tblViolationFileVote obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileVote_SelectByActivityYear").ObjectToLong();
        }
    }
}
