﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Report.tblActivityReport;

namespace DAL.Report.tblActivityReport
{
    public class Methods : DTO.Report.tblActivityReport.IMethods
    {
        public DTO.Report.tblActivityReport.tblActivityReport SelectByDate(DTO.Report.tblActivityReport.tblActivityReport obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblActivityReport_SelectAll");
            return lst.Any() ? lst[0] : obj;
        }
    }
}
