﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserAttributesMapView;
using System.Data.SqlClient;

namespace DAL.Config.tblUserAttributesMapView
{
    public class Methods : DTO.Config.tblUserAttributesMapView.IMethods
    {
        public List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView SelectByID(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView> rlst = new List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView>();

            rlst = SqlHelper.GetDataField(obj, "tblUserAttributesMapView_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView Authenticate(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView> rlst = new List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView>();

            rlst = SqlHelper.GetDataField(obj, "tblUserAttributesMapView_Authenticate");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView> SelectAllByPaging(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserAttributesMapView_SelectAllByPaging");
        }

        public long Insert(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblUserAttributesMapView_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@UserID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserAttributesMapView_Update");
        }

        public bool UpdateWithoutGroups(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserAttributesMapView_UpdateWithoutGroups");
        }

        public bool Delete(DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj)
        {
            throw new NotImplementedException();
        }
    }
}
