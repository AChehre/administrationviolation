﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblBoardMemberMap;
using System.Data.SqlClient;

namespace DAL.Admin.tblBoardMemberMap
{
    public class Methods : DTO.Admin.tblBoardMemberMap.IMethods
    {
        public List<DTO.Admin.tblBoardMemberMap.tblBoardMemberMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Admin.tblBoardMemberMap.tblBoardMemberMap SelectByID(DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }

        public long Insert(DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblBoardMemberMap_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@MapRecordID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblBoardMemberMap_Delete");
        }
    }
}
