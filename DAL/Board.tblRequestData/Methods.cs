﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblRequestData;

namespace DAL.Board.tblRequestData
{
    public class Methods : DTO.Board.tblRequestData.IMethods
    {
        public List<DTO.Board.tblRequestData.tblRequestData> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblRequestData.tblRequestData SelectByID(DTO.Board.tblRequestData.tblRequestData obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRequestData_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long Insert(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.InsertDataField(obj, "tblRequestData_Insert",true).ObjectToLong();
        }

        public bool Update(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.InsertDataField(obj, "tblRequestData_Update");
        }

        public bool Delete(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.InsertDataField(obj, "tblRequestData_Delete");
        }

        public List<DTO.Board.tblRequestData.tblRequestData> SelectByFileID(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.GetDataField(obj, "tblRequestData_SelectByFileID");
        }

        public bool UpdateStatus(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.InsertDataField(obj, "tblRequestData_UpdateStatus");
        }

        public int CheckHasDependedRequestData(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblRequestData_HasDependedRequestData").ObjectToInt();
        }

        public bool tblRequestDataCheckAllPrequisit(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblRequestData_Check_All_Prequisit").ObjectToLong() > 0;
        }

        public DTO.Board.tblRequestData.tblRequestData getVoteDate(DTO.Board.tblRequestData.tblRequestData obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRequestData_GetVoteDate");
            return lst.Any() ? lst[0] : obj;
        }

        public bool CheckAttachmentDateForVote(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblRequestData_Check_All_Prequisit").ObjectToLong() > 0;
        }

        public DTO.Board.tblRequestData.tblRequestData getEjectionDate(DTO.Board.tblRequestData.tblRequestData obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRequestData_GetEjectionDate");
            return lst.Any() ? lst[0] : obj;
        }

        public DTO.Board.tblRequestData.tblRequestData SelectByRequestID(DTO.Board.tblRequestData.tblRequestData obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRequestData_SelectBy_RequestID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool HasAnyNotCompletedData(DTO.Board.tblRequestData.tblRequestData obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblRequestData_HasAnyNotCompletedData").ObjectToLong() > 0;
        }
    }
}
