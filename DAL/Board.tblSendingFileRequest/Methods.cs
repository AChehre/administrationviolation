﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblSendingFileRequest;
using System.Data.SqlClient;

namespace DAL.Board.tblSendingFileRequest
{
    public class Methods : DTO.Board.tblSendingFileRequest.IMethods
    {
        public List<DTO.Board.tblSendingFileRequest.tblSendingFileRequest> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblSendingFileRequest.tblSendingFileRequest SelectByID(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblSendingFileRequest_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long[] Insert(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblSendingFileRequest_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@ID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            throw new NotImplementedException();
        }

        public long SelectByRequestDataID(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblSendingFileRequest_SelectBy_RequestDataID");
            if (lst.Any())
            {
                return lst[0].ID;
            }
            return 0;
        }

        public List<DTO.Board.tblSendingFileRequest.tblSendingFileRequest> SelectByRelatedFileID(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            return SqlHelper.GetDataField(obj, "tblSendingFileRequest_SelectBy_RelatedFileID");
        }

        public bool UpdateStatus(DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj)
        {
            return SqlHelper.InsertDataField(obj, "tblSendingFileRequest_UpdateStatus");
        }
    }
}
