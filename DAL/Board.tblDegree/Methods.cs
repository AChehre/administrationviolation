﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblDegree;

namespace DAL.Board.tblDegree
{
    public class Methods : DTO.Board.tblDegree.IMethods
    {
        public List<DTO.Board.tblDegree.tblDegree> SelectAll()
        {
            DTO.Board.tblDegree.tblDegree obj = new DTO.Board.tblDegree.tblDegree();
            return SqlHelper.GetDataField(obj, "tblDegree_SelectAll");
        }

        public DTO.Board.tblDegree.tblDegree SelectByID(DTO.Board.tblDegree.tblDegree obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblDegree_SelectByID");
            if (lst.Any())
            {
                return lst[0];
            }
            return obj;
        }

        public bool Insert(DTO.Board.tblDegree.tblDegree obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblDegree.tblDegree obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblDegree.tblDegree obj)
        {
            throw new NotImplementedException();
        }
    }
}
