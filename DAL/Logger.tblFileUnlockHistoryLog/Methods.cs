﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Logger.tblFileUnlockHistoryLog;

namespace DAL.Logger.tblFileUnlockHistoryLog
{
    public class Methods : DTO.Logger.tblFileUnlockHistoryLog.IMethods
    {
        public List<DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog SelectByID(DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog obj)
        {
            return SqlHelper.InsertDataField(obj,"tblFileUnlockHistoryLog_Insert");
        }

        public bool Update(DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog obj)
        {
            throw new NotImplementedException();
        }
    }
}
