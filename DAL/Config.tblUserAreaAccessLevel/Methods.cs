﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserAreaAccessLevel;

namespace DAL.Config.tblUserAreaAccessLevel
{
    public class Methods : DTO.Config.tblUserAreaAccessLevel.IMethods
    {
        public List<DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel SelectByUserID(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            List<DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel> rlst = new List<DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel>();

            rlst = SqlHelper.GetDataField(obj, "tblUserAreaAccessLevel_SelectByUserID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel SelectByID(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ثبت اطلاعات
        /// اگر برای یوزر آیدی رکوردی وجود داشته باشد، ویرایش انجام میشود، در غیر این صورت ثبت انجام میشود
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Save(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserAreaAccessLevel_Save");
        }

        public bool Insert(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel obj)
        {
            throw new NotImplementedException();
        }
    }
}
