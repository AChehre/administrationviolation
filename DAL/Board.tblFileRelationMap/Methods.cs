﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblFileRelationMap;

namespace DAL.Board.tblFileRelationMap
{
    public class Methods : DTO.Board.tblFileRelationMap.IMethods
    {
        public List<DTO.Board.tblFileRelationMap.tblFileRelationMap> SelectAll(DTO.Board.tblFileRelationMap.tblFileRelationMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblFileRelationMap_Select_ForRelatedFileLink");
        }

        public DTO.Board.tblFileRelationMap.tblFileRelationMap SelectByID(DTO.Board.tblFileRelationMap.tblFileRelationMap obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblFileRelationMap_Select_ForRelatedFileLink");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblFileRelationMap.tblFileRelationMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblFileRelationMap_Insert");
        }

        public bool Update(DTO.Board.tblFileRelationMap.tblFileRelationMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblFileRelationMap.tblFileRelationMap obj)
        {
            throw new NotImplementedException();
        }


        
    }
}
