using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblSchool;

namespace DAL.Config.tblSchool
{
    public class Methods : DTO.Config.tblSchool.IMethods
    {
        public List<DTO.Config.tblSchool.tblSchool> SelectAll(DTO.Config.tblSchool.tblSchool obj)
        {
            return SqlHelper.GetDataField(obj, "tblSchool_SelectAll");
        }

        public List<DTO.Config.tblSchool.tblSchool> SelectAllUserSchools(DTO.Config.tblSchool.tblSchool obj)
        {
            return SqlHelper.GetDataField(obj, "tblSchool_SelectAllUserSchools");
        }

        public DTO.Config.tblSchool.tblSchool SelectByID(DTO.Config.tblSchool.tblSchool obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblSchool.tblSchool obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblSchool.tblSchool obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblSchool.tblSchool obj)
        {
            throw new NotImplementedException();
        }
    }
}
