﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblPages;
using System.Data.SqlClient;

namespace DAL.Config.tblPages
{
    public class Methods : DTO.Config.tblPages.IMethods
    {
        public List<DTO.Config.tblPages.tblPages> SelectAll()
        {
            DTO.Config.tblPages.tblPages obj = new DTO.Config.tblPages.tblPages();
            return SqlHelper.GetDataField(obj, "tblPages_SelectAll");
        }

        public List<DTO.Config.tblPages.tblPages> SelectAll_Published(DTO.Config.tblPages.tblPages obj)
        {
            return SqlHelper.GetDataField(obj, "tblPages_SelectAll_Published");
        }

        public List<DTO.Config.tblPages.tblPages> SelectAllByPaging(DTO.Config.tblPages.tblPages obj)
        {
            return SqlHelper.GetDataField(obj, "tblPages_SelectAllByPaging");
        }

        public DTO.Config.tblPages.tblPages SelectByID(DTO.Config.tblPages.tblPages obj)
        {
            List<DTO.Config.tblPages.tblPages> rlst = new List<DTO.Config.tblPages.tblPages>();

            rlst = SqlHelper.GetDataField(obj, "tblPages_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblPages.tblPages SelectByAspxPage(DTO.Config.tblPages.tblPages obj)
        {
            List<DTO.Config.tblPages.tblPages> rlst = new List<DTO.Config.tblPages.tblPages>();

            rlst = SqlHelper.GetDataField(obj, "tblPages_SelectByAspxPage");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public int Insert(DTO.Config.tblPages.tblPages obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblPages_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@PageID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Config.tblPages.tblPages obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPages_Update");
        }

        public bool Delete(DTO.Config.tblPages.tblPages obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPages_Delete");
        }
    }
}
