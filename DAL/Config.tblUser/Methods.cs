﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUser;
using System.Data.SqlClient;

namespace DAL.Config.tblUser
{
    public class Methods : DTO.Config.tblUser.IMethods
    {
        public List<DTO.Config.tblUser.tblUser> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblUser.tblUser> SelectAllByPaging(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.GetDataField(obj, "tblUser_SelectAllByPaging");
        }

        public List<DTO.Config.tblUser.tblUser> SelectByLikeName(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.GetDataField(obj, "tblUser_SelectByLikeName");
        }

        public List<DTO.Config.tblUser.tblUser> SelectByLikeUserID(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.GetDataField(obj, "tblUser_SelectByLikeUserID");
        }

        public DTO.Config.tblUser.tblUser SelectByID(DTO.Config.tblUser.tblUser obj)
        {
            List<DTO.Config.tblUser.tblUser> rlst = new List<DTO.Config.tblUser.tblUser>();

            rlst = SqlHelper.GetDataField(obj, "tblUser_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblUser.tblUser SelectByUserName(DTO.Config.tblUser.tblUser obj)
        {
            List<DTO.Config.tblUser.tblUser> rlst = new List<DTO.Config.tblUser.tblUser>();

            rlst = SqlHelper.GetDataField(obj, "tblUser_SelectByUserName");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblUser.tblUser SelectByUserNameAndUserID(DTO.Config.tblUser.tblUser obj)
        {
            List<DTO.Config.tblUser.tblUser> rlst = new List<DTO.Config.tblUser.tblUser>();

            rlst = SqlHelper.GetDataField(obj, "tblUser_SelectByUserNameAndUserID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public long Insert(DTO.Config.tblUser.tblUser obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblUser_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@UserID"].Value.ToString().StringToLong();
            }
        }

        public bool Update(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUser_Update");
        }

        public bool ChangePass(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUser_ChangePass");
        }

        public DTO.Config.tblUser.tblUser CheckUserPass(DTO.Config.tblUser.tblUser obj)
        {
            List<DTO.Config.tblUser.tblUser> rlst = new List<DTO.Config.tblUser.tblUser>();

            rlst = SqlHelper.GetDataField(obj, "tblUser_CheckUserPass");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public bool Delete(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUser_Delete");
        }

        public bool RenewExpirationTime(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUser_RenewExpirationTime");
        }

        public bool Logout(DTO.Config.tblUser.tblUser obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUser_Logout");
        }
    }
}
