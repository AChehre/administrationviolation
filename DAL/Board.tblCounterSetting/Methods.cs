﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblCounterSetting;

namespace DAL.Board.tblCounterSetting
{
    public class Methods : DTO.Board.tblCounterSetting.IMethods
    {
        public List<DTO.Board.tblCounterSetting.tblCounterSetting> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblCounterSetting.tblCounterSetting SelectByID(DTO.Board.tblCounterSetting.tblCounterSetting obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblCounterSetting_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblCounterSetting.tblCounterSetting obj)
        {
            return SqlHelper.InsertDataField(obj, "tblCounterSetting_Insert");
        }

        public bool Update(DTO.Board.tblCounterSetting.tblCounterSetting obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblCounterSetting.tblCounterSetting obj)
        {
            throw new NotImplementedException();
        }
    }
}
