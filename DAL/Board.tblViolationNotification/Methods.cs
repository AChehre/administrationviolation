﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationNotification;

namespace DAL.Board.tblViolationNotification
{
    public class Methods : DTO.Board.tblViolationNotification.IMethods
    {
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<DTO.Board.tblViolationNotification.tblViolationNotification> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationNotification.tblViolationNotification SelectByID(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationNotification_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ثبت لایحه دفاعیه
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool AddReplyBill(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationNotification_AddReplyBill");
        }

        public List<DTO.Board.tblViolationNotification.tblViolationNotification> SelectByViolationFileID(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationNotification_SelectBy_ViolationFileID");
        }

        public long SelectByRequestDataID(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationNotification_SelectBy_RequestDataID");
            if (lst.Any())
            {
                return lst[0].NotificationID.Value;
            }
            return 0;
        }

        public bool CheckNotificationDate(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationNotification_CheckNotificationDate").ObjectToLong() > 0;
        }

        public bool CheckExistLetterSerialCode(DTO.Board.tblViolationNotification.tblViolationNotification obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationNotification_SelectLetterSerial").ObjectToByte() > 0;
        }
    }
}
