using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserGroupMenuMapView;

namespace DAL.Config.tblUserGroupMenuMapView
{
    public class Methods : DTO.Config.tblUserGroupMenuMapView.IMethods
    {
        public List<DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView> SelectAllByPaging(DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserGroupMenuMapView_SelectAllByPaging");
        }

        public DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView SelectByID(DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblUserGroupMenuMapView.tblUserGroupMenuMapView obj)
        {
            throw new NotImplementedException();
        }
    }
}
