﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblEstekhdamStatus;

namespace DAL.Board.tblEstekhdamStatus
{
    public class Methods : DTO.Board.tblEstekhdamStatus.IMethods
    {
        public List<DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus SelectByID(DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblEstekhdamStatus_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus obj)
        {
            throw new NotImplementedException();
        }
    }
}
