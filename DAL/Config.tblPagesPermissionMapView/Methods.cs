using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblPagesPermissionMapView;

namespace DAL.Config.tblPagesPermissionMapView
{
    public class Methods : DTO.Config.tblPagesPermissionMapView.IMethods
    {
        public List<DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView> SelectAllByPaging(DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblPagesPermissionMapView_SelectAllByPaging");
        }

        public DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView SelectByID(DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj)
        {
            throw new NotImplementedException();
        }
    }
}
