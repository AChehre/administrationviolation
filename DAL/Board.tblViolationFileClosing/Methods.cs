﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFileClosing;

namespace DAL.Board.tblViolationFileClosing
{
    public class Methods : DTO.Board.tblViolationFileClosing.IMethods
    {
        public List<DTO.Board.tblViolationFileClosing.tblViolationFileClosing> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationFileClosing.tblViolationFileClosing SelectByViolationFileID(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFileClosing_SelectByViolationFileID");
            return lst.Any() ? lst[0] : obj;
        }

        public DTO.Board.tblViolationFileClosing.tblViolationFileClosing SelectByID(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFileClosing_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileClosing_Insert");
        }

        public bool Update(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileClosing_Update");
        }

        public bool Delete(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            throw new NotImplementedException();
        }

        public bool CheckExistFileSerialCode(DTO.Board.tblViolationFileClosing.tblViolationFileClosing obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFileClosing_SelectBy_BoardID_AND_Number").ObjectToByte() > 0;
        }
    }
}
