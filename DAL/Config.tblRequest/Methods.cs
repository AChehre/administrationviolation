﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblRequest;

namespace DAL.Config.tblRequest
{
    public class Methods : DTO.Config.tblRequest.IMethods
    {
        public List<DTO.Config.tblRequest.tblRequest> SelectAll(DTO.Config.tblRequest.tblRequest obj)
        {
            return SqlHelper.GetDataField(obj, "tblRequest_SelectAll");
        }

        public DTO.Config.tblRequest.tblRequest SelectByID(DTO.Config.tblRequest.tblRequest obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRequest_SelectByID");
            if(lst.Any())
            {
                return lst[0];
            }
            return obj;
        }

        public bool Insert(DTO.Config.tblRequest.tblRequest obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblRequest.tblRequest obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblRequest.tblRequest obj)
        {
            throw new NotImplementedException();
        }
    }
}
