﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblBoard;
using System.Data.SqlClient;

namespace DAL.Admin.tblBoard
{
    public class Methods : DTO.Admin.tblBoard.IMethods
    {
        public List<DTO.Admin.tblBoard.tblBoard> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Admin.tblBoard.tblBoard> SelectAllByPaging(DTO.Admin.tblBoard.tblBoard obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoard_SelectAllByPaging");
        }

        public DTO.Admin.tblBoard.tblBoard SelectByID(DTO.Admin.tblBoard.tblBoard obj)
        {
            List<DTO.Admin.tblBoard.tblBoard> rlst = new List<DTO.Admin.tblBoard.tblBoard>();

            rlst = SqlHelper.GetDataField(obj, "tblBoard_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public long Insert(DTO.Admin.tblBoard.tblBoard obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblBoard_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@BoardID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Admin.tblBoard.tblBoard obj)
        {
            return SqlHelper.InsertDataField(obj, "tblBoard_Update");
        }

        public bool Delete(DTO.Admin.tblBoard.tblBoard obj)
        {
            return SqlHelper.InsertDataField(obj, "tblBoard_Delete");
        }


        public bool DeleteAllViolation(DTO.Admin.tblBoard.tblBoard obj)
        {
            return SqlHelper.InsertDataField(obj, "tblBoard_DeleteAllViolationFiles");
        }
    }
}
