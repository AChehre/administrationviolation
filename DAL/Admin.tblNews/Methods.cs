﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblNews;

namespace DAL.Admin.tblNews
{
    public class Methods : DTO.Admin.tblNews.IMethods
    {
        public List<DTO.Admin.tblNews.tblNews> SelectAll(DTO.Admin.tblNews.tblNews obj)
        {
            return SqlHelper.GetDataField(obj, "tblNews_SelectAll");
        }

        public DTO.Admin.tblNews.tblNews SelectByID(DTO.Admin.tblNews.tblNews obj)
        {
            try
            {
                return SqlHelper.GetDataField(obj, "tblNews_SelectByID")[0];
            }
            catch
            {
                return obj;
            }
        }

        public long Insert(DTO.Admin.tblNews.tblNews obj)
        {
            return SqlHelper.InsertDataField(obj, "tblNews_Insert",true).ObjectToLong();
        }

        public bool Update(DTO.Admin.tblNews.tblNews obj)
        {
            return SqlHelper.InsertDataField(obj, "tblNews_Update");
        }

        public bool Delete(DTO.Admin.tblNews.tblNews obj)
        {
            return SqlHelper.InsertDataField(obj, "tblNews_Delete");
        }

        public List<DTO.Admin.tblNews.tblNews> SelectByDate(DTO.Admin.tblNews.tblNews obj)
        {
            return SqlHelper.GetDataField(obj,"tblNews_SelectBy_Permitted_GroupIDs_WithSpecifiedDate");
        }
    }
}
