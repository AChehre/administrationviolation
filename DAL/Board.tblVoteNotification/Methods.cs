﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblVoteNotification;
using System.Data.SqlClient;

namespace DAL.Board.tblVoteNotification
{
    public class Methods : DTO.Board.tblVoteNotification.IMethods
    {
        public List<DTO.Board.tblVoteNotification.tblVoteNotification> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblVoteNotification.tblVoteNotification SelectByID(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblVoteNotification_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long[] Insert(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblVoteNotification_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@NotificationID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            return SqlHelper.InsertDataField(obj, "tblVoteNotification_Update");
        }

        public bool Delete(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            throw new NotImplementedException();
        }

        public long SelectByRequestDataID(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblVoteNotification_SelectBy_RequestDataID");
            if (lst.Any())
            {
                return lst[0].NotificationID;
            }
            return 0;
        }

        public bool CheckCounterConfilict(DTO.Board.tblVoteNotification.tblVoteNotification obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblVoteNotification_SelectBy_LetterSerial").ObjectToByte() > 0;
        }
    }
}
