using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserGroupMenuMap;

namespace DAL.Config.tblUserGroupMenuMap
{
    public class Methods : DTO.Config.tblUserGroupMenuMap.IMethods
    {
        public List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> SelectAll()
        {
            DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj = new DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap();
            return SqlHelper.GetDataField(obj, "tblUserGroupMenuMap_SelectAll");
        }


        public DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap SelectByID(DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> SelectByMenuID(DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserGroupMenuMap_SelectByMenuID");
        }

        public bool Insert(DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroupMenuMap_Insert");
        }

        public bool Update(DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroupMenuMap_Delete");
        }
    }
}
