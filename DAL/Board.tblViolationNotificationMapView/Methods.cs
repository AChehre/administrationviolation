﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationNotificationMapView;
using System.Data.SqlClient;

namespace DAL.Board.tblViolationNotificationMapView
{
    public class Methods : DTO.Board.tblViolationNotificationMapView.IMethods
    {
        public List<DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView SelectByID(DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView obj)
        {
            List<DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView> lst = SqlHelper.GetDataField(obj, "tblViolationNotificationMapView_SelectByID");
            if (lst.Any())
            {
                return lst[0];
            }
            return obj;
        }

        public long[] Insert(DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblViolationNotificationMapView_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@NotificationID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationNotificationMapView_Update");
        }
    }
}
