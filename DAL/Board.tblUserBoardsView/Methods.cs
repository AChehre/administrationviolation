﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblUserBoardsView;

namespace DAL.Board.tblUserBoardsView
{
    public class Methods : DTO.Board.tblUserBoardsView.IMethods
    {
        public List<DTO.Board.tblUserBoardsView.tblUserBoardsView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblUserBoardsView.tblUserBoardsView> SelectAllByPaging(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserBoardsView_SelectallByPaging");
        }

        public DTO.Board.tblUserBoardsView.tblUserBoardsView SelectByID(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            throw new NotImplementedException();
        }

        public long SelectByUserID(DTO.Board.tblUserBoardsView.tblUserBoardsView obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblUserBoardsView_SelectBy_UserID").ObjectToLong();
        }
    }
}
