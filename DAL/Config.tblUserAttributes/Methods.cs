﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserAttributes;

namespace DAL.Config.tblUserAttributes
{
    public class Methods : DTO.Config.tblUserAttributes.IMethods
    {
        public List<DTO.Config.tblUserAttributes.tblUserAttributes> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblUserAttributes.tblUserAttributes SelectByUserID(DTO.Config.tblUserAttributes.tblUserAttributes obj)
        {
            List<DTO.Config.tblUserAttributes.tblUserAttributes> rlst = new List<DTO.Config.tblUserAttributes.tblUserAttributes>();

            rlst = SqlHelper.GetDataField(obj, "tblUserAttributes_SelectByUserID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public DTO.Config.tblUserAttributes.tblUserAttributes SelectByID(DTO.Config.tblUserAttributes.tblUserAttributes obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblUserAttributes.tblUserAttributes obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserAttributes_Insert");
        }

        public bool Update(DTO.Config.tblUserAttributes.tblUserAttributes obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserAttributes_Update");
        }

        public bool Delete(DTO.Config.tblUserAttributes.tblUserAttributes obj)
        {
            throw new NotImplementedException();
        }
    }
}
