﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Logger.tblUserRequestDataLog;

namespace DAL.Logger.tblUserRequestDataLog
{
    public class Methods : DTO.Logger.tblUserRequestDataLog.IMethods
    {
        public List<DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog SelectByID(DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserRequestDataLog_Insert");
        }

        public bool Update(DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog obj)
        {
            throw new NotImplementedException();
        }
    }
}
