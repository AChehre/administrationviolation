﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblOstans;

namespace DAL.Config.tblOstans
{
    public class Methods : DTO.Config.tblOstans.IMethods
    {
        public List<DTO.Config.tblOstans.tblOstans> SelectAll()
        {
            DTO.Config.tblOstans.tblOstans obj = new DTO.Config.tblOstans.tblOstans();
            return SqlHelper.GetDataField(obj, "tblOstans_SelectAll");
        }

        public List<DTO.Config.tblOstans.tblOstans> SelectAllUserOstans(DTO.Config.tblOstans.tblOstans obj)
        {
            return SqlHelper.GetDataField(obj, "tblOstans_SelectAllUserOstans");
        }

        public DTO.Config.tblOstans.tblOstans SelectByID(DTO.Config.tblOstans.tblOstans obj)
        {
            List<DTO.Config.tblOstans.tblOstans> rlst = new List<DTO.Config.tblOstans.tblOstans>();

            rlst = SqlHelper.GetDataField(obj, "tblOstans_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public bool Insert(DTO.Config.tblOstans.tblOstans obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblOstans.tblOstans obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblOstans.tblOstans obj)
        {
            throw new NotImplementedException();
        }
    }
}
