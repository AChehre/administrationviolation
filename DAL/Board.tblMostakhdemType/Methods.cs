﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblMostakhdemType;

namespace DAL.Board.tblMostakhdemType
{
    public class Methods : DTO.Board.tblMostakhdemType.IMethods
    {
        public List<DTO.Board.tblMostakhdemType.tblMostakhdemType> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblMostakhdemType.tblMostakhdemType SelectByID(DTO.Board.tblMostakhdemType.tblMostakhdemType obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblMostakhdemType_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblMostakhdemType.tblMostakhdemType obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblMostakhdemType.tblMostakhdemType obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblMostakhdemType.tblMostakhdemType obj)
        {
            throw new NotImplementedException();
        }
    }
}
