﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblGroupUserView;

namespace DAL.Config.tblGroupUserView
{
    public class Methods : DTO.Config.tblGroupUserView.IMethods
    {
        public List<DTO.Config.tblGroupUserView.tblGroupUserView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Config.tblGroupUserView.tblGroupUserView> SelectAllByPaging(DTO.Config.tblGroupUserView.tblGroupUserView obj)
        {
            return SqlHelper.GetDataField(obj, "tblGroupUserView_SelectAllByPaging");
        }

        public DTO.Config.tblGroupUserView.tblGroupUserView SelectByID(DTO.Config.tblGroupUserView.tblGroupUserView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Config.tblGroupUserView.tblGroupUserView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblGroupUserView.tblGroupUserView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblGroupUserView.tblGroupUserView obj)
        {
            throw new NotImplementedException();
        }
    }
}
