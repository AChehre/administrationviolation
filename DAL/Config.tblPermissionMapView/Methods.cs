﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblPermissionMapView;

namespace DAL.Config.tblPermissionMapView
{
    public class Methods : DTO.Config.tblPermissionMapView.IMethods
    {
        public List<DTO.Config.tblPermissionMapView.tblPermissionMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblPermissionMapView.tblPermissionMapView SelectByID(DTO.Config.tblPermissionMapView.tblPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblPermissionMapView.tblPermissionMapView SelectByAspxPage(DTO.Config.tblPermissionMapView.tblPermissionMapView obj)
        {
            List<DTO.Config.tblPermissionMapView.tblPermissionMapView> rlst = new List<DTO.Config.tblPermissionMapView.tblPermissionMapView>();

            rlst = SqlHelper.GetDataField(obj, "tblPermissionMapView_SelectByAspxPage");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public bool Insert(DTO.Config.tblPermissionMapView.tblPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblPermissionMapView.tblPermissionMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblPermissionMapView.tblPermissionMapView obj)
        {
            throw new NotImplementedException();
        }
    }
}
