﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblAccusitionView;

namespace DAL.Board.tblAccusitionView
{
    public class Methods : DTO.Board.tblAccusitionView.IMethods
    {
        public List<DTO.Board.tblAccusitionView.tblAccusitionView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblAccusitionView.tblAccusitionView SelectByID(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            List<DTO.Board.tblAccusitionView.tblAccusitionView> rlst = new List<DTO.Board.tblAccusitionView.tblAccusitionView>();

            rlst = SqlHelper.GetDataField(obj, "tblAccusitionView_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public List<DTO.Board.tblAccusitionView.tblAccusitionView> SelectByNotificationID(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            return SqlHelper.GetDataField(obj, "tblAccusitionView_SelectByNotificationID");
        }

        public bool Insert(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblAccusitionView.tblAccusitionView> SelectByFileID(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            return SqlHelper.GetDataField(obj, "tblAccusitionView_SelectBy_FileID");
        }

        public List<DTO.Board.tblAccusitionView.tblAccusitionView> SelectNotNotificated(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            return SqlHelper.GetDataField(obj, "tblAccusitionView_SelectNotNotificated");
        }

        public bool HadAnyNounStatusAccusation(DTO.Board.tblAccusitionView.tblAccusitionView obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusitionView_HadAnyNounStatusAccusation").ObjectToInt() > 0;
        }
    }
}
