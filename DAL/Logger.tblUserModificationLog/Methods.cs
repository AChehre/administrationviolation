﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Logger.tblUserModificationLog;

namespace DAL.Logger.tblUserModificationLog
{
    public class Methods : DTO.Logger.tblUserModificationLog.IMethods
    {
        public List<DTO.Logger.tblUserModificationLog.tblUserModificationLog> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Logger.tblUserModificationLog.tblUserModificationLog SelectByID(DTO.Logger.tblUserModificationLog.tblUserModificationLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Logger.tblUserModificationLog.tblUserModificationLog obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserModificationLog_Insert");
        }

        public bool Update(DTO.Logger.tblUserModificationLog.tblUserModificationLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Logger.tblUserModificationLog.tblUserModificationLog obj)
        {
            throw new NotImplementedException();
        }
    }
}
