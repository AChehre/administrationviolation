﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblBoardMemberMapView;

namespace DAL.Admin.tblBoardMemberMapView
{
    public class Methods : DTO.Admin.tblBoardMemberMapView.IMethods
    {
        public List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView> SelectBoardMemberByPaging(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoardMemberMapView_SelectBoardMemberByPaging");
        }

        public List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView> SelectBoardMember(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoardMemberMapView_SelectBoardMember");
        }

        public DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView SelectByID(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView SelectByMemberRole(DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblBoardMemberMapView_SelectManager");
            return lst.Any() ? lst[0] : obj;
        }
    }
}
