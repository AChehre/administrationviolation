﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblAccusation;
using System.Data.SqlClient;

namespace DAL.Board.tblAccusation
{
    public class Methods : DTO.Board.tblAccusation.IMethods
    {
        public List<DTO.Board.tblAccusation.tblAccusation> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblAccusation.tblAccusation SelectByID(DTO.Board.tblAccusation.tblAccusation obj)
        {
            List<DTO.Board.tblAccusation.tblAccusation> lst = SqlHelper.GetDataField(obj, "tblAccusation_SelectByID");
            if (lst.Any())
            {
                return lst[0];
            }
            return obj;
        }

        public long[] Insert(DTO.Board.tblAccusation.tblAccusation obj)
        {
            long[] lngReturn = new long[2];
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblAccusation_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    lngReturn[0] = cmd.Parameters["@AccusationID"].Value.ObjectToLong();
                    lngReturn[1] = cmd.Parameters["@RequestDataID"].Value.ObjectToLong();
                }
            }
            return lngReturn;
        }

        public bool Update(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.InsertDataField(obj, "tblAccusation_Update");
        }

        public bool UpdateStatus(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.InsertDataField(obj, "tblAccusation_UpdateStatus");
        }

        public bool Delete(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.InsertDataField(obj, "tblAccusation_Delete");
        }

        public bool HasUnknownAccusitionStatus(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusation_HasUnknownAccusitionStatus").ObjectToLong() > 0;
        }

        public List<DTO.Board.tblAccusation.tblAccusation> SelectByFileID(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.GetDataField(obj, "tblAccusation_SelectByFileID");
        }

        public DateTime? GetMaxAccusisionDate(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusation_SelectByMaxDate").ObjectToDate();
        }

        public bool HasAnyConfirmAccusation(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusitionView_HasConfirmedAccusation").ObjectToLong() > 0;
        }

        public bool CheckDeletePernission(DTO.Board.tblAccusation.tblAccusation obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusation_CheckDeletePermitted").ObjectToLong() > 0;
        }
    }
}
