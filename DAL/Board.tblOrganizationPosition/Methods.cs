﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblOrganizationPosition;

namespace DAL.Board.tblOrganizationPosition
{
    public class Methods : DTO.Board.tblOrganizationPosition.IMethods
    {
        public List<DTO.Board.tblOrganizationPosition.tblOrganizationPosition> SelectAll()
        {
            var obj = new DTO.Board.tblOrganizationPosition.tblOrganizationPosition();
            return SqlHelper.GetDataField(obj, "tblOrganizationPosition_SelectAll");
        }

        public DTO.Board.tblOrganizationPosition.tblOrganizationPosition SelectByID(DTO.Board.tblOrganizationPosition.tblOrganizationPosition obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblOrganizationPosition_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblOrganizationPosition.tblOrganizationPosition obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblOrganizationPosition.tblOrganizationPosition obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblOrganizationPosition.tblOrganizationPosition obj)
        {
            throw new NotImplementedException();
        }
    }
}
