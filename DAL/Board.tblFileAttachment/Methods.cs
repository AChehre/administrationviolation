﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblFileAttachment;

namespace DAL.Board.tblFileAttachment
{
    public class Methods : DTO.Board.tblFileAttachment.IMethods
    {
        public List<DTO.Board.tblFileAttachment.tblFileAttachment> SelectAll(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.GetDataField(obj,"tblFileAttachment_SelectBy_ViolationFileID");
        }

        public List<DTO.Board.tblFileAttachment.tblFileAttachment> SelectAllWithoutCheckOwnerID(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.GetDataField(obj, "tblFileAttachment_SelectAll");
        }

        public List<DTO.Board.tblFileAttachment.tblFileAttachment> SelectBy_ViolationFileID_OwnerRecordID(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.GetDataField(obj, "tblFileAttachment_SelectBy_ViolationFileID_OwnerRecordID");
        }

        public DTO.Board.tblFileAttachment.tblFileAttachment SelectByID(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            throw new NotImplementedException();
        }

        public long Insert(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.InsertDataField(obj, "tblFileAttachment_Insert",true).ObjectToLong();
        }

        public bool Insert(List<DTO.Board.tblFileAttachment.tblFileAttachment> lstobj)
        {
            return SqlHelper.InsertDataFieldList(lstobj, "tblFileAttachment_Insert");
        }

        public bool Update(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.InsertDataField(obj, "tblFileAttachment_Update");
        }

        public bool Delete(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.InsertDataField(obj, "tblFileAttachment_Delete");
        }

        public bool HasAllRelatedFileTypes(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblFileAttachment_SelectBy_ViolationFileID_OwnerRecordID").ToString().StringToByte() > 0;
        }

        public List<DTO.Board.tblFileAttachment.tblFileAttachment> SelectByRequestDataID(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            return SqlHelper.GetDataField(obj, "tblFileAttachment_SelectBy_RequestDataID");
        }

        public DateTime? GetMaxDocumentDate(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            DateTime? dt = SqlHelper.GetScalerValue(obj, "tblFileAttachment_GetMaxDocumentDate").ObjectToDate();
            return dt.HasValue ? dt : null;
        }
    }
}
