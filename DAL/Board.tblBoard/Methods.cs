﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblBoard;
using System.Data.SqlClient;

namespace DAL.Board.tblBoard
{
    public class Methods : DTO.Board.tblBoard.IMethods
    {
        public List<DTO.Board.tblBoard.tblBoard> SelectAll()
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard();
            return SqlHelper.GetDataField(obj, "tblBoard_SelectAll");
        }

        public List<DTO.Board.tblBoard.tblBoard> SelectAllByPaging(DTO.Board.tblBoard.tblBoard obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblBoard.tblBoard SelectByID(DTO.Board.tblBoard.tblBoard obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblBoard_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool CheckUserAccess(DTO.Board.tblBoard.tblBoard obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblBoard_CheckUserAccess"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@UserAccessStatus"].Value.ToString().StringToBool();
            }
        }

        public long Insert(DTO.Board.tblBoard.tblBoard obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblBoard.tblBoard obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblBoard.tblBoard obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblBoard.tblBoard> SelectByBoardType(DTO.Board.tblBoard.tblBoard obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoard_SelectBy_BoardType");
        }


        bool IMethods.Insert(DTO.Board.tblBoard.tblBoard obj)
        {
            throw new NotImplementedException();
        }

        public bool SelectByCode(DTO.Board.tblBoard.tblBoard obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblBoard_SelectByCode").ObjectToByte() > 0;
        }
    }
}
