﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Logger.tblUserLoginLog;

namespace DAL.Logger.tblUserLoginLog
{
    public class Methods : DTO.Logger.tblUserLoginLog.IMethods
    {
        public List<DTO.Logger.tblUserLoginLog.tblUserLoginLog> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Logger.tblUserLoginLog.tblUserLoginLog SelectByID(DTO.Logger.tblUserLoginLog.tblUserLoginLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Logger.tblUserLoginLog.tblUserLoginLog obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserLoginLog_Insert");
        }

        public bool Update(DTO.Logger.tblUserLoginLog.tblUserLoginLog obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Logger.tblUserLoginLog.tblUserLoginLog obj)
        {
            throw new NotImplementedException();
        }
    }
}
