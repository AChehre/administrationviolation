﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblAccusationNotificationMap;

namespace DAL.Board.tblAccusationNotificationMap
{
    public class Methods : DTO.Board.tblAccusationNotificationMap.IMethods
    {
        public List<DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap SelectByID(DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap obj)
        {
            throw new NotImplementedException();
        }

        public bool IsNotifiedAccusition(DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblAccusationNotificationMap_SelectBy_AccusationID").ObjectToLong() > 0;
        }
    }
}
