﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblRegions;

namespace DAL.Config.tblRegions
{
    public class Methods : DTO.Config.tblRegions.IMethods
    {
        public List<DTO.Config.tblRegions.tblRegions> SelectAll(DTO.Config.tblRegions.tblRegions obj)
        {
            return SqlHelper.GetDataField(obj, "tblRegions_SelectAll");
        }

        public List<DTO.Config.tblRegions.tblRegions> SelectAllUserRegions(DTO.Config.tblRegions.tblRegions obj)
        {
            return SqlHelper.GetDataField(obj, "tblRegions_SelectAllUserRegions");
        }

        public DTO.Config.tblRegions.tblRegions SelectByID(DTO.Config.tblRegions.tblRegions obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblRegions_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Config.tblRegions.tblRegions obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblRegions.tblRegions obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblRegions.tblRegions obj)
        {
            throw new NotImplementedException();
        }
    }
}
