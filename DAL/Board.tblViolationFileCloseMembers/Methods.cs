﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFileCloseMembers;

namespace DAL.Board.tblViolationFileCloseMembers
{
    public class Methods : DTO.Board.tblViolationFileCloseMembers.IMethods
    {
        public List<DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers> SelectByViolationFileID(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFileCloseMembers_SelectBy_ViolationFileID");
        }

        public DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers SelectByID(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            throw new NotImplementedException();

        }

        public bool Insert(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            //return SqlHelper.InsertDataField(obj, "tblViolationFileCloseMembers_Insert");
            throw new NotImplementedException();
        }

        public bool InsertList(List<DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers> objList)
        {
            return SqlHelper.InsertDataFieldList(objList, "tblViolationFileCloseMembers_Insert");
        }

        public bool Update(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByViolationFileID(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileCloseMembers_DeleteBy_ViolationFileID");
        }

        public bool Delete(DTO.Board.tblViolationFileCloseMembers.tblViolationFileCloseMembers obj)
        {
            throw new NotImplementedException();
        }
    }
}
