﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblMenu;
using System.Data.SqlClient;

namespace DAL.Config.tblMenu
{
    public class Methods : DTO.Config.tblMenu.IMethods
    {
        public List<DTO.Config.tblMenu.tblMenu> SelectAll()
        {
            DTO.Config.tblMenu.tblMenu obj = new DTO.Config.tblMenu.tblMenu();
            return SqlHelper.GetDataField(obj, "tblMenu_SelectAll");
        }

        public List<DTO.Config.tblMenu.tblMenu> SelectAll_Published()
        {
            DTO.Config.tblMenu.tblMenu obj = new DTO.Config.tblMenu.tblMenu();
            return SqlHelper.GetDataField(obj, "tblMenu_SelectAll_Published");
        }

        public List<DTO.Config.tblMenu.tblMenu> SelectAllByPaging(DTO.Config.tblMenu.tblMenu obj)
        {
            return SqlHelper.GetDataField(obj, "tblMenu_SelectAllByPaging");
        }

        public DTO.Config.tblMenu.tblMenu SelectByID(DTO.Config.tblMenu.tblMenu obj)
        {
            List<DTO.Config.tblMenu.tblMenu> rlst = new List<DTO.Config.tblMenu.tblMenu>();

            rlst = SqlHelper.GetDataField(obj, "tblMenu_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public int Insert(DTO.Config.tblMenu.tblMenu obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblMenu_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@MenuID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Config.tblMenu.tblMenu obj)
        {
            return SqlHelper.InsertDataField(obj, "tblMenu_Update");
        }

        public bool Delete(DTO.Config.tblMenu.tblMenu obj)
        {
            return SqlHelper.InsertDataField(obj, "tblMenu_Delete");
        }
    }
}
