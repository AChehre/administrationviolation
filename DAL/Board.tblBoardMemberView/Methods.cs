﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblBoardMemberView;

namespace DAL.Board.tblBoardMemberView
{
    public class Methods : DTO.Board.tblBoardMemberView.IMethods
    {
        public List<DTO.Board.tblBoardMemberView.tblBoardMemberView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblBoardMemberView.tblBoardMemberView SelectByID(DTO.Board.tblBoardMemberView.tblBoardMemberView obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblBoardMemberView.tblBoardMemberView> SelectByMemberUserID(DTO.Board.tblBoardMemberView.tblBoardMemberView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoardMemberView_SelectByMemberUserID");
        }

        public bool Insert(DTO.Board.tblBoardMemberView.tblBoardMemberView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblBoardMemberView.tblBoardMemberView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblBoardMemberView.tblBoardMemberView obj)
        {
            throw new NotImplementedException();
        }
    }
}
