﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblSendingFileRequest_BoardView;

namespace DAL.Board.tblSendingFileRequest_BoardView
{
    public class Methods : DTO.Board.tblSendingFileRequest_BoardView.IMethods
    {
        public List<DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView SelectByID(DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView> SelectByBoardID(DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView obj)
        {
            return SqlHelper.GetDataField(obj, "tblSendingFileRequest_BoardView_SelectByBoardID");
        }
    }
}
