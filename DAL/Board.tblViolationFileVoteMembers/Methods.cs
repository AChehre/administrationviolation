﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFileVoteMembers;

namespace DAL.Board.tblViolationFileVoteMembers
{
    public class Methods : DTO.Board.tblViolationFileVoteMembers.IMethods
    {
        public List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> SelectByViolationFileID(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFileVoteMembers_SelectBy_ViolationFileID");
        }

        public DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers SelectByID(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            throw new NotImplementedException();

        }

        public bool Insert(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            //return SqlHelper.InsertDataField(obj, "tblViolationFileVoteMembers_Insert");
            throw new NotImplementedException();
        }

        public bool InsertList(List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> objList)
        {
            return SqlHelper.InsertDataFieldList(objList, "tblViolationFileVoteMembers_Insert");
        }

        public bool Update(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByViolationFileID(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFileVoteMembers_DeleteBy_ViolationFileID");
        }

        public bool Delete(DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers obj)
        {
            throw new NotImplementedException();
        }
    }
}
