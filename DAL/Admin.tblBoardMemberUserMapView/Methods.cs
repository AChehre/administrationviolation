﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblBoardMemberUserMapView;

namespace DAL.Admin.tblBoardMemberUserMapView
{
    public class Methods : DTO.Admin.tblBoardMemberUserMapView.IMethods
    {
        public List<DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView> SearchByPaging(DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoardMemberUserMapView_SearchMemberByPaging");
        }

        public DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView SelectByID(DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj)
        {
            throw new NotImplementedException();
        }
    }
}
