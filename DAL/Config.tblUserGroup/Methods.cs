﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblUserGroup;
using System.Data.SqlClient;

namespace DAL.Config.tblUserGroup
{
    public class Methods : DTO.Config.tblUserGroup.IMethods
    {
        public List<DTO.Config.tblUserGroup.tblUserGroup> SelectAll()
        {
            DTO.Config.tblUserGroup.tblUserGroup obj = new DTO.Config.tblUserGroup.tblUserGroup();
            return SqlHelper.GetDataField(obj, "tblUserGroup_SelectAll");
        }

        public List<DTO.Config.tblUserGroup.tblUserGroup> SelectAllByPaging(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            return SqlHelper.GetDataField(obj, "tblUserGroup_SelectAllByPaging");
        }

        public DTO.Config.tblUserGroup.tblUserGroup SelectByID(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            List<DTO.Config.tblUserGroup.tblUserGroup> rlst = new List<DTO.Config.tblUserGroup.tblUserGroup>();

            rlst = SqlHelper.GetDataField(obj, "tblUserGroup_SelectByID");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public int Insert(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblUserGroup_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return cmd.Parameters["@GroupID"].Value.ToString().StringToInt();
            }
        }

        public bool Update(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroup_Update");
        }

        public bool Delete(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            return SqlHelper.InsertDataField(obj, "tblUserGroup_Delete");
        }

        public long ChcekForDelete(DTO.Config.tblUserGroup.tblUserGroup obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblUserGroup_ChcekForDelete").ToString().StringToLong();
        }
    }
}
