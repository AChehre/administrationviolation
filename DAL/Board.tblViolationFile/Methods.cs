﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblViolationFile;
using System.Data.SqlClient;

namespace DAL.Board.tblViolationFile
{
    public class Methods : DTO.Board.tblViolationFile.IMethods
    {
        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblViolationFile.tblViolationFile SelectByID(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblViolationFile_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public long Insert(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblViolationFile_Insert"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return (long)cmd.Parameters["@FileID"].Value;
                }
            }
            return 0;
        }

        public bool Update(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_Update");
        }

        public bool UpdateFinalResult(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_UpdateFinalResult");
        }

        public bool UpdateWorkflowReport(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_UpdateWorkflowReport");
        }

        public bool Delete(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAllByBoardID(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_SelectAll_By_BoardID");
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAllByPagingByBoardID(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_SelectAllPaging_By_BoardID");
        }

        public int GetFileStatus(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFile_GetStatus").ObjectToInt();
        }

        public bool ChangeVilationFileBoard(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_ChangeBoard");
        }

        public byte GetBoardType(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFile_GetBoardType").ObjectToByte();
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> HasAccusedAnotherOpenFile(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_AssusedHasAnotherOpenFile");
        }

        public bool SetTempCommandFlag(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_SetTempCommandFlag");
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAllByBoardID_For_SendingFile(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAllByBoardIDForDaftar(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_SelectAll_By_BoardID_For_Daftar");
        }

        public bool UpdatePanelReferHistoryCounterField(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_UpdateReferHistoryCounterField");
        }

        public bool SetFileCloseDate(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_SetFileCloseDate");
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAll_By_BoardID_For_VoteReport(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_SelectAll_By_BoardID_For_VoteReport");
        }

        public List<DTO.Board.tblViolationFile.tblViolationFile> SelectAllByPersonnelCode(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetDataField(obj, "tblViolationFile_SelectAll_By_PersonnelCode");
        }

        public bool SetNaghzeRay(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_SetNaghzeRay");
        }

        public bool CheckExistFileSerialCode(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblViolationFile_SelectFileSerial").ObjectToLong() > 0;
        }

        public bool SetDefenceBillFlag(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_SetDefenceBillFlag");
        }

        public bool UpdateClosingReport(DTO.Board.tblViolationFile.tblViolationFile obj)
        {
            return SqlHelper.InsertDataField(obj, "tblViolationFile_UpdateClosingReport");
        }
    }
}
