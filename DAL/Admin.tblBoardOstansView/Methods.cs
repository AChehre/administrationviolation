﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Admin.tblBoardOstansView;

namespace DAL.Admin.tblBoardOstansView
{
    public class Methods : DTO.Admin.tblBoardOstansView.IMethods
    {
        public List<DTO.Admin.tblBoardOstansView.tblBoardOstansView> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Admin.tblBoardOstansView.tblBoardOstansView SelectByID(DTO.Admin.tblBoardOstansView.tblBoardOstansView obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Admin.tblBoardOstansView.tblBoardOstansView> SelectAllByPaging(DTO.Admin.tblBoardOstansView.tblBoardOstansView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoard_SelectAllByPaging");
        }

        public bool Insert(DTO.Admin.tblBoardOstansView.tblBoardOstansView obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Admin.tblBoardOstansView.tblBoardOstansView obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Admin.tblBoardOstansView.tblBoardOstansView obj)
        {
            throw new NotImplementedException();
        }
    }
}
