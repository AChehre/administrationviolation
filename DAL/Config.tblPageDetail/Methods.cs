﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblPageDetail;

namespace DAL.Config.tblPageDetail
{
    public class Methods : DTO.Config.tblPageDetail.IMethods
    {
        public List<DTO.Config.tblPageDetail.tblPageDetail> SelectByPageID(DTO.Config.tblPageDetail.tblPageDetail obj)
        {
            return SqlHelper.GetDataField(obj, "tblPageDetail_SelectByPageID");
        }

        public DTO.Config.tblPageDetail.tblPageDetail SelectByPageID_ActionCode(DTO.Config.tblPageDetail.tblPageDetail obj)
        {
            List<DTO.Config.tblPageDetail.tblPageDetail> rlst = new List<DTO.Config.tblPageDetail.tblPageDetail>();

            rlst = SqlHelper.GetDataField(obj, "tblPageDetail_SelectByPageID_ActionCode");

            if (rlst.Any())
            {// چون  تابع اِنی از کانت سریع تر است، از این استفاده میکنیم
                obj = rlst[0];
            }

            return obj;
        }

        public bool Insert(DTO.Config.tblPageDetail.tblPageDetail obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPageDetail_Insert");
        }

        public bool Update(DTO.Config.tblPageDetail.tblPageDetail obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPageDetail_Update");
        }

        public bool Delete(DTO.Config.tblPageDetail.tblPageDetail obj)
        {
            return SqlHelper.InsertDataField(obj, "tblPageDetail_Delete");
        }
    }
}
