﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Admin.tblBoardUserMapView
{
    public class Methods
    {
        public List<DTO.Admin.tblBoardUserMapView.tblBoardUserMapView> SelectUsersNotMember(DTO.Admin.tblBoardUserMapView.tblBoardUserMapView obj)
        {
            return SqlHelper.GetDataField(obj, "tblBoard_SelectUsersNotMember");
        }
    }
}
