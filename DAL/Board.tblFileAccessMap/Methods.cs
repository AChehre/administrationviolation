﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblFileAccessMap;

namespace DAL.Board.tblFileAccessMap
{
    public class Methods : DTO.Board.tblFileAccessMap.IMethods
    {
        public List<DTO.Board.tblFileAccessMap.tblFileAccessMap> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblFileAccessMap.tblFileAccessMap SelectByID(DTO.Board.tblFileAccessMap.tblFileAccessMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblFileAccessMap.tblFileAccessMap obj)
        {
            return SqlHelper.InsertDataField(obj, "tblFileAccessMap_Insert");
        }

        public bool Update(DTO.Board.tblFileAccessMap.tblFileAccessMap obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblFileAccessMap.tblFileAccessMap obj)
        {
            throw new NotImplementedException();
        }

        public byte HasPermission(DTO.Board.tblFileAccessMap.tblFileAccessMap obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblFileAccessMap_CheckFileViewPermission").ObjectToByte();
        }
    }
}
