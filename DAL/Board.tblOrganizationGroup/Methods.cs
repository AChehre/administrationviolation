﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblOrganizationGroup;

namespace DAL.Board.tblOrganizationGroup
{
    public class Methods : DTO.Board.tblOrganizationGroup.IMethods
    {
        public List<DTO.Board.tblOrganizationGroup.tblOrganizationGroup> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblOrganizationGroup.tblOrganizationGroup SelectByID(DTO.Board.tblOrganizationGroup.tblOrganizationGroup obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblOrganizationGroup_SelectByID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Board.tblOrganizationGroup.tblOrganizationGroup obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblOrganizationGroup.tblOrganizationGroup obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblOrganizationGroup.tblOrganizationGroup obj)
        {
            throw new NotImplementedException();
        }
    }
}
