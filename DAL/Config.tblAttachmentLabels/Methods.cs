﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblAttachmentLabels;

namespace DAL.Config.tblAttachmentLabels
{
    public class Methods : DTO.Config.tblAttachmentLabels.IMethods
    {
        public List<DTO.Config.tblAttachmentLabels.tblAttachmentLabels> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Config.tblAttachmentLabels.tblAttachmentLabels SelectByID(DTO.Config.tblAttachmentLabels.tblAttachmentLabels obj)
        {
            var lst = SqlHelper.GetDataField(obj, "tblAttachmentLabels_SelectBy_AttchmentID");
            return lst.Any() ? lst[0] : obj;
        }

        public bool Insert(DTO.Config.tblAttachmentLabels.tblAttachmentLabels obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Config.tblAttachmentLabels.tblAttachmentLabels obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblAttachmentLabels.tblAttachmentLabels obj)
        {
            throw new NotImplementedException();
        }
    }
}
