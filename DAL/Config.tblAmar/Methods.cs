using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Config.tblAmar;
using System.Data.SqlClient;

namespace DAL.Config.tblAmar
{
    public class Methods : DTO.Config.tblAmar.IMethods
    {
        public List<DTO.Config.tblAmar.tblAmar> SelectAll()
        {
            throw new NotImplementedException();
        }

        public int VisitCountBetweenTowDate(DTO.Config.tblAmar.tblAmar obj)
        {
            using (SqlCommand cmd = SqlHelper.GetSqlCommand("tblAmar_VisitCountBetweenTowDate"))
            {
                SqlHelper.SetParamValue(cmd, obj);
                cmd.ExecuteNonQuery();
                return int.Parse(cmd.Parameters["@VisitCount"].Value.ToString());
            }
        }

        public bool Insert(DTO.Config.tblAmar.tblAmar obj)
        {
            return SqlHelper.InsertDataField(obj, "tblAmar_Insert");
        }

        public bool Update(DTO.Config.tblAmar.tblAmar obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Config.tblAmar.tblAmar obj)
        {
            throw new NotImplementedException();
        }
    }
}
