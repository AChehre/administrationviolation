﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO.Board.tblBoardAccusation;

namespace DAL.Board.tblBoardAccusation
{
    public class Methods : DTO.Board.tblBoardAccusation.IMethods
    {
        public List<DTO.Board.tblBoardAccusation.tblBoardAccusation> SelectAll()
        {
            throw new NotImplementedException();
        }

        public DTO.Board.tblBoardAccusation.tblBoardAccusation SelectByID(DTO.Board.tblBoardAccusation.tblBoardAccusation obj)
        {
            throw new NotImplementedException();
        }

        public bool Insert(DTO.Board.tblBoardAccusation.tblBoardAccusation obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(DTO.Board.tblBoardAccusation.tblBoardAccusation obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(DTO.Board.tblBoardAccusation.tblBoardAccusation obj)
        {
            throw new NotImplementedException();
        }

        public long SelectCountByAccusationSubjectID(DTO.Board.tblBoardAccusation.tblBoardAccusation obj)
        {
            return SqlHelper.GetScalerValue(obj, "tblBoardAccusation_SelectQuantityBy_AccusationSubjectID").ObjectToLong();
        }
    }
}
