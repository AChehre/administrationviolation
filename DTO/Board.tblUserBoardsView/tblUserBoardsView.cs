﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblUserBoardsView
{
    [Serializable]
    public class tblUserBoardsView
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? BoardType {set;get;}
		public System.String BoardTitle {set;get;}
		public System.Int32? OstanCode {set;get;}
		public System.Int32? RegionCode {set;get;}
		public System.Int32? SchoolCode {set;get;}
        public System.DateTime? StartActivity { set; get; }
		public System.DateTime? EndActivity {set;get;}
		public System.Byte? BoardStatus {set;get;}
		public System.Byte? BoardMemberMapStatus {set;get;}
		public System.Int64? MemberUserID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

