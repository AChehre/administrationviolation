﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblUserBoardsView
{
    public interface IMethods
    {
        List<tblUserBoardsView> SelectAll();
        tblUserBoardsView SelectByID(tblUserBoardsView obj);
        bool Insert(tblUserBoardsView obj);
        bool Update(tblUserBoardsView obj);
        bool Delete(tblUserBoardsView obj);
        long SelectByUserID(tblUserBoardsView obj);
    }
}
