﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblAccusationNotificationMap
{
    [Serializable]
    public class tblAccusationNotificationMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? RecordID {set;get;}
		public System.Int64? NotificationID {set;get;}
		public System.Int64? AccusationID {set;get;}
        public System.Int64? RequestDataID { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

