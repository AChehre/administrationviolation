﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblAccusationNotificationMap
{
    public interface IMethods
    {
        List<tblAccusationNotificationMap> SelectAll();
        tblAccusationNotificationMap SelectByID(tblAccusationNotificationMap obj);
        bool Insert(tblAccusationNotificationMap obj);
        bool Update(tblAccusationNotificationMap obj);
        bool Delete(tblAccusationNotificationMap obj);
        bool IsNotifiedAccusition(tblAccusationNotificationMap obj);
    }
}
