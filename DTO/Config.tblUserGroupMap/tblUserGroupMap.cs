﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserGroupMap
{
    [Serializable]
    public class tblUserGroupMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? UserID {set;get;}
		public System.Int32? UserGroupID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// برای بدست آوردن نتیجه دسترسی یک کاربر به یک گروه کاربری، استفاده میشود
        /// </summary>
        public System.Boolean AccessResult { set; get; }
    }
}

