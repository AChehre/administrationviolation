﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserGroupMap
{
    public interface IMethods
    {
        List<tblUserGroupMap> SelectAll();
        tblUserGroupMap SelectByID(tblUserGroupMap obj);
        bool Insert(tblUserGroupMap obj);
        bool Update(tblUserGroupMap obj);
        bool Delete(tblUserGroupMap obj);
    }
}
