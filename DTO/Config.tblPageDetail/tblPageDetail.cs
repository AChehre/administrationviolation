﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblPageDetail
{
    [Serializable]
    public class tblPageDetail
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? PageID {set;get;}
		public System.Byte? ActionCode {set;get;}
		public System.String ActionString {set;get;}
		public System.String PageTitle {set;get;}
		public System.String PageDescription {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// لیست گروه های کاربری ای که برای یک کاربر انتخاب شده است
        /// به صورت کاما از هم جدا شده اند
        /// </summary>
        public string SelectedUserGroup { set; get; }
    }
}

