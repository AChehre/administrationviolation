﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblPageDetail
{
    public interface IMethods
    {
        List<tblPageDetail> SelectByPageID(tblPageDetail obj);
        tblPageDetail SelectByPageID_ActionCode(tblPageDetail obj);
        bool Insert(tblPageDetail obj);
        bool Update(tblPageDetail obj);
        bool Delete(tblPageDetail obj);
    }
}
