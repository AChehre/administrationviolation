﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblBoardMemberView
{
    public interface IMethods
    {
        List<tblBoardMemberView> SelectAll();
        tblBoardMemberView SelectByID(tblBoardMemberView obj);
        bool Insert(tblBoardMemberView obj);
        bool Update(tblBoardMemberView obj);
        bool Delete(tblBoardMemberView obj);
    }
}
