﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblNewsGroupMap
{
    public interface IMethods
    {
        List<tblNewsGroupMap> SelectAll();
        List<tblNewsGroupMap> SelectByNewsID(tblNewsGroupMap obj);
        bool Insert(List<tblNewsGroupMap> lst);
        bool Update(tblNewsGroupMap obj);
        bool Delete(tblNewsGroupMap obj);
    }
}
