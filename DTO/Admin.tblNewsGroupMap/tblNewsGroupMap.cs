﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblNewsGroupMap
{
    [Serializable]
    public class tblNewsGroupMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 NewsID {set;get;}
		public System.Int64 GroupID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

