﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserGroupMenuMapView
{
    public interface IMethods
    {
        List<tblUserGroupMenuMapView> SelectAllByPaging(tblUserGroupMenuMapView obj);
        tblUserGroupMenuMapView SelectByID(tblUserGroupMenuMapView obj);
        bool Insert(tblUserGroupMenuMapView obj);
        bool Update(tblUserGroupMenuMapView obj);
        bool Delete(tblUserGroupMenuMapView obj);
    }
}
