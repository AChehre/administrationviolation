﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserGroupMenuMapView
{
    [Serializable]
    public class tblUserGroupMenuMapView
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? MenuID {set;get;}
		public System.Int32? UserGroupID {set;get;}
		public System.String Title {set;get;}
		public System.String Link {set;get;}
		public System.String GroupName {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }
    }
}

