﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Logger.tblUserRequestDataLog
{
    public interface IMethods
    {
        List<tblUserRequestDataLog> SelectAll();
        tblUserRequestDataLog SelectByID(tblUserRequestDataLog obj);
        bool Insert(tblUserRequestDataLog obj);
        bool Update(tblUserRequestDataLog obj);
        bool Delete(tblUserRequestDataLog obj);
    }
}
