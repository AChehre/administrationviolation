﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblPermissionMap
{
    public interface IMethods
    {
        List<tblPermissionMap> SelectAll();
        tblPermissionMap SelectByID(tblPermissionMap obj);
        bool Insert(tblPermissionMap obj);
        bool Update(tblPermissionMap obj);
        bool Delete(tblPermissionMap obj);
    }
}
