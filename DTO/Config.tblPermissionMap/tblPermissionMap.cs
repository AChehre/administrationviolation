﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblPermissionMap
{
    [Serializable]
    public class tblPermissionMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? ID {set;get;}
		public System.Int32? PageID {set;get;}
		public System.Byte? ActionCode {set;get;}
		public System.Int32? UserGroupID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

