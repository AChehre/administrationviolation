﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationNotification
{
    [Serializable]
    public class tblViolationNotification
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? NotificationID {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.DateTime? NotificationDate {set;get;}
		public System.Byte? NotificationType {set;get;}
		public System.DateTime? DeliveryDate {set;get;}
		public System.String RecipientName {set;get;}
		public System.String GiverName {set;get;}
		public System.String RecipientRelationship {set;get;}
		public System.String NotificationAddress {set;get;}
		public System.String LetterSerial {set;get;}
        public System.Byte? ReplyBill { set; get; }
        public System.Int64 RequestDataID { set; get; }
        public System.Int64 SerialCounter { set; get; }
        public System.Int64 BoardID { set; get; }
        //----------- برای صفحه بندی 
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        /// <summary>
        /// لیست آیدی اتهام های ابلاغیه
        /// </summary>
        public string SelectedAccusationID { get; set; }
        public System.Int32 ActivityYear { set; get; }
    }
}

