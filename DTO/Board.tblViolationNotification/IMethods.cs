﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationNotification
{
    public interface IMethods
    {
        List<tblViolationNotification> SelectAll();
        tblViolationNotification SelectByID(tblViolationNotification obj);
        bool Insert(tblViolationNotification obj);
        bool Update(tblViolationNotification obj);
        bool Delete(tblViolationNotification obj);
        List<tblViolationNotification> SelectByViolationFileID(tblViolationNotification obj);
        long SelectByRequestDataID(tblViolationNotification obj);
        bool CheckNotificationDate(tblViolationNotification obj);
        bool CheckExistLetterSerialCode(tblViolationNotification obj);
    }
}
