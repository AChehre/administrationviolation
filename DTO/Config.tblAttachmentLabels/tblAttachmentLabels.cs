﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblAttachmentLabels
{
    [Serializable]
    public class tblAttachmentLabels
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32 AttachmentTypeIndex {set;get;}
        public System.String DescriptionLabel { set; get; }
		public System.String DocumentDateLabel {set;get;}
		public System.String DocumentCodeLabel {set;get;}
		public System.String SelectFileLabel {set;get;}
		public System.String AttachmentListLabel {set;get;}	
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

