﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblAttachmentLabels
{
    public interface IMethods
    {
        List<tblAttachmentLabels> SelectAll();
        tblAttachmentLabels SelectByID(tblAttachmentLabels obj);
        bool Insert(tblAttachmentLabels obj);
        bool Update(tblAttachmentLabels obj);
        bool Delete(tblAttachmentLabels obj);
    }
}
