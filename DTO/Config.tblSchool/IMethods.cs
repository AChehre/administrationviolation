﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblSchool
{
    public interface IMethods
    {
        List<tblSchool> SelectAll(tblSchool obj);
        tblSchool SelectByID(tblSchool obj);
        bool Insert(tblSchool obj);
        bool Update(tblSchool obj);
        bool Delete(tblSchool obj);
    }
}
