﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblBoardMemberMap
{
    public interface IMethods
    {
        List<tblBoardMemberMap> SelectAll();
        tblBoardMemberMap SelectByID(tblBoardMemberMap obj);
        long Insert(tblBoardMemberMap obj);
        bool Update(tblBoardMemberMap obj);
        bool Delete(tblBoardMemberMap obj);
    }
}
