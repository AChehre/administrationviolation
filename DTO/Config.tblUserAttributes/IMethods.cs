﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserAttributes
{
    public interface IMethods
    {
        List<tblUserAttributes> SelectAll();
        tblUserAttributes SelectByID(tblUserAttributes obj);
        bool Insert(tblUserAttributes obj);
        bool Update(tblUserAttributes obj);
        bool Delete(tblUserAttributes obj);
    }
}
