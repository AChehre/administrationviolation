﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserAttributes
{
    [Serializable]
    public class tblUserAttributes
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? UserID {set;get;}
        public System.Int32? OstanCode { set; get; }
		public System.Int32? RegionCode {set;get;}
        public System.Int32? SchoolCode { set; get; }
        public System.Int64? PersonnelCode { set; get; }

        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

