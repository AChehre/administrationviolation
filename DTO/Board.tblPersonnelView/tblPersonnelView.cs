﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblPersonnelView
{
    [Serializable]
    public class tblPersonnelView
    {
        public System.Int64 RowNumber { set; get; }
        public System.Int64 PersonnelID { set; get; }
        public System.Byte? UserType { set; get; }
        public System.Int64 PersonnelCode { set; get; }
        public System.String FirstName { set; get; }
        public System.String LastName { set; get; }
        public System.String FatherName { set; get; }
        public System.String Tel { set; get; }
        public System.String Mobile { set; get; }
        public System.String Email { set; get; }
        public System.String Address { set; get; }
        public System.String PostalCode { set; get; }
        public System.Int32? OstanCode { set; get; }
        public System.Int32? RegionCode { set; get; }
        public System.Int32? SchoolCode { set; get; }
        public System.DateTime? BirthDate { set; get; }
        public System.String ShenasNo { set; get; }
        public System.String OrganizationName { set; get; }
        public System.String NounSpaceFName { set; get; }
        public System.String NounSpaceLName { set; get; }
        public System.Byte Gender { set; get; }
        public System.Byte Education { set; get; }
        public System.Int32? FarsiBirthDate { set; get; }
        public System.String NationalCode { set; get; }
        public System.String BirthPlace { set; get; }
        public System.Int32 OrganizationGroup { set; get; }
        public System.Int32 OrganizationPosition { set; get; }
        public System.Byte ActivityYears { set; get; }
        public System.String OstanName { set; get; }
        public System.String RegionName { set; get; }
        public System.String SchoolName { set; get; }
        public System.Boolean Married { set; get; }
        public System.Byte EmpType { set; get; }
        public System.Byte MostkhdemType { set; get; }
        public System.Byte EstekhdamStatus { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// نام و نام خانوادگی
        /// جهت استفاده در جستجو براساس نام
        /// </summary>
        public System.String FullName { set; get; }

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }
    }
}

