﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblPersonnelView
{
    public interface IMethods
    {
        List<tblPersonnelView> SelectAll();
        tblPersonnelView SelectByID(tblPersonnelView obj);
        bool Insert(tblPersonnelView obj);
        bool Update(tblPersonnelView obj);
        bool Delete(tblPersonnelView obj);
        List<tblPersonnelView> SelectByLikePersonnelCode(tblPersonnelView obj);
        List<tblPersonnelView> SelectByLikeName(tblPersonnelView obj);
        tblPersonnelView SelectByPersonnelCode(tblPersonnelView obj);
    }
}
