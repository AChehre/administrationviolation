﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblChangeFileBoard
{
    public interface IMethods
    {
        List<tblChangeFileBoard> SelectAll();
        tblChangeFileBoard SelectByID(tblChangeFileBoard obj);
        long[] Insert(tblChangeFileBoard obj);
        bool Update(tblChangeFileBoard obj);
        bool Delete(tblChangeFileBoard obj);
        long SelectByRequestDataID(tblChangeFileBoard obj);
    }
}
