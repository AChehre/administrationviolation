﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblChangeFileBoard
{
    [Serializable]
    public class tblChangeFileBoard
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ID {set;get;}
		public System.Int64? FromBoardID {set;get;}
		public System.Int64? ToBoardID {set;get;}
		public System.DateTime? ChangeDate {set;get;}
		public System.String Description {set;get;}
		public System.Byte? Status {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.Int64? RequestDataID {set;get;}
        public System.Byte FileReferType { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Int64 RequestID { set; get; }
        public System.Int64 UserID { set; get; }
        public System.Int32 ActivityYear { set; get; }
    }
}

