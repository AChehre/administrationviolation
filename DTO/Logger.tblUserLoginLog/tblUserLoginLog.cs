﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Logger.tblUserLoginLog
{
    [Serializable]
    public class tblUserLoginLog
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ID {set;get;}
		public System.Int64? UserID {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.DateTime? EventDate {set;get;}
		public System.Byte? LoginFlag {set;get;}
		public System.String IPAddress {set;get;}
		public System.String BrowserAgent {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

