﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Logger.tblUserLoginLog
{
    public interface IMethods
    {
        List<tblUserLoginLog> SelectAll();
        tblUserLoginLog SelectByID(tblUserLoginLog obj);
        bool Insert(tblUserLoginLog obj);
        bool Update(tblUserLoginLog obj);
        bool Delete(tblUserLoginLog obj);
    }
}
