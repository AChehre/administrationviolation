﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblFileRelationMap
{
    [Serializable]
    public class tblFileRelationMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 FileID {set;get;}
		public System.Int64 RelatedFileID {set;get;}
        public System.String SerialCode { set; get; }
        public System.Int64 BoardID { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

