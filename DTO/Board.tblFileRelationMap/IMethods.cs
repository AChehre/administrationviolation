﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblFileRelationMap
{
    public interface IMethods
    {
        List<tblFileRelationMap> SelectAll(tblFileRelationMap obj);
        tblFileRelationMap SelectByID(tblFileRelationMap obj);
        bool Insert(tblFileRelationMap obj);
        bool Update(tblFileRelationMap obj);
        bool Delete(tblFileRelationMap obj);
    }
}
