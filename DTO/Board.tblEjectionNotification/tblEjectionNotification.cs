﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblEjectionNotification
{
    [Serializable]
    public class tblEjectionNotification
    {
        public System.Int64 RowNumber { set; get; }
        public System.Int64 NotificationID { set; get; }
        public System.Int64 ViolationFileID { set; get; }
        public System.Int64 VoteID { set; get; }
        public System.DateTime? NotificationDate { set; get; }
        public System.DateTime? DeliveryDate { set; get; }
        public System.String RecipientName { set; get; }
        public System.String GiverName { set; get; }
        public System.String RecipientRelationship { set; get; }
        public System.String NotificationAddress { set; get; }
        public System.String LetterSerial { set; get; }
        public System.Int64? RequestDataID { set; get; }
        public System.String Description { set; get; }
        public System.Int32 RequestID { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Int64 BoardID { set; get; }
        public System.Int32? ActivityYear { set; get; }
        public System.Int64 UserID { set; get; }
    }
}

