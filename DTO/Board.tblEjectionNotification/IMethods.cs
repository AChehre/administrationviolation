﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblEjectionNotification
{
    public interface IMethods
    {
        List<tblEjectionNotification> SelectAll();
        tblEjectionNotification SelectByID(tblEjectionNotification obj);
        long[] Insert(tblEjectionNotification obj);
        bool Update(tblEjectionNotification obj);
        bool Delete(tblEjectionNotification obj);
        long SelectByRequestDataID(tblEjectionNotification obj);
        tblEjectionNotification SelectByFileID(tblEjectionNotification obj);
    }
}
