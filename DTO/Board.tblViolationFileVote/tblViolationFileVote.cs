﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationFileVote
{
    [Serializable]
    public class tblViolationFileVote
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 VoteID {set;get;}
		public System.Int64 ViolationFileID {set;get;}
		public System.Byte VoteType {set;get;}
		public System.Byte? PenaltyType {set;get;}
		public System.String PenaltyDetail {set;get;}
        public System.Boolean IsFinite { set; get; }
        public System.Byte FiniteVoteType { set; get; }
        public System.DateTime VoteDate { set; get; }
        public System.Int32 ActivityYear { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //-----------------------
        public System.Int64 VoteNumber { set; get; }
        public System.Boolean AutoVoteNumber { set; get; }
        public System.Byte? SodourType { set; get; }

        public System.DateTime FromDate { set; get; }
        public System.DateTime ToDate { set; get; }

        public System.Byte BoardType { set; get; }
        public System.Byte Education { set; get; }
        public System.Byte Gender { set; get; }
        public System.Int32? OstanCode { set; get; }
        public System.Int32? RegionCode { set; get; }
        public System.Int32 OrganizationPosition { set; get; }
        public System.Byte ActivityYears { set; get; }

        public System.Byte FromYear { set; get; }
        public System.Byte ToYear { set; get; }

        public System.Int64 BoardID { set;get; }
        public System.String Address { set; get; }
        public System.Byte FinalResult { set; get; }

        public System.Boolean isFinal { set; get; }
    }
}

