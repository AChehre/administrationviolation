﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFileVote
{
    public interface IMethods
    {
        List<tblViolationFileVote> SelectAll();
        tblViolationFileVote SelectByViolationFileID(tblViolationFileVote obj);
        tblViolationFileVote SelectByID(tblViolationFileVote obj);
        bool Insert(tblViolationFileVote obj);
        bool Update(tblViolationFileVote obj);
        bool Delete(tblViolationFileVote obj);
        tblViolationFileVote getVoteDate(tblViolationFileVote obj);
        long getPenaltyTypeCount(tblViolationFileVote obj);
        long getVoteCountByEducationalGrade(tblViolationFileVote obj);
        long getVoteCountByGender(tblViolationFileVote obj);
        long getVoteCountByOrganizationPosition(tblViolationFileVote obj);
        long getVoteCountByOstanCode(tblViolationFileVote obj);
        long getVoteCountByActivityYear(tblViolationFileVote obj);
    }
}
