﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblOstans
{
    public interface IMethods
    {
        List<tblOstans> SelectAll();
        tblOstans SelectByID(tblOstans obj);
        bool Insert(tblOstans obj);
        bool Update(tblOstans obj);
        bool Delete(tblOstans obj);
    }
}
