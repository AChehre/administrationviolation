﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblRequest
{
    [Serializable]
    public class tblRequest
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32 RequestID {set;get;}
		public System.String Title {set;get;}
        public System.String PrequisitRequestIDs { set; get; }
		public System.String RelatedAttachementTypes {set;get;}
		public System.Byte? RequestType {set;get;}
		public System.Int32? OrderIndex {set;get;}
		public System.Boolean? Enabled {set;get;}
        public System.Int32? FileStatusIndex { set; get; }
        public System.Byte BoardType { set; get; }
        public System.String FontColor { set; get; }
        public System.String BgColor { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

