﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblRequest
{
    public interface IMethods
    {
        List<tblRequest> SelectAll(tblRequest obj);
        tblRequest SelectByID(tblRequest obj);
        bool Insert(tblRequest obj);
        bool Update(tblRequest obj);
        bool Delete(tblRequest obj);
    }
}
