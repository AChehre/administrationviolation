﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Report.tblActivityReport
{
    public interface IMethods
    {
        tblActivityReport SelectByDate(tblActivityReport obj);
    }
}
