﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Report.tblActivityReport
{
    [Serializable]
    public class tblActivityReport
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 Jari_First_Of_Month {set;get;}
		public System.Int64 Jari_Vaseleh {set;get;}
		public System.Int64 Jari_Ghatie {set;get;}
		public System.Int64 Jari_Researchable {set;get;}
		public System.Int64 Jari_Innocence {set;get;}
		public System.Int64 Jari_Closed {set;get;}
		public System.Int64 Jari_Last_Of_Month {set;get;}
		public System.Int64 Madde_22_24_First_Of_Month {set;get;}
		public System.Int64 Madde_22_24_Vaseleh {set;get;}
		public System.Int64 Madde_22_24_Ghatie {set;get;}
		public System.Int64 Madde_22_24_Researchable {set;get;}
		public System.Int64 Madde_22_24_Innocence {set;get;}
		public System.Int64 Madde_22_24_Closed {set;get;}
		public System.Int64 Madde_22_24_Last_Of_Month {set;get;}
		public System.Int64 Madde_11_First_Of_Month {set;get;}
		public System.Int64 Madde_11_Vaseleh {set;get;}
		public System.Int64 Madde_11_Ghatie {set;get;}
		public System.Int64 Madde_11_Researchable {set;get;}
		public System.Int64 Madde_11_Innocence {set;get;}
		public System.Int64 Madde_11_Closed {set;get;}
		public System.Int64 Madde_11_Last_Of_Month {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.DateTime FromDate { set; get; }
        public System.DateTime ToDate { set; get; }
        public System.Int64? BoardID { set; get; }
    }
}

