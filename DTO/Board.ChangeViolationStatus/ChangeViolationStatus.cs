﻿
namespace DTO.Board.ChangeViolationStatus
{
    /// <summary>
    /// به ازای این کلاس،جدولی در دیتابیس نداریم و فقط جهت تغییر وضعیت پرونده در مراحل ثبت مستندات از آن استفاده میکنیم
    /// </summary>
    public class ChangeViolationStatus
    {
        /// <summary>
        /// 
        /// </summary>
        public System.Int64? NotificationID { set; get; }

        /// <summary>
        /// حالت ابلاغ شده پرونده،بعد از اینکه تمامی ضمیمه های پرونده پاک شود و به شرط اینکه حالت فعلی پرونده جمع آوری مستندات باشد، حالت پرونده به این ست میشود
        /// </summary>
        public System.Byte? ViolationFileEblaghShodeStatus { set; get; }

        /// <summary>
        /// حالت جمع آوری مستندات پرونده
        /// </summary>
        public System.Byte? ViolationFileJamavariMostanadatStatus { set; get; }

        /// <summary>
        /// حالت پیش نمایش رای پرونده
        /// </summary>
        public System.Byte? ViolationFilePishNamayeshRaeiStatus { set; get; } 
    }
}
// گزارش تخلف
// صدور رای صدور رای داشته باشه