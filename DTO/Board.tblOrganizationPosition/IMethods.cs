﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblOrganizationPosition
{
    public interface IMethods
    {
        List<tblOrganizationPosition> SelectAll();
        tblOrganizationPosition SelectByID(tblOrganizationPosition obj);
        bool Insert(tblOrganizationPosition obj);
        bool Update(tblOrganizationPosition obj);
        bool Delete(tblOrganizationPosition obj);
    }
}
