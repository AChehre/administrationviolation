﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblOrganizationPosition
{
    [Serializable]
    public class tblOrganizationPosition
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32 OrganizationPositionID {set;get;}
		public System.String OrganizationPositionName {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

