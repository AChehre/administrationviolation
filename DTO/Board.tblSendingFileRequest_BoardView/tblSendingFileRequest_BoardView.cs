﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblSendingFileRequest_BoardView
{
    [Serializable]
    public class tblSendingFileRequest_BoardView
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ID {set;get;}
        public System.Int64? BoardID { set; get; }
		public System.Int64? FromBoardID {set;get;}
		public System.Int64? ToBoardID {set;get;}
		public System.DateTime? RequestDate {set;get;}
		public System.String RequestedFileSerialCode {set;get;}
		public System.Int64? FileID {set;get;}
		public System.Byte? Status {set;get;}
		public System.Int64? RelatedFileID {set;get;}
		public System.String Comment {set;get;}
		public System.Int64? RequestDataID {set;get;}
		public System.String FromBoardTitle {set;get;}
		public System.String ToBoardTitle {set;get;}
        public System.Byte RequesterBoardType { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Byte ViewType { set; get; }
    }
}

