﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblSendingFileRequest_BoardView
{
    public interface IMethods
    {
        List<tblSendingFileRequest_BoardView> SelectAll();
        tblSendingFileRequest_BoardView SelectByID(tblSendingFileRequest_BoardView obj);
        bool Insert(tblSendingFileRequest_BoardView obj);
        bool Update(tblSendingFileRequest_BoardView obj);
        bool Delete(tblSendingFileRequest_BoardView obj);
        List<tblSendingFileRequest_BoardView> SelectByBoardID(tblSendingFileRequest_BoardView obj);
    }
}
