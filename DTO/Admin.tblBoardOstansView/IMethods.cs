﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblBoardOstansView
{
    public interface IMethods
    {
        List<tblBoardOstansView> SelectAll();
        tblBoardOstansView SelectByID(tblBoardOstansView obj);
        bool Insert(tblBoardOstansView obj);
        bool Update(tblBoardOstansView obj);
        bool Delete(tblBoardOstansView obj);
    }
}
