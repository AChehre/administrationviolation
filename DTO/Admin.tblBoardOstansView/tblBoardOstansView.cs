﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblBoardOstansView
{
    [Serializable]
    public class tblBoardOstansView
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? BoardType {set;get;}
		public System.String BoardTitle {set;get;}
		public System.Int32? OstanCode {set;get;}
		public System.Int32? RegionCode {set;get;}
		public System.Int32? SchoolCode {set;get;}
		public System.String OstanName {set;get;}
        public System.Byte? Status { set; get; }
        public System.String BoardCode { set; get; }
        public System.Int64 FileCounter { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// آیدی کاربری که میخواهیم هیات های موجود در مناطق قابل دسترس او را بدست بیاوریم
        /// </summary>
        public System.Int64? UserID { set; get; }

        /// <summary>
        /// کد استان جهت فیلتر کردن
        /// </summary>
        public System.Int32? OstanCodeForFilter { set; get; }

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }

        /// <summary>
        /// بجز این هیات
        /// </summary>
        public System.Byte? ExceptedBoardType { set; get; }
    }
}

