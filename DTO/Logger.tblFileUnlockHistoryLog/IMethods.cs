﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Logger.tblFileUnlockHistoryLog
{
    public interface IMethods
    {
        List<tblFileUnlockHistoryLog> SelectAll();
        tblFileUnlockHistoryLog SelectByID(tblFileUnlockHistoryLog obj);
        bool Insert(tblFileUnlockHistoryLog obj);
        bool Update(tblFileUnlockHistoryLog obj);
        bool Delete(tblFileUnlockHistoryLog obj);
    }
}
