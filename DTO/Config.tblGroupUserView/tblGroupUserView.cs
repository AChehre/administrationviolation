﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblGroupUserView
{
    [Serializable]
    public class tblGroupUserView
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? UserID {set;get;}
		public System.String UserName {set;get;}
		public System.String FName {set;get;}
		public System.String LName {set;get;}

        /// <summary>
        /// گرفتن نام کامل
        /// </summary>
        public System.String FullName
        {
            get
            {
                return FName + " " + LName;
            }
        }

		public System.String EMail {set;get;}
		public System.String Password {set;get;}
		public System.DateTime? RegisterDate {set;get;}
		public System.Boolean? IsActive {set;get;}

        /// <summary>
        /// وضعیت کاربر
        /// </summary>
        public System.String ActiveStatus
        {
            get
            {
                return IsActive.Value ? "فعال" : "غیر فعال";
            }
        }

		public System.Int32? UserGroupID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        // برای فیلتر
        public System.Int32? OstanCode { set; get; }
        public System.Int32? RegionCode { set; get; }
        public System.Int32? SchoolCode { set; get; }	
    }
}

