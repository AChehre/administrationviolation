﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblGroupUserView
{
    public interface IMethods
    {
        List<tblGroupUserView> SelectAll();
        tblGroupUserView SelectByID(tblGroupUserView obj);
        bool Insert(tblGroupUserView obj);
        bool Update(tblGroupUserView obj);
        bool Delete(tblGroupUserView obj);
    }
}
