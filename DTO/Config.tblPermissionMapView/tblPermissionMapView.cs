﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblPermissionMapView
{
    [Serializable]
    public class tblPermissionMapView
    {
        public System.Int64 RowNumber { set; get; }
        public System.Int32? PermissionID { set; get; }
        public System.Int32? UserGroupID { set; get; }
        public System.Int32? PageID { set; get; }
        public System.Byte? ActionCode { set; get; }
        public System.Boolean? IsPublished { set; get; }
        public System.String AspxPage { set; get; }
        public System.Byte? DefaultActionCode { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        //برای اینکه لیست آیدی گروه های کاربری یک کاربر رو به دیتابیس بفرستیم
        //تا به ازای تمامی گروه ها، سطح دسترسی کاربر را به صفحه بررسی کنیم
        public string UserGroupIDs { get; set; }
    }
}

