﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblPermissionMapView
{
    public interface IMethods
    {
        List<tblPermissionMapView> SelectAll();
        tblPermissionMapView SelectByID(tblPermissionMapView obj);
        bool Insert(tblPermissionMapView obj);
        bool Update(tblPermissionMapView obj);
        bool Delete(tblPermissionMapView obj);
    }
}
