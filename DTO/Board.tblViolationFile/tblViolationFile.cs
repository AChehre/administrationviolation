﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationFile
{
    [Serializable]
    public class tblViolationFile
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 FileID {set;get;}
		public System.Int64? PersonnelCode {set;get;}
		public System.Int32? Status {set;get;}
		public System.String SerialCode {set;get;}
		public System.DateTime? CreationDate {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? FinalResult {set;get;}
		public System.String FirstName {set;get;}
		public System.String LastName {set;get;}
		public System.String NationalCode {set;get;}
		public System.String FatherName {set;get;}
		public System.Int32? ActivityYear {set;get;}
		public System.String WorkflowReport {set;get;}
		public System.String OstanName {set;get;}
		public System.String RegionName {set;get;}
		public System.String SchoolName {set;get;}
		public System.Boolean? HasTempCommand {set;get;}
        public System.Int64 SerialCounter { set; get; }
        public System.String ClosingReport { set; get; }

        public System.Int32 OstanCode { set; get; }
        public System.Int32 RegionCode { set; get; }
        public System.Int32 SchoolCode { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.String BoardTitle { set; get; }
        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }

        /// <summary>
        /// تاریخ شروع محدوده جستجو
        /// </summary>
        public DateTime? FromDate { set; get; }

        /// <summary>
        /// تاریخ پایان محدوده جستجو
        /// </summary>
        public DateTime? ToDate { set; get; }

        /// <summary>
        /// حالت فعلی پرونده، به عنوان پارامتر برای اس پی ها لازم داریم
        /// </summary>
        public System.Byte? OldStatus { set; get; }
        public System.Byte FileReferType { set; get; }
        public System.Byte? HintTag { set; get; }

        public System.Int64? CurrentBoardID { set; get; }
        public System.Int64 ReferHistoryCounter { set; get; }

        public System.Byte? VoteTypeCode { set; get; }
        public DateTime? FileCloseDate { set; get; }
        public System.Boolean OrderByReferType { set; get; }
        public System.Byte NaghzeRay { set; get; }
        public System.Boolean HasDefenseBill { set; get; }
    }
}

