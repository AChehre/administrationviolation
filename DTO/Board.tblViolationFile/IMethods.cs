﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFile
{
    public interface IMethods
    {
        bool ChangeVilationFileBoard(tblViolationFile obj);
        bool CheckExistFileSerialCode(tblViolationFile obj);
        bool Delete(tblViolationFile obj);
        byte GetBoardType(tblViolationFile obj);
        int GetFileStatus(tblViolationFile obj);
        List<tblViolationFile> HasAccusedAnotherOpenFile(tblViolationFile obj);
        long Insert(tblViolationFile obj);
        List<tblViolationFile> SelectAll();
        List<tblViolationFile> SelectAll_By_BoardID_For_VoteReport(tblViolationFile obj);
        List<tblViolationFile> SelectAllByBoardID(tblViolationFile obj);
        List<tblViolationFile> SelectAllByBoardID_For_SendingFile(tblViolationFile obj);
        List<tblViolationFile> SelectAllByBoardIDForDaftar(tblViolationFile obj);
        List<tblViolationFile> SelectAllByPagingByBoardID(tblViolationFile obj);
        List<tblViolationFile> SelectAllByPersonnelCode(tblViolationFile obj);
        tblViolationFile SelectByID(tblViolationFile obj);
        bool SetDefenceBillFlag(tblViolationFile obj);
        bool SetFileCloseDate(tblViolationFile obj);
        bool SetNaghzeRay(tblViolationFile obj);
        bool SetTempCommandFlag(tblViolationFile obj);
        bool Update(tblViolationFile obj);
        bool UpdateFinalResult(tblViolationFile obj);
        bool UpdatePanelReferHistoryCounterField(tblViolationFile obj);
        bool UpdateWorkflowReport(tblViolationFile obj);
        bool UpdateClosingReport(tblViolationFile obj);
    }
}
