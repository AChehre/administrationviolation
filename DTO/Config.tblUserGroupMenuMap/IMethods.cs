﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserGroupMenuMap
{
    public interface IMethods
    {
        List<tblUserGroupMenuMap> SelectAll();
        tblUserGroupMenuMap SelectByID(tblUserGroupMenuMap obj);
        bool Insert(tblUserGroupMenuMap obj);
        bool Update(tblUserGroupMenuMap obj);
        bool Delete(tblUserGroupMenuMap obj);
    }
}
