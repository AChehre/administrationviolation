﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserGroupMenuMap
{
    [Serializable]
    public class tblUserGroupMenuMap
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? MenuID {set;get;}
		public System.Int32? UserGroupID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

