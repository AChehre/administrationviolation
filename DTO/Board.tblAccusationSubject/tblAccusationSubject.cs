﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblAccusationSubject
{
    [Serializable]
    public class tblAccusationSubject
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? SubjectID {set;get;}
		public System.String Title {set;get;}
		public System.String Description {set;get;}
		public System.Byte? SubjectGroupID {set;get;}
		public System.String Status {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

