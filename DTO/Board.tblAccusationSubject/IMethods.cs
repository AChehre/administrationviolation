﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblAccusationSubject
{
    public interface IMethods
    {
        List<tblAccusationSubject> SelectAll();
        tblAccusationSubject SelectByID(tblAccusationSubject obj);
        bool Insert(tblAccusationSubject obj);
        bool Update(tblAccusationSubject obj);
        bool Delete(tblAccusationSubject obj);
    }
}
