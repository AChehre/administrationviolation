﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblFileAttachment
{
    [Serializable]
    public class tblFileAttachment
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? AttachmentID {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.String AttachmentSource {set;get;}
		public System.Int32? AttachmentType {set;get;}
		public System.Int64? OwnerRecordID {set;get;}
		public System.String AttachmentTitle {set;get;}
        public System.DateTime? RegDate { set; get; }
        public System.Byte? Status { set; get; }
        public System.Int64? RequestDataID { set; get; }
        public System.String FileTypeParms { set; get; }
        public System.Int32 NumberOfNeedFileType { set; get; }

        public System.DateTime? DocumentDate { set; get; }
        public System.String DocumentCode {set;get;}

        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// حالت ابلاغ شده پرونده،بعد از اینکه تمامی ضمیمه های پرونده پاک شود و به شرط اینکه حالت فعلی پرونده جمع آوری مستندات باشد، حالت پرونده به این ست میشود
        /// </summary>
        public System.Byte? ViolationFileEblaghShodeStatus { set; get; }

        /// <summary>
        /// حالت جمع آوری مستندات پرونده
        /// </summary>
        public System.Byte? ViolationFileJamavariMostanadatStatus { set; get; } 
    }
}

