﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblFileAttachment
{
    public interface IMethods
    {
        bool Delete(tblFileAttachment obj);
        long Insert(tblFileAttachment obj);
        bool Insert(List<tblFileAttachment> lstobj);
        List<tblFileAttachment> SelectAll(tblFileAttachment obj);
        List<tblFileAttachment> SelectAllWithoutCheckOwnerID(tblFileAttachment obj);
        List<tblFileAttachment> SelectBy_ViolationFileID_OwnerRecordID(tblFileAttachment obj);
        tblFileAttachment SelectByID(tblFileAttachment obj);
        bool Update(tblFileAttachment obj);
        bool HasAllRelatedFileTypes(tblFileAttachment obj);
        List<tblFileAttachment> SelectByRequestDataID(tblFileAttachment obj);
        DateTime? GetMaxDocumentDate(tblFileAttachment obj);
    }
}
