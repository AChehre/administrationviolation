﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserGroup
{
    [Serializable]
    public class tblUserGroup
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? GroupID {set;get;}
		public System.String GroupName {set;get;}
		public System.Int32? ParentID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

