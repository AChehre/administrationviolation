﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserGroup
{
    public interface IMethods
    {
        List<tblUserGroup> SelectAll();
        tblUserGroup SelectByID(tblUserGroup obj);
        int Insert(tblUserGroup obj);
        bool Update(tblUserGroup obj);
        bool Delete(tblUserGroup obj);
    }
}
