﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblAccusation
{
    [Serializable]
    public class tblAccusation
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? AccusationID {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.Int64? AccusationSubjectID {set;get;}
		public System.DateTime? AccusationDate {set;get;}
        public System.DateTime? RegisterDate { set; get; }
		public System.String Description {set;get;}
		public System.Byte? Status {set;get;}
        public System.String ReporterName { set; get; }
        public System.Int64 RequestDataID { set; get; }
        public System.Int32 RequestID { set; get; }
        public System.Byte ReportSource { set; get; }
        public System.Byte EventPlace { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //---------------------- 
        /// <summary>
        /// لیست آیدی اتهام ها
        /// </summary>
        public string SelectedAccusationID { get; set; }
        public System.Int64 UserID { set; get; }
    }
}

