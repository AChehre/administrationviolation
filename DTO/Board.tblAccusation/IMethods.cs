﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblAccusation
{
    public interface IMethods
    {
        List<tblAccusation> SelectAll();
        tblAccusation SelectByID(tblAccusation obj);
        long[] Insert(tblAccusation obj);
        bool Update(tblAccusation obj);
        bool Delete(tblAccusation obj);
        bool HasUnknownAccusitionStatus(tblAccusation obj);
        List<tblAccusation> SelectByFileID(tblAccusation obj);
        DateTime? GetMaxAccusisionDate(tblAccusation obj);
        bool HasAnyConfirmAccusation(tblAccusation obj);
        bool CheckDeletePernission(tblAccusation obj);
    }
}
