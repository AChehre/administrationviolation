﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblEstekhdamStatus
{
    public interface IMethods
    {
        List<tblEstekhdamStatus> SelectAll();
        tblEstekhdamStatus SelectByID(tblEstekhdamStatus obj);
        bool Insert(tblEstekhdamStatus obj);
        bool Update(tblEstekhdamStatus obj);
        bool Delete(tblEstekhdamStatus obj);
    }
}
