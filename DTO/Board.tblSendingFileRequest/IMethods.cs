﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblSendingFileRequest
{
    public interface IMethods
    {
        List<tblSendingFileRequest> SelectAll();
        tblSendingFileRequest SelectByID(tblSendingFileRequest obj);
        long[] Insert(tblSendingFileRequest obj);
        bool Update(tblSendingFileRequest obj);
        bool Delete(tblSendingFileRequest obj);
        long SelectByRequestDataID(tblSendingFileRequest obj);
        List<tblSendingFileRequest> SelectByRelatedFileID(tblSendingFileRequest obj);
        bool UpdateStatus(tblSendingFileRequest obj);
    }
}
