﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblRequestData
{
    public interface IMethods
    {
        List<tblRequestData> SelectAll();
        tblRequestData SelectByID(tblRequestData obj);
        long Insert(tblRequestData obj);
        bool Update(tblRequestData obj);
        bool Delete(tblRequestData obj);
        List<tblRequestData> SelectByFileID(tblRequestData obj);
        bool UpdateStatus(tblRequestData obj);
        int CheckHasDependedRequestData(tblRequestData obj);
        bool tblRequestDataCheckAllPrequisit(tblRequestData obj);
        tblRequestData getVoteDate(tblRequestData obj);
        bool CheckAttachmentDateForVote(tblRequestData obj);
        tblRequestData getEjectionDate(tblRequestData obj);
        tblRequestData SelectByRequestID(tblRequestData obj);
        bool HasAnyNotCompletedData(tblRequestData obj);
    }
}
