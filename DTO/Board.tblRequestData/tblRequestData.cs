﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblRequestData
{
    [Serializable]
    public class tblRequestData
    {
        public System.Int64 RowNumber { set; get; }
        public System.Int64 RequestDataID { set; get; }
        public System.Int32? RequestID { set; get; }
        public System.Byte? Status { set; get; }
        public System.Int64? FileID { set; get; }
        public System.String Note { set; get; }
        public System.DateTime? RequestDate { set; get; }
        public System.String PrequesitRequestDataID { set; get; }
        public System.String RelatedAttachementTypes { set; get; }
        public System.String Title { set; get; }
        public System.Byte? RequestType { set; get; }
        public System.Int32? OrderIndex { set; get; }
        public System.Boolean? Enabled { set; get; }
        public System.Int32? FileStatusIndex { set; get; }
        public System.Byte? BoardType { set; get; }
        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Boolean UpdateFileStatus { set; get; }
        public System.Byte HintTag { set; get; }
        public System.Int64 UserID { set; get; }
        public System.String FName { set; get; }
        public System.String LName { set; get; }
        public System.Byte OperationType { set; get; }
    }
}

