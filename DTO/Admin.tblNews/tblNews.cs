﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblNews
{
    [Serializable]
    public class tblNews
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ID {set;get;}
		public System.String Title {set;get;}
		public System.String ShortDescription {set;get;}
		public System.String Comment {set;get;}
		public System.DateTime? WriteDate {set;get;}
		public System.DateTime? ModifyDate {set;get;}
		public System.Boolean? Published {set;get;}
		public System.Int32? CategoryID {set;get;}
		public System.String ImageUrl {set;get;}
        public System.DateTime ExpirationDate { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public DateTime FromDate { set; get; }
        public DateTime ToDate { set; get; }

        public System.String GroupIDs { set; get; }
    }
}

