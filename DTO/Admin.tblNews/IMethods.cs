﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblNews
{
    public interface IMethods
    {
        List<tblNews> SelectAll(tblNews obj);
        tblNews SelectByID(tblNews obj);
        long Insert(tblNews obj);
        bool Update(tblNews obj);
        bool Delete(tblNews obj);
        List<tblNews> SelectByDate(tblNews obj);
    }
}
