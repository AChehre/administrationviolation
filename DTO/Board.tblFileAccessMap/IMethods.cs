﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblFileAccessMap
{
    public interface IMethods
    {
        List<tblFileAccessMap> SelectAll();
        tblFileAccessMap SelectByID(tblFileAccessMap obj);
        bool Insert(tblFileAccessMap obj);
        bool Update(tblFileAccessMap obj);
        bool Delete(tblFileAccessMap obj);
        byte HasPermission(tblFileAccessMap obj);
    }
}
