﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFileVoteMembers
{
    public interface IMethods
    {
        bool Delete(tblViolationFileVoteMembers obj);
        bool DeleteByViolationFileID(tblViolationFileVoteMembers obj);
        bool Insert(tblViolationFileVoteMembers obj);
        bool InsertList(List<tblViolationFileVoteMembers> objList);
        List<tblViolationFileVoteMembers> SelectAll();
        tblViolationFileVoteMembers SelectByID(tblViolationFileVoteMembers obj);
        List<tblViolationFileVoteMembers> SelectByViolationFileID(tblViolationFileVoteMembers obj);
        bool Update(tblViolationFileVoteMembers obj);
    }
}

