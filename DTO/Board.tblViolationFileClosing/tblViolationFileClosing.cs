﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationFileClosing
{
    [Serializable]
    public class tblViolationFileClosing
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ClosingID {set;get;}
		public System.Int64 ViolationFileID {set;get;}
		public System.Int64? ClosingNumber {set;get;}
		public System.DateTime? ClosingDate {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Int32? ActivityYear {set;get;}
        public System.Boolean AutoClosingNumber { set; get; }
        public System.Byte SodourType { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Boolean isFinal { set; get; }
    }
}

