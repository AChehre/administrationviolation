﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFileClosing
{
    public interface IMethods
    {
        List<tblViolationFileClosing> SelectAll();
        tblViolationFileClosing SelectByViolationFileID(tblViolationFileClosing obj);
        tblViolationFileClosing SelectByID(tblViolationFileClosing obj);
        bool Insert(tblViolationFileClosing obj);
        bool Update(tblViolationFileClosing obj);
        bool Delete(tblViolationFileClosing obj);
        bool CheckExistFileSerialCode(tblViolationFileClosing obj);
    }
}
