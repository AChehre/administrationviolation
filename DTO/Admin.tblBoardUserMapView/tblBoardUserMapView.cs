﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblBoardUserMapView
{
    /// <summary>
    /// از این کلاس برای بدست آوردن کاربرانی که عضو یک هیات خاص نیستند استفاده میکنیم و 
    /// در دیتابیس جدولی به نام این کلاس وجود ندارد و خودم ساخته ام
    /// </summary>
    public class tblBoardUserMapView
    {
        public System.Int64? RowNumber { set; get; }
        public System.Int64? BoardID { set; get; }
        public System.Int32? OstanCode { set; get; }
        public System.Int32? RegionCode { set; get; }
        public System.Int32? SchoolCode { set; get; }

        public System.Int64? UserID { set; get; }
        public System.String FName { set; get; }
        public System.String LName { set; get; }

        /// <summary>
        /// گرفتن نام کامل
        /// </summary>
        public System.String FullName
        {
            get
            {
                return FName + " " + LName;
            }
        }

        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// آیدی گروه کاربری جهت انتخاب کاربرانی که در هیات عضو نیستند
        /// </summary>
        public System.Int32? SelectedUserGroupID { set; get; }
    }
}
