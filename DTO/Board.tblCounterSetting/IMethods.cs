﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblCounterSetting
{
    public interface IMethods
    {
        List<tblCounterSetting> SelectAll();
        tblCounterSetting SelectByID(tblCounterSetting obj);
        bool Insert(tblCounterSetting obj);
        bool Update(tblCounterSetting obj);
        bool Delete(tblCounterSetting obj);
    }
}
