﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblCounterSetting
{
    [Serializable]
    public class tblCounterSetting
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 BoardID {set;get;}
		public System.Int32 Year {set;get;}
        public System.Int64 ViolationNotificationCounter { set; get; }
		public System.Int64 VoteNotificationCounter {set;get;}
		public System.Int64 VoteCounter {set;get;}
		public System.Int64 EjectionNotificationCounter {set;get;}
		public System.Int64 FileSerialCounter {set;get;}
        public System.Int64 ClosingCounter { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

