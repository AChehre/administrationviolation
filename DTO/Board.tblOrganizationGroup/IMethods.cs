﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblOrganizationGroup
{
    public interface IMethods
    {
        List<tblOrganizationGroup> SelectAll();
        tblOrganizationGroup SelectByID(tblOrganizationGroup obj);
        bool Insert(tblOrganizationGroup obj);
        bool Update(tblOrganizationGroup obj);
        bool Delete(tblOrganizationGroup obj);
    }
}
