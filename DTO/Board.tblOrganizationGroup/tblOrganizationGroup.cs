﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblOrganizationGroup
{
    [Serializable]
    public class tblOrganizationGroup
    {
        public System.Int64 RowNumber {set;get;}		public System.Int32 OrganizationGroupID {set;get;}		public System.String OrganizationGroupname {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

