﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblAccusitionView
{
    public interface IMethods
    {
        List<tblAccusitionView> SelectAll();
        tblAccusitionView SelectByID(tblAccusitionView obj);
        bool Insert(tblAccusitionView obj);
        bool Update(tblAccusitionView obj);
        bool Delete(tblAccusitionView obj);
        List<tblAccusitionView> SelectByFileID(tblAccusitionView obj);
        bool HadAnyNounStatusAccusation(tblAccusitionView obj);
    }
}
