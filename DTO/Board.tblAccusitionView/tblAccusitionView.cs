﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblAccusitionView
{
    [Serializable]
    public class tblAccusitionView
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? AccusationID {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.Int64? AccusationSubjectID {set;get;}
		public System.DateTime? AccusationDate {set;get;}

        public System.DateTime? RegisterDate { set; get; }
		public System.String Description {set;get;}
		public System.Byte? Status {set;get;}
		public System.String Title {set;get;}
        public System.Int64? RequestDataID { set; get; }
        public System.Int64? PrequesitRequestDataID {set;get;}
        public System.Byte? RequestStatus { set; get; }

        public System.Byte ReportSource { set; get; }
        public System.Byte EventPlace { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// جهت انتخاب اتهامات مربوط به یک ابلاغیه
        /// </summary>
        public System.Int64? NotificationID { set; get; }
    }
}

