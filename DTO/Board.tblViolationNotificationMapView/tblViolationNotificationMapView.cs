﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationNotificationMapView
{
    [Serializable]
    public class tblViolationNotificationMapView
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? AccusationID {set;get;}
		public System.Int64? NotificationID {set;get;}
		public System.Int64? ViolationFileID {set;get;}
		public System.DateTime? NotificationDate {set;get;}
		public System.Byte? NotificationType {set;get;}
		public System.DateTime? DeliveryDate {set;get;}
		public System.String RecipientName {set;get;}
		public System.String GiverName {set;get;}
		public System.String RecipientRelationship {set;get;}
		public System.String NotificationAddress {set;get;}
		public System.String LetterSerial {set;get;}
        public System.Byte? ReplyBill { set; get; }
        public System.Int32? RequestID { set; get; }
        public System.Int64? RequestDataID { set; get; }
        public System.String Description { set; get; }
        public System.Int64 BoardID { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Int64 UserID { set; get; }
        public System.Int32? ActivityYear { set; get; }
        /// <summary>
        /// لیست آیدی اتهام های ابلاغیه
        /// </summary>
        public string SelectedAccusationID { get; set; }

        /// <summary>
        ///  حالت پرونده وصول شده، بعد از حذف یا ویرایش یک ابلاغ، اگر  اتهامی ابلاغ نشده ماند، حالت پرونده به این ست میشود 
        /// </summary>
        public System.Byte? ViolationFileVosulShodeStatus { get; set; }

        /// <summary>
        /// حالت ابلاغ شده پرونده،بعد از اینکه تمامی اتهام ها ابلاغ شوند، حالت پرونده به این ست میشود
        /// </summary>
        public System.Byte? ViolationFileEblaghShodeStatus { get; set; }
        public System.Boolean AutoLetterSerial { set; get; }
        public System.Int64 NotificationCounter { set; get; }
    }
}

