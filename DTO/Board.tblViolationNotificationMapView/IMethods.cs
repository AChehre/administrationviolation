﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationNotificationMapView
{
    public interface IMethods
    {
        List<tblViolationNotificationMapView> SelectAll();
        tblViolationNotificationMapView SelectByID(tblViolationNotificationMapView obj);
        long[] Insert(tblViolationNotificationMapView obj);
        bool Update(tblViolationNotificationMapView obj);
       
    }
}
