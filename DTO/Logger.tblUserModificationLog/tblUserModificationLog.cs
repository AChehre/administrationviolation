﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Logger.tblUserModificationLog
{
    [Serializable]
    public class tblUserModificationLog
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64 ID {set;get;}
		public System.Int64? UserID {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Int64? TargetUserID {set;get;}
		public System.Byte? ActionFlag {set;get;}
		public System.DateTime? EventDate {set;get;}
		public System.String IPAddress {set;get;}
		public System.String BrowserAgent {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

