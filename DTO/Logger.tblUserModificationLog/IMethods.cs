﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Logger.tblUserModificationLog
{
    public interface IMethods
    {
        List<tblUserModificationLog> SelectAll();
        tblUserModificationLog SelectByID(tblUserModificationLog obj);
        bool Insert(tblUserModificationLog obj);
        bool Update(tblUserModificationLog obj);
        bool Delete(tblUserModificationLog obj);
    }
}
