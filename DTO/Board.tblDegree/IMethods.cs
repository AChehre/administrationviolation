﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblDegree
{
    public interface IMethods
    {
        List<tblDegree> SelectAll();
        tblDegree SelectByID(tblDegree obj);
        bool Insert(tblDegree obj);
        bool Update(tblDegree obj);
        bool Delete(tblDegree obj);
    }
}
