﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblBoardMemberMapView
{
    [Serializable]
    public class tblBoardMemberMapView
    {
        public System.Int64? RowNumber {set;get;}
		public System.String FName {set;get;}
		public System.String LName {set;get;}
        /// <summary>
        /// گرفتن نام کامل
        /// </summary>
        public System.String FullName
        {
            get
            {
                return FName + " " + LName;
            }
        }
		public System.Int64? UserID {set;get;}
		public System.Int64? MapRecordID {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? MemberRole {set;get;}
		public System.DateTime? StartActivity {set;get;}
		public System.DateTime? EndActivity {set;get;}
		public System.Byte? Status {set;get;}

        /// <summary>
        /// توضیح وضعیت کاربر
        /// </summary>
        public System.String StatusDescription
        {
            get
            {
                return Status.Value == 1 ? "فعال" : "غیر فعال";
            }
        }

        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

