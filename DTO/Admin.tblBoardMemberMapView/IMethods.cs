﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblBoardMemberMapView
{
    public interface IMethods
    {
        bool Delete(tblBoardMemberMapView obj);
        bool Insert(tblBoardMemberMapView obj);
        List<tblBoardMemberMapView> SelectAll();
        List<tblBoardMemberMapView> SelectBoardMember(tblBoardMemberMapView obj);
        List<tblBoardMemberMapView> SelectBoardMemberByPaging(tblBoardMemberMapView obj);
        tblBoardMemberMapView SelectByID(tblBoardMemberMapView obj);
        tblBoardMemberMapView SelectByMemberRole(tblBoardMemberMapView obj);
        bool Update(tblBoardMemberMapView obj);
    }
}
