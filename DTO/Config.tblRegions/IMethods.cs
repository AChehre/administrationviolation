﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblRegions
{
    public interface IMethods
    {
        List<tblRegions> SelectAll(tblRegions obj);
        tblRegions SelectByID(tblRegions obj);
        bool Insert(tblRegions obj);
        bool Update(tblRegions obj);
        bool Delete(tblRegions obj);
    }
}
