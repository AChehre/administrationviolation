﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblRegions
{
    [Serializable]
    public class tblRegions
    {
        public System.Int64 RowNumber {set;get;}
        public System.String Name { set; get; }
        public System.Int32? RegionCode { set; get; }
        public System.Int32? OstanCode { set; get; }
        public System.String PersonnelDepartmentAddress { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// آیدی کاربری که میخواهیم منطقه های مربوط به آن را بدست بیاوریم
        /// </summary>
        public System.Int64? UserID { set; get; }
    }
}

