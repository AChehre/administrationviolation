﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserAreaAccessLevel
{
    [Serializable]
    public class tblUserAreaAccessLevel
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? UserID {set;get;}
		public System.Byte? UserType {set;get;}
		public System.String AllowedArea {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

