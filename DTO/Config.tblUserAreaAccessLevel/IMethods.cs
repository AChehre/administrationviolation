﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserAreaAccessLevel
{
    public interface IMethods
    {
        List<tblUserAreaAccessLevel> SelectAll();
        tblUserAreaAccessLevel SelectByID(tblUserAreaAccessLevel obj);
        bool Insert(tblUserAreaAccessLevel obj);
        bool Update(tblUserAreaAccessLevel obj);
        bool Delete(tblUserAreaAccessLevel obj);
    }
}
