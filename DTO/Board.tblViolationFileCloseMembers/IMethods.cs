﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFileCloseMembers
{
    public interface IMethods
    {
        bool Delete(tblViolationFileCloseMembers obj);
        bool DeleteByViolationFileID(tblViolationFileCloseMembers obj);
        bool Insert(tblViolationFileCloseMembers obj);
        bool InsertList(List<tblViolationFileCloseMembers> objList);
        List<tblViolationFileCloseMembers> SelectAll();
        tblViolationFileCloseMembers SelectByID(tblViolationFileCloseMembers obj);
        List<tblViolationFileCloseMembers> SelectByViolationFileID(tblViolationFileCloseMembers obj);
        bool Update(tblViolationFileCloseMembers obj);
    }
}
