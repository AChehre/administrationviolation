﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationFileCloseMembers
{
    [Serializable]
    public class tblViolationFileCloseMembers
    {
        public System.Int64 RowNumber {set;get;}		public System.Int64 ID {set;get;}		public System.Int64? ViolationFileID {set;get;}		public System.Int64? MemberID {set;get;}		public System.String FName {set;get;}		public System.String LName {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

