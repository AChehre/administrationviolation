﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblUserAttributesMapView
{
    [Serializable]
    public class tblUserAttributesMapView
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? UserID {set;get;}
		public System.String UserName {set;get;}
		public System.String FName {set;get;}
		public System.String LName {set;get;}

        /// <summary>
        /// گرفتن نام کامل
        /// </summary>
        public System.String FullName
        {
            get
            {
                return FName + " " + LName;
            }
        }

		public System.String EMail {set;get;}
		public System.String Password {set;get;}
		public System.DateTime? RegisterDate {set;get;}
		public System.Boolean? IsActive {set;get;}

        /// <summary>
        /// وضعیت کاربر
        /// </summary>
        public System.String ActiveStatus
        {
            get
            {
                return IsActive.Value ? "فعال" : "غیر فعال";
            }
        }

        public System.Int32? OstanCode { set; get; }
		public System.Int32? RegionCode {set;get;}
        public System.Int32? SchoolCode { set; get; }
        public System.Int64? PersonnelCode { set; get; }
        public System.String DefaultPageUrl { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// لیست گروه های کاربری ای که برای یک کاربر انتخاب شده است
        /// به صورت کاما از هم جدا شده اند
        /// </summary>
        public string SelectedUserGroup { set; get; }

        /// <summary>
        /// کاربری که لاگین کرده و میخواهد اطلاعات یک کاربر دیگر را مشاهده نماید
        /// فقط وقتی میخواهیم در مد ویرایش یک کاربر، اطلاعات کاربر را لود نماییم از این پروپرتی استفاده میکنیم
        /// </summary>
        public System.Int64? CurrentUserID { set; get; }

        /// <summary>
        /// آیدی گروه کاربری برای فیلتر
        /// </summary>
        public System.Int64? UserGroupID { set; get; }

        /// <summary>
        /// نام کامل جهت فیلتر کردن
        /// </summary>
        public System.String FullNameForFilter { set; get; }

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }

        public System.Boolean Online { set; get; }
        public System.DateTime SessionExpiration { set; get; }
    }
}

