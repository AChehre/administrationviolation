﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUserAttributesMapView
{
    public interface IMethods
    {
        List<tblUserAttributesMapView> SelectAll();
        tblUserAttributesMapView SelectByID(tblUserAttributesMapView obj);
        long Insert(tblUserAttributesMapView obj);
        bool Update(tblUserAttributesMapView obj);
        bool Delete(tblUserAttributesMapView obj);
    }
}
