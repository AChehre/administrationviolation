﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblMenu
{
    [Serializable]
    public class tblMenu
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? MenuID {set;get;}
		public System.Int32? ParentID {set;get;}
		public System.String Link {set;get;}
		public System.String Title {set;get;}
		public System.Boolean? IsPublished {set;get;}
		public System.Int32? ViewOrder {set;get;}
		public System.Byte? Type {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// لیست گروه های کاربری ای که برای دسترسی به منو انتخاب شده است
        /// به صورت کاما از هم جدا شده اند
        /// </summary>
        public string SelectedUserGroup { set; get; }

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }

        /// <summary>
        /// آیدی گروه کاربری برای فیلتر کردن
        /// </summary>
        public System.Int64? UserGroupID { set; get; }
    }
}

