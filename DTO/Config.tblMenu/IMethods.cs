﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblMenu
{
    public interface IMethods
    {
        List<tblMenu> SelectAll();
        tblMenu SelectByID(tblMenu obj);
        int Insert(tblMenu obj);
        bool Update(tblMenu obj);
        bool Delete(tblMenu obj);
    }
}
