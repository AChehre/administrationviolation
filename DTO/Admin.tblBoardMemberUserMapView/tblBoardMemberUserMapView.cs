﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblBoardMemberUserMapView
{
    [Serializable]
    public class tblBoardMemberUserMapView
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? MemberRole {set;get;}
		public System.DateTime? StartActivity {set;get;}
		public System.DateTime? EndActivity {set;get;}
		public System.Byte? Status {set;get;}
		public System.String FName {set;get;}
		public System.String LName {set;get;}
		public System.String OstanName {set;get;}
		public System.String RegionName {set;get;}
		public System.Int64? PersonnelCode {set;get;}
		public System.Int64? UserID {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------

        /// <summary>
        /// تاریخ شروع فیلتر
        /// </summary>
        public System.DateTime? FromDate { set; get; }

        /// <summary>
        /// تاریخ پایان فیلتر
        /// </summary>
        public System.DateTime? ToDate { set; get; }

        /// <summary>
        /// نام ستونی که میخواهیم براساس آن مرتب سازی انجام شود
        /// </summary>
        public System.String SortColumn { set; get; }

        /// <summary>
        /// نحوه مرتب سازی ستون، صعودی یا نزولی
        /// </summary>
        public System.String SortOrder { set; get; }
    }
}

