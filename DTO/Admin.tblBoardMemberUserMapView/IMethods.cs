﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblBoardMemberUserMapView
{
    public interface IMethods
    {
        List<tblBoardMemberUserMapView> SelectAll();
        tblBoardMemberUserMapView SelectByID(tblBoardMemberUserMapView obj);
        bool Insert(tblBoardMemberUserMapView obj);
        bool Update(tblBoardMemberUserMapView obj);
        bool Delete(tblBoardMemberUserMapView obj);
    }
}
