﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblPages
{
    public interface IMethods
    {
        List<tblPages> SelectAll();
        tblPages SelectByID(tblPages obj);
        int Insert(tblPages obj);
        bool Update(tblPages obj);
        bool Delete(tblPages obj);
    }
}
