using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblAmar
{
    public interface IMethods
    {
        List<tblAmar> SelectAll();
        int VisitCountBetweenTowDate(tblAmar obj);
        bool Insert(tblAmar obj);
        bool Update(tblAmar obj);
        bool Delete(tblAmar obj);
    }
}
