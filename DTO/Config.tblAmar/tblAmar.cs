﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblAmar
{
    [Serializable]
    public class tblAmar
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int32? ID {set;get;}
		public System.DateTime? RegDate {set;get;}
		public System.String UserIP {set;get;}

        // این دوتا برای بدست آوردن آمار سایت در بازه های زمانی ، روزانه ، هفتگی و غیره
        public System.DateTime? FromDate { set; get; }
        public System.DateTime? ToDate { set; get; }

        /// <summary>
        /// اگر آخرین تاریخ ثبت شده برای آی پی از این تاریخ کوچکتر یا مساوی بود، رکورد ثبت شود
        /// </summary>
        public System.DateTime? LastTime { set; get; }

        /// <summary>
        /// جهت برگرداندن تعداد بازدید ها استفاده میشود
        /// </summary>
        public System.Int32? VisitCount { set; get; }
        
    }
}

