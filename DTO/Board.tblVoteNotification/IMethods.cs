﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblVoteNotification
{
    public interface IMethods
    {
        List<tblVoteNotification> SelectAll();
        tblVoteNotification SelectByID(tblVoteNotification obj);
        long[] Insert(tblVoteNotification obj);
        bool Update(tblVoteNotification obj);
        bool Delete(tblVoteNotification obj);
        long SelectByRequestDataID(tblVoteNotification obj);
        bool CheckCounterConfilict(tblVoteNotification obj);
    }
}
