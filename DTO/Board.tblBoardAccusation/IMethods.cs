﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblBoardAccusation
{
    public interface IMethods
    {
        List<tblBoardAccusation> SelectAll();
        tblBoardAccusation SelectByID(tblBoardAccusation obj);
        bool Insert(tblBoardAccusation obj);
        bool Update(tblBoardAccusation obj);
        bool Delete(tblBoardAccusation obj);
        long SelectCountByAccusationSubjectID(tblBoardAccusation obj);
    }
}
