﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblBoardAccusation
{
    [Serializable]
    public class tblBoardAccusation
    {
        public System.Int64 RowNumber {set;get;}
		public System.Byte? Status {set;get;}
		public System.DateTime RegisterDate {set;get;}
		public System.Int64 ViolationFileID {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Int64 AccusationSubjectID {set;get;}
		public System.Byte BoardType {set;get;}
		public System.Int32? OstanCode {set;get;}
		public System.Int32? RegionCode {set;get;}
		public System.Int32? SchoolCode {set;get;}
		public System.Int32? Expr1 {set;get;}
		public System.DateTime? CreationDate {set;get;}
		public System.Byte? FileReferType {set;get;}
		public System.DateTime? FileCloseDate {set;get;}
		public System.Byte? VoteTypeCode {set;get;}
		public System.Int64? ReferHistoryCounter {set;get;}
		public System.Byte? NaghzeRay {set;get;}
        public System.Byte FinalResult { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.DateTime? FromDate { set; get; }
        public System.DateTime? ToDate { set; get; }
    }
}

