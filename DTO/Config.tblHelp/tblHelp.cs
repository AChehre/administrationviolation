﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Config.tblHelp
{
    [Serializable]
    public class tblHelp
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? ID {set;get;}
		public System.Int32? RequestID {set;get;}
		public System.String HelpContent {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

