﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblHelp
{
    public interface IMethods
    {
        List<tblHelp> SelectAll();
        tblHelp SelectByID(tblHelp obj);
        bool Insert(tblHelp obj);
        bool Update(tblHelp obj);
        bool Delete(tblHelp obj);
    }
}
