﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Admin.tblBoard
{
    [Serializable]
    public class tblBoard
    {
        public System.Int64? RowNumber {set;get;}
		public System.Int64? BoardID {set;get;}
		public System.Byte? BoardType {set;get;}
		public System.String BoardTitle {set;get;}
		public System.Int32? OstanCode {set;get;}
		public System.Int32? RegionCode {set;get;}
		public System.Int32? SchoolCode {set;get;}
        public System.Byte? Status { set; get; }
        public System.String BoardCode { set; get; }
        public System.Int64 FileCounter { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.String Address { set; get; }
        /// <summary>
        /// آیدی کاربری که میخواهیم هیات های موجود در مناطق قابل دسترس او را بدست بیاوریم
        /// </summary>
        public System.Int64? UserID { set; get; }
    }
}

