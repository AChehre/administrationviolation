﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Admin.tblBoard
{
    public interface IMethods
    {
        List<tblBoard> SelectAll();
        tblBoard SelectByID(tblBoard obj);
        long Insert(tblBoard obj);
        bool Update(tblBoard obj);
        bool Delete(tblBoard obj);
    }
}
