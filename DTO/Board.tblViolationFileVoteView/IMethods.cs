﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblViolationFileVoteView
{
    public interface IMethods
    {
        List<tblViolationFileVoteView> SelectAll();
        tblViolationFileVoteView SelectByID(tblViolationFileVoteView obj);
        bool Insert(tblViolationFileVoteView obj);
        bool Update(tblViolationFileVoteView obj);
        bool Delete(tblViolationFileVoteView obj);
        bool CheckVoteNumberExists(tblViolationFileVoteView obj);
    }
}
