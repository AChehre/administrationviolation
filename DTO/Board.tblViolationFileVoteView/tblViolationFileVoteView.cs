﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblViolationFileVoteView
{
    [Serializable]
    public class tblViolationFileVoteView
    {
        public System.Int64 RowNumber {set;get;}
		public System.Int64? VoteNumber {set;get;}
		public System.String FirstName {set;get;}
		public System.String LastName {set;get;}
		public System.Byte PenaltyType {set;get;}
		public System.Int64? BoardID {set;get;}
        public System.DateTime VoteDate { set; get; }
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
        public System.Int32 ActivityYear { set; get; }
    }
}

