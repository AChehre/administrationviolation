﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblPagesPermissionMapView
{
    public interface IMethods
    {
        List<tblPagesPermissionMapView> SelectAll();
        tblPagesPermissionMapView SelectByID(tblPagesPermissionMapView obj);
        bool Insert(tblPagesPermissionMapView obj);
        bool Update(tblPagesPermissionMapView obj);
        bool Delete(tblPagesPermissionMapView obj);
    }
}
