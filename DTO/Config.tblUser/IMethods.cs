﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Config.tblUser
{
    public interface IMethods
    {
        List<tblUser> SelectAll();
        tblUser SelectByID(tblUser obj);
        long Insert(tblUser obj);
        bool Update(tblUser obj);
        bool Delete(tblUser obj);
        bool RenewExpirationTime(tblUser obj);
        bool Logout(tblUser obj);
    }
}
