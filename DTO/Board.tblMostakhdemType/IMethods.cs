﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DTO.Board.tblMostakhdemType
{
    public interface IMethods
    {
        List<tblMostakhdemType> SelectAll();
        tblMostakhdemType SelectByID(tblMostakhdemType obj);
        bool Insert(tblMostakhdemType obj);
        bool Update(tblMostakhdemType obj);
        bool Delete(tblMostakhdemType obj);
    }
}
