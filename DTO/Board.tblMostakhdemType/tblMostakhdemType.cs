﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO.Board.tblMostakhdemType
{
    [Serializable]
    public class tblMostakhdemType
    {
        public System.Int64 RowNumber {set;get;}		public System.Byte MostkhdemTypeID {set;get;}		public System.String Title {set;get;}		
        //----------- برای صفحه بندی
        public System.Int32? PageNumber { set; get; }
        public System.Int32? PageSize { set; get; }
        public System.Int64? TotalRow { set; get; }
        //----------------------
    }
}

