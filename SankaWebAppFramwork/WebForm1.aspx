﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="WebForm1.aspx.cs" Inherits="SankaWebAppFramework.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id='sanka_basic_modal'>
        <a href="Default.aspx" data-title="تست" data-duplicatetitle="true" data-width="800"
            data-height="400" class='basic'>تست نمایش آیفریم در مودال</a>
    </div>
    <div id='modal_content'>
        <h3>
        </h3>
        <iframe id="sanka_iframe" src=""></iframe>
    </div>
</asp:Content>
