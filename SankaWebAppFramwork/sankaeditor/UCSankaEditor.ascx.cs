﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaEditor
{
    public partial class UCSankaEditor : System.Web.UI.UserControl
    {
        public string Text
        {
            get
            {
                return ckDescription.Text;
            }
            set
            {
                ckDescription.Text = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}