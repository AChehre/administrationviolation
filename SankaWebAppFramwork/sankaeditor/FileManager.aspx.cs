﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

namespace SankaEditor
{
    public partial class FileManager : System.Web.UI.Page
    {
        private string FileManagerAddress
        {
            get
            {
                return WebConfigurationManager.AppSettings.Get("FileManagerPath");
            }
        }

        /// <summary>
        /// مشخص میکند که آدرس فایل های انتخابی به کدام کنترل اضافه شود
        /// </summary>
        private string ReturnUrlTo
        {
            get
            {
                if (Request.QueryString.Get("ReturnUrlTo") != null)
                {
                    return Request.QueryString.Get("ReturnUrlTo");
                }
                return "";
            }
        }

        /// <summary>
        /// صفر یعنی ستون انتخاب نباشد،یک یعنی ستون انتخاب چک باکسی باشد، و دو یعنی ستون انتخاب
        /// دکمه های رادیویی باشد
        /// </summary>
        private byte SelectType
        {
            get
            {
                if (Request.QueryString.Get("SelectType") != null)
                {
                    return Request.QueryString.Get("SelectType").StringToByte();
                }

                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // این مقادیر را از کوئری استرینگ میگیریم، و بعد به در صفحه میریزیم تا در سمت جی کوئری ازش استفاده کنیم
            hfTargetBachUrl.Value = ReturnUrlTo;
            hfSelectType.Value = SelectType.ToString();
        }

        /// <summary>
        /// The FileInfo object.
        /// </summary>
        FileInfo fi;

        /// <summary>
        /// Gets the root path.
        /// </summary>
        /// <value>The _root.</value>
        string _root
        {
            get
            {
                // Is the root in the session?
                string root = UrlEncoding.Decode(Request.QueryString["r"]);

                // If it's not there use the default.
                if (string.IsNullOrEmpty(root))
                {
                    root = Server.MapPath(WebConfigurationManager.AppSettings.Get("WFRoot"));
                }
                return root;
            }
        }

        /// <summary>
        /// Gets the source or return path.<br/>
        /// Note: it is up to the user to ensure that the config value contains a valid url.
        /// </summary>
        /// <value>The _return.</value>
        string _return
        {
            get { return WebConfigurationManager.AppSettings.Get("WFReturn"); }
        }

        /// <summary>
        /// Get the mode.<br/>
        /// 0: View.<br/>
        /// 1: Rename.<br/>
        /// 2: New File.<br/>
        /// 3: New Folder.<br/>
        /// 4: Upload File.
        /// </summary>
        /// <value>The mode.</value>
        int _mode
        {
            get { return Convert.ToInt32(Request.QueryString["m"]); }
        }

        /// <summary>
        /// Gets the full path to the file or folder.
        /// </summary>
        /// <value>The _file.</value>
        string _file
        {
            get { return UrlEncoding.Decode(Request.QueryString["f"]); }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Setup();
        }

        /// <summary>
        /// Setups this instance.
        /// </summary>
        void Setup()
        {
            try
            {
                // Should the Return button be available?
                // If there is a config entry for WFReturn and it has a value then display the Return button.
                if (!string.IsNullOrEmpty(_return))
                {
                    btnReturn.Visible = true;
                }

                // Display the correct view.
                mvFiler.ActiveViewIndex = _mode;

                // Instantiate according to need.
                switch (_mode)
                {
                    case 0:	// View
                        SetupView();
                        break;
                    case 1:	// Rename
                        // Get the FileInfo object.
                        fi = new FileInfo(_file);

                        if (!IsPostBack)
                        {
                            lblRename.Text = "تغییر نام : " + fi.Name;

                            txtRename.Text = fi.Name;
                        }

                        txtRename.Focus();
                        break;
                    case 2:	//New File.
                        txtNewFile.Focus();
                        break;
                    case 3:	// New Folder.
                        txtNewFolder.Focus();
                        break;
                    case 4:	// Upload.
                        fuUpload.Focus();
                        break;
                }

                lblRoot.Text = _root;
            }
            catch
            {
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Cancel(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Handles the Click event of the btnHome control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnHome_Click(object sender, EventArgs e)
        {
            // Return here with no query string items.
            Response.Redirect(FileManagerAddress, true);
        }

        /// <summary>
        /// Handles the Click event of the btnNewFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnNewFile_Click(object sender, EventArgs e)
        {
            string newFile =
                string.Format(
                    CultureInfo.InvariantCulture,
                    FileManagerAddress + "?r={0}&f=&m=2",
                    UrlEncoding.Encode(_root));
            Response.Redirect(newFile, true);
        }

        /// <summary>
        /// Handles the Click event of the btnNewFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnNewFolder_Click(object sender, EventArgs e)
        {
            string newFolder =
                string.Format(
                    CultureInfo.InvariantCulture,
                    FileManagerAddress + "?r={0}&f=&m=3",
                    UrlEncoding.Encode(_root));
            Response.Redirect(newFolder, true);
        }

        /// <summary>
        /// Handles the Click event of the btnUpload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string upload =
                string.Format(
                    CultureInfo.InvariantCulture,
                    FileManagerAddress +"?r={0}&f=&m=4",
                    UrlEncoding.Encode(_root));
            Response.Redirect(upload, true);
        }

        /// <summary>
        /// Handles the Click event of the btnReturn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnReturn_Click(object sender, EventArgs e)
        {
            // If we reach here then we've already tested that we can do this in Setup.
            Response.Redirect(_return, true);
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRenameSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRename.Text))
                {
                    // Change the name to the new one.
                    string newName = txtRename.Text;
                    string newPath = fi.FullName.Replace(fi.Name, newName);

                    // Is this a file or a folder?
                    if (fi.Attributes == FileAttributes.Directory)
                    {
                        Directory.Move(_file, newPath);
                    }
                    else
                    {
                        File.Move(_file, newPath);
                    }

                    Refresh();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnNewFileSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNewFile.Text))
                {
                    string file = txtNewFile.Text;
                    string path =
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "{0}\\{1}",
                            _root,
                            file);

                    StreamWriter writer = File.CreateText(path);
                    writer.Close();

                    Refresh();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnNewFolderSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNewFolder.Text))
                {
                    string folder = txtNewFolder.Text;
                    string path =
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "{0}?r={1}",
                            _root,
                            folder);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    Refresh();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            try
            {
                // Leave if no file selected.
                if (!fuUpload.HasFile)
                {
                    fuUpload.Focus();
                    return;
                }

                // Create the path for the new file.
                string path =
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}\\{1}",
                        _root,
                        fuUpload.FileName);

                // Save to the current folder.
                fuUpload.SaveAs(path);

                Refresh();
            }
            catch
            {

            }
        }

        /// <summary>
        /// Set up the view.
        /// </summary>
        void SetupView()
        {
            try
            {

                // If the folder is bogus...
                if (!Directory.Exists(_root))
                {
                    Label label = new Label();
                    label.Text =
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "Unable to find '{0}'<br/>Please Refresh or alter the 'Root' value in 'Web.config'.",
                            _root);
                    phDisplay.Controls.Add(label);
                    return;
                }

                string QueryString = "&SelectType=" + SelectType + "&ReturnUrlTo=" + ReturnUrlTo;

                // Get a new table of files and folders.
                HtmlGenericControl table = new TableEx(_root).Create(SelectType, QueryString);

                // Display the table.
                if (table != null)
                {
                    phDisplay.Controls.Add(table);
                }
                else
                {
                    // Table wasn't created.
                    Label label = new Label();
                    label.Text = "Unable to create the table.";
                    phDisplay.Controls.Add(label);
                }

                // Finally, if a file link was clicked, open it.
                OpenFile();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Refreshes the page.
        /// </summary>
        void Refresh()
        {
            string refresh =
                string.Format(
                    CultureInfo.InvariantCulture,
                    FileManagerAddress+"?r={0}",
                    UrlEncoding.Encode(_root));
            Response.Redirect(refresh, true);
        }

        /// <summary>
        /// Display the given file.
        /// </summary>
        void OpenFile()
        {
            try
            {
                // Someone clicked on a file or folder...
                // Doesn't matter what's in x: if it has anything then open the file.
                if (!string.IsNullOrEmpty(Request.QueryString["x"]))
                {
                    // Get the object.
                    string file = Request.QueryString["o"];
                    FileInfo fi = new FileInfo(file);

                    // Display the file.
                    // 'application/octet-stream' should be okay for most cases.
                    string attachment =
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "attachment; filename={0}",
                            fi.Name);
                    Response.ContentType = "application/octet-stream";
                    Response.AppendHeader("Content-Disposition", attachment);
                    Response.TransmitFile(fi.FullName);
                    Response.End();

                    // Clear the querystring.
                    Request.QueryString.Remove("x");
                }
            }
            catch
            {

            }

        }
    }
}