﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileManager.aspx.cs" Inherits="SankaEditor.FileManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="CSS/FileManager.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="scripts/FileManager.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfTargetBachUrl" runat="server" />
    <asp:HiddenField ID="hfSelectType" runat="server" />
    <asp:Label ID="lbltest" runat="server" Text=""></asp:Label>
    <asp:ScriptManager ID="smPrimary" runat="server" />
    <asp:MultiView ID="mvFiler" runat="server">
        <%-- PRIMARY VIEW --%>
        <asp:View ID="vView" runat="server">
            <asp:UpdatePanel ID="upPrimary" runat="server">
                <ContentTemplate>
                    <div id="RightColumn">
                        <img src="" id="imgPreView" alt="پیش نمایش فایل" title="پیش نمایش فایل" />
                        <br />
                        <br />
                        <button type="button" class="modalCloseImg simplemodal-close" onclick="SelectFile()">
                            درج</button>
                    </div>
                    <div id="LeftColumn">
                        <div id="buttons">
                            <asp:Label ID="lblRoot" runat="server" CssClass="title" />
                            <asp:Button ID="btnReturn" runat="server" CausesValidation="false" Text="بازگشت"
                                OnClick="btnReturn_Click" Visible="false" />
                            <asp:Button ID="btnHome" runat="server" CausesValidation="false" Text="صفحه اصلی"
                                OnClick="btnHome_Click" Visible="false" />
                            <asp:Button ID="btnNewFile" runat="server" CausesValidation="false" Text="فایل جدید"
                                OnClick="btnNewFile_Click" />
                            <asp:Button ID="btnNewFolder" runat="server" CausesValidation="false" Text="پوشه جدید"
                                OnClick="btnNewFolder_Click" />
                            <asp:Button ID="btnUpload" runat="server" CausesValidation="false" Text="آپلود فایل"
                                OnClick="btnUpload_Click" />
                        </div>
                        <asp:Panel ID="phDisplay" runat="server">
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>
        <%-- RENAME --%>
        <asp:View ID="vRename" runat="server">
            <asp:Panel ID="pnlRename" runat="server" DefaultButton="btnRenameSave">
                <table border="0" cellpadding="5" cellspacing="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="header">
                                <asp:Label ID="lblRename" runat="server" />
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvRename" runat="server" ErrorMessage="لطفا نام جدید را وارد نمایید"
                                    ControlToValidate="txtRename" SetFocusOnError="True">*&nbsp;</asp:RequiredFieldValidator>نام
                                جدید را وارد نمایید:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRename" runat="server" Columns="35" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnRenameCancel" runat="server" CausesValidation="false" Text="انصراف"
                                    OnClick="Cancel" UseSubmitBehavior="false" />
                                <asp:Button ID="btnRenameSave" runat="server" Text="ذخیره" OnClick="btnRenameSave_Click" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </asp:Panel>
        </asp:View>
        <%-- NEW FILE --%>
        <asp:View ID="vNewFile" runat="server">
            <asp:Panel ID="pnlNewFile" runat="server" DefaultButton="btnNewFileSave">
                <table border="0" cellpadding="5" cellspacing="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="header">
                                فایل جدید
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvNewFile" runat="server" ErrorMessage="لطفا نام فایل را وارد نمایید"
                                    ControlToValidate="txtNewFile" SetFocusOnError="True">*&nbsp;</asp:RequiredFieldValidator>نام
                                فایل:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNewFile" runat="server" Columns="35" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnNewFileCancel" runat="server" CausesValidation="false" Text="انصراف"
                                    OnClick="Cancel" UseSubmitBehavior="false" />
                                <asp:Button ID="btnNewFileSave" runat="server" Text="ذخیره" OnClick="btnNewFileSave_Click" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </asp:Panel>
        </asp:View>
        <%-- NEW FOLDER --%>
        <asp:View ID="vFolder" runat="server">
            <asp:Panel ID="pnlNewFolder" runat="server" DefaultButton="btnNewFolderSave">
                <table border="0" cellpadding="5" cellspacing="0">
                    <thead>
                        <tr>
                            <td colspan="2" class="header">
                                پوشه جدید
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvNewFolder" runat="server" ErrorMessage="لطفا نام را وارد نمایید"
                                    ControlToValidate="txtNewFolder" SetFocusOnError="True">*&nbsp;</asp:RequiredFieldValidator>نام
                                پوشه جدید:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNewFolder" runat="server" Columns="35" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnNewFolderCancel" runat="server" CausesValidation="false" Text="انصراف"
                                    OnClick="Cancel" UseSubmitBehavior="false" />
                                <asp:Button ID="btnNewFolderSave" runat="server" Text="ذخیره" OnClick="btnNewFolderSave_Click" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </asp:Panel>
        </asp:View>
        <%-- UPLOAD A FILE --%>
        <asp:View ID="vUpload" runat="server">
            <asp:Panel ID="pnlUpload" runat="server" DefaultButton="btnUploadSave">
                <table border="0" cellpadding="5" cellspacing="0">
                    <thead>
                        <tr>
                            <td class="header">
                                آپلود فایل
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvUpload" runat="server" ErrorMessage="لطفا فایل را انتخاب نمایید"
                                    ControlToValidate="fuUpload" SetFocusOnError="true">*&nbsp;</asp:RequiredFieldValidator>
                                <asp:FileUpload ID="fuUpload" runat="server" Width="400px" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnUploadCancel" runat="server" CausesValidation="false" Text="انصراف"
                                    UseSubmitBehavior="false" OnClick="Cancel" />
                                <asp:Button ID="btnUploadSave" runat="server" Text="ارسال" OnClick="btnUploadSave_Click" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </asp:Panel>
        </asp:View>
    </asp:MultiView>
    <asp:ValidationSummary ID="valSummary" runat="server" />
    </form>
</body>
</html>
