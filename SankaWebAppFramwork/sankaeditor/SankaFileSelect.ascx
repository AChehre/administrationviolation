﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SankaFileSelect.ascx.cs"
    Inherits="SankaEditor.SankaFileSelect" %>
<div>
    <asp:TextBox ID="txtSelectedFileUrl" runat="server"></asp:TextBox>
    <span id="sanka_basic_modal"><a href="/sankaeditor/FileManager.aspx?ReturnUrlTo=txtSelectedFileUrl&SelectType=2"
        target="_blank" data-sankatooltip="انتخاب فایل" data-title="انتخاب فایل"
        data-duplicatetitle="false" data-width="900" data-height="600" class='basic'>آپلود فایل</a> </span>
</div>
