﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Security;
using System.Globalization;
using System.Text;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;

namespace SankaEditor
{
    public class Utility
    {
        /// <summary>
        /// نمایش پیغام جاوا اسکریپت
        /// </summary>
        /// <param name="message">متن ورودی جهت نمایش</param>
        /// <param name="page">شی که پیغام در آن نمایش داده میشود</param>
        internal static void ShowMessage(string message, Page page)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "key1", string.Format("alert('{0}')", message), true);
        }

        /// <summary>
        /// تبدیل تاریخ میلادی به شمسی به همراه ساعت
        /// </summary>
        /// <param name="obj">شی حاوی ساعت لاتین</param>
        /// <returns>رشته حاوی ساعت فارسی</returns>
        public static string GetCustomDate(object obj)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(obj);
                StringBuilder sb = new StringBuilder();
                System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();
                DayOfWeek Day = pcal.GetDayOfWeek(dt);
                switch (Day)
                {
                    case DayOfWeek.Saturday:
                        {
                            sb.Append(" شنبه ");
                            break;
                        }
                    case DayOfWeek.Sunday:
                        {
                            sb.Append(" یکشنبه ");
                            break;
                        }
                    case DayOfWeek.Monday:
                        {
                            sb.Append(" دوشنبه ");
                            break;
                        }
                    case DayOfWeek.Tuesday:
                        {
                            sb.Append(" سه شنبه ");
                            break;
                        }
                    case DayOfWeek.Wednesday:
                        {
                            sb.Append(" چهارشنبه ");
                            break;
                        }
                    case DayOfWeek.Thursday:
                        {
                            sb.Append(" پنجشنبه ");
                            break;
                        }
                    case DayOfWeek.Friday:
                        {
                            sb.Append(" جمعه ");
                            break;
                        }
                }
                int DayOfMonth = pcal.GetDayOfMonth(dt);
                sb.Append(" " + DayOfMonth + " ");

                switch (pcal.GetMonth(dt))
                {
                    case 1:
                        {
                            sb.Append("فروردین");
                            break;
                        }
                    case 2:
                        {
                            sb.Append("اردیبهشت");
                            break;
                        }
                    case 3:
                        {
                            sb.Append("خرداد");
                            break;
                        }
                    case 4:
                        {
                            sb.Append("تیر");
                            break;
                        }
                    case 5:
                        {
                            sb.Append("مرداد");
                            break;
                        }
                    case 6:
                        {
                            sb.Append("شهریور");
                            break;
                        }
                    case 7:
                        {
                            sb.Append("مهر");
                            break;
                        }
                    case 8:
                        {
                            sb.Append("آبان");
                            break;
                        }
                    case 9:
                        {
                            sb.Append("آذر");
                            break;
                        }
                    case 10:
                        {
                            sb.Append("دی");
                            break;
                        }
                    case 11:
                        {
                            sb.Append("بهمن");
                            break;
                        }
                    case 12:
                        {
                            sb.Append("اسفند");
                            break;
                        }
                }
                sb.Append(" " + pcal.GetYear(dt));
                return sb.ToString() + "  ساعت : " + GetTime(dt);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// تبدیل تاریخ میلادی به شمسی بدون ساعت
        /// </summary>
        /// <param name="obj">شی حاوی ساعت لاتین</param>
        /// <returns>رشته حاوی ساعت فارسی</returns>
        public static string GetCustomDateWithoutTime(object obj)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(obj);
                StringBuilder sb = new StringBuilder();
                System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();
                DayOfWeek Day = pcal.GetDayOfWeek(dt);
                switch (Day)
                {
                    case DayOfWeek.Saturday:
                        {
                            sb.Append(" شنبه ");
                            break;
                        }
                    case DayOfWeek.Sunday:
                        {
                            sb.Append(" یکشنبه ");
                            break;
                        }
                    case DayOfWeek.Monday:
                        {
                            sb.Append(" دوشنبه ");
                            break;
                        }
                    case DayOfWeek.Tuesday:
                        {
                            sb.Append(" سه شنبه ");
                            break;
                        }
                    case DayOfWeek.Wednesday:
                        {
                            sb.Append(" چهارشنبه ");
                            break;
                        }
                    case DayOfWeek.Thursday:
                        {
                            sb.Append(" پنجشنبه ");
                            break;
                        }
                    case DayOfWeek.Friday:
                        {
                            sb.Append(" جمعه ");
                            break;
                        }
                }
                int DayOfMonth = pcal.GetDayOfMonth(dt);
                sb.Append(" " + DayOfMonth + " ");

                switch (pcal.GetMonth(dt))
                {
                    case 1:
                        {
                            sb.Append("فروردین");
                            break;
                        }
                    case 2:
                        {
                            sb.Append("اردیبهشت");
                            break;
                        }
                    case 3:
                        {
                            sb.Append("خرداد");
                            break;
                        }
                    case 4:
                        {
                            sb.Append("تیر");
                            break;
                        }
                    case 5:
                        {
                            sb.Append("مرداد");
                            break;
                        }
                    case 6:
                        {
                            sb.Append("شهریور");
                            break;
                        }
                    case 7:
                        {
                            sb.Append("مهر");
                            break;
                        }
                    case 8:
                        {
                            sb.Append("آبان");
                            break;
                        }
                    case 9:
                        {
                            sb.Append("آذر");
                            break;
                        }
                    case 10:
                        {
                            sb.Append("دی");
                            break;
                        }
                    case 11:
                        {
                            sb.Append("بهمن");
                            break;
                        }
                    case 12:
                        {
                            sb.Append("اسفند");
                            break;
                        }
                }
                sb.Append(" " + pcal.GetYear(dt));
                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// بازگرداندن ساعت
        /// </summary>
        /// <param name="dt">شی ساعت لاتین</param>
        /// <returns>رشته حاوی ساعت</returns>
        public static string GetTime(DateTime dt)
        {
            string Z = "";
            DateTime M = dt;
            if (M.Hour < 10) Z += "0";
            Z += M.Hour;
            Z += ":";
            if (M.Minute < 10) Z += "0";
            Z += M.Minute;
            Z += ":";
            if (M.Second < 10) Z += "0";
            Z += M.Second;
            return Z;
        }

        /// <summary>
        /// تبدیل تاریخ شمسی به میلادی
        /// </summary>
        /// <param name="year">سال شمسی</param>
        /// <param name="month">ماه شمسی</param>
        /// <param name="day">زوز شمسی</param>
        /// <param name="hour">ساعت شمسی</param>
        /// <param name="minute">دقیق شمسی</param>
        /// <param name="second">ثانیه شمسی</param>
        /// <returns>تاریخ لاتین</returns>
        public static DateTime getLatinDate(int year, int month, int day, int hour, int minute, int second)
        {
            PersianCalendar pc = new PersianCalendar();
            return pc.ToDateTime(year, month, day, hour, minute, second, 0);
        }

        /// <summary>
        /// تولید کد فعال سازی 16 حرفی
        /// </summary>
        /// <param name="SerialNumber">سریال نرم افزار</param>
        /// <param name="SystemCode">شناسه سخت افزاری سیستم</param>
        /// <returns>رشته حاوی 16 حرف</returns>
        internal static string GenerateActivationCode(string SerialNumber, string SystemCode)
        {
            string part1 = SerialNumber.getMD5();
            string part2 = SystemCode.getMD5();
            string result = (part1 + part2).getMD5().Substring(0, 16);
            StringBuilder sb = new StringBuilder(result);
            for (int i = 4; i < 16; i += 5)
            {
                sb.Insert(i, "-");
            }
            return sb.ToString();
        }

        #region majid

        /// <summary>
        /// ساخت یک عدد هشت رقمی تصادفی
        /// </summary>
        /// <returns></returns>
        public static string Create8DigitString()
        {
            Random RNG = new Random();
            var builder = new StringBuilder();
            while (builder.Length < 8)
            {
                builder.Append(RNG.Next(10).ToString());
            }
            return builder.ToString();
        }

        /// <summary>
        /// ساخت لینک های مربوط به شماره صفحات یک عملیات صفحه بندی
        /// </summary>
        /// <param name="TotalRow">تعداد کل سطرها</param>
        /// <param name="PageSize">تعداد سطر هر صفحه</param>
        /// <param name="PageIndex">شماره صفحه جاری</param>
        /// <param name="MainUrl">آدرس صفحه ای که باید هر لینک به آن صفحه هدایت شود -- به عبارتی صفحه مقصد</param>
        /// <param name="PageLinkNo">تعداد دکمه هایی که میخواهیم به عنوان لینک صفحات نمایش دهیم، همیشه زوج باشد</param>
        /// <returns></returns>
        public static string CreatePageNumberLink(int TotalRow, int PageSize, int PageIndex, string MainUrl, int PageLinkNo)
        {
            StringBuilder PagingItem = new StringBuilder();

            int NumberofPage = (TotalRow / PageSize);

            if ((TotalRow % PageSize) > 0)
            {// اگر تقسیم تعداد کل سطرها به تعداد سطر هر صفحه، باقیمانده داشت، یکی به تعداد صفحه اضافه بشود
                NumberofPage++;
            }

            if (NumberofPage > 1)
            {
                PagingItem.Append("<div id='PageNoContainer'>");

                string Url = "#";
                if (PageIndex > 1)
                {// اگر شماره صفحه بزرگتر از یک بود، لینک صفحه نخست و صفحه قبلی اضافه شود
                    #region دکمه صفحه نخست

                    Url = MainUrl + "Page=1";
                    PagingItem.Append("<a title='1' class='PageNo' href='" + Url + "'>");
                    PagingItem.Append("صفحه نخست");
                    PagingItem.Append("</a>");

                    #endregion دکمه صفحه نخست

                    #region دکمه صفحه قبلی

                    Url = MainUrl + "Page=" + (PageIndex - 1).ToString();
                    PagingItem.Append("<a title='" + (PageIndex - 1).ToString() + "' class='PageNo' href='" + Url + "'>");
                    PagingItem.Append("قبلی");
                    PagingItem.Append("</a>");

                    #endregion دکمه صفحه قبلی
                }

                Url = "#";
                string SelectedClass = "SelectedPageNo";
                // بدست آوردن شماره صفحه ابتدایی برای نمایش به کاربر
                int From = (PageIndex - (PageLinkNo / 2)) > 0 ? (PageIndex - (PageLinkNo / 2)) : 1;
                // بدست آوردن شماره صفحه انتهایی برای نمایش به کاربر
                int To = (From + PageLinkNo - 1) <= NumberofPage ? (From + PageLinkNo - 1) : NumberofPage;

                if (From > 1)
                {// اگر اولین دکمه لینک صفحات بزرگتر از صفر باشد، ی سه نقطه اول بگذاریم
                    PagingItem.Append("<span> ... </span>");
                }

                for (int i = From; i <= To; i++)
                {
                    Url = MainUrl + "Page=" + i.ToString();
                    SelectedClass = "";
                    if (PageIndex == i)
                    {
                        SelectedClass = "SelectedPageNo";
                        Url = "#";
                    }
                    PagingItem.Append("<a title='" + i.ToString() + "' class='PageNo " + SelectedClass + "' href='" + Url + "'>");
                    PagingItem.Append(i.ToString());
                    PagingItem.Append("</a>");
                }

                if (To < NumberofPage)
                {// اگر آخرین دکمه لینک صفحات کوچکتر از تعداد صفحات باشد، ی سه نقطه آخرش بگذاریم
                    PagingItem.Append("<span> ... </span>");
                }

                if (PageIndex < NumberofPage)
                {// اگر شماره صفحه کوچکتر از تعداد کل صفحات بود، لینک صفحه پایانی و صفحه بعدی اضافه شود

                    #region دکمه صفحه بعدی

                    Url = MainUrl + "Page=" + (PageIndex + 1).ToString();
                    PagingItem.Append("<a title='" + (PageIndex + 1).ToString() + "' class='PageNo' href='" + Url + "'>");
                    PagingItem.Append("بعدی");
                    PagingItem.Append("</a>");

                    #endregion دکمه صفحه بعدی

                    #region دکمه صفحه پایانی

                    Url = MainUrl + "Page=" + NumberofPage;
                    PagingItem.Append("<a title='" + NumberofPage + "' class='PageNo' href='" + Url + "'>");
                    PagingItem.Append("صفحه پایانی");
                    PagingItem.Append("</a>");

                    #endregion دکمه صفحه پایانی
                }

                PagingItem.Append("</div>");
            }

            return PagingItem.ToString();
        }

        /// <summary>
        /// نوع تارگت یک لینک را برمی گرداند
        /// </summary>
        public static String[] GetLinkTarget =
        {
            "_parent",
            "_blank",
        };

        #endregion
    }
}