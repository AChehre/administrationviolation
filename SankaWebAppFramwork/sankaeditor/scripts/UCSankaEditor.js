﻿
$(document).ready(function () {
    $("[id$='ckDescription']").ckeditor();
});

function AddUrlToEditor(Url, TargetId) {
    var target = "[id$='" + TargetId + "']";
    CKEDITOR.instances[$(target).attr("id")].insertHtml(Url);
    window.parent.jQuery('#modal_content').dialog('close');
}

function CloseModalWindow() {
    $.modal.close();
}