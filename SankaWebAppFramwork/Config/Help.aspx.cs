﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class Help : GeneralPage
    {
        /// <summary>
        /// شناسه راهنما
        /// </summary>
        private long HelpID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("ID")))
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // مقدار دهی نوع هئیت ها
                AdministrationViolation.Admin.Common.BindBoardType(ddlBoardType);
            }

            BindActionMenu();
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveClick(object sender, EventArgs e)
        {
            if (!ValidateData())
            {
                return;
            }

            DTO.Config.tblHelp.tblHelp Obj = new DTO.Config.tblHelp.tblHelp();

            Obj.RequestID = ddlHelpType.SelectedValue.StringToInt();
            Obj.HelpContent = ckDescription.Text;

            bool SaveStatus = false;
            try
            {
                SaveStatus = (new DAL.Config.tblHelp.Methods()).Save(Obj);
            }
            catch (Exception ex)
            {
                //LogError(ex);
            }

            if (SaveStatus)
            {
                SankaDialog1.ShowMessage("اطلاعات با موفقیت ذخیره شد.", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                SankaDialog1.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
        }

        private bool ValidateData()
        {
            if (ddlBoardType.SelectedIndex == 0)
            {
                SankaDialog1.ShowMessage("هیات  مورد نظر را انتخاب نمایید",DialogMessage.SankaDialog.Message_Type.Warning,Page);
                return false;
            }
            if (ddlHelpType.SelectedIndex == 0)
            {
                SankaDialog1.ShowMessage("موضوع راهنما را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return false;
            }
            return true;
        }

        /// <summary>
        /// ساخت منو
        /// </summary>
        void BindActionMenu()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            //  دکمه ذخیره
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnSave",
                Title = "ذخیره",
                button_click = btnSaveClick,
                Type = ActionMenuType.ImageButton,
                Icon = "/images/icon/save.png"
            });

            ActionMenu.DataSource = lstAction;
            ActionMenu.DataBind();
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک راهنما
        /// </summary>
        void LoadData()
        {
            ckDescription.Text = "";

            DTO.Config.tblHelp.tblHelp Obj = new DTO.Config.tblHelp.tblHelp();

            Obj.RequestID = ddlHelpType.SelectedValue.StringToInt();

            if (Obj.RequestID == 0)
            {
                return;
            }

            Obj = (new DAL.Config.tblHelp.Methods()).SelectByType(Obj);
            if (Obj.RowNumber > 0)
            {
                ddlHelpType.SelectedValue = Obj.RequestID.ToString();
                ckDescription.Text = Obj.HelpContent;
            }
        }

        protected void ddlHelpType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void ddlBoardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Common.BindHelpTypes(ddlHelpType, ddlBoardType.SelectedValue.StringToByte());
        }

        protected void ddlHelpType_DataBound(object sender, EventArgs e)
        {
            ddlHelpType_SelectedIndexChanged(sender, e);
        }

        protected void ddlBoardType_DataBound(object sender, EventArgs e)
        {
            ddlBoardType_SelectedIndexChanged(sender, e);
        }
    }
}