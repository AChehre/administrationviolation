﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class _GroupUser : GeneralPage
    {
        /// <summary>
        /// آیدی کاربر جهت ویرایش اطلاعات
        /// </summary>
        private long UID
        {
            get
            {
                if (Request.QueryString.Get("UID") != null)
                {
                    return Request.QueryString.Get("UID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی گروه
        /// </summary>
        private int GID
        {
            get
            {
                if (Request.QueryString.Get("GID") != null)
                {
                    return Request.QueryString.Get("GID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();
                #region نمایش گروه های کاربری به صورت درختی

                try
                {
                    CreateGroupTree();
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }

                #endregion نمایش گروه های کاربری به صورت درختی

                #region نمایش اطلاعات گروه انتخاب شده

                if (GID > 0)
                {
                    DTO.Config.tblUserGroup.tblUserGroup ObjUserGroup = new DTO.Config.tblUserGroup.tblUserGroup();
                    ObjUserGroup.GroupID = GID;
                    ObjUserGroup = (new DAL.Config.tblUserGroup.Methods()).SelectByID(ObjUserGroup);

                    if (ObjUserGroup.RowNumber > 0)
                    {// اگر اطلاعات کاربر وجود داشت
                        string Action = UID > 0 ? "ویرایش " : "ثبت ";
                        PageLabelTitle = Action + "کاربر در گروه [ " + ObjUserGroup.GroupName + " ]";

                        ViewState["PageTitle"] = Action + "کاربر در گروه « " + ObjUserGroup.GroupName + " »";
                    }

                    #region نمایش گروه جاری به عنوان گروه عضو برای کاربر

                    if (UID < 1)
                    {// فقط در حالت ثبت، گروه جاری را به صورت پیش فرض انتخاب کن
                        List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lstUserGroup = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>
                        {
                            new DTO.Config.tblUserGroupMap.tblUserGroupMap {
                                UserGroupID = GID,
                            }
                        };
                        foreach (TreeNode node in tvGroup.Nodes)
                        {
                            CheckedNodes(lstUserGroup, node);
                        }
                    }

                    #endregion نمایش گروه جاری به عنوان گروه عضو برای کاربر
                }

                #endregion نمایش اطلاعات گروه انتخاب شده

                lnkUserAreaAccessLevel.Visible = false;

                if (UID > 0)
                {
                    #region نمایش اطلاعات هنگام ویرایش
                    // اگر حالت ویرایش باشد، کلمه عبور و تکرار آن را نمایش نمیدهیم
                    PassDiv.Visible = false;
                    LoadData();
                    //  اگر حالت ویرایش باشد، لینک انتخاب مناطق دسترسی را نمایش میدهیم
                    lnkUserAreaAccessLevel.Visible = true;
                    #endregion نمایش اطلاعات هنگام ویرایش
                }
            }

            this.Title = ViewState["PageTitle"].ToString();
        }

        /// <summary>
        /// نمایش اطلاعات کاربر روی فرم
        /// </summary>
        protected void LoadData()
        {
            try
            {
                DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();
                ObjUser.UserID = UID;
                ObjUser = (new DAL.Config.tblUser.Methods()).SelectByID(ObjUser);

                if (ObjUser.RowNumber > 0)
                {// اگر اطلاعات کاربر وجود داشت
                    txtEMail.Text = ObjUser.EMail;
                    txtFName.Text = ObjUser.FName;
                    txtLName.Text = ObjUser.LName;
                    txtUserName.Text = ObjUser.UserName;
                    rbtnIsActive.SelectedValue = "0";
                    if (ObjUser.IsActive.Value)
                    {
                        rbtnIsActive.SelectedValue = "1";
                    }

                    // انتخاب گروه هایی که کاربر در آنها عضو است
                    DTO.Config.tblUserGroupMap.tblUserGroupMap obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                    obj.UserID = ObjUser.UserID;
                    List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lstUserGroup = (new DAL.Config.tblUserGroupMap.Methods()).SelectByUserID(obj);
                    foreach (TreeNode node in tvGroup.Nodes)
                    {
                        CheckedNodes(lstUserGroup, node);
                    }

                    // نمایش اطلاعات اضافی کاربر ، مانند استان ، منطقه و غیره
                    DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr = new DTO.Config.tblUserAttributes.tblUserAttributes();
                    ObjUserAttr.UserID = ObjUser.UserID;
                    ObjUserAttr = (new DAL.Config.tblUserAttributes.Methods()).SelectByUserID(ObjUserAttr);

                    States1.OstanCode = 0;
                    //States1.RegionCode = 0;
                    //States1.SchoolCode = 0;

                    if (ObjUserAttr.OstanCode.HasValue)
                    {
                        States1.OstanCode = ObjUserAttr.OstanCode.Value;
                    }

                    //if (ObjUserAttr.RegionCode.HasValue)
                    //{
                    //    States1.RegionCode = ObjUserAttr.RegionCode.Value;
                    //}

                    //if (ObjUserAttr.SchoolCode.HasValue)
                    //{
                    //    States1.SchoolCode = ObjUserAttr.SchoolCode.Value;
                    //}

                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.HRef = "UserAreaAccessLevel.aspx?SchoolCode=" + ObjUserAttr.SchoolCode + "&regioncode=" + ObjUserAttr.RegionCode + "&OstanCode=" + ObjUserAttr.OstanCode + "&uid=" + ObjUserAttr.UserID;
                }
                else
                {
                    MessageDialog.ShowMessage("اطلاعاتی برای کاربری با آیدی " + UID + " یافت نشد.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?UID=" + NewUserID + "&gid=" + GID);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?gid=" + GID);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "GroupUsersList.aspx?gid=" + GID);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ثبت یا ویرایش اطلاعات
        /// </summary>
        /// <returns>آیدی کاربر ذخیره شده</returns>
        protected long SaveData()
        {
            long Result = 0;

            try
            {
                #region گرفتن اطلاعات فرم

                DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();

                ObjUser.EMail = txtEMail.Text;
                ObjUser.FName = txtFName.Text;
                ObjUser.IsActive = rbtnIsActive.SelectedValue.StringToByte() == 0 ? false : true;
                ObjUser.LName = txtLName.Text;
                ObjUser.Password = txtPass.Text.getMD5();
                ObjUser.RegisterDate = DateTime.Now;
                ObjUser.UserName = txtUserName.Text;

                DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr = new DTO.Config.tblUserAttributes.tblUserAttributes();

                ObjUserAttr.OstanCode = States1.OstanCode;

                if (ObjUserAttr.OstanCode < 1)
                {
                    ObjUserAttr.OstanCode = null;
                }

                //ObjUserAttr.RegionCode = States1.RegionCode;

                //if (ObjUserAttr.RegionCode < 1)
                //{
                //    ObjUserAttr.RegionCode = null;
                //}

                //ObjUserAttr.SchoolCode = States1.SchoolCode;

                //if (ObjUserAttr.SchoolCode < 1)
                //{
                //    ObjUserAttr.SchoolCode = null;
                //}

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(ObjUser, ObjUserAttr, txtPass.Text.Trim(), txtRePass.Text.Trim(), out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return 0;
                }

                if (UID > 0)
                {
                    #region ویرایش اطلاعات

                    ObjUser.UserID = UID;

                    using (UserManagment UManageObj = new UserManagment())
                    {// ویرایش
                        Result = UManageObj.EditUser(ObjUser, ObjUserAttr);
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    ObjUser.UserID = 0;

                    // لیست گروه های کاربری ای که کاربر در آنها عضو شده است
                    List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();

                    foreach (TreeNode node in tvGroup.Nodes)
                    {
                        GetAllCheckedNodes(_lstSelectedgroup, 0, node);
                    }

                    if (!_lstSelectedgroup.Any())
                    {
                        MessageDialog.ShowMessage("حداقل یک گروه کاربری را برای عضویت کاربر انتخاب نمایید.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                        return 0;
                    }

                    using (UserManagment UManageObj = new UserManagment())
                    {// ثبت
                        Result = UManageObj.AddUser(ObjUser, ObjUserAttr, _lstSelectedgroup);
                    }

                    #endregion ثبت اطلاعات
                }

                if (Result > 0)
                {
                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.HRef = "UserAreaAccessLevel.aspx?SchoolCode=" + ObjUserAttr.SchoolCode + "&regioncode=" + ObjUserAttr.RegionCode + "&OstanCode=" + ObjUserAttr.OstanCode + "&uid=" + ObjUserAttr.UserID;
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="ObjUser">آبجکت از نوع کلاس یوزر</param>
        /// <param name="ObjUserAttr">آبجکت از نوع کلاس ویژگی های یوزر</param>
        /// <param name="Pass">کلمه عبور</param>
        /// <param name="RePass">تکرار کلمه عبور</param>
        /// <param name="Message">متن خطایی که برمیگرداند</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, string Pass, string RePass, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(ObjUser.FName))
            {
                Result = false;
                Message += "لطفا نام را وارد نمایید";
            }

            if (string.IsNullOrEmpty(ObjUser.LName))
            {
                Result = false;
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا نام خانوادگی را وارد نمایید";
            }
            if (!string.IsNullOrEmpty(ObjUser.EMail) && !ObjUser.EMail.IsEmailAdress())
            {// در صورتی که آدرس ایمیل وارد کرده باشد، اعتبارسنجی میشود، اگر ناردست باشد، خطا میدهد
                Result = false;
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا آدرس ایمیل را به صورت صحیح وارد نمایید";
            }
            if (string.IsNullOrEmpty(ObjUser.UserName))
            {
                Result = false;
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا نام کاربری را وارد نمایید";
            }
            else
            {// اگر نام کاربری وارد شده باشد، یکتا بودن آنرا در سیستم بررسی میکنیم

                bool CheckUserName = false;

                if (UID > 0)
                {// اگر در حالت ویرایش باشیم
                    CheckUserName = Common.CheckUserName(ObjUser.UserName, UID);
                }
                else
                {// اگر ثبت جدید باشد
                    CheckUserName = Common.CheckUserName(ObjUser.UserName);
                }

                if (!CheckUserName)
                {
                    Result = false;
                    if (!string.IsNullOrEmpty(Message))
                    {
                        Message += "<br/>";
                    }
                    Message += "نام کاربری تکراری می باشد";
                }
            }
            if (UID < 1)
            {// فقط زمانی که در حال ثبت کاربر هستیم کلمه عبور را چک میکنیم، در حالت ویرایش چک نمی کنیم
                if (string.IsNullOrEmpty(Pass))
                {
                    Result = false;
                    if (!string.IsNullOrEmpty(Message))
                    {
                        Message += "<br/>";
                    }
                    Message += "لطفا کلمه عبور را وارد نمایید";
                }
                else
                {
                    if (Pass != RePass)
                    {
                        Result = false;
                        if (!string.IsNullOrEmpty(Message))
                        {
                            Message += "<br/>";
                        }
                        Message += "<br/>" + "کلمه عبور تطابق ندارد";
                    }
                }
            }

            // بررسی اطلاعات اضافی کاربر، مانند استان، منطقه،مدرسه
            //if (ObjUserAttr.StateID < 1)
            //{
            //    Result = false;
            //    if (!string.IsNullOrEmpty(Message))
            //    {
            //        Message += "<br/>";
            //    }
            //    Message += "لطفا استان را انتخاب نمایید";
            //}
            //if (ObjUserAttr.RegionCode < 1)
            //{
            //    Result = false;
            //    if (!string.IsNullOrEmpty(Message))
            //    {
            //        Message += "<br/>";
            //    }
            //    Message += "لطفا منطقه را انتخاب نمایید";
            //}
            //if (ObjUserAttr.SchoolID < 1)
            //{
            //    Result = false;
            //    if (!string.IsNullOrEmpty(Message))
            //    {
            //        Message += "<br/>";
            //    }
            //    Message += "لطفا مدرسه را انتخاب نمایید";
            //}

            return Result;
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر منفی یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, long _UserID, TreeNode Node)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Config.tblUserGroupMap.tblUserGroupMap Obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                Obj.UserGroupID = Node.Value.StringToInt();
                Obj.UserID = _UserID;
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, _UserID, thisNode);
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/GroupUsersList.aspx?gid=" + GID
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}