﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class UserAreaAccessLevel : System.Web.UI.Page
    {
        /// <summary>
        /// آیدی مدرسه
        /// </summary>
        private int SchoolCode
        {
            get
            {
                if (Request.QueryString.Get("schoolcode") != null)
                {
                    return Request.QueryString.Get("schoolcode").StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی منطقه
        /// </summary>
        private int RegionCode
        {
            get
            {
                if (Request.QueryString.Get("regioncode") != null)
                {
                    return Request.QueryString.Get("regioncode").StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی استان
        /// </summary>
        private int OstanCode
        {
            get
            {
                if (Request.QueryString.Get("ostancode") != null)
                {
                    return Request.QueryString.Get("ostancode").StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی کاربر
        /// </summary>
        private long UserID
        {
            get
            {
                if (Request.QueryString.Get("uid") != null)
                {
                    return Request.QueryString.Get("uid").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserArea1.SchoolCode = SchoolCode;
                UserArea1.RegionCode = RegionCode;
                UserArea1.OstanCode = OstanCode;
                UserArea1.UserID = UserID;
            }
        }
    }
}