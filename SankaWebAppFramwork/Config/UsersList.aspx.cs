﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class UsersList : GeneralPage
    {
        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        /// <summary>
        /// آیا دکمه فیلتر کلیک شده است یا نه
        /// </summary>
        private bool ISbtnFilterClicked
        {
            get
            {
                return hfbtnFilterClicked.Value == "1" ? true : false;
            }
            set
            {
                hfbtnFilterClicked.Value = "0";

                if (value)
                {
                    hfbtnFilterClicked.Value = "1";
                }
            }
        }

        /// <summary>
        /// آیدی کاربر جهت حذف اطلاعات
        /// </summary>
        private long UID
        {
            get
            {
                if (Request.QueryString.Get("UID") != null)
                {
                    return Request.QueryString.Get("UID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            States1.VisibleRegion = false;
            States1.VisibleSchool = false;

            #region مد حذف یک رکورد

            if (UID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                // اگر اکشن کد در کوئری استرینگ نبود، یا آیدی برابر با آیدی کاربر جاری بود، پیغام دهد که 
                // مجوز ندارد ، چون یک کاربر نمیتواند خودش را ویرایش یا حذف نماید
                if (!CheckActionCode(Enums.Page_ActionCode.Delete) || UID == UserID)
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UsersList.aspx?action=" + (byte)Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                DeleteUser(UID);
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();
                Common.BindUserGroup(ddlUserGroups);
                Common.BindUserActive(ddlStatus);

                #region ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                if (UserOstanCode.HasValue)
                {
                    States1.OstanCode = UserOstanCode.Value;
                    States1.CurrentUserOstanCode = UserOstanCode.Value;
                }
                //if (UserRegionCode.HasValue)
                //{
                //    States1.RegionCode = UserRegionCode.Value;
                //    States1.CurrentUserRegionCode = UserRegionCode.Value;
                //}
                //if (UserSchoolCode.HasValue)
                //{
                //    States1.SchoolCode = UserSchoolCode.Value;
                //    States1.CurrentUserSchoolCode = UserSchoolCode.Value;
                //}

                States1.UserID = UserID;

                #endregion ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                // برابر فالس میکنیم که در لودهای بعدی صفحه، دوباره ترو برنگرداند
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvUsers.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
            }
        }

        /// <summary>
        /// مقداردهی گرید
        /// <param name="Filter">اگر ترو باشد، یعنی دکمه فیلتر کلیک شده و باید مقادیر رو از کمبوباکس ها بگیریم</param>
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj = new DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView();

                #region فیلتر

                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.UserID = UserID;
                obj.SortColumn = SortColumn;
                obj.SortOrder = SortOrder;

                if (ISbtnFilterClicked)
                {//اگر ترو باشد، یعنی دکمه فیلتر کلیک شده و باید مقادیر رو از کمبوباکس ها بگیریم
                    obj.OstanCode = States1.OstanCode;
                    //obj.RegionCode = States1.RegionCode;
                    //obj.SchoolCode = States1.SchoolCode;
                }
                else
                {// در غیر اینصورت مقادیر را از کداستان ، منطقه و مدرسه کاربر جاری
                    obj.OstanCode = UserOstanCode;
                    obj.RegionCode = UserRegionCode;
                    obj.SchoolCode = UserSchoolCode;
                }

                if (obj.OstanCode < 1)
                {
                    obj.OstanCode = null;
                }
                if (obj.RegionCode < 1)
                {
                    obj.RegionCode = null;
                }
                if (obj.SchoolCode < 1)
                {
                    obj.SchoolCode = null;
                }

                obj.FullNameForFilter = txtFullName.Text.Trim().CorrectArabicChars();
                if (ddlStatus.SelectedValue.StringToInt() >= 0)
                {// اگر غیر از حالت انتخاب کنید بود، مقدار دهی شود
                    obj.IsActive = ddlStatus.SelectedValue.StringToByte() == (byte)Enums.User_Active.Active ? true : false;
                }
                obj.PersonnelCode = txtPersonnelCode.Text.StringToLong();
                if (obj.PersonnelCode < 1)
                {
                    obj.PersonnelCode = null;
                }
                obj.UserGroupID = ddlUserGroups.SelectedValue.StringToLong();
                if (obj.UserGroupID < 1)
                {
                    obj.UserGroupID = null;
                }

                #endregion فیلتر

                List<DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView> RList = (new DAL.Config.tblUserAttributesMapView.Methods()).SelectAllByPaging(obj);
                gvUsers.CustomDataBind(RList, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;

                SetSortStatus();
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// حذف یک سطر
        /// </summary>
        protected void DeleteUser(long UserID)
        {
            bool DeleteStat = false;

            try
            {
                using (UserManagment UManageObj = new UserManagment())
                {
                    DeleteStat = UManageObj.DeleteUser(UserID);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (DeleteStat)
            {
                //  لاگ حذف کاربر
                (new Logger()).InsertUserModificationLog(UserID, Logger.RecardModificationFlag.DELETE);
                MessageDialog.ShowMessage("کاربر مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.ImageButton,
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ثبت جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("User.aspx" + "?action=" + (byte)Enums.Page_ActionCode.Insert, true);
        }

        /// <summary>
        /// نمایش سطوح دسترسی براساس گروه انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            ISbtnFilterClicked = true;
            BindGrid();
        }

        /// <summary>
        /// نمایش تمام سطوح دسترسی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            ISbtnFilterClicked = false;
            States1.OstanCodeSelectedIndex = 0;
            //States1.RegionCodeSelectedIndex = 0;
            //States1.SchoolCodeSelectedIndex = 0;

            #region ریست کردن کنترل های فیلتر

            txtFullName.Text = "";
            txtPersonnelCode.Text = "";
            ddlStatus.SelectedValue = "-1";
            ddlUserGroups.SelectedValue = "-1";

            #endregion ریست کردن کنترل های فیلتر

            BindGrid();
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            BindGrid();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvUsers.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvUsers.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvUsers.HeaderRow.FindControl("img" + SortColumn) as Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }
}