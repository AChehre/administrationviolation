﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master"
    AutoEventWireup="true" CodeBehind="PermissionList.aspx.cs" Inherits="SankaWebAppFramework.Config.PermissionList" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Import Namespace="SankaWebAppFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <label class="form-field-label">
            انتخاب گروه :</label>
        <asp:DropDownList ID="ddlUserGroup" runat="server">
        </asp:DropDownList>
        <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server"
            Text="فیلتر" />
        <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server"
            Text="نمایش همه" />
    </div>
    <div class="grid">
        <SG:SankaGrid ID="gvPermissions" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="PermissionID" HeaderText="PermissionID" Visible="False" />
                <asp:BoundField DataField="GroupName" HeaderText="عنوان گروه کاربری" />
                <asp:TemplateField HeaderText="نام صفحه">
                    <HeaderTemplate>
                        <span dir="ltr">
                            <asp:LinkButton CommandArgument="AspxPage" data-sankatooltip="مرتب سازی براساس نام صفحه"
                                ToolTip="مرتب سازی براساس نام صفحه" CommandName="ASC" ID="lnkbtnAspxPage" OnCommand="lnkbtnSort_Command"
                                runat="server">نام صفحه</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgAspxPage"
                                runat="server" Visible="false" /></span>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("AspxPage").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="وضعیت دسترسی">
                    <ItemTemplate>
                        <%# Convert.ToBoolean(Eval("IsPublished")) ? Enums.PublishStatus_Label[1] : Enums.PublishStatus_Label[0]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="اکشن">
                    <ItemTemplate>
                        <%# Enums.Page_ActionCode_Label[Eval("ActionCode").ToString().StringToInt()]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عنوان صفحه">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="PageTitle" data-sankatooltip="مرتب سازی براساس عنوان صفحه"
                            ToolTip="مرتب سازی براساس عنوان صفحه" CommandName="ASC" ID="lnkbtnPageTitle"
                            OnCommand="lnkbtnSort_Command" runat="server">عنوان صفحه</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgPageTitle"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("PageTitle").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="حذف" onclick="return ShowConfirm(this);" href='<%# string.Format("PermissionList.aspx?ID={0}&action={1}", Eval("PermissionID"),(byte)Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
