﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class SetPermission : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد
            // مد ثبت رکورد جدید
            if (!CheckActionCode(Enums.Page_ActionCode.Insert))
            {
                MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PermissionList.aspx", true);
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                // مقداردهی گروههای کاربری
                Common.BindUserGroup(ddlUserGroup);

                // مقدار دهی صفحات
                Common.BindPages(ddlPage);
            }
        }

        /// <summary>
        /// ثبت سطح دسترسی برای گروه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_click(object sender, EventArgs e)
        {
            try
            {
                int PageID = ddlPage.SelectedValue.StringToInt();
                byte ActionCode = ddlAction.SelectedValue.StringToByte();
                int UserGroupID = ddlUserGroup.SelectedValue.StringToInt();

                string Message = "";
                if (!Validation(UserGroupID, ActionCode, out Message))
                {
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return;
                }

                #region ثبت سطح دسترسی جدید

                // مقداردهی آبجکت
                DTO.Config.tblPermissionMap.tblPermissionMap obj = new DTO.Config.tblPermissionMap.tblPermissionMap();
                obj.ActionCode = ActionCode;
                obj.PageID = PageID;
                obj.UserGroupID = UserGroupID;

                // ثبت در جدول
                bool InsertStatus = (new DAL.Config.tblPermissionMap.Methods()).Insert(obj);

                #region نمایش پیام مناسب

                if (InsertStatus)
                {
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, this);

                    //ddlPage.SelectedIndex = 0;
                    ddlAction.SelectedIndex = 0;
                }
                else
                {
                    MessageDialog.ShowMessage("سطح دسترسی موردنظر قبلا ثبت شده است.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام مناسب

                #endregion ثبت سطح دسترسی جدید
            }
            catch (Exception ex)
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                LogError(ex);
            }
        }

        /// <summary>
        /// نمایش اکشن های مربوطه به یک صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            int PageID = ddlPage.SelectedValue.StringToInt();

            #region نمایش اکشن های ثبت شده صفحه انتخابی

            DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();

            obj.PageID = PageID;

            ddlAction.DataSource = (new DAL.Config.tblPageDetail.Methods()).SelectByPageID(obj);
            ddlAction.DataTextField = "PageTitle";
            ddlAction.DataValueField = "ActionCode";
            ddlAction.DataBind();

            ddlAction.Items.Insert(0, new ListItem("انتخاب اکشن", "255"));

            #endregion نمایش اکشن های ثبت شده صفحه انتخابی

            ddlAction.Enabled = true;
        }

        /// <summary>
        /// اعتبار سنجی داده های ورودی فرم
        /// </summary>
        /// <param name="UserGroupID">آیدی گروه</param>
        /// <param name="ActionCode">اکشن کد</param>
        /// <param name="Message">پیغام خطا</param>
        /// <returns>ترو یعنی بدون خطا و فالس یعنی خطا دارد و باید محتوای مسیج نمایش داده شود</returns>
        protected bool Validation(int UserGroupID, byte ActionCode, out string Message)
        {
            bool Result = true;

            Message = "";

            if (UserGroupID < 0)
            {
                Message = "لطفا گروه موردنظر خود را انتخاب نمایید";
                Result = false;
            }

            if (ActionCode == 255)
            {
                Message += "<br/>";
                Message += "لطفا اکشن موردنظر خود را انتخاب نمایید";
                Result = false;
            }

            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });


            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/PermissionList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}