﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="MenuPermission.aspx.cs" Inherits= "SankaWebAppFramework.Config.MenuPermission" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="_10bm">
        <label class="form-field-label">
            انتخاب منو</label>
        <asp:DropDownList ID="ddlMenu" runat="server" Width="300px">
        </asp:DropDownList>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            انتخاب گروه :</label>
        <asp:DropDownList ID="ddlUserGroup" runat="server" Width="300px">
        </asp:DropDownList>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
