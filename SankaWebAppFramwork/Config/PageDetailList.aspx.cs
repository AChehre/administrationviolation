﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class PageDetailList : GeneralPage
    {
        /// <summary>
        /// آیدی صفحه
        /// </summary>
        private int PageID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// اکشن کد صفحه
        /// </summary>
        private byte Target_ActionCode
        {
            get
            {
                if (Request.QueryString.Get("ActionCode") != null)
                {
                    return Request.QueryString.Get("ActionCode").StringToByte();
                }
                // یعنی در کوئری استرینگ وجود ندارد
                return 255;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region مد حذف یک رکورد

            if (Target_ActionCode != 255 && PageID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PageDetailList.aspx?ID=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                Delete();
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                #region بدست آوردن اطلاعات صفحه جهت نمایش در لیبل عنوان صفحه

                DTO.Config.tblPages.tblPages obj = new DTO.Config.tblPages.tblPages();
                obj.PageID = PageID;

                obj = (new DAL.Config.tblPages.Methods()).SelectByID(obj);
                if (obj.RowNumber > 0)
                {
                    PageLabelTitle = "لیست اکشن های صفحه [ " + obj.AspxPage + " ]";
                }

                #endregion بدست آوردن اطلاعات صفحه جهت نمایش در لیبل عنوان صفحه

                if (PageID > 0)
                {
                    BindGrid();
                }
            }
        }

        /// <summary>
        /// نمایش اطلاعات
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();
                obj.PageID = PageID;
                List<DTO.Config.tblPageDetail.tblPageDetail> RList = (new DAL.Config.tblPageDetail.Methods()).SelectByPageID(obj);
                gvPages.DataSource = RList;
                gvPages.DataBind();
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// حذف صفحه
        /// </summary>
        protected void Delete()
        {
            bool DeleteStat = false;
            bool DeleteDefaultAction = false;
            try
            {
                #region بدست آوردن اطلاعات صفحه

                DTO.Config.tblPages.tblPages objPage = new DTO.Config.tblPages.tblPages();
                objPage.PageID = PageID;

                objPage = (new DAL.Config.tblPages.Methods()).SelectByID(objPage);
                if (objPage.RowNumber > 0)
                {// اگر اکشنی که میخواهد حذف کند، اکشن پیش فرض باشد، اخطار میدهیم و حذف نمیکنیم
                    if (objPage.DefaultActionCode == Target_ActionCode)
                    {
                        DeleteDefaultAction = true;
                    }
                }

                #endregion بدست آوردن اطلاعات صفحه

                if (!DeleteDefaultAction)
                {// اگر اکشن پیش فرض نباشد، حذف شود
                    DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();
                    obj.PageID = PageID;
                    obj.ActionCode = Target_ActionCode;
                    DeleteStat = (new DAL.Config.tblPageDetail.Methods()).Delete(obj);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (DeleteDefaultAction)
            {// اگر اکشن پیش فرض برای حذف انتخاب شده بود، پیغام خطا دهد
                MessageDialog.ShowMessage("شما نمیتوانید، اکشن پیش فرض صفحه را حذف نمایید", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?Id=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.View, true);
                return;
            }

            if (DeleteStat)
            {
                MessageDialog.ShowMessage("اکشن مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?Id=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.View, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در عملیات", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?Id=" + PageID + "&action="+(byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.ImageButton
            });

            ActionMenu1.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ثبت جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("PageDetail.aspx?ID=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.Insert, true);
        }
    }
}