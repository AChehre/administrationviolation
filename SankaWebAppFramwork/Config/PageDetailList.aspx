﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageDetailList.aspx.cs"
    Inherits="SankaWebAppFramework.Config.PageDetailList" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register TagPrefix="sanka" TagName="ActionMenu" Src="~/controls/UCActionMenu.ascx" %>
<%@ Import Namespace="SankaWebAppFramework" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/superfish.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/hoverIntent.js" type="text/javascript"></script>
    <script src="/js/superfish.js" type="text/javascript"></script>
    <script src="/js/site.js" type="text/javascript"></script>
    <link href="/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <sanka:ActionMenu ID="ActionMenu1" runat="server"></sanka:ActionMenu>
    <div class="notification-area" runat="server" id="divMessage">
    </div>
    <div class="grid">
        <asp:GridView ID="gvPages" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="PageID" HeaderText="PageID" Visible="False" />
                <asp:BoundField DataField="PageTitle" HeaderText="عنوان صفحه" />
                <asp:BoundField DataField="PageDescription" HeaderText="توضیحات صفحه" />
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="ویرایش" href='<%# string.Format("PageDetail.aspx?ID={0}&ActionCode={1}&action={2}",Eval("PageID").ToString(), Eval("ActionCode").ToString(), (byte)Enums.Page_ActionCode.Edit) %>'>
                            <asp:Image ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش" />
                        </a><a title="حذف" onclick="return ShowConfirm(this,'در صورت حذف این اکشن ، تمام سطوح دسترسی مربوطه نیز پاک میشوند، آیا از حذف این مورد اطمینان دارید ؟');"
                            href='<%# string.Format("PageDetailList.aspx?ID={0}&ActionCode={1}&action={2}", Eval("PageID"), Eval("ActionCode"), (byte)Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </asp:GridView>
    </div>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </sanka:SankaDialog>
    <asp:HiddenField ID="hfPageNumber" runat="server" Value="1" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageSize" runat="server" Value="10" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageChanged" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hfTotalRow" runat="server" Value="0" ClientIDMode="Static" />
    </form>
</body>
</html>
