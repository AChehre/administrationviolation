﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master"
    AutoEventWireup="true" CodeBehind="MenusList.aspx.cs" Inherits="SankaWebAppFramework.Config.MenusList" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Import Namespace="SankaWebAppFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <fieldset>
            <legend>جستجو</legend>

            <div class="_10bm">
                <label class="form-field-label">
                    عنوان :</label>
                <asp:TextBox ID="txtTitle" placeholder="عنوان" data-enter-target="btnFilter" MaxLength="50" runat="server"></asp:TextBox>
                <label class="form-field-label _50rmargin">
                    انتخاب گروه :</label>
                <asp:DropDownList ID="ddlUserGroup" runat="server">
                </asp:DropDownList>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    وضعیت دسترسی :</label>
                <asp:DropDownList ID="ddlPublish" runat="server">
                </asp:DropDownList>
                <label class="form-field-label _50rmargin">
                    نوع لینک :</label>
                <asp:DropDownList ID="ddlLinkType" runat="server">
                </asp:DropDownList>
            </div>

            <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server" Text="فیلتر" />
            <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server" Text="نمایش همه" />
        </fieldset>
    </div>
    <div class="grid">
        <SG:SankaGrid ID="gvMenu" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="MenuID" HeaderText="MenuID" Visible="False" />
                <asp:TemplateField HeaderText="عنوان">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="Title" data-sankatooltip="مرتب سازی براساس عنوان" ToolTip="مرتب سازی براساس عنوان"
                            CommandName="ASC" ID="lnkbtnTitle" OnCommand="lnkbtnSort_Command" runat="server">عنوان</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgTitle"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("Title").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Link" HeaderText="لینک" />
                <asp:TemplateField HeaderText="وضعیت دسترسی">
                    <ItemTemplate>
                        <%# Convert.ToBoolean(Eval("IsPublished")) ? Enums.PublishStatus_Label[1] : Enums.PublishStatus_Label[0]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نوع لینک">
                    <ItemTemplate>
                        <%# Enums.Menu_LinkType_Label[Eval("Type").ToString().StringToInt()]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ترتیب نمایش">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="ViewOrder" data-sankatooltip="مرتب سازی براساس ترتیب نمایش" ToolTip="مرتب سازی براساس ترتیب نمایش"
                            CommandName="ASC" ID="lnkbtnViewOrder" OnCommand="lnkbtnSort_Command" runat="server">ترتیب نمایش</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgViewOrder"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("ViewOrder").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="ویرایش" href='<%# string.Format("Menu.aspx?ID={0}&action={1}",Eval("MenuID").ToString(), (byte)Enums.Page_ActionCode.Edit ) %>'>
                            <asp:Image ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش" />
                        </a>
                        <a title="حذف" onclick="return ShowConfirm(this);" href='<%# string.Format("MenusList.aspx?MenuID={0}&action={1}", Eval("MenuID"),(byte)Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
