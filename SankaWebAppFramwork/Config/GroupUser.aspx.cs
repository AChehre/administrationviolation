﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class GroupUser : GeneralPage
    {
        /// <summary>
        /// آیدی کاربر جهت ویرایش اطلاعات
        /// </summary>
        private long UID
        {
            get
            {
                if (Request.QueryString.Get("UID") != null)
                {
                    return Request.QueryString.Get("UID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی گروه
        /// </summary>
        private int GID
        {
            get
            {
                if (Request.QueryString.Get("GID") != null)
                {
                    return Request.QueryString.Get("GID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            States1.VisibleRegion = false;
            States1.VisibleSchool = false;

            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (UID == 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "GroupUsersList.aspx?GID=" + GID, true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();
                #region بررسی اینکه کاربر جاری،عضو گروه جاری باشد یا اینکه سرگروه گروه جاری باشد

                CheckUserAccessToGroup(GID);

                #endregion بررسی اینکه کاربر جاری،عضو گروه جاری باشد یا اینکه سرگروه گروه جاری باشد

                #region ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                if (UserOstanCode.HasValue)
                {
                    States1.OstanCode = UserOstanCode.Value;
                    States1.CurrentUserOstanCode = UserOstanCode.Value;
                }
                //if (UserRegionCode.HasValue)
                //{
                //    States1.RegionCode = UserRegionCode.Value;
                //    States1.CurrentUserRegionCode = UserRegionCode.Value;
                //}
                //if (UserSchoolCode.HasValue)
                //{
                //    States1.SchoolCode = UserSchoolCode.Value;
                //    States1.CurrentUserSchoolCode = UserSchoolCode.Value;
                //}

                States1.UserID = UserID;

                #endregion ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                #region نمایش اطلاعات گروه انتخاب شده

                if (GID > 0)
                {
                    DTO.Config.tblUserGroup.tblUserGroup ObjUserGroup = new DTO.Config.tblUserGroup.tblUserGroup();
                    ObjUserGroup.GroupID = GID;
                    ObjUserGroup = (new DAL.Config.tblUserGroup.Methods()).SelectByID(ObjUserGroup);

                    if (ObjUserGroup.RowNumber > 0)
                    {// اگر اطلاعات کاربر وجود داشت
                        string Action = UID > 0 ? "ویرایش " : "ثبت ";
                        PageLabelTitle = Action + "کاربر در گروه [ " + ObjUserGroup.GroupName + " ]";

                        ViewState["PageTitle"] = Action + "کاربر در گروه « " + ObjUserGroup.GroupName + " »";

                        this.Title = ObjUserGroup.GroupName;
                        lblGroupTitle.Text = ObjUserGroup.GroupName;
                        ViewState.Add("PageTitle", ObjUserGroup.GroupName);
                    }

                    #region نمایش گروه جاری به عنوان گروه عضو برای کاربر

                    //if (UID < 1)
                    //{// فقط در حالت ثبت، گروه جاری را به صورت پیش فرض انتخاب کن
                    //    List<DTO.tblUserGroupMap.tblUserGroupMap> lstUserGroup = new List<DTO.tblUserGroupMap.tblUserGroupMap>
                    //    {
                    //        new DTO.tblUserGroupMap.tblUserGroupMap {
                    //            UserGroupID = GID,
                    //        }
                    //    };
                    //    foreach (TreeNode node in tvGroup.Nodes)
                    //    {
                    //        CheckedNodes(lstUserGroup, node);
                    //    }
                    //}

                    #endregion نمایش گروه جاری به عنوان گروه عضو برای کاربر
                }

                #endregion نمایش اطلاعات گروه انتخاب شده

                //btnUserAreaAccessLevel.Visible = false;

                if (UID > 0)
                {// مد ویرایش
                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    // اگر اکشن کد در کوئری استرینگ نبود، یا آیدی برابر با آیدی کاربر جاری بود، پیغام دهد که 
                    // مجوز ندارد ، چون یک کاربر نمیتواند خودش را ویرایش یا حذف نماید
                    if (!CheckActionCode(Enums.Page_ActionCode.Edit) || UID == UserID)
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "GroupUsersList.aspx?GID=" + GID, true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    #region نمایش اطلاعات هنگام ویرایش
                    LoadData();
                    //  اگر حالت ویرایش باشد، لینک انتخاب مناطق دسترسی را نمایش میدهیم
                    //btnUserAreaAccessLevel.Visible = true;
                    #endregion نمایش اطلاعات هنگام ویرایش
                }
            }

            this.Title = ViewState["PageTitle"].ToString();
        }

        /// <summary>
        /// نمایش اطلاعات کاربر روی فرم
        /// </summary>
        protected void LoadData()
        {
            States1.Initialize();
            try
            {
                DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView ObjUser = new DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView();
                ObjUser.UserID = UID;
                ObjUser.OstanCode = UserOstanCode;
                ObjUser.RegionCode = UserRegionCode;
                ObjUser.SchoolCode = UserSchoolCode;
                ObjUser.CurrentUserID = UserID;
                ObjUser = (new DAL.Config.tblUserAttributesMapView.Methods()).SelectByID(ObjUser);

                if (ObjUser.RowNumber > 0)
                {// اگر اطلاعات کاربر وجود داشت
                    txtEMail.Text = ObjUser.EMail;
                    txtFName.Text = ObjUser.FName;
                    txtLName.Text = ObjUser.LName;
                    txtUserName.Text = ObjUser.UserName;
                    rbtnIsActive.SelectedValue = "0";
                    if (ObjUser.IsActive.Value)
                    {
                        rbtnIsActive.SelectedValue = "1";
                    }

                    // نمایش اطلاعات اضافی کاربر ، مانند استان ، منطقه و غیره
                    States1.OstanCode = 0;
                    //States1.RegionCode = 0;
                    //States1.SchoolCode = 0;

                    if (ObjUser.OstanCode.HasValue)
                    {
                        States1.OstanCode = ObjUser.OstanCode.Value;

                        // آیدی استان را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر استانش را تغییر داده است یا نه
                        hfOstanCode.Value = ObjUser.OstanCode.Value.ToString();
                    }

                    //if (ObjUser.RegionCode.HasValue)
                    //{
                    //    States1.RegionCode = ObjUser.RegionCode.Value;
                    //    // آیدی را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر منطقه اش را تغییر میدهد یا نه
                    //    hfRegionCode.Value = ObjUser.RegionCode.Value.ToString();
                    //}

                    //if (ObjUser.SchoolCode.HasValue)
                    //{
                    //    States1.SchoolCode = ObjUser.SchoolCode.Value;
                    //    // آیدی را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر مدرسه اش را تغییر میدهد یا نه
                    //    hfSchoolCode.Value = ObjUser.SchoolCode.Value.ToString();
                    //}

                    if (ObjUser.PersonnelCode.HasValue)
                    {
                        txtPersonnelCode.Text = ObjUser.PersonnelCode.Value.ToString();
                    }

                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.Attributes.Add("data-href", "UserAreaAccessLevel.aspx?schoolcode=" + ObjUser.SchoolCode + "&regioncode=" + ObjUser.RegionCode + "&ostancode=" + ObjUser.OstanCode + "&uid=" + ObjUser.UserID);
                }
                else
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "GroupUsersList.aspx?GID=" + GID, true);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?UID=" + NewUserID + "&gid=" + GID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?gid=" + GID + "&action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "GroupUsersList.aspx?gid=" + GID + "&action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ثبت یا ویرایش اطلاعات
        /// </summary>
        /// <returns>آیدی کاربر ذخیره شده</returns>
        protected long SaveData()
        {
            long Result = 0;

            try
            {
                #region گرفتن اطلاعات فرم

                DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();

                ObjUser.EMail = txtEMail.Text;
                ObjUser.FName = txtFName.Text;
                ObjUser.IsActive = rbtnIsActive.SelectedValue.StringToByte() == 0 ? false : true;
                ObjUser.LName = txtLName.Text;
                if (!string.IsNullOrEmpty(txtPass.Text.Trim()))
                {// اگر پسورد خالی نبود، مقدارش ست شود
                    ObjUser.Password = txtPass.Text.getMD5();
                }
                ObjUser.RegisterDate = DateTime.Now;
                ObjUser.UserName = txtUserName.Text;

                DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr = new DTO.Config.tblUserAttributes.tblUserAttributes();

                ObjUserAttr.OstanCode = States1.OstanCode;

                if (ObjUserAttr.OstanCode < 1)
                {
                    ObjUserAttr.OstanCode = null;
                }

                //ObjUserAttr.RegionCode = States1.RegionCode;

                //if (ObjUserAttr.RegionCode < 1)
                //{
                //    ObjUserAttr.RegionCode = null;
                //}

                //ObjUserAttr.SchoolCode = States1.SchoolCode;

                //if (ObjUserAttr.SchoolCode < 1)
                //{
                //    ObjUserAttr.SchoolCode = null;
                //}

                ObjUserAttr.PersonnelCode = txtPersonnelCode.Text.StringToLong();

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(ObjUser, ObjUserAttr,this.GID, txtPass.Text.Trim(), txtRePass.Text.Trim()))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    return 0;
                }

                if (UID > 0)
                {
                    #region ویرایش اطلاعات

                    ObjUser.UserID = UID;

                    using (UserManagment UManageObj = new UserManagment())
                    {// ویرایش
                        Result = UManageObj.EditUser(ObjUser, ObjUserAttr);
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    ObjUser.UserID = 0;

                    // انتخاب گروه جاری برای کاربر
                    List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();

                    _lstSelectedgroup.Add(new DTO.Config.tblUserGroupMap.tblUserGroupMap
                    {
                        UserGroupID = GID
                    });

                    using (UserManagment UManageObj = new UserManagment())
                    {// ثبت

                        if (_lstSelectedgroup.Where(g => g.UserGroupID == Common.PublicGroupID).FirstOrDefault() == null)
                        {// اگر گروه عمومی انتخاب نشده باشد،به صورت پیش فرض هرکاربر که ثبت نام شود، باید عضو گروه عمومی قرار بگیرد
                            _lstSelectedgroup.Add(new DTO.Config.tblUserGroupMap.tblUserGroupMap
                            {
                                UserID = 0,
                                UserGroupID = Common.PublicGroupID
                            });
                        }

                        Result = UManageObj.AddUser(ObjUser, ObjUserAttr, _lstSelectedgroup);
                    }

                    #endregion ثبت اطلاعات
                }

                if (Result > 0)
                {
                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.Attributes.Add("data-href", "UserAreaAccessLevel.aspx?schoolcode=" + ObjUserAttr.SchoolCode + "&regioncode=" + ObjUserAttr.RegionCode + "&ostancode=" + ObjUserAttr.OstanCode + "&uid=" + ObjUserAttr.UserID);
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="ObjUser">آبجکت از نوع کلاس یوزر</param>
        /// <param name="ObjUserAttr">آبجکت از نوع کلاس ویژگی های یوزر</param>
        /// <param name="SelecteGroupCount">تعداد گروه های انتخاب شده برای کاربر</param>
        /// <param name="Pass">کلمه عبور</param>
        /// <param name="RePass">تکرار کلمه عبور</param>
        /// <param name="Message">متن خطایی که برمیگرداند</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, int SelecteGroupCount, string Pass, string RePass)
        {
            List<string> lstMessage = new List<string>();
            if (string.IsNullOrEmpty(ObjUser.FName))
            {
                lstMessage.Add("لطفا نام را وارد نمایید");
            }

            if (string.IsNullOrEmpty(ObjUser.LName))
            {
                lstMessage.Add("لطفا نام خانوادگی را وارد نمایید");
            }
            if (!string.IsNullOrEmpty(ObjUser.EMail) && !ObjUser.EMail.IsEmailAdress())
            {// در صورتی که آدرس ایمیل وارد کرده باشد، اعتبارسنجی میشود، اگر ناردست باشد، خطا میدهد

                lstMessage.Add("لطفا آدرس ایمیل را به صورت صحیح وارد نمایید");
            }
            if (string.IsNullOrEmpty(ObjUser.UserName))
            {
                lstMessage.Add("لطفا نام کاربری را وارد نمایید");
            }
            else
            {// اگر نام کاربری وارد شده باشد، یکتا بودن آنرا در سیستم بررسی میکنیم

                bool CheckUserName = false;

                if (UID > 0)
                {// اگر در حالت ویرایش باشیم
                    CheckUserName = Common.CheckUserName(ObjUser.UserName, UID);
                }
                else
                {// اگر ثبت جدید باشد
                    CheckUserName = Common.CheckUserName(ObjUser.UserName);
                }

                if (!CheckUserName)
                {
                    lstMessage.Add("نام کاربری تکراری می باشد");
                }
            }

            if (UID < 1)
            {// فقط زمانی که در حال ثبت کاربر هستیم کلمه عبور را چک میکنیم، در حالت ویرایش اگر وارد شده باشد چک میکنیم
                if (string.IsNullOrEmpty(Pass))
                {
                    lstMessage.Add("لطفا کلمه عبور را وارد نمایید");
                }
                else
                {
                    if (Pass != RePass)
                    {
                        lstMessage.Add("کلمه عبور و تکرار آن مطابقت ندارد");
                    }
                }
            }
            else //  در حالت ویرایش اگر کلمه عبور وارد شد، چک شود که تطابق داشته باشد
            {
                if (!string.IsNullOrEmpty(Pass))
                {
                    if (Pass != RePass)
                    {
                        lstMessage.Add("کلمه عبور تطابق ندارد");
                    }
                }
            }

            if (SelecteGroupCount < 1)
            {
                lstMessage.Add("حداقل یک گروه کاربری را برای عضویت کاربر انتخاب نمایید.");
            }

            // بررسی اطلاعات اضافی کاربر، مانند استان، منطقه،مدرسه

            if (!ObjUserAttr.OstanCode.HasValue)
            {
                lstMessage.Add("لطفا استان را انتخاب نمایید");
            }
            //if (!ObjUserAttr.RegionCode.HasValue)
            //{
            //    lstMessage.Add("لطفا منطقه را انتخاب نمایید");
            //}
            //if (ObjUserAttr.SchoolID < 1)
            //{
            //    Result = false;
            //    if (!string.IsNullOrEmpty(Message))
            //    {
            //        Message += "<br/>";
            //    }
            //    Message += "لطفا مدرسه را انتخاب نمایید";
            //}

            //if (ddlDefaultPageUrl.SelectedIndex == 0)
            //{
            //    lstMessage.Add("صفحه پیش فرض را انتخاب نمایید .");
            //}
            if (lstMessage.Count > 0)
            {
                MessageDialog.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return false;
            }
            return true;
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, long _UserID, TreeNode Node)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Config.tblUserGroupMap.tblUserGroupMap Obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                Obj.UserGroupID = Node.Value.StringToInt();
                Obj.UserID = _UserID;
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, _UserID, thisNode);
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/GroupUsersList.aspx?gid=" + GID + "&action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}