﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageDetail.aspx.cs" Inherits="SankaWebAppFramework.Config.PageDetail" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register TagPrefix="sanka" TagName="ActionMenu" Src="~/controls/UCActionMenu.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/superfish.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/hoverIntent.js" type="text/javascript"></script>
    <script src="/js/superfish.js" type="text/javascript"></script>
    <script src="/js/site.js" type="text/javascript"></script>
    <link href="/css/icons.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <Sanka:ActionMenu ID="ActionMenu1" runat="server">
    </Sanka:ActionMenu>
    <div class="notification-area" runat="server" id="divMessage">
    </div>
    <div class="prevententer">
        <div class="_10bm">
            <label class="form-field-label">
                انتخاب عملیات :</label>
            <asp:DropDownList ID="ddlActionCode" runat="server">
            </asp:DropDownList>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                عنوان صفحه :</label>
            <asp:TextBox ID="txtPageTitle" MaxLength="128" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                توضیحات صفحه :</label>
            <asp:TextBox TextMode="MultiLine" MaxLength="256" ID="txtPageDescription" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <div style="float: right; width: 150px; overflow: hidden">
                <label class="form-field-label">
                    گروه انتخابی :</label>
            </div>
            <div style="float: right; overflow: hidden">
                <asp:TreeView Style="padding: 0; margin-top: -15px" ID="tvGroup" runat="server" ShowCheckBoxes="All"
                    ShowLines="True">
                </asp:TreeView>
            </div>
        </div>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
    </form>
</body>
</html>
