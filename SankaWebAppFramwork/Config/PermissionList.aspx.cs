﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class PermissionList : GeneralPage
    {
        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        /// <summary>
        /// آیدی گروه کاربری
        /// </summary>
        private int UGroupID
        {
            get
            {
                if (Request.QueryString.Get("GroupID") != null)
                {
                    return Request.QueryString.Get("GroupID").StringToInt();
                }
                return -1;
            }
        }

        /// <summary>
        /// آیدی پرمیژن جهت حذف اطلاعات
        /// </summary>
        private int PermissionID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region مد حذف یک رکورد

            if (PermissionID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PermissionList.aspx?action=" + (byte)Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                Delete();
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                // مقداردهی گروههای کاربری
                Common.BindUserGroup(ddlUserGroup);

                ddlUserGroup.SelectedValue = UGroupID.ToString();

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvPermissions.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
            }
        }

        /// <summary>
        /// حذف یک سطر
        /// </summary>
        protected void Delete()
        {
            bool DeleteStat = false;
            try
            {
                DTO.Config.tblPermissionMap.tblPermissionMap obj = new DTO.Config.tblPermissionMap.tblPermissionMap();
                obj.ID = PermissionID;
                DeleteStat = (new DAL.Config.tblPermissionMap.Methods()).Delete(obj);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (DeleteStat)
            {
                BindGrid();
                MessageDialog.ShowMessage("سطح دسترسی مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        ///گرید در اطلاعات نمایش 
        ///</summary>
        private void BindGrid()
        {
            try
            {
                DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView obj = new DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView();
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.SortColumn = SortColumn;
                obj.SortOrder = SortOrder;
                obj.UserGroupID = ddlUserGroup.SelectedValue.StringToInt();
                List<DTO.Config.tblPagesPermissionMapView.tblPagesPermissionMapView> RList = (new DAL.Config.tblPagesPermissionMapView.Methods()).SelectAllByPaging(obj);
                gvPermissions.CustomDataBind(RList, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);

                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;

                SetSortStatus();
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// نمایش سطوح دسترسی براساس گروه انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            BindGrid();
        }

        /// <summary>
        /// نمایش تمام سطوح دسترسی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            ddlUserGroup.SelectedIndex = 0;
            this.PageNumber = 1;
            BindGrid();
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.ImageButton
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ثبت جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("SetPermission.aspx?action=" + (byte)Enums.Page_ActionCode.Insert, true);
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            BindGrid();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvPermissions.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvPermissions.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvPermissions.HeaderRow.FindControl("img" + SortColumn) as Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }
}