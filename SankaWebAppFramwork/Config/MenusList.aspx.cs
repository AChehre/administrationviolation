﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class MenusList : GeneralPage
    {
        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        /// <summary>
        /// آیدی منو جهت حذف اطلاعات
        /// </summary>
        private int MenuID
        {
            get
            {
                if (Request.QueryString.Get("MenuID") != null)
                {
                    return Request.QueryString.Get("MenuID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region مد حذف یک رکورد

            if (MenuID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "MenusList.aspx?action=" + (byte)Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                DeleteMenu();
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                Common.BindPublishStatus(ddlPublish);
                Common.BindUserGroup(ddlUserGroup);
                Common.BindMenuLinkType(ddlLinkType);

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvMenu.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
            }
        }

        /// <summary>
        /// مقداردهی گرید
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Config.tblMenu.tblMenu obj = new DTO.Config.tblMenu.tblMenu();
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.SortColumn = SortColumn;
                obj.SortOrder = SortOrder;
                obj.Title = txtTitle.Text.CorrectArabicChars();
                obj.Type = null;
                obj.IsPublished = null;
                obj.UserGroupID = null;

                if (ddlLinkType.SelectedValue.StringToInt() > 0)
                {
                    obj.Type = ddlLinkType.SelectedValue.StringToByte();
                }

                if (ddlPublish.SelectedValue.StringToInt() >= 0)
                {
                    obj.IsPublished = ddlPublish.SelectedValue.StringToByte() == (byte)Enums.PublishStatus.Published ? true : false;
                }

                if (ddlUserGroup.SelectedValue.StringToInt() > 0)
                {
                    obj.UserGroupID = ddlUserGroup.SelectedValue.StringToInt();
                }

                List<DTO.Config.tblMenu.tblMenu> RList = (new DAL.Config.tblMenu.Methods()).SelectAllByPaging(obj);
                gvMenu.CustomDataBind(RList, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;

                SetSortStatus();
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// حذف یک سطر
        /// </summary>
        protected void DeleteMenu()
        {
            bool DeleteStat = false;

            try
            {
                DTO.Config.tblMenu.tblMenu obj = new DTO.Config.tblMenu.tblMenu();
                obj.MenuID = MenuID;
                DeleteStat = (new DAL.Config.tblMenu.Methods()).Delete(obj);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (DeleteStat)
            {
                MessageDialog.ShowMessage("منو مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
                btnFilter_Click(null, null);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.ImageButton
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ثبت جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("Menu.aspx" + "?action=" + (byte)Enums.Page_ActionCode.Insert, true);
        }

        /// <summary>
        /// کلیک دکمه فیلتر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            BindGrid();
        }

        /// <summary>
        /// کلیک دکمه نمایش همه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;

            txtTitle.Text = "";
            ddlLinkType.SelectedValue = "-1";
            ddlPublish.SelectedValue = "-1";
            ddlUserGroup.SelectedValue = "-1";

            BindGrid();
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            BindGrid();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvMenu.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvMenu.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvMenu.HeaderRow.FindControl("img" + SortColumn) as Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }
}