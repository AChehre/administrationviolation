﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserAreaAccessLevel.aspx.cs"
    Inherits= "SankaWebAppFramework.Config.UserAreaAccessLevel" %>

<%@ Register TagPrefix="sanka" TagName="UserArea" Src="~/controls/UCUserAreaAccessLevel.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/UserAreaAccessLevel.css" rel="stylesheet" type="text/css" />
     <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <Sanka:UserArea ID="UserArea1" runat="server">
        </Sanka:UserArea>
    </div>
    </form>
</body>
</html>
