﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class Menu : GeneralPage
    {
        private int MenuID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (MenuID == 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "MenusList.aspx", true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                try
                {
                    // مقدار دهی صفحات
                    Common.BindPublishedPages(ddlPage);

                    //** مقداردهی منوها
                    Common.BindMenus(ddlParent);

                    CreateGroupTree();

                    if (MenuID > 0)
                    {// مد ویرایش

                        #region چک کردن دسترسی کاربر به ویرایش رکورد

                        if (!CheckActionCode(Enums.Page_ActionCode.Edit))
                        {
                            MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "MenusList.aspx", true);
                        }

                        #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                        LoadData();
                    }
                }
                catch
                {

                }
            }

            hfInternalMenu.Value = ((byte)Enums.Menu_Type.Internal).ToString();
            hfExternalMenu.Value = ((byte)Enums.Menu_Type.External).ToString();
        }

        /// <summary>
        /// نمایش اکشن های مربوطه به یک صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            int PageID = ddlPage.SelectedValue.StringToInt();

            #region نمایش اکشن های ثبت شده صفحه انتخابی

            DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();

            obj.PageID = PageID;

            ddlAction.DataSource = (new DAL.Config.tblPageDetail.Methods()).SelectByPageID(obj);
            ddlAction.DataTextField = "PageTitle";
            ddlAction.DataValueField = "ActionCode";
            ddlAction.DataBind();

            ddlAction.Items.Insert(0, new ListItem("انتخاب اکشن", "255"));

            #endregion نمایش اکشن های ثبت شده صفحه انتخابی

            ddlAction.Enabled = true;
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?ID=" + NewID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                int NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود

                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                int NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "MenusList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// اگر کوئری استرینگ حاوی آیدی صفحه باشد، عمل ویرایش
        /// در غیر این صورت عمل ثبت انجام میشود
        /// </summary>
        protected int SaveData()
        {
            int Result = 0;

            try
            {
                #region گرفتن اطلاعات فرم

                string Title = txtTitle.Text.Trim();
                bool IsPublished = rbtnPublish.SelectedValue.StringToByte() == 0 ? false : true;
                byte Type = rbtnType.SelectedValue.StringToByte();
                int Actioncode = ddlAction.SelectedValue.StringToInt();
                string Link = "";

                if (Type == (byte)Enums.Menu_Type.Internal)
                {
                    Link = ddlPage.SelectedItem.Text + "?action=" + Actioncode;
                }
                else if (Type == (byte)Enums.Menu_Type.External)
                {
                    Link = txtLink.Text.Trim();
                }

                int ParentID = ddlParent.SelectedValue.StringToInt();
                int ViewOrder = txtViewOrder.Text.StringToInt();

                // لیست گروه های کاربری ای که سطح دسترسی دیدن این اکشن را دارند
                List<DTO.Config.tblPermissionMap.tblPermissionMap> _lstSelectedgroup = new List<DTO.Config.tblPermissionMap.tblPermissionMap>();

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    GetAllCheckedNodes(_lstSelectedgroup, node);
                }

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(Title, Type, Actioncode, Link, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return 0;
                }

                #region مقداردهی آبجکت جهت ویرایش یا ثبت

                DTO.Config.tblMenu.tblMenu obj = new DTO.Config.tblMenu.tblMenu();
                obj.Title = Title;
                obj.IsPublished = IsPublished;
                obj.Type = Type;
                obj.Link = Link;
                obj.ParentID = ParentID;
                if (obj.ParentID == -1)
                {
                    obj.ParentID = null;
                }
                obj.ViewOrder = ViewOrder;

                obj.SelectedUserGroup = "";
                // گروه های کاربری انتخاب شده
                if (_lstSelectedgroup != null)
                {
                    foreach (DTO.Config.tblPermissionMap.tblPermissionMap Item in _lstSelectedgroup)
                    {
                        obj.SelectedUserGroup += Item.UserGroupID + ",";
                    }
                    if (obj.SelectedUserGroup.Length > 1)
                    {// پاک کردن آخرین کاما
                        obj.SelectedUserGroup = obj.SelectedUserGroup.Substring(0, obj.SelectedUserGroup.Length - 1);
                    }
                }

                #endregion مقداردهی آبجکت جهت ویرایش یا ثبت

                if (MenuID > 0)
                {
                    #region ویرایش اطلاعات

                    obj.MenuID = MenuID;

                    if ((new DAL.Config.tblMenu.Methods()).Update(obj))
                    {// اگر ویرایش موفقیت آمیز بود،آیدی رکورد را برمیگردانیم
                        Result = obj.MenuID.Value;
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    obj.MenuID = 0;

                    Result = (new DAL.Config.tblMenu.Methods()).Insert(obj);

                    #endregion ثبت اطلاعات
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// نمایش اطلاعات
        /// </summary>
        protected void LoadData()
        {
            DTO.Config.tblMenu.tblMenu Obj = new DTO.Config.tblMenu.tblMenu();

            Obj.MenuID = MenuID;

            Obj = (new DAL.Config.tblMenu.Methods()).SelectByID(Obj);

            if (Obj.RowNumber > 0)
            {
                if (Obj.ParentID.HasValue)
                {
                    ddlParent.SelectedValue = Obj.ParentID.Value.ToString();
                }

                txtTitle.Text = Obj.Title;
                txtViewOrder.Text = Obj.ViewOrder.ToString();
                rbtnPublish.SelectedValue = "0";
                if (Obj.IsPublished.Value)
                {
                    rbtnPublish.SelectedValue = "1";
                }
                rbtnType.SelectedValue = Obj.Type.ToString();

                if (Obj.Type == (byte)Enums.Menu_Type.Internal)
                {
                    string[] Array = Obj.Link.Split('?');

                    #region گرفتن آیدی صفحه

                    DTO.Config.tblPages.tblPages PageObj = new DTO.Config.tblPages.tblPages();

                    PageObj.AspxPage = Array[0];

                    PageObj = (new DAL.Config.tblPages.Methods()).SelectByAspxPage(PageObj);

                    if (PageObj.RowNumber > 0)
                    {
                        ddlPage.SelectedValue = PageObj.PageID.ToString();
                    }

                    #endregion گرفتن آیدی صفحه

                    ddlPage_SelectedIndexChanged(null, null);

                    ddlAction.SelectedValue = Array[1].Replace("action=","");
                }
                else if (Obj.Type == (byte)Enums.Menu_Type.External)
                {
                    txtLink.Text = Obj.Link;
                }

                #region انتخاب گروه هایی که به منو جاری دسترسی دارند

                // انتخاب گروه هایی که به منو جاری دسترسی دارند
                DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap objPermission = new DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap();
                objPermission.MenuID = Obj.MenuID;
                List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> lstPermission = (new DAL.Config.tblUserGroupMenuMap.Methods()).SelectByMenuID(objPermission);

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    CheckedNodes(lstPermission, node);
                }

                #endregion انتخاب گروه هایی که به منو جاری دسترسی دارند
            }
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="AspxPage">عنوان</param>
        /// <param name="DefaultActionCode">کد عملیات پیش فرض</param>
        /// <param name="Message">متن خطایی که برگشت داده میشود</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(string Title, byte Type, int Actioncode, string Link, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(Title))
            {
                Result = false;
                Message += "لطفا عنوان را وارد نمایید";
            }

            if (Type == (byte)Enums.Menu_Type.Internal && Actioncode == 255)
            {
                Result = false;
                Message += "<br/>" + "لطفا صفحه و اکشن مورد نظر را انتخاب نمایید";
            }
            else if (Type == (byte)Enums.Menu_Type.External && string.IsNullOrEmpty(Link))
            {
                Result = false;
                Message += "<br/>" + "لطفا آدرس لینک موردنظر را وارد نمایید";
            }

            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/MenusList.aspx?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر نال یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Config.tblPermissionMap.tblPermissionMap> _lstSelectedgroup, TreeNode Node)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Config.tblPermissionMap.tblPermissionMap Obj = new DTO.Config.tblPermissionMap.tblPermissionMap();
                Obj.UserGroupID = Node.Value.StringToInt();
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, thisNode);
            }
        }
    }
}