﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class MenuPermission : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (!CheckActionCode(Enums.Page_ActionCode.Insert))
            {
                MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "MenuPermissionList.aspx", true);
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                // مقداردهی گروههای کاربری
                Common.BindUserGroup(ddlUserGroup);

                // مقداردهی منوها
                Common.BindMenus(ddlMenu);
            }
        }

        /// <summary>
        /// دکمه ثبت
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_click(object sender, EventArgs e)
        {
            try
            {
                #region گرفتن اطلاعات فرم

                int UserGroupID = ddlUserGroup.SelectedValue.StringToInt();
                int MenuID = ddlMenu.SelectedValue.StringToInt();

                #endregion گرفتن اطلاعات فرم

                #region اعتبار سنجی

                if (MenuID < 1)
                {
                    MessageDialog.ShowMessage("لطفا منوی مورد نظر را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return;
                }
                if (UserGroupID < 1)
                {
                    MessageDialog.ShowMessage("لطفا گروه کاربری مورد نظر را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return;
                }

                #endregion اعتبار سنجی

                #region ثبت

                DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap Obj = new DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap();
                Obj.MenuID = MenuID;
                Obj.UserGroupID = UserGroupID;

                bool InsertStat = (new DAL.Config.tblUserGroupMenuMap.Methods()).Insert(Obj);

                if (InsertStat)
                {
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, this);
                }
                else
                {
                    MessageDialog.ShowMessage("سطح دسترسی موردنظر قبلا ثبت شده است.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                return;
            }

        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });


            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/MenuPermissionList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}