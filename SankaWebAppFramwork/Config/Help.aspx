﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/Config/Config.Master"
    AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="SankaWebAppFramework.Config.Help" %>

<%@ Register TagPrefix="sanka" TagName="Editor" Src="/sankaeditor/UCSankaEditor.ascx" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/sankaeditor/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/sankaeditor/ckeditor/adapters/jquery.js" type="text/javascript"></script>
    <script src="/sankaeditor/scripts/UCSankaEditor.js"></script>
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc2:SankaDialog>
    <div class="prevententer">
        <div class="_10bm">
            <label class="form-field-label">
                نوع هیات:</label>
            <asp:DropDownList AutoPostBack="true"
                ID="ddlBoardType" runat="server" ondatabound="ddlBoardType_DataBound" 
                onselectedindexchanged="ddlBoardType_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                انتخاب موضوع راهنما:</label>
            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlHelpType_SelectedIndexChanged"
                ID="ddlHelpType" runat="server" ondatabound="ddlHelpType_DataBound" 
                Width="250px">
            </asp:DropDownList>
        </div>
        <div class="_10bm">
            <sanka:Editor ID="ckDescription" runat="server" defaultlanguage="fa"></sanka:Editor>
        </div>
    </div>
</asp:Content>
