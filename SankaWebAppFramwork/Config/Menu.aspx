﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="Menu.aspx.cs" Inherits="SankaWebAppFramework.Config.Menu" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/Menus.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hfInternalMenu" runat="server" />
    <asp:HiddenField ID="hfExternalMenu" runat="server" />
    <div class="prevententer">
        <div class="_10bm">
            <label class="form-field-label">
                انتخاب منو پدر</label>
            <asp:DropDownList ID="ddlParent" runat="server">
            </asp:DropDownList>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                عنوان منو</label>
            <asp:TextBox ID="txtTitle" MaxLength="50" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                ترتیب نمایش</label>
            <asp:TextBox ID="txtViewOrder" CssClass="MustBeNumber" MaxLength="5" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                وضیعت دسترسی</label>
            <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
                ID="rbtnPublish" runat="server">
                <asp:ListItem Value="0" Text="غیر قابل دسترس"></asp:ListItem>
                <asp:ListItem Selected="True" Value="1" Text="قابل دسترس"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                نوع لینک منو</label>
            <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
                ID="rbtnType" runat="server">
                <asp:ListItem Value="1" Selected="True" Text="Internal"></asp:ListItem>
                <asp:ListItem Value="2" Text="External"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="_10bm" id="ExLinkContainer">
            <label class="form-field-label">
                لینک</label>
            <asp:TextBox ID="txtLink" CssClass="leftAlign" MaxLength="256" runat="server"></asp:TextBox>
        </div>
        <div id="InLinkContainer">
            <div class="_10bm" id="Div1">
                <label class="form-field-label">
                    صفحه</label>
                <asp:DropDownList ID="ddlPage" CssClass="leftAlign" AutoPostBack="true" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged"
                    runat="server">
                </asp:DropDownList>
            </div>
            <div class="_10bm" id="Div2">
                <label class="form-field-label">
                    اکشن</label>
                <asp:DropDownList ID="ddlAction" Enabled="false" runat="server">
                    <asp:ListItem Value="255" Selected="True" Text="انتخاب اکشن"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="_10bm">
            <div style="float: right; width: 150px; overflow: hidden">
                <label class="form-field-label">
                    گروه انتخابی :</label>
            </div>
            <div style="float: right; overflow: hidden">
                <asp:TreeView Style="padding: 0; margin-top: -15px" ID="tvGroup" runat="server" ShowCheckBoxes="All"
                    ShowLines="True">
                </asp:TreeView>
            </div>
        </div>
        <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
            ID="MessageDialog">
        </Sanka:SankaDialog>
    </div>
</asp:Content>
