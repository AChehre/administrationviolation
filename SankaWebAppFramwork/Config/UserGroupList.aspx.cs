﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class UserGroupList : GeneralPage
    {
        int GroupID;

        /// <summary>
        /// آیدی گروه کاربری
        /// </summary>
        private int UGID
        {
            get
            {
                if (Request.QueryString.Get("UGID") != null)
                {
                    return Request.QueryString.Get("UGID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region مد حذف یک رکورد

            if (UGID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UserGroupList.aspx?action=" + (byte)Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                Delete();
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                CreateGroupTree();
            }
        }

        /// <summary>
        /// حذف یک سطر
        /// </summary>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int SelectedId = GetSelectedIdinTree();
            if (SelectedId <= 0)
            {
                MessageDialog.ShowMessage("لطفا گروه موردنظر را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, this);
                return;
            }

            // رفتن به مد حذف رکورد
            Response.Redirect(HttpContext.Current.Request.Url.AbsolutePath + "?UGID=" + SelectedId + "&action=" + (byte)Enums.Page_ActionCode.Delete, true);
        }

        /// <summary>
        /// حذف رکورد
        /// </summary>
        protected void Delete()
        {

            if (UGID == 1)
            {// اگر گروه انتخابی، گروه عمومی باشد، حذف نشود
                MessageDialog.ShowMessage("گروه عمومی قابل حذف نمی باشد.", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }

            long RelatedRowCount = 0;
            bool DeleteStat = false;
            try
            {
                DTO.Config.tblUserGroup.tblUserGroup obj = new DTO.Config.tblUserGroup.tblUserGroup();

                obj.GroupID = UGID;

                // بدست آوردن تعداد رکوردهای مرتبط با این گروه کاربری
                RelatedRowCount = (new DAL.Config.tblUserGroup.Methods()).ChcekForDelete(obj);

                if (RelatedRowCount < 1)
                {// اگر گروه انتخابی، زیرگروه داشته باشد یا عضوی در آن باشد، حذف نشود
                    DeleteStat = (new DAL.Config.tblUserGroup.Methods()).Delete(obj);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (RelatedRowCount > 0)
            {// اگر گروه انتخابی، زیرگروه داشته باشد یا عضوی در آن باشد، حذف نشود
                MessageDialog.ShowMessage("امکان حذف گروه نمی باشد،گروه انتخابی شما، یا دارای زیرگروه می باشد و یا کاربری عضو آن است.", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View,true);
            }

            if (DeleteStat)
            {
                MessageDialog.ShowMessage("گروه کاربری مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر منفی یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        protected void tvGroup_SelectedNodeChanged(object sender, EventArgs e)
        {
            Response.Redirect("UserGroup.aspx?ugid=" + tvGroup.SelectedNode.Value + "&action=" + (byte)Enums.Page_ActionCode.Edit);
        }

        /// <summary>
        /// گرفتن آیدی آیتم انتخاب شده در نمایش درختی گروه ها
        /// </summary>
        /// <returns></returns>
        protected int GetSelectedIdinTree()
        {
            foreach (TreeNode node in tvGroup.Nodes)
            {
                GetSelectedValue(node);
            }

            return GroupID;
        }

        /// <summary>
        /// بدست آوردن آیدی گروهی که انتخاب شده است
        /// </summary>
        /// <param name="Node"></param>
        /// <returns></returns>
        protected void GetSelectedValue(TreeNode Node)
        {
            if (Node.Checked)
            {
                GroupID = Node.Value.StringToInt();
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetSelectedValue(thisNode);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه حذف
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnDelete_Click,
                CausesValidation = false,
                Icon = "/images/icon/Delete.png",
                TargetID = "btnDelete",
                Title = "حذف",
                OnClientClick = "return ShowConfirm(this);",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ثبت کاربر در گروه
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnAddUser_Click,
                CausesValidation = false,
                Icon = "/images/icon/AddUser.png",
                TargetID = "btnAddUser",
                Title = "ثبت کاربر",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه نمایش لیست کاربران گروه
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnUsersList_Click,
                CausesValidation = false,
                Icon = "/images/icon/people.png",
                TargetID = "btnUsersList",
                Title = "لیست کاربران",
                Type = ActionMenuType.ImageButton
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ثبت جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGroup.aspx" + "?action=" + (byte)Enums.Page_ActionCode.Insert, true);
        }

        /// <summary>
        /// کلیک دکمه ثبت کاربر جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            int SelectedId = GetSelectedIdinTree();
            if (SelectedId > 0)
            {
                Response.Redirect("GroupUser.aspx?gid=" + GetSelectedIdinTree() + "&action=" + (byte)Enums.Page_ActionCode.Insert, true);
            }

            MessageDialog.ShowMessage("لطفا گروه موردنظر را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, this);
        }

        /// <summary>
        /// کلیک دکمه ثبت کاربر جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUsersList_Click(object sender, EventArgs e)
        {
            int SelectedId = GetSelectedIdinTree();
            if (SelectedId > 0)
            {
                Response.Redirect("GroupUsersList.aspx?gid=" + GetSelectedIdinTree() + "&action=" + (byte)Enums.Page_ActionCode.View, true);
            }

            MessageDialog.ShowMessage("لطفا گروه موردنظر را انتخاب نمایید", DialogMessage.SankaDialog.Message_Type.Warning, this);
        }
    }
}