﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true" CodeBehind="GroupUsersList.aspx.cs" Inherits="SankaWebAppFramework.Config.GroupUsersList" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>
<%@ Import Namespace="SankaWebAppFramework" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <fieldset>
        <legend>جستجو</legend>
        <div>
            <div class="_10bm">
                <label class="form-field-label">
                    نام :</label>
                <asp:TextBox ID="txtFullName" placeholder="نام و نام خانوادگی" data-enter-target="btnFilter" MaxLength="30" runat="server"></asp:TextBox>
                <label class="form-field-label _50rmargin">
                    وضعیت :</label>
                <asp:DropDownList ID="ddlStatus" runat="server">
                </asp:DropDownList>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    کد پرسنلی :</label>
                <asp:TextBox ID="txtPersonnelCode" CssClass="MustBeNumber" data-enter-target="btnFilter" placeholder="کد پرسنلی" MaxLength="15" runat="server"></asp:TextBox>
            </div>
            <sanka:States DispalyInLine="True" ID="States1" runat="server"></sanka:States>
            <div class="_10bm">
                <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server"
                    Text="فیلتر" />
                <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server"
                    Text="نمایش همه" />
            </div>
        </div>
    </fieldset>
    <div class="grid">
        <SG:SankaGrid ID="gvUsers" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="UserID" HeaderText="UserID" Visible="False" />
                <asp:TemplateField HeaderText="نام و نام خانوادگی">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="FullName" data-sankatooltip="مرتب سازی براساس نام و نام خانوادگی" ToolTip="مرتب سازی براساس نام و نام خانوادگی"
                            CommandName="ASC" ID="lnkbtnFullName" OnCommand="lnkbtnSort_Command" runat="server">نام و نام خانوادگی</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgFullName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("FullName").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="نام کاربری">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="UserName" data-sankatooltip="مرتب سازی براساس نام کاربری" ToolTip="مرتب سازی براساس نام و نام خانوادگی"
                            CommandName="ASC" ID="lnkbtnUserName" OnCommand="lnkbtnSort_Command" runat="server">نام کاربری</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgUserName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("UserName").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ایمیل">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="EMail" data-sankatooltip="مرتب سازی براساس ایمیل" ToolTip="مرتب سازی براساس ایمیل"
                            CommandName="ASC" ID="lnkbtnEMail" OnCommand="lnkbtnSort_Command" runat="server">ایمیل</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgEMail"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("EMail").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="وضعیت">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="IsActive" data-sankatooltip="مرتب سازی براساس وضعیت" ToolTip="مرتب سازی براساس وضعیت"
                            CommandName="ASC" ID="lnkbtnIsActive" OnCommand="lnkbtnSort_Command" runat="server">وضعیت</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgIsActive"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("ActiveStatus").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="ویرایش" href='<%# string.Format("GroupUser.aspx?uid={0}&gid={1}&action={2}", Eval("UserID"), GID, (byte)SankaWebAppFramework.Enums.Page_ActionCode.Edit)%>'>
                            <asp:Image ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش" />
                        </a>

                        <a title="حذف" onclick="return ShowConfirm(this);" href='<%# string.Format("GroupUsersList.aspx?uid={0}&gid={1}&action={2}", Eval("UserID"), GID, (byte)SankaWebAppFramework.Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfbtnFilterClicked" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>
