﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="Pages.aspx.cs" Inherits="SankaWebAppFramework.Config.AddPages" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="prevententer">
            <h3 style="color: Red">
                مثلا اگر بخواهیم صفحه www.sample.com/sample.aspx را ثبت نماییم، در قسمت نام صفحه
                باید بنویسیم :<br />
                /sample.aspx
            </h3>
            <div class="_10bm">
                <label class="form-field-label">
                    نام صفحه</label>
                <asp:TextBox ID="txtAspxPage" placeholder="example : /sample.aspx" MaxLength="50"
                    CssClass="leftAlign" runat="server"></asp:TextBox>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    انتخاب اکشن پیش فرض :</label>
                <asp:DropDownList ID="ddlAction" runat="server">
                </asp:DropDownList>
                <span id='sanka_basic_modal'><a runat="server" id="lnkActionList" data-duplicatetitle="true" data-width="800" data-height="600" class='basic'>
                    <asp:Image CssClass="cp_vm" ID="imgSerial" runat="server" ImageUrl="~/images/icon/index_add.png" ToolTip="لیست اکشن ها" />
                </a></span>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    وضیعت دسترسی</label>
                <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
                    ID="rbtnPublish" runat="server">
                    <asp:ListItem Value="0" Text="غیر قابل دسترس"></asp:ListItem>
                    <asp:ListItem Selected="True" Value="1" Text="قابل دسترس"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
