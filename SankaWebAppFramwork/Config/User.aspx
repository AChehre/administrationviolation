﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="User.aspx.cs" Inherits="SankaWebAppFramework.Config.User" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <link href="/css/UCStates.css" rel="stylesheet" type="text/css" />
    <script src="js/User.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="prevententer">
        <div class="_10bm">
            <label class="form-field-label">
                نام :</label>
            <asp:TextBox ID="txtFName" MaxLength="50" runat="server"></asp:TextBox>
            <span id="sanka_basic_modal"><a href="/SearchUsers.aspx" data-sankatooltip="جستجو"
                data-title="جستجو" data-duplicatetitle="false" data-width="900" data-height="550"
                class='basic'>
                <img id="img1" class="cp_vm" alt="" src="/images/icon/Search.png" />
            </a></span>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                نام خانوادگی :</label>
            <asp:TextBox ID="txtLName" MaxLength="50" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                کد پرسنلی :</label>
            <asp:TextBox ID="txtPersonnelCode" MaxLength="14" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                ایمیل :</label>
            <asp:TextBox ID="txtEMail" MaxLength="50" CssClass="leftAlign" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                نام کاربری :</label>
            <asp:TextBox ID="txtUserName" MaxLength="50" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                وضیعت :</label>
            <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
                ID="rbtnIsActive" runat="server">
                <asp:ListItem Value="0" Text="غیرفعال"></asp:ListItem>
                <asp:ListItem Selected="True" Value="1" Text="فعال"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <sanka:States ID="States1" runat="server">
        </sanka:States>
        <div class="_10bm">
            <div style="float: right; width: 150px; overflow: hidden">
                <label class="form-field-label">
                    گروه انتخابی :</label>
            </div>
            <div style="float: right; overflow: hidden">
                <asp:TreeView Style="padding: 0; margin-top: -15px" ID="tvGroup" runat="server" ShowCheckBoxes="All"
                    ShowLines="True">
                </asp:TreeView>
            </div>
        </div>
       <%-- <div>
            <button type="button" id="btnUserAreaAccessLevel" runat="server" class="btn btn-primary"
                onclick="CheckChange()" visible="False">
                ثبت مناطق قابل دسترس</button>
        </div>--%>
        <div id="PassDiv" runat="server">
            <div class="_10bm">
                <label class="form-field-label">
                    کلمه عبور :</label>
                <asp:TextBox ID="txtPass" TextMode="Password" MaxLength="30" runat="server"></asp:TextBox>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    تکرار کلمه عبور :</label>
                <asp:TextBox ID="txtRePass" TextMode="Password" MaxLength="30" runat="server"></asp:TextBox>
            </div>
        </div>
        <%--<div class="_10bm">
            <label class="form-field-label">
                صفحه پیش فرض :</label>
            <span dir="ltr">
                <asp:DropDownList ID="ddlDefaultPageUrl" Width="270px" runat="server" />
            </span>
        </div>--%>
        <span id='sanka_basic_modal'>
            <button type="button" style="display: none" clientidmode="Static" id="lnkUserAreaAccessLevel"
                data-title="مناطق قابل دسترس" data-duplicatetitle="true" data-width="800" data-height="500"
                runat="server" class="btn btn-default basic">
                ثبت مناطق قابل دسترس</button>
        </span>
        <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
            ID="MessageDialog">
        </sanka:SankaDialog>
        <asp:HiddenField ID="hfOstanCode" runat="server" Value="0" />
        <asp:HiddenField ID="hfRegionCode" runat="server" Value="0" />
        <asp:HiddenField ID="hfSchoolCode" runat="server" Value="0" />
    </div>
</asp:Content>
