﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="SetPermission.aspx.cs" Inherits= "SankaWebAppFramework.Config.SetPermission" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
       <div class="_10bm">
            <label class="form-field-label">انتخاب گروه :</label>
            <asp:DropDownList ID="ddlUserGroup" runat="server" Width="250px">
            </asp:DropDownList>
        </div>
        <div class="_10bm">
            <label class="form-field-label">انتخاب صفحه :</label>
            <asp:DropDownList ID="ddlPage" AutoPostBack="true" 
                OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" runat="server" 
                Width="400px">
            </asp:DropDownList>
        </div>
       <div class="_10bm">
            <label class="form-field-label">انتخاب اکشن :</label>
            <asp:DropDownList ID="ddlAction" Enabled="false" runat="server" Width="250px">
            <asp:ListItem Value="255" Selected="True" Text="انتخاب اکشن"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
