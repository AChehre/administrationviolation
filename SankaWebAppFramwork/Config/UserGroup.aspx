﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="UserGroup.aspx.cs" Inherits= "SankaWebAppFramework.Config.UserGroup" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="_10bm prevententer">
        <label class="form-field-label">
            عنوان گروه :</label>
        <asp:TextBox ID="txtTitle" MaxLength="50" runat="server"></asp:TextBox>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            انتخاب سرگروه :</label>
        <asp:DropDownList ID="ddlUserGroup" runat="server">
        </asp:DropDownList>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
