﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="MenuPermissionList.aspx.cs" Inherits="SankaWebAppFramework.Config.MenuPermissionList" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Import Namespace="SankaWebAppFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <legend>جستجو</legend>

        <div class="_10bm">
            <label class="form-field-label">
                عنوان منو :</label>
            <asp:TextBox ID="txtTitle" placeholder="عنوان" data-enter-target="btnFilter" MaxLength="50" runat="server"></asp:TextBox>
            <label class="form-field-label _50rmargin">
                انتخاب گروه :</label>
            <asp:DropDownList ID="ddlUserGroup" runat="server">
            </asp:DropDownList>
        </div>
        <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server" Text="فیلتر" />
        <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server" Text="نمایش همه" />
    </fieldset>
    <div class="grid">
        <SG:SankaGrid ID="gvPages" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="عنوان گروه">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="GroupName" data-sankatooltip="مرتب سازی براساس عنوان گروه" ToolTip="مرتب سازی براساس عنوان گروه"
                            CommandName="ASC" ID="lnkbtnGroupName" OnCommand="lnkbtnSort_Command" runat="server">عنوان گروه</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgGroupName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("GroupName")%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="عنوان منو">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="Title" data-sankatooltip="مرتب سازی براساس عنوان منو" ToolTip="مرتب سازی براساس عنوان منو"
                            CommandName="ASC" ID="lnkbtnTitle" OnCommand="lnkbtnSort_Command" runat="server">عنوان</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgTitle"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href='<%# Eval("Link") %>'>
                            <%# Eval("Title")%>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="حذف" onclick="return ShowConfirm(this);" href='<%# string.Format("MenuPermissionList.aspx?MenuID={0}&gid={1}&action={2}", Eval("MenuID"), Eval("UserGroupID"), (byte)Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>

                        <%--                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="/images/icon/delete.png"
                            OnClientClick="return ShowConfirm(this);" ToolTip="حذف" OnCommand="imgDelete_Click"
                            CommandArgument='<%# Eval("MenuID")%>' CommandName='<%# Eval("UserGroupID")%>' />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
