﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="SankaWebAppFramework.Config.ChangePassword" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="prevententer">
        <div class="_5p odd-row">
            <label class="form-field-label">
                کلمه عبور فعلی :</label>
            <asp:TextBox ID="txtPass" TextMode="Password" MaxLength="30" data-required="true" runat="server"></asp:TextBox>
        </div>
        <div class="_5p even-row">
            <label class="form-field-label">
                کلمه عبور جدید :</label>
            <asp:TextBox ID="txtNewPass" TextMode="Password" MaxLength="30" data-required="true" runat="server"></asp:TextBox>
        </div>
        <div class="_5p odd-row">
            <label class="form-field-label">
                تکرار کلمه عبور جدید :</label>
            <asp:TextBox ID="txtReNewPass" TextMode="Password" MaxLength="30" data-required="true" runat="server"></asp:TextBox>
        </div>
    </div>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </sanka:SankaDialog>
</asp:Content>
