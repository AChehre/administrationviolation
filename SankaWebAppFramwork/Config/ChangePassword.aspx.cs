﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class ChangePassword : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateActionButton();
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="ObjUser">آبجکت از نوع کلاس یوزر</param>
        /// <param name="ObjUserAttr">آبجکت از نوع کلاس ویژگی های یوزر</param>
        /// <param name="SelecteGroupCount">تعداد گروه های انتخاب شده برای کاربر</param>
        /// <param name="Pass">کلمه عبور</param>
        /// <param name="RePass">تکرار کلمه عبور</param>
        /// <param name="Message">متن خطایی که برمیگرداند</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(string Pass,string NewPass , string ReNewPass, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(Pass))
            {
                Result = false;
                Message += "لطفا کلمه عبور فعلی را وارد نمایید";
            }

            if (string.IsNullOrEmpty(NewPass))
            {
                Result = false;
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا کلمه عبور جدید را وارد نمایید";
            }
            else
            {
                if (NewPass != ReNewPass)
                {
                    Result = false;
                    if (!string.IsNullOrEmpty(Message))
                    {
                        Message += "<br/>";
                    }
                    Message += "کلمه عبور تطابق ندارد";
                }
            }


            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton,
                OnClientClick = "return ValidateRequiredField();"
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region گرفتن اطلاعات فرم

                string Pass = txtPass.Text;
                string NewPass = txtNewPass.Text;
                string ReNewPass = txtReNewPass.Text;

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(Pass, NewPass, ReNewPass, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return;
                }

                DTO.Config.tblUser.tblUser Obj = new DTO.Config.tblUser.tblUser();

                Obj.Password = Pass.getMD5();
                Obj.UserID = UserID;

                if ((new DAL.Config.tblUser.Methods()).CheckUserPass(Obj).RowNumber > 0)
                {// اگر کلمه عبور صحیح وارد شده بود، کلمه عبور ویرایش شود
                    Obj.Password = NewPass.getMD5();
                    if ((new DAL.Config.tblUser.Methods()).ChangePass(Obj))
                    {
                        MessageDialog.ShowMessage("کلمه عبور با موفقیت تغییر یافت.", DialogMessage.SankaDialog.Message_Type.Message, this);
                    }
                    else
                    {
                        MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                    }
                }
                else
                {
                    MessageDialog.ShowMessage("کلمه عبور صحیح نمی باشد.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }
    }
}