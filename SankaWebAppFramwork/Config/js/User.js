﻿

function CheckChange(Item) {

    var StateID = $("[id$='hfOstanCode']").val();
    var RegionCode = $("[id$='hfRegionCode']").val();
    var SchoolID = $("[id$='hfSchoolCode']").val();
    //**
    var SelectedStateID = parseInt($("[id$='ddlState']").val()) > 0 ? parseInt($("[id$='ddlState']").val()) : 0;
    var SelectedRegionCode = parseInt($("[id$='ddlRegion']").val()) > 0 ? parseInt($("[id$='ddlRegion']").val()) : 0;
    var SelectedSchoolID = parseInt($("[id$='ddlSchool']").val()) > 0 ? parseInt($("[id$='ddlSchool']").val()) : 0;

    if (StateID == SelectedStateID && SelectedRegionCode == RegionCode && SchoolID == SelectedSchoolID) {
        $("#lnkUserAreaAccessLevel").trigger("click");
    }
    else {

        $("body").append("<div id='Confirm_dialog_auto_create'></div>");
        $("#Confirm_dialog_auto_create").html("جهت انجام تنظیمات ، ابتدا باید تغییرات را ذخیره نمایید، آیا مایل به ذخیره تغییرات هستید؟");
        $("#Confirm_dialog_auto_create").dialog({
            title: "اطلاع رسانی",
            buttons: {
                'خیر': function () {
                    $("[id$='ddlState']").val(StateID);
                    $("[id$='ddlState']").trigger("change");
                    $(this).dialog('close');
                    $("#lnkUserAreaAccessLevel").trigger("click");
                    $("#notification_dialog_auto_create").remove();
                },
                'بله': function () {
                    $("[id$='btnSave']").trigger("click");
                    $("#notification_dialog_auto_create").remove();
                }
            },
            modal: true
        });
    }
}