﻿
$(document).ready(function () {
    ChangeMenuType();
    $("[id$='rbtnType'] input").click(function () {
        ChangeMenuType();
    });
});

function ChangeMenuType() {
    var selvalue = $("[id$='rbtnType'] input:checked").val();
    var Internaltype = $("[id$='hfInternalMenu']").val();
    var Externaltype = $("[id$='hfExternalMenu']").val();
    if (selvalue == Internaltype) {
        $("#InLinkContainer").show();
        $("#ExLinkContainer").hide();
    }
    else {
        $("#ExLinkContainer").show();
        $("#InLinkContainer").hide();
    }
}