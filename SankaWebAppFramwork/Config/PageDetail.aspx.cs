﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class PageDetail : GeneralPage
    {
        /// <summary>
        /// آیدی صفحه
        /// </summary>
        private int PageID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// اکشن کد صفحه
        /// </summary>
        private byte Target_ActionCode
        {
            get
            {
                if (Request.QueryString.Get("ActionCode") != null)
                {
                    return Request.QueryString.Get("ActionCode").StringToByte();
                }
                // یعنی در کوئری استرینگ وجود ندارد
                return 255;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (Target_ActionCode == 255 && PageID > 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PageDetailList.aspx?ID=" + PageID, true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                try
                {
                    #region بدست آوردن اطلاعات صفحه جهت نمایش در لیبل عنوان صفحه

                    DTO.Config.tblPages.tblPages obj = new DTO.Config.tblPages.tblPages();
                    obj.PageID = PageID;

                    obj = (new DAL.Config.tblPages.Methods()).SelectByID(obj);
                    if (obj.RowNumber > 0)
                    {
                        string Action = Target_ActionCode != 255 ? "ویرایش " : "ثبت ";
                        PageLabelTitle = Action + "اکشن صفحه [ " + obj.AspxPage + " ]";
                    }

                    #endregion بدست آوردن اطلاعات صفحه جهت نمایش در لیبل عنوان صفحه

                    CreateGroupTree();
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }

                #region بایند کمبو اکشن کد

                Common.BindActions(ddlActionCode);

                #endregion بایند کمبو اکشن کد

                if (Target_ActionCode != 255 && PageID > 0)
                {// مد ویرایش

                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    if (!CheckActionCode(Enums.Page_ActionCode.Edit))
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PageDetailList.aspx?ID=" + PageID, true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    LoadData();
                    ddlActionCode.Enabled = false;
                }
            }
        }

        /// <summary>
        ///  نمایش اطلاعات جزئیات صفحه ای که ایدیش در کوئری استرینگ هست برای ویرایش
        /// </summary>
        protected void LoadData()
        {
            try
            {
                DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();
                obj.PageID = PageID;
                obj.ActionCode = Target_ActionCode;

                obj = (new DAL.Config.tblPageDetail.Methods()).SelectByPageID_ActionCode(obj);
                if (obj.RowNumber > 0)
                {// مقداردهی کنترل ها در صورت وجود رکورد درخواستی
                    txtPageDescription.Text = obj.PageDescription;
                    txtPageTitle.Text = obj.PageTitle;
                    ddlActionCode.SelectedValue = obj.ActionCode.ToString();

                    // انتخاب گروه هایی که به اکشن جاری دسترسی دارند
                    DTO.Config.tblPermissionMap.tblPermissionMap objPermission = new DTO.Config.tblPermissionMap.tblPermissionMap();
                    objPermission.PageID = obj.PageID;
                    objPermission.ActionCode = obj.ActionCode;
                    List<DTO.Config.tblPermissionMap.tblPermissionMap> lstPermission = (new DAL.Config.tblPermissionMap.Methods()).SelectByPageIDActionCode(objPermission);

                    foreach (TreeNode node in tvGroup.Nodes)
                    {
                        CheckedNodes(lstPermission, node);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                byte NewID = SaveData();

                if (NewID != 255)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?ID=" + PageID + "&ActionCode=" + NewID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                byte NewID = SaveData();

                if (NewID != 255)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود

                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?ID=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                byte NewID = SaveData();

                if (NewID != 255)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "PageDetailList.aspx?ID=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// اگر کوئری استرینگ حاوی آیدی جزئیات صفحه باشد، عمل ویرایش
        /// در غیر این صورت عمل ثبت انجام میشود
        /// </summary>
        protected byte SaveData()
        {
            byte Result = 255;

            try
            {
                #region گرفتن اطلاعات
                string PageTitle = txtPageTitle.Text;
                string PageDescription = txtPageDescription.Text;
                byte ActionCode = ddlActionCode.SelectedValue.StringToByte();

                // لیست گروه های کاربری ای که سطح دسترسی دیدن این اکشن را دارند
                List<DTO.Config.tblPermissionMap.tblPermissionMap> _lstSelectedgroup = new List<DTO.Config.tblPermissionMap.tblPermissionMap>();

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    GetAllCheckedNodes(_lstSelectedgroup, node);
                }

                #endregion گرفتن اطلاعات

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(PageTitle, ActionCode, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return Result;
                }

                #region مقداردهی آبجکت جهت ویرایش یا ثبت

                DTO.Config.tblPageDetail.tblPageDetail obj = new DTO.Config.tblPageDetail.tblPageDetail();
                obj.PageTitle = PageTitle;
                obj.PageID = PageID;
                obj.PageDescription = PageDescription;
                obj.ActionCode = ActionCode;
                obj.ActionString = " ";

                obj.SelectedUserGroup = "";
                // گروه های کاربری انتخاب شده
                if (_lstSelectedgroup != null)
                {
                    foreach (DTO.Config.tblPermissionMap.tblPermissionMap Item in _lstSelectedgroup)
                    {
                        obj.SelectedUserGroup += Item.UserGroupID + ",";
                    }
                    if (obj.SelectedUserGroup.Length > 1)
                    {// پاک کردن آخرین کاما
                        obj.SelectedUserGroup = obj.SelectedUserGroup.Substring(0, obj.SelectedUserGroup.Length - 1);
                    }
                }

                #endregion مقداردهی آبجکت جهت ویرایش یا ثبت

                if (Target_ActionCode != 255)
                {// اگر 255 باشد یعنی کوئری استرینگ وجود ندارد
                    #region ویرایش اطلاعات

                    obj.PageID = PageID;

                    if ((new DAL.Config.tblPageDetail.Methods()).Update(obj))
                    {
                        Result = obj.ActionCode.Value;
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    if ((new DAL.Config.tblPageDetail.Methods()).Insert(obj))
                    {
                        Result = obj.ActionCode.Value;
                    }

                    #endregion ثبت اطلاعات
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 255)
                {
                    MessageDialog.ShowMessage("اکشن موردنظر برای صفحه موردنظر قبلا ثبت شده است.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="PageTitle">عنوان صفحه</param>
        /// <param name="Message">متن خطایی که برگشت داده میشود</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(string PageTitle, byte ActionCode, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(PageTitle))
            {
                Result = false;
                Message = "لطفا نام صفحه را وارد نمایید";
            }
            if (ActionCode < 0)
            {
                Result = false;
                Message += "<br/>" + "لطفا اکشن مورد نظر را انتخاب نمایید";
            }

            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/PageDetailList.aspx?ID=" + PageID + "&action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu1.DataSource = LstButton;
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر نال یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblPermissionMap.tblPermissionMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Config.tblPermissionMap.tblPermissionMap> _lstSelectedgroup, TreeNode Node)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Config.tblPermissionMap.tblPermissionMap Obj = new DTO.Config.tblPermissionMap.tblPermissionMap();
                Obj.UserGroupID = Node.Value.StringToInt();
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

    }
}