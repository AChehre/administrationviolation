﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class UserGroup : GeneralPage
    {
        /// <summary>
        /// آیدی گروه کاربری
        /// </summary>
        private int UGID
        {
            get
            {
                if (Request.QueryString.Get("UGID") != null)
                {
                    return Request.QueryString.Get("UGID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (UGID == 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UserGroupList.aspx", true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                Common.BindUserGroup(ddlUserGroup);
                ddlUserGroup.Items[0].Text = "ندارد";
                if (UGID > 0)
                {// مد ویرایش

                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    if (!CheckActionCode(Enums.Page_ActionCode.Edit))
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UserGroupList.aspx", true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    LoadData();
                }
            }
        }

        /// <summary>
        /// نمایش اطلاعات گروه کاربری روی فرم
        /// </summary>
        protected void LoadData()
        {
            try
            {
                DTO.Config.tblUserGroup.tblUserGroup ObjUserGroup = new DTO.Config.tblUserGroup.tblUserGroup();
                ObjUserGroup.GroupID = UGID;
                ObjUserGroup = (new DAL.Config.tblUserGroup.Methods()).SelectByID(ObjUserGroup);

                if (ObjUserGroup.RowNumber > 0)
                {// اگر اطلاعات گروه وجود داشت
                    txtTitle.Text = ObjUserGroup.GroupName;
                    if (ObjUserGroup.ParentID.HasValue)
                    {
                        ddlUserGroup.SelectedValue = ObjUserGroup.ParentID.Value.ToString();
                    }
                }
                else
                {
                    MessageDialog.ShowMessage("اطلاعاتی برای گروه کاربری با آیدی " + UGID + " یافت نشد.", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?UGID=" + NewID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود

                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "UserGroupList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ثبت یا ویرایش اطلاعات
        /// </summary>
        /// <param name="ObjUserGroup">آبجکت از نوع کلاس گروه های کاربری</param>
        protected int SaveData()
        {
            int Result = 0;

            try
            {
                #region گرفتن اطلاعات فرم

                DTO.Config.tblUserGroup.tblUserGroup ObjUserGroup = new DTO.Config.tblUserGroup.tblUserGroup();

                ObjUserGroup.GroupName = txtTitle.Text;
                ObjUserGroup.ParentID = ddlUserGroup.SelectedValue.StringToInt();

                if (ObjUserGroup.ParentID == -1)
                {// اگر گروه پدر انتخاب نشده باشد، مقدارش را نال میگذاریم
                    ObjUserGroup.ParentID = null;
                }

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(ObjUserGroup, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return 0;
                }

                if (UGID > 0)
                {
                    #region ویرایش اطلاعات

                    ObjUserGroup.GroupID = UGID;

                    if ((new DAL.Config.tblUserGroup.Methods()).Update(ObjUserGroup))
                    {// اگر ویرایش موفقیت آمیز بود،آیدی رکورد را برمیگردانیم
                        Result = ObjUserGroup.GroupID.Value;
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    ObjUserGroup.GroupID = 0;

                    Result = (new DAL.Config.tblUserGroup.Methods()).Insert(ObjUserGroup);

                    #endregion ثبت اطلاعات
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="ObjUserGroup">آبجکت از نوع کلاس گروه های کاربری</param>
        /// <param name="Message">متن خطایی که برمیگرداند</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Config.tblUserGroup.tblUserGroup ObjUserGroup, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(ObjUserGroup.GroupName))
            {
                Result = false;
                Message += "لطفا عنوان گروه را وارد نمایید";
            }
            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/UserGroupList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}