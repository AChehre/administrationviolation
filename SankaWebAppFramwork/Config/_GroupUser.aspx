﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true"
    CodeBehind="_GroupUser.aspx.cs" Inherits= "SankaWebAppFramework.Config._GroupUser" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <link href="../css/UCStates.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="_10bm">
        <label class="form-field-label">
            نام :</label>
        <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            نام خانوادگی :</label>
        <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            ایمیل :</label>
        <asp:TextBox ID="txtEMail" CssClass="leftAlign" runat="server"></asp:TextBox>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            نام کاربری :</label>
        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
    </div>
    <div id="PassDiv" runat="server">
        <div class="_10bm">
            <label class="form-field-label">
                کلمه عبور :</label>
            <asp:TextBox ID="txtPass" TextMode="Password" runat="server"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                تکرار کلمه عبور :</label>
            <asp:TextBox ID="txtRePass" TextMode="Password" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="_10bm">
        <label class="form-field-label">
            وضیعت :</label>
        <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbtnIsActive" runat="server">
            <asp:ListItem Value="0" Text="غیرفعال"></asp:ListItem>
            <asp:ListItem Selected="True" Value="1" Text="فعال"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <sanka:states id="States1" runat="server">
    </sanka:states>
    <div class="_10bm">
        <div style="float: right; width: 150px; overflow: hidden">
            <label class="form-field-label">
                گروه های کاربر :</label>
        </div>
        <div style="float: right; overflow: hidden">
            <asp:TreeView Style="padding: 0; margin-top: -15px" ID="tvGroup" runat="server" ShowCheckBoxes="All"
                ShowLines="True">
            </asp:TreeView>
        </div>
    </div>
    <span id='sanka_basic_modal'><a id="lnkUserAreaAccessLevel" data-title="مناطق قابل دسترس"
        data-duplicatetitle="true" data-width="800" data-height="400" runat="server"
        class="btn btn-default basic">ثبت مناطق قابل دسترس</a> </span>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </sanka:SankaDialog>
</asp:Content>
