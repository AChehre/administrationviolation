﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master" AutoEventWireup="true" CodeBehind="UserGroupList.aspx.cs" Inherits= "SankaWebAppFramework.Config.UserGroupList" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <script src="js/UserGroupList.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="_10bm">
        <asp:TreeView ID="tvGroup" runat="server" ShowCheckBoxes="All" ShowLines="True" OnSelectedNodeChanged="tvGroup_SelectedNodeChanged">
        </asp:TreeView>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
