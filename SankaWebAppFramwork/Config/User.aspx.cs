﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Config
{
    public partial class User : GeneralPage
    {
        /// <summary>
        /// آیدی کاربر جهت ویرایش اطلاعات
        /// </summary>
        private long UID
        {
            get
            {
                if (Request.QueryString.Get("UID") != null)
                {
                    return Request.QueryString.Get("UID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (UID == 0)
            {// مد ثبت رکورد جدید
                // مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UsersList.aspx", true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();
                #region ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                if (UserOstanCode.HasValue)
                {
                    States1.OstanCode = UserOstanCode.Value;
                    States1.CurrentUserOstanCode = UserOstanCode.Value;
                }
                //if (UserRegionCode.HasValue)
                //{
                //    States1.RegionCode = UserRegionCode.Value;
                //    States1.CurrentUserRegionCode = UserRegionCode.Value;
                //}
                //if (UserSchoolCode.HasValue)
                //{
                //    States1.SchoolCode = UserSchoolCode.Value;
                //    States1.CurrentUserSchoolCode = UserSchoolCode.Value;
                //}

                States1.UserID = UserID;

                #endregion ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                 // مقدار دهی صفحات
                //this.BindPages(ddlDefaultPageUrl);
           
                try
                {
                    CreateGroupTree();
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }

                //btnUserAreaAccessLevel.Visible = false;

                if (UID > 0)
                {// مد ویرایش

                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    // اگر اکشن کد در کوئری استرینگ نبود، یا آیدی برابر با آیدی کاربر جاری بود، پیغام دهد که 
                    // مجوز ندارد ، چون یک کاربر نمیتواند خودش را ویرایش یا حذف نماید
                    if (!CheckActionCode(Enums.Page_ActionCode.Edit) || UID == UserID)
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UsersList.aspx", true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    LoadData();
                    //  اگر حالت ویرایش باشد، لینک انتخاب مناطق دسترسی را نمایش میدهیم
                    //btnUserAreaAccessLevel.Visible = true;
                }
            }
        }

        /// <summary>
        /// نمایش اطلاعات کاربر روی فرم
        /// </summary>
        protected void LoadData()
        {
            States1.VisibleRegion = false;
            States1.VisibleSchool = false;

            try
            {
                DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView ObjUser = new DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView();
                ObjUser.UserID = UID;
                ObjUser.OstanCode = UserOstanCode;
                ObjUser.RegionCode = UserRegionCode;
                ObjUser.SchoolCode = UserSchoolCode;
                //ObjUser.CurrentUserID = UserID;
                ObjUser = (new DAL.Config.tblUserAttributesMapView.Methods()).SelectByID(ObjUser);

                if (ObjUser.RowNumber > 0)
                {// اگر اطلاعات کاربر وجود داشت
                    txtEMail.Text = ObjUser.EMail;
                    txtFName.Text = ObjUser.FName;
                    txtLName.Text = ObjUser.LName;
                    txtUserName.Text = ObjUser.UserName;
                    rbtnIsActive.SelectedValue = "0";
                    //ddlDefaultPageUrl.SelectedValue = ObjUser.DefaultPageUrl;
                    if (ObjUser.IsActive.Value)
                    {
                        rbtnIsActive.SelectedValue = "1";
                    }

                    // انتخاب گروه هایی که کاربر در آنها عضو است

                    DTO.Config.tblUserGroupMap.tblUserGroupMap obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                    obj.UserID = ObjUser.UserID;
                    List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lstUserGroup = (new DAL.Config.tblUserGroupMap.Methods()).SelectByUserID(obj);

                    foreach (TreeNode node in tvGroup.Nodes)
                    {
                        CheckedNodes(lstUserGroup, node);
                    }

                    // نمایش اطلاعات اضافی کاربر ، مانند استان ، منطقه و غیره
                    States1.OstanCode = 0;
                    //States1.RegionCode = 0;
                    //States1.SchoolCode = 0;

                    if (ObjUser.OstanCode.HasValue)
                    {
                        States1.OstanCode = ObjUser.OstanCode.Value;

                        // آیدی استان را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر استانش را تغییر داده است یا نه
                        hfOstanCode.Value = ObjUser.OstanCode.Value.ToString();
                    }

                    //if (ObjUser.RegionCode.HasValue)
                    //{
                    //    States1.RegionCode = ObjUser.RegionCode.Value;
                    //    // آیدی را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر منطقه اش را تغییر میدهد یا نه
                    //    hfRegionCode.Value = ObjUser.RegionCode.Value.ToString();
                    //}

                    //if (ObjUser.SchoolCode.HasValue)
                    //{
                    //    States1.SchoolCode = ObjUser.SchoolCode.Value;
                    //    // آیدی را در هیدن فیلد ذخیره میکنیم تا در جی کوئری بفهمیم کاربر مدرسه اش را تغییر میدهد یا نه
                    //    hfSchoolCode.Value = ObjUser.SchoolCode.Value.ToString();
                    //}

                    if (ObjUser.PersonnelCode.HasValue)
                    {
                        txtPersonnelCode.Text = ObjUser.PersonnelCode.Value.ToString();
                    }

                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.Attributes.Add("data-href", "UserAreaAccessLevel.aspx?schoolcode=" + ObjUser.SchoolCode + "&regioncode=" + ObjUser.RegionCode + "&ostancode=" + ObjUser.OstanCode + "&uid=" + ObjUser.UserID);
                }
                else
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "UsersList.aspx", true);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?UID=" + NewUserID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewUserID = SaveData();

                if (NewUserID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست کاربران
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "UsersList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ثبت یا ویرایش اطلاعات
        /// </summary>
        /// <returns>آیدی کاربر ذخیره شده</returns>
        protected long SaveData()
        {
            long Result = 0;

            try
            {
                #region گرفتن اطلاعات فرم

                DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();

                ObjUser.EMail = txtEMail.Text;
                ObjUser.FName = txtFName.Text;
                ObjUser.IsActive = rbtnIsActive.SelectedValue.StringToByte() == 0 ? false : true;
                ObjUser.LName = txtLName.Text;
                if (!string.IsNullOrEmpty(txtPass.Text.Trim()))
                {// اگر پسورد خالی نبود، مقدارش ست شود
                    ObjUser.Password = txtPass.Text.getMD5();
                }
                ObjUser.RegisterDate = DateTime.Now;
                ObjUser.UserName = txtUserName.Text;

                DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr = new DTO.Config.tblUserAttributes.tblUserAttributes();

                ObjUserAttr.OstanCode = States1.OstanCode;
                if (ObjUserAttr.OstanCode < 1)
                {
                    ObjUserAttr.OstanCode = null;
                }

                //ObjUserAttr.RegionCode = States1.RegionCode;

                //if (ObjUserAttr.RegionCode < 1)
                //{
                //    ObjUserAttr.RegionCode = null;
                //}

                //ObjUserAttr.SchoolCode = States1.SchoolCode;

                //if (ObjUserAttr.SchoolCode < 1)
                //{
                //    ObjUserAttr.SchoolCode = null;
                //}

                ObjUserAttr.PersonnelCode = txtPersonnelCode.Text.StringToLong();

                // لیست گروه های کاربری ای که کاربر در آنها عضو شده است
                List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    GetAllCheckedNodes(_lstSelectedgroup, 0, node);
                }

                //ObjUser.DefaultPageUrl = ddlDefaultPageUrl.SelectedValue;

                #endregion گرفتن اطلاعات فرم

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(ObjUser, ObjUserAttr, _lstSelectedgroup.Count, txtPass.Text.Trim(), txtRePass.Text.Trim()))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    return 0;
                }

                if (UID > 0)
                {
                    #region ویرایش اطلاعات

                    ObjUser.UserID = UID;

                    using (UserManagment UManageObj = new UserManagment())
                    {// ویرایش
                        Result = UManageObj.EditUser(ObjUser, ObjUserAttr, _lstSelectedgroup);
                    }

                    if (Result > 0)
                    {
                        //  لاگ ویرایش کاربر جدید
                        (new Logger()).InsertUserModificationLog(Result, Logger.RecardModificationFlag.UPDATE);
                    }
                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    ObjUser.UserID = 0;

                    using (UserManagment UManageObj = new UserManagment())
                    {// ثبت

                        if (_lstSelectedgroup.Where(g => g.UserGroupID == Common.PublicGroupID).FirstOrDefault() == null)
                        {// اگر گروه عمومی انتخاب نشده باشد،به صورت پیش فرض هرکاربر که ثبت نام شود، باید عضو گروه عمومی قرار بگیرد
                            _lstSelectedgroup.Add(new DTO.Config.tblUserGroupMap.tblUserGroupMap
                            {
                                UserID = 0,
                                UserGroupID = Common.PublicGroupID
                            });
                        }

                        Result = UManageObj.AddUser(ObjUser, ObjUserAttr, _lstSelectedgroup);
                        if (Result > 0)
                        {
                            //  لاگ ویرایش کاربر جدید
                            (new Logger()).InsertUserModificationLog(Result, Logger.RecardModificationFlag.INSERT);
                        }
                    }

                    #endregion ثبت اطلاعات
                }

                if (Result > 0)
                {
                    // مقدار دهی لینک به مناطق قابل دسترس کاربر
                    lnkUserAreaAccessLevel.Attributes.Add("data-href", "UserAreaAccessLevel.aspx?schoolcode=" + ObjUserAttr.SchoolCode + "&regioncode=" + ObjUserAttr.RegionCode + "&ostancode=" + ObjUserAttr.OstanCode + "&uid=" + ObjUserAttr.UserID);
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="ObjUser">آبجکت از نوع کلاس یوزر</param>
        /// <param name="ObjUserAttr">آبجکت از نوع کلاس ویژگی های یوزر</param>
        /// <param name="SelecteGroupCount">تعداد گروه های انتخاب شده برای کاربر</param>
        /// <param name="Pass">کلمه عبور</param>
        /// <param name="RePass">تکرار کلمه عبور</param>
        /// <param name="Message">متن خطایی که برمیگرداند</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, int SelecteGroupCount, string Pass, string RePass)
        {
            List<string> lstMessage = new List<string>();
            if (string.IsNullOrEmpty(ObjUser.FName))
            {
                lstMessage.Add("لطفا نام را وارد نمایید");
            }

            if (string.IsNullOrEmpty(ObjUser.LName))
            {
                lstMessage.Add("لطفا نام خانوادگی را وارد نمایید");
            }
            if (!string.IsNullOrEmpty(ObjUser.EMail) && !ObjUser.EMail.IsEmailAdress())
            {// در صورتی که آدرس ایمیل وارد کرده باشد، اعتبارسنجی میشود، اگر ناردست باشد، خطا میدهد
                
               lstMessage.Add("لطفا آدرس ایمیل را به صورت صحیح وارد نمایید");
            }
            if (string.IsNullOrEmpty(ObjUser.UserName))
            {
                lstMessage.Add("لطفا نام کاربری را وارد نمایید");
            }
            else
            {// اگر نام کاربری وارد شده باشد، یکتا بودن آنرا در سیستم بررسی میکنیم

                bool CheckUserName = false;

                if (UID > 0)
                {// اگر در حالت ویرایش باشیم
                    CheckUserName = Common.CheckUserName(ObjUser.UserName, UID);
                }
                else
                {// اگر ثبت جدید باشد
                    CheckUserName = Common.CheckUserName(ObjUser.UserName);
                }

                if (!CheckUserName)
                {
                   lstMessage.Add("نام کاربری تکراری می باشد");
                }
            }

            if (UID < 1)
            {// فقط زمانی که در حال ثبت کاربر هستیم کلمه عبور را چک میکنیم، در حالت ویرایش اگر وارد شده باشد چک میکنیم
                if (string.IsNullOrEmpty(Pass))
                {
                   lstMessage.Add("لطفا کلمه عبور را وارد نمایید");
                }
                else
                {
                    if (Pass != RePass)
                    {
                        lstMessage.Add("کلمه عبور و تکرار آن مطابقت ندارد");
                    }
                }
            }
            else //  در حالت ویرایش اگر کلمه عبور وارد شد، چک شود که تطابق داشته باشد
            {
                if (!string.IsNullOrEmpty(Pass))
                {
                    if (Pass != RePass)
                    {
                        lstMessage.Add("کلمه عبور تطابق ندارد");
                    }
                }
            }

            if (SelecteGroupCount < 1)
            {
                lstMessage.Add("حداقل یک گروه کاربری را برای عضویت کاربر انتخاب نمایید.");
            }

            // بررسی اطلاعات اضافی کاربر، مانند استان، منطقه،مدرسه

            if (!ObjUserAttr.OstanCode.HasValue)
            {
                lstMessage.Add("لطفا استان را انتخاب نمایید");
            }
            //if (!ObjUserAttr.RegionCode.HasValue)
            //{
            //   lstMessage.Add("لطفا منطقه را انتخاب نمایید");
            //}
            //if (ObjUserAttr.SchoolID < 1)
            //{
            //    Result = false;
            //    if (!string.IsNullOrEmpty(Message))
            //    {
            //        Message += "<br/>";
            //    }
            //    Message += "لطفا مدرسه را انتخاب نمایید";
            //}

            //if (ddlDefaultPageUrl.SelectedIndex == 0)
            //{
            //    lstMessage.Add("صفحه پیش فرض را انتخاب نمایید .");
            //}
            if (lstMessage.Count > 0)
            {
                MessageDialog.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return false;
            }
            return true;
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر منفی یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, long _UserID, TreeNode Node)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Config.tblUserGroupMap.tblUserGroupMap Obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                Obj.UserGroupID = Node.Value.StringToInt();
                Obj.UserID = _UserID;
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, _UserID, thisNode);
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/UsersList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// پرکردن دراپ دان لیست صفحات
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست صفحه ها را نمایش دهد</param>
        private void BindPages(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Config.tblPages.Methods()).SelectAll();
            ddl.DataTextField = "AspxPage";
            ddl.DataValueField = "AspxPage";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("انتخاب صفحه", "-1"));
        }

    }
}