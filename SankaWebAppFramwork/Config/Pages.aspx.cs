﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace SankaWebAppFramework.Config
{
    public partial class AddPages : GeneralPage
    {
        /// <summary>
        /// آیدی صفحه
        /// </summary>
        protected int PageID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToInt();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (PageID == 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PagesList.aspx", true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                // مقدار دهی اکشن ها
                Common.BindActions(ddlAction);

                lnkActionList.Visible = false;

                if (PageID > 0)
                {// مد ویرایش

                    lnkActionList.Visible = true;

                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    if (!CheckActionCode(Enums.Page_ActionCode.Edit))
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "PagesList.aspx", true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    LoadData();
                }
            }
        }

        /// <summary>
        /// نمایش اطلاعات صفحه ای که ایدیش در کوئری استرینگ هست
        /// </summary>
        protected void LoadData()
        {
            try
            {
                DTO.Config.tblPages.tblPages obj = new DTO.Config.tblPages.tblPages();
                obj.PageID = PageID;

                obj = (new DAL.Config.tblPages.Methods()).SelectByID(obj);
                if (obj.RowNumber > 0)
                {// مقداردهی کنترل ها در صورت وجود صفحه درخواستی
                    txtAspxPage.Text = obj.AspxPage;
                    lnkActionList.HRef = string.Format("PageDetailList.aspx?ID={0}&action={1}", PageID, (byte)SankaWebAppFramework.Enums.Page_ActionCode.View);
                    lnkActionList.Attributes.Add("data-title", "لیست اکشن های صفحه " + obj.AspxPage);
                    ddlAction.SelectedValue = obj.DefaultActionCode.ToString();
                    rbtnPublish.SelectedValue = obj.IsPublished.ToString();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?ID=" + NewID + "&action=" + (byte)Enums.Page_ActionCode.Edit);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)Enums.Page_ActionCode.Insert);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست صفحات
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "PagesList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View);
                }
            }
            catch
            {
                MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
            }
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// اگر کوئری استرینگ حاوی آیدی صفحه باشد، عمل ویرایش
        /// در غیر این صورت عمل ثبت انجام میشود
        /// </summary>
        protected int SaveData()
        {
            int Result = 0;

            try
            {
                string AspxPage = txtAspxPage.Text;
                bool IsPublished = rbtnPublish.SelectedValue.StringToInt() > 0 ? true : false;
                byte DefaultActionCode = ddlAction.SelectedValue.StringToByte();
                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(AspxPage, DefaultActionCode, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return 0;
                }

                #region مقداردهی آبجکت جهت ویرایش یا ثبت

                DTO.Config.tblPages.tblPages obj = new DTO.Config.tblPages.tblPages();
                obj.AspxPage = AspxPage;
                obj.DefaultActionCode = DefaultActionCode;
                obj.IsPublished = IsPublished;

                #endregion مقداردهی آبجکت جهت ویرایش یا ثبت

                if (PageID > 0)
                {
                    #region ویرایش اطلاعات

                    obj.PageID = PageID;

                    if ((new DAL.Config.tblPages.Methods()).Update(obj))
                    {// اگر ویرایش موفقیت آمیز بود، آیدی برگردانده شود
                        Result = obj.PageID.Value;
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    obj.PageID = 0;

                    // اینجا ست میکنیم که
                    // به صورت پیش فرض، گروه عمومی، به اکشن پیش فرض صفحه ایجاد شده دسترسی داشته باشد
                    obj.DefaultGroupID = Common.PublicGroupID;

                    Result = (new DAL.Config.tblPages.Methods()).Insert(obj);

                    #endregion ثبت اطلاعات
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }
                else if (Result == -100)
                {
                    MessageDialog.ShowMessage("این صفحه قبلا ثبت شده است", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="AspxPage">نام صفحه</param>
        /// <param name="IsPublished">وضعیت دسترسی</param>
        /// <param name="DefaultActionCode">کد عملیات پیش فرض</param>
        /// <param name="Message">متن خطایی که برگشت داده میشود</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(string AspxPage, byte DefaultActionCode, out string Message)
        {
            bool Result = true;
            Message = "";

            if (string.IsNullOrEmpty(AspxPage))
            {
                Result = false;
                Message = "لطفا نام صفحه را وارد نمایید";
            }

            if (DefaultActionCode < 0)
            {
                Result = false;
                Message += "<br/>" + "لطفا اکشن پیش فرض را انتخاب نمایید";
            }

            return Result;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/config/PagesList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }
    }
}