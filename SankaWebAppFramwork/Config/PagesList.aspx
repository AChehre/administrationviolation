﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Config/Config.Master"
    AutoEventWireup="true" CodeBehind="PagesList.aspx.cs" Inherits="SankaWebAppFramework.Config.PagesList" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Import Namespace="SankaWebAppFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <fieldset>
            <legend>جستجو</legend>
            <div>
                <div class="_10bm">
                    <label class="form-field-label">
                        نام صفحه :</label>
                    <asp:TextBox ID="txtAspxPage" data-enter-target="btnFilter" placeholder="example : /sample.aspx" MaxLength="50" CssClass="leftAlign" runat="server"></asp:TextBox>
                    <label class="form-field-label _50rmargin">
                        وضعیت دسترسی :</label>
                    <asp:DropDownList ID="ddlPublish" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="_10bm">
                    <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server"
                        Text="فیلتر" />
                    <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server"
                        Text="نمایش همه" />
                </div>
            </div>
        </fieldset>
    </div>
    <div class="grid">
        <SG:SankaGrid ID="gvPages" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="PageID" HeaderText="PageID" Visible="False" />
                <asp:TemplateField HeaderText="نام صفحه">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="AspxPage" data-sankatooltip="مرتب سازی براساس نام صفحه" ToolTip="مرتب سازی براساس نام صفحه"
                            CommandName="ASC" ID="lnkbtnAspxPage" OnCommand="lnkbtnSort_Command" runat="server">نام صفحه</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgAspxPage"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("AspxPage").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="وضعیت دسترسی">
                    <ItemTemplate>
                        <%# Convert.ToBoolean(Eval("IsPublished")) ? Enums.PublishStatus_Label[1] : Enums.PublishStatus_Label[0]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات پیش فرض">
                    <ItemTemplate>
                        <%# Enums.Page_ActionCode_Label[Eval("DefaultActionCode").ToString().StringToInt()]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <span id='sanka_basic_modal'><a href='<%# string.Format("PageDetailList.aspx?ID={0}&action={1}",Eval("PageID").ToString(), (byte)SankaWebAppFramework.Enums.Page_ActionCode.View ) %>'
                            data-title='<%# string.Format("لیست اکشن های صفحه [{0}]",Eval("AspxPage").ToString()) %>'
                            data-duplicatetitle="true" data-width="800" data-height="600" class='basic'>
                            <asp:Image ID="imgSerial" runat="server" ImageUrl="~/images/icon/index_add.png" ToolTip="لیست اکشن ها" />
                        </a></span><a title="ویرایش" href='<%# string.Format("Pages.aspx?ID={0}&action={1}",Eval("PageID").ToString(), (byte)Enums.Page_ActionCode.Edit ) %>'>
                            <asp:Image ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش" />
                        </a><a title="حذف" onclick="return ShowConfirm(this,'در صورت حذف این صفحه، تمامی اکشن های مربوط به آن نیز حذف میشوند، آیا از حذف این صفحه اطمینان دارید؟');"
                            href='<%# string.Format("PagesList.aspx?ID={0}&action={1}", Eval("PageID"),(byte)Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
