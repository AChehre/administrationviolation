﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace SankaWebAppFramework
{
    public partial class News : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews
                {
                    FromDate = DateTime.Parse("2015/01/01"),
                    ToDate = DateTime.Now.AddYears(100),
                    GroupIDs = getUserGroupIds()
                };
                rptNews.DataSource = (new DAL.Admin.tblNews.Methods()).SelectByDate(obj);
                rptNews.DataBind();
            }
        }

        /// <summary>
        /// دریافت لیست گرو هایی که کاربر در آنها عضویت دارد
        /// </summary>
        /// <returns></returns>
        private string getUserGroupIds()
        {
            string strGroupIDs = "";
            var lst = this.UserGroupIDs;
            foreach (var obj in lst)
            {
                strGroupIDs += obj.UserGroupID + ",";
            }
            return strGroupIDs.Remove(strGroupIDs.LastIndexOf(","));
        }
    }
}