﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsDetails.aspx.cs" Inherits="SankaWebAppFramework.NewsDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td style="width: 100px; color: Blue;">
                <strong>عنوان اطلاعیه :</strong>
            </td>
            <td>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100px; color: Blue;">
                <strong>تاریخ درج اطلاعیه :</strong>
            </td>
            <td>
                <asp:Label ID="lblWriteDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100px; color: Blue; vertical-align: top;">
                <strong>متن کامل اطلاعیه : </strong>
            </td>
            <td>
                <asp:Literal runat="server" ID="ltrNewsDetails" />
            </td>
        </tr>
    </table>
    <a style="float: left; color: #0000ff" href="News.aspx"><strong>بازگشت</strong></a>
</asp:Content>
