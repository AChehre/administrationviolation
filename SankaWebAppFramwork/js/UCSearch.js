﻿
$(document).ready(function () {
    // مقدار دهی تکست باکس های کنترل از صفحه پدر
    try {
        var FName = $("[id$='txtFName']");
        var LName = $("[id$='txtLName']");
        var PersonnelCode = $("[id$='txtPersonnelCode']");
        if ($("[id$='hfIsFirstLoad']").val() !== "1") {
            var ParentFirstNameInputFieldID = $("[id$='hfParentFirstNameInputFieldID']").val();
            var ParentLastNameInputFieldID = $("[id$='hfParentLastNameInputFieldID']").val();
            var ParentPersonnelCodeInputFieldID = $("[id$='hfParentPersonnelCodeInputFieldID']").val();
            $(FName).val(window.parent.$("[id$='" + ParentFirstNameInputFieldID + "']").val());
            $(LName).val(window.parent.$("[id$='" + ParentLastNameInputFieldID + "']").val());
            $(PersonnelCode).val(window.parent.$("[id$='" + ParentPersonnelCodeInputFieldID + "']").val());
            //  برای اینکه فقط اولین بار، اطلاعات پیش فرض رو از صفحه پدر لود کند، فلگ میزاریم
            $("[id$='hfIsFirstLoad']").val("1");
        }
    }
    catch (ex) {

    }
});
// جستجو براساس نام
function SearchByName() {
    var FirstNameMinCharLengh = $("[id$='hfFirstNameMinCharLengh']").val();
    var LastNameMinCharLengh = $("[id$='hfLastNameMinCharLengh']").val();
    var Name = $("[id$='txtFName']");
    var Family = $("[id$='txtLName']")
    var DataValid = true;

    //  بررسی طول کاراکتر های نام
    if (FirstNameMinCharLengh > $(Name).val().length) {
        $(Name).addClass("invalid-item");
        $(Name).attr('data-invalid', 'true');
        $(Name).on("keydown", function () {
            InvalidItemKeyDown(this);
        });
        DataValid = false;

        if (!DataValid) {
            ShowPopupWindow("لطفا در قسمت نام حداقل " + FirstNameMinCharLengh + " کاراکتر وارد نمایید", "اخطار");
            return false;
        }
    }
    // بررسی طول کاراکتر های نام خانوادگی
    if (LastNameMinCharLengh > $(Family).val().length) {
        $(Family).addClass("invalid-item");
        $(Family).attr('data-invalid', 'true');
        $(Family).on("keydown", function () {
            InvalidItemKeyDown(this);
        });
        DataValid = false;

        if (!DataValid) {
            ShowPopupWindow("لطفا در قسمت نام خانوادگی حداقل " + LastNameMinCharLengh + " کاراکتر وارد نمایید", "اخطار");
            return false;
        }
    }

    //lockScreen();
}
// جستجو براساس کد پرسنلی
function SearchByPCode() {
    var PersonnelCodeMinCharLengh = $("[id$='hfPersonnelCodeMinCharLengh']").val();
    var Code = $("[id$='txtPersonnelCode']");
    var DataValid = true;
    // بررسی طول کاراکتر های کد پرسنلی
    if (PersonnelCodeMinCharLengh > $(Code).val().length) {
        $(Code).addClass("invalid-item");
        $(Code).attr('data-invalid', 'true');
        $(Code).on("keydown", function () {
            InvalidItemKeyDown(this);
        });
        DataValid = false;

        if (!DataValid) {
            ShowPopupWindow("لطفا در قسمت کد پرسنلی حداقل " + PersonnelCodeMinCharLengh + " کاراکتر وارد نمایید", "اخطار");
            return false;
        }
    }

    lockScreen();
}
// انتخاب یک رکورد
function SelectRecord(Record) {

    try {
        // ریختن نتیجه در صفحه پدر
        var ParentFirstNameOutputFieldID = $("[id$='hfParentFirstNameOutputFieldID']").val();
        var ParentLastNameOutputFieldID = $("[id$='hfParentLastNameOutputFieldID']").val();
        var ParentPersonnelCodeOutputFieldID = $("[id$='hfParentPersonnelCodeOutputFieldID']").val();
        window.parent.$("[id$='" + ParentFirstNameOutputFieldID + "']").val($(Record).attr("data-fname"));
        window.parent.$("[id$='" + ParentLastNameOutputFieldID + "']").val($(Record).attr("data-lname"));
        window.parent.$("[id$='" + ParentPersonnelCodeOutputFieldID + "']").val($(Record).attr("data-pcode"));

        window.parent.$("[id$='" + $("[id$='hfParentFatherNameOutputFieldID']").val() + "']").val($(Record).attr("data-fathername"));
        window.parent.$("[id$='" + $("[id$='hfParentOstanCodeOutputFieldID']").val() + "']").val($(Record).attr("data-ostan"));
        window.parent.$("[id$='" + $("[id$='hfParentRegionCodeOutputFieldID']").val() + "']").val($(Record).attr("data-region"));
        window.parent.$("[id$='" + $("[id$='hfParentSchoolCodeOutputFieldID']").val() + "']").val($(Record).attr("data-school"));
        //بستن پنجره
        return window.parent.$('#modal_content').dialog('close');
    }
    catch (ex) {

    }
}