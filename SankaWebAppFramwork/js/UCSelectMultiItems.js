﻿function SelecteItemClick(element) {

    if ($(element).attr("data-islock") == "True") {
        return;
    }

    if ($(element).attr("data-status") == "False") {
        $(element).attr("data-status", "True")
        $("#LeftPanel").append($(element).detach());
    }
    else {
        $(element).attr("data-status", "False")
        $("#RightPanel").append($(element).detach());
    }
    $("#hfLeftControlsID").val("");
    var newVal = "";
    var arr = $("#LeftPanel").find("[data-status=True]");
    if (arr.length > 0) {
        arr.each(function () {
            newVal = newVal + $(this).attr("data-id") + ",";
        });
        newVal = newVal.substr(0, newVal.length - 1)
    }
    $("#hfLeftControlsID").val(newVal);

    //var tooltips = $(element).tooltip();
    //tooltips.tooltip("close");
}