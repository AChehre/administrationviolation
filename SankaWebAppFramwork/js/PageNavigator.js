﻿
jQuery(document).ready(function () {
    showDefault();
    //** کلیک دکمه های سمت راست آیفریم
    $(".btn-Navigator").click(function () {
        Navigate(this);
    });
});

//در هنگام لود صفحه به صورت پیش فرض صفحه ای که فعال است را نمایش میدهیم 
function showDefault() {
    var Target = $(".btn-Navigator[data-isactive='True']:first");
    if (Target.length > 0) {
        Navigate(Target);
    }
}

//** نمایش محتوای آدرس موردنظر در آیفرم
function Navigate(Target) {
    var TargetUrl = $(Target).attr("data-href");
    if (TargetUrl != $("#iframePageNavigator").attr("src")) {
        // نمایش پنل لودینگ هنگام تغییر آدرس آیفریم
        $(".pn-loading").show();
    }
    $("#iframePageNavigator").attr("src", TargetUrl);
    loadCompleteIframe($(Target).attr("data-height") + "px");
    $(".btn-Navigator").removeClass("btn-pn-active");
    $(".btn-Navigator").attr("data-isactive", "False");
    $(Target).addClass("btn-pn-active");
    $(Target).attr("data-isactive", "True");
    //نمایش یا عدم نمایش دکمه قبلی
    var FirstIndex = parseInt($(".btn-Navigator:first").attr("data-index"));
    if (FirstIndex == parseInt($(Target).attr("data-index"))) {
        $("#btnPrev").hide();
    }
    else {
        $("#btnPrev").show();
    }
    //نمایش یا عدم نمایش دکمه بعدی
    var lastIndex = parseInt($(".btn-Navigator:last").attr("data-index"));
    if (lastIndex == parseInt($(Target).attr("data-index"))) {
        $("#btnNext").hide();
    }
    else {
        $("#btnNext").show();
    }
}
//** کلیک دکمه های قبلی و بعدی
function Prev_NextClick(Status) {

    var btnActive = $(".btn-Navigator[data-isactive='True']:first");
    if (btnActive.length < 1) {
        return;
    }

    var CurrentIndex = parseInt($(btnActive).attr("data-index"));

    if (Status == -1) {
        // کلیک دکمه قبلی
        CurrentIndex--;
    } else if (Status == 1) {
        // کلیک دکمه بعدی
        CurrentIndex++;
    }

    var btnTarget = $(".btn-Navigator[data-index='" + CurrentIndex + "']:first");
    if (btnTarget.length < 1) {
        return;
    }

    Navigate(btnTarget);
}
// پنهان کردن پنل لودینگ پس از اتمام لود محتوای آیفریم
function loadCompleteIframe(height) {
    var iframe = document.getElementById("iframePageNavigator");

    if (navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) {
        iframe.onreadystatechange = function () {
            if (iframe.readyState == "complete") {
                $(".pn-loading").hide();
                $('#iframePageNavigator').css("height", height);
            }
        };
    } else {
        iframe.onload = function () {
            $(".pn-loading").hide();
            $('#iframePageNavigator').css("height", height);
        };
    }

}
