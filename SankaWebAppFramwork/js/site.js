﻿
$(document).ready(function () {
    $(".MustBeNumber").keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
            e.preventDefault();         // Prevent character input
        }
        else {
            var n = e.keyCode;
            if (!((n == 8)              // backspace
                    || (n == 46)                // delete
                    || (n == 9)                // tab
                    || (n == 13)                // enter
                    || (n >= 35 && n <= 40)     // arrow keys/home/end
                    || (n >= 48 && n <= 57)     // numbers on keyboard
                    || (n >= 96 && n <= 105)))   // number on keypad
            {
                e.preventDefault();     // Prevent character input
            }
        }
    });
    //*** <!-- initialise Superfish -->
    try {
        jQuery('ul.sf-menu').superfish();
    }
    catch (ex) {

    }
    //*** <!-- initialise accordion -->
    $(".create_accordion").accordion({
        collapsible: true
    });
    //*** initialise tooltip
    $(document).tooltip({
        items: "[data-sankatooltip]",
        content: function () {
            var element = $(this);
            return element.attr("data-sankatooltip");
        },
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
            .addClass("arrow")
            .addClass(feedback.vertical)
            .addClass(feedback.horizontal)
            .appendTo(this);
            }
        }
    });
    //** initialise required 
    $(".required-star").remove();
    $("<span class='required-star'>*</span>").insertAfter("[data-required=true]");
    //** initialise invalid 
    $("[data-invalid=true]").addClass("invalid-item");
    $("[data-invalid=true]").keydown(function () {
        InvalidItemKeyDown(this);
    });
    ////////////////////// NOTIFICATION CLOSE BUTTON
    $(function () {
        $('.notification-area div.close').append('<span class="close-button" data-icon="s"></span>');
        $('.notification-area div .close-button').click(function () {
            $(this).parent().fadeOut(400);
        });
    });

    // ذخیره آدرس برگشت برای تگهای a
    $("[data-save-return-url=true]").click(function () {

        var stack = new Array();
        try
        {
            stack = getCookie("returnurl").split("@");
        }
        catch (ex) {

        }
        stack.push(window.location.href);

        setCookie("returnurl", stack.join("@"), 1);
        window.location.href = $(this).attr("href");
        return false;
    });
    //**  هدایت اینتر در تکست باکس ها جهت کلیک یک آیتم خاص
    $("[data-enter-target]").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }

            var Selector = "[id$='" + $(this).attr("data-enter-target") + "']";

            $(Selector).click();
            return false;
        }
    });
    //**
    $(".prevententer input[type=text]").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
            return false;
        }
    });
});
//** نمایش یک متن دلخواه به صورت پوپاپ ویندو
function ShowPopupWindow(message, title) {
    $(function () {
        $("body").append("<div id='notification_dialog_auto_create'></div>");
        $("#notification_dialog_auto_create").html(message);
        $("#notification_dialog_auto_create").dialog({
            title: title,
            buttons: {
                'بستن': function () {
                    $(this).dialog('close');
                    $("#notification_dialog_auto_create").remove();
                    $(".a-m-container .item-container").attr("data-clicked","0");
                }
            },
            modal: true
        });
    });
};
//** نمایش پنجره گرفتن تاییدیه
function ShowConfirm(Item, message, title, YesLable, NoLable) {

    if ($(Item).attr("data-confirmed") == "1") {
        return true;
    }
    message = (message !== undefined && message !== "") ? message : "آیا از حذف این مورد اطمینان دارید ؟";
    title = (title !== undefined && title !== "") ? title : "اخطار";
    //    YesLable = (YesLable !== undefined && YesLable !== "") ? YesLable : "بله";
    //    NoLable = (NoLable !== undefined && NoLable !== "") ? NoLable : "خیر";

    $("body").append("<div id='Confirm_dialog_auto_create'></div>");
    $("#Confirm_dialog_auto_create").html(message);
    $("#Confirm_dialog_auto_create").dialog({
        title: title,
        buttons: {
            'خیر': function () {
                $(Item).attr("data-confirmed", "0");
                $(this).dialog('close');
                $("#Confirm_dialog_auto_create").remove();
                $(".a-m-container .item-container").attr("data-clicked", "0");
            },
            'بله': function () {
                $(Item).attr("data-confirmed", "1");
                var href = $(Item).attr("href");
                if (href !== undefined && href !== "") {
                    // اگر لینک بود
                    window.location = href;
                }
                else {
                    // اگر باتن باشد
                    $(Item).trigger("click");
                }
                $(this).dialog('close');
                $("#Confirm_dialog_auto_create").remove();
            }
        },
        modal: true
    });
    return false;
};
//** بررسی مقدار داشتن فیلدهای اجباری
function ValidateRequiredField(ParentContainer) {

    var DataValid = true;
    var Selector = "[data-required=true]";
    if (ParentContainer !== undefined) {
        Selector = ParentContainer + " [data-required=true]";
    }

    $(Selector).each(function () {
        if ($(this).val() == "") {
            $(this).addClass("invalid-item");
            $(this).attr('data-invalid', 'true');
            $(this).on("keydown", function () {
                InvalidItemKeyDown(this);
            });
            DataValid = false;
        }
    });

    if (!DataValid) {
        ShowPopupWindow("لطفا فیلد های اجباری را پر نمایید", "اخطار");
        return false;
    }

    return true;
}
//** اعمالی که باید هنگام تایپ در فیلدهای نامعتبر، انجام شود
function InvalidItemKeyDown(item) {
    $(item).removeAttr("data-invalid");
    $(item).removeClass("invalid-item");
}
//** به دست آوردن کوکی
function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}
//** ایجاد کوکی
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString() + "; path=/;");
    document.cookie = c_name + "=" + c_value;
}
//** حذف کوکی
function RemoveCookie(name) {
    var username = getCookie(name);
    if (username != null && username != "") {
        document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT' + '; path=/;';

    }
}
//** خواندن آدرس بازگشت از کوکی و فرستادن کاربر به آدرس موردنظر
function GoBackUrl(Item) {
    try
    {
        if ($(Item).attr("data-clicked") != "1") {
            $(Item).attr("data-clicked", "1");

            var stack = new Array();

            try {
                stack = getCookie("returnurl").split("@");
            }
            catch (ex) {

            }

            var ReturnURL = "#";

            try
            {
                ReturnURL = stack.pop();

                ReturnURL = (ReturnURL !== undefined && ReturnURL !== "") ? ReturnURL : $(Item).attr("href");

                // اگر نه در پشته آدرسی بود و نه خود تگ آدرس مقصد داشت، به جایی نرود
                ReturnURL = (ReturnURL !== undefined && ReturnURL !== "") ? ReturnURL : "#";

                setCookie("returnurl", stack.join("@"), 1);
            }
            catch (ex) {
            }

            window.location.href = ReturnURL;
        }
    }
    catch (ex) {

    }
    return false;
}

//**  چک کردن انتخاب فایل در فایل آپلود ها
function CheckAttachment() {

    var Status = ValidateRequiredField('#divFileAttachmentField');
    if (!Status) {
        return Status;
    }
    var txtUpload = document.getElementById("MyFileUpload");
    if (txtUpload.value.length == 0) {
        ShowPopupWindow("هیچ سندی انتخاب نشده است", "اخطار");
        return false;
    }
    lockScreen();
}
