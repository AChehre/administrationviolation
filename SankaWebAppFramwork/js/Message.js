﻿
function ShowPopup(message, title, TargetID, OnCloseFunction, Arg) {
    $(function () {
        $("#" + TargetID).html(message);
        $("#" + TargetID).dialog({
            title: title,
            buttons: {
                'بستن': function () {
                    $(this).dialog('close');
                }
            },
            modal: true,
            close: function (event, ui) {
                if (OnCloseFunction !== undefined)
                    OnCloseFunction(Arg);
            }
        });
    });
};

function Test() {
    alert("تست");
}

function ShowPopupForConfirm(message, title, TargetID, YesFunction, Arg) {
    $(function () {
        $("#" + TargetID).html(message);
        $("#" + TargetID).dialog({
            title: title,
            buttons: {
                'بلی': function () {
                    if (YesFunction !== undefined) {
                        YesFunction();
                    }
                    $(this).dialog('close');
                },
                'خیر': function () {
                    $(this).dialog('close');
                }
            },
            modal: true,
            close: function (event, ui) {

            }
        });
    });
};