﻿
$(document).ready(function () {
    // min length
    var Nameminlength = $("[id$='txtSearchFullName']").attr("data-minlength");
    var PersonalCodeminlength = $("[id$='txtSearchPersonnelCode']").attr("data-minlength");
    // delay
    var Namedelay = $("[id$='txtSearchFullName']").attr("data-delay");
    var PersonalCodedelay = $("[id$='txtSearchPersonnelCode']").attr("data-delay");
    //** جستجو براساس نام و نام خانوادگی
    $("[id$='txtSearchFullName']").autocomplete({
        source: function (request, response) {
            $(".pnl-waiting").show();
            $.ajax({
                url: "/webmethods/Public.asmx/Search",
                data: "{'SearchValue':'" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $(".pnl-waiting").hide();
                    MapResult(response, data, "name");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $(".pnl-waiting").hide();
                }
            });
        }, select: function (event, ui) {
            SelectSearchItem(event, ui);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        },
        width: 100,
        minLength: Nameminlength,
        autoFocus: true,
        scroll: true,
        highlight: true,
        appendTo: "#UserSearchContainer",
        delay: Namedelay
    });
    //** جستجو براساس کد پرسنلی
    $("[id$='txtSearchPersonnelCode']").autocomplete({
        source: function (request, response) {
            $(".pnl-waiting").show();
            $.ajax({
                url: "/webmethods/Public.asmx/Search",
                data: "{'SearchValue':'" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $(".pnl-waiting").hide();
                    MapResult(response, data, "pcode");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $(".pnl-waiting").hide();
                }
            });
        }, select: function (event, ui) {
            SelectSearchItem(event, ui);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        },
        width: 100,
        minLength: PersonalCodeminlength,
        autoFocus: true,
        scroll: true,
        highlight: true,
        appendTo: "#UserSearchContainer",
        delay: PersonalCodedelay
    });
});
//** ست کردن فیلدهای لیست جستجو به فیلدهای متناظر منو
function MapResult(response, data, type) {
    if (type == "name") {
        // اگر جستجو براساس نام بود،نام کامل در منو نمایش داده شود
        response($.map(data.d, function (item) {
            return {
                label: item.FullName,
                id: item.PersonnelID,
                PersonnelCode: item.PersonnelCode,
                FullName: item.FullName,
                FirstName: item.FirstName,
                LastName: item.LastName,
                FatherName: item.FatherName
            };
        }));
    }
    else if (type == "pcode") {
        // اگر جستجو براساس کد پرسنلی بود، کد پرسنلی در منو نمایش داده شود
        response($.map(data.d, function (item) {
            return {
                label: item.PersonnelCode,
                id: item.PersonnelID,
                PersonnelCode: item.PersonnelCode,
                FullName: item.FullName,
                FirstName: item.FirstName,
                LastName: item.LastName,
                FatherName: item.FatherName
            };
        }));
    }
}
//** عملیات موردنیاز هنگام انتخاب یک آیتم
function SelectSearchItem(event, ui) {
    $("[id$='txtSearchPersonnelCode']").val(ui.item.PersonnelCode);
    $("[id$='txtPersonnelCode']").val(ui.item.PersonnelCode);
    $("[id$='txtSearchFullName']").val(ui.item.FullName);
    $("[id$='txtFName']").val(ui.item.FirstName);
    $("[id$='txtLName']").val(ui.item.LastName);
    $("[id$='txtFatherName']").val(ui.item.FatherName);
    $("#UserSearchContainer").dialog('close');
    return false;
}
//** نمایش پنجره جستجوی کاربران
function ShowSearchWindow(title) {
    $("#UserSearchContainer").dialog({
        title: title,
        width: 430,
        buttons: {
            'بستن': function () {
                $(this).dialog('close');
            }
        },
        modal: true
    });

    $("#UserSearchContainer").parents(".ui-dialog").css("overflow", "visible");
}