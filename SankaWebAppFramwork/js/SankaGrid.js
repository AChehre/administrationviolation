﻿function ChangePage(pageNumber) {
    var hf = document.getElementById("hfPageNumber");
    hf.value = pageNumber;
    document.getElementById("hfPageChanged").value = pageNumber;
    document.forms[0].submit();
}

function setPageSize(pageSize) {
    var hf = document.getElementById("hfPageSize");
    hf.value = pageSize;
    ChangePage(1);
}