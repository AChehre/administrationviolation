﻿
function SelectMenu(item, TargetID) {
    if ($(item).attr("data-clicked") != "1") {
        $(item).attr("data-clicked", "1");
        var href = $("[id$='" + TargetID + "']").attr("href");
        if (href !== undefined && href !== "") {
            window.location = href;
        }
        else {
            $("[id$='" + TargetID + "']").trigger("click");
        }
    }
}