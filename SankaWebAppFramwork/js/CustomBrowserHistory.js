﻿
$(document).ready(function () {
    // ذخیره آدرس برگشت برای تگهای a
    $("[data-save-return-url=true]").click(function () {

        var stack = new Array();
        try {
            stack = getCookie("returnurl").split("@");
        }
        catch (ex) {

        }
        stack.push(window.location.href);

        setCookie("returnurl", stack.join("@"), 1);
        window.location.href = $(this).attr("href");
        return false;
    });
});
//** به دست آوردن کوکی
function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}
//** ایجاد کوکی
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString() + "; path=/;");
    document.cookie = c_name + "=" + c_value;
}
//** حذف کوکی
function RemoveCookie(name) {
    var username = getCookie(name);
    if (username != null && username != "") {
        document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT' + '; path=/;';

    }
}
//** خواندن آدرس بازگشت از کوکی و فرستادن کاربر به آدرس موردنظر
function GoBackUrl(Item) {
    try {
        if ($(Item).attr("data-clicked") != "1") {
            $(Item).attr("data-clicked", "1");

            var stack = new Array();

            try {
                stack = getCookie("returnurl").split("@");
            }
            catch (ex) {

            }

            var ReturnURL = "#";

            try {
                ReturnURL = stack.pop();

                ReturnURL = (ReturnURL !== undefined && ReturnURL !== "") ? ReturnURL : $(Item).attr("href");

                // اگر نه در پشته آدرسی بود و نه خود تگ آدرس مقصد داشت، به جایی نرود
                ReturnURL = (ReturnURL !== undefined && ReturnURL !== "") ? ReturnURL : "#";

                setCookie("returnurl", stack.join("@"), 1);
            }
            catch (ex) {
            }

            window.location.href = ReturnURL;
        }
    }
    catch (ex) {

    }
    return false;
}