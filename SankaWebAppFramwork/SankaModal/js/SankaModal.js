﻿
$(document).ready(function () {
    // Load dialog on click
    $('#sanka_basic_modal .basic').click(function (e) {

        try {
            if ($(this).attr("data-islock") == "True") {
                return false;
            }
            var Title = $(this).attr("data-title") ? $(this).attr("data-title") : "";
            var width = $(this).attr("data-width") ? $(this).attr("data-width") : 400;
            var height = $(this).attr("data-height") ? $(this).attr("data-height") : 300;
            var duplicatetitle = $(this).attr("data-duplicatetitle") ? $(this).attr("data-duplicatetitle") : "false";
            duplicatetitleheight = 30;
            if (Title.length > 0) {
                if (duplicatetitle.length > 0 && duplicatetitle == "true") {
                    $('#modal_content h3').html(Title);
                    duplicatetitleheight = 0;
                }
            }

            var src = e.currentTarget.href;

            if (src == undefined || src == "") {
                src = $(this).attr("data-href");
            }

            $('#sanka_iframe')[0].src = src;
            var dialogheight = (height - (duplicatetitleheight));
            $('#modal_content').dialog({
                title: Title,
                width: width,
                height: dialogheight,
                modal: true,
                close: function () {
                    // در صورت نیاز، پنجره والد را رفرش میکند
                    RefreshParent();
                    $('#sanka_iframe')[0].src = "";
                    $(".a-m-container .item-container").attr("data-clicked", "0");
                }
            });

            if (width.length > 0) {
                $('#sanka_iframe').css("width", (width - 30) + "px");
            }
            if (height.length > 0) {
                $('#sanka_iframe').css("height", (height - 95) + "px");
            }

            return false;
        }
        catch (ex) {
        }
        return false;
    });
});

// اگر در آیفریم تغییراتی ایجاد شده باشد که نیاز باشد بعد از بستن آن ، پنجره والد رفرش شود، این تابع فراخوانی میشود
function RefreshParent() {
    try {
        var childiFrame = document.getElementById("sanka_iframe");

        var innerDoc = childiFrame.contentDocument || childiFrame.contentWindow.document;
        // بدست آوردن کنترل درون آیفریم
        var ChildiFrameControl = innerDoc.getElementById("hfParentReloadFlag");
        // گرفتن مقدار کنترل
        var value = ChildiFrameControl.value;
        if (value === "1") {
            var url = window.location.href;

            window.location = url + "&u=" + Math.floor((Math.random() * 100) + 1);
        }
    }
    catch (ex) 
    {
    }
}