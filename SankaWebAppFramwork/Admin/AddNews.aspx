﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="AddNews.aspx.cs" Inherits="SankaWebAppFramework.Admin.AddNews" ValidateRequest="false"
    EnableEventValidation="true" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register TagPrefix="sanka" TagName="Editor" Src="~/sankaeditor/UCSankaEditor.ascx" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/sankaeditor/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/sankaeditor/ckeditor/adapters/jquery.js" type="text/javascript"></script>
    <script src="/sankaeditor/scripts/UCSankaEditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc2:SankaDialog>
    <fieldset>
        <legend>
            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></legend>
        <table>
            <tr>
                <td>
                    عنوان اطلاعیه :
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" ValidationGroup="grpShekayat" placeholder="عنوان اطلاعیه را درج نمایید ."
                        Width="500px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="درج عنوان اطلاعیه الزامی است" SetFocusOnError="True" ValidationGroup="grpShekayat">*</asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hfNewsID" runat="server" Value="0" />
                </td>
            </tr>
            <tr id="trWriteDate" runat="server">
                <td>
                    تاریخ درج اطلاعیه :
                </td>
                <td>
                    <uc1:UCDatePicker ID="txtWriteDate" runat="server" />
                </td>
            </tr>
            <tr id="tr2" runat="server">
                <td>
                    تاریخ انقضاء :
                </td>
                <td>
                    <uc1:UCDatePicker ID="txtExpDate" runat="server" />
                </td>
            </tr>
            <tr id="Tr1" runat="server">
                <td>
                    توضیح کوتاه :
                </td>
                <td>
                    <asp:TextBox ID="txtShortDescription" runat="server" Columns="20" placeholder="عنوان اطلاعیه را درج نمایید ."
                        Rows="3" TextMode="MultiLine" ValidationGroup="grpShekayat" Width="500px" MaxLength="256"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvShortDescription" runat="server" ControlToValidate="txtShortDescription"
                        ErrorMessage="درج شرح کوتاه برای اطلاعیه الزامی است" SetFocusOnError="True" ValidationGroup="grpShekayat">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    محتوای اطلاعیه :
                </td>
                <td>
                    <sanka:Editor ID="ckDescription" runat="server" defaultlanguage="fa"></sanka:Editor>
                </td>
            </tr>
            <tr>
                <td>
                    تصویر اطلاعیه :
                </td>
                <td>
                    <asp:FileUpload ID="MyFileUpload" runat="server" />
                    <asp:HiddenField ID="hfPrevFileUrl" runat="server" />
                    <asp:Label runat="server" ID="lblMaxFileSize" />
                </td>
            </tr>
            <tr>
                <td>
                    انتشار اطلاعیه :
                </td>
                <td>
                    <asp:CheckBox ID="chkPublished" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    دسترسی گروه ها :
                </td>
                <td>
                    <asp:TreeView Style="padding: 0; margin-top: -15px" ID="tvGroup" runat="server" ShowCheckBoxes="All"
                        ShowLines="True">
                    </asp:TreeView>
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Content>
