﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="NewsList.aspx.cs" Inherits="SankaWebAppFramework.Admin.NewsList" %>

<%@ Register Src="../controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Src="../sankaeditor/UCSankaEditor.ascx" TagName="UCSankaEditor" TagPrefix="uc3" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Board.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <legend>&nbsp;جستجوی خبر&nbsp;</legend>
        <div class="_10bm">
            <label class="form-field-label">
                از تاریخ&nbsp; :</label>
            <uc1:UCDatePicker ID="txtFromDate" runat="server" />
            <label class="form-field-label _50rmargin">
                تا تاریخ&nbsp; :</label>
            <uc1:UCDatePicker ID="txtToDate" runat="server" />
            <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/images/icon/Search.png"
                Style="cursor: pointer" OnClick="btnSearch_Click" class="cp_vm" />
        </div>
    </fieldset>
    <div class="grid">
        <cc1:SankaGrid ID="gvNews" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ReadOnly="True" SortExpression="RowNumber"
                    Visible="True">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField HeaderText="عنوان اطلاعیه" DataField="Title"></asp:BoundField>
                <asp:TemplateField HeaderText="تاریج درج">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("WriteDate")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="بروزرسانی">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("ModifyDate"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="انتشار">
                    <ItemTemplate>
                        <asp:Image ID="imgStatus" runat="server" ImageUrl='<%# Convert.ToBoolean(Eval("Published")) ? "~/images/icon/online.png" : "~/images/icon/offline.png"  %>'
                            ToolTip='<%# Convert.ToBoolean(Eval("Published")) ? "منتشر شده" : "منتشر نشده"  %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="لینک مشاهده">
                    <ItemTemplate>
                        <a href='<%# string.Format("/NewsDetails.aspx?ID={0}",Eval("ID")) %>' target="_blank">
                            لینک اطلاعیه</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="انقضاء">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("ExpirationDate"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش"
                            NavigateUrl='<%# string.Format("/Admin/AddNews.aspx?ID={0}", Eval("ID")) %>' />
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/icon/delete.png"
                            OnClientClick="return ShowConfirm(this,'آیا از حذف این اطلاعیه اطمینان دارید ؟')" ToolTip="حذف" CommandArgument='<%# Eval("ID") %>'
                            OnCommand="btnDelete_Command" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <span>رکوردی جهت نمایش موجود نیست .</span>
            </EmptyDataTemplate>
        </cc1:SankaGrid>
    </div>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="SankaDialog1">
    </sanka:SankaDialog>
</asp:Content>
