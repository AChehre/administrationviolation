﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Admin
{
    public partial class SearchBoardMember : GeneralPage
    {
        /// <summary>
        /// آیدی هیات
        /// </summary>
        private long BoardID
        {
            get
            {
                if (Request.QueryString.Get("BoardID") != null)
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        #region برای صفحه بندی گریدها

        /// <summary>
        /// اندازه صفحات گرید
        /// </summary>
        public int PageSize
        {
            get
            {
                int Size = hfPageSize.Value.StringToInt();
                return Size == 0 ? 10 : Size;
            }
            set
            {
                hfPageSize.Value = value.ToString();
            }
        }

        /// <summary>
        /// تعداد کل سطرهای پیدا شده در گرید
        /// </summary>
        private long TotalRow
        {
            get
            {
                return hfTotalRow.Value.StringToInt();
            }
            set
            {
                hfTotalRow.Value = value.ToString();
            }
        }

        /// <summary>
        /// شماره صفحه جاری گرید
        /// </summary>
        private int PageNumber
        {
            get
            {
                return hfPageNumber.Value.StringToInt();
            }
            set
            {
                hfPageNumber.Value = value.ToString();
            }
        }

        /// <summary>
        ///است کرده تغییر گرید صفحه شماره آیا که میکند مشخص 
        ///<para>است رفته گرید از دیگری صفحه به کاربر عبارتی به </para>
        ///</summary>
        protected bool GridViewPageChanged
        {
            get
            {
                return hfPageChanged.Value.StringToInt() > 0;
            }
            set
            {
                hfPageChanged.Value = (value) ? "1" : "0";
            }
        }

        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        #endregion برای صفحه بندی گریدها

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFromDate.Empty = true;
                txtToDate.Empty = true;
            }
        }

        /// <summary>
        /// کلیک دکمه جستجو
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        /// <summary>
        /// جستجوی اعضای هیات
        /// </summary>
        private void Search()
        {
            try
            {
                DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView obj = new DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView();

                #region از تاریخ
                // مقدار پیش فرض
                obj.FromDate = null;
                if (txtFromDate.DateIsValid)
                {// اگر انتخاب شده بود مقدار دهی شود
                    obj.FromDate = txtFromDate.DateMiladi;
                }

                #endregion از تاریخ

                #region تا تاریخ
                // مقدار پیش فرض
                obj.ToDate = null;
                if (txtToDate.DateIsValid)
                {// اگر انتخاب شده بود مقدار دهی شود
                    obj.ToDate = txtToDate.DateShamsi.ToLatinDateTimeFull("23:59:59");// همیشه تاریخ پایانی ، تا آخرین ساعت روز باشد
                }

                #endregion تا تاریخ

                obj.SortColumn = SortColumn;
                obj.SortOrder = SortOrder;
                obj.PageNumber = PageNumber;
                obj.PageSize = PageSize;
                obj.BoardID = BoardID;

                List<DTO.Admin.tblBoardMemberUserMapView.tblBoardMemberUserMapView> RList = (new DAL.Admin.tblBoardMemberUserMapView.Methods()).SearchByPaging(obj);

                gvSearchResult.CustomDataBind(RList, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                long _TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;

                this.TotalRow = _TotalRow;
                lblTotalSearchCount.Text = _TotalRow.ToString();

                SetSortStatus();
            }
            catch(Exception ex)
            {
                LogError(ex, "خطا در نمایش نتیجه جستجوی اعضا.");
            }
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            Search();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvSearchResult.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvSearchResult.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvSearchResult.HeaderRow.FindControl("img" + SortColumn) as Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }
}