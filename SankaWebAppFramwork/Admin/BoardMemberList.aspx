﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BoardMemberList.aspx.cs"
    Inherits="AdministrationViolation.Admin.BoardMemberList" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/superfish.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/hoverIntent.js" type="text/javascript"></script>
    <script src="/js/superfish.js" type="text/javascript"></script>
    <script src="/js/site.js" type="text/javascript"></script>
    <link href="/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="notification-area" runat="server" id="divMessage">
    </div>
    <div>
        <fieldset>
            <legend>ثبت عضو جدید</legend>
            <div class="_10bm">
                <label class="form-field-label">
                    انتخاب کاربر :</label>
                <asp:DropDownList ID="ddlMemberUser" runat="server">
                    <asp:ListItem Text="انتخاب کاربر" Value="-1" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <label class="form-field-label">
                    انتخاب نقش :</label>
                <asp:DropDownList ID="ddlMemberRole" runat="server">
                </asp:DropDownList>
                <asp:Button ID="btnAddMember" OnClientClick="lockScreen();" OnClick="btnAddMember_Click" runat="server" Text="ثبت عضو"
                    CssClass="btn btn-primary" />
            </div>
        </fieldset>
    </div>
    <div>
        <fieldset>
            <legend>لیست اعضای فعلی</legend>
            <div class="grid">
                <SG:SankaGrid ID="gvBoardMember" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UserID" HeaderText="MenuID" Visible="False" />
                        <asp:BoundField DataField="FullName" HeaderText="نام و نام خانوادگی" />
                        <%--<asp:BoundField DataField="StatusDescription" HeaderText="وضعیت" />--%>
                        <asp:TemplateField HeaderText="نقش عضو">
                            <ItemTemplate>
                                <%# AdministrationViolation.Admin.Enums.Board_Member_Role_Label[Eval("MemberRole").ToString().StringToInt()]%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="تاریخ شروع">
                            <ItemTemplate>
                                <%# Convert.ToDateTime(Eval("StartActivity").ToString()).ToPersianDate()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="عملیات">
                            <ItemTemplate>
                                <a title="حذف" onclick="return ShowConfirm(this);" href='<%# string.Format("BoardMemberList.aspx?MapRecordID={0}&action={1}&BoardID={2}", Eval("MapRecordID"), (byte)SankaWebAppFramework.Enums.Page_ActionCode.Delete,Eval("BoardID"))%>'>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        رکوردی موجود نیست
                    </EmptyDataTemplate>
                    <PagerStyle BackColor="#CCCCCC" />
                </SG:SankaGrid>
            </div>
            <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
                ID="MessageDialog">
            </Sanka:SankaDialog>
        </fieldset>
    </div>
    <asp:HiddenField ID="hfPageNumber" runat="server" Value="1" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageSize" runat="server" Value="10" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageChanged" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hfTotalRow" runat="server" Value="0" ClientIDMode="Static" />
    </form>
</body>
</html>
