﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchBoardMember.aspx.cs"
    Inherits="SankaWebAppFramework.Admin.SearchBoardMember" %>

<%@ Register TagPrefix="sanka" TagName="Date" Src="~/controls/UCDatePicker.ascx" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/site.js" type="text/javascript"></script>
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------->
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css"
        media="all" />
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css"
        media="all" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------->
    <script src="../screen_locker/js/uilock.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
    <div>
        <fieldset>
            <legend>جستجو</legend>
            <div class="_5p odd-row">
                <div>
                    <label class="form-field-label">
                        تاریخ شروع :</label>
                    <Sanka:Date DontValidate="false" ID="txtFromDate" runat="server">
                    </Sanka:Date>
                    <label class="form-field-label">
                        تاریخ پایان :</label>
                    <Sanka:Date DontValidate="false" ID="txtToDate" runat="server">
                    </Sanka:Date>
                    <asp:Button title="جستجو" ID="btnSearch" OnClientClick="lockScreen();" CssClass="btn btn-primary"
                        runat="server" Text="جستجو" OnClick="btnSearch_Click" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>نتایج جستجو</legend>
            <div class="_5p odd-row">
                <label>
                    تعداد کل نتایج جستجو :
                </label>
                <asp:Label ID="lblTotalSearchCount" runat="server" Text=""></asp:Label>
            </div>
            <div class="grid">
                <cc1:SankaGrid ID="gvSearchResult" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="RowNumber" HeaderText="ردیف" />
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="OstanName" data-sankatooltip="مرتب سازی براساس استان"
                                    ToolTip="مرتب سازی براساس استان" CommandName="ASC" ID="lnkbtnOstanName" OnCommand="lnkbtnSort_Command"
                                    runat="server">استان</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgOstanName"
                                    runat="server" Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("OstanName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="RegionName" data-sankatooltip="مرتب سازی براساس منطقه"
                                    ToolTip="مرتب سازی براساس منطقه" CommandName="ASC" ID="lnkbtnRegionName" OnCommand="lnkbtnSort_Command"
                                    runat="server">منطقه</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgRegionName"
                                    runat="server" Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("RegionName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="fname">
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="FName" data-sankatooltip="مرتب سازی براساس نام"
                                    ToolTip="مرتب سازی براساس نام" CommandName="ASC" ID="lnkbtnFName" OnCommand="lnkbtnSort_Command"
                                    runat="server">نام</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgFName" runat="server"
                                    Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("FName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="lname">
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="LName" data-sankatooltip="مرتب سازی براساس نام خانوادگی"
                                    ToolTip="مرتب سازی براساس نام خانوادگی" CommandName="ASC" ID="lnkbtnLName" OnCommand="lnkbtnSort_Command"
                                    runat="server">نام خانوادگی</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgLName" runat="server"
                                    Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("LName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="pcode">
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="PersonnelCode" data-sankatooltip="مرتب سازی براساس کد پرسنلی"
                                    ToolTip="مرتب سازی براساس کد پرسنلی" CommandName="ASC" ID="lnkbtnPersonnelCode"
                                    OnCommand="lnkbtnSort_Command" runat="server">کد پرسنلی</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgPersonnelCode"
                                    runat="server" Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("PersonnelCode")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="StartActivity" data-sankatooltip="مرتب سازی براساس تاریخ شروع فعالیت"
                                    ToolTip="مرتب سازی براساس تاریخ شروع فعالیت" CommandName="ASC" ID="lnkbtnStartActivity"
                                    OnCommand="lnkbtnSort_Command" runat="server">تاریخ شروع فعالیت</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgStartActivity"
                                    runat="server" Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Utility.GetCustomDateWithoutTime(Eval("StartActivity")) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton CommandArgument="EndActivity" data-sankatooltip="مرتب سازی براساس تاریخ پایان فعالیت"
                                    ToolTip="مرتب سازی براساس تاریخ پایان فعالیت" CommandName="ASC" ID="lnkbtnEndActivity"
                                    OnCommand="lnkbtnSort_Command" runat="server">تاریخ پایان فعالیت</asp:LinkButton>
                                <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgEndActivity"
                                    runat="server" Visible="false" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("EndActivity") !=null ? Utility.GetCustomDateWithoutTime(Eval("EndActivity")) : "ادامه دارد ..." %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        رکوردی موجود نیست .
                    </EmptyDataTemplate>
                </cc1:SankaGrid>
            </div>
        </fieldset>
    </div>
    <%--for Paging--%>
    <asp:HiddenField ID="hfPageNumber" runat="server" Value="1" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageSize" runat="server" Value="10" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageChanged" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hfTotalRow" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
    </form>
</body>
</html>
