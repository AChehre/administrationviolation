﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SankaWebAppFramework;

namespace AdministrationViolation.Admin
{
    public partial class BoardList : GeneralPage
    {
        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        /// <summary>
        /// آیدی هیات
        /// </summary>
        private long BoardID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            States1.VisibleRegion = false;
            States1.VisibleSchool = false;

            #region مد حذف یک رکورد

            if (BoardID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(SankaWebAppFramework.Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "BoardList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                Delete();
            }

            #endregion مد حذف یک رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();
                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;

                // مقدار دهی نوع هئیت ها
                Common.BindBoardType(ddlBoardType);
                // حذف دفتر هماهنگی از لیست
                for (int i = 0; i < ddlBoardType.Items.Count; i++)
                {
                    if (ddlBoardType.Items[i].Value.StringToByte() == (byte)Enums.Board_Type.Shora)
                    {
                        ddlBoardType.Items.RemoveAt(i);
                    }
                }
                //------------------

                // مقداردهی وضعیت هیات ها
                Common.BindBoardStatus(ddlBoardStatus);

                //#region ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                //if (UserOstanCode.HasValue)
                //{
                //    States1.OstanCode = UserOstanCode.Value;
                //    States1.CurrentUserOstanCode = UserOstanCode.Value;
                //}
                //if (UserRegionCode.HasValue)
                //{
                //    States1.RegionCode = UserRegionCode.Value;
                //    States1.CurrentUserRegionCode = UserRegionCode.Value;
                //}
                //if (UserSchoolCode.HasValue)
                //{
                //    States1.SchoolCode = UserSchoolCode.Value;
                //    States1.CurrentUserSchoolCode = UserSchoolCode.Value;
                //}

                //States1.UserID = UserID;

                //#endregion ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                ddlBoardStatus.SelectedValue = ((byte)Enums.Board_Status.Active).ToString();

                States1.VisibleOstan = true;
                States1.VisibleRegion = false;
                States1.VisibleSchool = false;

                // نوع هئیت تجدید نظر رو روی فرم میریزیم، تا در جاوااسکریپت ازش استفاده کنیم که استان ها را نمایش ندهیم
                hfRevisionBoardType.Value = ((byte)Enums.Board_Type.Revision).ToString();
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvBoards.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)SankaWebAppFramework.Enums.PageLinkNumber.TenNumber);
            }

            if (ddlBoardType.SelectedValue.StringToByte() == (byte)Enums.Board_Type.Revision)
            {// اگر هیات از نوع تجدید نظر بود، کنترل مناطق پنهان شود
                ZoneDiv.Attributes.Add("class", "hidden");
            }
            else
            {
                ZoneDiv.Attributes.Add("class", "");
            }
        }

        /// <summary>
        /// نمایش اطلاعات
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Admin.tblBoardOstansView.tblBoardOstansView obj = new DTO.Admin.tblBoardOstansView.tblBoardOstansView();
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.OstanCode = UserOstanCode;
                obj.RegionCode = UserRegionCode;
                obj.SchoolCode = UserSchoolCode;
                obj.UserID = UserID;
                obj.SortOrder = SortOrder;
                obj.SortColumn = SortColumn;
                obj.Status = null;
                if (ddlBoardStatus.SelectedValue.StringToInt() > 0)
                {
                    obj.Status = ddlBoardStatus.SelectedValue.StringToByte();
                }
                if (ddlBoardType.SelectedValue.StringToByte() > 0)
                {
                    obj.BoardType = ddlBoardType.SelectedValue.StringToByte();
                }

                if (States1.OstanCode > 0)
                {
                    obj.OstanCodeForFilter = States1.OstanCode;
                }

                if (obj.OstanCode < 1)
                {
                    obj.OstanCode = null;
                }

                obj.RegionCode = null;
                obj.SchoolCode = null;

                if (obj.BoardType == (byte)Enums.Board_Type.Revision)
                {// اگر هیات تجدید نظر بود، استان فیلتر نشود، چون هیات تجدید نظر استان ندارد
                    obj.OstanCodeForFilter = null;
                }

                obj.ExceptedBoardType = (byte)Enums.Board_Type.Shora;

                List<DTO.Admin.tblBoardOstansView.tblBoardOstansView> RList = (new DAL.Admin.tblBoardOstansView.Methods()).SelectAllByPaging(obj);
                gvBoards.CustomDataBind(RList, this.PageSize, this.PageNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;

                SetSortStatus();
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش لیست هیات ها");
            }
        }

        /// <summary>
        /// حذف صفحه
        /// </summary>
        protected void Delete()
        {
            if (BoardHasFile())
            {
                MessageDialog.ShowMessage("این هیات دارای پرونده میباشد . شما مجاز به خذف ایم هیات نمی باشید", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }

            bool DeleteStat = false;

            try
            {
                DTO.Admin.tblBoard.tblBoard obj = new DTO.Admin.tblBoard.tblBoard();
                obj.BoardID = BoardID;
                DeleteStat = (new DAL.Admin.tblBoard.Methods()).Delete(obj);
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در حذف هیات");
            }

            if (DeleteStat)
            {
                MessageDialog.ShowMessage("هیات مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف هیات", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// چک کردن اینکه هیات دارای پرونده میباشسد یا خیر
        /// </summary>
        /// <returns></returns>
        private bool BoardHasFile()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                BoardID = this.BoardID
            };
            return (new DAL.Board.tblViolationFile.Methods()).SelectAllByBoardID(obj).Any();
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ثبت جدید
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                TargetID = "btnNew",
                Title = "ثبت جدید",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/admin/Board.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.Insert
            });

            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// کد دکمه فیلتر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            BindGrid();
        }

        /// <summary>
        /// کد دکمه نمایش همه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            // ست کردن کمبوباکس ها به گزینه انتخاب کنید
            ddlBoardType.SelectedValue = "-1";
            States1.OstanCodeSelectedIndex = -1;
            ddlBoardStatus.SelectedValue = "-1";
            BindGrid();
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            BindGrid();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvBoards.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvBoards.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvBoards.HeaderRow.FindControl("img" + SortColumn) as Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// حذف تمام پرونده ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteFiles_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DTO.Admin.tblBoard.tblBoard obj = new DTO.Admin.tblBoard.tblBoard();
                obj.BoardID = e.CommandArgument.ToString().StringToLong();
                bool status = (new DAL.Admin.tblBoard.Methods()).DeleteAllViolation(obj);

                if (status)
                {
                    MessageDialog.ShowMessage("عملیات با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, Page);
                }
            }
            catch(Exception ex)
            {
                LogError(ex, "خطا در حذف");
            }
        }
    }
}