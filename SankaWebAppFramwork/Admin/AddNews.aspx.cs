﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace SankaWebAppFramework.Admin
{
    public partial class AddNews : GeneralPage
    {
        long MaxFileSize = 1024 * 1024;
        /// <summary>
        /// شناسه خبر
        /// </summary>
        private long NewsID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (NewsID > 0)
            {
                result = UpdateRecord();
            }
            else
            {
                result = InsertRecord();
            }
        }

        /// <summary>
        /// ویرایش خبر
        /// </summary>
        /// <returns></returns>
        private bool UpdateRecord()
        {
            if (!ValidateData()) return false;
            string strImageUrl = UploadFile();
            if (strImageUrl.Length > 0)
            {
                // مسیر فایل تصویر خبر پیش فرض
                if (hfPrevFileUrl.Value.Length > 0)
                {
                    string strPrevFilePhysicalPath = Server.MapPath(hfPrevFileUrl.Value);
                    if (File.Exists(strPrevFilePhysicalPath))
                    {
                        File.Delete(strPrevFilePhysicalPath);
                    }
                }
                hfPrevFileUrl.Value = strImageUrl;
            }
            else
            {
                hfPrevFileUrl.Value = "~/images/noImage.png";
            }

            DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews();
            obj.ID = hfNewsID.Value.StringToLong();
            obj.CategoryID = 1;
            obj.Title = txtTitle.Text;
            obj.ShortDescription = txtShortDescription.Text;
            obj.Comment = ckDescription.Text;
            obj.WriteDate = txtWriteDate.DateMiladi;
            obj.ModifyDate = DateTime.Now;
            obj.ImageUrl = hfPrevFileUrl.Value;
            obj.Published = chkPublished.Checked;
            obj.ExpirationDate = txtExpDate.DateMiladi;
            bool bln = (new DAL.Admin.tblNews.Methods()).Update(obj);
            if (bln)
            {
                //ShowMessage("اطلاعیه جدید افزوده شد .");
                txtTitle.Clear();
                // لیست گروه های کاربری ای که سطح دسترسی دیدن این اکشن را دارند
                List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> _lstSelectedgroup = new List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap>();

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    GetAllCheckedNodes(_lstSelectedgroup, node, NewsID);
                }
                // گروه های کاربری انتخاب شده
                if (_lstSelectedgroup != null)
                {
                    UpdateNewsGroupMap(NewsID, _lstSelectedgroup);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// درج حبر جدید
        /// </summary>
        /// <returns></returns>
        private bool InsertRecord()
        {
            if (!ValidateData()) return false;
            string strImageUrl = UploadFile();
            // در صورتی که تصویری بالا گذاری نگردد ، این لینک پیش فرض برای تصویر در نظر گرفته میشود .
            if (strImageUrl.Length == 0) strImageUrl = "~/images/noImage.png";

            DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews();
            obj.CategoryID = 1;
            obj.Title = txtTitle.Text;
            obj.ShortDescription = txtShortDescription.Text;
            obj.Comment = new HtmlString(ckDescription.Text).ToHtmlString();
            obj.WriteDate = txtWriteDate.DateMiladi;
            obj.ModifyDate = DateTime.Now;
            obj.ImageUrl = strImageUrl;
            obj.Published = chkPublished.Checked;
            obj.ExpirationDate = txtExpDate.DateMiladi;
            long NewsID = (new DAL.Admin.tblNews.Methods()).Insert(obj);
            if (NewsID > 0)
            {
                //ShowMessage("اطلاعیه جدید افزوده شد .");
                txtTitle.Clear();
                // لیست گروه های کاربری ای که سطح دسترسی دیدن این اکشن را دارند
                List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> _lstSelectedgroup = new List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap>();

                foreach (TreeNode node in tvGroup.Nodes)
                {
                    GetAllCheckedNodes(_lstSelectedgroup, node, NewsID);
                }
                // گروه های کاربری انتخاب شده
                if (_lstSelectedgroup != null)
                {
                    UpdateNewsGroupMap(NewsID, _lstSelectedgroup);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// ویرایش لیست گروه هایی که مجوز مشاهده خبر را دارند .
        /// </summary>
        /// <param name="newsID">شناسه خبر</param>
        /// <param name="_lstSelectedgroup">لیست گرو های انتخابی</param>
        private void UpdateNewsGroupMap(long newsID, List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> _lstSelectedgroup)
        {
            DTO.Admin.tblNewsGroupMap.tblNewsGroupMap obj = new DTO.Admin.tblNewsGroupMap.tblNewsGroupMap { NewsID = newsID };
            (new DAL.Admin.tblNewsGroupMap.Methods()).Delete(obj);

            bool bln = (new DAL.Admin.tblNewsGroupMap.Methods()).Insert(_lstSelectedgroup);
        }

        /// <summary>
        /// بالا گذاری و ذخیره تصویر خبر
        /// </summary>
        /// <returns></returns>
        private string UploadFile()
        {
            string strSavedFileUrl = "";
            try
            {
                bool isSupperted = false;
                if (MyFileUpload.HasFile)
                {
                    if (MyFileUpload.PostedFile.ContentLength > MaxFileSize)
                    {
                        SankaDialog1.ShowMessage("حجم فایل ارسالی بالاتر از مقدار مجاز می باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                        return strSavedFileUrl;
                    }

                    // لیست فرمت های مجاز برای آپلود
                    List<string> strFilter = new List<string>();
                    strFilter.Add("gif");
                    strFilter.Add("png");
                    strFilter.Add("jpeg");
                    strFilter.Add("jpg");

                    if (strFilter.Count > 0)
                    {
                        foreach (string strType in strFilter)
                        {
                            if (MyFileUpload.PostedFile.ContentType.ToLower().Contains(strType) || strType.Equals("*.*"))
                            {
                                isSupperted = true;
                                break;
                            }
                        }
                        if (isSupperted)
                        {
                            // بالا گذاری فایل ، ایجاد و ثبت یک رکورد در جدولی در حافظه و بروز رسانی گرید
                            int index = MyFileUpload.FileName.LastIndexOf(".");
                            strSavedFileUrl = "~/newsImage/" + Guid.NewGuid().ToString() + "." + MyFileUpload.FileName.Remove(0, index + 1);
                            string UploadPath = Server.MapPath(strSavedFileUrl);
                            MyFileUpload.SaveAs(UploadPath);
                        }
                        else
                        {
                            SankaDialog1.ShowMessage("فرمت فایل نامعتبر است .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                        }
                    }
                }
            }
            catch (Exception)
            {
               // Utility.ShowMessage("خطایی در بارگذاری پیوست رخ داده است .", Page);
            }
            return strSavedFileUrl;
        }

        /// <summary>
        /// اعتبار سنجی پیش از ذخیره
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            List<string> lstMessage = new List<string>();
            if (txtTitle.IsEmpty())
            {
                lstMessage.Add("درج عنوان اطلاعیه الزامی است .");
            }
           
            if (!txtWriteDate.DateIsValid)
            {
                lstMessage.Add("تاریخ درج اطلاعیه را وارد نمایید .");
            }
            if ((txtWriteDate.DateMiladi.Ticks - txtExpDate.DateMiladi.Ticks) >= 0)
            {
                lstMessage.Add("تاریخ انقضاء باید بزرگتر از تاریخ درج اطلاعیه باشد .");
            }
            if (!txtExpDate.DateIsValid)
            {
                lstMessage.Add("تاریخ انقضاء اطلاعیه را وارد نمایید .");
            }
            if (txtShortDescription.IsEmpty())
            {
                lstMessage.Add("توضیح  متن کوتاه در مورد اطلاعیه الزامی است .");
            }
            if (ckDescription.Text.Trim().Length == 0)
            {
                lstMessage.Add("متن اطلاعیه را درج نمایید .");
            }
            if (lstMessage.Any())
            {
                SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
            return (lstMessage.Count == 0);
        }

        /// <summary>
        /// ریست کردن کنترلهای فرم
        /// </summary>
        protected void ResetForm()
        {
            txtTitle.Clear();
            txtShortDescription.Clear();
            ckDescription.Text = string.Empty;
            txtWriteDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
        }

        /// <summary>
        /// بارگراری رکورد خبر
        /// </summary>
        /// <param name="NewsID"></param>
        private void LoadData(long NewsID)
        {
            DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews();
            obj.ID = NewsID;
            obj = (new DAL.Admin.tblNews.Methods()).SelectByID(obj);
            hfNewsID.Value = obj.ID.ToString();
            txtWriteDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(obj.WriteDate);
            txtTitle.Text = obj.Title;
            txtShortDescription.Text = obj.ShortDescription;
            ckDescription.Text = obj.Comment;
            hfPrevFileUrl.Value = obj.ImageUrl;
            chkPublished.Checked = obj.Published.Value;
            txtExpDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(obj.ExpirationDate.AddDays(1));
            List<long> _lstSelectedgroup = getPermittedGroup();
            foreach (TreeNode node in tvGroup.Nodes)
            {
                SetAllCheckedNodes(_lstSelectedgroup,node);
            }
        }

        /// <summary>
        /// گرفتن لیست گروهای انتخاب شده از درختواره گروه ها
        /// </summary>
        /// <returns></returns>
        private List<long> getPermittedGroup()
        {
            DTO.Admin.tblNewsGroupMap.tblNewsGroupMap obj = new DTO.Admin.tblNewsGroupMap.tblNewsGroupMap
            {
                NewsID = this.NewsID
            };
            var lstNewsGroupMap = (new DAL.Admin.tblNewsGroupMap.Methods()).SelectByNewsID(obj);
            List<long> lst = new List<long>();
            foreach (var NewsGroupMap in lstNewsGroupMap)
            {
                lst.Add(NewsGroupMap.GroupID);
            }
            return lst;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/admin/NewsList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }

        private void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (NewsID > 0)
            {
                result = UpdateRecord();
            }
            else
            {
                result = InsertRecord();
            }
            if (result)
            {
                SankaDialog1.ShowMessage("اطلاعیه با موفقیت افزوده شد", DialogMessage.SankaDialog.Message_Type.Message, Page);
                ResetForm();
            }
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (NewsID > 0)
            {
                result = UpdateRecord();
            }
            else
            {
                result = InsertRecord();
            }
            if (result)
            {
                SankaDialog1.ShowMessage("اطلاعیه با موفقیت افزوده شد", DialogMessage.SankaDialog.Message_Type.Message, "/Admin/NewsList.aspx" + "?action=" + (byte)Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void GetAllCheckedNodes(List<DTO.Admin.tblNewsGroupMap.tblNewsGroupMap> _lstSelectedgroup, TreeNode Node , long NewsID)
        {
            if (Node.Checked)
            {//  اگر گروه انتخاب شده باشد به لیست اضافه شود
                DTO.Admin.tblNewsGroupMap.tblNewsGroupMap Obj = new DTO.Admin.tblNewsGroupMap.tblNewsGroupMap();
                Obj.GroupID = Node.Value.StringToInt();
                Obj.NewsID = NewsID;
                _lstSelectedgroup.Add(Obj);
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                GetAllCheckedNodes(_lstSelectedgroup, thisNode , NewsID);
            }
        }

        /// <summary>
        /// گرفتن لیست تمام گروه هایی که آنها در نمایش درختی انتخاب شده اند 
        /// </summary>
        /// <param name="Node">نود ریشه که میخواهیم فرزندانش را پیمایش کنیم</param>
        protected void SetAllCheckedNodes(List<long> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Contains(Node.Value.StringToLong())) //  اگر گروه انتخاب شده باشد به لیست اضافه شود
            {
                Node.Checked = true;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                SetAllCheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// اگر مقدار نودی از درخت، در لیست ورودی تابع وجود داشت، آن نود ، انتخاب شود
        /// </summary>
        /// <param name="_lstSelectedgroup">لیست حاوی گروه هایی که کاربر در آنها عضو است</param>
        /// <param name="Node">نودی که میخواهیم آن را بررسی کنیم</param>
        protected void CheckedNodes(List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> _lstSelectedgroup, TreeNode Node)
        {
            if (_lstSelectedgroup.Count(g => g.UserGroupID == Node.Value.StringToInt()) > 0)
            {
                Node.Checked = true;
            }
            else
            {
                Node.Checked = false;
            }

            foreach (TreeNode thisNode in Node.ChildNodes)
            {
                CheckedNodes(_lstSelectedgroup, thisNode);
            }
        }

        /// <summary>
        /// ساخت یک تری ویو از گروه ها و زیر گروه ها
        /// </summary>
        /// <param name="lstUserGroup">لیست حاوی تمام گروه ها</param>
        /// <param name="ParentID">آیدی گروهی که میخواهیم زیر گروه آن ساخته شود</param>
        /// <param name="treeNode">نودی که باید زیر گروه ها به آن اضافه شوند</param>
        protected void CreateSubGroup(List<DTO.Config.tblUserGroup.tblUserGroup> lstUserGroup, int? ParentID, TreeNode treeNode)
        {
            // بدست آوردن تمام سرگروه ها
            var lstParentMenu = lstUserGroup.Where(x => x.ParentID == ParentID).ToList();

            foreach (DTO.Config.tblUserGroup.tblUserGroup Item in lstParentMenu)
            {
                TreeNode child = new TreeNode
                {
                    Text = Item.GroupName,
                    Value = Item.GroupID.ToString()
                };

                if (!ParentID.HasValue)
                {// اگر سر گروه باشد
                    tvGroup.Nodes.Add(child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }

                //اگر زیر گروه داشت، ساخت زیرگروه ها به صورت بازگشتی صدا زده شود
                if (lstUserGroup.Any(x => x.ParentID == Item.GroupID))
                {
                    CreateSubGroup(lstUserGroup, Item.GroupID.Value, child);
                }
            }
        }

        /// <summary>
        /// نمایش گروه های کاربری به صورت درختی
        /// </summary>
        /// <returns></returns>
        protected void CreateGroupTree()
        {
            #region بدست آوردن تمام گروه های کاربری

            List<DTO.Config.tblUserGroup.tblUserGroup> lstGroup = new List<DTO.Config.tblUserGroup.tblUserGroup>();
            lstGroup = (new DAL.Config.tblUserGroup.Methods()).SelectAll();

            #endregion بدست آوردن تمام گروه های کاربری

            // ساخت زیرگروه های ،گروه های پدر
            // قرارداد: اگر پرنت آیدی گروه برابر نال یک بود، یعنی گروه سطح یک و پدر است
            CreateSubGroup(lstGroup, null, null);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateGroupTree();
                if (this.NewsID > 0)
                {
                    LoadData(NewsID);
                }
            }
            CreateActionButton();
        }
    }
}