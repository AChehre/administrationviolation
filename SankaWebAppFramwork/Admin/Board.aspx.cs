﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SankaWebAppFramework;

namespace AdministrationViolation.Admin
{
    public partial class Board : SankaWebAppFramework.GeneralPage
    {
        /// <summary>
        /// آیدی هیات
        /// </summary>
        private long BoardID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            States1.VisibleOstan = true;
            States1.VisibleRegion = false;
            States1.VisibleSchool = false;

            #region چک کردن دسترسی کاربر به ثبت رکورد

            if (BoardID == 0)
            {// مد ثبت رکورد جدید
                if (!CheckActionCode(SankaWebAppFramework.Enums.Page_ActionCode.Insert))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "BoardList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
                }
            }

            #endregion چک کردن دسترسی کاربر به ثبت رکورد

            CreateActionButton();

            if (!IsPostBack)
            {
                States1.Initialize();

                #region ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                if (UserOstanCode.HasValue)
                {
                    States1.OstanCode = UserOstanCode.Value;
                    States1.CurrentUserOstanCode = UserOstanCode.Value;
                }
                //if (UserRegionCode.HasValue)
                //{
                //    States1.RegionCode = UserRegionCode.Value;
                //    States1.CurrentUserRegionCode = UserRegionCode.Value;
                //}
                //if (UserSchoolCode.HasValue)
                //{
                //    States1.SchoolCode = UserSchoolCode.Value;
                //    States1.CurrentUserSchoolCode = UserSchoolCode.Value;
                //}

                States1.UserID = UserID;

                #endregion ست کردن مقدار استان ، منطقه و مدرسه برابر با محدوده کاربر جاری

                States1.VisibleRegion = false;
                States1.VisibleSchool = false;

                // مقدار دهی نوع هئیت ها
                Common.BindBoardType(ddlBoardType);

                // حذف دفتر هماهنگی از لیست
                for (int i = 0; i < ddlBoardType.Items.Count; i++)
                {
                    if (ddlBoardType.Items[i].Value.StringToByte() == (byte)Enums.Board_Type.Shora)
                    {
                        ddlBoardType.Items.RemoveAt(i);
                    }
                }
                //------------------
                // مقداردهی وضعیت هیات ها
                Common.BindBoardStatus(ddlBoardStatus);

                if (BoardID > 0)
                {// مد ویرایش

                    #region چک کردن دسترسی کاربر به ویرایش رکورد

                    if (!CheckActionCode(SankaWebAppFramework.Enums.Page_ActionCode.Edit))
                    {
                        MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "BoardList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
                    }

                    #endregion چک کردن دسترسی کاربر به ویرایش رکورد

                    LoadData();
                }

                // نوع هئیت تجدید نظر رو روی فرم میریزیم، تا در جاوااسکریپت ازش استفاده کنیم که استان ها را نمایش ندهیم
                hfRevisionBoardType.Value = ((byte)Enums.Board_Type.Revision).ToString();
            }

            if (ddlBoardType.SelectedValue.StringToByte() == (byte)Enums.Board_Type.Revision)
            {// اگر هیات از نوع تجدید نظر بود، کنترل مناطق پنهان شود
                ZoneDiv.Attributes.Add("class", "hidden");
            }
            else
            {
                ZoneDiv.Attributes.Add("class", "");
            }
        }

        /// <summary>
        /// نمایش اطلاعات هیات ای که ایدیش در کوئری استرینگ هست
        /// </summary>
        protected void LoadData()
        {
            try
            {
                DTO.Admin.tblBoard.tblBoard obj = new DTO.Admin.tblBoard.tblBoard();
                obj.BoardID = BoardID;

                obj = (new DAL.Admin.tblBoard.Methods()).SelectByID(obj);
                if (obj.RowNumber > 0)
                {// مقداردهی کنترل ها در صورت وجود هیات درخواستی
                    txtBoardTitle.Text = obj.BoardTitle;
                    txtBoardCode.Text = hfBoardCode.Value = obj.BoardCode.ToString();
                    // اگر داده های نال، مقدار داشتند، مقدارشان ست شود
                    if (obj.BoardType.HasValue)
                    {
                        ddlBoardType.SelectedValue = obj.BoardType.ToString();
                        if (obj.BoardType.Value == (byte)Enums.Board_Type.Revision)
                        {// اگر هیات از نوع تجدید نظر بود، کنترل مناطق پنهان شود
                            ZoneDiv.Attributes.Add("class", "hidden");
                        }
                    }

                    if (obj.Status.HasValue)
                    {
                        ddlBoardStatus.SelectedValue = obj.Status.Value.ToString();
                    }

                    if (obj.OstanCode.HasValue)
                    {
                        States1.OstanCode = obj.OstanCode.Value;
                    }

                    //if (obj.RegionCode.HasValue)
                    //{
                    //    States1.RegionCode = obj.RegionCode.Value;
                    //}

                    //if (obj.SchoolCode.HasValue)
                    //{
                    //    States1.SchoolCode = obj.SchoolCode.Value;
                    //}
                    txtAddress.Text = obj.Address;
                }
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش اطلاعات هیات");
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود در حالت ویرایش
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?ID=" + NewID + "&action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.Edit);
                }
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در ثبت اطلاعات");
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود کنترل ها خالی شود
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.Insert);
                }
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در ثبت اطلاعات");
            }
        }

        /// <summary>
        /// کلیک دکمه ذخیره و بستن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                long NewID = SaveData();

                if (NewID > 0)
                {// اگر ثبت موفقیت آمیز بود برود به صفحه لیست صفحات
                    MessageDialog.ShowMessage("اطلاعات با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, "BoardList.aspx" + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View);
                }
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در ثبت اطلاعات");
            }
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// اگر کوئری استرینگ حاوی آیدی صفحه باشد، عمل ویرایش
        /// در غیر این صورت عمل ثبت انجام میشود
        /// </summary>
        protected long SaveData()
        {
            long Result = 0;

            try
            {
                #region مقداردهی آبجکت جهت ویرایش یا ثبت

                DTO.Admin.tblBoard.tblBoard obj = new DTO.Admin.tblBoard.tblBoard();
                obj.BoardTitle = txtBoardTitle.Text;
                obj.BoardType = ddlBoardType.SelectedValue.StringToByte();
                obj.Status = ddlBoardStatus.SelectedValue.StringToByte();
                obj.BoardCode = txtBoardCode.Text;
                // اگر مقدارهای، استان، منطقه و مدرسه انتخاب شده بود، ست شود درغیر این صورت نال ذخیره میشود
                if (States1.OstanCode > 0)
                {
                    obj.OstanCode = States1.OstanCode;
                }
                //if (States1.RegionCode > 0)
                //{
                //    obj.RegionCode = States1.RegionCode;
                //}
                //if (States1.SchoolCode > 0)
                //{
                //    obj.SchoolCode = States1.SchoolCode;
                //}

                if (obj.BoardType == (byte)Enums.Board_Type.Revision)
                {// اگر هیات تجدید نظر بود، مناطق برایش ست نشود
                    obj.OstanCode = null;
                    obj.RegionCode = null;
                    obj.SchoolCode = null;
                }

                obj.Address = txtAddress.Text.CorrectArabicChars();

                #endregion مقداردهی آبجکت جهت ویرایش یا ثبت

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(obj, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    MessageDialog.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, this);
                    return 0;
                }

                if (BoardID > 0)
                {
                    #region ویرایش اطلاعات

                    obj.BoardID = BoardID;

                    if ((new DAL.Admin.tblBoard.Methods()).Update(obj))
                    {// اگر ویرایش موفقیت آمیز بود، آیدی برگردانده شود
                        Result = obj.BoardID.Value;
                    }

                    #endregion ویرایش اطلاعات
                }
                else
                {
                    #region ثبت اطلاعات

                    // چون پارامتر اوتپوت تعریف شده در استورپروسیجر، اگر هنگام ثبت مقدار ندهیم خطا میدهد
                    obj.BoardID = 0;

                    Result = (new DAL.Admin.tblBoard.Methods()).Insert(obj);

                    #endregion ثبت اطلاعات
                }

                #region نمایش پیام خطا در ثبت

                if (Result == 0)
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام خطا در ثبت
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در ثبت اطلاعات");
            }

            return Result;
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="AspxPage">نام صفحه</param>
        /// <param name="IsPublished">وضعیت دسترسی</param>
        /// <param name="DefaultActionCode">کد عملیات پیش فرض</param>
        /// <param name="Message">متن خطایی که برگشت داده میشود</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Admin.tblBoard.tblBoard obj, out string Message)
        {
            Message = "";
            if (string.IsNullOrEmpty(obj.BoardTitle))
            {
                Message = "لطفا نام هیات را وارد نمایید .";
            }

            if (obj.BoardType.Value < 1)
            {
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا نوع هیات را انتخاب نمایید .";
            }

            if (obj.Status.Value < 1)
            {
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا وضعیت هیات را انتخاب نمایید .";
            }
            if (txtBoardCode.IsEmpty())
            {
                if (!string.IsNullOrEmpty(Message))
                {
                    Message += "<br/>";
                }
                Message += "لطفا کد 5 رقمی هیات را انتخاب نمایید .";
            }
            if (ExistAnotherBoardCode())
            {
                Message += "این کد هیات قبلا استفاده شده است.";
            }

            return (Message.Length == 0);
        }

        /// <summary>
        /// آیا این کد هیات در سیستم موجود میباشد ؟
        /// </summary>
        /// <returns>بلی / خیر</returns>
        private bool ExistAnotherBoardCode()
        {
            if (!hfBoardCode.Value.Equals(txtBoardCode.Text))
            {
                DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
                {
                    BoardCode = txtBoardCode.Text.Trim()
                };
                return (new DAL.Board.tblBoard.Methods()).SelectByCode(obj);
            }
            return false;
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // دکمه ذخیره
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSave_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSave",
                Title = "ذخیره",
                Type = ActionMenuType.ImageButton,
                OnClientClick = "return ValidateRequiredField();"
            });

            //  دکمه ذخیره و جدید
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndNew_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndNew",
                Title = "ذخیره و جدید",
                Type = ActionMenuType.ImageButton,
                OnClientClick = "return ValidateRequiredField();"
            });

            //  دکمه ذخیره و بستن
            LstButton.Add(new ActionMenuModel()
            {
                button_click = btnSaveAndClose_Click,
                CausesValidation = false,
                Icon = "/images/icon/Save.png",
                TargetID = "btnSaveAndClose",
                Title = "ذخیره و بستن",
                Type = ActionMenuType.ImageButton,
                OnClientClick = "return ValidateRequiredField();"
            });

            //  دکمه بازگشت
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/Admin/BoardList.aspx" + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View
            });

            ActionMenu.DataSource = LstButton;
        }

    }
}