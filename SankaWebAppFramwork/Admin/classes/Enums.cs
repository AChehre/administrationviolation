﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdministrationViolation.Admin
{
    public class Enums
    {
        /// <summary>
        /// انواع مختلف هیات
        /// </summary>
        public enum Board_Type
        {
            /// <summary>
            /// بدوی
            /// </summary>
            Initial = 1,
            /// <summary>
            /// تجدید نظر
            /// </summary>
            Revision = 2,
            /// <summary>
            /// شورا
            /// </summary>
            Shora = 3
        }

        /// <summary>
        /// عنوان هیات های مختلف
        /// </summary>
        public static string[] Board_Type_Label =
        {
            "نامشخص",
            "بدوی",
            "تجدید نظر",
            "دفتر هماهنگی"
        };

        public enum Board_Status
        {
            /// <summary>
            /// فعال
            /// </summary>
            Active = 1,

            /// <summary>
            /// غیرفعال
            /// </summary>
            InActive = 2,
        }

        /// <summary>
        /// عنوان هیات های مختلف
        /// </summary>
        public static string[] Board_Status_Label =
        {
            "فعال",
            "غیرفعال",
        };

        /// <summary>
        /// نقش یک عضو در هیات
        /// </summary>
        public enum Board_Member_Role
        {
            /// <summary>
            /// عضو اصلی
            /// </summary>
            MainMember = 1,

            /// <summary>
            /// عضو علی البدل
            /// </summary>
            AlternateMember = 2,
            
            /// <summary>
            /// رئیس هیات
            /// </summary>
            BoardAdmin = 3
        }

        /// <summary>
        /// عنوان نقش های مختلف یک عضو در هیات
        /// </summary>
        public static string[] Board_Member_Role_Label =
        {
            "نامشخص",
            "عضو اصلی",
            "عضو علی البدل",
            "رئیس هیات (عضو اصلی)"
        };

        /// <summary>
        /// وضعیت یک عضو هیات
        /// </summary>
        public enum Board_Member_Status
        {
            /// <summary>
            /// غیرفعال
            /// </summary>
            InActive = 0,

            /// <summary>
            /// فعال
            /// </summary>
            Active = 1
        }

        public static string[] Board_Member_Status_Label =
        {
            "غیرفعال",
            "فعال",
        };
    }
}