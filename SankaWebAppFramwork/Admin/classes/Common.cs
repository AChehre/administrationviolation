﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace AdministrationViolation.Admin
{
    public class Common
    {
        /// <summary>
        /// پر کردن دراپ دان نوع هیات
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست نوع هیات ها را نمایش دهد</param>
        public static void BindBoardType(DropDownList ddl)
        {
            for (int i = 1; i < Enums.Board_Type_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.Board_Type_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
            ddl.Items.Insert(0, new ListItem("انتخاب نوع هیات", "-1"));
            ddl.DataBind();
        }

        /// <summary>
        /// پر کردن دراپ دان وضعیت هیات
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست وضعیت هیات ها را نمایش دهد</param>
        public static void BindBoardStatus(DropDownList ddl)
        {
            for (int i = 0; i < Enums.Board_Status_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.Board_Status_Label[i], (i + 1).ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب وضعیت", "-1"));
        }

        /// <summary>
        /// پر کردن دراپ دان نقش عضو هیات
        /// </summary>
        /// <param name="ddl">دراپ دوان جهت نمایش لیست</param>
        public static void BindMemberRole(DropDownList ddl)
        {
            if (ddl.Items.Count > 0)
            {
                ddl.Items.Clear();
            }

            for (int i = 1; i < Enums.Board_Member_Role_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.Board_Member_Role_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب نقش عضو", "-1"));
        }

    }
}