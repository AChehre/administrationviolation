﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SankaWebAppFramework;

namespace AdministrationViolation.Admin
{
    public partial class BoardMemberList : SankaWebAppFramework.GeneralPage
    {
        /// <summary>
        /// آیدی هیات
        /// </summary>
        private long BoardID
        {
            get
            {
                if (Request.QueryString.Get("BoardID") != null)
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی جدول اعضای هیات
        /// </summary>
        private long MapRecordID
        {
            get
            {
                if (Request.QueryString.Get("MapRecordID") != null)
                {
                    return Request.QueryString.Get("MapRecordID").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region مد حذف یک رکورد

            if (MapRecordID > 0)
            {
                #region چک کردن دسترسی کاربر به حذف رکورد

                if (!CheckActionCode(SankaWebAppFramework.Enums.Page_ActionCode.Delete))
                {
                    MessageDialog.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "BoardMemberList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
                }

                #endregion چک کردن دسترسی کاربر به حذف رکورد

                // اگر سطح دسترسی حذف را داشت، تابع حذف فراخوانی شود
                Delete();
            }

            #endregion مد حذف یک رکورد

            if (!IsPostBack)
            {
                ShowDefaultData();

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvBoardMember.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)SankaWebAppFramework.Enums.PageLinkNumber.TenNumber);
            }
        }

        /// <summary>
        /// مقداردهی کمبوباکس ها
        /// </summary>
        protected void ShowDefaultData()
        {
            try
            {
                // بدست آوردن کاربری که عضو گروه اعضای هیات هستند و در هیات جاری عضو نباشند
                // و کد استان، منطقه و مدرسه انها با هیات انتخابی برابر باشد

                DTO.Admin.tblBoardUserMapView.tblBoardUserMapView obj = new DTO.Admin.tblBoardUserMapView.tblBoardUserMapView();
                obj.BoardID = BoardID;
                //obj.OstanCode = 0;
                //obj.RegionCode = 0;
                //obj.SchoolCode = 0;
                obj.SelectedUserGroupID = 2; // اعضای هیات
                ddlMemberUser.DataSource = (new DAL.Admin.tblBoardUserMapView.Methods()).SelectUsersNotMember(obj);
                ddlMemberUser.DataTextField = "FullName";
                ddlMemberUser.DataValueField = "UserID";
                ddlMemberUser.DataBind();

                ddlMemberUser.Items.Insert(0, new ListItem("انتخاب کاربر", "-1"));

                //** نمایش نقش اعضا
                Common.BindMemberRole(ddlMemberRole);
            }
            catch(Exception ex)
            {
                LogError(ex, "خطا در نمایش اطلاعات اولیه");
            }
        }

        /// <summary>
        /// نمایش اطلاعات
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView obj = new DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView();
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.BoardID = BoardID;
                obj.Status = (byte)Enums.Board_Member_Status.Active;
                List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView> RList = (new DAL.Admin.tblBoardMemberMapView.Methods()).SelectBoardMemberByPaging(obj);
                gvBoardMember.CustomDataBind(RList, this.PageSize, this.PageNumber, 5);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;
            }
            catch (Exception ex)
            {
                LogError(ex,"خطا در نمایش اطلاعات.");
            }
        }

        /// <summary>
        /// حذف صفحه
        /// </summary>
        protected void Delete()
        {
            bool DeleteStat = false;

            try
            {
                DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj = new DTO.Admin.tblBoardMemberMap.tblBoardMemberMap();
                obj.MapRecordID = MapRecordID;
                obj.EndActivity = DateTime.Now;
                obj.Status = (byte)Enums.Board_Member_Status.InActive;
                // حذف فیزیکی در جدول نداریم و فقط وضعیت کاربر را
                // در هیات به غیرفعال تبدیل کرده و تاریخ اتمام فعالیتش را بروزرسانی میکنیم
                DeleteStat = (new DAL.Admin.tblBoardMemberMap.Methods()).Delete(obj);
            }
            catch (Exception ex)
            {
                LogError(ex,"خطا در حذف ");
            }

            if (DeleteStat)
            {
                MessageDialog.ShowMessage("عضو مورد نظر با موفقیت حذف شد", DialogMessage.SankaDialog.Message_Type.Message, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View + "&BoardID=" + BoardID, true);
            }
            else
            {
                MessageDialog.ShowMessage("خطا در حذف", DialogMessage.SankaDialog.Message_Type.Warning, HttpContext.Current.Request.Url.AbsolutePath + "?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View + "&BoardID=" + BoardID, true);
            }
        }

        /// <summary>
        /// کلیک دکمه ثبت یک عضو
        /// </summary>
        protected void btnAddMember_Click(object sender, EventArgs e)
        {
            try
            {
                #region مقداردهی آبجکت

                DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj = new DTO.Admin.tblBoardMemberMap.tblBoardMemberMap();

                obj.StartActivity = DateTime.Now;
                obj.MemberRole = ddlMemberRole.SelectedValue.StringToByte();
                obj.MemberUserID = ddlMemberUser.SelectedValue.StringToLong();
                obj.BoardID = BoardID;
                obj.Status = (byte)Enums.Board_Member_Status.Active;

                #endregion مقداردهی آبجکت

                // اعتبار سنجی داده ها
                if (!Validation(obj))
                {
                    return;
                }

                #region ثبت اطلاعات

                // چون پارامتر اوتپوت تعریف شده در استورپروسیجر، اگر هنگام ثبت مقدار ندهیم خطا میدهد
                obj.MapRecordID = 0;

                long Result = (new DAL.Admin.tblBoardMemberMap.Methods()).Insert(obj);

                #endregion ثبت اطلاعات

                #region نمایش پیام

                if (Result > 0)
                {
                    BindGrid();
                    ShowDefaultData();
                    MessageDialog.ShowMessage("عضو جدید با موفقیت به ثبت رسید", DialogMessage.SankaDialog.Message_Type.Message, this);
                }
                else
                {
                    MessageDialog.ShowMessage("خطا در ثبت اطلاعات", DialogMessage.SankaDialog.Message_Type.Warning, this);
                }

                #endregion نمایش پیام

            }
            catch (Exception ex)
            {
                LogError(ex,"خطا در ثبت عضو جدید");
            }
        }

        /// <summary>
        /// اعتبارسنجی اطلاعات فرم
        /// </summary>
        /// <param name="obj">آبجکت اعضا هیات</param>
        /// <param name="Message">متن خطایی که برگشت داده میشود</param>
        /// <returns>ترو یعنی بدون خطا</returns>
        protected bool Validation(DTO.Admin.tblBoardMemberMap.tblBoardMemberMap obj)
        {
            List<string> lstMessage = new List<string>();

            if (obj.MemberUserID.Value < 1)
            {
               lstMessage.Add("لطفا کاربر موردنظر را انتخاب نمایید.");
            }

            if (obj.MemberRole.Value < 1)
            {
                lstMessage.Add("لطفا نقش کاربر در هیات را انتخاب نمایید.");
            }
            if (ddlMemberRole.SelectedValue.StringToByte() == (byte)Enums.Board_Member_Role.BoardAdmin)
            {
                if (CheckOneManagerAllowed())
                {
                    lstMessage.Add("هر هیات تنها یک رئیس میتواند داشته باشد .");
                }
            }
            if (lstMessage.Any())
            {
                MessageDialog.ShowMessage(GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
            return lstMessage.Count == 0;
        }

        /// <summary>
        /// آیا این هیات دارای رئیس میباشد ؟
        /// </summary>
        /// <returns></returns>
        private bool CheckOneManagerAllowed()
        {
            var obj = new DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView();
            obj.BoardID = BoardID;
            obj.Status = (byte)Enums.Board_Member_Status.Active;
            var lst = (new DAL.Admin.tblBoardMemberMapView.Methods()).SelectBoardMember(obj);
            if (lst.Any())
            {
                return lst.Where(x => x.MemberRole == (byte)Enums.Board_Member_Role.BoardAdmin).Any();
            }
            return false;
        }
    }
}