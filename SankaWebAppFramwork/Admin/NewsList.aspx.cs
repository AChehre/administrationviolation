﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Admin
{
    public partial class NewsList : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // تاریخ شمسی روز اول سال را تولید میکند
                txtFromDate.DateShamsi = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now) + "/01/01";
                // تاریخ شمسی امروز را تولید میکند
                txtToDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                BindGrid();
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            CreateActionButton();
        }

        /// <summary>
        /// نمایش لیست اخبار در گرید
        /// </summary>
        private void BindGrid()
        {
            DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews
            {
                FromDate = txtFromDate.DateMiladi,
                ToDate = txtToDate.DateMiladi,
                PageNumber = this.PageNumber,
                PageSize = this.PageSize,
            };
            gvNews.CustomDataBind((new DAL.Admin.tblNews.Methods()).SelectAll(obj), obj.PageSize.Value, obj.PageNumber.Value);
        }

        /// <summary>
        /// ساخت اکشن منو مخصوص این صفحه
        /// </summary>
        protected void CreateActionButton()
        {
            // پیدا کردن اکشن منو در مستر پیج
            UCActionMenu ActionMenu = this.Master.FindControl("ActionMenu1") as UCActionMenu;

            List<ActionMenuModel> LstButton = new List<ActionMenuModel>();

            // افزودن
            LstButton.Add(new ActionMenuModel()
            {
                CausesValidation = false,
                Icon = "/images/icon/New.png",
                NavigateUrl = "/Admin/AddNews.aspx",
                Title = "اطلاعیه جدید",
                Type = ActionMenuType.HyperLink,
            });
            ActionMenu.DataSource = LstButton;
        }

        /// <summary>
        /// برگرداندن لیست گروه ها که با کاما جدا شذه اند 
        /// جهت ارسال بعنوان پارامتر به استورد پروسجر
        /// </summary>
        /// <returns></returns>
        private string getGroupIDs()
        {
            string strGroupIds = "";
            var UserGroupIDs = this.UserGroupIDs;
            foreach (var group in UserGroupIDs)
            {
                strGroupIds += group.UserGroupID + ",";
            }
            return strGroupIds.Remove(strGroupIds.LastIndexOf(","));
        }

        /// <summary>
        /// جستجوی اخبار در بازه زمانی مشخص
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (!txtFromDate.DateIsValid || !txtToDate.DateIsValid)
            {
                SankaDialog1.ShowMessage("درج تاریخ شروع و پایان الزامی است .",DialogMessage.SankaDialog.Message_Type.Warning,Page);
                return;
            }
            if (txtFromDate.DateMiladi > txtToDate.DateMiladi)
            {
                SankaDialog1.ShowMessage("تاریخ شروع باید کوچکتر از تاریخ پایان باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }
            BindGrid();
        }

        /// <summary>
        /// متد حذف خبر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            long NewsID = e.CommandArgument.ObjectToLong();
            DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews();
            obj.ID = NewsID;
            bool bln = (new DAL.Admin.tblNews.Methods()).Delete(obj);
            if (bln)
            {
                SankaDialog1.ShowMessage("اطلاعیه حذف شد .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                BindGrid();
            }
        }
    }
}