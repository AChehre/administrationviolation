﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="Board.aspx.cs" Inherits="AdministrationViolation.Admin.Board" %>

<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/UCStates.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/Board.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="prevententer">
        <div class="_10bm">
            <label class="form-field-label">
                نوع هیات :</label>
            <asp:DropDownList ID="ddlBoardType" onchange="BoardTypeChange(this)" runat="server">
            </asp:DropDownList>
        </div>
        <div id="ZoneDiv" runat="server">
            <Sanka:States ID="States1" runat="server"></Sanka:States>
        </div>
         <div class="_10bm">
            <label class="form-field-label">
                کد هیات :</label>
            <asp:TextBox ID="txtBoardCode" data-required="true" Width="250px" placeholder="کد 5 رقمی هیات" MaxLength="5" runat="server"></asp:TextBox>
             <asp:HiddenField ID="hfBoardCode" runat="server" />
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                نام هیات :</label>
            <asp:TextBox ID="txtBoardTitle" data-required="true" Width="250px" placeholder="نام هیات" MaxLength="50" runat="server"></asp:TextBox>
        </div>
         <div class="_10bm">
            <label class="form-field-label">
                نشانی هیات :</label>
            <asp:TextBox TextMode="MultiLine" ID="txtAddress" data-required="true" 
                 Width="425px" placeholder="نشانی هیات" MaxLength="255" runat="server" 
                 Height="102px"></asp:TextBox>
        </div>
        <div class="_10bm">
            <label class="form-field-label">
                وضعیت :</label>
            <asp:DropDownList ID="ddlBoardStatus" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
    <asp:HiddenField ID="hfRevisionBoardType" runat="server" />
</asp:Content>
