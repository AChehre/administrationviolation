﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="BoardList.aspx.cs" Inherits="AdministrationViolation.Admin.BoardList" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="SG" %>
<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Board.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="_10bm">
        <label class="form-field-label">
            نوع هیات :</label>
        <asp:DropDownList ID="ddlBoardType" onchange="BoardTypeChange(this)" runat="server">
        </asp:DropDownList>
        <label class="form-field-label _50rmargin">
            وضعیت :</label>
        <asp:DropDownList ID="ddlBoardStatus" runat="server">
        </asp:DropDownList>
    </div>
    <div id="ZoneDiv" runat="server">
        <sanka:States ID="States1" runat="server">
        </sanka:States>
    </div>
    <div class="_10bm">
        <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server"
            Text="فیلتر" />
        <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server"
            Text="نمایش همه" />
    </div>
    <div class="grid">
        <SG:SankaGrid ID="gvBoards" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="BoardID" HeaderText="BoardID" Visible="False" />
                <asp:BoundField DataField="BoardCode" HeaderText="کد هیات" />
                <asp:TemplateField HeaderText="نام هیات">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="BoardTitle" data-sankatooltip="مرتب سازی براساس نام هیات"
                            ToolTip="مرتب سازی براساس نام هیات" CommandName="ASC" ID="lnkbtnBoardTitle" OnCommand="lnkbtnSort_Command"
                            runat="server">نام هیات</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgBoardTitle"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("BoardTitle").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نوع هیات">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="BoardType" data-sankatooltip="مرتب سازی براساس نوع هیات"
                            ToolTip="مرتب سازی براساس نوع هیات" CommandName="ASC" ID="lnkbtnBoardType" OnCommand="lnkbtnSort_Command"
                            runat="server">نوع هیات</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgBoardType"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AdministrationViolation.Admin.Enums.Board_Type_Label[Eval("BoardType").ToString().StringToInt()]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="OstanName" data-sankatooltip="مرتب سازی براساس استان"
                            ToolTip="مرتب سازی براساس استان" CommandName="ASC" ID="lnkbtnOstanName" OnCommand="lnkbtnSort_Command"
                            runat="server">استان</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgOstanName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("OstanName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="وضعیت هیات">
                    <ItemTemplate>
                        <%# AdministrationViolation.Admin.Enums.Board_Status_Label[Eval("Status").ToString().StringToInt() -1]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="اعضا">
                    <ItemTemplate>
                        <span id='sanka_basic_modal'><a href='<%# string.Format("BoardMemberList.aspx?BoardID={0}&action={1}",Eval("BoardID").ToString(), (byte)SankaWebAppFramework.Enums.Page_ActionCode.View ) %>'
                            data-title='<%# string.Format("اعضا[{0}]",Eval("BoardTitle").ToString()) %>'
                            data-duplicatetitle="true" data-width="800" data-height="550" class='basic'>
                            <img src="/images/icon/people.png" /></a> </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="سابقه اعضا">
                    <ItemTemplate>
                        <span id='sanka_basic_modal'><a href='<%# string.Format("SearchBoardMember.aspx?BoardID={0}&action={1}",Eval("BoardID").ToString(), (byte)SankaWebAppFramework.Enums.Page_ActionCode.View ) %>'
                            data-title='<%# string.Format("سابقه اعضا[{0}]",Eval("BoardTitle").ToString()) %>'
                            data-duplicatetitle="true" data-width="900" data-height="550" class='basic'>
                            <img src="/images/icon/id_cards.png" /></a> </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FileCounter" HeaderText="تعداد پرونده" />
                <asp:TemplateField HeaderText="لیست پرونده ها">
                    <ItemTemplate>
                        <a data-save-return-url="true" href='<%# string.Format("/Board/ViolationFiles.aspx?BoardID={0}", Eval("BoardID")) %>'>
                            <img src="/images/icon/document_into.png" /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="حذف پرونده ها" Visible="False">
                    <ItemTemplate>
                        <asp:Button class="btn btn-primary basic" ID="btnDeleteFiles" OnClientClick="return ShowConfirm(this)"
                            OnCommand="btnDeleteFiles_Command" CommandArgument='<%# Eval("BoardID") %>' runat="server"
                            Text="حذف تمام پرونده ها" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="عملیات">
                    <ItemTemplate>
                        <a title="ویرایش" href='<%# string.Format("Board.aspx?ID={0}&action={1}",Eval("BoardID").ToString(), (byte)SankaWebAppFramework.Enums.Page_ActionCode.Edit ) %>'>
                            <asp:Image ID="lnkEdit" runat="server" ImageUrl="~/images/icon/edit.png" ToolTip="ویرایش" />
                        </a><a title="حذف" onclick="return ShowConfirm(this,'در صورت حذف این هیات، برای تمامی اعضای آن نیز غیر قابل دسترس میشود، آیا از حذف این هیات اطمینان دارید؟');"
                            href='<%# string.Format("BoardList.aspx?ID={0}&action={1}", Eval("BoardID"),(byte)SankaWebAppFramework.Enums.Page_ActionCode.Delete)%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon/delete.png" ToolTip="حذف" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </SG:SankaGrid>
    </div>
    <sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </sanka:SankaDialog>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfRevisionBoardType" runat="server" />
</asp:Content>
