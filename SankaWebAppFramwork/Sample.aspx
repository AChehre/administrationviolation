﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Sample.aspx.cs" Inherits="SankaWebAppFramework.Sample" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <link href="css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <asp:Button data-sankatooltip="نمایش پیام در همین صفحه" ID="Button1" CssClass="btn btn-info"
                    runat="server" CommandArgument="1" OnCommand="ShowMessage_Command" Text="نمایش پیام در همین صفحه" />
            </td>
            <td>
                <asp:Button data-sankatooltip="نمایش پیام در همین صفحه به صورت دیالوگ" ID="Button2"
                    CssClass="btn btn-info" runat="server" CommandArgument="2" OnCommand="ShowMessage_Command"
                    Text="نمایش پیام در همین صفحه به صورت دیالوگ" />
            </td>
            <td>
                <asp:Button data-toggle="tooltip" title="رفتن به صفحه ای دیگر و نمایش پیام" ID="Button3"
                    CssClass="btn btn-info" runat="server" CommandArgument="3" OnCommand="ShowMessage_Command"
                    Text="رفتن به صفحه ای دیگر و نمایش پیام" />
            </td>
            <td>
                <asp:Button data-toggle="tooltip" title="رفتن به صفحه ای دیگر و نمایش پیام به صورت دیالوگ"
                    ID="Button4" CssClass="btn btn-info" runat="server" CommandArgument="4" OnCommand="ShowMessage_Command"
                    Text="رفتن به صفحه ای دیگر و نمایش پیام به صورت دیالوگ" />
            </td>
        </tr>
    </table>
    <hr />
    <table>
        <tr>
            <td>
                <a class="btn btn-link" href="Registration.aspx">رفتن به صفحه ای که دسترسی نداریم</a>
            </td>
            <td>
                <a class="btn btn-link" href="Default.aspx">رفتن به صفحه ای که دسترسی داریم</a>
            </td>
            <td>
                <a class="btn btn-link" href="test.aspx">رفتن به صفحه ای که دسترسی داریم ولی پابلیش
                    نشده</a>
            </td>

            <td>
                <a class="btn btn-link" href="MessagingSample.aspx">صفحه سمپل نمایش پیام</a>
            </td>

            <td>
                <asp:Button ID="Button5" OnClientClick="return ShowConfirm(this);" runat="server"
                    Text="کانفیرم باکس" OnClick="Button5_Click" />
            </td>
        </tr>
    </table>
    <hr />
    <div id='sanka_basic_modal'>
        <a href="Default.aspx" data-title="تست" data-duplicatetitle="true" data-width="800"
            data-height="400" class='basic'>تست نمایش آیفریم در مودال</a>
    </div>
    <hr />

    <div class="container">
        <h2>نمونه مراحل فرم های مرحله ای </h2>
        <div>
            <ul class='nav nav-wizard'>

                <li><a href='#step1' data-toggle="tab">گام اول - ایجاد پرونده</a></li>

                <li class='active'><a href='#step2' data-toggle="tab">گام دوم - درج اتهام</a></li>

                <li><a>Step 3 - Not Yet Reached</a></li>

            </ul>
        </div>
    </div>


    <hr />

    <div>
        <div role="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">Home tab</div>
                <div role="tabpanel" class="tab-pane" id="profile">Profile tab</div>
                <div role="tabpanel" class="tab-pane" id="messages">Messages tab</div>
                <div role="tabpanel" class="tab-pane" id="settings">Settings tab</div>
            </div>

        </div>
    </div>

    <hr />


    <div class="container">
        <h2>نمونه فیلد های اجباری و نامعتبر </h2>
        <div>
            <label>فیلد اجباری</label>
            <asp:TextBox data-required="true" ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>فیلد نامعتبر</label>
            <asp:TextBox data-invalid="true" ID="TextBox2" runat="server"></asp:TextBox>
        </div>
        <div>
            <a target="_blank" href="EmptyForm.aspx">سمپل فرم خالی</a>
        </div>
    </div>

    <hr />
    <div class="container">
        <h2>نمونه گرید</h2>
        <div class="grid">
            <table>
                <tr>
                    <td>مقدار یک
                    </td>
                    <td>مقدار دو
                    </td>
                </tr>
                <tr>
                    <td>مقدار سه
                    </td>
                    <td>مقدار چهار
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <hr />
    <div class="container">
        <h2>رنگ فونت متن های مختلف</h2>
        <p>
            Use the contextual classes to provide "meaning through colors":
        </p>
        <p class="text-muted">
            This text is muted.
        </p>
        <p class="text-primary">
            This text is important.
        </p>
        <p class="text-success">
            This text indicates success.
        </p>
        <p class="text-info">
            This text represents some information.
        </p>
        <p class="text-warning">
            This text represents a warning.
        </p>
        <p class="text-danger">
            This text represents danger.
        </p>
    </div>
    <hr />
    <div class="container">
        <h2>رنگ پس زمینه متن های مختلف</h2>
        <p>
            Use the contextual background classes to provide "meaning through colors":
        </p>
        <p class="bg-primary">
            This text is important.
        </p>
        <p class="bg-success">
            This text indicates success.
        </p>
        <p class="bg-info">
            This text represents some information.
        </p>
        <p class="bg-warning">
            This text represents a warning.
        </p>
        <p class="bg-danger">
            This text represents danger.
        </p>
    </div>
    <hr />
    <div class="container">
        <h2>استایل برای عکس ها</h2>
        <img src="images/Koala.jpg" class="img-thumbnail" alt="Cinque Terre" style="width: 204px; height: auto; float: right" />
        <img src="images/Koala.jpg" class="img-circle" alt="Cinque Terre" style="width: 204px; height: auto; float: right" />
        <img src="images/Koala.jpg" class="img-rounded" alt="Cinque Terre" style="width: 204px; height: auto; float: right" />
    </div>
    <hr />
    <div class="container">
        <h2>استایل دکمه ها</h2>
        <button type="button" class="btn btn-default">
            Default</button>
        <button type="button" class="btn btn-primary">
            Primary</button>
        <button type="button" class="btn btn-success">
            Success</button>
        <button type="button" class="btn btn-info">
            Info</button>
        <button type="button" class="btn btn-warning">
            Warning</button>
        <button type="button" class="btn btn-danger">
            Danger</button>
        <button type="button" class="btn btn-link">
            Link</button>
        <h2>اندازه دکمه ها</h2>
        <button type="button" class="btn btn-primary btn-lg">
            Large</button>
        <button type="button" class="btn btn-primary btn-md">
            Medium</button>
        <button type="button" class="btn btn-primary btn-sm">
            Small</button>
        <button type="button" class="btn btn-primary btn-xs">
            XSmall</button>
    </div>
    <div class="container">
        <h2>دکمه های با عرض کامل صفحه</h2>
        <button type="button" class="btn btn-primary btn-block">
            Button 1</button>
        <button type="button" class="btn btn-default btn-block">
            Button 2</button>
        <h2>Large Block Level Buttons</h2>
        <button type="button" class="btn btn-primary btn-lg btn-block">
            Button 1</button>
        <button type="button" class="btn btn-default btn-lg btn-block">
            Button 2</button>
        <h2>Small Block Level Buttons</h2>
        <button type="button" class="btn btn-primary btn-sm btn-block">
            Button 1</button>
        <button type="button" class="btn btn-default btn-sm btn-block">
            Button 2</button>
    </div>
    <div class="container">
        <h2>دکمه های گروهی</h2>
        <div class="btn-group">
            <button type="button" class="btn btn-primary">
                Apple</button>
            <button type="button" class="btn btn-primary">
                Samsung</button>
            <button type="button" class="btn btn-primary">
                Sony</button>
        </div>
    </div>
    <div class="container">
        <h2>دکمه های گروهی عمودی</h2>
        <div class="btn-group-vertical">
            <button type="button" class="btn btn-primary">
                Apple</button>
            <button type="button" class="btn btn-primary">
                Samsung</button>
            <button type="button" class="btn btn-primary">
                Sony</button>
        </div>
    </div>
    <hr />
    <div class="container">
        <h2>علائم خاص</h2>
        <a href="#">News <span class="badge">5</span></a><br>
        <a href="#">Comments <span class="badge">10</span></a><br>
        <a href="#">Updates <span class="badge">2</span></a>
        <br />
        <button type="button" class="btn btn-primary">
            Primary <span class="badge">7</span></button>
    </div>
    <hr />
    <div class="container">
        <h2>لینک های صفحه بندی</h2>
        <p>
            Add class .active to let the user know which page he/she is on:
        </p>
        <ul class="pagination">
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
        </ul>
    </div>
    <hr />
    <div class="container">
        <h2>صفحات قبلی و بعدی</h2>
        <p>
            The .pager class provides previous and next buttons (links):
        </p>
        <ul class="pager">
            <li><a href="#">Previous</a></li>
            <li><a href="#">Next</a></li>
        </ul>
    </div>
    <div class="container">
        <h2>لینک صفحات قبلی و بعدی با فاصله</h2>
        <p>
            The .previous and .next classes align each link to the sides of the page:
        </p>
        <ul class="pager">
            <li class="previous"><a href="#">Previous</a></li>
            <li class="next"><a href="#">Next</a></li>
        </ul>
    </div>
    <hr />
    <div class="container">
        <h2>تگ های اینپوت</h2>
        <div>
            <label for="Name">
                نام</label>
            <input type="text" id="Name" />
        </div>
        <div>
            <label for="pass">
                کلمه عبور</label>
            <input type="password" id="pass" />
        </div>
        <div>
            <select id="sel1">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        </div>
        <div>
            <div class="radio">
                <label>
                    <input type="radio" name="optradio">Option 1</label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="optradio">Option 2</label>
            </div>
            <div class="radio disabled">
                <label>
                    <input type="radio" name="optradio" disabled>Option 3</label>
            </div>
        </div>
        <div>
            <h4>نمایش چند دکمه رادیویی در یک خط</h4>
            <label class="radio-inline">
                <input type="radio" name="optradio" />Option 1
            </label>
            <label class="radio-inline">
                <input type="radio" name="optradio" />Option 2
            </label>
            <label class="radio-inline">
                <input type="radio" name="optradio" />Option 3
            </label>
        </div>
        <div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="">Option 1</label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="">Option 2</label>
            </div>
            <div class="checkbox disabled">
                <label>
                    <input type="checkbox" value="" disabled>Option 3</label>
            </div>
        </div>
        <div>
            <h4>نمایش چند چک باکس در یک خط</h4>
            <label class="checkbox-inline">
                <input type="checkbox" value="">Option 1
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="">Option 2
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="">Option 3
            </label>
        </div>
        <div>
            <textarea rows="5" id="comment"></textarea>
        </div>
    </div>
    <hr />
    <div class="container">
        <br>
        <h2>اسلایدر</h2>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/Koala.jpg" alt="Chania" width="460" height="345" />
                </div>
                <div class="item">
                    <img src="images/Lighthouse.jpg" alt="Chania" width="460" height="345" />
                </div>
                <div class="item">
                    <img src="images/Penguins.jpg" alt="Flower" width="460" height="345" />
                </div>
                <div class="item">
                    <img src="images/Tulips.jpg" alt="Flower" width="460" height="345" />
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span> </a><a class="right carousel-control" href="#myCarousel" role="button"
                    data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span> </a>
        </div>
    </div>
    <hr />
    <div class="container">
        <h2>آکاردیون</h2>
        <div class="create_accordion">
            <h3>Section 1</h3>
            <div>
                <p>
                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque.
                    Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a
                    nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada.
                    Vestibulum a velit eu ante scelerisque vulputate.
                </p>
            </div>
            <h3>Section 2</h3>
            <div>
                <p>
                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus
                    hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum
                    tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
                </p>
            </div>
            <h3>Section 3</h3>
            <div>
                <p>
                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus
                    pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque
                    semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam
                    nisi, eu iaculis leo purus venenatis dui.
                </p>
                <ul>
                    <li>List item one</li>
                    <li>List item two</li>
                    <li>List item three</li>
                </ul>
            </div>
            <h3>Section 4</h3>
            <div>
                <p>
                    Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada
                    fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et
                    ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.
                </p>
                <p>
                    Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus. Class
                    aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                </p>
            </div>
        </div>
    </div>
    <hr />
    <div id='modal_content'>
        <h3></h3>
        <iframe id="sanka_iframe" src=""></iframe>
    </div>
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
