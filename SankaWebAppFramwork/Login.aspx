﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="SankaWebAppFramework.Login" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<%@ Register Src="controls/LoginForm.ascx" TagName="LoginForm" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/Capcha.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:LoginForm ID="LoginForm1" runat="server" />
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static"
        ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
