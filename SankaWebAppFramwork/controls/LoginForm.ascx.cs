﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;
using System.Configuration;
using System.Web.Configuration;

namespace SankaWebAppFramework.controls
{
    public partial class LoginForm : System.Web.UI.UserControl
    {
        public delegate void dlgAfterLogin(UserProfile profile,bool anotherLogonUser);
        public event dlgAfterLogin Login_Succeed;

        public delegate void dlgError(string message);
        public event dlgError Error_Occured;

        /// <summary>
        /// متد ورود کاربر
        /// </summary>
        /// <param name="username">نام کاربری</param>
        /// <param name="password">کلمه عبور</param>
        private void Login(string username, string password)
        {
            bool blnAnotherLogonPerson = false;
            #region اعتبارسنجی

            if (string.IsNullOrEmpty(txtCaptcha.Text))
            {
                Error_Occured("لطفا متن تصویر امنیتی وارد نمایید.");
                return;
            }
            else
            {
                if (HttpContext.Current.Session["Captcha"] == null)
                {
                    Error_Occured("متن تصویر منقضی شده است");
                    return;
                }
                else
                {
                    if (!Session["Captcha"].ToString().Equals(txtCaptcha.Text))
                    {
                        Error_Occured("متن تصویر وارد شده اشتباه است");
                        txtCaptcha.Text = "";
                        return;
                    }
                }
            }

            #endregion اعتبارسنجی

            UserProfile UProfileObj = new UserProfile();

            using (UserManagment UManageObj = new UserManagment())
            {// بررسی اطلاعات کاربر و بدست آوردن اطلاعات او
                UProfileObj = UManageObj.Login(txtUsername.Text, txtPassword.Text.getMD5());
            }

            if (UProfileObj.UserID > 0)
            {
                if (!UProfileObj.IsActive.HasValue || !UProfileObj.IsActive.Value)
                {
                    Error_Occured("کاربر گرامی متاسفانه حساب کاربری شما غیر فعال شده است .");
                    return;
                }

                if (UProfileObj.Online && UProfileObj.SessionExpiration > DateTime.Now)
                {
                    blnAnotherLogonPerson = true;
                }





                //  گرفتن مقدار زمانن تا انقضاء سشن بر حسب دقیقه از داخل وب کانفیگ
                Configuration conf = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
                SessionStateSection section = (SessionStateSection)conf.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes;
                //---------------------
                UProfileObj.SessionExpiration = DateTime.Now.AddMinutes(timeout);
                UProfileObj.SessionTimeOut = timeout;

                // اگر نام کاربری و رمز درست بود
                UProfileObj.CurrentApplicationYear = ddlYear.SelectedValue.StringToInt();

                Session["UserProfile"] = UProfileObj;
                Session.Remove("Capcha");
                Login_Succeed(UProfileObj, blnAnotherLogonPerson);
            }
            else
            {// اگر اطلاعات ورود اشتباه بود
                Error_Occured("نام کاربری یا رمز ورود اشتباه است");
            }

            #region کد قدیمی مستر مسلمی

            //if (HttpContext.Current.Session["Captcha"] == null || !HttpContext.Current.Session["Captcha"].ToString().Equals(txtCaptcha.Text))
            //{
            //    myMessage.ShowMessage("کد امنیتی وارد شده صحیح نمیباشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
            //    txtCaptcha.Focus();
            //    return;
            //}
            //HttpContext.Current.Session.Remove("Capcha");


            //DTO.tblUserGroupMapView.tblUserGroupMapView obj = new DTO.tblUserGroupMapView.tblUserGroupMapView();
            //obj.UserName = username;
            //obj.Password = password;
            //List<DTO.tblUserGroupMapView.tblUserGroupMapView> lst = (new DAL.tblUserGroupMapView.Methods()).Authenticate(obj);

            //if (lst.Count > 0)
            //{
            //    List<DTO.tblUserGroupMap.tblUserGroupMap> lstGroup = new List<DTO.tblUserGroupMap.tblUserGroupMap>();
            //    foreach (DTO.tblUserGroupMapView.tblUserGroupMapView objUser in lst)
            //    {
            //        lstGroup.Add(new DTO.tblUserGroupMap.tblUserGroupMap
            //        {
            //            UserID = objUser.UserID,
            //            UserGroupID = objUser.UserGroupID
            //        });
            //    }
            //    UserProfile profile = new UserProfile
            //    {
            //        UserID = lstGroup[0].UserID.Value,
            //        lstGroupIDs = lstGroup
            //    };
            //    Session["UserProfile"] = profile;


            //var ticket = new FormsAuthenticationTicket(1, // version 
            //                                            txtUsername.Text, // user name
            //                                            DateTime.Now, // create time
            //                                            DateTime.Now.AddDays(1), // expire time
            //                                            chkRememberMe.Checked, // persistent
            //                                            getUserRole(profile.lstGroupIDs)); //role 
            //var strEncryptedTicket = FormsAuthentication.Encrypt(ticket);
            //var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, strEncryptedTicket);
            //Response.Cookies.Add(cookie);

            //Login_Succeed(profile);
            //}
            //else
            //{
            //    myMessage.ShowMessage("نام کاربری یا رمز ورود اشتباه است .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
            //}

            #endregion کد قدیمی مستر مسلمی
        }

        //private string getUserRole(List<DTO.tblUserGroupMap.tblUserGroupMap> list)
        //{
        //    try
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        string[] strUserGroupList = new string[] { "user", "assesmenter", "administrator", "config" };
        //        foreach (DTO.tblUserGroupMap.tblUserGroupMap grp in list)
        //        {
        //            string strGroupName = strUserGroupList[grp.UserGroupID.Value - 1];
        //            sb.Append(strGroupName + ",");
        //        }
        //        return sb.Remove(sb.Length - 1, 1).ToString();
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }
        //}

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Login(txtUsername.Text, txtPassword.Text.getMD5());
        }

        /// <summary>
        /// نمایش سال های شمسی از سال 50 تا سال جاری
        /// </summary>
        void BindYear()
        {
            int YearTo = DateTime.Now.GetPersianYear();

            int YearFrom = 1350;

            for (int i = YearTo; i >= YearFrom; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            ddlYear.SelectedValue = DateTime.Now.GetPersianYear().ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Form.DefaultButton = btnLogin.UniqueID;
            if (!IsPostBack)
            {
                BindYear();
            }
        }
    }
}