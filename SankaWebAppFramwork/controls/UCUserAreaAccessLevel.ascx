﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCUserAreaAccessLevel.ascx.cs"
    Inherits= "SankaWebAppFramework.controls.UCUserAreaAccessLevel" %>
<div class="_10bm">
    <asp:Literal ID="ltMessage" runat="server"></asp:Literal>
</div>
<div class="_10bm">
    <asp:Repeater ID="rptSchools" runat="server">
        <ItemTemplate>
            <div class="rptitem">
                <asp:CheckBox ID="chkItem" Text='<%# Eval("SchoolName") %>' runat="server" />
                <asp:HiddenField ID="hfID" Value='<%# Eval("SchoolCode") %>' runat="server" />
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptRegions" runat="server">
        <ItemTemplate>
            <div class="rptitem">
                <asp:CheckBox ID="chkItem" Text='<%# Eval("Name") %>' runat="server" />
                <asp:HiddenField ID="hfID" Value='<%# Eval("RegionCode") %>' runat="server" />
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptStates" runat="server">
        <ItemTemplate>
            <div class="rptitem">
                <asp:CheckBox ID="chkItem" Text='<%# Eval("Name") %>' runat="server" />
                <asp:HiddenField ID="hfID" Value='<%# Eval("OstanCode") %>' runat="server" />
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div class="_10bm">
    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-info" runat="server"
        Text="ذخیره" />
</div>
<asp:HiddenField ID="hfOstanCode" Value="0" runat="server" />
<asp:HiddenField ID="hfRegionCode" Value="0" runat="server" />
<asp:HiddenField ID="hfSchoolCode" Value="0" runat="server" />
<asp:HiddenField ID="hfUserID" Value="0" runat="server" />
