﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.IO;
using System.ComponentModel;

namespace SankaWebAppFramework
{
    /// <summary>
    /// نوع کنترل تولید شونده در منو اکشن ها
    /// </summary>
    public enum ActionMenuType
    {
        /// <summary>
        /// جدا کننده
        /// </summary>
        Seperator = 0,
        /// <summary>
        /// لینک
        /// </summary>
        HyperLink = 1,
        /// <summary>
        /// ایمیج باتن
        /// </summary>
        ImageButton = 2,
    }

    /// <summary>
    /// کلاس ساخت اکشن منو
    /// </summary>
    public class ActionMenuModel
    {
        /// <summary>
        /// آیدی کنترل
        /// صد در صد اجباری و باید یک آیدی یونیک داده شود
        /// فقط جدا کننده آیدی نمی خواهد
        /// اگر آیدی ندهیم، هنگام کلیک روی متن، کلیک دکمه ها کار نمیکند ، و فقط برای انجام اکشن، باید روی آیکون کلیک کنیم
        /// </summary>
        public string TargetID { get; set; }
        /// <summary>
        /// عنوان
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// نوع کنترل
        /// </summary>
        public ActionMenuType Type { get; set; }
        /// <summary>
        /// آدرس آیکون کنترل
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// آرگومان جهت پاس دادن به متد
        /// dpPostBack
        /// </summary>
        public string Arg { get; set; }

        /// <summary>
        /// آدرس لینک
        /// </summary>
        public string NavigateUrl { get; set; }

        /// <summary>
        /// ایونت کلیک دکمه
        /// </summary>
        public ImageClickEventHandler button_click { set; get; }

        /// <summary>
        /// آیا نیاز به انجام پست بک از سمت کاربر وجود دارد یا نه
        /// </summary>
        public bool ClientdoPostBack { set; get; }

        /// <summary>
        /// نحوه آیدی دادن به کنترل استاتیک باشد یا داینامیک
        /// </summary>
        [DefaultValue(System.Web.UI.ClientIDMode.Static)]
        public System.Web.UI.ClientIDMode clientIDMode { set; get; }

        /// <summary>
        /// نام گروه جهت اعتبارسنجی کنترل های فرم
        /// </summary>
        public string ValidationGroup { set; get; }

        /// <summary>
        /// اعتبارسنجی را انجام دهد یا خیر
        /// </summary>
        public bool CausesValidation { set; get; }

        /// <summary>
        /// نام تابع جهت فراخوانی روی خاصیت کلیک سمت کلاینت
        /// </summary>
        public string OnClientClick { set; get; }

        /// <summary>
        /// ویژگی های مربوط به کنترل
        /// Attributes
        /// </summary>
        public Dictionary<string, string> Attributes;

        /// <summary>
        /// نمایش یا عدم نمایش کنترل
        /// </summary>
        //public bool Visible {set;get;}

        /// <summary>
        /// لینک در تب جدید باز شود
        /// </summary>
        public bool OpenNewTab { set; get; }
    }

    public partial class UCActionMenu : System.Web.UI.UserControl
    {
        /// <summary>
        /// لیستی از کنترل های جهت ساخت اکشن منو
        /// </summary>
        public List<ActionMenuModel> DataSource
        {
            set;
            get;
        }

        /// <summary>
        /// آیای منو از فایل ایکس ام ال ساخته شود ؟
        /// </summary>
        public bool LoadFromXml { get; set; }

        /// <summary>
        /// آدرس فایل ایکس ام ال جهت ساخت منو
        /// </summary>
        public string XmlPath { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // نکته: فایل جی کوئری حتما باید در ابتدا هدر صفحه اضافه شده باشد
            // اضافه کردن فایل های جاوا اسکریپت و سی اس اس موردنیاز به صفحه
            Utility.AddFileToPageHeader("~/css/ActionMenu.css", this.Page, Utility.AddedFileType.Css);
            Utility.AddFileToPageHeader("~/js/ActionMenu.js", this.Page, Utility.AddedFileType.Js);

            if (LoadFromXml)
            {
                // پرکردن لیست با مقادیر داخل فایل ایکس ام ال
                LoadXml();
            }

            //ساخت منو
            CreateMenu();
        }

        /// <summary>
        /// ساخت منو
        /// </summary>
        public void CreateMenu()
        {
            phMenuContainer.Controls.Clear();

            if (DataSource == null)
            {// اگر لیست خالی بود، عملی انجام نشود
                return;
            }

            foreach (ActionMenuModel Item in DataSource)
            {
                if (Item.Type == ActionMenuType.Seperator)
                {// اگر جدا کننده باشد
                    #region جداکننده

                    Image imgsp = new Image();
                    imgsp.ImageUrl = Item.Icon;
                    imgsp.CssClass = "item-sep";
                    phMenuContainer.Controls.Add(imgsp);

                    #endregion جداکننده
                }
                else if (Item.Type == ActionMenuType.HyperLink)
                {// اگر هایپرلینک باشد
                    #region هایپرلینک

                    Panel pnlItemContainer = new Panel();

                    HyperLink hlAction = new HyperLink();
                    hlAction.ID = Item.TargetID;
                    hlAction.Text = Item.Title;
                    hlAction.ToolTip = Item.Title;
                    hlAction.ImageUrl = Item.Icon;
                    hlAction.NavigateUrl = Item.NavigateUrl;
                    hlAction.ClientIDMode = Item.clientIDMode;
                    if (Item.OpenNewTab)
                    {
                        hlAction.Target = "_blank";
                    }
                    if (Item.ClientdoPostBack)
                    {// اگر دو پست بک ترو باشد، به آن کلیک کنترل اضافه شود
                        hlAction.Attributes.Add("onclick", "javascript:__doPostBack('" + hlAction.ClientID + "','" + Item.Arg + "');return false;");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Item.OnClientClick))
                        {// اگر مقداری در کلاینت کلیک وجود داشت،به رویداد کلیک هایپرلینک انتصاب میدهیم
                            hlAction.Attributes.Add("onclick", Item.OnClientClick);
                        }
                    }

                    // اضافه کردن ویژگی های کنترل
                    if (Item.Attributes != null)
                    {
                        foreach (var attr in Item.Attributes)
                        {
                            hlAction.Attributes.Add(attr.Key, attr.Value);
                        }
                    }

                    Label lblTitle = new Label();

                    lblTitle.Text = Item.Title;
                    lblTitle.CssClass = "item-title";
                    pnlItemContainer.Controls.Add(hlAction);
                    pnlItemContainer.Controls.Add(lblTitle);
                    pnlItemContainer.CssClass = "item-container";
                    pnlItemContainer.Attributes.Add("onclick", "SelectMenu(this,'" + hlAction.ClientID + "')");
                    phMenuContainer.Controls.Add(pnlItemContainer);
                    #endregion هایپرلینک
                }
                else if (Item.Type == ActionMenuType.ImageButton)
                {// اگر ایمیج باتن باشد
                    #region ایمیج باتن

                    Panel pnlItemContainer = new Panel();

                    ImageButton ibtnAction = new ImageButton();
                    ibtnAction.ID = Item.TargetID;
                    ibtnAction.ClientIDMode = Item.clientIDMode;
                    ibtnAction.ToolTip = Item.Title;
                    ibtnAction.ImageUrl = Item.Icon;
                    ibtnAction.CommandArgument = Item.Arg;
                    ibtnAction.ValidationGroup = Item.ValidationGroup;
                    ibtnAction.CausesValidation = Item.CausesValidation;
                    ibtnAction.OnClientClick = Item.OnClientClick;
                    if (!Item.ClientdoPostBack)
                    {// اگر دو پست بک فالس باشد، به رویداد کلیک سروری آن تابع را ست میکنیم
                        ibtnAction.Click += Item.button_click;
                    }
                    else
                    {// اگر دو پست بک ترو باشد، به رویداد کلیک جاوااسکریپتی آن،تابع دوپست بک را ست میکنیم
                        ibtnAction.OnClientClick = "javascript:__doPostBack('" + ibtnAction.ClientID + "','" + Item.Arg + "');return false;";
                    }

                    // اضافه کردن ویژگی های کنترل
                    if (Item.Attributes != null)
                    {
                        foreach (var attr in Item.Attributes)
                        {
                            ibtnAction.Attributes.Add(attr.Key, attr.Value);
                        }
                    }

                    Label lblTitle = new Label();

                    lblTitle.Text = Item.Title;
                    lblTitle.CssClass = "item-title";
                    pnlItemContainer.Controls.Add(ibtnAction);
                    pnlItemContainer.Controls.Add(lblTitle);
                    pnlItemContainer.CssClass = "item-container";
                    pnlItemContainer.Attributes.Add("onclick", "SelectMenu(this,'" + ibtnAction.ClientID + "')");
                    phMenuContainer.Controls.Add(pnlItemContainer);

                    #endregion ایمیج باتن
                }
            }
        }

        /// <summary>
        /// ساخت منو از روی فایل ایکسا ام ال
        /// </summary>
        protected void LoadXml()
        {
            if (DataSource != null)
            {// اگر لیست مقدار داشته باشد، خالی شود
                DataSource.Clear();
            }
            else
            {// در صورتی که لیست تعریف نشده، تعریف شود
                DataSource = new List<ActionMenuModel>();
            }

            try
            {
                #region خواندن فایل ایکس ام ال و ساخت لیست برای ساخت منو

                DataSet ds = new DataSet();
                // خواندن و قراردادن مقادیر فایل ایکس ام ال در جدول
                ds.ReadXml(Server.MapPath(XmlPath));

                if (ds.Tables.Count < 1)
                {// اگر مقداری از فایل ایکس ام ال لود نشده بود، کاری انجام نشود
                    return;
                }

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ActionMenuModel ObjMenu = new ActionMenuModel();
                    ObjMenu.Arg = dr["Arg"].ToString();
                    ObjMenu.CausesValidation = dr["CausesValidation"].ToString().StringToBool();
                    ObjMenu.ClientdoPostBack = dr["ClientdoPostBack"].ToString().StringToBool();
                    ObjMenu.clientIDMode = dr["clientIDMode"].ToString().StringToClientIDMode();
                    ObjMenu.Icon = dr["Icon"].ToString();
                    ObjMenu.NavigateUrl = dr["NavigateUrl"].ToString();
                    ObjMenu.TargetID = dr["TargetID"].ToString();
                    ObjMenu.Title = dr["Title"].ToString();

                    #region تشخیص نوع کنترل

                    byte Type = dr["Type"].ToString().StringToByte();
                    if (Type == (byte)ActionMenuType.HyperLink)
                    {
                        ObjMenu.Type = ActionMenuType.HyperLink;
                    }
                    else if (Type == (byte)ActionMenuType.Seperator)
                    {
                        ObjMenu.Type = ActionMenuType.Seperator;
                    }
                    else if (Type == (byte)ActionMenuType.ImageButton)
                    {
                        ObjMenu.Type = ActionMenuType.ImageButton;
                    }

                    #endregion تشخیص نوع کنترل

                    ObjMenu.ValidationGroup = dr["ValidationGroup"].ToString();

                    //اضافه کردن به لیست
                    DataSource.Add(ObjMenu);
                }

                #endregion خواندن فایل ایکس ام ال و ساخت لیست برای ساخت منو
            }
            catch
            {

            }
        }
    }
}