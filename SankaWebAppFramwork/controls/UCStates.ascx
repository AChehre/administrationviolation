﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCStates.ascx.cs" Inherits= "SankaWebAppFramework.controls.UCStates" %>
<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div class="updatepnl-waiting">
                        <asp:Image CssClass="waitimg" ID="imgUpdateProgress" runat="server" ImageUrl="~/images/loading.gif"
                            AlternateText="لطفا منتظر بمانید ..." ToolTip="لطفا منتظر بمانید ..." />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="_10bm" runat="server" id="OstanDiv" >
                <label class="form-field-label">
                    استان :</label>
                <asp:DropDownList ID="ddlState" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    runat="server" ondatabound="ddlState_DataBound">
                    <asp:ListItem Text="انتخاب استان" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </div>
           <%-- <div class="_10bm" runat="server" id="RegionDiv" >
                <label id="lblRegion" runat="server" class="form-field-label">
                    منطقه :</label>
                <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"
                    runat="server">
                    <asp:ListItem Text="انتخاب منطقه" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="_10bm" runat="server" id="SchoolDiv" >
                <label class="form-field-label">
                    مدرسه :</label>
                <asp:DropDownList ID="ddlSchool" Enabled="false" runat="server">
                    <asp:ListItem Text="انتخاب مدرسه" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
