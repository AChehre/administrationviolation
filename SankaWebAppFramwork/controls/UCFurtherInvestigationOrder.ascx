﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFurtherInvestigationOrder.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCFurtherInvestigationOrder" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                دستور رسیدگی مجدد پرونده</h4>
        </div>
        <div class="panel-body prevententer">
            <asp:Panel ID="divAccusitionField" runat="server" ClientIDMode="Static">
                <div class="_5p">
                    <label class="form-field-label">
                        سال عملیاتی :</label>
                    <asp:DropDownList ID="ddlApplicationYear" runat="server" Width="100px" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        نام هیات مبداء :</label>
                    <asp:DropDownList ID="ddlFromBoard" runat="server" Width="250px" AutoPostBack="True"
                        Enabled="false" OnSelectedIndexChanged="ddlFromBoard_SelectedIndexChanged" OnDataBound="ddlFromBoard_DataBound">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        لیست پرونده ها :</label>
                    <asp:DropDownList RepeatLayout="Flow" ID="ddlViolationFile" runat="server" Width="200">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        نام هیات مقصد :</label>
                    <asp:DropDownList ID="ddlToBoard" runat="server" Width="250px">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        نوع پرونده ارجاعی :</label>
                    <asp:DropDownList ID="ddlFileReferType" runat="server" Width="250px">
                        <asp:ListItem Text="اعتراض به رای قابل پژوهش" Selected="True" Value="1" />
                        <asp:ListItem Text="اعتراض رای ماده 12" Value="3" />
                        <asp:ListItem Text="اعمال ماده 11" Value="4" />
                       <asp:ListItem Text="اعمال تبصره 4 ماده 22" Value="5" />
                        <asp:ListItem Value="6">ماده 24 قانون</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        توضیحات :</label>
                    <asp:TextBox TextMode="MultiLine" Style="text-align: right" runat="server" ID="txtComment"
                        Height="93px" Width="499px"></asp:TextBox>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
        <uc2:sankafileupload id="FileUpload1" runat="server" />
    </asp:Panel>
    <div class="_5p even-row">
        <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
            ClientIDMode="Static">
        </cc1:SankaDialog>
    </div>
</div>
