﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCStates : System.Web.UI.UserControl
    {
        #region پروپرتی ها

        /// <summary>
        /// فعال بودن یا نبودن دراپ دوان استان ها
        /// </summary>
        public bool? EnableOstan
        {
            get;
            set;
        }

        /// <summary>
        /// فعال بودن یا نبودن دراپ دوان منطقه ها
        /// </summary>
        public bool? EnableRegion
        {
            get;
            set;
        }

        /// <summary>
        /// فعال بودن یا نبودن دراپ دوان مدرسه ها
        /// </summary>
        public bool? EnableSchool
        {
            get;
            set;
        }

        /// <summary>
        /// وضعیت نمایش دراپ دوان استان ها
        /// </summary>
        public bool? VisibleOstan
        {
            get;
            set;
        }

        /// <summary>
        /// وضعیت نمایش دراپ دوان منطقه ها
        /// </summary>
        public bool? VisibleRegion
        {
            get;
            set;
        }

        /// <summary>
        /// وضعیت نمایش دراپ دوان مدرسه ها
        /// </summary>
        public bool? VisibleSchool
        {
            get;
            set;
        }

        /// <summary>
        /// آیدی استان
        /// </summary>
        public int OstanCode
        {
            get
            {
                return ddlState.SelectedValue.StringToInt();
            }
            set
            {
                if (value > 0)
                {
                    ddlState.SelectedValue = value.ToString();
                }
            }
        }

        ///// <summary>
        ///// کد منطقه
        ///// </summary>
        //public int RegionCode
        //{
        //    get
        //    {
        //        return ddlRegion.SelectedValue.StringToInt();
        //    }
        //    set
        //    {
        //        if (value > 0)
        //        {
        //            ddlRegion.SelectedValue = value.ToString();
        //            ddlRegion.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlRegion.Enabled = false;
        //        }
        //    }
        //}

        ///// <summary>
        ///// آیدی مدرسه
        ///// </summary>
        //public int SchoolCode
        //{
        //    get
        //    {
        //        return ddlSchool.SelectedValue.StringToInt();
        //    }
        //    set
        //    {
        //        if (value > 0)
        //        {
        //            ddlSchool.SelectedValue = value.ToString();
        //            ddlSchool.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlSchool.Enabled = false;
        //        }
        //    }
        //}

        /// <summary>
        /// ایندکس استان انتخابی در کمبوباکس
        /// </summary>
        public int OstanCodeSelectedIndex
        {
            get
            {
                return ddlState.SelectedIndex;
            }
            set
            {
                ddlState.SelectedIndex = value;
            }
        }

        ///// <summary>
        ///// ایندکس منطقه انتخابی در کمبوباکس
        ///// </summary>
        //public int RegionCodeSelectedIndex
        //{
        //    get
        //    {
        //        return ddlRegion.SelectedIndex;
        //    }
        //    set
        //    {
        //        ddlRegion.SelectedIndex = value;
        //    }
        //}

        ///// <summary>
        ///// ایندکس مدرسه انتخابی در کمبوباکس
        ///// </summary>
        //public int SchoolCodeSelectedIndex
        //{
        //    get
        //    {
        //        return ddlSchool.SelectedIndex;
        //    }
        //    set
        //    {
        //        ddlSchool.SelectedIndex = value;
        //    }
        //}

        /// <summary>
        /// آیدی کاربر
        /// </summary>
        public long UserID
        {
            get;
            set;
        }

        /// <summary>
        /// آیدی استان کاربری که لاگین کرده است
        /// </summary>
        public int CurrentUserOstanCode
        {
            get;
            set;
        }

        /// <summary>
        /// کد منطقه کاربری که لاگین کرده است
        /// </summary>
        public int CurrentUserRegionCode
        {
            get;
            set;
        }

        /// <summary>
        /// آیدی مدرسه کاربری که لاگین کرده است
        /// </summary>
        public int CurrentUserSchoolCode
        {
            get;
            set;
        }

        /// <summary>
        /// ست کردن عرض کمبوباکس استان
        /// </summary>
        [Browsable(true)]
        public int OstanWidth
        {
            set
            {
                ddlState.Width = value;
            }
        }

        ///// <summary>
        ///// ست کردن عرض کمبوباکس منطقه
        ///// </summary>
        //[Browsable(true)]
        //public int RegionWidth
        //{
        //    set
        //    {
        //        ddlRegion.Width = value;
        //    }
        //}

        ///// <summary>
        ///// ست کردن عرض کمبوباکس مدرسه
        ///// </summary>
        //[Browsable(true)]
        //public int SchoolWidth
        //{
        //    set
        //    {
        //        ddlSchool.Width = value;
        //    }
        //}

        /// <summary>
        /// اگر به ترو ست شود، کمبوهای استان و منطقه را در یک سطر نمایش میدهد 
        /// </summary>
        [Browsable(true)]
        public bool DispalyInLine
        {
            set
            {
                if (value)
                {
                    OstanDiv.Attributes.Add("class", "displayinline");
                    OstanDiv.Attributes.Add("style", "padding: 2px;");
                    //RegionDiv.Attributes.Add("class", "displayinline");
                    //RegionDiv.Attributes.Add("style", "padding: 2px;");
                    //lblRegion.Attributes.Add("class", "form-field-label _50rmargin");
                    //SchoolDiv.Attributes.Add("style", "clear: both;");
                }
            }
        }

        #endregion پروپرتی ها

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    Initialize();
            //}
        }

        public void Initialize()
        {
            BindStates(ddlState);
            /*
            #region نمایش مقادیر استان، منطقه و مدرسه در صورت ست شدن آیدی آنها
            
            if (OstanCode > 0)
            {// اگر آیدی استان ست شود، استان انتخاب شود و لیست مناطقش نمایش داده شود
                ddlState.SelectedValue = OstanCode.ToString();
                ddlState_SelectedIndexChanged(null, null);

                if (!EnableOstan.HasValue)
                {
                    if (CurrentUserOstanCode > 0)
                    {// به طور پیش فرض، اگر مقدار آیدی استان کاربر لاگین کرده ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                        //ddlState.Enabled = false;
                    }
                }
                else
                {
                    ddlState.Enabled = EnableOstan.Value;
                }

                if (RegionCode > 0)
                {// اگر آیدی منطقه ست شده باشد، منطقه را انتخاب کنیم از لیست و مدرسه های مربوط به آن را نمایش دهیم
                    ddlRegion.SelectedValue = RegionCode.ToString();
                    ddlRegion_SelectedIndexChanged(null, null);

                    if (!EnableRegion.HasValue)
                    {
                        if (CurrentUserRegionCode > 0)
                        {// به طور پیش فرض، اگر مقدار آیدی منطقه کاربر لاگین کرده ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                            //ddlRegion.Enabled = false;
                        }
                    }
                    else
                    {
                        ddlRegion.Enabled = EnableRegion.Value;
                    }

                    if (SchoolCode > 0)
                    {// اگر آیدی مدرسه ست شده باشد، از لیست انتخاب شود

                        if (!EnableSchool.HasValue)
                        {
                            if (CurrentUserSchoolCode > 0)
                            {// به طور پیش فرض، اگر مقدار آیدی مدرسه ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                                //ddlSchool.Enabled = false;
                            }
                        }
                        else
                        {
                            ddlSchool.Enabled = EnableSchool.Value;
                        }

                        ddlSchool.SelectedValue = SchoolCode.ToString();
                    }
                }
            }

            #endregion نمایش مقادیر استان، منطقه و مدرسه در صورت ست شدن آیدی آنها

            // ست کردن وضعیت نمایش کمبوها
            if (VisibleOstan.HasValue)
            {
                OstanDiv.Visible = VisibleOstan.Value;
            }
            if (VisibleRegion.HasValue)
            {
                RegionDiv.Visible = VisibleRegion.Value;
            }
            if (VisibleSchool.HasValue)
            {
                SchoolDiv.Visible = VisibleSchool.Value;
            }
            */
        }

        /// <summary>
        /// نمایش مناطق مربوط به استان انتخاب شده ، هنگام تغییر استان انتخابی
        /// </summary>
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindRegion(ddlRegion, ddlState.SelectedValue.StringToInt());
            //ddlRegion.Enabled = true;
            //ddlSchool.Enabled = false;
            //ddlSchool.SelectedIndex = -1;

        }

        ///// <summary>
        ///// نمایش مدرسه های مربوط به منطقه انتخاب شده ، هنگام تغییر منطقه انتخابی
        ///// </summary>
        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindSchool(ddlSchool, ddlRegion.SelectedValue.StringToInt());
        //    ddlSchool.Enabled = true;
        //}

        /// <summary>
        /// پرکردن دراپ دان استان ها
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست استان ها را نمایش دهد</param>
        protected void BindStates(DropDownList ddl)
        {
            DTO.Config.tblOstans.tblOstans obj = new DTO.Config.tblOstans.tblOstans();
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblOstans.Methods()).SelectAllUserOstans(obj);
            ddl.DataTextField = "Name";
            ddl.DataValueField = "OstanCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب استان", "-1"));
            ddl.SelectedIndex = 0;
        }

        /// <summary>
        /// پرکردن دراپ دان مناطق
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست منطقه ها را نمایش دهد</param>
        protected void BindRegion(DropDownList ddl, int OstanCode)
        {
            DTO.Config.tblRegions.tblRegions obj = new DTO.Config.tblRegions.tblRegions();
            obj.OstanCode = OstanCode;
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblRegions.Methods()).SelectAllUserRegions(obj);
            ddl.DataTextField = "Name";
            ddl.DataValueField = "RegionCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب منطقه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان مدارس
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست مدرسه ها را نمایش دهد</param>
        protected void BindSchool(DropDownList ddl, int RegionCode)
        {
            DTO.Config.tblSchool.tblSchool obj = new DTO.Config.tblSchool.tblSchool();
            obj.RegionCode = RegionCode;
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblSchool.Methods()).SelectAllUserSchools(obj);
            ddl.DataTextField = "SchoolName";
            ddl.DataValueField = "SchoolCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب مدرسه", "-1"));
        }

        protected void ddlState_DataBound(object sender, EventArgs e)
        {
            ddlState_SelectedIndexChanged(sender, e);
        }
    }
}