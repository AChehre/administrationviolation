﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCSelectMultiItems : System.Web.UI.UserControl
    {
        /// <summary>
        /// کلاس مدل جهت آیتم های کنترل
        /// </summary>
        public class ListItem
        {
            /// <summary>
            /// آیدی آیتم
            /// </summary>
            public long ID { get; set; }
            /// <summary>
            /// عنوان آیتم
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// توضیحات آیتم
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// وضعیت آیتم، انتخاب شده یا نشده
            /// </summary>
            public bool Status { get; set; }
        }

        /// <summary>
        /// لیست ورودی آیتم های انتخاب شده
        /// </summary>
        public List<ListItem> SelectedItems
        {
            get;
            set;
        }

        /// <summary>
        /// لیست ورودی آیتم های انتخاب نشده
        /// </summary>
        public List<ListItem> DeSelectedItems
        {
            get;
            set;
        }

        /// <summary>
        /// گرفتن آیدی آیتم های انتخاب شده به صورت جدا شده با کاما
        /// </summary>
        public string GetSelectedIDs
        {
            get
            {
                return hfLeftControlsID.Value;
            }
        }

        /// <summary>
        /// گرفتن آیدی آیتم های انتخاب شده به صورت آرایه
        /// </summary>
        public string[] GetSelectedIDsArray
        {
            get
            {
                return hfLeftControlsID.Value.Split(',');
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public bool Lock
        {
            get
            {
                if (ViewState["Lock"] != null)
                {
                    return ViewState["Lock"].ToString().StringToBool();
                }
                return false;
            }
            set
            {
                ViewState.Add("Lock", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowItems();
        }

        /// <summary>
        /// نمایش آیتم ها روی صفحه
        /// </summary>
        public void ShowItems()
        {
            // نمایش آیتم های انتخاب شده
            rptLeftPanel.DataSource = SelectedItems;
            rptLeftPanel.DataBind();
            if (SelectedItems != null)
            {
                hfLeftControlsID.Value = "";
                foreach (ListItem li in SelectedItems)
                {
                    hfLeftControlsID.Value += li.ID.ToString() + ",";
                }
                if (hfLeftControlsID.Value.Length > 1)
                {
                    hfLeftControlsID.Value = hfLeftControlsID.Value.Remove(hfLeftControlsID.Value.Length - 1, 1);
                }
            }

            // نمایش آیتم های انتخاب نشده
            rptRightPanel.DataSource = DeSelectedItems;
            rptRightPanel.DataBind();
        }
    }
}