﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCAddViolationFile : GeneralControl
    {
        public delegate void dlgAddViolation_Completed(long FileID,string strCreationDate);
        public delegate void dlgAddViolation_Error(List<string> lstMessages);
        public event dlgAddViolation_Error Error_Occured;
        public event dlgAddViolation_Completed AddViolation_Completed;
        /// <summary>
        /// سال عملیاتی
        /// </summary>
        public int? CurrentApplicationYear
        {
            get
            {
                if (ViewState["CurrentApplicationYear"] != null)
                {
                    return ViewState["CurrentApplicationYear"].ToString().StringToInt();
                }
                return null;
            }
            set
            {
                ViewState["CurrentApplicationYear"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindActionMenu();
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();
            ActionMenuModel item = null;

            item = new ActionMenuModel()
            {
                TargetID = "btnSaveNewFileData",
                Title = "ذخیره پرونده جدید",
                Type = ActionMenuType.ImageButton,
                button_click = SaveNewFileData,
                OnClientClick = "return ValidateRequiredField();",
                Icon = "/images/icon/save.png"
            };
            lstAction.Add(item);

            item = new ActionMenuModel()
            {
                Icon = "/images/icon/back.png",
                TargetID = "btnBack",
                Title = "بازگشت",
                Type = ActionMenuType.HyperLink,
                NavigateUrl = "/board/ViolationFiles.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View + "&BoardID=" + BoardID
            };
            lstAction.Add(item);
            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// ذخیره پرونده اتهام
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveNewFileData(object sender, EventArgs e)
        {
            ExistAnotherRelatedOpenFile();

            if(!ValidateData())
            {
                return;
            }

            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();
            obj.BoardID = BoardID;
            obj.PersonnelCode = txtPersonnelCode.Text.StringToLong();
            obj.FirstName = txtFName.Text.CorrectArabicChars();
            obj.LastName = txtLName.Text.CorrectArabicChars();
            obj.FatherName = txtFatherName.Text.CorrectArabicChars();
            obj.CreationDate = txtCreationDate.DateMiladi;
            obj.NationalCode = txtNationalCode.Text;
            obj.OstanName = txtOstanName.Text.CorrectArabicChars();
            obj.RegionName = txtRegionName.Text.CorrectArabicChars();
            obj.SchoolName = txtSchoolName.Text.CorrectArabicChars();
            obj.Status = null;  //تشکیل شده - فاقد اتهام
            obj.SerialCode = "0";
            obj.ActivityYear = CurrentApplicationYear.Value;
            obj.FileReferType = getReferType();

            obj.OstanCode = hfOstanCode.Value.StringToInt();
            obj.RegionCode = hfRegionCode.Value.StringToInt();
            obj.SchoolCode = hfSchoolCode.Value.StringToInt();

            long FileID = (new DAL.Board.tblViolationFile.Methods()).Insert(obj);
            if (FileID > 0)
            {
                AddViolation_Completed(FileID,txtCreationDate.DateShamsi);
            }
        }

        private bool ValidateData()
        {
            List<string> lstWarnning = new List<string>();
            if (!txtCreationDate.DateIsValid)
            {
                lstWarnning.Add("تاریخ تشکیل پرونده را وارد نمایید");
                return false;
            }
            if (txtPersonnelCode.IsEmpty())
            {
                lstWarnning.Add("تاریخ تشکیل پرونده را وارد نمایید");
                return false;
            }
            if (txtFName.IsEmpty())
            {
                lstWarnning.Add("نام متهم را وارد نمایید");
            }
            if (txtLName.IsEmpty())
            {
                lstWarnning.Add("نام خانوادگی  متهم را وارد نمایید");
            }
            if (txtFatherName.IsEmpty())
            {
                lstWarnning.Add("نام پدر را وارد نمایید");
            }
            this.Error_Occured(lstWarnning);
            return !lstWarnning.Any();
        }

        /// <summary>
        /// گرفتن نوع ارجاع
        /// </summary>
        /// <returns></returns>
        private byte getReferType()
        {
            if (UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                return 2; // اعتراض حکم اخراج ماده ی 17
            }
            return 0;
        }

        /// <summary>
        /// چک کردن اینکه ایا متهم پرونده باز دیگری در سیستم دارد 
        /// </summary>
        private void ExistAnotherRelatedOpenFile()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile { PersonnelCode = txtPersonnelCode.Text.StringToLong() , NationalCode = txtNationalCode.Text , BoardID = BoardID };
            var lst = (new DAL.Board.tblViolationFile.Methods()).HasAccusedAnotherOpenFile(obj);
            if (lst.Any())
            {
                string strUrl = (string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", lst[0].FileID, lst[0].BoardID));
                SankaDialog1.ShowMessage("متهم قبلا پرونده مفتوحی به قرار ذیل در این هیات دارد . ", DialogMessage.SankaDialog.Message_Type.Warning, strUrl, true);
            }
        }

        /// <summary>
        /// پاک کردن مقدار کنترل های یوزرکنترل
        /// </summary>
        public void ClearControls()
        {
            txtFatherName.Clear();
            txtFName.Clear();
            txtLName.Clear();
            txtPersonnelCode.Clear();
            hfRelatedPersonFullName.Clear();
            txtNationalCode.Clear();
            txtOstanName.Clear();
            txtRegionName.Clear();
            txtSchoolName.Clear();
        }

        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            var obj = new DTO.Board.tblPersonnelView.tblPersonnelView
            {
                PersonnelCode = txtPersonnelCode.Text.StringToLong()
            };
            obj = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(obj);
            if (obj.RowNumber > 0)
            {
                txtFName.Text = obj.FirstName;
                txtLName.Text = obj.LastName;
                txtFatherName.Text = obj.FatherName;
                txtPersonnelCode.Text = obj.PersonnelCode.ToString();
                txtNationalCode.Text = obj.NationalCode;
                txtOstanName.Text = obj.OstanName;
                txtRegionName.Text = obj.RegionName;
                txtSchoolName.Text = obj.SchoolName;
                hfOstanCode.Value = obj.OstanCode.ToString();
                hfRegionCode.Value = obj.RegionCode.ToString();
                hfSchoolCode.Value = obj.SchoolCode.ToString();
            }
            else
            {
                txtFName.Clear();
                txtLName.Clear();
                txtFatherName.Clear();
                txtPersonnelCode.Clear();
                txtNationalCode.Clear();
                txtOstanName.Clear();
                txtRegionName.Clear();
                txtSchoolName.Clear();
                hfOstanCode.Clear();
                hfRegionCode.Clear();
                hfSchoolCode.Clear();
                SankaDialog1.ShowMessage("مشخصات متهم در سیستم یافت نشد", DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
        }

        /// <summary>
        /// جستجو پرونده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchNationalCode_Click(object sender, ImageClickEventArgs e)
        {
            var obj = new DTO.Board.tblPersonnelView.tblPersonnelView
            {
                NationalCode = txtNationalCode.Text
            };
            obj = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(obj);
            if (obj.RowNumber > 0)
            {
                txtFName.Text = obj.FirstName;
                txtLName.Text = obj.LastName;
                txtFatherName.Text = obj.FatherName;
                txtPersonnelCode.Text = obj.PersonnelCode.ToString();
                txtNationalCode.Text = obj.NationalCode;
                txtOstanName.Text = obj.OstanName;
                txtRegionName.Text = obj.RegionName;
                txtSchoolName.Text = obj.SchoolName;
                hfOstanCode.Value = obj.OstanCode.ToString();
                hfRegionCode.Value = obj.RegionCode.ToString();
                hfSchoolCode.Value = obj.SchoolCode.ToString();
            }
            else
            {
                txtFName.Clear();
                txtLName.Clear();
                txtFatherName.Clear();
                txtPersonnelCode.Clear();
                txtNationalCode.Clear();
                txtOstanName.Clear();
                txtRegionName.Clear();
                txtSchoolName.Clear();
                hfOstanCode.Clear();
                hfRegionCode.Clear();
                hfSchoolCode.Clear();
                SankaDialog1.ShowMessage("مشخصات متهم در سیستم یافت نشد", DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
        }
    }
}