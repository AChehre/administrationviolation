﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCVoteNotification : GeneralControl
    {
        #region تعریف رویدادها

        public delegate void dlgAddNotification_Completed(bool Status);
        public event dlgAddNotification_Completed AddNotification_Completed;

        public delegate void dlgCancel_Click();
        public event dlgCancel_Click Cancel_Click;

        public delegate void dlgError(List<string> Message);
        public event dlgError Error_Occured;

        #endregion تعریف رویدادها

        #region پروپرتی ها

        /// <summary>
        /// شناسه ابلاغیه
        /// </summary>
        protected long NotificationID
        {
            get
            {
                if (ViewState["NotificationID"] != null)
                {
                    return ViewState["NotificationID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("NotificationID", value);
            }
        }

        /// <summary>
        /// آیا دکمه های عملیاتی ، مثلا ارسال فایل و حذف از گرید نمایش داده شود یا نه
        /// </summary>
        public bool HideActionButtons
        {
            get
            {
                if (ViewState["HideActionButtons"] != null)
                {
                    return ViewState["HideActionButtons"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("HideActionButtons", value);
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        /// <summary>
        /// سال عملیاتی
        /// </summary>
        public int? CurrentApplicationYear
        {
            get
            {
                if (ViewState["CurrentApplicationYear"] != null)
                {
                    return ViewState["CurrentApplicationYear"].ToString().StringToInt();
                }
                return null;
            }
            set
            {
                ViewState["CurrentApplicationYear"] = value;
            }
        }

        #endregion پروپرتی ها

        protected void Page_Load(object sender, EventArgs e)
        {
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);

            if (!IsPostBack)
            {
                if (NotificationID < 1) //حالت ثبت ابلاغیه
                {
                    // فقط در حالت ثبت جدید، روی لود فرم کمبوها رو مقداردهی میکنیم،در حالت ویرایش در تابع لود دیتا این کار را میکنیم
                    // چون لود صفحه این کنترل بعد تابع لود دیتا در حالت ویرایش فراخوانی میشود
                    SankaWebAppFramework.Board.Common.BindNotificationLocations(ddlNotificationLocation);
                    SankaWebAppFramework.Board.Common.BindNotificationType(ddlNotificationType);
                }
                //txtDeliveryDate.Attributes = DateAttr;
                txtNotificationDate.DontValidate = true;
            }
            BindActionMenu();
        }

        /// <summary>
        /// بارگذاری جزئیات ابلاغیه
        /// </summary>
        /// <param name="NotificationID"></param>
        public void LoadData()
        {
            getNotificationID();

            var obj = new DTO.Board.tblVoteNotification.tblVoteNotification();
            obj.NotificationID = this.NotificationID;
            obj = (new DAL.Board.tblVoteNotification.Methods()).SelectByID(obj);

            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                txtNotificationDate.DateMiladi = obj.NotificationDate.Value;
                if (obj.DeliveryDate.HasValue)
                {
                    txtDeliveryDate.DateMiladi = obj.DeliveryDate.Value;
                }
                txtRecipientName.Text = obj.RecipientName;
                txtGiverName.Text = obj.GiverName;
                txtRecipientRelationship.Text = obj.RecipientRelationship;
                ddlNotificationLocation.SelectedValue = obj.NotificationAddress;
                txtLeterSerial.Text = obj.LetterSerial;
                txtDescription.Text = obj.Description;
                txtRecipientName.Text = obj.RecipientName;
                ddlNotificationType.SelectedValue = obj.NotificationType.ToString();
                pnlAttachment.Show();
            }
            BindActionMenu();
        }

        /// <summary>
        /// استخراج شناسه ابلاغیه رای
        /// </summary>
        private void getNotificationID()
        {
            var obj = new DTO.Board.tblVoteNotification.tblVoteNotification
            {
                RequestDataID = this.RequestDataID
            };
            this.NotificationID = (new DAL.Board.tblVoteNotification.Methods()).SelectByRequestDataID(obj);
        }

        /// <summary>
        /// ذخیره ابلاغیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool Result = false;

            if (!ValidateData()) return;

            var obj = new DTO.Board.tblVoteNotification.tblVoteNotification();
            obj.ViolationFileID = this.FileID;
            obj.NotificationDate = txtNotificationDate.DateMiladi;
            if (txtDeliveryDate.DateIsValid)
            {
                obj.DeliveryDate = txtDeliveryDate.DateMiladi;
            }
            obj.RecipientName = txtRecipientName.Text.CorrectArabicChars();
            obj.GiverName = txtGiverName.Text.CorrectArabicChars();
            obj.RecipientName = txtRecipientName.Text.CorrectArabicChars();
            obj.RecipientRelationship = txtRecipientRelationship.Text.CorrectArabicChars();
            obj.NotificationAddress = ddlNotificationLocation.SelectedValue;
            obj.LetterSerial = "0"; //  خودکار تولید میشود
            obj.Description = txtDescription.Text.CorrectArabicChars();
            obj.RequestID = this.RequestID;
            obj.BoardID = this.BoardID;
            obj.ActivityYear = this.CurrentApplicationYear;
            obj.UserID = this.UserID;
            obj.NotificationType = ddlNotificationType.SelectedValue.StringToByte();
            #region ویرایش

            if (this.NotificationID > 0)
            {// ویرایش
                obj.NotificationID = this.NotificationID;
                obj.RequestDataID = this.RequestDataID;
                bool bln = (new DAL.Board.tblVoteNotification.Methods()).Update(obj);
                if (bln)
                {
                    Result = true;
                }
            }

            #endregion ویرایش

            #region ثبت
            else
            {
                long[] ReturnIDs = (new DAL.Board.tblVoteNotification.Methods()).Insert(obj);

                if (ReturnIDs[0] > 0)
                {
                    this.NotificationID = ReturnIDs[0];
                    this.RequestDataID = ReturnIDs[1];
                    if (this.NotificationID > 0 && this.RequestDataID > 0)
                    {
                        Result = true;
                    }
                }
            }

            #endregion ثبت

            if (Result)
            {
                this.LoadData();
                AddNotification_Completed(Result);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه صدور رای خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        /// <summary>
        /// اعتبار سنجی پیش از ارسال
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            List<string> lstMessage = new List<string>();

            if (txtDeliveryDate.DateShamsi.Length > 0)
            {
                if (!txtDeliveryDate.DateIsValid)
                {
                    lstMessage.Add("تاریخ ابلاغ به محکوم نامعتبر است .");
                }
                else
                {
                    if (!CheckNotificationDate())
                    {
                        lstMessage.Add("تاریخ ابلاغ رای باید بزرگتر یا مساوی تاریخ صدور رای باشد .");
                    }
                }
            }
           
            if (txtGiverName.IsEmpty())
            {
                lstMessage.Add("نام مامور ابلاغ را درج نمایید .");
            }
            if (txtRecipientName.IsEmpty())
            {
                lstMessage.Add("نام گیرنده ابلاغیه را درج نمایید .");
            }
            if (txtDescription.IsEmpty())
            {
                lstMessage.Add("درج توضیحات الزامی است .");
            }

            if (lstMessage.Any())
            {
                this.Error_Occured(lstMessage);
            }
            return (lstMessage.Count == 0);
        }
         
        /// <summary>
        /// چک کردن اینکه تاریخ تحویل به متهم حتما بعد از تاریخ صدور رای باشد
        /// </summary>
        /// <returns></returns>
        private bool CheckNotificationDate()
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                ViolationFileID = this.FileID
            };
            obj = (new DAL.Board.tblViolationFileVote.Methods()).getVoteDate(obj);
            if (obj.RowNumber > 0)
            {
                return (txtDeliveryDate.DateMiladi.Date - obj.VoteDate.Date).Days >= 0;
            }
            return true;
        }


        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            divNotificationField.Enabled = false;
            FileUpload1.LockAllControls();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            divNotificationField.Enabled = true;
            FileUpload1.UnLockAllControls();
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// <param name="CreateAgain">اگر ترو باشد، یعنی فانکشن در حالت لود صفحه فراخوانی نشده و باید مجددا منو ساخته شودو گزینه های قبلی پاک شود</param>
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancelView",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png",
            });

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره ابلاغیه",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#divNotificationField');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            //if (NotificationID > 0)
            //{// اگر در حالت ویرایش بود، دکمه چاپ نمایش داده شود
            //    lstAction.Add(new ActionMenuModel()
            //    {
            //        TargetID = "btnPrint",
            //        Title = "چاپ",
            //        Type = ActionMenuType.ImageButton,
            //        OnClientClick = "return ShowNotificationPrint();",
            //        Icon = "/images/icon/printer.png"
            //    });
            //}

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// رویداد انصراف از ثبت اتهام جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            // این خط رو فراخوانی میکنیم تا اطلاعات از سشن پاک شود
            FileUpload1.DeleteListFromSession();
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Cancel_Click();
        }

        /// <summary>
        /// پاک کردن مقدار کنترل های فرم
        /// </summary>
        public void ResetAllControls(string DefaultDate)
        {
            ddlNotificationLocation.SelectedIndex = 0;
            txtDeliveryDate.DateShamsi = DefaultDate;
            txtGiverName.Text = "--------------";
            txtLeterSerial.Text = "";
            txtNotificationDate.DateShamsi = DefaultDate;
            txtRecipientName.Text = "--------------";
            txtRecipientRelationship.Text = "";
            this.NotificationID = 0;
            FileUpload1.OwnerRecordID = 0;
            FileUpload1.ResetUploadFile(DefaultDate);
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            // با یک کردن این فلگ ، یعنی بعد از بستن مودال، صفحه پرنت مودال، دوباره بارگذاری شود تا تغییرات در آن دیده شود
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }
    }
}