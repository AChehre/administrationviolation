﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCChangeFileBoard : GeneralControl
    {
        public delegate void dlgChangeFileBoard_Completed(bool Status);
        public event dlgChangeFileBoard_Completed ChangeFileBoard_Completed;

        public delegate void dlgChangeFileBoard_CancelClick();
        public event dlgChangeFileBoard_CancelClick Cancel_Click;

        public delegate void dlgChangeFileBoard_Error(List<string> lstMessage);
        public event dlgChangeFileBoard_Error Error_Occured;

        public bool LockToBoardDropDownList
        {
            set
            {
                ViewState.Add("LockToBoardDropDownList", value);
            }
            get
            {
                if (ViewState["LockToBoardDropDownList"] != null)
                {
                    return ViewState["LockToBoardDropDownList"].ToString().StringToBool();
                }
                return false;
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Board.Common.BindYear(ddlApplicationYear);
                Board.Common.BindBoards(ddlToBoard,2);
                ddlFileReferType.SelectedValue = "2";
                ddlFileReferType.Enabled = false;
                txtSerialCode.Text = getFileSerialCode();
            }

            BindActionMenu();
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);
            //ddlApplicationYear.Enabled = false;
        }

        /// <summary>
        /// استخراج سریال پرونده
        /// </summary>
        /// <returns></returns>
        private string getFileSerialCode()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            return obj.SerialCode;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            //ddlViolationFile.Enabled = false;
            txtComment.Enabled = false;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            //ddlViolationFile.Enabled = true;
            txtComment.Enabled = true;
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && this.RequestDataID == 0)
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ShowConfirm(this,'توجه:  با انجام این عملیات پرونده از این هیات منتقل میگردد . آیا اطمینان دارید ؟ !');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// شناسه رکورد
        /// </summary>
        public long ChangeBoardID
        {
            get
            {
                if (ViewState["ChangeBoardID"] != null)
                {
                    return ViewState["ChangeBoardID"].ObjectToLong();
                }
                return 0;
            }
            set
            {
                ViewState.Add("ChangeBoardID", value);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Validated()) return;
            {
                var obj = new DTO.Board.tblChangeFileBoard.tblChangeFileBoard
                {
                    ViolationFileID = this.FileID,
                    FromBoardID = this.BoardID,
                    ToBoardID = ddlToBoard.SelectedValue.StringToLong(),
                    Description = txtComment.Text.CorrectArabicChars(),
                    ChangeDate = DateTime.Now,
                    Status = 1,
                    RequestID = this.RequestID,
                    FileReferType = ddlFileReferType.SelectedValue.StringToByte(),
                    UserID = this.UserID,
                    ActivityYear = ddlApplicationYear.SelectedValue.StringToInt()
                    
                };
                long[] result = (new DAL.Board.tblChangeFileBoard.Methods()).Insert(obj);
                if (result[0] > 0)
                {
                  
                    this.ChangeBoardID = result[0];
                    this.RequestDataID = result[1];
                    // لاگ ثبت درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                    this.ChangeFileBoard_Completed(true);
                }
                else
                {
                    List<string> lstMessage = new List<string>();
                    lstMessage.Add("در پروسه ایجاد پرونده بررسی مجدد خطایی رخ داده است .");
                    this.Error_Occured(lstMessage);
                }
            }
        }

        /// <summary>
        /// تغییر هیات جاری دارنده این پرونده
        /// </summary>
        /// <param name="boardID"></param>
        /// <returns></returns>
        private bool ChangeViolationFileBoard(long boardID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                BoardID = boardID,
                FileID = this.FileID
            };
            return (new DAL.Board.tblViolationFile.Methods()).ChangeVilationFileBoard(obj);
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool Validated()
        {
            List<string> lstMessage = new List<string>();
            //if (ddlFromBoard.SelectedValue.Equals(ddlToBoard.SelectedValue))
            //{
            //    this.Error_Occured("هیات مقصد و مبدا یکسان است .");
            //    return false;
            //}
            //if (ddlViolationFile.SelectedIndex == 0)
            //{
            //    this.Error_Occured("پرونده مورد نظر را انتخاب نمایید .");
            //    return false;
            //}
            if (txtComment.IsEmpty())
            {
                lstMessage.Add("درج توضیحات الزامی است .");
            }
            this.Error_Occured(lstMessage);
            return lstMessage.Count == 0;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel_Click();
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک اتهام
        /// </summary>
        /// <param name="AccusationID"></param>
        public void LoadData()
        {
            getRecordID();
            DTO.Board.tblChangeFileBoard.tblChangeFileBoard obj = new DTO.Board.tblChangeFileBoard.tblChangeFileBoard();
            obj.ID = this.ChangeBoardID;
            obj = (new DAL.Board.tblChangeFileBoard.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                txtComment.Text = obj.Description;
                this.RequestDataID = obj.RequestDataID.Value;
                ddlToBoard.SelectedValue = obj.ToBoardID.ToString();
                txtComment.Text = obj.Description;
                divChangeFileBoard.Enabled = false;
                //pnlAttachment.Show();
            }
            BindActionMenu();
        }

        /// <summary>
        /// استخراج شناسه جدول سابقه تغییر هیات بر اساس شناسه درخواست ثبت شده
        /// </summary>
        private void getRecordID()
        {
            var obj = new DTO.Board.tblChangeFileBoard.tblChangeFileBoard
            {
                RequestDataID = this.RequestDataID
            };
            this.ChangeBoardID = (new DAL.Board.tblChangeFileBoard.Methods()).SelectByRequestDataID(obj);
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                if (ChangeViolationFileBoard(ddlToBoard.SelectedValue.StringToLong()))
                {
                    SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                }
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }
    }
}