﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDatePicker.ascx.cs"
    Inherits="SankaWebAppFramework.UCDatePicker" %>
<asp:TextBox ID="TextBoxDatePicker" placeholder="کلیک نمایید" MaxLength="10" runat="server"></asp:TextBox>
<asp:Label runat="server" ID="lblStar" ForeColor="Red" Visible="false">*</asp:Label>
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%= TextBoxDatePicker.ClientID %>").persianDatepicker();
    });
</script>
