﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCOtherRequest : GeneralControl
    {
        public delegate void dlgAddRequest_Completed(bool Status , long RequestID);
        public event dlgAddRequest_Completed AddRequest_Completed;

        public delegate void dlgAddAccusition_Cancel();
        public event dlgAddAccusition_Cancel Cancel;

       /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long AccusationID
        {
            get
            {
                if (ViewState["AccusationID"] != null)
                {
                    return ViewState["AccusationID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("AccusationID", value);
            }
        }

        /// <summary>
        /// عنوان درخواست
        /// </summary>
        public string RequestTitle
        {
            set
            {
                pnlTitle.InnerHtml = value;
            }
            get
            {
                return pnlTitle.InnerHtml;
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // وقتی میخوای کامنت های پیوست ها رو برداری، هرجار فایل آپلود یک رو کامنت کردی، کامنت ها رو بردار
                // تنظیمات فایل آپلود 
                if (AccusationID < 1)
                {//  حالت ثبت درخواست
                    // فقط در حالت ثبت جدید، روی لود فرم کمبوها رو مقداردهی میکنیم،در حالت ویرایش در تابع لود دیتا این کار را میکنیم
                    // چون لود صفحه این کنترل بعد تابع لود دیتا در حالت ویرایش فراخوانی میشود

                }
                Dictionary<string, string> DateAttr = new Dictionary<string, string>();
                DateAttr.Add("data-required", "true");

            }
            else
            {
                BindActionMenu();
            }
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);
        }

        /// <summary>
        /// بار گذاری برچسب عنوان کنترل
        /// </summary>
        private void LoadTitle()
        {
            DTO.Config.tblRequest.tblRequest obj = new DTO.Config.tblRequest.tblRequest { RequestID = this.RequestID };
            obj = (new DAL.Config.tblRequest.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                this.RequestTitle = obj.Title;
            }
        }

        /// <summary>
        /// ثبت درخواست
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool Result = false;

            // درخواست جدید درج میشود
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData();
            obj.FileID = this.FileID;
            obj.RequestID = this.RequestID;
            obj.RequestDataID = this.RequestDataID;
            obj.Note = txtDescription.Text.CorrectArabicChars();
            obj.UserID = this.UserID;
            #region ویرایش

            if (this.RequestDataID > 0)
            {
                bool bln = (new DAL.Board.tblRequestData.Methods()).Update(obj);
                if (bln)
                {
                    // لاگ ویرایش درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);

                    Result = true;
                }
            }

            #endregion ویرایش

            #region ثبت

            else
            {
                long requestDataID = (new DAL.Board.tblRequestData.Methods()).Insert(obj);

                if (requestDataID > 0)
                {
                    // لاگ ثبت درخواست
                    (new Logger()).InsertRequestDataModificationLog(requestDataID, Logger.RecardModificationFlag.INSERT);

                    this.RequestDataID = requestDataID;

                    //  بروز رسانی حالت پرونده
                    //SankaWebAppFramework.Board.Common.ChangeViolationFileStatus(this.FileID, SankaWebAppFramework.Board.Enums.FileStatus.OTHER_REQUEST);
                    Result = true;
                }
            }

            #endregion ثبت

            if (Result)
            {
                if (RequestID == 86 || RequestID == 87 || RequestID == 88) //  درخواست صورت جلسه مختومه
                {
                    SetFileCloseDate();
                }
            }
            this.AddRequest_Completed(Result, this.RequestID);
        }

        /// <summary>
        /// ثلت تاریخ بسته شدن فایل
        /// </summary>
        /// <returns></returns>
        private bool SetFileCloseDate()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                FileCloseDate = DateTime.Now,
                VoteTypeCode = (byte)Board.Enums.VoteTypeCode.CLOSE
            };
            return (new DAL.Board.tblViolationFile.Methods()).SetFileCloseDate(obj);
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            txtDescription.Enabled = false;
            FileUpload1.LockAllControls();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnlockAllControls()
        {
            txtDescription.Enabled = true;
            FileUpload1.UnLockAllControls();
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#divAccusitionField');",
                    Icon = "/images/icon/save.png"
                });
                UnlockAllControls();
            }
            else
            {
                LockAllControls();
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// رویداد انصراف از ثبت درخواست جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            FileUpload1.DeleteListFromSession();
            this.Cancel();
        }

        /// <summary>
        /// ریست کردن فرم
        /// </summary>
        public void ResetAllControls(string DefaultDate)
        {
            this.AccusationID = 0;
            FileUpload1.OwnerRecordID = 0;
            FileUpload1.ResetUploadFile(DefaultDate);
        }

        /// <summary>
        /// بار گذاری اطلاعات
        /// </summary>
        public void LoadData()
        {
            LoadTitle();
            if (this.RequestDataID > 0)
            {
                DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData { RequestDataID = this.RequestDataID };
                obj = (new DAL.Board.tblRequestData.Methods()).SelectByID(obj);
                if (obj.RowNumber > 0)
                {
                    txtDescription.Text = obj.Note;
                }

                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();
                if (!string.IsNullOrEmpty(RelatedAttachementTypes))
                {
                    pnlAttachment.Show();
                }
                #endregion تنظیمات فایل آپلود در حالت ویرایش 
            }
            BindActionMenu();
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                this.RequestDataID = obj.RequestDataID.Value;
                FileUpload1.Load();
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                if (bln)
                {
                    if (this.RequestID == 8 || this.RequestID == 36)
                    {
                        SetDefenceBillFlag(true);
                    }
                }
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        /// <summary>
        /// درج فلگ لایحه دفاعیه متهم
        /// </summary>
        /// <param name="hasDefenceBill">آیا دفاعیه دارد ؟</param>
        private void SetDefenceBillFlag(bool hasDefenceBill)
        {
            var objFile = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                HasDefenseBill = hasDefenceBill
            };
            (new DAL.Board.tblViolationFile.Methods()).SetDefenceBillFlag(objFile);

        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }
    }
}