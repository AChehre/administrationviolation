﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCFileSendingRequest : GeneralControl
    {
        public delegate void dlgSendingFileRequest_Completed(bool Status);
        public event dlgSendingFileRequest_Completed SendingFileRequest_Completed;

        public delegate void dlgSendingFileRequest_CancelClick();
        public event dlgSendingFileRequest_CancelClick Cancel_Click;

        public delegate void dlgSendingFileRequest_Error(string errorMessage);
        public event dlgSendingFileRequest_Error Error_Occured;

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long SendingRequestID
        {
            get
            {
                if (ViewState["ID"] != null)
                {
                    return ViewState["ID"].ObjectToLong();
                }
                return 0;
            }
            set
            {
                ViewState.Add("ID", value);
            }
        }

        /// <summary>
        /// غیر فعال کردن کمبو باکس انتخاب هیات
        /// </summary>
        public bool LockToBoardDropDownList
        {
            set
            {
                ViewState.Add("LockToBoardDropDownList", value);
            }
            get
            {
                if (ViewState["LockToBoardDropDownList"] != null)
                {
                    return ViewState["LockToBoardDropDownList"].ToString().StringToBool();
                }
                return false;
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Board.Common.BindBoards(ddlFromBoard);
                Board.Common.BindBoards(ddlToBoard);
                ddlToBoard.SelectedValue = this.BoardID.ToString();
            }
            BindActionMenu();
            ddlToBoard.Enabled = !this.LockToBoardDropDownList;
            ddlViolationFile.Enabled = false;
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            ddlFromBoard.Enabled = false;
            //ddlViolationFile.Enabled = false;
            txtComment.Enabled = false;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            ddlFromBoard.Enabled = true;
            //ddlViolationFile.Enabled = true;
            txtComment.Enabled = true;
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && this.RequestDataID == 0)
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#inputDiv');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool Result = false;
            if (!Validated()) return;
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                FileID = this.FileID,
                FromBoardID = ddlFromBoard.SelectedValue.StringToLong(),
                ToBoardID = ddlToBoard.SelectedValue.StringToLong(),
                Status = 0,
                RequestDate = DateTime.Now,
                RequestedFileSerialCode = ddlViolationFile.SelectedItem.Text,
                Comment = txtComment.Text.CorrectArabicChars(),
                RequestDataDescription = " درخواست ارسال پرونده [ " + ddlViolationFile.SelectedItem.Text + " ] از " + ddlFromBoard.SelectedItem.Text,
                RequestID = this.RequestID,
                RelatedFileID = ddlViolationFile.SelectedValue.StringToLong(),
                RequesterBoardType = this.UserBoardType,
                UserID = this.UserID
            };
            long[] ReturnIDs = (new DAL.Board.tblSendingFileRequest.Methods()).Insert(obj);
            if (ReturnIDs[0] > 0)
            {
                this.SendingRequestID = ReturnIDs[0];
                this.RequestDataID = ReturnIDs[1];
                if (this.SendingRequestID > 0 && this.RequestDataID > 0)
                {
                    Result = true;
                }
            }
            if (Result)
            {
                // لاگ ثبت درخواست
                (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                LoadData();
                this.SendingFileRequest_Completed(Result);
            }
        }

        /// <summary>
        /// متهم پرونده فعلی و پرونده درخواست شده باید یکی باشد
        /// </summary>
        /// <returns></returns>
        private bool CheckSameAccusedFileRequest()
        {
            var objCurrentFile = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            objCurrentFile = (new DAL.Board.tblViolationFile.Methods()).SelectByID(objCurrentFile);
            //----------------------------------------------------------
            var objRequestedFile = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = ddlViolationFile.SelectedValue.StringToLong()
            };
            objRequestedFile = (new DAL.Board.tblViolationFile.Methods()).SelectByID(objRequestedFile);

            if (objCurrentFile.RowNumber > 0 && objRequestedFile.RowNumber > 0)
            {
                return (objCurrentFile.PersonnelCode == objRequestedFile.PersonnelCode) ||
                    (objCurrentFile.NationalCode == objRequestedFile.NationalCode);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// تغییر هیات بررسی کننده پرونده
        /// </summary>
        /// <param name="boardID">شناسه هیات</param>
        /// <returns></returns>
        private bool ChangeViolationFileBoard(long boardID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                BoardID = boardID,
                FileID = this.FileID
            };
            return (new DAL.Board.tblViolationFile.Methods()).ChangeVilationFileBoard(obj);
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool Validated()
        {
            if (ddlViolationFile.SelectedIndex == 0)
            {
                this.Error_Occured("پرونده مورد نظر را انتخاب نمایید .");
                return false;
            }
            //if (!CheckHomoLevelBoard())
            //{
            //    this.Error_Occured("هیات مبداء و مقصد هم عرض نیستند .");
            //    return false;
            //}
            if (!CheckSameAccusedFileRequest())
            {
                this.Error_Occured("متهم پرونده درخواست شده و متهم پرونده جاری یکسان نمیباشد .");
                return false;
            }
            if (txtComment.IsEmpty())
            {
                this.Error_Occured("درج توضیحات الزامی است .");
                return false;
            }
            return true;
        }

        /// <summary>
        /// چک کردن اینکه هر دو هیات د یک استان و منطقه باشند
        /// </summary>
        /// <returns></returns>
        private bool CheckHomoLevelBoard()
        {
            var objFrom = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = ddlFromBoard.SelectedValue.StringToLong()
            };
            objFrom = (new DAL.Board.tblBoard.Methods()).SelectByID(objFrom);

            var objToBoard = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = ddlToBoard.SelectedValue.StringToLong()
            };
            objToBoard = (new DAL.Board.tblBoard.Methods()).SelectByID(objToBoard);

            return (objFrom.OstanCode == objToBoard.OstanCode) &&
                (objFrom.RegionCode == objToBoard.RegionCode) &&
                (objFrom.SchoolCode == objToBoard.SchoolCode);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel_Click();
        }

        protected void ddlFromBoard_SelectedIndexChanged(object sender, EventArgs e)
        {
            Board.Common.BindViolationfile(ddlViolationFile, ddlFromBoard.SelectedValue.StringToLong(),this.BoardID);
        }

        protected void ddlFromBoard_DataBound(object sender, EventArgs e)
        {
            ddlFromBoard_SelectedIndexChanged(sender, e);
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک اتهام
        /// </summary>
        /// <param name="AccusationID"></param>
        public void LoadData()
        {
            getFileID();
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest();
            obj.ID = this.SendingRequestID;
            obj = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                ddlFromBoard.SelectedValue = obj.FromBoardID.ToString();
                ddlFromBoard_SelectedIndexChanged(null, null);
                ddlViolationFile.SelectedValue = obj.RelatedFileID.ToString();
                txtComment.Text = obj.Comment;
                //pnlAttachment.Show();
                this.RequestDataID = obj.RequestDataID;
                divAccusitionField.Enabled = false;
            }
            BindActionMenu();
        }

        /// <summary>
        /// دریافت شناسه پرونده
        /// </summary>
        private void getFileID()
        {
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                RequestDataID = this.RequestDataID
            };
            this.SendingRequestID = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByRequestDataID(obj);
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                if (ChangeViolationFileBoard(ddlToBoard.SelectedValue.StringToLong()))
                {
                    SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                }
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }
    }
}