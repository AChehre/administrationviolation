﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace SankaWebAppFramework.controls
{
    public partial class UCAllocationMembers : GeneralControl
    {
        public delegate void dlgAllocationMembers_Completed(bool Status);
        public event dlgAllocationMembers_Completed AllocationMembers_Completed;

        public delegate void dlgAddAccusition_CancelClick();
        public event dlgAddAccusition_CancelClick Cancel_Click;

        public delegate void dlgAddAccusition_Error(string errorMessage);
        public event dlgAddAccusition_Error Error;

        private int BaseYear = 1394;

        /// <summary>
        /// سال عملیاتی پرونده
        /// </summary>
        public int ViolationFileYear
        {
            set
            {
                ViewState.Add("ViolationFileYear", value);
            }
            get
            {
                return ViewState["ViolationFileYear"].ToString().StringToInt();
            }
        }

        /// <summary>
        /// آیا در وضعیت ویرایش هستیم ؟
        /// </summary>
        private bool isEditMode
        {
            set
            {
                ViewState.Add("isEditMode", value);
            }
            get
            {
                if (ViewState["isEditMode"] != null)
                {
                    return ViewState["isEditMode"].ToString().StringToBool();
                }
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindActionMenu();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            txtFName1.Enabled = false;
            txtFName2.Enabled = false;
            txtFName3.Enabled = false;
            txtLName1.Enabled = false;
            txtLName2.Enabled = false;
            txtLName3.Enabled = false;
            chkListMembers.Enabled = false;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            txtFName1.Enabled = true;
            txtFName2.Enabled = true;
            txtFName3.Enabled = true;
            txtLName1.Enabled = true;
            txtLName2.Enabled = true;
            txtLName3.Enabled = true;
            chkListMembers.Enabled = true;
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#inputDiv');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// ثبت اتهام
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool Result = false;

            List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> lstObj = new List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers>();

             bool blnAfterYear1394 = (ViolationFileYear >= BaseYear);

            if (blnAfterYear1394) // اگر سال عملیاتی پرونده بعد سال 94 بود
            {
                #region ثبت جدید

                foreach (ListItem item in chkListMembers.Items)
                {
                    if (item.Selected)
                    {
                        lstObj.Add(new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers
                        {
                            FName = item.Text,
                            LName = item.Text,
                            MemberID = item.Value.StringToLong(),
                            ViolationFileID = FileID
                        });
                    }
                }

                #endregion ثبت جدید
            }
            else
            {// اگر سال عملیاتی پرونده قبل سال 94 بود
                #region ثبت قدیم

                lstObj.Add(new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers
                {
                    FName = txtFName1.Text,
                    LName = txtLName1.Text,
                    ViolationFileID = FileID
                });

                lstObj.Add(new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers
                {
                    FName = txtFName2.Text,
                    LName = txtLName2.Text,
                    ViolationFileID = FileID
                });

                lstObj.Add(new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers
                {
                    FName = txtFName3.Text,
                    LName = txtLName3.Text,
                    ViolationFileID = FileID
                });
    
                #endregion ثبت قدیم
            }

            if (lstObj.Count() != 3)
            {
                this.Error("تعداد اعضای تعیین صدور رای، باید سه نفر باشد .");
                return;
            }

            // حذف اعضایی که قبلا تخصیص داده ایم
            (new DAL.Board.tblViolationFileVoteMembers.Methods()).DeleteByViolationFileID(new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers
            {
                ViolationFileID = FileID
            });

            // ثبت مجدد اعضا
            Result = (new DAL.Board.tblViolationFileVoteMembers.Methods()).InsertList(lstObj);

            if (Result)
            {
                if (!this.isEditMode)
                {
                    Board.Common.InsertRequestDataRecord(this.UserID, this.RequestID, this.FileID, getMemberNames(blnAfterYear1394));
                }
            }

            this.AllocationMembers_Completed(Result);
        }

        /// <summary>
        /// چک کردن اینکه سال عملیاتی قبل از 1394 باشد
        /// لازم به ذکر است که در سالهای قبلی نام اعضای هیات را مستقیما درج میکنیم .
        /// </summary>
        /// <param name="blnAfterYear1394"></param>
        /// <returns></returns>
        private string getMemberNames(bool blnAfterYear1394)
        {
            if (blnAfterYear1394)
            {
                string strMember = "";
                foreach (ListItem item in chkListMembers.Items)
                {
                    if (item.Selected)
                    {
                        strMember += " [ " + item.Text + " ] و";
                    }
                }
                int index = strMember.LastIndexOf("و");
                return strMember.Remove(index);
            }
            else
            {
                return "[ " + txtFName1.Text + " " + txtLName1.Text + " ] و [ " + txtFName2.Text + " " + txtLName2.Text + " ] و [ " + txtFName3.Text + " " + txtLName3.Text + " ]";
            }
        }

        /// <summary>
        /// نمایش اعضا تخصیص داده شده به صدور رای پرونده در صورت وجود
        /// </summary>
        public void LoadData()
        {
            if (ViolationFileYear >= BaseYear) // اگر سال عملیاتی پرونده بعد سال 94 بود
            {
                pnlNew.Visible = true;
                pnlOld.Visible = false;

                #region نمایش اعضا

                List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView> lstObj = new List<DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView>();

                DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView Obj = new DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView();
                Obj.BoardID = BoardID;
                Obj.Status = (byte)AdministrationViolation.Admin.Enums.Board_Member_Status.Active; ;

                lstObj = (new DAL.Admin.tblBoardMemberMapView.Methods()).SelectBoardMember(Obj);

                chkListMembers.DataSource = lstObj;
                chkListMembers.DataValueField = "UserID";
                chkListMembers.DataTextField = "FullName";
                chkListMembers.DataBind();

                //**
                List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> lstmember = new List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers>();

                DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers Objmember = new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers();
                Objmember.ViolationFileID = FileID;
                lstmember = (new DAL.Board.tblViolationFileVoteMembers.Methods()).SelectByViolationFileID(Objmember);

                //  تعیین میکنیم که در مد ویرایش هستیم یا نه برای جلوگیری از ثبت تکراری درخواست
                this.isEditMode = lstmember.Any();

                foreach (ListItem item in chkListMembers.Items)
                {
                    if (lstmember.Any(x => x.MemberID == item.Value.StringToLong()))
                    {
                        item.Selected = true;
                    }
                }

                #endregion نمایش اعضا
            }
            else // اگر سال عملیاتی پرونده قبل سال 94 بود
            {
                pnlOld.Visible = true;
                pnlNew.Visible = false;

                #region نمایش اعضا

                List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers> lstObj = new List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers>();

                DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers Obj = new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers();
                Obj.ViolationFileID = FileID;

                try
                {
                    lstObj = (new DAL.Board.tblViolationFileVoteMembers.Methods()).SelectByViolationFileID(Obj);

                    int RowCount = lstObj.Count;

                    if (RowCount > 0)
                    {//نمایش عضو اول
                        txtFName1.Text = lstObj[0].FName;
                        txtLName1.Text = lstObj[0].LName;
                    }
                    if (RowCount > 1)
                    {//نمایش عضو دوم
                        txtFName2.Text = lstObj[1].FName;
                        txtLName2.Text = lstObj[1].LName;
                    }
                    if (RowCount > 2)
                    {//نمایش عضو سوم
                        txtFName3.Text = lstObj[2].FName;
                        txtLName3.Text = lstObj[2].LName;
                    }
                }
                catch
                {

                }

                #endregion نمایش اعضا
            }
            BindActionMenu();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel_Click();
        }
    }
}