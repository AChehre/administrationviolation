﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;

namespace SankaWebAppFramework
{
    [ValidationProperty("Text")]
    public partial class UCDatePicker : System.Web.UI.UserControl
    {
        public UCDatePicker()
        {
            //this.PreRender += new EventHandler(UCDatePicker_PreRender);
        }

        //void UCDatePicker_PreRender(object sender, EventArgs e)
        //{
        //    if (TextBoxDatePicker.IsEmpty())
        //    {
        //        UserProfile profile = Session["UserProfile"] as UserProfile;
        //        if (profile != null)
        //        {
        //            var pcal = (new PersianCalendar());
        //            TextBoxDatePicker.Text = profile.CurrentApplicationYear + "/" +
        //                pcal.GetMonth(DateTime.Now) + "/" +
        //                pcal.GetDayOfMonth(DateTime.Now);
        //        }
        //    }
        //}

        /// <summary>
        /// بررسی معتبر بودن تاریخ شمسی
        /// </summary>
        public bool DateIsValid
        {
            get
            {
                try
                {
                    DateTime OrderPayDate = TextBoxDatePicker.Text.ToLatinDate();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// دریافت مقدار تکست باکس
        /// </summary>
        public string Text
        {
            get
            {
                return TextBoxDatePicker.Text;
            }
        }

        
        /// <summary>
        /// اگر فالس شود، کنترل اعتبارسنجی نمیشود
        /// </summary>
        
        public bool DontValidate
        {
            set
            {
                lblStar.Visible = !value;
            }
            get
            {
                return !lblStar.Visible;
            }
        }

        /// <summary>
        /// فعال بودن یا نبودن تکست باکس
        /// </summary>
        public bool Enabled
        {
            set
            {
                TextBoxDatePicker.Enabled = value;
            }
        }

        /// <summary>
        /// اگر ترو باشد، تاریخ فعلی رو به صورت شمسی در تکست باکس نمایش میدهد
        /// </summary>
        public bool ShowNow
        {
            set
            {
                if (value)
                    DateMiladi = DateTime.Now;
            }
        }

        /// <summary>
        /// ست کردن عرض تکست باکس
        /// </summary>
        public int TextBoxWidth
        {
            set
            {
                TextBoxDatePicker.Width = value;
            }
        }

        /// <summary>
        /// اگر برابر ترو باشد، تکست باکس بدون مقدار پیش فرض نمایش داده میشود ، به صورت خالی
        /// </summary>
        public bool Empty
        {
            set
            {
                _Empty = value;
                if (_Empty)
                {
                    TextBoxDatePicker.Text = string.Empty;
                }
            }
        }

        private bool _Empty = false;

        /// <summary>
        /// مقداردهی تکست باکس برابر با یک تاریخ شمسی
        /// یا گرفتن تاریخ انتخابی به صورت شمسی
        /// </summary>
        public string DateShamsi
        {
            get
            {
                return TextBoxDatePicker.Text; 
            }
            set { TextBoxDatePicker.Text = value; }
        }

        /// <summary>
        /// گرفتن تاریخ انتخابی به صورت میلادی
        /// ست کردن مقدار تکست باکس با معادل شمسی تاریخی که به این پروپرتی ست شده است
        /// </summary>
        public DateTime DateMiladi
        {
            get { return TextBoxDatePicker.Text.ToLatinDate(); }
            set { TextBoxDatePicker.Text = value.ToPersianDate(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!_Empty)
                {
                    if (string.IsNullOrEmpty(TextBoxDatePicker.Text))
                        TextBoxDatePicker.Text = DateTime.Now.ToPersianDate();
                }
                else
                {
                    TextBoxDatePicker.Text = "";
                }
            }
        }
    }
}