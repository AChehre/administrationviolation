﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace SankaWebAppFramework.controls
{
    public partial class SankaFileUpload : System.Web.UI.UserControl
    {
        public delegate void dlgUpload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj);
        public event dlgUpload_Completed Upload_Completed;

        public delegate void dlgUploaded_Deleted(long RequestDataID);
        public event dlgUploaded_Deleted Uploaded_Deleted;

        /// <summary>
        ///ارسال قابل سایز ماکزیمم 
        ///</summary>
        [Browsable(true)]
        public long MaxFileSize
        {
            get
            {
                return hfFileSize.Value.StringToLong();
            }
            set
            {
                hfFileSize.Value = value.ToString();
            }
        }

        /// <summary>
        /// ذخیره فیزیکی مسیر 
        ///</summary>
        [Browsable(true)]
        public string UploadPath
        {
            get
            {
                return hfUploadPath.Value;
            }
            set
            {
                hfUploadPath.Value = value.ToString();
            }
        }

        /// <summary>
        /// آیدی مالک فایل های پیوست
        /// </summary>
        public long OwnerRecordID
        {
            get
            {
                return hfOwnerRecordID.Value.StringToLong();
            }
            set
            {
                hfOwnerRecordID.Value = value.ToString();
            }
        }

        /// <summary>
        /// فعال یا غیر فعال کردن کمبوباکس انتخاب نوع سند
        /// </summary>
        public string AllowedFileTypes
        {
            get
            {
                if (ViewState["AllowedFileTypes"] != null)
                {
                    return ViewState["AllowedFileTypes"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState.Add("AllowedFileTypes", value);
            }
        }

        public bool HasFile
        {
            get
            {
                return gvUplodedFiles.Rows.Count > 0;
            }
        }

        public bool HasPostedFile
        {
            get
            {
                return MyFileUpload.HasFile;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long RequestDataID
        {
            set
            {
                ViewState.Add("RequestDataID", value);
            }
            get
            {
                if (ViewState["RequestDataID"] != null)
                {
                    return ViewState["RequestDataID"].ToString().StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// بدست آوردن لیست فایل هایی که آپلود شده اند
        /// </summary>
        public List<DTO.Board.tblFileAttachment.tblFileAttachment> lstAttachments
        {
            get
            {
                try
                {
                    if (Session["lstAttachments"] != null)
                    {
                        return Session["lstAttachments"] as List<DTO.Board.tblFileAttachment.tblFileAttachment>;
                    }
                }
                catch
                {
                }
                return new List<DTO.Board.tblFileAttachment.tblFileAttachment>();
            }

            set
            {
                if (Session["lstAttachments"] != null)
                {
                    Session["lstAttachments"] = value;
                }
                else
                {
                    Session.Add("lstAttachments", value);
                }
            }
        }

        /// <summary>
        /// حذف لیست فایل های آپلود شده از سشن
        /// </summary>
        public void DeleteListFromSession()
        {
            Session.Remove("lstAttachments");
        }

        /// <summary>
        /// اگر ترو باشد، یعنی فایل های پیوست ، مستقیما برای پرونده هستند، در غیر این صورت برای یک جز از پرونده هستند
        /// مثلا برای اتهام یا ابلاغیه
        /// </summary>
        public bool WorkOnViolation
        {
            get
            {
                if (ViewState["WorkOnViolation"] != null)
                {
                    return ViewState["WorkOnViolation"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("WorkOnViolation", value);
            }
        }

        /// <summary>
        /// فعال یا غیر فعال کردن کمبوباکس انتخاب نوع سند
        /// </summary>
        public int RequestID
        {
            get
            {
                if (ViewState["RequestID"] != null)
                {
                    return ViewState["RequestID"].ToString().StringToInt();
                }

                return 0;
            }
            set
            {
                ViewState.Add("RequestID", value);
            }
        }

        /// <summary>
        /// عنوان کنترل
        /// </summary>
        public string UploaderTitle
        {
            set
            {
                hTitle.InnerHtml = value;
            }
            get
            {
                return hTitle.InnerHtml;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long FileID
        {
            get
            {
                if (ViewState["FileID"] != null)
                {
                    return ViewState["FileID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("FileID", value);
            }
        }

        /// <summary>
        /// بارگذاری لیست آپلودها
        /// </summary>
        /// <returns></returns>
        public new bool Load()
        {
            //DTO.Config.tblRequest.tblRequest obj = new DTO.Config.tblRequest.tblRequest { RequestID = this.RequestID };
            //obj = (new DAL.Config.tblRequest.Methods()).SelectByID(obj);
            //if (obj.RowNumber > 0)
            //{
            //    AllowedFileTypes = obj.RelatedAttachementTypes;
            //    UploaderTitle = " ضمیمه " + obj.Title;
            //}
            SankaWebAppFramework.Board.Common.BindAttachmentFileType(ddlDocumrntType, AllowedFileTypes);
            return LoadDataFromDB();
        }

        /// <summary>
        /// غیر فعال کردن کنترل ها
        /// </summary>
        public void LockAllControls()
        {
            gvUplodedFiles.Columns[9].Visible = false;
            txtDescription.Enabled = false;
            txtDocumentCode.Enabled = false;
            txtDocumentDate.Enabled = false;
            pnlUpload.Hide();
        }

        /// <summary>
        /// فعال کردن کنترل ها
        /// </summary>
        public void UnLockAllControls()
        {
            gvUplodedFiles.Columns[9].Visible = true;
            txtDescription.Enabled = true;
            txtDocumentCode.Enabled = true;
            txtDocumentDate.Enabled = true;
            pnlUpload.Show();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.MaxFileSize = 1024 * 1024;
                this.UploadPath = "~/Temp";
                txtDocumentDate.DontValidate = false;
            }
        }

        /// <summary>
        /// کدهای مربوط دکمه ارسال فایل
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (!Validated()) return;
            try
            {
                bool isSupperted = false;
                if (MyFileUpload.HasFile)
                {
                    if (MyFileUpload.PostedFile.ContentLength > MaxFileSize)
                    {
                        myMessage.ShowMessage("حجم فایل ارسالی بالاتر از مقدار مجاز می باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                        return;
                    }

                    // لیست فرمت های مجاز برای آپلود
                    List<string> strFilter = new List<string>();
                    strFilter.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                    strFilter.Add("application/msword,application/vnd.ms-excel");
                    strFilter.Add("application/msword");
                    strFilter.Add("application/vnd.ms-excel");
                    strFilter.Add("pdf");
                    strFilter.Add("bmp");
                    strFilter.Add("gif");
                    strFilter.Add("png");
                    strFilter.Add("jpeg");
                    strFilter.Add("jpg");
                    strFilter.Add("tiff");

                    if (strFilter.Count > 0)
                    {
                        foreach (string strType in strFilter)
                        {
                            if (MyFileUpload.PostedFile.ContentType.ToLower().Contains(strType) || strType.Equals("*.*"))
                            {
                                isSupperted = true;
                                break;
                            }
                        }
                        if (isSupperted)
                        {
                            // بالا گذاری فایل ، ایجاد و ثبت یک رکورد در جدولی در حافظه و بروز رسانی گرید

                            int index = MyFileUpload.FileName.LastIndexOf(".");
                            string strSavedFileName = Guid.NewGuid().ToString() + "." + MyFileUpload.FileName.Remove(0, index + 1);
                            string strSaveFilePath = Server.MapPath(UploadPath) + @"\" + strSavedFileName;
                            MyFileUpload.SaveAs(strSaveFilePath);

                            DTO.Board.tblFileAttachment.tblFileAttachment obj = new DTO.Board.tblFileAttachment.tblFileAttachment();
                            obj.AttachmentTitle = txtDescription.Text.Trim();
                            obj.AttachmentSource = UploadPath + @"/" + strSavedFileName;
                            obj.RegDate = DateTime.Now;
                            obj.DocumentDate = txtDocumentDate.DateMiladi;
                            obj.DocumentCode = txtDocumentCode.Text;
                            obj.RequestDataID = this.RequestDataID;
                            obj.AttachmentType = ddlDocumrntType.SelectedValue.StringToInt();

                            obj.ViolationFileID = this.FileID;
                            long AttachmentID = (new DAL.Board.tblFileAttachment.Methods()).Insert(obj);
                            if (AttachmentID > 0)
                            {
                                obj.AttachmentID = AttachmentID;
                                //if (this.RequestDataID == 0)
                                //{
                                //    this.RequestDataID = Board.Common.InsertRequestDataRecord(this.RequestID, this.FileID , txtDescription.Text.CorrectArabicChars());
                                //    if (this.RequestDataID > 0)
                                //    {
                                //        UpdateRequestIDForThisAttachment(AttachmentID);
                                //    }
                                //}
                                //Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                                //  فایر کردن رویداد اتمام ذخیره فیزیکی فایل
                                //this.Upload_Completed(obj);
                                txtDescription.Clear();
                                txtDocumentCode.Text = "";
                                this.Upload_Completed(obj);
                            } 
                        }
                        else
                        {
                            myMessage.ShowMessage("فرمت فایل نامعتبر است .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                        }
                    }
                }
            }
            catch
            {
                myMessage.ShowMessage("خطایی در بارگذاری پیوست رخ داده است .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
            }
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool Validated()
        {
            List<string> lstMessage = new List<string>();
            if (!txtDocumentDate.DateIsValid)
            {
                lstMessage.Add("تاریخ نامعتبر است .");
            }
            //if (!CheckDocumentDate())
            //{
            //    lstMessage.Add("تاریخ مستند باید کوچکتر یا مساوی تاریخ عملیات مربوطه باشد .");
            //}
            if (txtDocumentCode.IsEmpty())
            {
                lstMessage.Add("شماره سند را وارد نمایید .");
            }
            if (txtDescription.IsEmpty())
            {
                lstMessage.Add("توضیحات سند را وارد نمایید .");
            }
            if (lstMessage.Count > 0)
            {
                myMessage.ShowMessage(GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page,true);
            }
            return (lstMessage.Count == 0);
        }

        /// <summary>
        /// چک کردن اینکه تاریخ درخوایت حتما قبل از تاریخ ضمائم باشد
        /// </summary>
        /// <returns></returns>
        private bool CheckDocumentDate()
        {
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData
            {
                RequestDataID = this.RequestDataID
            };
            obj = (new DAL.Board.tblRequestData.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return (obj.RequestDate.Value.Date.Day - txtDocumentDate.DateMiladi.Day >= 0);
            }
            return false;
        }

        /// <summary>
        /// ویرایش رکورد ضمائم درخواست
        /// </summary>
        /// <param name="attachmentID"></param>
        private void UpdateRequestIDForThisAttachment(long attachmentID)
        {
            var obj = new DTO.Board.tblFileAttachment.tblFileAttachment
            {
                AttachmentID = attachmentID,
                RequestDataID = this.RequestDataID,
            };
            (new DAL.Board.tblFileAttachment.Methods()).Update(obj);
        }

        /// <summary>
        /// مقدار دهی گرید فایل های پیوست شده از جدول داخل سشن
        /// </summary>
        protected void LoadDataFromSession()
        {
            try
            {
                gvUplodedFiles.DataSource = (lstAttachments.Count > 0) ? lstAttachments : null;
                gvUplodedFiles.DataBind();
            }
            catch
            {
                myMessage.ShowMessage("خطایی در نمایش پیوست ها رخ داده است .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
            }
        }

        /// <summary>
        /// بدست آوردن فایل های پیوست از دیتابیس
        /// </summary>
        /// <param name="_ViolationFileID">آیدی پرونده</param>
        /// <param name="_OwnerRecordID">آیدی قسمتی از پرونده که میخواهیم ضمائم آن را بدست آوریم، مثلا آیدی اتهام یا ابلاغیه</param>
        private bool LoadDataFromDB()
        {
            SetColumnTitle();
            List<DTO.Board.tblFileAttachment.tblFileAttachment> lst = new List<DTO.Board.tblFileAttachment.tblFileAttachment>();
            DTO.Board.tblFileAttachment.tblFileAttachment obj = new DTO.Board.tblFileAttachment.tblFileAttachment();
            obj.RequestDataID = this.RequestDataID;
            lst = (new DAL.Board.tblFileAttachment.Methods()).SelectByRequestDataID(obj);
            gvUplodedFiles.DataSource = (lst.Any()) ? lst : null; 
            gvUplodedFiles.DataBind();
            return lst.Any();
        }

        /// <summary>
        /// ضمیمه ای موجود میباشد ؟
        /// </summary>
        /// <returns></returns>
        public bool HasAnyFile()
        {
            if (this.RequestDataID > 0)
            {
                List<DTO.Board.tblFileAttachment.tblFileAttachment> lst = new List<DTO.Board.tblFileAttachment.tblFileAttachment>();
                DTO.Board.tblFileAttachment.tblFileAttachment obj = new DTO.Board.tblFileAttachment.tblFileAttachment();
                obj.RequestDataID = this.RequestDataID;
                lst = (new DAL.Board.tblFileAttachment.Methods()).SelectByRequestDataID(obj);
                return lst.Any();
            }
            return false;
        }

        /// <summary>
        /// مقدار دهی ستون های گرید
        /// به ازای هر نوع ضمیمه ای قابل تنظیم است
        /// </summary>
        private void SetColumnTitle()
        {
            gvUplodedFiles.Columns[3].HeaderText = lblDescription.InnerText;
            gvUplodedFiles.Columns[4].HeaderText = lblDocumentCode.InnerText;
            gvUplodedFiles.Columns[5].HeaderText = lblDocumentDate.InnerText;
        }

        /// <summary>
        /// حذف یک رکورد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUplodedFiles_RowDeleting(object sender, CommandEventArgs e)
        {
            #region  حذف فایل پیوست در حالت ویرایش

            // حذف از جدول
            DTO.Board.tblFileAttachment.tblFileAttachment obj = new DTO.Board.tblFileAttachment.tblFileAttachment();
            obj.AttachmentID = e.CommandArgument.ToString().StringToLong();

            if ((new DAL.Board.tblFileAttachment.Methods()).Delete(obj))
            {
                Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                this.Uploaded_Deleted(e.CommandArgument.ObjectToLong());
            }
            else
            {
                myMessage.ShowMessage("خطایی در حذف پیوست ها رخ داده است .", DialogMessage.SankaDialog.Message_Type.Warning, Page,true);
            }

            #endregion  حذف فایل پیوست در حالت ویرایش
        }

        /// <summary>
        /// حذف تمام رکوردهای موجود در سشن و گرید
        /// </summary>
        public void ResetUploadFile(string DefaultDocumentDate)
        {
            gvUplodedFiles.DataSource = null;
            gvUplodedFiles.DataBind();
            txtDocumentDate.DateShamsi = DefaultDocumentDate;
        }

        /// <summary>
        /// دریافت لیست خطا ها جهت ارسال به متد نمایش لیست پیام
        /// </summary>
        /// <param name="lstMessage"></param>
        /// <returns></returns>
        private string GetWarrningList(List<string> lstMessage)
        {
            string strMessage = "";
            for (int i = 0; i < lstMessage.Count; i++)
            {
                if (i == 0)
                {
                    strMessage += lstMessage[i];
                    continue;
                }
                strMessage += "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp" + lstMessage[i];
            }
            return strMessage;
        }

        protected void ddlDocumrntType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAttachmentLabel(ddlDocumrntType.SelectedValue.StringToInt());
        }

        /// <summary>
        /// بار گذاری عناوین برچسب کنترل ها
        /// </summary>
        /// <param name="attachmentTypeIndex"></param>
        private void LoadAttachmentLabel(int attachmentTypeIndex)
        {
            var obj = new DTO.Config.tblAttachmentLabels.tblAttachmentLabels { AttachmentTypeIndex = attachmentTypeIndex };
            obj = (new DAL.Config.tblAttachmentLabels.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                lblDescription.InnerText = obj.DescriptionLabel;
                lblDocumentDate.InnerText = obj.DocumentDateLabel;
                lblDocumentCode.InnerText = obj.DocumentCodeLabel;
                lblSelectFileLabel.InnerText = obj.SelectFileLabel;
                lblAttachmentList.InnerText = obj.AttachmentListLabel;
            }
        }

        protected void ddlDocumrntType_DataBound(object sender, EventArgs e)
        {
            ddlDocumrntType_SelectedIndexChanged(sender, e);
        }
    }
}