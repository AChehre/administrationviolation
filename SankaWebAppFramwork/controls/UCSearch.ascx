﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCSearch.ascx.cs" Inherits="SankaWebAppFramework.controls.UCSearch" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>

<style type="text/css">
    input [type='text'] {
        width: 120px;
        height: 22px;
        line-height: 20px;
    }

    select.form-select {
        width: 142px;
        height: 30px;
        line-height: 18px !important;
        padding: 2px;
    }

    .inp {
        height: 18px !important;
    }

    .sbtn {
        padding: 2px 5px;
    }
    .SearchByName {
        width:530px;
    }
    .SearchByCode {
        width:310px;
    }
</style>

<div>
    <fieldset style="padding: 0">
        <legend>جستجو</legend>
        <div class="_5p odd-row">
            <div class="SearchByName displayinline" id="divSearchByName" runat="server">
                <label class="form-field-label" style="width:40px !important">
                    نام :</label>
                <asp:TextBox CssClass="inp" MaxLength="50" placeholder="" ID="txtFName" runat="server"></asp:TextBox>

                <label class="form-field-label" style="width:90px !important">
                    نام خانوادگی :</label>
                <asp:TextBox CssClass="inp" MaxLength="50" ID="txtLName" runat="server"></asp:TextBox>

                <asp:Button title="جستجو" ID="btnSearchByName" OnClientClick="return SearchByName();"
                    CssClass="btn btn-primary sbtn" runat="server" Text="جستجو"
                    OnClick="btnSearchByName_Click" />
            </div>

            <div class="SearchByCode displayinline" id="divSearchByCode" runat="server">
                <label class="form-field-label" style="width:90px !important">
                    کد پرسنلی :</label>
                <asp:TextBox CssClass="inp" MaxLength="14" ID="txtPersonnelCode" runat="server"></asp:TextBox>
                <asp:Button title="جستجو" ID="btnSearchByCode" OnClientClick="return SearchByPCode();"
                    CssClass="btn btn-primary sbtn" runat="server" Text="جستجو"
                    OnClick="btnSearchByCode_Click" />
            </div>
        </div>
        <div class="even-row" style="padding: 0 5px 5px;">
            <label class="form-field-label" style="width:90px !important">
                استان :</label>
            <asp:DropDownList CssClass="form-select" ID="ddlOstan" AutoPostBack="true" OnSelectedIndexChanged="ddlOstan_SelectedIndexChanged"
                runat="server">
                <asp:ListItem Text="انتخاب استان" Value="-1"></asp:ListItem>
            </asp:DropDownList>
            <label class="form-field-label" style="width:90px !important">
                منطقه :</label>
            <asp:DropDownList CssClass="form-select" ID="ddlRegion" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"
                runat="server">
                <asp:ListItem Text="انتخاب منطقه" Value="-1"></asp:ListItem>
            </asp:DropDownList>
            <label class="form-field-label" style="width:90px !important">
                مدرسه :</label>
            <asp:DropDownList CssClass="form-select" ID="ddlSchool" Enabled="false" runat="server">
                <asp:ListItem Text="انتخاب مدرسه" Value="-1"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button title="پالایش نتایج" ID="btnFilter"
                CssClass="btn btn-primary sbtn" runat="server" Text="پالایش نتایج" OnClientClick="lockScreen();"
                OnClick="btnFilter_Click" />
        </div>
    </fieldset>
    <fieldset style="padding: 0">
        <legend>نتایج جستجو</legend>
        <div class="_5p odd-row">
            <label>تعداد کل نتایج جستجو : </label>
            <asp:Label ID="lblTotalSearchCount" runat="server" Text=""></asp:Label>
        </div>
        <div class="grid">
            <cc1:SankaGrid ID="gvSearchResult" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="RowNumber" HeaderText="ردیف" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="OstanName" data-sankatooltip="مرتب سازی براساس استان" ToolTip="مرتب سازی براساس استان"
                                CommandName="ASC" ID="lnkbtnOstanName" OnCommand="lnkbtnSort_Command" runat="server">استان</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgOstanName"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("OstanName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="RegionName" data-sankatooltip="مرتب سازی براساس منطقه" ToolTip="مرتب سازی براساس منطقه"
                                CommandName="ASC" ID="lnkbtnRegionName" OnCommand="lnkbtnSort_Command" runat="server">منطقه</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgRegionName"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("RegionName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="SchoolName" data-sankatooltip="مرتب سازی براساس مدرسه" ToolTip="مرتب سازی براساس مدرسه"
                                CommandName="ASC" ID="lnkbtnSchoolName" OnCommand="lnkbtnSort_Command" runat="server">مدرسه</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgSchoolName"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("SchoolName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="fname">
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="FirstName" data-sankatooltip="مرتب سازی براساس نام" ToolTip="مرتب سازی براساس نام"
                                CommandName="ASC" ID="lnkbtnFirstName" OnCommand="lnkbtnSort_Command" runat="server">نام</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgFirstName"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("FirstName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="lname">
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="LastName" data-sankatooltip="مرتب سازی براساس نام خانوادگی" ToolTip="مرتب سازی براساس نام خانوادگی"
                                CommandName="ASC" ID="lnkbtnLastName" OnCommand="lnkbtnSort_Command" runat="server">نام خانوادگی</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgLastName"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("LastName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="نام پدر" ItemStyle-CssClass="fathername">
                        <ItemTemplate>
                            <%# Eval("FatherName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="pcode">
                        <HeaderTemplate>
                            <asp:LinkButton CommandArgument="PersonnelCode" data-sankatooltip="مرتب سازی براساس کد پرسنلی" ToolTip="مرتب سازی براساس کد پرسنلی"
                                CommandName="ASC" ID="lnkbtnPersonnelCode" OnCommand="lnkbtnSort_Command" runat="server">کد پرسنلی</asp:LinkButton>
                            <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgPersonnelCode"
                                runat="server" Visible="false" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("PersonnelCode")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="انتخاب">
                        <ItemTemplate>
                            <button type="button" class="btn btn-success sbtn" data-fname='<%# Eval("FirstName")%>' data-lname='<%# Eval("LastName")%>' data-pcode='<%# Eval("PersonnelCode")%>'
                                data-ostan='<%# Eval("OstanCode")%>' data-region='<%# Eval("RegionCode")%>' data-school='<%# Eval("SchoolCode")%>' data-fathername='<%# Eval("FatherName")%>' onclick="SelectRecord(this)">
                                تایید</button>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    رکوردی موجود نیست .
                </EmptyDataTemplate>
            </cc1:SankaGrid>
        </div>
    </fieldset>
</div>

<asp:HiddenField ID="hfParentFirstNameInputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentLastNameInputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentFatherNameInputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentFatherNameOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentPersonnelCodeInputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfFirstNameMinCharLengh" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfLastNameMinCharLengh" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfPersonnelCodeMinCharLengh" runat="server" Value="0" ClientIDMode="Static" />

<asp:HiddenField ID="hfParentFirstNameOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentLastNameOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentPersonnelCodeOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentOstanCodeOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentRegionCodeOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfParentSchoolCodeOutputFieldID" runat="server" Value="0" ClientIDMode="Static" />

<%--for Paging--%>
<asp:HiddenField ID="hfPageNumber" runat="server" Value="1" ClientIDMode="Static" />
<asp:HiddenField ID="hfPageSize" runat="server" Value="10" ClientIDMode="Static" />
<asp:HiddenField ID="hfPageChanged" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfTotalRow" runat="server" Value="0" ClientIDMode="Static" />

<asp:HiddenField ID="hfBtnclick" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfIsFirstLoad" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
<asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />

