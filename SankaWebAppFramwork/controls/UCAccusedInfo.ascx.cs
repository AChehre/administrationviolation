﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCAccusedInfo : GeneralControl
    {
        /// <summary>
        /// بدست آوردن وضعیت پرونده متهم
        /// </summary>
        public byte ViolationFileStatus
        {
            get
            {
                return hfStatus.Value.StringToByte();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblStatus.Text = Board.Common.getFileStatusLabel(this.FileID);
            }
        }

        /// <summary>
        /// بارگذاری جزئیات پرونده تخلف
        /// </summary>
        /// <param name="FileID"></param>
        public long LoadData()
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();
            obj.FileID = this.FileID;
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                // بار گذاری اطلاعات متهم
                txtPersonnelCode.Text = obj.PersonnelCode.ToString();
                txtFName.Text = obj.FirstName;
                txtLName.Text = obj.LastName;
                txtFatherName.Text = obj.FatherName;
                txtSerialCode.Text = obj.SerialCode;
                hfStatus.Value = obj.Status.ToString();
                txtNationalCode.Text = obj.NationalCode;
                txtOstanCode.Text = obj.OstanName;
                txtRegionName.Text = obj.RegionName;
                txtSchoolName.Text = obj.SchoolName;
                return obj.PersonnelCode.Value;
            }
            return 0;
        }
    }
}