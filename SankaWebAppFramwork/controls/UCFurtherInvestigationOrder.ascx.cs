﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCFurtherInvestigationOrder : GeneralControl
    {
        public delegate void dlgFurtherInvestigationOrder_Completed(bool Status);
        public event dlgFurtherInvestigationOrder_Completed FurtherInvestigationOrder_Completed;

        public delegate void dlgFurtherInvestigationOrder_CancelClick();
        public event dlgFurtherInvestigationOrder_CancelClick Cancel_Click;

        public delegate void dlgFurtherInvestigationOrder_Error(string errorMessage);
        public event dlgFurtherInvestigationOrder_Error Error_Occured;

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long SendingRequestID
        {
            get
            {
                if (ViewState["ID"] != null)
                {
                    return ViewState["ID"].ObjectToLong();
                }
                return 0;
            }
            set
            {
                ViewState.Add("ID", value);
            }
        }

        /// <summary>
        /// غیر فعال کردن کمبو باکس هیات مقصد
        /// </summary>
        public bool LockToBoardDropDownList
        {
            set
            {
                ViewState.Add("LockToBoardDropDownList", value);
            }
            get
            {
                if (ViewState["LockToBoardDropDownList"] != null)
                {
                    return ViewState["LockToBoardDropDownList"].ToString().StringToBool();
                }
                return false;
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Board.Common.BindBoards(ddlFromBoard);
                Board.Common.BindBoards(ddlToBoard);

                long ShoraBoardID = (new DAL.Board.tblBoard.Methods()).SelectByBoardType(new DTO.Board.tblBoard.tblBoard { BoardType = 3 })[0].BoardID;
                for (int i = 0; i < ddlToBoard.Items.Count; i++)
                {
                    if (ddlToBoard.Items[i].Value.StringToLong() == ShoraBoardID)
                    {
                        ddlToBoard.Items.RemoveAt(i);
                        break;
                    }
                }
                Board.Common.BindYear(ddlApplicationYear);
            }

            BindActionMenu();
            //ddlFromBoard.Enabled = !this.LockToBoardDropDownList;
            //ddlFromBoard.SelectedValue = this.BoardID.ToString();
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);
            ddlViolationFile.Enabled = false;
            //ddlApplicationYear.Enabled = false;
        }

        private long getFileBoardID()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile { FileID = this.FileID };
            return (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj).BoardID.Value;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            //ddlViolationFile.Enabled = false;
            txtComment.Enabled = false;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            //ddlViolationFile.Enabled = true;
            txtComment.Enabled = true;
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && this.RequestDataID == 0)
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ShowConfirm(this,'توجه : پس از ارسال دستور رسیدگی مجدد ، امکان حذف درخواست میسر نمیباشد . آیا از انجام عملیات اطمینان دارید ؟')",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Validated()) return;
            long NewFileID = MakeParallerViolationFile();
            if (NewFileID > 0)
            {
                if (InsertRelatedFileAccusitionListIntoNewFile(NewFileID, this.FileID))
                {
                    if (MakeFileSendingRequest(NewFileID))
                    {
                        if (UpdatePanelReferHistoryCounterField(NewFileID))
                        {
                            this.FurtherInvestigationOrder_Completed(true);
                            return;
                        }
                    }
                }
                this.Error_Occured("در پروسه ایجاد پرونده بررسی مجدد خطایی رخ داده است .");
            }
        }

        /// <summary>
        /// بروزرسانی عدد نمایانگر تعداد دفعت ارجاع این پرونده
        /// </summary>
        /// <param name="NewFileID">شناسه پرونده جدید</param>
        /// <returns></returns>
        private bool UpdatePanelReferHistoryCounterField(long NewFileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
            };
            long referHistoryCounter = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj).ReferHistoryCounter;

            var objNewFile = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = NewFileID,
                ReferHistoryCounter = referHistoryCounter + 1
            };
            return (new DAL.Board.tblViolationFile.Methods()).UpdatePanelReferHistoryCounterField(objNewFile);
        }

        /// <summary>
        /// ثبت اتهامات پرونده مورد درخواست در پرونده ایجاد شده جدید
        /// </summary>
        /// <param name="NewFileID">شناسه پرونده جدید</param>
        /// <param name="OldFileID">شناسه پرونده قبلی</param>
        /// <returns></returns>
        private bool InsertRelatedFileAccusitionListIntoNewFile(long NewFileID, long OldFileID)
        {
            DTO.Board.tblAccusation.tblAccusation obj = new DTO.Board.tblAccusation.tblAccusation
            {
                ViolationFileID = OldFileID
            };
            var lstPrevAccusitions = (new DAL.Board.tblAccusation.Methods()).SelectByFileID(obj);

            if (!lstPrevAccusitions.Any()) return false;

            foreach (var objPrevAccusition in lstPrevAccusitions)
            {
                DTO.Board.tblAccusation.tblAccusation objNewAccusition = new DTO.Board.tblAccusation.tblAccusation
                {
                    ViolationFileID = NewFileID,
                    AccusationSubjectID = objPrevAccusition.AccusationSubjectID,
                    AccusationDate = objPrevAccusition.AccusationDate,
                    Description = objPrevAccusition.Description,
                    Status = 0,
                    RegisterDate = DateTime.Now,
                    ReporterName = objPrevAccusition.ReporterName,
                    RequestID = getAddAcusitionRequestID(ddlToBoard.SelectedValue.StringToLong()),
                    ReportSource = objPrevAccusition.ReportSource,
                    EventPlace = objPrevAccusition.EventPlace,
                    UserID = this.UserID
                };
                long[] ReturnIDs = (new DAL.Board.tblAccusation.Methods()).Insert(objNewAccusition);

                if (ReturnIDs[0] > 0)
                {
                    long accusationID = ReturnIDs[0];
                    long requestDataID = ReturnIDs[1];
                    if (accusationID > 0 && requestDataID > 0)
                    {
                        DTO.Board.tblFileAttachment.tblFileAttachment objFileAttachment = new DTO.Board.tblFileAttachment.tblFileAttachment
                        {
                            RequestDataID = objPrevAccusition.RequestDataID
                        };
                        var lstFileAttachment = (new DAL.Board.tblFileAttachment.Methods()).SelectByRequestDataID(objFileAttachment);
                        if (lstFileAttachment.Any())
                        {
                            foreach (var fileAttachment in lstFileAttachment)
                            {
                                fileAttachment.RequestDataID = requestDataID;
                                fileAttachment.ViolationFileID = NewFileID;
                                long AttachmentID = (new DAL.Board.tblFileAttachment.Methods()).Insert(fileAttachment);
                                if (AttachmentID > 0)
                                {
                                    Board.Common.UpdateRequestDataStatus(requestDataID);
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// ایجاد یک رکورد درخواست ارسال پرونده
        /// </summary>
        /// <param name="newFileID">شناسه فایل جدید</param>
        /// <returns></returns>
        private bool MakeFileSendingRequest(long newFileID)
        {
            bool Result = false;
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                FileID = newFileID,
                FromBoardID = ddlFromBoard.SelectedValue.StringToLong(),
                ToBoardID = ddlToBoard.SelectedValue.StringToLong(),
                Status = 1,
                RequestDate = DateTime.Now,
                RequestedFileSerialCode = ddlViolationFile.SelectedItem.Text,
                Comment = txtComment.Text.CorrectArabicChars(),
                RequestDataDescription = " درخواست ارسال پرونده [ " + ddlViolationFile.SelectedItem.Text + " ] از " + ddlFromBoard.SelectedItem.Text,
                RequestID = this.RequestID,
                RelatedFileID = ddlViolationFile.SelectedValue.StringToLong(),
                RequesterBoardType = this.UserBoardType,
                UserID = this.UserID
            };
            long[] ReturnIDs = (new DAL.Board.tblSendingFileRequest.Methods()).Insert(obj);
            if (ReturnIDs[0] > 0)
            {
                this.SendingRequestID = ReturnIDs[0];
                this.RequestDataID = ReturnIDs[1];
                if (this.SendingRequestID > 0 && this.RequestDataID > 0)
                {
                    Result = true;
                }
            }
            if (Result)
            {
                LoadData();
            }
            return Result;
        }

        /// <summary>
        /// ساخت یک پرونده جدید و ارتباط دادن آن با پرونده مورد درخواست جهت بررسی مجدد
        /// </summary>
        /// <returns></returns>
        private long MakeParallerViolationFile()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);

            //------------------------------
            long oldFileID = obj.FileID;
            string oldFileSerialCode = obj.SerialCode;
            //------------------------------

            // چک کردن وجود تنظیمات شمارنده برای این هیات در سال انتخابی
            CheckCounterSetting(ddlToBoard.SelectedValue.StringToLong(), ddlApplicationYear.SelectedValue.StringToInt());

            if (obj.RowNumber > 0)
            {
                obj.BoardID = ddlToBoard.SelectedValue.StringToLong();
                obj.CreationDate = DateTime.Now;
                obj.Status = 0;  //تشکیل شده - فاقد اتهام
                obj.ActivityYear = ddlApplicationYear.SelectedValue.StringToInt();
                obj.FileReferType = ddlFileReferType.SelectedValue.StringToByte();
                long fileID = (new DAL.Board.tblViolationFile.Methods()).Insert(obj);
                if (fileID > 0)
                {
                    // Never Use !!!!
                    //long RequestDataID = Board.Common.InsertRequestDataRecord(this.RequestID, fileID, string.Format("این پرونده جهت بررسی مجدد به این هیات {0} ارجاع گردید .",ddlToBoard.SelectedItem.Text));
                    //if(RequestDataID > 0)
                    {
                        InsertAccessMap(oldFileID);
                        InsertFileRelationMap(fileID, oldFileID, oldFileSerialCode);
                        if (Board.Common.InsertRequestDataRecord(this.UserID, getCreateFileRequestID(ddlToBoard.SelectedValue.StringToLong()), fileID, "تشکیل پرونده صورت گرفت .") > 0)
                        {
                            return fileID;
                        }
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// ثبت یک رکورد درخواست از نوع ایجاد پرونده
        /// </summary>
        /// <param name="boardID"></param>
        /// <returns></returns>
        private int getCreateFileRequestID(long boardID)
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = boardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            byte BoardType = obj.BoardType;
            switch (BoardType)
            {
                case 1:
                    {
                        return 1;
                    }
                case 2:
                    {
                        return 29;
                    }
                case 3:
                    {
                        return 56;
                    }
            }
            return 0;
        }

        /// <summary>
        /// استخراج شناسه درخواست پس از درج اتهام
        /// </summary>
        /// <param name="boardID">شناسه هیات</param>
        /// <returns></returns>
        private int getAddAcusitionRequestID(long boardID)
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = boardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            byte BoardType = obj.BoardType;
            switch (BoardType)
            {
                case 1:
                    {
                        return 2;
                    }
                case 2:
                    {
                        return 30;
                    }
                case 3:
                    {
                        return 57;
                    }
            }
            return 0;
        }

        /// <summary>
        /// تغییر هیات بررسی کننده پرونده
        /// </summary>
        /// <param name="boardID">شنایه هیات</param>
        /// <returns></returns>
        private bool ChangeViolationFileBoard(long boardID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                BoardID = boardID,
                FileID = this.FileID
            };
            return (new DAL.Board.tblViolationFile.Methods()).ChangeVilationFileBoard(obj);
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool Validated()
        {
            //if (ddlFromBoard.SelectedValue.Equals(ddlToBoard.SelectedValue))
            //{
            //    this.Error_Occured("هیات مقصد و مبدا یکسان است .");
            //    return false;
            //}
            if (ddlViolationFile.SelectedIndex == 0)
            {
                this.Error_Occured("پرونده مورد نظر را انتخاب نمایید .");
                return false;
            }
            if (txtComment.IsEmpty())
            {
                this.Error_Occured("درج توضیحات الزامی است .");
                return false;
            }
            return true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel_Click();
        }

        protected void ddlFromBoard_SelectedIndexChanged(object sender, EventArgs e)
        {
            Board.Common.BindViolationfile(ddlViolationFile, ddlFromBoard.SelectedValue.StringToLong(),this.BoardID);
        }

        protected void ddlFromBoard_DataBound(object sender, EventArgs e)
        {
            ddlFromBoard_SelectedIndexChanged(sender, e);
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک اتهام
        /// </summary>
        /// <param name="AccusationID"></param>
        public void LoadData()
        {
            getFileID();
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest();
            obj.ID = this.SendingRequestID;
            obj = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                ddlFromBoard.SelectedValue = getFileBoardID().ToString();
                ddlFromBoard_SelectedIndexChanged(null, null);
                ddlViolationFile.SelectedValue = obj.RelatedFileID.ToString();
                txtComment.Text = obj.Comment;
                //pnlAttachment.Show();
                this.RequestDataID = obj.RequestDataID;
                ddlFromBoard.SelectedValue = getFileBoardID().ToString();
                ddlToBoard.SelectedValue = obj.ToBoardID.ToString();
                divAccusitionField.Enabled = false;
                ddlFileReferType.SelectedValue = obj.FileReferType.ToString();
            }
            else
            {
                ddlFromBoard.SelectedValue = getFileBoardID().ToString();
                ddlFromBoard_SelectedIndexChanged(null,null);
                ddlViolationFile.SelectedValue = this.FileID.ToString();
            }
            BindActionMenu();
        }

        /// <summary>
        /// استخذاج شناسه پرونده ایجاد شده
        /// </summary>
        private void getFileID()
        {
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                RequestDataID = this.RequestDataID
            };
            this.SendingRequestID = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByRequestDataID(obj);
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                if (ChangeViolationFileBoard(ddlToBoard.SelectedValue.StringToLong()))
                {
                    SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                }
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        /// <summary>
        /// ثبت رکورد ارتباط دهی این پرونده  جدید با پرونده قبلی
        /// </summary>
        /// <param name="newFileID"></param>
        /// <param name="oldFileID"></param>
        /// <param name="oldFileSerialCode"></param>
        /// <returns></returns>
        private bool InsertFileRelationMap(long newFileID, long oldFileID, string oldFileSerialCode)
        {
            var obj = new DTO.Board.tblFileRelationMap.tblFileRelationMap
            {
                FileID = newFileID,
                RelatedFileID = oldFileID,
                SerialCode = oldFileSerialCode
            };
            return (new DAL.Board.tblFileRelationMap.Methods()).Insert(obj);
        }

        /// <summary>
        /// ثبت رکورد دسترسی هیات به پرونده مرتبط با پرونده جدید
        /// </summary>
        /// <param name="oldFileID"></param>
        /// <returns></returns>
        private bool InsertAccessMap(long oldFileID)
        {
            var obj = new DTO.Board.tblFileAccessMap.tblFileAccessMap
            {
                FileID = oldFileID,
                BoardID = ddlToBoard.SelectedValue.StringToLong()
            };
            return (new DAL.Board.tblFileAccessMap.Methods()).Insert(obj);
        }

        /// <summary>
        /// بروزرسانی شمارنده ها سریال نامه ها
        /// </summary>
        /// <param name="boardID">شناسه هیات</param>
        /// <param name="activityYear">سال عملیاتی</param>
        private void CheckCounterSetting(long boardID , int activityYear)
        {
            if (boardID == 0) return;

            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting
            {
                BoardID = boardID,
                Year = activityYear
            };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            if (obj.RowNumber == 0)
            {
                var objSetting = new DTO.Board.tblCounterSetting.tblCounterSetting
                {
                    FileSerialCounter = 1,
                    VoteCounter = 1,
                    VoteNotificationCounter = 1,
                    ViolationNotificationCounter = 1,
                    BoardID = boardID,
                    Year = activityYear
                };
                bool bln = (new DAL.Board.tblCounterSetting.Methods()).Insert(objSetting);
            }
        }
    }
}