﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAccusedInfo.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCAccusedInfo" %>
<div class="panel-group" id="accordion">
    <h4 class="panel-title">
        وضعیت پرونده :
        <asp:Label runat="server" ID="lblStatus" /></h4>
    <div class="panel panel-primary" id="pnlRequestsList" runat="server">
        <div data-toggle="collapse" data-target="#collapse10" class="panel-heading pointer-cursor">
            <h4 runat="server" id="hTitle" class="panel-title">
                مشخصات شخص مورد اتهام</h4>
        </div>
        <div id="collapse10" class="panel-collapse collapse in">
            <div class="panel-body prevententer">
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        نام :</label>
                    <asp:TextBox ReadOnly="true" ID="txtFName" runat="server" />
                    <label class="form-field-label">
                        نام خانوادگی :</label>
                    <asp:TextBox ReadOnly="true" ID="txtLName" runat="server" />
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        نام پدر :</label>
                    <asp:TextBox ReadOnly="true" ID="txtFatherName" runat="server" />
                     <label class="form-field-label">
                        کد ملی :</label>
                    <asp:TextBox ReadOnly="true" ID="txtNationalCode" runat="server" />
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        کد پرسنلی :</label>
                    <asp:TextBox ReadOnly="true" ID="txtPersonnelCode" runat="server" />
                    <label class="form-field-label">
                        کلاسه پرونده :</label>
                    <asp:TextBox ReadOnly="true" ID="txtSerialCode" runat="server" />
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        استان :</label>
                    <asp:TextBox ReadOnly="true" ID="txtOstanCode" runat="server" />
                    <label class="form-field-label">
                       منطقه :</label>
                    <asp:TextBox ReadOnly="true" ID="txtRegionName" runat="server" />
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        مدرسه :</label>
                    <asp:TextBox ReadOnly="true" ID="txtSchoolName" runat="server" />
                    
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hfFileID" runat="server" Value="0" />
<asp:HiddenField ID="hfStatus" runat="server" Value="0" />
