﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAddViolationFile.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCAddViolationFile" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="sanka" TagName="AutoComplete" Src="~/controls/AutoComplete.ascx" %>
<cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
    ClientIDMode="Static">
</cc2:SankaDialog>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="container">
    <fieldset>
        <legend>مشخصات شخص مورد اتهام</legend>
        <div class="prevententer">
            <div class="_5p odd-row">
                <label class="form-field-label">
                    تاریخ تشکیل پرونده :</label>
                <uc1:UCDatePicker ID="txtCreationDate" DontValidate="false" runat="server" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    کد پرسنلی :</label>
                <asp:TextBox ID="txtPersonnelCode" MaxLength="8" runat="server" data-required="true" />
                <%-- <asp:ImageButton ID="btnSearch" runat="server" 
                ImageUrl="~/images/icon/Search.png" />--%>
                <asp:ImageButton class="cp_vm" ID="btnSearch" runat="server" ImageUrl="~/images/icon/Search.png"
                    OnClick="btnSearch_Click" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    کد ملی :</label>
                <asp:TextBox ID="txtNationalCode" MaxLength="10" runat="server" data-required="false" />
                <asp:ImageButton class="cp_vm" ID="btnSearchNationalCode" runat="server" ImageUrl="~/images/icon/Search.png"
                    OnClick="btnSearchNationalCode_Click" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    نام :</label>
                <asp:TextBox ID="txtFName" MaxLength="10" runat="server" data-required="true" />
                <%--<span id="sanka_basic_modal"><a href="/SearchUsers.aspx" data-sankatooltip="جستجو"
                    data-title="جستجو" data-duplicatetitle="false" data-width="900" data-height="650"
                    class='basic'>
                    <img id="img1" class="cp_vm" alt="" src="/images/icon/Search.png" />
                </a></span>--%>
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    نام خانوادگی :</label>
                <asp:TextBox ID="txtLName" MaxLength="50" runat="server" data-required="true" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    نام پدر :</label>
                <asp:TextBox ID="txtFatherName" MaxLength="50" runat="server" data-required="true" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    استان :</label>
                <asp:TextBox ID="txtOstanName" MaxLength="50" runat="server" data-required="false" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    منطقه :</label>
                <asp:TextBox ID="txtRegionName" MaxLength="50" runat="server" data-required="false" />
                <asp:HiddenField ID="hfOstanCode" runat="server" />
                <asp:HiddenField ID="hfRegionCode" runat="server" />
                <asp:HiddenField ID="hfSchoolCode" runat="server" />
            </div>
            <div class="_5p odd-row">
                <label class="form-field-label">
                    مدرسه :</label>
                <asp:TextBox ID="txtSchoolName" MaxLength="50" runat="server" data-required="false" />
            </div>
            <sanka:AutoComplete ShowWaitingFullScreen="false" NameDelay="1000" runat="server"
                NameMinLength="3" PersonalCodeMinLength="8" ClientIDMode="Static" ID="AutoComplete1">
            </sanka:AutoComplete>
        </div>
    </fieldset>
</div>
<asp:HiddenField ID="hfRelatedPersonFullName" runat="server" ClientIDMode="Static" />
