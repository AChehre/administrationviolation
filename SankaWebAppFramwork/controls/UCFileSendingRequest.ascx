﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFileSendingRequest.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCFileSendingRequest" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                درخواست ارسال پرونده</h4>
        </div>
        <div class="panel-body prevententer">
            <asp:Panel ID="divAccusitionField" runat="server" ClientIDMode="Static">
                <div class="_5p">
                    <label class="form-field-label">
                        نام هیات مبداء :</label>
                    <asp:DropDownList ID="ddlFromBoard" runat="server" Width="250px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlFromBoard_SelectedIndexChanged" OnDataBound="ddlFromBoard_DataBound">
                    </asp:DropDownList>
                </div>
                
                <div class="_5p">
                    <label class="form-field-label">
                        لیست پرونده ها :</label>
                    <asp:DropDownList RepeatLayout="Flow" ID="ddlViolationFile" runat="server" Width="200">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        نام هیات مقصد :</label>
                    <asp:DropDownList ID="ddlToBoard" runat="server" Width="250px" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
                <div class="_5p">
                    <label class="form-field-label">
                        توضیحات :</label>
                    <asp:TextBox TextMode="MultiLine" style="text-align:right" runat="server" ID="txtComment" Height="93px" Width="499px"></asp:TextBox>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
        <uc2:sankafileupload id="FileUpload1" runat="server" />
    </asp:Panel>
    <div class="_5p even-row">
        <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
            ClientIDMode="Static">
        </cc1:SankaDialog>
    </div>
</div>
