﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace SankaWebAppFramework.controls
{
    public partial class UCViolationFileVote : GeneralControl
    {
        public delegate void dlgVote_Completed(bool Status);
        public event dlgVote_Completed Vote_Completed;

        public delegate void dlgVote_Cancel();
        public event dlgVote_Cancel Cancel;

        public delegate void dlgError(List<string> lstMessage);
        public event dlgError Error_Occured;

        /// <summary>
        /// شناسه رای صادره
        /// </summary>
        public long VoteID
        {
            get
            {
                if (ViewState["VoteID"] != null)
                {
                    return ViewState["VoteID"].ObjectToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("VoteID", value);
            }
        }

        /// <summary>
        /// نوع ارجاع پرونده
        /// </summary>
        public byte FileReferType
        {
            get
            {
                if (ViewState["FileReferType"] != null)
                {
                    return ViewState["FileReferType"].ObjectToByte();
                }
                return 0;
            }
            set
            {
                ViewState.Add("FileReferType", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Board.Common.BindSodourType(ddlSoudorType);
                SankaWebAppFramework.Board.Common.BindPenaltyType(ddlPenaltyType);
                SankaWebAppFramework.Board.Common.BindVoteType(ddlVoteType);
                txtVoteDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
            }
            BindActionMenu();
        }

        /// <summary>
        /// استخراج نوع ارجاع پرونده
        /// </summary>
        private void getFileReferType()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            this.FileReferType = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj).FileReferType;
        }

        /// <summary>
        /// بارگذاری اطلاعات
        /// </summary>
        public void LoadData()
        {
            getFileReferType();

            #region بدست آوردن اطلاعات رای ثبت شده

            DTO.Board.tblViolationFileVote.tblViolationFileVote Obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote();

            Obj.ViolationFileID = FileID;
            Obj = (new DAL.Board.tblViolationFileVote.Methods()).SelectByViolationFileID(Obj);

            if (Obj.RowNumber > 0)
            {
                this.VoteID = Obj.VoteID;
                txtPenaltyDetail.Text = Obj.PenaltyDetail;
                ddlVoteType.SelectedValue = Obj.VoteType.ToString();
                ddlVoteType_SelectedIndexChanged(null, null);
                ddlPenaltyType.SelectedValue = Obj.PenaltyType.ToString();
                rbtnIsFinite.SelectedValue = Obj.IsFinite.ToString();
                txtVoteNumber.Text = Obj.VoteNumber.ToString();
                //txtVoteNumber.Enabled = false;
                txtVoteDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(Obj.VoteDate);
                txtVoteNumber.Enabled = false;
                txtAddress.Text = Obj.Address;
                ddlSoudorType.SelectedValue = Obj.SodourType.ToString();
            }

            rbtnIsFinite_SelectedIndexChanged(null, null);
            #endregion بدست آوردن اطلاعات رای ثبت شده

            BindActionMenu();

            #region بدست آوردن وضعیت هیات جاری

            DTO.Admin.tblBoard.tblBoard ObjBoard = new DTO.Admin.tblBoard.tblBoard();

            ObjBoard.BoardID = BoardID;

            ObjBoard = (new DAL.Admin.tblBoard.Methods()).SelectByID(ObjBoard);

            if (ObjBoard.RowNumber > 0)
            {
                if (ObjBoard.BoardType.Value == (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision)
                {
                    rbtnIsFinite.Enabled = false;
                    rbtnIsFinite.SelectedValue = "True";
                    rbtnIsFinite_SelectedIndexChanged(null, null);
                }
            }
            #endregion بدست آوردن وضعیت هیات جاری

            if (!HasAnyConfirmAccusation())
            {
                ddlVoteType.SelectedIndex = 0;
            }
            else
            {
                ddlVoteType.SelectedIndex = 1;
            }
            ddlVoteType.Enabled = false;
            ddlVoteType_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// عدم وجود اتهام محرز شده
        /// </summary>
        /// <returns></returns>
        private bool HasAnyConfirmAccusation()
        {
            DTO.Board.tblAccusation.tblAccusation obj = new DTO.Board.tblAccusation.tblAccusation();
            obj.ViolationFileID = this.FileID;
            return (new DAL.Board.tblAccusation.Methods()).HasAnyConfirmAccusation(obj);
        }

        /// <summary>
        /// ذخیره رای
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Validate()) return;

            if (hfIgnoreFlagID.Value.StringToByte() == 0)
            {
                var obj = new DTO.Board.tblFileAttachment.tblFileAttachment { ViolationFileID = this.FileID };
                DateTime? dtMaxAttachmentDate = (new DAL.Board.tblFileAttachment.Methods()).GetMaxDocumentDate(obj);
                if (dtMaxAttachmentDate.HasValue)
                {
                    if ((dtMaxAttachmentDate.Value - txtVoteDate.DateMiladi).Days > 0)
                    {
                        string strWarnning = "تاریخ برخی ضمائم بعد از تاریخ صدور رای است . آیا با این وجود رای صادر گردد ؟";
                        SankaDialog1.ShowMessageYesNo(strWarnning, DialogMessage.SankaDialog.Message_Type.Warning, Page, true, "SetIgnoreFlagAndSend");
                        return;
                    }
                }
            }
            bool Result = false;

            DTO.Board.tblViolationFileVote.tblViolationFileVote Obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote();
            Obj.AutoVoteNumber = (txtVoteNumber.Text.StringToLong() == 0);
            Obj.VoteNumber = txtVoteNumber.Text.StringToLong();
            Obj.VoteID = this.VoteID;
            Obj.PenaltyDetail = txtPenaltyDetail.Text;
            Obj.PenaltyType = ddlPenaltyType.SelectedValue.StringToByte();
            Obj.VoteType = ddlVoteType.SelectedValue.StringToByte();
            Obj.FinalResult = getFinalResult();
            Obj.ViolationFileID = FileID;
            Obj.IsFinite = rbtnIsFinite.SelectedValue.StringToBool();
            Obj.FiniteVoteType = this.FileReferType;
            Obj.VoteDate = txtVoteDate.DateMiladi;
            Obj.SodourType = ddlSoudorType.SelectedValue.StringToByte();
            Obj.BoardID = this.BoardID;
            Obj.ActivityYear = this.ActivityYear;
            Obj.Address = txtAddress.Text.CorrectArabicChars();
            Obj.isFinal = true;

            if (this.VoteID > 0)
            {
                Result = (new DAL.Board.tblViolationFileVote.Methods()).Update(Obj);
            }
            else
            {
                Result = (new DAL.Board.tblViolationFileVote.Methods()).Insert(Obj);
            }

            if (Result)
            {
                this.RequestDataID = Board.Common.InsertRequestDataRecord(this.UserID,this.RequestID, this.FileID, "رای صادر گردید .", true, this.RequestDataID);
                if (RequestDataID > 0)
                {
                    if (this.VoteID > 0)
                    {
                        // لاگ ویرایش درخواست
                        (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);
                    }
                    else
                    {
                        // لاگ ثبت درخواست
                        (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                    }
                

                    if (SetFileCloseDate())
                    {
                        if (UserBoardType == 2)  //هیات تجدید نظر 
                        {
                            CheckForNaghzeRay();
                        }
                        LoadData();
                    }
                }
            }
            Vote_Completed(Result);
        }

        /// <summary>
        /// چک کردن نقض رای
        /// </summary>
        private void CheckForNaghzeRay()
        {
            long prevFileID = 0;
            DTO.Board.tblFileRelationMap.tblFileRelationMap obj = new DTO.Board.tblFileRelationMap.tblFileRelationMap
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblFileRelationMap.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                prevFileID = obj.RelatedFileID;

                var objBadviVote = new DTO.Board.tblViolationFileVote.tblViolationFileVote
                {
                    ViolationFileID = this.FileID
                };
                objBadviVote = (new DAL.Board.tblViolationFileVote.Methods()).SelectByViolationFileID(objBadviVote);

                var objTajdidNazarVote = new DTO.Board.tblViolationFileVote.tblViolationFileVote
                {
                    ViolationFileID = prevFileID
                };
                objBadviVote = (new DAL.Board.tblViolationFileVote.Methods()).SelectByViolationFileID(objBadviVote);

                if (objBadviVote.VoteType != objTajdidNazarVote.VoteType ||
                    objBadviVote.PenaltyType != objTajdidNazarVote.PenaltyType ||
                    objBadviVote.IsFinite != objTajdidNazarVote.IsFinite)
                {
                    var objCurrentFile = new DTO.Board.tblViolationFile.tblViolationFile
                    {
                        FileID = this.FileID
                    };
                    bool bln = (new DAL.Board.tblViolationFile.Methods()).SetNaghzeRay(objCurrentFile);
                }
            }
        }

        /// <summary>
        /// درج تاریخ بسته شدن پرونده
        /// </summary>
        /// <returns></returns>
        private bool SetFileCloseDate()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                FileCloseDate = DateTime.Now
            };

            if (ddlVoteType.SelectedValue == "0")
            {
                obj.VoteTypeCode = (byte)Board.Enums.VoteTypeCode.INNOCENCE;
            }
            else
            {
                if (rbtnIsFinite.SelectedValue.StringToBool())
                {
                    obj.VoteTypeCode = (byte)Board.Enums.VoteTypeCode.FINIT_CONDEMNATION;
                }
                else
                {
                    obj.VoteTypeCode = (byte)Board.Enums.VoteTypeCode.RESEARCHABLE_CONDEMNATION;
                }
            }
            return (new DAL.Board.tblViolationFile.Methods()).SetFileCloseDate(obj);
        }

        /// <summary>
        /// ذخیره رای
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTempSave_Click(object sender, EventArgs e)
        {
            if (!Validate()) return;
            bool Result = false;

            DTO.Board.tblViolationFileVote.tblViolationFileVote Obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote();
            Obj.AutoVoteNumber = (txtVoteNumber.Text.StringToLong() == 0);
            Obj.VoteNumber = txtVoteNumber.Text.StringToLong();
            Obj.VoteID = this.VoteID;
            Obj.PenaltyDetail = txtPenaltyDetail.Text;
            Obj.PenaltyType = ddlPenaltyType.SelectedValue.StringToByte();
            Obj.VoteType = ddlVoteType.SelectedValue.StringToByte();
            Obj.FinalResult = getFinalResult();
            Obj.ViolationFileID = FileID;
            Obj.IsFinite = rbtnIsFinite.SelectedValue.StringToBool();
            Obj.FiniteVoteType = this.FileReferType;
            Obj.VoteDate = txtVoteDate.DateMiladi;
            Obj.SodourType = ddlSoudorType.SelectedValue.StringToByte();
            Obj.BoardID = this.BoardID;
            Obj.ActivityYear = this.ActivityYear;
            Obj.Address = txtAddress.Text.CorrectArabicChars();
            Obj.isFinal = false;

            if (this.VoteID > 0)
            {
                Result = (new DAL.Board.tblViolationFileVote.Methods()).Update(Obj);
            }
            else
            {
                Result = (new DAL.Board.tblViolationFileVote.Methods()).Insert(Obj);
            }

            if (Result)
            {
                this.RequestDataID = Board.Common.InsertRequestDataRecord(this.UserID, this.RequestID, this.FileID, "رای صادر گردید .", false, this.RequestDataID);
                if (RequestDataID > 0)
                {
                    if (SetFileCloseDate())
                    {
                        LoadData();
                    }
                }
            }
            Vote_Completed(Result);
        }

        /// <summary>
        /// تعیین وضعیت نهایی پرونده
        /// </summary>
        /// <returns></returns>
        private byte getFinalResult()
        {
            if (ddlVoteType.SelectedValue.StringToByte() == (byte)SankaWebAppFramework.Board.Enums.VoteType.INNOCENCE)
            {
                return (byte)2;
            }
            else
            {
                return rbtnIsFinite.SelectedValue.StringToBool() ? (byte)3 : (byte)4;
            }
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// <param name="AfterSaveVote">ترو باشد ، یعنی بعد از صدور رای منو ساخته شود</param>
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if ((Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission()) ||  Board.Common.IsUnlockedFile(this.FileID))
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnTempSaveVote",
                    clientIDMode= System.Web.UI.ClientIDMode.Static,
                    Title = "ثبت موقت",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnTempSave_Click,
                    Icon = "/images/icon/save.png"
                });

                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSaveVote",
                    clientIDMode = System.Web.UI.ClientIDMode.Static,
                    Title = "ثبت نهایی",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ShowConfirm(this,'توجه:  پس از صدور رای امکان اصلاح یا ویرایش رای و جزئیات پرونده وجود نخواهد داشت. آیا از رای صادره اطمینان دارید؟!');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }
            if (this.VoteID > 0)
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnPrint",
                    Title = "پیش نمایش / چاپ",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnPrint_Click,
                    Icon = "/images/icon/printer.png"
                });
            }
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });
            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// غیر فعال کردن کنترل
        /// </summary>
        void LockAllControls()
        {
            if (!Board.Common.IsUnlockedFile(this.FileID))
            {
                txtPenaltyDetail.Enabled = false;
                ddlVoteType.Enabled = false;
                ddlPenaltyType.Enabled = false;
                rbtnIsFinite.Enabled = false;
                ddlSoudorType.Enabled = false;
                txtAddress.Enabled = false;
            }
        }

        /// <summary>
        /// فعال کردن کنترل
        /// </summary>
        void UnLockAllControls()
        {
            txtPenaltyDetail.Enabled = true;
            //ddlVoteType.Enabled = true;
            ddlPenaltyType.Enabled = true;
            rbtnIsFinite.Enabled = (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision);
            ddlSoudorType.Enabled = true;
            txtAddress.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel();
        }

        /// <summary>
        /// چاپ فرم رای
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            string PrintPageUrl = "";
            if (rbtnIsFinite.SelectedValue.StringToBool())
            {
                if ((UserBoardType == 1 || UserBoardType == 3) && ddlVoteType.SelectedValue == "0")
                {
                    PrintPageUrl = string.Format("/Board/print/BadviInnocenceVote.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                    Response.Redirect(PrintPageUrl);
                }
                if ((UserBoardType == 1 || UserBoardType == 3) && ddlVoteType.SelectedValue == "1")
                {
                    PrintPageUrl = string.Format("/Board/print/BadviFinalVote.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                    Response.Redirect(PrintPageUrl);
                }
                if ((UserBoardType == 2  || UserBoardType == 3) && ddlVoteType.SelectedValue == "1" && this.FileReferType == 2)
                {
                    PrintPageUrl = string.Format("/Board/print/TajdidNazarFinalVote-17.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                    Response.Redirect(PrintPageUrl);
                }
                if ((UserBoardType == 2  || UserBoardType == 3) && ddlVoteType.SelectedValue == "0")
                {
                    PrintPageUrl = string.Format("/Board/print/TajdidNazarInnocenceVote.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                    Response.Redirect(PrintPageUrl);
                }
                if ((UserBoardType == 2  || UserBoardType == 3) && ddlVoteType.SelectedValue == "1")
                {
                    PrintPageUrl = string.Format("/Board/print/TajdidNazarFinalVote.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                    Response.Redirect(PrintPageUrl);
                }
            }
            else
            {
                //switch (UserBoardType)
                //{
                    //case 1: // هیات بدوی
                     //   {
                            PrintPageUrl = string.Format("/Board/print/ResearchableVote.aspx?VoteID={0}&FileID={1}&BoardID={2}", this.VoteID, this.FileID, this.BoardID);
                            Response.Redirect(PrintPageUrl);
                    //        break;
                     //   }
                //}
            }
        }

        protected void ddlVoteType_SelectedIndexChanged(object sender, EventArgs e)
        {
            byte voteType = ddlVoteType.SelectedValue.StringToByte();
            switch (voteType)
            {
                case 0:
                    {
                        CondemnationDiv.Hide();
                        rbtnIsFinite.SelectedValue = true.ToString();
                        rbtnIsFinite.Enabled = false;
                        break;
                    }
                case 1:
                    {
                        CondemnationDiv.Show();
                        ddlPenaltyType_SelectedIndexChanged(sender, e);
                        break;
                    }

            }
            rbtnIsFinite_SelectedIndexChanged(sender, e);
        }

        /// <summary>
        /// اعتبار سنجی پیش از ارسال
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            List<string> lstMessage = new List<string>();

            if (HasNotCompletedRequest())
            {
                lstMessage.Add("قبل از صدور رای باید تمامی اقدامات صورت گرفته برروی پرونده تکمیل گردد .");
            }
            else
            {
                if (txtVoteNumber.Text.StringToLong() > 0 && VoteID == 0)
                {
                    long MaxNotificationCounter = getLastVoteCounter();
                    if (MaxNotificationCounter > txtVoteNumber.Text.StringToLong())
                    {
                        string strMessage = string.Format(" شمارنده  فعلی  برای رای صادره برابر  {0}  میباشد . شماره رای فعلی باید برابر یا بزرگتر از این مقدار تعیین گردد .", MaxNotificationCounter);
                        lstMessage.Add(strMessage);
                    }
                }

                if (!txtVoteDate.DateIsValid)
                {
                    lstMessage.Add("تاریخ صدور رای را وارد نمایید .");
                }
                if (txtPenaltyDetail.IsEmpty() && (Board.Enums.VoteType)ddlVoteType.SelectedValue.StringToByte() == Board.Enums.VoteType.CONDEMNATION)
                {
                    lstMessage.Add("ذکر شرح مجازات الزامی است .");
                }
                if (HasNotificationDateAfterVoteDate())
                {
                    lstMessage.Add("تاریخ برخی ابلاغیه ها بعد از تاریخ صدور رای است .");
                }
            }
            if (lstMessage.Any())
            {
                this.Error_Occured(lstMessage);
            }
            return (lstMessage.Count == 0);
        }

        private bool HasNotCompletedRequest()
        {
            var obj = new DTO.Board.tblRequestData.tblRequestData
            {
                FileID = this.FileID,
                RequestDataID = this.RequestDataID
            };
            return (new DAL.Board.tblRequestData.Methods()).HasAnyNotCompletedData(obj);
        }

        /// <summary>
        /// گرفتن آخرین شماره ابلاغیه برای این هیات و در سال عملیاتی انتخاب شده
        /// </summary>
        /// <returns></returns>
        private long getLastVoteCounter()
        {
            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting { BoardID = this.BoardID, Year = this.ActivityYear };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            return obj.VoteCounter;
        }

        /// <summary>
        /// چک کردن اینکه تاریخ ابلاغ رای حتما بعد از تاریخ صدور رای باشد
        /// </summary>
        /// <returns></returns>
        private bool HasNotificationDateAfterVoteDate()
        {
            var objNotification = new DTO.Board.tblViolationNotification.tblViolationNotification { ViolationFileID = this.FileID };
            var lstNotification = (new DAL.Board.tblViolationNotification.Methods()).SelectByViolationFileID(objNotification);
            var lst = lstNotification.Where(x => x.NotificationDate.Value > txtVoteDate.DateMiladi).ToList();
            return lst.Any();
        }

        /// <summary>
        /// اگر با وجود اینکه رایی قطعی یا غیر قطعی است عکس آن انتخاب شود اخطار میدهد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rbtnIsFinite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision)
            {
                rbtnIsFinite.SelectedValue = true.ToString();
                rbtnIsFinite.Enabled = false;
            }
            else if (ddlVoteType.SelectedValue == "1")
            {
                if (rbtnIsFinite.SelectedValue == false.ToString())
                {
                    List<int> lst = new List<int>();
                    lst.AddRange(new int[] { 0, 1, 2, 5, 6 });
                    if (lst.Contains(ddlPenaltyType.SelectedValue.StringToInt()))
                    {
                        SankaDialog1.ShowMessage("این رای از نوع قطعی میباشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                    }
                }
                if (rbtnIsFinite.SelectedValue == true.ToString())
                {
                    List<int> lst = new List<int>();
                    lst.AddRange(new int[] { 3, 4, 7, 8, 9, 10 });
                    if (lst.Contains(ddlPenaltyType.SelectedValue.StringToInt()))
                    {
                        SankaDialog1.ShowMessage("این رای از نوع قابل پژوهش میباشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                    }
                }
            }

            divAddress.Visible = (rbtnIsFinite.SelectedValue == false.ToString());
        }

        /// <summary>
        /// بر اساس اینکه چه نوع جریمه ای تعیین گردیده رای به حالت قطعی یا غیر قطعی در می آید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPenaltyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision)
            {
                rbtnIsFinite.SelectedValue = true.ToString();
                rbtnIsFinite.Enabled = false;
                rbtnIsFinite_SelectedIndexChanged(null, null);
                return;
            }
            if (ddlVoteType.SelectedValue == "1")
            {
                switch (ddlPenaltyType.SelectedValue.StringToInt())
                {
                    case 0:
                        {
                            rbtnIsFinite.SelectedValue = true.ToString();
                            break;
                        }
                    case 1:
                        {
                            rbtnIsFinite.SelectedValue = true.ToString();
                            break;
                        }
                    case 2:
                        {
                            rbtnIsFinite.SelectedValue = true.ToString();
                            break;
                        }
                    case 3:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                    case 4:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                    case 5:
                        {
                            rbtnIsFinite.SelectedValue = true.ToString();
                            break;
                        }
                    case 6:
                        {
                            rbtnIsFinite.SelectedValue = true.ToString();
                            break;
                        }
                    case 7:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                    case 8:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                    case 9:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                    case 10:
                        {
                            rbtnIsFinite.SelectedValue = false.ToString();
                            break;
                        }
                }
                //rbtnIsFinite.Enabled = false;
                rbtnIsFinite_SelectedIndexChanged(null, null);
            }
        }
    }
}