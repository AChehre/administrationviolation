﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SankaWebAppFramework.ascx.cs"
    Inherits="SankaWebAppFramework.Controls.AutoComplete" %>
<div id="UserSearchContainer">
    <div class="_5p odd-row">
        <label class="form-field-label">
            جستجو با نام :</label>
        <asp:TextBox MaxLength="50" ID="txtSearchFullName" runat="server"></asp:TextBox>
    </div>
    <div class="_5p even-row">
        <label class="form-field-label">
            جستجو با کد پرسنلی :</label>
        <asp:TextBox MaxLength="10" CssClass="MustBeNumber" ID="txtSearchPersonnelCode" runat="server"></asp:TextBox>
    </div>
    <asp:Panel ID="pnlControlLock" runat="server" Style="display: none">
        <img class="minwaitimg" runat="server" src="/images/loading.gif" alt="لطفا منتظر بمانید ..."
            title="لطفا منتظر بمانید ..." />
    </asp:Panel>
</div>
<asp:Panel ID="pnlFullScreenLock" runat="server" Style="display: none">
    <img class="waitimg" src="/images/loading.gif" alt="لطفا منتظر بمانید ..."
        title="لطفا منتظر بمانید ..." />
</asp:Panel>
