﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace SankaWebAppFramework.controls
{
    public partial class UCAddAccusition : GeneralControl
    {
        public delegate void dlgAddAccusition_Completed(bool Status);
        public event dlgAddAccusition_Completed AddAccusition_Completed;

        public delegate void dlgAddAccusition_Cancel();
        public event dlgAddAccusition_Cancel Cancel;

        public delegate void dlgAddAccusition_Error(List<string> lstMessage);
        public event dlgAddAccusition_Error Error_Occured;

        /// <summary>
        /// شناسه ثبت گزارش تخلف
        /// </summary>
        public long AccusationID
        {
            get
            {
                if (ViewState["AccusationID"] != null)
                {
                    return ViewState["AccusationID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("AccusationID", value);
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);
            BindActionMenu();
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک ثبت گزارش تخلف
        /// </summary>
        /// <param name="AccusationID"></param>
        public void LoadData()
        {
            Board.Common.BindSubject(ddlSubject);
            CheckMadde17Protest();

            Board.Common.BindEventPlace(ddlEventPlace);
            Board.Common.BindReportSource(ddlReportSource);
            Board.Common.BindAccusitionStatus(ddlStatus);

            txtAccusitionDate.DontValidate = false;

            DTO.Board.tblAccusation.tblAccusation obj = new DTO.Board.tblAccusation.tblAccusation();
            obj.RequestDataID = RequestDataID;
            obj = (new DAL.Board.tblAccusation.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                ddlStatus.SelectedValue = obj.Status.Value.ToString();
                ddlSubject.SelectedValue = obj.AccusationSubjectID.ToString();
                txtAccusitionDate.DateMiladi = obj.AccusationDate.Value;
                txtDescription.Text = obj.Description;
                txtReporterName.Text = obj.ReporterName;
                ddlReportSource.SelectedValue = obj.ReportSource.ToString();
                ddlEventPlace.SelectedValue = obj.EventPlace.ToString();
                this.AccusationID = obj.AccusationID.Value;
                pnlAttachment.Show();
            }
            BindActionMenu();
        }

        /// <summary>
        /// آیا وضعیت فایل در وضعیت بهد از صدور رای است ؟
        /// </summary>
        /// <returns></returns>
        private bool AfterVote()
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            int index = (new DAL.Board.tblViolationFile.Methods()).GetFileStatus(obj);
            if (index >= (int)Board.Enums.FileStatus.SODOURE_RAY)
            {
                divAccusitionField.Enabled = false;
                divStatus.Enabled = false;
                return true;
            }
            return false;
        }

        /// <summary>
        /// چک کردن اعتراض به حکم ماده 17
        /// </summary>
        private void CheckMadde17Protest()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.FileReferType == 2) // ماده 17
            {
                ddlSubject.SelectedValue = "29";
                ddlSubject.Enabled = false;
            }
        }

        /// <summary>
        /// آیا اتهام ابلاغ شده ست ؟
        /// </summary>
        /// <param name="accutitionID">شناسه اتهام</param>
        /// <returns></returns>
        private bool HasNotified(long accutitionID)
        {
            var obj = new DTO.Board.tblAccusationNotificationMap.tblAccusationNotificationMap
            {
                AccusationID = accutitionID
            };
            return (new DAL.Board.tblAccusationNotificationMap.Methods()).IsNotifiedAccusition(obj);

        }

        /// <summary>
        /// ثبت ثبت گزارش تخلف
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateData()) return;
            bool Result = false;

            // ثبت گزارش تخلف جدید درج میشود
            DTO.Board.tblAccusation.tblAccusation obj = new DTO.Board.tblAccusation.tblAccusation();
            obj.ViolationFileID = this.FileID;
            obj.AccusationSubjectID = ddlSubject.SelectedValue.StringToLong();
            obj.AccusationDate = txtAccusitionDate.DateMiladi;
            obj.Description = txtDescription.Text;
            obj.Status = ddlStatus.SelectedValue.StringToByte();
            obj.ReporterName = txtReporterName.Text;
            obj.RegisterDate = DateTime.Now;
            obj.RequestDataID = this.RequestDataID;
            obj.RequestID = this.RequestID;

            obj.ReportSource = ddlReportSource.SelectedValue.StringToByte();
            obj.EventPlace = ddlEventPlace.SelectedValue.StringToByte();

            obj.UserID = this.UserID;

            #region ویرایش

            if (this.RequestDataID > 0)
            {
                bool bln = (new DAL.Board.tblAccusation.Methods()).Update(obj);
                if (bln)
                {
                    // لاگ ویرایش درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);
                    Result = true;
                }
            }

            #endregion ویرایش

            #region ثبت

            else
            {
                long[] ReturnIDs = (new DAL.Board.tblAccusation.Methods()).Insert(obj);

                if (ReturnIDs[0] > 0)
                {
                    this.AccusationID = ReturnIDs[0];
                    this.RequestDataID = ReturnIDs[1];
                    if (this.AccusationID > 0 && this.RequestDataID > 0)
                    {
                        // لاگ ویرایش درخواست
                        (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                        Result = true;
                    }
                }
            }

            #endregion ثبت

            if (Result)
            {
                LoadData();
                this.AddAccusition_Completed(Result);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت گزارش تخلف خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            List<string> lstMessage = new List<string>();
            if (!txtAccusitionDate.DateIsValid)
            {
                lstMessage.Add("تاریخ وارد شده نامعتبر است .");
            }
            if (txtDescription.IsEmpty())
            {
                lstMessage.Add("درج توضیحات الزامی است .");
            }
            if (txtReporterName.IsEmpty())
            {
                lstMessage.Add("نام گزارش دهنده را درج نمایید .");
            }
            if (lstMessage.Any())
            {
                this.Error_Occured(lstMessage);
            }
            return lstMessage.Count == 0;
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private bool LockAllControls()
        {
            divAccusitionField.Enabled = false;
            FileUpload1.LockAllControls();

            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            int index = (new DAL.Board.tblViolationFile.Methods()).GetFileStatus(obj);
            if (index >= (int)Board.Enums.FileStatus.SODOURE_RAY)
            {
                divAccusitionField.Enabled = false;
                divStatus.Enabled = false;
                return true;
            }
            return false;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            divAccusitionField.Enabled = true;
            FileUpload1.UnLockAllControls();
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (!HasNotified(this.AccusationID) && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره گزارش تخلف",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#divAccusitionField');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                if (!LockAllControls() && HasUpdatePermission())
                {
                    lstAction.Add(new ActionMenuModel()
                    {
                        TargetID = "btnSave",
                        Title = "تغییر وضعیت اتهام",
                        Type = ActionMenuType.ImageButton,
                        button_click = btnSave_Click,
                        OnClientClick = "return ValidateRequiredField('#divAccusitionField');",
                        Icon = "/images/icon/save.png"
                    });
                }
                ddlStatus.BackColor = Color.FromArgb(252,208,105);
            }

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// رویداد انصراف از ثبت ثبت گزارش تخلف جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            FileUpload1.DeleteListFromSession();
            this.Cancel();
        }

        /// <summary>
        /// ریست کردن فرم
        /// </summary>
        public void ResetAllControls(string DefaultDate)
        {
            txtAccusitionDate.DateShamsi = DefaultDate;
            txtDescription.Text = "";
            txtReporterName.Text = "";
            ddlSubject.SelectedIndex = 0;
            AccusationID = 0;
            FileUpload1.OwnerRecordID = 0;
            FileUpload1.ResetUploadFile(DefaultDate);
        }
    }
}