﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAllocationMembers.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCAllocationMembers" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<cc1:SankaDialog ID="myMessage" runat="server">
</cc1:SankaDialog>
<div class="panel-group" id="accordion">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                اعضای صدور رای</h4>
        </div>
        <div class="panel-body prevententer">
            <asp:Panel ID="pnlOld" runat="server">
                <div id="inputDiv">
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            عضو اول :</label>
                        <label class="form-field-label">
                            نام :</label>
                        <asp:TextBox ID="txtFName1" MaxLength="50" runat="server" data-required="true" />
                        <label class="form-field-label _50rmargin">
                            نام خانوادگی :</label>
                        <asp:TextBox ID="txtLName1" MaxLength="50" runat="server" data-required="true" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label">
                            عضو دوم :</label>
                        <label class="form-field-label">
                            نام :</label>
                        <asp:TextBox ID="txtFName2" MaxLength="50" runat="server" data-required="true" />
                        <label class="form-field-label _50rmargin">
                            نام خانوادگی :</label>
                        <asp:TextBox ID="txtLName2" MaxLength="50" runat="server" data-required="true" />
                    </div>
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            عضو سوم :</label>
                        <label class="form-field-label">
                            نام :</label>
                        <asp:TextBox ID="txtFName3" MaxLength="50" runat="server" data-required="true" />
                        <label class="form-field-label _50rmargin">
                            نام خانوادگی :</label>
                        <asp:TextBox ID="txtLName3" MaxLength="50" runat="server" data-required="true" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlNew" runat="server">
                <div id="inputDiv">
                    <asp:CheckBoxList RepeatColumns="6" RepeatDirection="Horizontal" ID="chkListMembers"
                        runat="server">
                    </asp:CheckBoxList>
                </div>
            </asp:Panel>
        </div>
    </div>
</div>
