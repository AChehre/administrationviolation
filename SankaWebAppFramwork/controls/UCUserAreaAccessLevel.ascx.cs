﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCUserAreaAccessLevel : System.Web.UI.UserControl
    {
        /// <summary>
        /// آیدی استان
        /// </summary>
        public int OstanCode
        {
            get
            {
                return hfOstanCode.Value.StringToInt();
            }
            set
            {
                hfOstanCode.Value = value.ToString();
            }
        }

        /// <summary>
        /// کدمنطقه
        /// </summary>
        public int RegionCode
        {
            get
            {
                return hfRegionCode.Value.StringToInt();
            }
            set
            {
                hfRegionCode.Value = value.ToString();
            }
        }

        /// <summary>
        /// آیدی مدرسه
        /// </summary>
        public int SchoolCode
        {
            get
            {
                return hfSchoolCode.Value.StringToInt();
            }
            set
            {
                hfSchoolCode.Value = value.ToString();
            }
        }

        /// <summary>
        /// آیدی کاربر
        /// </summary>
        public long UserID
        {
            get
            {
                return hfUserID.Value.StringToLong();
            }
            set
            {
                hfUserID.Value = value.ToString();
            }
        }

        /// <summary>
        /// لود صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Enums.UserType UserType = GetUserType();

                if (UserType == Enums.UserType.School)
                {
                    ltMessage.Text = "<span style='color:red'>" + "آیتمی جهت انتخاب یافت نشد." + "</span>";
                }

                else if (UserType == Enums.UserType.Region)
                {
                    #region نمایش لیست مدارس منطقه

                    List<DTO.Config.tblSchool.tblSchool> RList = new List<DTO.Config.tblSchool.tblSchool>();
                    DTO.Config.tblSchool.tblSchool Obj = new DTO.Config.tblSchool.tblSchool();
                    Obj.RegionCode = RegionCode;
                    RList = (new DAL.Config.tblSchool.Methods()).SelectAll(Obj);
                    rptSchools.DataSource = RList;
                    rptSchools.DataBind();

                    ShowValue(rptSchools);
                    #endregion نمایش لیست مدارس منطقه
                }
                else if (UserType == Enums.UserType.State)
                {
                    #region نمایش لیست مناطق استان

                    List<DTO.Config.tblRegions.tblRegions> RList = new List<DTO.Config.tblRegions.tblRegions>();
                    DTO.Config.tblRegions.tblRegions Obj = new DTO.Config.tblRegions.tblRegions();
                    Obj.OstanCode = OstanCode;
                    RList = (new DAL.Config.tblRegions.Methods()).SelectAll(Obj);
                    rptRegions.DataSource = RList;
                    rptRegions.DataBind();

                    ShowValue(rptRegions);
                    #endregion نمایش لیست مناطق استان
                }
                else if (UserType == Enums.UserType.Setad)
                {
                    #region نمایش لیست استان ها

                    List<DTO.Config.tblOstans.tblOstans> RList = new List<DTO.Config.tblOstans.tblOstans>();
                    RList = (new DAL.Config.tblOstans.Methods()).SelectAll();
                    rptStates.DataSource = RList;
                    rptStates.DataBind();

                    ShowValue(rptStates);
                    #endregion نمایش لیست استان ها
                }
            }
        }

        /// <summary>
        /// بدست آوردن نوع کاربری
        /// </summary>
        /// <returns></returns>
        protected Enums.UserType GetUserType()
        {
            if (SchoolCode > 0)
            {// کاربر مدرسه است
                return Enums.UserType.School;
            }
            else if (RegionCode > 0)
            {// کاربر منطقه است
                return Enums.UserType.Region;
            }
            else if (OstanCode > 0)
            {// کاربر استان است
                return Enums.UserType.State;
            }

            // کاربر ستاد است
            return Enums.UserType.Setad;
        }

        /// <summary>
        /// ذخیره اطلاعات
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region گرفتن اطلاعات

                DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel Obj = new DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel();

                Obj.AllowedArea = GetSelectedItems();

                Obj.UserID = UserID;

                Obj.UserType = (byte)GetUserType();

                #endregion گرفتن اطلاعات

                string Message = "";

                // اعتبار سنجی داده ها
                if (!Validation(Obj, out Message))
                {// اگر داده ها معتبر نبود،پیغام دهد
                    ltMessage.Text = "<span style='color:red'>" + Message + "</span>";
                    return;
                }

                // ذخیره اطلاعات
                bool Status = (new DAL.Config.tblUserAreaAccessLevel.Methods()).Save(Obj);

                #region نمایش پیام مناسب

                if (Status)
                {
                    ltMessage.Text = "<span style='color:green'>" + "اطلاعات با موفقیت به ثبت رسید" + "</span>";
                }
                else
                {
                    ltMessage.Text = "<span style='color:red'>" + "خطا در ثبت اطلاعات" + "</span>";
                }

                #endregion نمایش پیام مناسب
            }
            catch
            {
                ltMessage.Text = "<span style='color:red'>" + "خطا در ثبت اطلاعات" + "</span>";
            }
        }

        /// <summary>
        /// بدست آوردن آیدی آیتم های انتخاب شده
        /// </summary>
        /// <returns></returns>
        protected string GetSelectedItems()
        {
            string Result = "";

            #region تشخیص نوع کاربر

            Enums.UserType UserType = GetUserType();

            Repeater rpt = null;

            if (UserType == Enums.UserType.Region)
            {
                rpt = rptSchools;
            }

            else if (UserType == Enums.UserType.State)
            {
                rpt = rptRegions;
            }

            else if (UserType == Enums.UserType.Setad)
            {
                rpt = rptStates;
            }

            #endregion تشخیص نوع کاربر

            #region گرفتن آیدی های انتخاب شده

            for (int i = 0; i < rpt.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)rpt.Items[i].FindControl("chkItem");
                if (chk.Checked)
                {
                    HiddenField hfID = (HiddenField)rpt.Items[i].FindControl("hfID");
                    Result += hfID.Value + ",";
                }
            }

            if (Result.Length > 1)
            {// پاک کردن آخرین کاما
                Result = Result.Substring(0, Result.Length - 1);
            }

            #endregion گرفتن آیدی های انتخاب شده

            return Result;
        }

        /// <summary>
        /// بدست آوردن مناطق قابل دسترس کاربر
        /// </summary>
        /// <param name="rpt">ریپیتری که باید اطلاعات را در آن نمایش دهیم</param>
        protected void ShowValue(Repeater rpt)
        {
            #region گرفتن مناطق قابل دسترس کاربر

            DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel Obj = new DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel();

            Obj.UserID = UserID;

            Obj = (new DAL.Config.tblUserAreaAccessLevel.Methods()).SelectByUserID(Obj);

            #endregion گرفتن مناطق قابل دسترس کاربر

            if (Obj.RowNumber < 1)
            {
                return;
            }

            string[] SelecetedArray = Obj.AllowedArea.Split(',');

            #region نمایش مناطق کاربر روی ریپیتر

            for (int i = 0; i < rpt.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)rpt.Items[i].FindControl("chkItem");
                HiddenField hfID = (HiddenField)rpt.Items[i].FindControl("hfID");
                if (SelecetedArray.Contains(hfID.Value))
                {
                    chk.Checked = true;
                }
            }

            #endregion نمایش مناطق کاربر روی ریپیتر
        }

        /// <summary>
        /// اعتبارسنجی داده ها
        /// </summary>
        /// <param name="Obj">شی از نوع tblUserAreaAccessLevel</param>
        /// <param name="Message">پیام خطا</param>
        /// <returns>ترو یعنی خطا وجود ندارد</returns>
        protected bool Validation(DTO.Config.tblUserAreaAccessLevel.tblUserAreaAccessLevel Obj, out string Message)
        {
            bool Result = true;
            Message = "";

            if (Obj.UserID < 1)
            {
                Result = false;
                Message += "کاربری انتخاب نشده است";
            }

            return Result;
        }

    }
}