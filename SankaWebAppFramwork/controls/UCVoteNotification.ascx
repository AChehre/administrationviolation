﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCVoteNotification.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCVoteNotification" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Src="~/controls/UCSelectMultiItems.ascx" TagName="SelectMultiItems"
    TagPrefix="uc3" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="ucactionmenu" %>
<ucactionmenu:UCActionMenu ID="UCActionMenu1" runat="server" />
<%--<span id="sanka_basic_modal">
    <input type="button" id="btnShowPrintNotification" data-href='<%= string.Format("/Board/print/ViolationNotification.aspx?ID={0}&FileID={1}&BoardID={2}",this.NotificationID,this.FileID,this.BoardID) %>'
        data-sankatooltip="فرم چاپی ابلاغ اتهام" data-title="فرم چاپی ابلاغ اتهام" data-duplicatetitle="false"
        data-width="1000" data-height="760" class="basic" style="display: none" />
</span>--%>
<div class="panel-group" id="accordion">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                جزییات ابلاغیه</h4>
        </div>
        <div class="panel-body prevententer">
            <div>
                <asp:Panel ID="divNotificationField" runat="server">
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            نوع ابلاغیه :</label>
                        <asp:DropDownList ID="ddlNotificationType" runat="server" Width="150px">
                        </asp:DropDownList>
                    </div>
                    <div class="_5p odd-row" style="display: none">
                        <label class="form-field-label">
                            شماره ابلاغیه :</label>
                        <asp:TextBox ID="txtLeterSerial" MaxLength="50" runat="server" data-required="true"
                            Width="200px" ReadOnly="True" />
                    </div>
                    <div class="_5p odd-row" style="display: none">
                        <label class="form-field-label">
                            تاریخ ابلاغیه :</label>
                        <uc1:UCDatePicker ID="txtNotificationDate" DontValidate="false" runat="server" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label">
                            تاریخ ابلاغ به محکوم :</label>
                        <uc1:UCDatePicker ID="txtDeliveryDate" DontValidate="false" runat="server" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label">
                            نام مامور ابلاغ :</label>
                        <asp:TextBox ID="txtGiverName" MaxLength="50" runat="server" Width="200px" 
                            data-required="true" >--------------</asp:TextBox>
                    </div>
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            نام گیرنده :</label>
                        <asp:TextBox ID="txtRecipientName" data-required="true" MaxLength="50" runat="server"
                            Width="200px" >--------------</asp:TextBox>
                    </div>
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            نسبت با محکوم :</label>
                        <asp:TextBox ID="txtRecipientRelationship" MaxLength="50" runat="server" Width="200px" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label">
                            محل ابلاغ :</label>
                        <asp:DropDownList ID="ddlNotificationLocation" runat="server" Width="150px">
                        </asp:DropDownList>
                    </div>
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            توضیحات :</label>
                        <asp:TextBox TextMode="MultiLine" ID="txtDescription" MaxLength="255" runat="server"
                            Height="126px" Width="433px" />
                    </div>
                </asp:Panel>
                <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
                    <uc2:SankaFileUpload ID="FileUpload1" runat="server" />
                </asp:Panel>
                <div class="_5p odd-row">
                    <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
                        ClientIDMode="Static">
                    </cc1:SankaDialog>
                </div>
            </div>
        </div>
    </div>
</div>
