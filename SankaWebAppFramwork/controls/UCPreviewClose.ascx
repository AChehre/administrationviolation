﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPreviewClose.ascx.cs" Inherits="SankaWebAppFramework.controls.UCPreviewClose" %>
<%@ Register TagPrefix="sanka" TagName="Editor" Src="~/sankaeditor/UCSankaEditor.ascx" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<script src="/js/UCPreviewVote.js" type="text/javascript"></script>
<cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
    ClientIDMode="Static">
</cc1:SankaDialog>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<span id="sanka_basic_modal">
    <input type="button" id="btnShowPrintWorkflowReport" data-href='<%= string.Format("/Board/print/ClosingProceedings.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID) %>'
        style="display: none" data-sankatooltip="فرم چاپی گردش کار" data-title="فرم چاپی گردش کار"
        data-duplicatetitle="false" data-width="920" data-height="600" class="basic" />
</span>
<div>
    <sanka:Editor ID="ckDescription" runat="server" defaultlanguage="fa"></sanka:Editor>
    <asp:Literal ID="ltrDescription" runat="server" Visible="false" />
</div>
