﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAddAccusition.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCAddAccusition" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="ucDate" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="panel-group" dir="rtl">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                جزییات ثبت گزارش تخلف</h4>
        </div>
        <div class="panel-body prevententer">
            <asp:Panel ID="divAccusitionField" runat="server" ClientIDMode="Static">
                <!---------------------------------------------------------------->
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        منبع گزارش :</label>
                    <asp:DropDownList ID="ddlReportSource" runat="server" Width="300px">
                    </asp:DropDownList>
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        محل وقوع تخلف :</label>
                    <asp:DropDownList ID="ddlEventPlace" runat="server" Width="300px">
                    </asp:DropDownList>
                </div>
                <!---------------------------------------------------------------->
                <div class="_5p odd-row">
                    <label class="form-field-label" style="vertical-align: top">
                        گزارش دهنده تخلف / شاکی :
                    </label>
                    <asp:TextBox data-required="true" ID="txtReporterName" runat="server" MaxLength="50"
                        Width="200px" />
                </div>
                <div class="_5p even-row">
                    <label class="form-field-label">
                        تاریخ گزارش :</label>
                    <ucDate:UCDatePicker ID="txtAccusitionDate" DontValidate="false" runat="server" />
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label">
                        موضوع اتهام :</label>
                    <asp:DropDownList ID="ddlSubject" runat="server" Width="300px">
                    </asp:DropDownList>
                </div>
                <div class="_5p odd-row">
                    <label class="form-field-label" style="vertical-align: top">
                        توضیحات :
                    </label>
                    <asp:TextBox data-required="true" ID="txtDescription" runat="server" TextMode="MultiLine"
                        Style="width: 720px; height: 75px;" MaxLength="1000" />
                </div>
            </asp:Panel>
            <asp:Panel class="_5p odd-row" id="divStatus" runat="server">
                <label class="form-field-label">
                    وضعیت احراز اتهام :</label>
                <asp:DropDownList ID="ddlStatus" runat="server" Width="300px">
                </asp:DropDownList>
            </asp:Panel>
        </div>
        <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
            <uc2:sankafileupload id="FileUpload1" runat="server" />
        </asp:Panel>
        <div class="_5p even-row">
            <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
                ClientIDMode="Static">
            </cc1:SankaDialog>
        </div>
    </div>
</div>
