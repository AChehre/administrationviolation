﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCViolationFileClosing.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCViolationFileClosing" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<script src="/js/Message.js" type="text/javascript"></script>
<script type="text/javascript">
    function SetIgnoreFlagAndSend() {
        document.getElementById('hfIgnoreFlagID').value = "1";
        document.getElementById('btnSaveClosing').click();
    }
</script>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="_5p">
    <label class="form-field-label">
        شماره صورت جلسه :</label>
    <asp:TextBox ID="txtClosingNumber" Width="160px" runat="server"></asp:TextBox>
</div>
<div class="_5p">
    <label class="form-field-label">
        تاریخ صدور :</label>
    <uc1:UCDatePicker ID="txtClosingDate" DontValidate="false" runat="server" />
</div>
<div class="_5p">
    <label class="form-field-label">
        به اتفاق / اکثریت :</label>
    <asp:DropDownList ID="ddlSoudorType" runat="server" Width="150px">
    </asp:DropDownList>
</div>
<div class="_5p even-row">
    <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc1:SankaDialog>
</div>
<asp:HiddenField ID="hfIgnoreFlagID" runat="server" Value="0" ClientIDMode="Static" />