﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace SankaWebAppFramework.controls
{
    public partial class UCViolationFileClosing : GeneralControl
    {
        public delegate void dlgClosing_Completed(bool Status,bool isFinalSave);
        public event dlgClosing_Completed Closing_Completed;

        public delegate void dlgClosing_Cancel();
        public event dlgClosing_Cancel Cancel;

        public delegate void dlgError(List<string> lstMessage);
        public event dlgError Error_Occured;

        /// <summary>
        /// شناسه صورت جلسه صادره
        /// </summary>
        public long ClosingID
        {
            get
            {
                if (ViewState["ClosingID"] != null)
                {
                    return ViewState["ClosingID"].ObjectToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("ClosingID", value);
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtClosingDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                Board.Common.BindSodourType(ddlSoudorType);
            }
            BindActionMenu();
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            if (obj.AttachmentID > 0)
            {
                this.RequestDataID = obj.RequestDataID.Value;
                bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
        }

        /// <summary>
        /// بارگذاری اطلاعات
        /// </summary>
        public void LoadData()
        {
            #region بدست آوردن اطلاعات صورت جلسه ثبت شده

            DTO.Board.tblViolationFileClosing.tblViolationFileClosing Obj = new DTO.Board.tblViolationFileClosing.tblViolationFileClosing();

            Obj.ViolationFileID = FileID;
            Obj = (new DAL.Board.tblViolationFileClosing.Methods()).SelectByViolationFileID(Obj);

            if (Obj.RowNumber > 0)
            {
                this.ClosingID = Obj.ClosingID;
                
                //txtClosingNumber.Enabled = false;
                txtClosingDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(Obj.ClosingDate);
                txtClosingNumber.Enabled = false;
                txtClosingNumber.Text = Obj.ClosingNumber.ToString();
                ddlSoudorType.SelectedValue = Obj.SodourType.ToString();
            }

            #endregion بدست آوردن اطلاعات صورت جلسه ثبت شده

            BindActionMenu();

            #region بدست آوردن وضعیت هیات جاری

            DTO.Admin.tblBoard.tblBoard ObjBoard = new DTO.Admin.tblBoard.tblBoard();

            ObjBoard.BoardID = BoardID;

            ObjBoard = (new DAL.Admin.tblBoard.Methods()).SelectByID(ObjBoard);

            if (ObjBoard.RowNumber > 0)
            {
                if (ObjBoard.BoardType.Value == (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision)
                {
                   
                }
            }
            #endregion بدست آوردن وضعیت هیات جاری
        }

        /// <summary>
        /// ذخیره صورت جلسه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Validate()) return;

            if (hfIgnoreFlagID.Value.StringToByte() == 0)
            {
                var obj = new DTO.Board.tblFileAttachment.tblFileAttachment { ViolationFileID = this.FileID };
                DateTime? dtMaxAttachmentDate = (new DAL.Board.tblFileAttachment.Methods()).GetMaxDocumentDate(obj);
                if (dtMaxAttachmentDate.HasValue)
                {
                    if ((dtMaxAttachmentDate.Value - txtClosingDate.DateMiladi).Days > 0)
                    {
                        string strWarnning = "تاریخ برخی ضمائم بعد از تاریخ صدور صورت جلسه است . آیا با این وجود صورت جلسه صادر گردد ؟";
                        SankaDialog1.ShowMessageYesNo(strWarnning, DialogMessage.SankaDialog.Message_Type.Warning, Page, true, "SetIgnoreFlagAndSend");
                        return;
                    }
                }
            }
            bool Result = false;

            DTO.Board.tblViolationFileClosing.tblViolationFileClosing Obj = new DTO.Board.tblViolationFileClosing.tblViolationFileClosing();
            Obj.ViolationFileID = FileID;
            Obj.BoardID = this.BoardID;
            Obj.ActivityYear = this.ActivityYear;
            Obj.AutoClosingNumber = (txtClosingNumber.Text.StringToLong() == 0);
            Obj.ClosingNumber = txtClosingNumber.Text.StringToLong();
            Obj.ClosingDate = txtClosingDate.DateMiladi;
            Obj.SodourType = ddlSoudorType.SelectedValue.StringToByte();
            Obj.isFinal = true; 

            if (this.ClosingID > 0)
            {
                Obj.ClosingID = this.ClosingID;
                Result = (new DAL.Board.tblViolationFileClosing.Methods()).Update(Obj);
            }
            else
            {
                Result = (new DAL.Board.tblViolationFileClosing.Methods()).Insert(Obj);
            }

            if (Result)
            {
                this.RequestDataID = Board.Common.InsertRequestDataRecord(this.UserID,this.RequestID, this.FileID, "صورت جلسه صادر گردید .", true, this.RequestDataID);

                if (this.ClosingID > 0)
                {
                    // لاگ ویرایش درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);
                }
                else
                {
                    // لاگ ثبت درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                }
            }
            Closing_Completed(Result,true);
        }

        /// <summary>
        /// درج تاریخ بسته شدن پرونده
        /// </summary>
        /// <returns></returns>
        private bool SetFileCloseDate()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                FileCloseDate = DateTime.Now
            };
            return (new DAL.Board.tblViolationFile.Methods()).SetFileCloseDate(obj);
        }

        /// <summary>
        /// ذخیره   موقت صورت جلسه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTempSave_Click(object sender, EventArgs e)
        {
            if (!Validate()) return;
            bool Result = false;

            DTO.Board.tblViolationFileClosing.tblViolationFileClosing Obj = new DTO.Board.tblViolationFileClosing.tblViolationFileClosing();
            Obj.ViolationFileID = FileID;
            Obj.BoardID = this.BoardID;
            Obj.ActivityYear = this.ActivityYear;
            Obj.AutoClosingNumber = (txtClosingNumber.Text.StringToLong() == 0);
            Obj.ClosingNumber = txtClosingNumber.Text.StringToLong();
            Obj.ClosingDate = txtClosingDate.DateMiladi;
            Obj.SodourType = ddlSoudorType.SelectedValue.StringToByte();
            Obj.isFinal = false;

            if (this.ClosingID > 0)
            {
                Obj.ClosingID = this.ClosingID;
                Result = (new DAL.Board.tblViolationFileClosing.Methods()).Update(Obj);
            }
            else
            {
                Result = (new DAL.Board.tblViolationFileClosing.Methods()).Insert(Obj);
            }

            if (Result)
            {
                this.RequestDataID = Board.Common.InsertRequestDataRecord(this.UserID, this.RequestID, this.FileID, "صورت جلسه صادر گردید .", false, this.RequestDataID);
                if (RequestDataID > 0)
                {
                    if (SetFileCloseDate())
                    {
                        LoadData();
                    }
                }
            }
            Closing_Completed(Result,false);
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// <param name="AfterSaveClosing">ترو باشد ، یعنی بعد از صدور صورت جلسه منو ساخته شود</param>
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnTempSaveClosing",
                    clientIDMode = System.Web.UI.ClientIDMode.Static,
                    Title = "ثبت موقت",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnTempSave_Click,
                    Icon = "/images/icon/save.png"
                });

                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSaveClosing",
                    clientIDMode = System.Web.UI.ClientIDMode.Static,
                    Title = "ثبت نهایی",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ShowConfirm(this,'توجه:  پس از صدور صورت جلسه امکان اصلاح یا ویرایش صورت جلسه و جزئیات پرونده وجود نخواهد داشت. آیا از صورت جلسه صادره اطمینان دارید؟!');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }
            if (this.ClosingID > 0)
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnPrint",
                    Title = "پیش نمایش / چاپ",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnPrint_Click,
                    Icon = "/images/icon/printer.png"
                });
            }
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });
            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// غیر فعال کردن کنترل
        /// </summary>
        void LockAllControls()
        {
            txtClosingDate.Enabled = false;
        }

        /// <summary>
        /// فعال کردن کنترل
        /// </summary>
        void UnLockAllControls()
        {
            txtClosingDate.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel();
        }

        /// <summary>
        /// چاپ فرم صورت جلسه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            string PrintPageUrl = "#";
            if (UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Initial)
            {
                PrintPageUrl = string.Format("~/Board//print/BadviFileClosing.aspx?FileID={0}&BoardID={1}&ClosingID={2}&SType={3}", this.FileID, this.BoardID, this.ClosingID,ddlSoudorType.SelectedValue);
            }
            if (UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Revision)
            {
                PrintPageUrl = string.Format("~/Board/print/RevisionFileClosing.aspx?FileID={0}&BoardID={1}&ClosingID={2}&SType={3}", this.FileID, this.BoardID, this.ClosingID, ddlSoudorType.SelectedValue);
            }
            Response.Redirect(PrintPageUrl);
        }

        /// <summary>
        /// اعتبار سنجی پیش از ارسال
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            List<string> lstMessage = new List<string>();

            if (HasNotCompletedRequest())
            {
                lstMessage.Add("قبل از صدور صورت جلسه مختومه باید تمامی اقدامات صورت گرفته برروی پرونده تکمیل گردد .");
            }
            else
            {
                if (txtClosingNumber.Text.StringToLong() > 0 && ClosingID == 0)
                {
                    long MaxNotificationCounter = getLastClosingCounter();
                    if (MaxNotificationCounter > txtClosingNumber.Text.StringToLong())
                    {
                        string strMessage = string.Format(" شمارنده  فعلی  برای صورت جلسه مختومه برابر  {0}  میباشد . شماره صورت جلسه باید برابر یا بزرگتر از این مقدار تعیین گردد .", MaxNotificationCounter);
                        lstMessage.Add(strMessage);
                    }
                }

                if (!txtClosingDate.DateIsValid)
                {
                    lstMessage.Add("تاریخ صدور صورت جلسه را وارد نمایید .");
                }
            }
            if (lstMessage.Any())
            {
                this.Error_Occured(lstMessage);
            }
            return (lstMessage.Count == 0);
        }

        private bool HasNotCompletedRequest()
        {
            var obj = new DTO.Board.tblRequestData.tblRequestData
            {
                FileID = this.FileID,
                RequestDataID = this.RequestDataID
            };
            return (new DAL.Board.tblRequestData.Methods()).HasAnyNotCompletedData(obj);
        }

        /// <summary>
        /// گرفتن آخرین شماره ابلاغیه برای این هیات و در سال عملیاتی انتخاب شده
        /// </summary>
        /// <returns></returns>
        private long getLastClosingCounter()
        {
            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting { BoardID = this.BoardID, Year = this.ActivityYear };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            return obj.ClosingCounter;
        }

        /// <summary>
        /// چک کردن اینکه تاریخ ابلاغ صورت جلسه حتما بعد از تاریخ صدور صورت جلسه باشد
        /// </summary>
        /// <returns></returns>
        private bool HasNotificationDateAfterClosingDate()
        {
            var objNotification = new DTO.Board.tblViolationNotification.tblViolationNotification { ViolationFileID = this.FileID };
            var lstNotification = (new DAL.Board.tblViolationNotification.Methods()).SelectByViolationFileID(objNotification);
            var lst = lstNotification.Where(x => x.NotificationDate.Value > txtClosingDate.DateMiladi).ToList();
            return lst.Any();
        }
    }
}