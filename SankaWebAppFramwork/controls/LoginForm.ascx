﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginForm.ascx.cs" Inherits="SankaWebAppFramework.controls.LoginForm" %>
<style type="text/css">


button,
select {
    text-transform: none;
}

select {
    width: 158px;
}

    </style>
<script src="/js/Capcha.js" type="text/javascript"></script>
<center>
        <div class="RoundedGrayBox">
            <table style="width: 500px">
                <tr>
                    <td>
                        نام کاربری :
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        رمز ورود :
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="100" TextMode="Password" 
                            Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="display:none">
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:CheckBox ID="chkRememberMe" runat="server" Text="مرا به خاطر بسپار" 
                            TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                <td>
                    سال عملیاتی :</td>
                <td>
                <asp:DropDownList ID="ddlYear" runat="server">
                </asp:DropDownList>
                </td>
            </tr>
                <tr>
                <td>
                    کد امنیتی :
                </td>
                <td>
                   <asp:Image ID="imgCaptcha" runat="server" ClientIDMode="Static" />
                   <asp:Image ID="imgRefersh" ToolTip="تصویر جدید" runat="server" ImageUrl="~/images/icon/refresh.png"
                    Style="cursor: pointer"  onclick="RefreshCapcha();" />
                </td>
            </tr>
            <tr>
                <td>
                    عبارت داخل تصویر :
                </td>
                <td>
                    <asp:TextBox ID="txtCaptcha" runat="server" MaxLength="4" 
                        EnableViewState="False" ViewStateMode="Disabled" Font-Names="Arial" 
                        Width="100px"></asp:TextBox>
                </td>
            </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnLogin" runat="server" Text="ورود" OnClientClick="RemoveCookie('returnurl')" OnClick="btnLogin_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </center>
