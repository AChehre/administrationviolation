﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCOtherRequest.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCOtherRequest" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="ucDate" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="panel-group" id="accordion">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title" runat="server" id="pnlTitle">
            </h4>
        </div>
        <div class="panel-body prevententer">
            <div id="divAccusitionField">
                <div class="_5p odd-row">
                    <label class="form-field-label" style="vertical-align: top">
                        توضیحات :
                    </label>
                    <asp:TextBox data-required="true" ID="txtDescription" runat="server" TextMode="MultiLine"
                        Style="width: 720px; height: 75px;" MaxLength="1000" />
                </div>
            </div>
            <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
                <uc2:SankaFileUpload ID="FileUpload1" runat="server" />
            </asp:Panel>
            <div class="_5p even-row">
                <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
                    ClientIDMode="Static">
                </cc1:SankaDialog>
            </div>
        </div>
    </div>
</div>
