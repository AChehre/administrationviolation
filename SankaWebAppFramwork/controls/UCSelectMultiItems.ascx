﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCSelectMultiItems.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCSelectMultiItems" %>
<asp:Panel runat="server" ID="pnlSelectMultipleAccusition">
    <div class="right">
        <div class="title">
            جهت انتخاب، روی گزینه ی مورد نظر کلیک نمایید.
        </div>
        <div id="RightPanel">
            <asp:Repeater ID="rptRightPanel" runat="server">
                <ItemTemplate>
                    <a data-islock="<%# Lock %>" data-status='<%# Eval("Status") %>' class="btn btn-info _5m"
                        data-id='<%# Eval("ID") %>' data-sankatooltip='<%# Eval("Description") %>' title='<%# Eval("Description") %>'
                        onclick="SelecteItemClick(this)">
                        <img src="/images/icon/Info-icon-16.png" />&nbsp;<%# Eval("Title") %>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="left">
        <div class="title">
            جهت حذف، روی گزینه ی مورد نظر کلیک نمایید.
        </div>
        <div id="LeftPanel">
            <asp:Repeater ID="rptLeftPanel" runat="server">
                <ItemTemplate>
                    <a data-islock="<%# Lock %>" data-status='<%# Eval("Status") %>' class="btn btn-info _5m"
                        data-id='<%# Eval("ID") %>' data-sankatooltip='<%# Eval("Description") %>' title='<%# Eval("Description") %>'
                        onclick="SelecteItemClick(this)">
                        <img src="/images/icon/Info-icon-16.png" />&nbsp;<%# Eval("Title") %>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Panel>
<asp:HiddenField ID="hfLeftControlsID" runat="server" ClientIDMode="Static" />
