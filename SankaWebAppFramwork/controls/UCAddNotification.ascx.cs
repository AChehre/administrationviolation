﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCAddNotification : GeneralControl
    {
        #region تعریف رویدادها

        public delegate void dlgAddNotification_Completed(bool Status);
        public event dlgAddNotification_Completed AddNotification_Completed;

        public delegate void dlgCancel_Click();
        public event dlgCancel_Click Cancel_Click;

        public delegate void dlgError(List<string> lstMessage , bool closeView);
        public event dlgError Error_Occured;

        #endregion تعریف رویدادها

        #region پروپرتی ها

        /// <summary>
        /// شناسه ابلاغیه
        /// </summary>
        protected long NotificationID
        {
            get
            {
                if (ViewState["NotificationID"] != null)
                {
                    return ViewState["NotificationID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("NotificationID", value);
            }
        }

        /// <summary>
        /// آیا دکمه های عملیاتی ، مثلا ارسال فایل و حذف از گرید نمایش داده شود یا نه
        /// </summary>
        public bool HideActionButtons
        {
            get
            {
                if (ViewState["HideActionButtons"] != null)
                {
                    return ViewState["HideActionButtons"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("HideActionButtons", value);
            }
        }

        /// <summary>
        /// نوع فایلهای قابل ضمیمه کردن برای این درخواست
        /// </summary>
        public string RelatedAttachementTypes
        {
            get
            {
                if (ViewState["RelatedAttachementTypes"] != null)
                {
                    return ViewState["RelatedAttachementTypes"].ToString();
                }
                return null;
            }
            set
            {
                ViewState.Add("RelatedAttachementTypes", value);
            }
        }

        /// <summary>
        /// سال عملیاتی
        /// </summary>
        public int? CurrentApplicationYear
        {
            get
            {
                if (ViewState["CurrentApplicationYear"] != null)
                {
                    return ViewState["CurrentApplicationYear"].ToString().StringToInt();
                }
                return null;
            }
            set
            {
                ViewState["CurrentApplicationYear"] = value;
            }
        }

        #endregion پروپرتی ها

        protected void Page_Load(object sender, EventArgs e)
        {
            // تنظیمات فایل آپلود 
            FileUpload1.Upload_Completed += new controls.SankaFileUpload.dlgUpload_Completed(FileUpload1_Upload_Completed);
            FileUpload1.Uploaded_Deleted += new SankaFileUpload.dlgUploaded_Deleted(FileUpload1_Uploaded_Deleted);

            if (!IsPostBack)
            {
                if (NotificationID < 1) //حالت ثبت ابلاغیه
                {
                    // فقط در حالت ثبت جدید، روی لود فرم کمبوها رو مقداردهی میکنیم،در حالت ویرایش در تابع لود دیتا این کار را میکنیم
                    // چون لود صفحه این کنترل بعد تابع لود دیتا در حالت ویرایش فراخوانی میشود
                    SankaWebAppFramework.Board.Common.BindNotificationType(ddlNotificationType);
                    SankaWebAppFramework.Board.Common.BindNotificationLocations(ddlNotificationLocation);
                }

            }

            BindActionMenu();
        }

        /// <summary>
        /// بارگذاری جزئیات ابلاغیه
        /// </summary>
        /// <param name="NotificationID"></param>
        public void LoadData()
        {
            txtDeliveryDate.Empty = true;

            getNotificationID();
            DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView obj =
                new DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView();
            obj.NotificationID = this.NotificationID;
            obj = (new DAL.Board.tblViolationNotificationMapView.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                #region تنظیمات فایل آپلود در حالت ویرایش

                FileUpload1.AllowedFileTypes = RelatedAttachementTypes;
                FileUpload1.RequestDataID = this.RequestDataID;
                FileUpload1.FileID = this.FileID;
                FileUpload1.RequestID = this.RequestID;
                FileUpload1.Load();

                #endregion تنظیمات فایل آپلود در حالت ویرایش

                txtNotificationDate.DateMiladi = obj.NotificationDate.Value;

                if (obj.DeliveryDate.HasValue)
                {
                    txtDeliveryDate.DateMiladi = obj.DeliveryDate.Value;
                }

                txtRecipientName.Text = obj.RecipientName;
                txtGiverName.Text = obj.GiverName;
                txtRecipientRelationship.Text = obj.RecipientRelationship;
                ddlNotificationLocation.SelectedValue = obj.NotificationAddress;
                txtLeterSerial.Text = obj.LetterSerial;
                ddlNotificationType.SelectedValue = obj.NotificationType.Value.ToString();
                txtDescription.Text = obj.Description;
                txtLeterSerial.Enabled = false;
                pnlAttachment.Show();
                ValidateData(true);
            }
            BindAccusation(true);
            BindActionMenu();
        }

        /// <summary>
        /// استخراج شناسه ابلاغ اتهم بر اساس شناسه درخواست ثبت شده
        /// </summary>
        private void getNotificationID()
        {
            DTO.Board.tblViolationNotification.tblViolationNotification obj = new DTO.Board.tblViolationNotification.tblViolationNotification
            {
                RequestDataID = this.RequestDataID
            };
            this.NotificationID = (new DAL.Board.tblViolationNotification.Methods()).SelectByRequestDataID(obj);
        }

        /// <summary>
        /// بدست آوردن اتهامات
        /// <param name="SaveState">اگر ترو باشد، یعنی بعد از پست بک، کنترل های انتخاب شده از دست نروند</param>
        /// </summary>
        private void BindAccusation(bool SaveState = false)
        {
            // لیست آیتم های انتخاب نشده
            List<controls.UCSelectMultiItems.ListItem> lst = new List<controls.UCSelectMultiItems.ListItem>();
            // لیست ایتمهای انتخاب شده
            List<controls.UCSelectMultiItems.ListItem> lstselected = new List<controls.UCSelectMultiItems.ListItem>();

            if (this.NotificationID < 1)
            {
                #region بدست آوردن اتهامات تایید شده ای که ابلاغیه ندارند

                DTO.Board.tblAccusitionView.tblAccusitionView obj = new DTO.Board.tblAccusitionView.tblAccusitionView();

                obj.ViolationFileID = this.FileID;
                //obj.Status = (byte)Board.Enums.AccusationStatus.CONFIRMED;

                List<DTO.Board.tblAccusitionView.tblAccusitionView> RList = (new DAL.Board.tblAccusitionView.Methods()).SelectByFileID(obj);

                // اگر حالت ثبت بود، و اتهامی نبود، پیغام دهد و  پنجره را ببندد
                if (!RList.Any())
                {
                    List<string> lstMessage = new List<string>();
                    lstMessage.Add("اتهامی جهت ابلاغ وجود ندارد .");
                    this.Error_Occured(lstMessage,true);
                    return;
                }

                string[] Selecteds = SelectMultiItems1.GetSelectedIDsArray;

                foreach (DTO.Board.tblAccusitionView.tblAccusitionView Item in RList)
                {
                    controls.UCSelectMultiItems.ListItem NewItem = new controls.UCSelectMultiItems.ListItem
                    {
                        ID = Item.AccusationID.Value,
                        Description = "تاریخ اعلام گزارش : " + Item.AccusationDate.Value.ToPersianDate() + " - تاریخ ثبت در سیستم : " + Item.RegisterDate.Value.ToPersianDate() + "<hr/>" + Item.Description,
                        Status = false,
                        Title = Item.Title
                    };

                    //  در زمانی که میخواهیم در پست بک ها، آیتم های انتخاب شده، در حفظ شوند و از بین نروند، آنهایی که در هیدن فیلد هستند  را ، در قسمت انتخاب شده ها ذخیه میکنیم
                    if (SaveState && Selecteds.Contains(Item.AccusationID.ToString()))
                    {
                        NewItem.Status = true;
                        lstselected.Add(NewItem);
                    }
                    else
                    {
                        lst.Add(NewItem);
                    }
                }
                SelectMultiItems1.SelectedItems = lstselected;
                SelectMultiItems1.DeSelectedItems = lst;

                #endregion بدست آوردن اتهامات تایید شده ای که ابلاغیه ندارند
            }
            else
            {
                #region بدست آوردن اتهامات ابلاغیه و نمایش آنها در حالت ویرایش

                // لیست آیتم های انتخاب شده


                DTO.Board.tblAccusitionView.tblAccusitionView objAccusition = new DTO.Board.tblAccusitionView.tblAccusitionView();
                objAccusition.NotificationID = NotificationID;

                //نمایش روی فرم
                List<DTO.Board.tblAccusitionView.tblAccusitionView> lstAccusition = (new DAL.Board.tblAccusitionView.Methods()).SelectByNotificationID(objAccusition);

                foreach (DTO.Board.tblAccusitionView.tblAccusitionView Item in lstAccusition)
                {
                    lstselected.Add(new controls.UCSelectMultiItems.ListItem
                    {
                        ID = Item.AccusationID.Value,
                        Description = "تاریخ اعلام گزارش : " + Item.AccusationDate.Value.ToPersianDate() + " - تاریخ ثبت در سیستم : " + Item.RegisterDate.Value.ToPersianDate() + "<hr/>" + Item.Description,
                        Status = true,
                        Title = Item.Title
                    });
                }

                SelectMultiItems1.SelectedItems = lstselected;


                DTO.Board.tblAccusitionView.tblAccusitionView obj = new DTO.Board.tblAccusitionView.tblAccusitionView();
                obj.ViolationFileID = this.FileID;
                //obj.Status = (byte)Board.Enums.AccusationStatus.CONFIRMED;
                List<DTO.Board.tblAccusitionView.tblAccusitionView> RList = (new DAL.Board.tblAccusitionView.Methods()).SelectByFileID(obj);
                // لیست آیتم های انتخاب نشده
                foreach (DTO.Board.tblAccusitionView.tblAccusitionView Item in RList)
                {
                    if(SelectMultiItems1.SelectedItems.Where(x =>x.ID == Item.AccusationID.Value).Any())
                    {
                        continue;
                    }

                    controls.UCSelectMultiItems.ListItem NewItem = new controls.UCSelectMultiItems.ListItem
                    {
                        ID = Item.AccusationID.Value,
                        Description = "تاریخ اعلام گزارش : " + Item.AccusationDate.Value.ToPersianDate() + " - تاریخ ثبت در سیستم : " + Item.RegisterDate.Value.ToPersianDate() + "<hr/>" + Item.Description,
                        Status = false,
                        Title = Item.Title
                    };
                    lst.Add(NewItem);
                }

                #endregion بدست آوردن اتهامات ابلاغیه و نمایش آنها در حالت ویرایش
            }
            SelectMultiItems1.DeSelectedItems = lst;
            SelectMultiItems1.ShowItems();
        }

        /// <summary>
        /// ذخیره ابلاغیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool Result = false;

            if (!ValidateData()) return;

            DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView obj = new DTO.Board.tblViolationNotificationMapView.tblViolationNotificationMapView();
            obj.ViolationFileID = this.FileID;
            obj.NotificationType = ddlNotificationType.SelectedValue.StringToByte();
            obj.SelectedAccusationID = SelectMultiItems1.GetSelectedIDs;
            obj.NotificationDate = txtNotificationDate.DateMiladi;

            if(txtDeliveryDate.DateIsValid)
            {
                obj.DeliveryDate = txtDeliveryDate.DateMiladi;
            }

            obj.RecipientName = txtRecipientName.Text.CorrectArabicChars();
            obj.GiverName = txtGiverName.Text.CorrectArabicChars();
            obj.RecipientRelationship = txtRecipientRelationship.Text.CorrectArabicChars();
            obj.NotificationAddress = ddlNotificationLocation.SelectedValue;
            obj.LetterSerial = txtLeterSerial.Text; // اگر درح نگردد ، خودکار تولید میشود
            obj.Description = txtDescription.Text.CorrectArabicChars();
            obj.RequestID = this.RequestID;
            obj.BoardID = this.BoardID;
            obj.ActivityYear = this.CurrentApplicationYear;
            obj.AutoLetterSerial = (txtLeterSerial.IsEmpty());
            obj.UserID = this.UserID;
            obj.NotificationCounter = txtLeterSerial.Text.StringToLong();

            #region ویرایش

            if (this.NotificationID > 0)
            {// ویرایش
                obj.NotificationID = this.NotificationID;
                obj.RequestDataID = this.RequestDataID;
                bool bln = (new DAL.Board.tblViolationNotificationMapView.Methods()).Update(obj);
                if (bln)
                {
                    // لاگ ویرایش درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);
                    Result = true;
                }
            }

            #endregion ویرایش

            #region ثبت
            else
            {
                obj.ReplyBill = (byte)SankaWebAppFramework.Board.Enums.ReplyBill.NOT_REPLIED;

                long[] ReturnIDs = (new DAL.Board.tblViolationNotificationMapView.Methods()).Insert(obj);

                if (ReturnIDs[0] > 0)
                {
                    this.NotificationID = ReturnIDs[0];
                    this.RequestDataID = ReturnIDs[1];
                    if (this.NotificationID > 0 && this.RequestDataID > 0)
                    {
                        // لاگ ثبت درخواست
                        (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                        Result = true;
                    }
                }
            }

            #endregion ثبت

            if (Result)
            {
                if (FileUpload1.HasAnyFile())
                {
                    bool bln = Board.Common.UpdateRequestDataStatus(this.RequestDataID);
                }
                this.LoadData();

                AddNotification_Completed(Result);
                
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت اتهام خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        /// <summary>
        /// اعتبار سنجی
        /// </summary>
        /// <returns></returns>
        private bool ValidateData(bool byPassAccusationvalidate = false)
        {
            List<string> lstMessage = new List<string>();

            if (!byPassAccusationvalidate)
            {
                if (SelectMultiItems1.GetSelectedIDs.Length < 1)
                {
                    lstMessage.Add("انتخاب حداقل یک اتهام الزامی است.");
                }
                else
                {
                    if (txtLeterSerial.Text.StringToLong() > 0 && this.NotificationID == 0)
                    {
                        long MaxNotificationCounter = getLastNotificationCounter();
                        if (MaxNotificationCounter > txtLeterSerial.Text.StringToLong())
                        {
                            string strMessage = string.Format("شمارنده فعلی برای این ابلاغیه برابر  {0}  میباشد . شماره ابلاغیه فعلی باید برابر یا بزرگتر از این مقدار تعیین گردد .", MaxNotificationCounter);
                            lstMessage.Add(strMessage);
                        }
                    }
                }
            }
            if (!txtNotificationDate.DateIsValid)
            {
                lstMessage.Add("تاریخ ابلاغیه را درج نمایید .");
            }
            else
            {
                if (!CheckNotificationDate())
                {
                    lstMessage.Add("تاریخ ابلاغیه باید از تاریخ های اتهام های مندرج در آن بزرگتر باشد .");
                }
            }
            // اگر مستندی آپلود شده است ، موارد زیر اعتبار سنجی شود
            if (FileUpload1.HasAnyFile())
            {
                txtGiverName.Attributes["data-required"] = "true";
                txtRecipientName.Attributes["data-required"] = "true";
                txtDeliveryDate.DontValidate = false;

                if (txtDeliveryDate.Text.Length == 0)
                {
                    lstMessage.Add("تاریخ ابلاغ به متهم را درج نمایید .");
                }
                else
                {
                    if (txtDeliveryDate.DateShamsi.Length > 0)
                    {
                        if (!txtDeliveryDate.DateIsValid)
                        {
                            lstMessage.Add("تاریخ ابلاغ به متهم نامعتبر است .");
                        }
                        else
                        {
                            if (txtDeliveryDate.DateMiladi < txtNotificationDate.DateMiladi)
                            {
                                lstMessage.Add("تاریخ ابلاغ به متهم نباید کوچکتر از تاریخ ابلاغیه باشد .");
                            }
                        }
                    }
                }
                if (txtGiverName.IsEmpty())
                {
                    lstMessage.Add("نام مامور ابلاغ را درج نمایید .");
                }
                if (txtRecipientName.IsEmpty())
                {
                    
                    lstMessage.Add("نام گیرنده ابلاغیه را درج نمایید .");
                }
                //if (txtDescription.IsEmpty())
                //{
                //    lstMessage.Add("درج توضیحات الزامی است .");
                //}
                if (ddlNotificationLocation.SelectedIndex == 0)
                {
                    lstMessage.Add("محل ابلاغ را تعیین نمایید .");
                }
                if (ddlNotificationType.SelectedIndex == 0)
                {
                    lstMessage.Add("نوع ابلاغ را تعیین نمایید .");
                }
            }
            else
            {
                txtGiverName.Attributes["data-required"] = "false";
                txtRecipientName.Attributes["data-required"] = "false";
                txtDeliveryDate.DontValidate = true;
            }


            if (lstMessage.Any())
            {
                lstMessage.Insert(0, "جهت تکمیل عملیات :\n");
                BindAccusation(true);
                this.Error_Occured(lstMessage, false);
            }
            return (lstMessage.Count == 0);
        }

        /// <summary>
        /// گرفتن آخرین شماره ابلاغیه برای این هیات و در سال عملیاتی انتخاب شده
        /// </summary>
        /// <returns></returns>
        private long getLastNotificationCounter()
        {
            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting { BoardID = this.BoardID , Year = this.CurrentApplicationYear.Value  };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            return obj.ViolationNotificationCounter;
        }

        /// <summary>
        /// چک کردن اینکه تاریخ اتهامات پیش از تاریخ ابلاغ باشد
        /// </summary>
        /// <returns></returns>
        private bool CheckNotificationDate()
        {
            var obj = new DTO.Board.tblAccusation.tblAccusation
            {
                SelectedAccusationID = SelectMultiItems1.GetSelectedIDs
            };
            DateTime? dt = (new DAL.Board.tblAccusation.Methods()).GetMaxAccusisionDate(obj);
            if (dt.HasValue)
            {
                return (txtNotificationDate.DateMiladi.Date - dt.Value.Date).Days >= 0;
            }
            return true;
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            divNotificationField.Enabled = false;
            SelectMultiItems1.Lock = true;
            SelectMultiItems1.ShowItems();
            FileUpload1.LockAllControls();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            divNotificationField.Enabled = true;
            SelectMultiItems1.Lock = false;
            SelectMultiItems1.ShowItems();
            FileUpload1.UnLockAllControls();
        }

        /// <summary>
        /// ساخت منو و دکمه ها
        /// <param name="CreateAgain">اگر ترو باشد، یعنی فانکشن در حالت لود صفحه فراخوانی نشده و باید مجددا منو ساخته شودو گزینه های قبلی پاک شود</param>
        /// </summary>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancelView",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png",
            });

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnSave",
                    Title = "ذخیره ابلاغیه",
                    Type = ActionMenuType.ImageButton,
                    button_click = btnSave_Click,
                    OnClientClick = "return ValidateRequiredField('#divNotificationField');",
                    Icon = "/images/icon/save.png"
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }

            if (NotificationID > 0)
            {// اگر در حالت ویرایش بود، دکمه چاپ نمایش داده شود
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnPrint",
                    Title = "چاپ",
                    Type = ActionMenuType.HyperLink,
                    NavigateUrl = string.Format("~/Board/Print/ViolationNotification.aspx?ID={0}&FileID={1}&BoardID={2}", this.NotificationID, this.FileID, this.BoardID),
                    Icon = "/images/icon/printer.png"
                });
            }

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// رویداد انصراف از ثبت اتهام جدید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            // این خط رو فراخوانی میکنیم تا اطلاعات از سشن پاک شود
            FileUpload1.DeleteListFromSession();
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Cancel_Click();
        }

        /// <summary>
        /// پاک کردن مقدار کنترل های فرم
        /// </summary>
        public void ResetAllControls(string DefaultDate)
        {
            ddlNotificationLocation.SelectedIndex = 0;
            txtDeliveryDate.Empty = true;
            txtGiverName.Clear();
            txtLeterSerial.Text = "";
            txtNotificationDate.DateShamsi = DefaultDate;
            txtRecipientName.Clear();
            txtRecipientRelationship.Text = "";
            ddlNotificationType.SelectedIndex = 0;
            this.NotificationID = 0;
            BindAccusation(true);
            FileUpload1.OwnerRecordID = 0;
            FileUpload1.ResetUploadFile(DefaultDate);
        }

        /// <summary>
        /// رویداد اتمام بارگذاری فایل
        /// </summary>
        /// <param name="obj"></param>
        void FileUpload1_Upload_Completed(DTO.Board.tblFileAttachment.tblFileAttachment obj)
        {
            // با یک کردن این فلگ ، یعنی بعد از بستن مودال، صفحه پرنت مودال، دوباره بارگذاری شود تا تغییرات در آن دیده شود
            if (obj.AttachmentID > 0)
            {
                FileUpload1.Load();
                this.RequestDataID = obj.RequestDataID.Value;
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                ValidateData(true);
            }
            else
            {
                // حالت ثبت ابلاغیه جدید
                SankaDialog1.ShowMessage("فایل بارگذاری گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            //بدست آوردن لیست اتهامات مربوط به این ابلاغیه و نمایش آنها
            BindAccusation(true);
        }

        void FileUpload1_Uploaded_Deleted(long RequestDataID)
        {
            if (RequestDataID > 0)
            {
                FileUpload1.Load();
                //بدست آوردن لیست اتهامات مربوط به این ابلاغیه و نمایش آنها
                BindAccusation(true);
                SankaDialog1.ShowMessage("این ضمیمه حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                ValidateData(true);
            }
        }
    }
}