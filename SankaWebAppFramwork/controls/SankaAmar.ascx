﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SankaAmar.ascx.cs" Inherits= "SankaWebAppFramework.controls.SankaAmar" %>

<div>
    <span> بازدید امروز :
    </span>
    <asp:Label ID="lblTodayAmar" runat="server" Text=""></asp:Label>
</div>
<div>
    <span> بازدید دیروز :
    </span>
    <asp:Label ID="lblYesterdayAmar" runat="server" Text=""></asp:Label>
</div>
<div>
    <span> بازدید هفته اخیر :
    </span>
    <asp:Label ID="lblLastWeekAmar" runat="server" Text=""></asp:Label>
</div>
<div>
    <span> بازدید ماه اخیر :
    </span>
    <asp:Label ID="lblLastMonthAmar" runat="server" Text=""></asp:Label>
</div>
<div>
    <span> بازدید کل :
    </span>
    <asp:Label ID="lblAllAmar" runat="server" Text=""></asp:Label>
</div>
