﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCPreviewVote : GeneralControl
    {
        public delegate void dlSaveCompleted(bool Status);
        public event dlSaveCompleted Save_Completed;

        public delegate void dlgAddAccusition_Cancel();
        public event dlgAddAccusition_Cancel Cancel;

        /// <summary>
        /// وضعیت پرونده
        /// </summary>
        public byte ViolationFileStatus
        {
            set
            {
                ViewState.Add("ViolationFileStatus", value);
            }
            get
            {
                if (ViewState["ViolationFileStatus"] != null)
                {
                    return ViewState["ViolationFileStatus"].ToString().StringToByte();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindActionMenu();
        }

        /// <summary>
        /// ذخیره متن گردش کار
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bntSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ckDescription.Text))
            {
                SankaDialog1.ShowMessage("جهت ذخیره متن گردش کار، لطفا متن را وارد نمایید.", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }

            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();

            obj.FileID = FileID;
            obj.WorkflowReport = ckDescription.Text;

            bool bln = (new DAL.Board.tblViolationFile.Methods()).UpdateWorkflowReport(obj);

            if (bln)
            {
                if (this.RequestDataID == 0)
                {
                    this.RequestDataID = Board.Common.InsertRequestDataRecord(this.UserID, this.RequestID, this.FileID, "متن گردش کار نوشته و ثبت گردید .");
                    // لاگ ثبت درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.INSERT);
                }
                else
                {
                    // لاگ ویرایش درخواست
                    (new Logger()).InsertRequestDataModificationLog(this.RequestDataID, Logger.RecardModificationFlag.UPDATE);
                }
            }
            this.Save_Completed(bln);
        }

        /// <summary>
        /// ساخت منو
        /// </summary>
        /// <param name="ShowPrinBtn">اگر ترو باشد، دکمه چاپ نمایش داده شود</param>
        void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (Board.Common.CheckHasDependedRequestData(this.RequestDataID, this.FileID) == 0 && HasUpdatePermission())
            {
                lstAction.Add(new ActionMenuModel()
                {
                    Icon = "/images/icon/save.png",
                    TargetID = "bntSave",
                    Title = "ذخیره",
                    Type = ActionMenuType.ImageButton,
                    button_click = bntSave_Click,
                });
                UnLockAllControls();
            }
            else
            {
                LockAllControls();
            }
            //if (this.RequestDataID > 0)
            //{
            //    lstAction.Add(new ActionMenuModel()
            //    {
            //        Icon = "/images/icon/printer.png",
            //        TargetID = "btnPrint",
            //        Title = "چاپ فرم گردش کار",
            //        Type = ActionMenuType.ImageButton,
            //        OnClientClick = "return ShowPrintWorkflowReport();",
            //    });
            //}

            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnCancel",
                Title = "بازگشت",
                Type = ActionMenuType.ImageButton,
                button_click = btnCancel_Click,
                Icon = "/images/icon/back.png"
            });

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void LockAllControls()
        {
            ltrDescription.Show();
            ckDescription.Hide();
        }

        /// <summary>
        /// قفل کردن تمامی کنترل ها در حالتی که کاربر نباید بتواند چیزی را ویرایش کند
        /// </summary>
        private void UnLockAllControls()
        {
            ltrDescription.Hide();
            ckDescription.Show();
        }

        /// <summary>
        /// بارگذاری اطلاعات
        /// </summary>
        public void LoadData(string DefaultDate)
        {
            #region بدست آوردن متن گردش کار

            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();

            obj.FileID = FileID;
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                ckDescription.Text = obj.WorkflowReport;
                ltrDescription.Text = obj.WorkflowReport;
            }
            BindActionMenu();
            #endregion بدست آوردن متن گردش کار

            Literal ltr = new Literal();
            ltr.Text = @"<script src='/sankaeditor/ckeditor/ckeditor.js' type='text/javascript'></script>
            <script src='/sankaeditor/ckeditor/adapters/jquery.js' type='text/javascript'></script>
            <script src='/sankaeditor/scripts/UCSankaEditor.js'></script>";
            this.Page.Header.Controls.Add(ltr);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancel();
        }
    }
}