﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SankaFileUpload.ascx.cs"
    Inherits="SankaWebAppFramework.controls.SankaFileUpload" %>
<%@ Register Src="UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<div class="panel-group" id="accordion">
    <div class="panel panel-primary" id="pnlUpload" runat="server">
        <div data-toggle="collapse" data-target="#collapse1" class="panel-heading pointer-cursor">
            <h4 runat="server" id="hTitle" class="panel-title">
                مشخصات ضمیمه</h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse out">
            <div class="panel-body prevententer">
                <div id="divFileAttachmentField" runat="server">
                    <div class="_5p odd-row">
                        <label class="form-field-label">
                            نوع سند :</label>
                        <asp:DropDownList ID="ddlDocumrntType" runat="server" Width="300px" AutoPostBack="True"
                            OnDataBound="ddlDocumrntType_DataBound" OnSelectedIndexChanged="ddlDocumrntType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label" runat="server" id="lblDescription">
                            توضیحات :</label>
                        <asp:TextBox ID="txtDescription" runat="server" data-required="true" Style="width: 680px;
                            height: 75px;" ClientIDMode="Static" TextMode="MultiLine" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label" runat="server" id="lblDocumentDate">
                            تاریخ سند :</label>
                        <uc1:UCDatePicker ID="txtDocumentDate" runat="server" ValidationEnabled="true" />
                    </div>
                    <div class="_5p odd-row">
                        <label class="form-field-label" runat="server" id="lblDocumentCode">
                            شماره سند :</label>
                        <asp:TextBox ID="txtDocumentCode" data-required="true" runat="server" ClientIDMode="Static"
                            MaxLength="20" />
                    </div>
                    <div class="_5p even-row">
                        <label class="form-field-label" runat="server" id="lblSelectFileLabel">
                            انتخاب فایل :</label>
                        <asp:FileUpload Style="display: inline" ID="MyFileUpload" runat="server" data-required="true"
                            ClientIDMode="Static" />
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" OnClientClick="return CheckAttachment();"
                            Text="ارسال فایل" ValidationGroup="Upload" CssClass="btn btn-info" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div data-toggle="collapse" data-target="#collapse2" class="panel-heading pointer-cursor">
            <h4 class="panel-title" runat="server" id="lblAttachmentList">
                لیست ضمائم</h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="grid" style="width: 100%">
                    <asp:GridView ID="gvUplodedFiles" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ReadOnly="True" />
                            <asp:BoundField DataField="AttachmentID" HeaderText="AttachmentID" ReadOnly="True"
                                Visible="false" />
                            <asp:TemplateField HeaderText="نوع سند">
                                <ItemTemplate>
                                    <center>
                                        <%# SankaWebAppFramework.Board.Enums.strAttachmentFileType[Eval("AttachmentType").ToString().StringToByte()]%>
                                    </center>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="توضیحات">
                                <ItemTemplate>
                                    <center>
                                        <span data-sankatooltip='<%# Eval("AttachmentTitle")%>' title='<%# Eval("AttachmentTitle")%>'>
                                            <%# Eval("AttachmentTitle").ToString().ShowSummary(20)%></span>
                                    </center>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DocumentCode" HeaderText="شماره سند" ReadOnly="True" />
                            <asp:TemplateField HeaderText="تاریخ سند">
                                <ItemTemplate>
                                    <center>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("DocumentDate"))%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="تاریخ ثبت">
                                <ItemTemplate>
                                    <%# Utility.GetCustomDateWithoutTime(Eval("RegDate"))%>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="حجم فایل به بایت">
                                <ItemTemplate>
                                    <center>
                                        <%# SankaWebAppFramework.Board.Common.GetFileSize(Eval("AttachmentSource").ToString())%>
                                    </center>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="مشاهده">
                                <ItemTemplate>
                                    <center>
                                        <asp:HyperLink ID="lnkURL" runat="server" NavigateUrl='<%# Eval("AttachmentSource") %>'
                                            Target="_blank">مشاهده</asp:HyperLink>
                                    </center>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="حذف" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("AttachmentID") %>'
                                        CommandName='<%# Eval("AttachmentSource") %>' OnCommand="gvUplodedFiles_RowDeleting"
                                        ImageUrl="~/images/icon/delete.png" OnClientClick="return ShowConfirm(this);"
                                        Text="حذف"></asp:ImageButton>
                                </ItemTemplate>
                                <HeaderStyle />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <center>
                                سندی ضمیمه نگردیده است
                            </center>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hfFileType" runat="server" />
<asp:HiddenField ID="hfFileSize" runat="server" />
<asp:HiddenField ID="hfUploadPath" runat="server" />
<asp:HiddenField ID="hfOwnerRecordID" runat="server" />
<cc1:SankaDialog ID="myMessage" runat="server">
</cc1:SankaDialog>
