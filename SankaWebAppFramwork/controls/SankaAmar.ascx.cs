﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class SankaAmar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                #region بدست آوردن آمار امروز

                DTO.Config.tblAmar.tblAmar Obj = new DTO.Config.tblAmar.tblAmar();
                Obj.FromDate = DateTime.Today;
                Obj.ToDate = DateTime.Now;

                lblTodayAmar.Text = (new DAL.Config.tblAmar.Methods()).VisitCountBetweenTowDate(Obj).ToString();

                #endregion بدست آوردن آمار امروز

                #region بدست آوردن آمار دیروز

                Obj.FromDate = DateTime.Today.AddDays(-1);
                Obj.ToDate = Obj.FromDate.Value.AddDays(1).AddMilliseconds(-1);

                lblYesterdayAmar.Text = ((new DAL.Config.tblAmar.Methods()).VisitCountBetweenTowDate(Obj)).ToString();

                #endregion بدست آوردن آمار دیروز

                #region بدست آوردن آمار هفته اخیر

                Obj.FromDate = DateTime.Today.AddDays(-6);
                Obj.ToDate = DateTime.Now;

                lblLastWeekAmar.Text = (new DAL.Config.tblAmar.Methods()).VisitCountBetweenTowDate(Obj).ToString();

                #endregion بدست آوردن آمار هفته اخیر

                #region بدست آوردن آمار ماه اخیر

                Obj.FromDate = DateTime.Today.AddMonths(-1);
                Obj.ToDate = DateTime.Now;

                lblLastMonthAmar.Text = (new DAL.Config.tblAmar.Methods()).VisitCountBetweenTowDate(Obj).ToString();

                #endregion بدست آوردن آمار ماه اخیر

                #region بدست آوردن آمارکل

                Obj.FromDate = "01/01/1970".StringToDateTime(); 
                Obj.ToDate = DateTime.Now;

                lblAllAmar.Text = (new DAL.Config.tblAmar.Methods()).VisitCountBetweenTowDate(Obj).ToString();

                #endregion بدست آوردن آمارکل

            }
        }
    }
}