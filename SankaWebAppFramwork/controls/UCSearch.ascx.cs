﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.controls
{
    public partial class UCSearch : System.Web.UI.UserControl
    {
        #region پروپرتی ها

        /// <summary>
        /// حداقل کاراکتر ورودی جهت شروع جستجو در براساس نام
        /// </summary>
        [Browsable(true)]
        public int FirstNameMinCharLengh
        {
            get
            {
                return ViewState["FirstNameMinCharLengh"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("FirstNameMinCharLengh", value);
                hfFirstNameMinCharLengh.Value = value.ToString();
                txtFName.Attributes.Add("placeholder", "حداقل تعداد کاراکتر " + value);
            }
        }

        /// <summary>
        /// حداقل کاراکتر ورودی جهت شروع جستجو در براساس نام خانوادگی
        /// </summary>
        [Browsable(true)]
        public int LastNameMinCharLengh
        {
            get
            {
                return ViewState["LastNameMinCharLengh"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("LastNameMinCharLengh", value);
                hfLastNameMinCharLengh.Value = value.ToString();
                txtLName.Attributes.Add("placeholder", "حداقل تعداد کاراکتر " + value);
            }
        }

        /// <summary>
        /// حداقل کاراکتر ورودی جهت شروع جستجو در براساس کدپرسنلی
        /// </summary>
        [Browsable(true)]
        public int PersonnelCodeMinCharLengh
        {
            get
            {
                return ViewState["PersonnelCodeMinCharLengh"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("PersonnelCodeMinCharLengh", value);
                hfPersonnelCodeMinCharLengh.Value = value.ToString();
                txtPersonnelCode.Attributes.Add("placeholder", "حداقل تعداد کاراکتر " + value);
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام) در صفحه ی فراخواننده جهت ریختن نتیجه در آن
        /// </summary>
        [Browsable(true)]
        public string ParentFirstNameOutputFieldID
        {
            get
            {
                return hfParentFirstNameOutputFieldID.Value;
            }
            set
            {
                hfParentFirstNameOutputFieldID.Value = value.ToString();
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام خانوادگی) در صفحه ی فراخواننده جهت ریختن نتیجه در آن
        /// </summary>
        [Browsable(true)]
        public string ParentLastNameOutputFieldID
        {
            get
            {
                return hfParentLastNameOutputFieldID.Value;
            }
            set
            {
                hfParentLastNameOutputFieldID.Value = value;
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام پدر) در صفحه ی فراخواننده جهت ریختن نتیجه در آن
        /// </summary>
        [Browsable(true)]
        public string ParentFatherNameOutputFieldID
        {
            get
            {
                return hfParentFatherNameOutputFieldID.Value;
            }
            set
            {
                hfParentFatherNameOutputFieldID.Value = value.ToString();
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(کدپرسنلی) در صفحه ی فراخواننده جهت ریختن نتیجه
        /// </summary>
        [Browsable(true)]
        public string ParentPersonnelCodeOutputFieldID
        {
            get
            {
                return hfParentPersonnelCodeOutputFieldID.Value;
            }
            set
            {
                hfParentPersonnelCodeOutputFieldID.Value = value;
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(کد استان) در صفحه ی فراخواننده جهت ریختن نتیجه
        /// </summary>
        [Browsable(true)]
        public string ParentOstanCodeOutputFieldID
        {
            get
            {
                return hfParentOstanCodeOutputFieldID.Value;
            }
            set
            {
                hfParentOstanCodeOutputFieldID.Value = value.ToString();
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(کد منطقه) در صفحه ی فراخواننده جهت ریختن نتیجه
        /// </summary>
        [Browsable(true)]
        public string ParentRegionCodeOutputFieldID
        {
            get
            {
                return hfParentRegionCodeOutputFieldID.Value;
            }
            set
            {
                hfParentRegionCodeOutputFieldID.Value = value.ToString();
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(کد مدرسه) در صفحه ی فراخواننده جهت ریختن نتیجه
        /// </summary>
        [Browsable(true)]
        public string ParentSchoolCodeOutputFieldID
        {
            get
            {
                return hfParentSchoolCodeOutputFieldID.Value;
            }
            set
            {
                hfParentSchoolCodeOutputFieldID.Value = value.ToString();
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام) در صفحه ی فراخواننده جهت دریافت اطلاعات پیش فرض
        /// </summary>
        [Browsable(true)]
        public string ParentFirstNameInputFieldID
        {
            get
            {
                return hfParentFirstNameInputFieldID.Value;
            }
            set
            {
                hfParentFirstNameInputFieldID.Value = value;
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام پدر) در صفحه ی فراخواننده جهت دریافت اطلاعات پیش فرض
        /// </summary>
        [Browsable(true)]
        public string ParentFatherNameInputFieldID
        {
            get
            {
                return hfParentFatherNameInputFieldID.Value;
            }
            set
            {
                hfParentFatherNameInputFieldID.Value = value;
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(نام خانوادگی) در صفحه ی فراخواننده جهت دریافت اطلاعات پیش فرض
        /// </summary>
        [Browsable(true)]
        public string ParentLastNameInputFieldID
        {
            get
            {
                return hfParentLastNameInputFieldID.Value;
            }
            set
            {
                hfParentLastNameInputFieldID.Value = value;
            }
        }

        /// <summary>
        /// شناسه فیلد اچ.تی.ام.ال مورد نظر(کدپرسنلی) در صفحه ی فراخواننده جهت دریافت اطلاعات پیش فرض
        /// </summary>
        [Browsable(true)]
        public string ParentPersonnelCodeInputFieldID
        {
            get
            {
                return hfParentPersonnelCodeInputFieldID.Value;
            }
            set
            {
                hfParentPersonnelCodeInputFieldID.Value = value;
            }
        }

        /// <summary>
        /// فعال بودن امکان جستجو براساس پارامتر نام و نام خانوادگی
        /// </summary>
        [Browsable(true)]
        public bool PerformSearchOnName
        {
            get
            {
                if (ViewState["PerformSearchOnName"] != null)
                {
                    return ViewState["PerformSearchOnName"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("PerformSearchOnName", value);
            }
        }

        /// <summary>
        /// فعال بودن امکان جستجو براساس پارامتر کد پرسنلی
        /// </summary>
        [Browsable(true)]
        public bool PerformSearchOnPersonnelCode
        {
            get
            {
                if (ViewState["PerformSearchOnPersonnelCOde"] != null)
                {
                    return ViewState["PerformSearchOnPersonnelCOde"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("PerformSearchOnPersonnelCOde", value);
            }
        }

        /// <summary>
        /// مقدار پیش فرض استان
        /// </summary>
        [Browsable(true)]
        public int FilterResultByOstanCode
        {
            get
            {
                return ViewState["FilterResultByOstanCode"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("FilterResultByOstanCode", value);
            }
        }

        /// <summary>
        /// مقدار پیش فرض منطقه
        /// </summary>
        [Browsable(true)]
        public int FilterResultByRegionCode
        {
            get
            {
                return ViewState["FilterResultByRegionCode"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("FilterResultByRegionCode", value);
            }
        }

        /// <summary>
        /// مقدار پیش فرض مدرسه
        /// </summary>
        [Browsable(true)]
        public int FilterResultBySchoolCode
        {
            get
            {
                return ViewState["FilterResultBySchoolCode"].ToString().StringToInt();
            }
            set
            {
                ViewState.Add("FilterResultBySchoolCode", value);
            }
        }

        /// <summary>
        /// فعال بودن امکان فیلتر براساس استان
        /// </summary>
        [Browsable(true)]
        public bool CanFilterResultByOstan
        {
            get
            {
                if (ViewState["CanFilterResultByOstan"] != null)
                {
                    return ViewState["CanFilterResultByOstan"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("CanFilterResultByOstan", value);
            }
        }

        /// <summary>
        /// فعال بودن امکان فیلتر براساس منطقه
        /// </summary>
        [Browsable(true)]
        public bool CanFilterResultByRegion
        {
            get
            {
                if (ViewState["CanFilterResultByRegion"] != null)
                {
                    return ViewState["CanFilterResultByRegion"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("CanFilterResultByRegion", value);
            }
        }

        /// <summary>
        /// فعال بودن امکان فیلتر براساس مدرسه
        /// </summary>
        [Browsable(true)]
        public bool CanFilterResultBySchool
        {
            get
            {
                if (ViewState["CanFilterResultBySchool"] != null)
                {
                    return ViewState["CanFilterResultBySchool"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("CanFilterResultBySchool", value);
            }
        }

        /// <summary>
        /// از این پروپرتی تشخیص میدهیم که کدام باتن کلیک شده است
        /// </summary>
        private byte ButtonClick
        {
            get
            {
                return hfBtnclick.Value.StringToByte();
            }
            set
            {
                hfBtnclick.Value = value.ToString();
            }
        }

        /// <summary>
        /// آیدی کاربر
        /// </summary>
        public long UserID
        {
            get;
            set;
        }

        /// <summary>
        /// اگر ترو باشد، یعنی کنترل های منطقه، براساس مناطق قابل دسترس کاربر جاری، قفل شود
        /// </summary>
        public bool LockByUserAccessLevel
        {
            get
            {
                if (ViewState["LockByUserAccessLevel"] != null)
                {
                    return ViewState["LockByUserAccessLevel"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("LockByUserAccessLevel", value);
            }
        }

        #endregion پروپرتی ها

        #region برای صفحه بندی گریدها

        /// <summary>
        /// اندازه صفحات گرید
        /// </summary>
        [Browsable(true)]
        public int PageSize
        {
            get
            {
                int Size = hfPageSize.Value.StringToInt();

                return Size == 0 ? 10 : Size;
            }
            set
            {
                hfPageSize.Value = value.ToString();
            }
        }

        /// <summary>
        /// تعداد کل سطرهای پیدا شده در گرید
        /// </summary>
        private long TotalRow
        {
            get
            {
                return hfTotalRow.Value.StringToInt();
            }
            set
            {
                hfTotalRow.Value = value.ToString();
            }
        }

        /// <summary>
        /// شماره صفحه جاری گرید
        /// </summary>
        private int PageNumber
        {
            get
            {
                return hfPageNumber.Value.StringToInt();
            }
            set
            {
                hfPageNumber.Value = value.ToString();
            }
        }

        /// <summary>
        ///است کرده تغییر گرید صفحه شماره آیا که میکند مشخص 
        ///<para>است رفته گرید از دیگری صفحه به کاربر عبارتی به </para>
        ///</summary>
        protected bool GridViewPageChanged
        {
            get
            {
                return hfPageChanged.Value.StringToInt() > 0;
            }
            set
            {
                hfPageChanged.Value = (value) ? "1" : "0";
            }
        }

        #endregion برای صفحه بندی گریدها

        /// <summary>
        /// وضعیت جستجوی انجام شده
        /// </summary>
        enum SearchStats
        {
            /// <summary>
            /// جستجو براساس نام
            /// </summary>
            ByName = 0,

            /// <summary>
            /// جستجو براساس کد پرسنلی
            /// </summary>
            ByPCode = 1
        }

        /// <summary>
        /// لود صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region ست کردن وضعیت فعال بودن آیتم های روی صفحه با توجه به پارامترهای ورودی کنترل

                if (!LockByUserAccessLevel)
                {//  اگر براساس مناطق قابل دسترس کاربر، قفل نشود، برای قفل کردن از پروپرتی های معمولی استفاده شود 
                    ddlOstan.Enabled = CanFilterResultByOstan;
                    ddlRegion.Enabled = CanFilterResultByRegion;
                    ddlSchool.Enabled = CanFilterResultBySchool;
                }

                divSearchByName.Visible = PerformSearchOnName;
                divSearchByCode.Visible = PerformSearchOnPersonnelCode;

                #endregion ست کردن وضعیت فعال بودن آیتم های روی صفحه با توجه به پارامترهای ورودی کنترل

                BindStates(ddlOstan);

                #region نمایش مقادیر استان، منطقه و مدرسه در صورت ست شدن آیدی آنها

                if (FilterResultByOstanCode > 0)
                {// اگر آیدی استان ست شود، استان انتخاب شود و لیست مناطقش نمایش داده شود
                    ddlOstan.SelectedValue = FilterResultByOstanCode.ToString();
                    ddlOstan_SelectedIndexChanged(sender, e);

                    if (LockByUserAccessLevel)
                    {// اگر قفل شدن مناطق، براساس مناطق قابل دسترس کاربر باشد، استان قفل شود
                        if (FilterResultByOstanCode > 0)
                        {// اگر مقدار آیدی استان کاربر لاگین کرده ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                            ddlOstan.Enabled = false;
                        }
                    }

                    if (FilterResultByRegionCode > 0)
                    {// اگر آیدی منطقه ست شده باشد، منطقه را انتخاب کنیم از لیست و مدرسه های مربوط به آن را نمایش دهیم
                        ddlRegion.SelectedValue = FilterResultByRegionCode.ToString();
                        ddlRegion_SelectedIndexChanged(sender, e);

                        if (LockByUserAccessLevel)
                        {// اگر قفل شدن مناطق، براساس مناطق قابل دسترس کاربر باشد، منطقه قفل شود
                            if (FilterResultByRegionCode > 0)
                            {//  اگر مقدار آیدی منطقه کاربر لاگین کرده ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                                ddlRegion.Enabled = false;
                            }
                        }

                        if (FilterResultBySchoolCode > 0)
                        {// اگر آیدی مدرسه ست شده باشد، از لیست انتخاب شود
                            ddlSchool.SelectedValue = FilterResultBySchoolCode.ToString();

                            if (LockByUserAccessLevel)
                            {// اگر قفل شدن مناطق، براساس مناطق قابل دسترس کاربر باشد، منطقه قفل شود
                                if (FilterResultBySchoolCode > 0)
                                {//  اگر مقدار آیدی مدرسه کاربر لاگین کرده ست شده باشد، کمبو مربوطه اش را قفل میکنیم
                                    ddlSchool.Enabled = false;
                                }
                            }
                        }
                    }
                }

                #endregion نمایش مقادیر استان، منطقه و مدرسه در صورت ست شدن آیدی آنها

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                //GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;

                if (ButtonClick == (byte)SearchStats.ByName)
                {// یعنی جستجو براساس نام می باشد
                    Search(txtFName.Text, txtLName.Text);
                }
                else
                {// یعنی جستجو براساس کد پرسنلی میباشد
                    Search(txtPersonnelCode.Text);
                }
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvSearchResult.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
            }
        }

        /// <summary>
        /// جستجو براساس نام
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchByName_Click(object sender, EventArgs e)
        {
            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;
            Search(txtFName.Text, txtLName.Text);
            ButtonClick = (byte)SearchStats.ByName;
        }

        /// <summary>
        /// جستجو براساس کد پرسنلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchByCode_Click(object sender, EventArgs e)
        {
            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;
            Search(txtPersonnelCode.Text);
            ButtonClick = (byte)SearchStats.ByPCode;
        }

        /// <summary>
        /// جستجو و نمایش نتایج در گرید
        /// <param name="SearchValue">مقدار جهت جستجو، نام کامل یا کد پرسنلی</param>
        /// </summary>
        private void Search(string SearchValue, string LastName = "")
        {
            List<DTO.Board.tblPersonnelView.tblPersonnelView> lstUsers = new List<DTO.Board.tblPersonnelView.tblPersonnelView>();


            #region جستجو

            DTO.Board.tblPersonnelView.tblPersonnelView Obj = new DTO.Board.tblPersonnelView.tblPersonnelView();

            Obj.PageSize = PageSize;
            Obj.PageNumber = PageNumber;
            Obj.OstanCode = null;
            if (ddlOstan.SelectedValue.StringToInt() > 0)
            {
                Obj.OstanCode = ddlOstan.SelectedValue.StringToInt();
            }
            Obj.RegionCode = null;
            if (ddlRegion.SelectedValue.StringToInt() > 0)
            {
                Obj.RegionCode = ddlRegion.SelectedValue.StringToInt();
            }
            Obj.SchoolCode = null;
            if (ddlSchool.SelectedValue.StringToInt() > 0)
            {
                Obj.SchoolCode = ddlSchool.SelectedValue.StringToInt();
            }
            Obj.SortColumn = hfSortColumn.Value;
            Obj.SortOrder = hfSortOrder.Value;

            if (SearchValue.StringToLong() > 0)
            {//  اگر عدد باشد، یعنی کد پرسنلی وارد شده است و براساس کد پرسنلی جستجو میکنیم
                Obj.PersonnelCode = SearchValue.StringToLong();
                lstUsers = (new DAL.Board.tblPersonnelView.Methods()).SelectByLikePersonnelCode(Obj);
            }
            else
            {// در غیر اینصورت براساس نام جستجو میکنیم

                SearchValue = SearchValue.CorrectArabicChars();

                SearchValue = SearchValue.Trim().Replace(" ", "");

                Obj.FirstName = SearchValue;
                Obj.LastName = LastName;
                lstUsers = (new DAL.Board.tblPersonnelView.Methods()).SelectByLikeName(Obj);
            }

            #endregion جستجو

            #region نمایش نتایج

            gvSearchResult.CustomDataBind(lstUsers, PageSize, this.PageNumber, (int)Enums.PageLinkNumber.TenNumber);
            // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
            TotalRow = lstUsers.Any() ? lstUsers[0].TotalRow.Value : 0;
            lblTotalSearchCount.Text = TotalRow.ToString();

            #endregion نمایش نتایج

            SetSortStatus();
        }
        

        /// <summary>
        /// نمایش مناطق مربوط به استان انتخاب شده ، هنگام تغییر استان انتخابی
        /// </summary>
        protected void ddlOstan_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRegion(ddlRegion, ddlOstan.SelectedValue.StringToInt());
            ddlRegion.Enabled = true;
            ddlSchool.Enabled = false;
            ddlSchool.SelectedIndex = -1;
        }

        /// <summary>
        /// نمایش مدرسه های مربوط به منطقه انتخاب شده ، هنگام تغییر منطقه انتخابی
        /// </summary>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchool(ddlSchool, ddlRegion.SelectedValue.StringToInt());
            ddlSchool.Enabled = true;
        }

        /// <summary>
        /// کدهای مربوط به کلیک دکمه پالایش جستجو
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            if (ButtonClick == (byte)SearchStats.ByName)
            {// یعنی جستجو براساس نام می باشد
                Search(txtFName.Text, txtLName.Text);
            }
            else
            {// یعنی جستجو براساس کد پرسنلی میباشد
                Search(txtPersonnelCode.Text);
            }
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            hfSortColumn.Value = e.CommandArgument.ToString();
            hfSortOrder.Value = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            if (ButtonClick == (byte)SearchStats.ByName)
            {// یعنی جستجو براساس نام می باشد
                Search(txtFName.Text, txtLName.Text);
            }
            else
            {// یعنی جستجو براساس کد پرسنلی میباشد
                Search(txtPersonnelCode.Text);
            }
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvSearchResult.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + hfSortColumn.Value;
                    LinkButton lnkbtn = gvSearchResult.HeaderRow.FindControl(ControlID) as LinkButton;
                    Image img = gvSearchResult.HeaderRow.FindControl("img" + hfSortColumn.Value) as Image;

                    if (lnkbtn != null)
                    {
                        if (hfSortOrder.Value == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (hfSortOrder.Value == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// پرکردن دراپ دان استان ها
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست استان ها را نمایش دهد</param>
        protected void BindStates(DropDownList ddl)
        {
            DTO.Config.tblOstans.tblOstans obj = new DTO.Config.tblOstans.tblOstans();
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblOstans.Methods()).SelectAllUserOstans(obj);
            ddl.DataTextField = "Name";
            ddl.DataValueField = "OstanCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب استان", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان مناطق
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست منطقه ها را نمایش دهد</param>
        protected void BindRegion(DropDownList ddl, int OstanCode)
        {
            DTO.Config.tblRegions.tblRegions obj = new DTO.Config.tblRegions.tblRegions();
            obj.OstanCode = OstanCode;
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblRegions.Methods()).SelectAllUserRegions(obj);
            ddl.DataTextField = "Name";
            ddl.DataValueField = "RegionCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب منطقه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان مدارس
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست مدرسه ها را نمایش دهد</param>
        protected void BindSchool(DropDownList ddl, int RegionCode)
        {
            DTO.Config.tblSchool.tblSchool obj = new DTO.Config.tblSchool.tblSchool();
            obj.RegionCode = RegionCode;
            obj.UserID = UserID;
            ddl.DataSource = (new DAL.Config.tblSchool.Methods()).SelectAllUserSchools(obj);
            ddl.DataTextField = "SchoolName";
            ddl.DataValueField = "SchoolCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب مدرسه", "-1"));
        }
    }
}