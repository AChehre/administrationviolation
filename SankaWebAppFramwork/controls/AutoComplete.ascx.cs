﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Controls
{
    public partial class AutoComplete : System.Web.UI.UserControl
    {
        /// <summary>
        /// حداقل کاراکتر ورودی جهت شروع جستجو در براساس نام
        /// </summary>
        public int NameMinLength
        {
            set
            {
                txtSearchFullName.Attributes.Add("data-minlength", value.ToString());
            }
        }

        /// <summary>
        /// حداقل کاراکتر ورودی جهت شروع جستجو در براساس کدپرسنلی
        /// </summary>
        public int PersonalCodeMinLength
        {
            set
            {
                txtSearchPersonnelCode.Attributes.Add("data-minlength", value.ToString());
            }
        }

        /// <summary>
        /// تاخیر به میلی ثانیه جهت ارسال درخواست های جستجو به سمت سرور در جستجو با نام
        /// </summary>
        public int NameDelay
        {
            set
            {
                txtSearchFullName.Attributes.Add("data-delay", value.ToString());
            }
        }

        /// <summary>
        /// تاخیر به میلی ثانیه جهت ارسال درخواست های جستجو به سمت سرور در جستجو با کدپرسنلی
        /// </summary>
        public int PersonalCodeDelay
        {
            set
            {
                txtSearchPersonnelCode.Attributes.Add("data-delay", value.ToString());
            }
        }
        
        /// <summary>
        /// نمایش پنل در حال انتظار کل صفحه را قفل کند یا خیر
        /// </summary>
        public bool ShowWaitingFullScreen
        {
            set;
            get;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlControlLock.CssClass = "pnl-waiting";
            pnlControlLock.Visible = true;
            pnlFullScreenLock.Visible = false;

            if (ShowWaitingFullScreen)
            {// اگر بخواهیم پنل در حال انتظار کل صفحه را قفل کند
                pnlControlLock.CssClass = "";
                pnlFullScreenLock.CssClass = "pnl-waiting";

                pnlControlLock.Visible = false;
                pnlFullScreenLock.Visible = true;
            }
        }
    }
}