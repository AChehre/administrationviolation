﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCViolationFileVote.ascx.cs"
    Inherits="SankaWebAppFramework.controls.UCViolationFileVote" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Src="~/controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<script src="/js/Message.js" type="text/javascript"></script>
<script type="text/javascript">
    function SetIgnoreFlagAndSend() {
        document.getElementById('hfIgnoreFlagID').value = "1";
        document.getElementById('btnSaveVote').click();
    }
</script>
<uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
<div class="_5p">
    <label class="form-field-label">
        شماره رای :</label>
    <asp:TextBox ID="txtVoteNumber" Width="160px" runat="server"></asp:TextBox>
</div>
<div class="_5p">
    <label class="form-field-label">
        نوع رای :</label>
    <asp:DropDownList ID="ddlVoteType" runat="server" Width="150px" AutoPostBack="True"
        OnSelectedIndexChanged="ddlVoteType_SelectedIndexChanged" Enabled="False">
    </asp:DropDownList>
</div>
<div class="_5p">
    <label class="form-field-label">
        تاریخ صدور :</label>
    <uc1:UCDatePicker ID="txtVoteDate" DontValidate="false" runat="server" />
</div>
<div class="_5p">
    <label class="form-field-label">
        به اتفاق / اکثریت :</label>
    <asp:DropDownList ID="ddlSoudorType" runat="server" Width="150px">
    </asp:DropDownList>
</div>
<div id="CondemnationDiv" runat="server" visible="false" class="prevententer">
    <div class="_5p">
        <label class="form-field-label">
            مجازات ماده 9 قانون :</label>
        <asp:DropDownList ID="ddlPenaltyType" runat="server" Width="150px" AutoPostBack="True"
            OnSelectedIndexChanged="ddlPenaltyType_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div class="_5p">
        <label class="form-field-label">
            شرح مجازات :</label>
        <asp:TextBox data-required="true" ID="txtPenaltyDetail" runat="server" TextMode="MultiLine"
            Style="width: 720px; height: 75px;" MaxLength="1000" />
    </div>
</div>
<div class="_5p">
    <label class="form-field-label">
        وضیعت :</label>
    <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
        ID="rbtnIsFinite" runat="server" OnSelectedIndexChanged="rbtnIsFinite_SelectedIndexChanged"
        AutoPostBack="True">
        <asp:ListItem Value="True" Text="قطعی"></asp:ListItem>
        <asp:ListItem Value="False" Text="قابل پژوهش" Selected="True"></asp:ListItem>
    </asp:RadioButtonList>
</div>
<div class="_5p" runat="server" id="divAddress">
    <label class="form-field-label">
        نشانی کارگزینی :</label>
    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Height="83px" MaxLength="256"
        Width="691px" />
</div>
<div class="_5p even-row">
    <cc1:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc1:SankaDialog>
</div>
<asp:HiddenField ID="hfIgnoreFlagID" runat="server" Value="0" ClientIDMode="Static" />
