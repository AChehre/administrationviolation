﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SankaWebAppFramework.Default" %>

<%@ Register TagPrefix="sanka" TagName="Amar" Src="~/controls/SankaAmar.ascx" %>
<%@ Register TagPrefix="sanka" TagName="States" Src="~/controls/UCStates.ascx" %>

<%@ Register TagPrefix="sanka" TagName="ActionMenu" Src="~/controls/UCActionMenu.ascx" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/UCStates.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <Sanka:SankaDialog runat="server" MessageLocationID="divMessage" ClientIDMode="Static" ID="MessageDialog">
    </Sanka:SankaDialog>
</asp:Content>
