﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Web.Configuration;
using DialogMessage;

namespace SankaWebAppFramework
{
    public class GeneralControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long FileID
        {
            get
            {
                if (ViewState["FileID"] != null)
                {
                    return ViewState["FileID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("FileID", value);
            }
        }

        /// <summary>
        /// شناسه هیات
        /// </summary>
        public long BoardID
        {
            get
            {
                if (ViewState["BoardID"] != null)
                {
                    return ViewState["BoardID"].ToString().StringToInt();
                }

                return 0;
            }
            set
            {
                ViewState.Add("BoardID", value);
            }
        }

        /// <summary>
        /// شناسه درخواست
        /// </summary>
        public int RequestID
        {
            set
            {
                ViewState.Add("RequestID", value);
            }
            get
            {
                if (ViewState["RequestID"] != null)
                {
                    return ViewState["RequestID"].ToString().StringToInt();
                }
                return 0;
            }
        }

        /// <summary>
        /// شناسه درخواست ثبت شده
        /// </summary>
        public long RequestDataID
        {
            set
            {
                ViewState.Add("RequestDataID", value);
            }
            get
            {
                if (ViewState["RequestDataID"] != null)
                {
                    return ViewState["RequestDataID"].ToString().StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// نوع هیات
        /// </summary>
        public byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// مجوزهای انجام عملیات برای کاربر جاری
        /// </summary>
        public List<int> PermitedRequestID
        {
            get
            {
                if (Session["PermitedRequestID"] != null)
                {
                    return (List<int>)Session["PermitedRequestID"];
                }
                return new List<int>();
            }
        }

        /// <summary>
        /// آیا کاربر مجوز بروزرسانی دارد ؟
        /// </summary>
        /// <returns></returns>
        public bool HasUpdatePermission()
        {
            return PermitedRequestID.Contains(this.RequestID);
        }

        /// <summary>
        /// یال عملیاتی جاری
        /// </summary>
        public int ActivityYear
        {
            get
            {
                if (ViewState["ActivityYear"] != null)
                {
                    return ViewState["ActivityYear"].ToString().StringToInt();
                }

                return 0;
            }
            set
            {
                ViewState.Add("ActivityYear", value);
            }
        }

        /// <summary>
        /// گرفتن شناسه کاربر وارد شده
        /// </summary>
        protected long UserID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserID;
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}
