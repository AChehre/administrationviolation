﻿

using System.Collections.Generic;
using System.Linq;
using System;
namespace SankaWebAppFramework
{
    public class UserManagment : IDisposable
    {
        /// <summary>
        /// ثبت اطلاعات یک کاربر در دیتابیس
        /// از تمام پارامتر های ورودی، یک شی از نوع کلاس 
        /// tblUserAttributesMapView
        /// ساخته و مقداردهی میشود و در یکبار اتصال به دیتابیس، تمام اطلاعات مربوط به کاربر، ویژگی هایش و گروه های او ثبت میشود
        /// </summary>
        /// <param name="ObjUser">شی از نوع کلاس کاربر</param>
        /// <param name="ObjUserAttr">شی از نوع کلاس ویژگی های کاربر</param>
        /// <param name="_lstSelectedgroup">لیستی از گروه هایی که کاربر در آنها عضو است</param>
        /// <returns>آیدی کاربر ثبت شده</returns>
        public long AddUser(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup)
        {
            long Result = 0;

            try
            {
                Result = SaveData(ObjUser, ObjUserAttr, _lstSelectedgroup);
            }
            catch
            {
            }

            return Result;
        }

        /// <summary>
        /// ویرایش اطلاعات یک کاربر در دیتابیس
        /// از تمام پارامتر های ورودی، یک شی از نوع کلاس 
        /// tblUserAttributesMapView
        /// ساخته و مقداردهی میشود و در یکبار اتصال به دیتابیس، تمام اطلاعات مربوط به کاربر، ویژگی هایش و گروه های او , ویرایش میشود
        /// </summary>
        /// <param name="ObjUser">شی از نوع کلاس کاربر</param>
        /// <param name="ObjUserAttr">شی از نوع کلاس ویژگی های کاربر</param>
        /// <param name="_lstSelectedgroup">لیستی از گروه هایی که کاربر در آنها عضو است</param>
        /// <returns>آیدی کاربر ویرایش شده</returns>
        public long EditUser(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup)
        {
            long Result = 0;

            try
            {
                Result = SaveData(ObjUser, ObjUserAttr, _lstSelectedgroup);
            }
            catch
            {
            }

            return Result;
        }

        /// <summary>
        /// ویرایش اطلاعات یک کاربر در دیتابیس
        /// از تمام پارامتر های ورودی، یک شی از نوع کلاس 
        /// tblUserAttributesMapView
        /// ساخته و مقداردهی میشود و در یکبار اتصال به دیتابیس، تمام اطلاعات مربوط به کاربر و ویژگی هایش ویرایش میشود
        /// </summary>
        /// <param name="ObjUser">شی از نوع کلاس کاربر</param>
        /// <param name="ObjUserAttr">شی از نوع کلاس ویژگی های کاربر</param>
        /// <returns>آیدی کاربر ویرایش شده</returns>
        public long EditUser(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr)
        {
            long Result = 0;

            try
            {
                Result = SaveData(ObjUser, ObjUserAttr, null);
            }
            catch
            {
            }

            return Result;
        }

        /// <summary>
        /// ثبت یا ویرایش اطلاعات یک کاربر در دیتابیس
        /// اگر ایدی کاربر در شی کلاس کاربر بزرگتر از صفر ست شود،عمل ویرایش و در غیر اینصورت ثبت انجام میشود
        /// از تمام پارامتر های ورودی، یک شی از نوع کلاس 
        /// tblUserAttributesMapView
        /// ساخته و مقداردهی میشود و در یکبار اتصال به دیتابیس، تمام اطلاعات مربوط به کاربر، ویژگی هایش و گروه های او ثبت میشود
        /// </summary>
        /// <param name="ObjUser">شی از نوع کلاس کاربر</param>
        /// <param name="ObjUserAttr">شی از نوع کلاس ویژگی های کاربر</param>
        /// <param name="_lstSelectedgroup">لیستی از گروه هایی که کاربر در آنها عضو است</param>
        /// <returns>آیدی کاربر ثبت شده یا ویرایش شده</returns>
        private long SaveData(DTO.Config.tblUser.tblUser ObjUser, DTO.Config.tblUserAttributes.tblUserAttributes ObjUserAttr, List<DTO.Config.tblUserGroupMap.tblUserGroupMap> _lstSelectedgroup)
        {
            long Result = 0;

            #region مقدار دهی آبجکت

            DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView Obj = new DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView();
            // اطلاعات کاربر
            Obj.EMail = ObjUser.EMail;
            Obj.FName = ObjUser.FName;
            Obj.IsActive = ObjUser.IsActive;
            Obj.LName = ObjUser.LName;
            Obj.Password = ObjUser.Password;
            Obj.RegisterDate = ObjUser.RegisterDate;
            Obj.UserID = ObjUser.UserID;
            Obj.UserName = ObjUser.UserName;
            // سایر ویژگی های کاربر
            Obj.RegionCode = ObjUserAttr.RegionCode;
            Obj.SchoolCode = ObjUserAttr.SchoolCode;
            Obj.OstanCode = ObjUserAttr.OstanCode;
            Obj.PersonnelCode = ObjUserAttr.PersonnelCode;
            Obj.DefaultPageUrl = ObjUser.DefaultPageUrl;
            // گروه های کاربر
            if (_lstSelectedgroup != null)
            {
                foreach (DTO.Config.tblUserGroupMap.tblUserGroupMap Item in _lstSelectedgroup)
                {
                    Obj.SelectedUserGroup += Item.UserGroupID + ",";
                }
                if (Obj.SelectedUserGroup.Length > 1)
                {// پاک کردن آخرین کاما
                    Obj.SelectedUserGroup = Obj.SelectedUserGroup.Substring(0, Obj.SelectedUserGroup.Length - 1);
                }
            }

            #endregion مقدار دهی آبجکت

            if (Obj.UserID.HasValue && Obj.UserID.Value > 0)
            {// اگر آیدی کاربر به بزرگتر از صفر باشد، عمل ویرایش انجام شود
                #region ویرایش اطلاعات

                if (_lstSelectedgroup != null)
                {// اگر گروه های کاربری وجود داشت
                    if ((new DAL.Config.tblUserAttributesMapView.Methods()).Update(Obj))
                    {// در صورت موفقیت آمیز بودن ویرایش، آیدی کاربر برگردانده شود
                        Result = Obj.UserID.Value;
                    }
                }
                else
                {
                    if ((new DAL.Config.tblUserAttributesMapView.Methods()).UpdateWithoutGroups(Obj))
                    {// در صورت موفقیت آمیز بودن ویرایش، آیدی کاربر برگردانده شود
                        Result = Obj.UserID.Value;
                    }
                }

                #endregion ویرایش اطلاعات
            }
            else
            {// در غیر این صورت عمل حذف انجام شود
                #region ثبت اطلاعات

                Obj.UserID = 0;

                // آیدی کاربر برگردانده میشود
                Result = (new DAL.Config.tblUserAttributesMapView.Methods()).Insert(Obj);

                #endregion ثبت اطلاعات
            }

            return Result;
        }

        /// <summary>
        /// پاک کردن اطلاعات کاربر
        /// شامل ، اطلاعات فردی ، ویژگی ها و گروه هایی که در آن عضو است
        /// </summary>
        /// <param name="UserID">آیدی کاربر موردنظر</param>
        /// <returns>ترو یعنی حذف کردن موفقیت آمیز بوده است</returns>
        public bool DeleteUser(long UserID)
        {
            DTO.Config.tblUser.tblUser obj = new DTO.Config.tblUser.tblUser();
            obj.UserID = UserID;
            return (new DAL.Config.tblUser.Methods()).Delete(obj);
        }

        /// <summary>
        /// بررسی ورود کاربران و در صورت درست بودن نام کاربری و کلمه عبور، اطلاعات کاربر برگردانده میشود
        /// </summary>
        /// <param name="UserName">نام کاربری</param>
        /// <param name="Pass">کلمه عبور</param>
        /// <returns>اطلاعات کاربر لاگین شده</returns>
        public UserProfile Login(string UserName, string Pass)
        {
            UserProfile Result = new UserProfile();
            Result.lstGroupIDs = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();

            DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView obj = new DTO.Config.tblUserAttributesMapView.tblUserAttributesMapView();
            obj.UserName = UserName;
            obj.Password = Pass;
            obj.IsActive = (byte)Enums.User_Active.Active > 0;
            obj = (new DAL.Config.tblUserAttributesMapView.Methods()).Authenticate(obj);

            if (obj.RowNumber > 0)
            {// اگر رکوردی وجود داشت،یعنی ورود موفقیت آمیز است
                Result.UserID = obj.UserID.Value;
                if (obj.OstanCode.HasValue)
                {
                    Result.OstanCode = obj.OstanCode.Value;
                }
                if (obj.RegionCode.HasValue)
                {
                    Result.RegionCode = obj.RegionCode.Value;
                }
                if (obj.SchoolCode.HasValue)
                {
                    Result.SchoolCode = obj.SchoolCode.Value;
                }

                Result.FullName = obj.FullName;

                // گرفتن آیدی گروه هایی که کاربر در آن عضو است
                DTO.Config.tblUserGroupMap.tblUserGroupMap ugObj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                ugObj.UserID = obj.UserID.Value;
                Result.lstGroupIDs = (new DAL.Config.tblUserGroupMap.Methods()).SelectByUserID(ugObj);
                Result.DefaultPageUrl = obj.DefaultPageUrl;
                Result.Online = obj.Online;
                Result.SessionExpiration = obj.SessionExpiration;
                Result.IsActive = obj.IsActive;
            }

            return Result;
        }

        public void Dispose()
        {
            GC.WaitForPendingFinalizers();
        }
    }
}