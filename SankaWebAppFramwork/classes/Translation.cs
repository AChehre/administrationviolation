﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SankaWebAppFramework
{
    public class Translation
    {
        Dictionary<MessageContentType, string> MessageRepository;

        public Translation()
        {
            MessageRepository = new Dictionary<MessageContentType, string>();
            MessageRepository.Add(MessageContentType.MSG_PAGE_ACCESS_DENIED, "شما مجوز دسترسی به این صفحه را ندارید.");
            MessageRepository.Add(MessageContentType.MSG_YOU_MUST_LOGIN_TO_ACCESS_THIS_PAGE, "برای دسترسی به این بخش، باید با نام کاربری و رمز ورود به سامانه وارد شود.");
            MessageRepository.Add(MessageContentType.MSG_PAGE_IS_NOT_PUBLISH, "صفحه درخواستی شما قابل دسترس نمی باشد.");
            MessageRepository.Add(MessageContentType.MSG_PAGE_NOT_EXIST, "صفحه درخواستی شما وجود ندارد.");
        }

        /// <summary> 
        /// نوع پیام ها جهت نمایش به کاربران
        /// </summary>
        public enum MessageContentType
        {
            /// <summary>
            /// مجوز دسترسی به صفحه را ندارد
            /// </summary>
            MSG_PAGE_ACCESS_DENIED = 0,

            /// <summary>
            /// برای دسترسی به صفحه باید لاگین کند
            /// </summary>
            MSG_YOU_MUST_LOGIN_TO_ACCESS_THIS_PAGE = 1,

            /// <summary>
            /// صفحه پابلیش نشده است
            /// </summary>
            MSG_PAGE_IS_NOT_PUBLISH = 2,

            /// <summary>
            /// صفحه وجود ندارد
            /// </summary>
            MSG_PAGE_NOT_EXIST = 3
        }

        public string MessageContent(MessageContentType Type)
        {
            string Result = "";

            Result = MessageRepository[Type].ToString();

            return Result;
        }
    }
}