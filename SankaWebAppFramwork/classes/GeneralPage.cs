﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Web.Configuration;
using DialogMessage;

namespace SankaWebAppFramework
{
    public class GeneralPage : System.Web.UI.Page
    {
        /// <summary>
        /// لیبل عنوان صفحه
        /// </summary>
        public string PageLabelTitle
        {
            get
            {
                try
                {
                    if (this.Master != null)
                    {
                        return ((Label)this.Master.FindControl("lblPageTitle")).Text;
                    }

                    return ((Label)Page.FindControl("lblPageTitle")).Text;
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    if (this.Master != null)
                    {
                        ((Label)this.Master.FindControl("lblPageTitle")).Text = value;
                    }
                    else
                    {
                        ((Label)Page.FindControl("lblPageTitle")).Text = value;
                    }
                }
                catch
                {

                }
            }
        }

        public GeneralPage()
        {
            Page.PreInit += new EventHandler(Page_PreInit);
            Page.PreLoad += new EventHandler(Page_PreLoad);
            Page.PreRender += new EventHandler(Page_PreRender);
        }

        /// <summary>
        /// چک کردن اتمام سشن و هدایت به صفحه ورود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.Url.AbsolutePath.ToLower() != "/login.aspx")
            {
                UserProfile profile = Session["UserProfile"] as UserProfile;
                if (profile.UserID == 0)
                {
                    Response.Redirect("~/Login.aspx", true);
                }
            }
        }

        /// <summary>
        /// گرفتن شناسه کاربر وارد شده
        /// </summary>
        protected long UserID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserID;
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// گرفتن نام کاربر لاگین کرده
        /// </summary>
        protected string UserFullName
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).FullName;
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// گرفتن کد استان کاربر جاری
        /// </summary>
        protected int? UserOstanCode
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).OstanCode;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// گرفتن کد منطقه کاربر جاری
        /// </summary>
        protected int? UserRegionCode
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).RegionCode;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// گرفتن کد مدرسه کاربر جاری
        /// </summary>
        protected int? UserSchoolCode
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).SchoolCode;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                //// اگر در کوئری استرینگ آیدی هیات بود، از آن استفاده میکنیم
                //if (Request.QueryString.Get("BoardID") != null)
                //{
                //    return Request.QueryString.Get("BoardID").StringToLong();
                //}

                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardID = value;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        protected byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        protected int? UserCurrentApplicationYear
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).CurrentApplicationYear;
                }
                catch
                {
                    return null;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).CurrentApplicationYear = value;
            }
        }

        /// <summary>
        /// استان هیات
        /// </summary>
        protected int? BoardOstanCode
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).BoardOstanCode;
                }
                catch
                {
                    return null;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).BoardOstanCode = value;
            }
        }

        /// <summary>
        /// منطقه هیات
        /// </summary>
        protected int? BoardRegionCode
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).BoardRegionCode;
                }
                catch
                {
                    return null;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).BoardRegionCode = value;
            }
        }

        /// <summary>
        /// بدست آوردن نوع کاربری
        /// </summary>
        /// <returns></returns>
        protected Enums.UserType GetUserType()
        {
            if (UserSchoolCode.HasValue)
            {
                if (UserSchoolCode.Value > 0)
                {// کاربر مدرسه است
                    return Enums.UserType.School;
                }
            }
            else if (UserRegionCode.HasValue)
            {
                if (UserRegionCode.Value > 0)
                {// کاربر منطقه است
                    return Enums.UserType.Region;
                }
            }
            else if (UserOstanCode.HasValue)
            {
                if (UserOstanCode.Value > 0)
                {// کاربر استان است
                    return Enums.UserType.State;
                }
            }

            // کاربر ستاد است
            return Enums.UserType.Setad;
        }

        /// <summary>
        /// بررسی سشن کاربر
        /// اینکه سشن جدیدا ایجاد شده است یا خیر
        /// ترو یعنی سشن جدید ایجاد شده است و اگر در حالت پست بک باشیم باید تابع چک پرمیژن فراخوانی شود
        /// </summary>
        protected bool UserSessionIsNew
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserSessionIsNew;
                }
                catch
                {
                    return false;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserSessionIsNew = value;
            }
        }

        /// <summary>
        /// گرفتن گروه های کاربری کاربر جاری
        /// </summary>
        protected List<DTO.Config.tblUserGroupMap.tblUserGroupMap> UserGroupIDs
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).lstGroupIDs;
                }
                catch
                {
                    return new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();
                }
            }
        }

        /// <summary>
        /// بررسی دسترسی کاربر جاری به گروه کاربری ورودی تابع، جهت اجازه دادن به کاربر برای اینکه لیست کاربران گروه موردنظر را ببیند یا خیر
        /// </summary>
        /// <param name="GID">آیدی گروه کاربری موردنظر</param>
        protected void CheckUserAccessToGroup(int GID)
        {
            if (UserGroupIDs.Where(g => g.UserGroupID == GID).FirstOrDefault() != null)
            {// اگر گروه درخواستی ، جزو گروه هایی هست که کاربر در آن ها عضو است، پس دسترسی دارد
                return;
            }

            bool Result = false;
            try
            {
                DTO.Config.tblUserGroupMap.tblUserGroupMap Obj = new DTO.Config.tblUserGroupMap.tblUserGroupMap();
                Obj.UserID = UserID;
                Obj.UserGroupID = GID;
                //Obj.AccessResult = false;
                Result = (new DAL.Config.tblUserGroupMap.Methods()).CheckUserAccessToGroup(Obj);
            }
            catch
            {

            }

            if (!Result)
            {
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "~", true);
            }
        }

        #region برای صفحه بندی گریدها

        /// <summary>
        ///میباشد گرید صفحه هر در نمایش قابل های ردیف تعداد کننده تعیین و میشود استفاده گرید نمایش برای پروپرتی این 
        ///</summary>
        protected int PageSize
        {
            get
            {
                if (this.Master != null)
                {
                    return (this.Master.FindControl("hfPageSize") as HiddenField).Value.StringToInt();
                }

                return (Page.FindControl("hfPageSize") as HiddenField).Value.StringToInt();
            }

            set
            {
                if (this.Master != null)
                {
                    (this.Master.FindControl("hfPageSize") as HiddenField).Value = value.ToString();
                }
                else
                {
                    (Page.FindControl("hfPageSize") as HiddenField).Value = value.ToString();
                }
            }
        }

        /// <summary>
        ///ها رکورد نمایش جهت گرید صفحات تعداد 
        ///</summary>
        protected int PageNumber
        {
            get
            {
                if (this.Master != null)
                {
                    return (this.Master.FindControl("hfPageNumber") as HiddenField).Value.StringToInt();
                }

                return (Page.FindControl("hfPageNumber") as HiddenField).Value.StringToInt();
            }

            set
            {
                if (this.Master != null)
                {
                    (this.Master.FindControl("hfPageNumber") as HiddenField).Value = value.ToString();
                }
                else
                {
                    (Page.FindControl("hfPageNumber") as HiddenField).Value = value.ToString();
                }
            }
        }

        /// <summary>
        /// تعداد کل سطرهای یک گرید، جهت صفحه بندی استفادی میشود
        /// </summary>
        protected long TotalRow
        {
            get
            {
                if (this.Master != null)
                {
                    return (this.Master.FindControl("hfTotalRow") as HiddenField).Value.StringToInt();
                }
                return (Page.FindControl("hfTotalRow") as HiddenField).Value.StringToInt();
            }

            set
            {
                if (this.Master != null)
                {
                    (this.Master.FindControl("hfTotalRow") as HiddenField).Value = value.ToString();
                }
                else
                {
                    (Page.FindControl("hfTotalRow") as HiddenField).Value = value.ToString();
                }
            }
        }

        /// <summary>
        ///است کرده تغییر گرید صفحه شماره آیا که میکند مشخص 
        ///<para>است رفته گرید از دیگری صفحه به کاربر عبارتی به </para>
        ///</summary>
        protected bool GridViewPageChanged
        {
            get
            {
                if (this.Master != null)
                {
                    return (this.Master.FindControl("hfPageChanged") as HiddenField).Value.StringToInt() > 0;
                }
                return (Page.FindControl("hfPageChanged") as HiddenField).Value.StringToInt() > 0;
            }
            set
            {
                if (this.Master != null)
                {
                    (this.Master.FindControl("hfPageChanged") as HiddenField).Value = (value) ? "1" : "0";
                }
                else
                {
                    (Page.FindControl("hfPageChanged") as HiddenField).Value = (value) ? "1" : "0";
                }
            }
        }

        #endregion برای صفحه بندی گریدها

        /// <summary>
        /// گرفتن اکشن کد صفحات
        /// اگر اکشن کد در کوئری استرینگ نبود، طبق نام صفحه، اکشن کد دیفالت آن را از دیتابیس میخونیم  وآن را برمیگردانیم
        /// </summary>
        /// <returns></returns>
        protected byte GetActionCode(string AspxPage)
        {
            if (Request.QueryString["action"] != null)
            {
                return Request.QueryString["action"].StringToByte();
            }
            else
            {// اگر اکشن کد در کوئری استرینگ نبود، اکشن کد پیش فرض صفحه را بدست میاوریم

                DTO.Config.tblPages.tblPages Obj = new DTO.Config.tblPages.tblPages();
                Obj.AspxPage = AspxPage;

                Obj = (new DAL.Config.tblPages.Methods()).SelectByAspxPage(Obj);

                if (Obj.RowNumber > 0)
                {// اگر اطلاعات اکشن کد پیش فرض وجود داشت برگردانده شود
                    return Obj.DefaultActionCode.Value;
                }

                // اگر اطلاعات اکشن کد پیش فرض برای صفحه نبود، اکشن کد نمایش صفحه را برمیگردانیم
                return (byte)Enums.Page_ActionCode.View;
            }
        }

        /// <summary>
        /// چک کردن اکشن کد موجود در کوئری استرینگ با اکشن کد ورودی تابع
        /// </summary>
        /// <param name="TargetAction">اکشن مورد انتظار</param>
        /// <returns>ترو یعنی، اکشن کد موجود در کوئری استرینگ، برابر با اکشن کد موردنظر ما در ورودی تابع است</returns>
        protected bool CheckActionCode(Enums.Page_ActionCode TargetAction)
        {
            if (Request.QueryString["action"] != null)
            {
                return ((byte)TargetAction == Request.QueryString["action"].StringToByte());
            }

            return false;
        }

        /// <summary>
        /// قبل از لود صفحه ، پیغام ها رو بررسی کرده و نمایش میدهیم
        /// سطوح دسترسی هم اینجا چک میشود 
        /// تا اگر سطح دسترسی رو نداشت، قبل از لود صفحه ، به صفحه مناسب هدایت شود
        /// </summary>
        void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // چک کردن سطح دسترسی
                string PageTitle = CheckPermission();
                // ست کردن عنوان صفحه
                ViewState.Add("PageTitle", PageTitle);

                // نمایش پیام ها
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessageAfterRedirect(this);
            }
            else
            {// حالت پست بک
                // اگر زمان سشن تمام شده باشد،پرمیژن چک شود
                if (UserSessionIsNew)
                {// در حالت پست بک و اینکه سشن زمانش تمام شده است، پرمیژن چک شود
                    CheckPermission();
                }
            }

            // ست کردن عنوان صفحه
            SetPageTitle();

            // نمایش اطلاعات کاربر
            ShowLoggedInUserInfo();
        }

        /// <summary>
        /// کدهای بعد از لود صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RenewSession();
            }

            // بعد از لود صفحه چک میشود که اگر متنی در لیبل عنوان صفحه ست نشده باشد، به صورت پیش فرض، عنوان صفحه را در آن نمایش دهد
            if (string.IsNullOrEmpty(PageLabelTitle))
            {
                PageLabelTitle = this.Header.Title;
            }

        }

        /// <summary>
        /// مجوز دسترسی کاربر به صفحات را چک مینماید
        /// این تابع قبل از لود صفحه باید بررسی شود که اگر کاربر دسترسی نداشت، اطلاعات صفحه بارگزاری نشود
        /// </summary>
        private string CheckPermission()
        {
            // در این خط در پروفایل کاربر، مقدار فلگ تازه بودن سشن را فالس میکنیم تا در پست بک ها، تابع چک پرمیژن فراخوانی نشود
            // مگر اینکه سشن از بین رفته باشد و مقدار این فلگ دوباره از فایل گلوبال ، برابر ترو شود
            UserSessionIsNew = false;
            if (!WebConfigurationManager.AppSettings.Get("CheckPermision").StringToBool())
            {
                return "عنوان صفحه";
            }

            string AspxPage = Request.Url.AbsolutePath;

            DTO.Config.tblPermissionMapView.tblPermissionMapView obj = new DTO.Config.tblPermissionMapView.tblPermissionMapView();
            string PageTitle = "";
            string Message = "";//پیام خطا جهت نمایش به کاربر
            string ReDirectURL = ""; // چون در ترای کچ ریدایرکت بشود خطا میدهد،آدرس رو میگیریم در بیرون ترای کچ ری دایرکت میکنیم
            try
            {
                obj.AspxPage = AspxPage.ToLower().Trim();

                #region با کاما جدا کردن آیدی تمام گروه های کاربری کاربر جاری

                obj.UserGroupIDs = "";

                if (UserGroupIDs != null)
                {
                    foreach (DTO.Config.tblUserGroupMap.tblUserGroupMap Item in UserGroupIDs)
                    {
                        obj.UserGroupIDs += Item.UserGroupID + ",";
                    }
                    if (obj.UserGroupIDs.Length > 1)
                    {// پاک کردن آخرین کاما
                        obj.UserGroupIDs = obj.UserGroupIDs.Substring(0, obj.UserGroupIDs.Length - 1);
                    }
                }

                #endregion

                obj.ActionCode = GetActionCode(obj.AspxPage);
                obj = (new DAL.Config.tblPermissionMapView.Methods()).SelectByAspxPage(obj);
                if (obj.RowNumber == 0)
                {// اگر مجوز دسترسی به آن را نداشته باشد

                    if (UserID == 0)
                    {// اگر کاربر لاگین نباشد
                        Message = (new Translation()).MessageContent(Translation.MessageContentType.MSG_YOU_MUST_LOGIN_TO_ACCESS_THIS_PAGE);
                        ReDirectURL = "~/Login.aspx";
                    }
                    else
                    {// اگر کاربر لاگین باشد
                        Message = (new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED);
                        ReDirectURL = "~";
                    }
                }
                else
                {
                    if (!obj.IsPublished.Value)
                    {// اگر صفحه پابلیش نشده باشد
                        Message = (new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_IS_NOT_PUBLISH);
                        ReDirectURL = "~";
                    }
                    else
                    {// مجوز دسترسی دارد و صفحه هم پابلیش شده است

                        #region بدست آوردن جزئیات صفحه و گرفتن عنوان آن

                        DTO.Config.tblPageDetail.tblPageDetail Target_Page = new DTO.Config.tblPageDetail.tblPageDetail();
                        Target_Page.PageID = obj.PageID;
                        Target_Page.ActionCode = obj.ActionCode;
                        Target_Page = (new DAL.Config.tblPageDetail.Methods()).SelectByPageID_ActionCode(Target_Page);
                        if (Target_Page.RowNumber > 0)
                        {// اگر اطلاعات صفحه وجود داشت
                            PageTitle = Target_Page.PageTitle;
                        }
                        else
                        {// اگر به ازای صفحه و اکشن کد، اطلاعاتی وجود نداشت
                            Message = (new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_NOT_EXIST);
                            ReDirectURL = "~";
                        }

                        #endregion بدست آوردن جزئیات صفحه و گرفتن عنوان آن
                    }
                }
            }
            catch
            {
                ReDirectURL = "/Error.html";
            }

            if (!string.IsNullOrEmpty(ReDirectURL))
            {// همیشه این تابع در اینجا با اند ریسپونز = ترو صدا زده شود تا از ادامه دادن کدها جلوگیری کند
                // خیلی خیلی مهم
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessage(Message, DialogMessage.SankaDialog.Message_Type.Warning, ReDirectURL, true);
            }

            return PageTitle;
        }

        /// <summary>
        /// تخصیص عنوان به صفحه
        /// </summary>
        /// <param name="title">عنوان</param>
        protected void SetPageTitle()
        {
            try
            {
                this.Title = ViewState["PageTitle"].ToString();
            }
            catch
            {

            }
        }

        /// <summary>
        /// متدی برای مدیریت جامع خطاهای برنامه
        /// </summary>
        /// <param name="ex"></param>
        protected void LogError(Exception ex, string ErrMessage = "خطایی رخ داده است.")
        {
            try
            {
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessage(ErrMessage, DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
            }
            catch
            {
            }
        }

        /// <summary>
        /// نمایش اطلاعات کاربر وارد شده
        /// </summary>
        protected void ShowLoggedInUserInfo()
        {
            if (UserID > 0)
            {// اگر کاربر لاگین باشد، اطلاعات نمایش داده شود
                try
                {
                    // نمایش دکمه خروج
                    ((ImageButton)this.Master.FindControl("ibtnLogOut")).Visible = true;

                    if (this.Master != null)
                    {
                        Label lblUserInfo = (Label)this.Master.FindControl("lblLoginInfo");

                        if (lblUserInfo == null)
                        {// اگر تو مستر پیج پیدا نشد، تو خود صفحه دنبال لیبل نمایش اطلاعات کاربر برگردد
                            lblUserInfo = (Label)Page.FindControl("lblLoginInfo");
                        }

                        lblUserInfo.Text = UserFullName  + getBoardInfo();

                        lblUserInfo.Attributes.Add("data-sankatooltip", lblUserInfo.Text);
                    }
                    else
                    {
                        ((Label)Page.FindControl("lblLoginInfo")).Text = UserFullName + " [ " + getBoardInfo() + " ]";
                    }
                }
                catch
                {

                }
            }
            else
            {// اگر کاربر لاگین نیست، دکمه ورود نمایش داده شود
                try
                {
                    System.Web.UI.HtmlControls.HtmlAnchor lnkLogin = (this.Master.FindControl("lnkLogin") as System.Web.UI.HtmlControls.HtmlAnchor);
                    lnkLogin.Visible = true;
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// استخراج برچسب نام هیات و نوع هیات
        /// </summary>
        /// <returns></returns>
        private string getBoardInfo()
        {
            var obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = this.UserBoardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0 && obj.BoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                return "[ هیات " + AdministrationViolation.Admin.Enums.Board_Type_Label[obj.BoardType]  + " " + obj.BoardTitle + " ]";
            }
            return string.Empty;
        }

        /// <summary>
        /// چک کردن اینکه کاربر جاری به هیات ورودی تابع دسترسی دارد یا نه
        /// </summary>
        /// <param name="BoardID">آیدی هیات</param>
        /// <param name="ReturnUrl">آدرس مقصد در صورتی که کاربر به هیات دسترسی نداشته باشد</param>
        /// <returns>ترو یعنی عضو هیات است و دسترسی دارد، فالس یعنی عضو هیات نیست ولی دسترسی دارد، اگر هم دسترسی نداشته باشد، کاربر را به آدرس ورودی تابع هدایت میکند</returns>
        protected bool CheckUserToAccessToBoard(long BoardID, string ReturnUrl)
        {
            if (BoardID < 1)
            {// اگر آیدی هئیت وجود نداشت، خطای عدم دسترسی میدهیم
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, ReturnUrl, true);
            }

            bool IsCurrentUserBoardMember = false;

            #region چک کردن اینکه کاربر جاری، عضو هیات جاری هست یا نه

            DTO.Board.tblBoardMemberView.tblBoardMemberView Obj = new DTO.Board.tblBoardMemberView.tblBoardMemberView();
            Obj.MemberUserID = UserID;
            if (this.UserBoardType != 3)
            {
                Obj.BoardID = BoardID;
            }
            Obj.BoardMemberMapStatus = (byte)AdministrationViolation.Admin.Enums.Board_Member_Status.Active;
            Obj.BoardStatus = (byte)AdministrationViolation.Admin.Enums.Board_Status.Active;

            bool IsBoardMember = false;

            try
            {
                // بررسی اینکه کاربر جاری برای هیاتی که آیدی اش در کوئری استرینگ است، آیا یک عضو فعال است یا خیر
                List<DTO.Board.tblBoardMemberView.tblBoardMemberView> RList = (new DAL.Board.tblBoardMemberView.Methods()).SelectByMemberUserID(Obj);

                IsBoardMember = RList.Any();

                // در اینجا اگر کاربر عضو هیات باشد، در این متغیر میریزیم تا در توابع دیگر از آن استفاده کنیم
                IsCurrentUserBoardMember = IsBoardMember;

                #region بررسی اینکه کاربر جاری،عضو گروه جاری باشد یا اینکه سرگروه گروه جاری باشد

                //CheckUserAccessToGroup(GID);

                #endregion بررسی اینکه کاربر جاری،عضو گروه جاری باشد یا اینکه سرگروه گروه جاری باشد

                if (!IsBoardMember)
                {// اگر کاربر جاری عضو هیات نبود، چک میکنیم که هیات جاری در محدوده کاربر هست یا نه
                    DTO.Board.tblBoard.tblBoard ObjBoard = new DTO.Board.tblBoard.tblBoard();
                    ObjBoard.UserID = UserID;
                    ObjBoard.BoardID = BoardID;
                    IsBoardMember = (new DAL.Board.tblBoard.Methods()).CheckUserAccess(ObjBoard);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            if (!IsBoardMember)
            {
                DialogMessage.SankaDialog MyMessage = new DialogMessage.SankaDialog();
                MyMessage.MessageLocationID = "divMessage";
                MyMessage.ID = "MessageDialog";
                MyMessage.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, ReturnUrl, true);
            }

            #endregion چک کردن اینکه کاربر جاری، عضو هیات جاری هست یا نه

            return IsCurrentUserBoardMember;
        }

        /// <summary>
        /// ذخیره لیست متتو اخطار جهت نمایش توسط متد مربوط به نمایش آنها
        /// </summary>
        /// <param name="lstMessage"></param>
        /// <returns></returns>
        protected string GetWarrningList(List<string> lstMessage)
        {
            string strMessage = "";
            for (int i = 0; i < lstMessage.Count; i++)
            {
                if (i == 0)
                {
                    strMessage += lstMessage[i];
                    continue;
                }
                strMessage += "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp" + lstMessage[i];
            }
            return strMessage;
        }

        /// <summary>
        /// تاریخ انقضاء وضعیت آنلاین را از نو براب  مقدار انقضای سشن در وب کانفیگ میکند
        /// </summary>
        private void RenewSession()
        {
            UserProfile UProfileObj = Session["UserProfile"] as UserProfile;
            if (UProfileObj != null && UProfileObj.UserID > 0)
            {
                var obj = new DTO.Config.tblUser.tblUser
                {
                    UserID = this.UserID,
                    SessionExpiration = DateTime.Now.AddMinutes(UProfileObj.SessionTimeOut)
                };
                bool bln = (new DAL.Config.tblUser.Methods()).RenewExpirationTime(obj);
            }
        }

        /// <summary>
        /// فلگ آنلاین را برابر 0 میکند .
        /// </summary>
        public void Logout()
        {
            // Login 1 , Logout = 2
            (new Logger()).InsertLoginLogoutLog(this.UserID, this.UserBoardID, Logger.LoginFlag.Logout);

            var objUser = new DTO.Config.tblUser.tblUser { UserID = this.UserID };
            (new DAL.Config.tblUser.Methods()).Logout(objUser);
        }
    }
}
