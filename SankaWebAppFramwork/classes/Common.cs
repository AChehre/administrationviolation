﻿
using System.Web.UI.WebControls;
using System.Web;
using System;
using System.Web.Configuration;
using System.Web.UI;

namespace SankaWebAppFramework
{
    public class Common
    {
        /// <summary>
        /// آیدی گروه کاربری عمومی یا پابلیک
        /// </summary>
        public static int PublicGroupID
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// پرکردن دراپ دان گروه های کاربری
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست گروه ها را نمایش دهد</param>
        public static void BindUserGroup(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Config.tblUserGroup.Methods()).SelectAll();
            ddl.DataTextField = "GroupName";
            ddl.DataValueField = "GroupID";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب گروه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان لیست صفحات
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست صفحه ها را نمایش دهد</param>
        public static void BindPages(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Config.tblPages.Methods()).SelectAll();
            ddl.DataTextField = "AspxPage";
            ddl.DataValueField = "PageID";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("انتخاب صفحه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان لیست صفحات
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست صفحاتی که قابل دسترس هستند را نمایش دهد</param>
        public static void BindPublishedPages(DropDownList ddl)
        {
            DTO.Config.tblPages.tblPages Obj = new DTO.Config.tblPages.tblPages();
            Obj.IsPublished = true;
            ddl.DataSource = (new DAL.Config.tblPages.Methods()).SelectAll_Published(Obj);
            ddl.DataTextField = "AspxPage";
            ddl.DataValueField = "PageID";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("انتخاب صفحه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان لیست منوها
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست منو ها را نمایش دهد</param>
        public static void BindMenus(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Config.tblMenu.Methods()).SelectAll();
            ddl.DataTextField = "Title";
            ddl.DataValueField = "MenuID";
            ddl.DataBind();

            ddl.Items.Insert(0, new ListItem("انتخاب منو", "-1"));
        }

        /// <summary>
        /// پر کردن دراپ دان اکشن های یک صفحه
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست اکشن ها را نمایش دهد</param>
        public static void BindActions(DropDownList ddl)
        {
            for (int i = 0; i < Enums.Page_ActionCode_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.Page_ActionCode_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب اکشن", "-1"));
        }

        /// <summary>
        /// بایند کردن کمبو انواع راهنما
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindHelpTypes(DropDownList ddl , byte boardType)
        {
            DTO.Config.tblRequest.tblRequest obj = new DTO.Config.tblRequest.tblRequest { BoardType = boardType , Enabled = null };
            ddl.DataSource = (new DAL.Config.tblRequest.Methods()).SelectAll(obj);
            ddl.DataTextField = "Title";
            ddl.DataValueField = "RequestID";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب راهنما", "-1"));
        }

        /// <summary>
        /// بایند کردن دراپ داون وضعیت کاربر
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست وضعیت ها را نمایش دهد</param>
        public static void BindUserActive(DropDownList ddl)
        {
            for (int i = 0; i < Enums.User_Active_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.User_Active_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب وضعیت", "-1"));
        }

        /// <summary>
        /// بایند کردن دراپ دوان وضعیت دسترسی
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست وضعیت ها را نمایش دهد</param>
        public static void BindPublishStatus(DropDownList ddl)
        {
            for (int i = 0; i < Enums.PublishStatus_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.PublishStatus_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب کنید", "-1"));
        }

        /// <summary>
        /// پر کردن کمبوباکس نوع لینک منوها
        /// </summary>
        /// <param name="ddl">کمبوباکس مورد نظر</param>
        public static void BindMenuLinkType(DropDownList ddl)
        {
            for (int i = 1; i < Enums.Menu_LinkType_Label.Length; i++)
            {
                ListItem item = new ListItem(Enums.Menu_LinkType_Label[i], i.ToString(), true);
                ddl.Items.Add(item);
            }

            ddl.Items.Insert(0, new ListItem("انتخاب کنید", "-1"));
        }

        /// <summary>
        /// بررسی وجود نداشتن نام کاربری 
        /// </summary>
        /// <param name="UserName">نام کاربری مورد نظر</param>
        /// <returns>اگر نام کاربری وجود نداشته باشد، ترو بر می گرداند</returns>
        public static bool CheckUserName(string UserName)
        {

            DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();
            ObjUser.UserName = UserName;
            ObjUser = (new DAL.Config.tblUser.Methods()).SelectByUserName(ObjUser);

            if (ObjUser.RowNumber > 0)
            {// نام کاربری وجود دارد
                return false;
            }
            else
            {// نام کاربری وجود ندارد
                return true;
            }
        }

        /// <summary>
        /// بررسی وجود نداشتن نام کاربری به غیر از کاربر ورودی تابع
        /// </summary>
        /// <param name="UserName">نام کاربری مورد نظر</param>
        /// <param name="UserID">آیدی کاربر مورد نظر</param>
        /// <returns>اگر نام کاربری وجود نداشته باشد، ترو بر می گرداند</returns>
        public static bool CheckUserName(string UserName, long UserID)
        {

            DTO.Config.tblUser.tblUser ObjUser = new DTO.Config.tblUser.tblUser();
            ObjUser.UserName = UserName;
            ObjUser.UserID = UserID;
            ObjUser = (new DAL.Config.tblUser.Methods()).SelectByUserName(ObjUser);

            if (ObjUser.RowNumber > 0)
            {// نام کاربری وجود دارد
                if (ObjUser.UserID == UserID)
                {// نام کاربری ، متعلق به کاربر ورودی تابع باشد
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {// نام کاربری وجود ندارد
                return true;
            }
        }

        /// <summary>
        /// پرکردن دراپ دان استان ها
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست استان ها را نمایش دهد</param>
        public static void BindStates(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Config.tblOstans.Methods()).SelectAll();
            ddl.DataTextField = "Name";
            ddl.DataValueField = "OstanCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب استان", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان مناطق
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست منطقه ها را نمایش دهد</param>
        public static void BindRegion(DropDownList ddl, int OstanCode)
        {
            DTO.Config.tblRegions.tblRegions obj = new DTO.Config.tblRegions.tblRegions();
            obj.OstanCode = OstanCode;
            ddl.DataSource = (new DAL.Config.tblRegions.Methods()).SelectAll(obj);
            ddl.DataTextField = "Name";
            ddl.DataValueField = "RegionCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب منطقه", "-1"));
        }

        /// <summary>
        /// پرکردن دراپ دان مدارس
        /// </summary>
        /// <param name="ddl">دراپ دانی که میخواهیم لیست مدرسه ها را نمایش دهد</param>
        public static void BindSchool(DropDownList ddl, int RegionCode)
        {
            DTO.Config.tblSchool.tblSchool obj = new DTO.Config.tblSchool.tblSchool();
            obj.RegionCode = RegionCode;
            ddl.DataSource = (new DAL.Config.tblSchool.Methods()).SelectAll(obj);
            ddl.DataTextField = "SchoolName";
            ddl.DataValueField = "SchoolCode";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب مدرسه", "-1"));
        }

        /// <summary>
        /// بدست آوردن آی پی کاربر
        /// </summary>
        public static string UserIP()
        {
            string ip = "";

            try
            {
                ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            catch
            {

            }
            return ip;
        }

        /// <summary>
        /// پاک کردن کوکی نگهدارنده آدرس های بازگشت
        /// </summary>
        public static void RemoveReturnUrlCookie()
        {
            if (HttpContext.Current.Request.Cookies["returnurl"] != null)
            {
                HttpContext.Current.Request.Cookies["returnurl"].Expires = DateTime.Now.AddDays(-5);
            }
        }
    }
}
