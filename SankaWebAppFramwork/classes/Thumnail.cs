﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing.Imaging;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SankaWebAppFramework
{
    public class Thumnail
    {
        /// <summary>
        /// متدی برای کوچک کردن تصویر
        /// </summary>
        /// <param name="strm">استریم حاوب فایل تصویر</param>
        /// <returns>شی تصویر</returns>
        public static System.Drawing.Image GetImage(Stream strm , int width , int height)
        {
            System.Drawing.Image image = System.Drawing.Image.FromStream(strm);
            System.Drawing.Image thumbnailImage = image.GetThumbnailImage(width, height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
            MemoryStream imageStream = new MemoryStream();
            thumbnailImage.Save(imageStream, ImageFormat.Jpeg);
            byte[] imageContent = new Byte[imageStream.Length];
            imageStream.Position = 0;
            imageStream.Read(imageContent, 0, (int)imageStream.Length);
            return System.Drawing.Image.FromStream(imageStream);
        }

        /// <summary>
        /// رند کردن گوشه های تصویر
        /// </summary>
        /// <param name="StartImage">شی تصویر</param>
        /// <param name="CornerRadius">میزان خمش کناره های تصویر</param>
        /// <param name="BackgroundColor">رنگ پس زمینه</param>
        /// <returns></returns>
        public static System.Drawing.Image RoundCorners(System.Drawing.Image StartImage, int CornerRadius, Color BackgroundColor)
        {
            CornerRadius *= 2;
            Bitmap RoundedImage = new Bitmap(StartImage.Width, StartImage.Height);
            Graphics g = Graphics.FromImage(RoundedImage);
            g.Clear(BackgroundColor);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Brush brush = new TextureBrush(StartImage);
            GraphicsPath gp = new GraphicsPath();
            gp.AddArc(0, 0, CornerRadius, CornerRadius, 180, 90);
            gp.AddArc(0 + RoundedImage.Width - CornerRadius, 0, CornerRadius, CornerRadius, 270, 90);
            gp.AddArc(0 + RoundedImage.Width - CornerRadius, 0 + RoundedImage.Height - CornerRadius, CornerRadius, CornerRadius, 0, 90);
            gp.AddArc(0, 0 + RoundedImage.Height - CornerRadius, CornerRadius, CornerRadius, 90, 90);
            g.FillPath(brush, gp);
            return RoundedImage;
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }
    }
}