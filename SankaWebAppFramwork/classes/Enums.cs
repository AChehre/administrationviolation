﻿
namespace SankaWebAppFramework
{
    public static class Enums
    {
        /// <summary>
        /// تعداد لینک هایی که برای صفحه بندی میخواهیم در پایین گرید نمایش دهیم
        /// </summary>
        public enum PageLinkNumber
        {
            TenNumber = 10
        }

        /// <summary>
        /// اکشن کدهای مختلف برای صفحات
        /// </summary>
        public enum Page_ActionCode
        {
            View = 0,
            Insert = 1,
            Edit = 2,
            Delete = 3
        }

        /// <summary>
        /// انواع مختلف منو
        /// </summary>
        public enum Menu_Type
        {
            Internal = 1,// مقصد لینک داخل سایت
            External = 2,// مقصد لینک خارج سایت
        }

        /// <summary>
        /// عنوان اکشن کدهای مختلف
        /// </summary>
        public static string[] Page_ActionCode_Label =
        {
            "مشاهده",
            "ثبت رکورد جدید",
            "ویرایش",
            "حذف"
        };

        public static string[] Menu_LinkType_Label =
        {
            "",
            "داخلی",
            "خارجی",
        };

        /// <summary>
        /// وضعیت انتشار یک صفحه
        /// </summary>
        public enum PublishStatus
        {
            /// <summary>
            /// پابلیش نشده
            /// </summary>
            IsNotPublished = 0,

            /// <summary>
            /// پابلیش شده
            /// </summary>
            Published = 1,
        }

        /// <summary>
        /// عنوان وضعیت دسترسی صفحات یا منو ها
        /// </summary>
        public static string[] PublishStatus_Label =
        {
            "غیر قابل دسترس",
            "قابل دسترس"
        };

        /// <summary>
        /// مشخص کننده نوع کاربر
        /// مدرسه، منطقه،استان یا ستاد
        /// </summary>
        public enum UserType
        {
            /// <summary>
            /// مدرسه
            /// </summary>
            School = 0,
            /// <summary>
            /// منطقه
            /// </summary>
            Region = 1,
            /// <summary>
            /// استان
            /// </summary>
            State = 2,
            /// <summary>
            /// ستاد
            /// </summary>
            Setad = 3,
        }

        public static string[] UserType_Label = 
        {
            "مدرسه","منطقه","استان","ستاد"
        };

        /// <summary>
        /// وضعیت کاربران
        /// </summary>
        public enum User_Active
        {
            /// <summary>
            /// غیرفعال
            /// </summary>
            DeActive = 0,

            /// <summary>
            /// فعال
            /// </summary>
            Active = 1,
        }

        public static string[] User_Active_Label = 
        {
            "غیرفعال","فعال"
        };
    }
}
