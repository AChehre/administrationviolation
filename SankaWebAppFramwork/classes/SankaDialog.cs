﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.Web.UI;
using System.ComponentModel;

namespace DialogMessage
{
    public class SankaDialog : Panel
    {
        public string MessageLocationID { get; set; }

        /// <summary>
        /// رفتن به آدرس ورودی
        /// </summary>
        /// <param name="url">آدرس صفحه مقصد</param>
        /// <param name="EndResponse">وضعیت فلاش شدن، اگر فالس باشد، یعنی کدهای بعد از فراخوانی این تابع اجرا شود، اگر ترو باشد، یعنی کدهای بعد از این تابع اجرا نشود</param>
        protected void CustomRedirect(string url, bool EndResponse = false)
        {
            HttpContext.Current.Response.Redirect(url, EndResponse);
        }

        /// <summary>
        /// ساخت قالب نمایشی پیام
        /// </summary>
        /// <param name="Message">پیام</param>
        /// <param name="MessageType">نحوه ی نمایش پیام</param>
        /// <returns>متن پیام به همراه قالب نمایشی</returns>
        protected string CreateMessageFormat(string Message, Message_Type MessageType)
        {
            if (MessageType == Message_Type.Info)
            {
                Message = string.Format("<div class=\"info close\"><span data-icon=\"K\"></span>{0}</div>", Message);
            }
            else if (MessageType == Message_Type.Message)
            {
                Message = string.Format("<div class=\"message close\"><span data-icon=\"E\"></span>{0}</div>", Message);
            }
            else if (MessageType == Message_Type.Warning)
            {
                Message = string.Format("<div class=\"warning close\"><span data-icon=\"C\"></span>{0}</div>", Message);
            }
            else if (MessageType == Message_Type.Error)
            {
                Message = string.Format("<div class=\"error close\"><span data-icon=\"u\">{0}</div>", Message);
            }

            return Message;
        }

        /// <summary>
        /// نمایش یک متن به کاربران
        /// </summary>
        /// <param name="Message">متنی که باید نمایش داده شود</param>
        /// <param name="MessageType">نوع متن، مثلا متن خطا،اخطار و غیره ، از اینام استفاده شود</param>
        /// <param name="_master">مستر پیجی که قرار است پیغام در آن نمایش داده شود</param>
        /// <param name="ShowInDialog">اگر ترو باشد، پیام در دیالوگ به کاربر نمایش داده میشود،در غیر اینصورت در یک دیو نمایش داده میشود</param>
        /// <param name="_page">صفحه ای که این متد در آن فراخوانی میشود</param>
        /// <param name="OnCloseFunction">نام تابعی که میخواهیم بعد از بستن دیالوگ در سمت جاوااسکریپت فراخوانی شود، نام تابع بدون پرانتز وارد شود</param>
        public void ShowMessage(string Message, Message_Type MessageType, Page _page, bool ShowInDialog = false, string OnCloseFunction = "", string CloseFunctionArg = "")
        {
            #region ساخت قالب پیام با توجه به نوع نمایش پیام

            Message = CreateMessageFormat(Message, MessageType);

            #endregion ساخت قالب پیام با توجه به نوع نمایش پیام

            if (ShowInDialog)
            {
                #region نمایش پیام در دیالوگ

                if (!string.IsNullOrEmpty(OnCloseFunction))
                {
                    OnCloseFunction = "," + OnCloseFunction;
                }

                if (!string.IsNullOrEmpty(CloseFunctionArg))
                {
                    CloseFunctionArg = ",'" + CloseFunctionArg + "'";
                }

                _page.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "ShowPopup('" + Message + "','" + Message_Type_Label[(byte)MessageType] + "','" + this.ClientID + "'" + OnCloseFunction + CloseFunctionArg + ");", true);

                #endregion نمایش پیام در دیالوگ
            }
            else
            {
                #region نمایش پیام در دیو

                System.Web.UI.HtmlControls.HtmlContainerControl divMessage = (_page.FindControl(MessageLocationID)
                            as System.Web.UI.HtmlControls.HtmlContainerControl);

                if (divMessage == null && !string.IsNullOrEmpty(_page.MasterPageFile))
                {// اگر دیو را در صفحه پیدا نکرد، و صفحه مسترپیج داشته باشد در مستر پیج صفحه دنبال آن بگردد
                    divMessage = (_page.Master.FindControl(MessageLocationID)
                            as System.Web.UI.HtmlControls.HtmlContainerControl);
                }

                if (divMessage != null)
                {
                    divMessage.InnerHtml = Message;
                    divMessage.EnableViewState = false;
                    divMessage.Visible = true;
                }


                #endregion نمایش پیام در دیو
            }
        }

        /// <summary>
        /// نمایش یک متن به کاربران
        /// </summary>
        /// <param name="Message">متنی که باید نمایش داده شود</param>
        /// <param name="MessageType">نوع متن، مثلا متن خطا،اخطار و غیره ، از اینام استفاده شود</param>
        /// <param name="_master">مستر پیجی که قرار است پیغام در آن نمایش داده شود</param>
        /// <param name="RedirectUrl">آدرس مقصد</param>
        /// <param name="EndResponse">اگر ترو باشد، کدهایی که بعد از فراخوانی این تابع باشند، اجرا نمیشوند</param>
        /// <param name="ShowInDialog">اگر ترو باشد، پیام در دیالوگ به کاربر نمایش داده میشود،در غیر اینصورت در یک دیو نمایش داده میشود</param>
        public void ShowMessage(string Message, Message_Type MessageType, string RedirectUrl, bool EndResponse = false, bool ShowInDialog = false)
        {
            #region ذخیره پیام در سشن و هدایت کاربر به صفحه مورد نظر

            //ذخیره پیام در سشن
            HttpContext.Current.Session.Add(MessageType.ToString(), Message);

            // ذخیره حالت نمایش در سشن، اینکه در دیالوگ نمایش داده شود یا دیو
            HttpContext.Current.Session.Add(MessageType.ToString() + "_ShowInDialog", ShowInDialog);

            CustomRedirect(RedirectUrl, EndResponse);

            #endregion ذخیره پیام در سشن و هدایت کاربر به صفحه مورد نظر
        }

        /// <summary>
        /// نمایش پیام هایی که بعد از ریدایرکت باید نمایش داده شوند
        /// <param name="_master">مستر پیجی که قرار است پیام در آن نمایش داده شود</param>
        /// </summary>
        public void ShowMessageAfterRedirect(Page _page)
        {
            // متن خطایی که داخل سشن بار شده را نمایش میدهد
            if (HttpContext.Current.Session[Message_Type.Error.ToString()] != null)
            {
                ShowMessage(HttpContext.Current.Session[Message_Type.Error.ToString()].ToString(), Message_Type.Error, _page, Convert.ToBoolean(HttpContext.Current.Session[Message_Type.Error.ToString() + "_ShowInDialog"]));
                HttpContext.Current.Session.Remove(Message_Type.Error.ToString());
            }
            // متن پیامی که داخل سشن بار شده را نمایش میدهد
            else if (HttpContext.Current.Session[Message_Type.Message.ToString()] != null)
            {
                ShowMessage(HttpContext.Current.Session[Message_Type.Message.ToString()].ToString(), Message_Type.Message, _page, Convert.ToBoolean(HttpContext.Current.Session[Message_Type.Message.ToString() + "_ShowInDialog"]));
                HttpContext.Current.Session.Remove(Message_Type.Message.ToString());
            }
            // متن اخطاری که داخل سشن بار شده را نمایش میدهد
            else if (HttpContext.Current.Session[Message_Type.Warning.ToString()] != null)
            {
                ShowMessage(HttpContext.Current.Session[Message_Type.Warning.ToString()].ToString(), Message_Type.Warning, _page, Convert.ToBoolean(HttpContext.Current.Session[Message_Type.Warning.ToString() + "_ShowInDialog"]));
                HttpContext.Current.Session.Remove(Message_Type.Warning.ToString());
            }
            else if (HttpContext.Current.Session[Message_Type.Info.ToString()] != null)
            {
                ShowMessage(HttpContext.Current.Session[Message_Type.Info.ToString()].ToString(), Message_Type.Info, _page, Convert.ToBoolean(HttpContext.Current.Session[Message_Type.Info.ToString() + "_ShowInDialog"]));
                HttpContext.Current.Session.Remove(Message_Type.Info.ToString());
            }
        }

        /// <summary>
        /// جهت مشخص کردن نوع پیام هایی که به کاربران نمایش میدهیم
        /// </summary>
        public enum Message_Type
        {
            Info = 0,// اطلاع رسانی
            Message = 1,// پیام
            Warning = 2,// اخطار
            Error = 3// خطا
        }

        /// <summary>
        /// عنوان انواع پیام های نمایشی به کاربر
        /// </summary>
        public string[] Message_Type_Label =
        {
            "اطلاع رسانی",
            "پیام",
            "اخطار",
            "خطا"
        };

        /// <summary>
        ///قبل از رندر شدن کنترل فایل های موردنیاز را به صفحه اضافه میکنیم
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // آیدی پنل دیالوگ را از کلاس مسیج میگیریم، 
                //چون در جاوااسکرپیت همان آیدی پاس میشود برای اینکه به صورت دیالوگ نمایش داده شود

                #region ساخت دیویژنی که باید پیام در آن نمایش داده شود

                //System.Web.UI.HtmlControls.HtmlContainerControl divMessage = (Page.FindControl(MessageLocationID)
                //            as System.Web.UI.HtmlControls.HtmlContainerControl);
                //divMessage.InnerHtml = Message;
                //divMessage.EnableViewState = false;
                //divMessage.Visible = true;

                #endregion

                #region کامنت

                // اضافه کردن فایل سی اس اس مورد نیاز
                //Literal TargetFile = new Literal();
                //TargetFile.Text = @"<link href=""" + Page.ClientScript.GetWebResourceUrl(this.GetType(), "Message.jquery-ui.min.css") + @""" type=""text/css"" rel=""stylesheet"" />";
                //Page.Header.Controls.Add(TargetFile);

                // اضافه کردن فایل جاوااسکریپت
                //TargetFile = new Literal();
                //TargetFile.Text = @"<script type='text/javascript' src=" + Page.ClientScript.GetWebResourceUrl(this.GetType(), "Message.jquery-1.11.0.min.js") + "></script>";
                //Page.Header.Controls.Add(TargetFile);
                ////**
                //TargetFile = new Literal();
                //TargetFile.Text = @"<script type='text/javascript' src=" + Page.ClientScript.GetWebResourceUrl(this.GetType(), "Message.jquery-ui.min.js") + "></script>";
                //Page.Header.Controls.Add(TargetFile);
                ////**

                #endregion کامنت

                Literal TargetFile = new Literal();
                TargetFile.Text = @"<script type='text/javascript' src='/js/Message.js'></script>";
                Page.Header.Controls.Add(TargetFile);
            }
            catch
            {

            }
            base.OnPreRender(e);
        }

        // saeed

        /// <summary>
        /// نمایش یک متن به کاربران
        /// </summary>
        /// <param name="Message">متنی که باید نمایش داده شود</param>
        /// <param name="MessageType">نوع متن، مثلا متن خطا،اخطار و غیره ، از اینام استفاده شود</param>
        /// <param name="_master">مستر پیجی که قرار است پیغام در آن نمایش داده شود</param>
        /// <param name="ShowInDialog">اگر ترو باشد، پیام در دیالوگ به کاربر نمایش داده میشود،در غیر اینصورت در یک دیو نمایش داده میشود</param>
        /// <param name="_page">صفحه ای که این متد در آن فراخوانی میشود</param>
        /// <param name="OnCloseFunction">نام تابعی که میخواهیم بعد از بستن دیالوگ در سمت جاوااسکریپت فراخوانی شود، نام تابع بدون پرانتز وارد شود</param>
        public void ShowMessageYesNo(string Message, Message_Type MessageType, Page _page, bool ShowInDialog = false, string YesFunction = "")
        {
            #region ساخت قالب پیام با توجه به نوع نمایش پیام

            Message = CreateMessageFormat(Message, MessageType);

            #endregion ساخت قالب پیام با توجه به نوع نمایش پیام

            if (ShowInDialog)
            {
                #region نمایش پیام در دیالوگ

                if (!string.IsNullOrEmpty(YesFunction))
                {
                    YesFunction = "," + YesFunction;
                }

                _page.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "ShowPopupForConfirm('" + Message + "','" + Message_Type_Label[(byte)MessageType] + "','" + this.ClientID + "'" + YesFunction + ");", true);

                #endregion نمایش پیام در دیالوگ
            }
            else
            {
                #region نمایش پیام در دیو

                System.Web.UI.HtmlControls.HtmlContainerControl divMessage = (_page.FindControl(MessageLocationID)
                            as System.Web.UI.HtmlControls.HtmlContainerControl);

                if (divMessage == null && !string.IsNullOrEmpty(_page.MasterPageFile))
                {// اگر دیو را در صفحه پیدا نکرد، و صفحه مسترپیج داشته باشد در مستر پیج صفحه دنبال آن بگردد
                    divMessage = (_page.Master.FindControl(MessageLocationID)
                            as System.Web.UI.HtmlControls.HtmlContainerControl);
                }

                if (divMessage != null)
                {
                    divMessage.InnerHtml = Message;
                    divMessage.EnableViewState = false;
                    divMessage.Visible = true;
                }


                #endregion نمایش پیام در دیو
            }
        }
    }
}
