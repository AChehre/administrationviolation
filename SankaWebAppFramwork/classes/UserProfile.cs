﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SankaWebAppFramework
{
    /// <summary>
    /// کلاسی حاوی اطلاعات کاربر وارد شده به سیستم
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// آیدی کاربر
        /// </summary>
        public long UserID { set; get; }

        /// <summary>
        /// کد استان
        /// </summary>
        public int? OstanCode { set; get; }
        /// <summary>
        /// کد منطقه
        /// </summary>
        public int? RegionCode { set; get; }
        /// <summary>
        /// کد مدرسه
        /// </summary>
        public int? SchoolCode { set; get; }

        /// <summary>
        /// نام کامل کاربر
        /// </summary>
        public string FullName { set; get; }

        /// <summary>
        /// لیست آیدی گروه های کاربری ، کاربر
        /// </summary>
        public List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lstGroupIDs { set; get; }

        /// <summary>
        /// اگر ترو باشد، یعنی زمان یوزر تمام شده است و تازه سشن ست شده است
        /// در چک کردن پرمیژن صفحات در زمان پست بک ها بکار می آید
        /// </summary>
        public bool UserSessionIsNew { set; get; }

        /// <summary>
        /// سالی عملیاتی کاربر جاری
        /// </summary>
        public int? CurrentApplicationYear { set; get; }

        /// <summary>
        /// آیدی هیاتی که کاربر جاری در آن عضو است
        /// </summary>
        public long UserBoardID { set; get; }

        /// <summary>
        /// نوع هیاتی که کاربر عضو آن است . بدوی / تجدید نظر و ...
        /// </summary>
        public byte UserBoardType { set; get; }

        /// <summary>
        /// آدرس صفحه ای که پس از لاگین به آنجا هدایت میشود
        /// </summary>
        public string DefaultPageUrl { set; get; }

        /// <summary>
        /// وضعیت آنلاین یا آفلاین
        /// </summary>
        public bool Online { set; get; }
        /// <summary>
        /// تاریخ انقضای وضعیت آنلاین
        /// </summary>
        public System.DateTime? SessionExpiration { set; get; }
        /// <summary>
        /// مدت زمان تا انقضای سشن
        /// </summary>
        public int SessionTimeOut { set; get; }
        /// <summary>
        ///وضعیت اکانت کاربر فعال است
        /// </summary>
        public bool? IsActive { set; get; }

        /// <summary>
        /// استان هیات
        /// </summary>
        public int? BoardOstanCode { set; get; }
        /// <summary>
        /// منطقه هیات
        /// </summary>
        public int? BoardRegionCode { set; get; }
    }
}