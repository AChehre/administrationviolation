﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SankaWebAppFramework
{
    public sealed class Logger
    {
        public enum LoginFlag
        {
            Login = 1, //  ورود
            Logout = 2 // خروج
        }

        public enum RecardModificationFlag
        {
            INSERT = 1, //  جدید
            UPDATE = 2, // ویرایش
            DELETE = 3, // حذف
        }

        /// <summary>
        /// ثبت لاگ ورود و خروج کاربر
        /// </summary>
        /// <param name="userID">شناسه کاربر</param>
        /// <param name="boardID">شناسه هیات</param>
        /// <param name="loginFlag">نوع فلگ ، ورود یا خروج</param>
        public void InsertLoginLogoutLog(long userID, long boardID, LoginFlag loginFlag)
        {
            var obj = new DTO.Logger.tblUserLoginLog.tblUserLoginLog
            {
                UserID = userID,
                BoardID = boardID,
                EventDate = DateTime.Now,
                LoginFlag = (byte)loginFlag,
                IPAddress = Utility.getUserIP(),
                BrowserAgent = Utility.getBroaserAgent()
            };
            (new DAL.Logger.tblUserLoginLog.Methods()).Insert(obj);
        }

        /// <summary>
        /// ثبت لاگ تغییر کاربران اعم از ایجاد ، ویرایش و حذف
        /// </summary>
        /// <param name="userID">شناسه کاربر</param>
        /// <param name="boardID">شناسه هیات</param>
        /// <param name="targetUserID">شناسه کاربر مورد بروزرسانی</param>
        /// <param name="flag">فلگ نوع برورزسانی</param>
        public void InsertUserModificationLog(long targetUserID, RecardModificationFlag flag)
        {
            var obj = new DTO.Logger.tblUserModificationLog.tblUserModificationLog
            {
                UserID = this.UserID,
                BoardID = this.UserBoardID,
                TargetUserID = targetUserID,
                ActionFlag = (byte)flag,
                EventDate = DateTime.Now,
                IPAddress = Utility.getUserIP(),
                BrowserAgent = Utility.getBroaserAgent()
            };
            (new DAL.Logger.tblUserModificationLog.Methods()).Insert(obj);
        }

        /// <summary>
        /// ثبت لاگ تغییرات روی جدول درخواست های ثبت شده
        /// </summary>
        /// <param name="userID">شناسه کاربر</param>
        /// <param name="requestDataID">شناسه درخواست ثبت شده</param>
        /// <param name="targetUserID">شناسه کاربر مورد بروزرسانی</param>
        /// <param name="flag">فلگ نوع برورزسانی</param>
        public void InsertRequestDataModificationLog(long requestDataID, RecardModificationFlag flag)
        {
            var obj = new DTO.Logger.tblUserRequestDataLog.tblUserRequestDataLog
            {
                UserID = this.UserID,
                BoardID = this.UserBoardID,
                RequestDataID = requestDataID,
                ActionFlag = (byte)flag,
                EventDate = DateTime.Now,
                IPAddress = Utility.getUserIP(),
                BrowserAgent = Utility.getBroaserAgent()
            };
            (new DAL.Logger.tblUserRequestDataLog.Methods()).Insert(obj);
        }

        /// <summary>
        /// ثبت لاگ خارج کردنفایل از وضعیت قفل
        /// </summary>
        /// <param name="FleID">akhsi thdg</param>
        public void InsertFileUnlockHistoryLog(long fileID)
        {
            var obj = new DTO.Logger.tblFileUnlockHistoryLog.tblFileUnlockHistoryLog
            {
                UserID = this.UserID,
                BoardID = this.UserBoardID,
                TargetFileID = fileID,
                EventDate = DateTime.Now,
                IPAddress = Utility.getUserIP(),
                BrowserAgent = Utility.getBroaserAgent()
            };
            (new DAL.Logger.tblFileUnlockHistoryLog.Methods()).Insert(obj);
        }

        /// <summary>
        /// گرفتن شناسه کاربر وارد شده
        /// </summary>
        private long UserID
        {
            get
            {
                try
                {
                    return (HttpContext.Current.Session["UserProfile"] as UserProfile).UserID;
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                try
                {
                    return (HttpContext.Current.Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}