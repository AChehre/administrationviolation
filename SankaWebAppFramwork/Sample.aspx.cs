﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class Sample : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// نمایش پیام های مختلف
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowMessage_Command(object sender, CommandEventArgs e)
        {
            byte Type = e.CommandArgument.ToString().StringToByte();
            //
            switch (Type)
            {
                case 1:
                    MessageDialog.ShowMessage("نمایش پیام در همین صفحه", DialogMessage.SankaDialog.Message_Type.Message, this);
                    break;
                case 2:
                    MessageDialog.ShowMessage("نمایش پیام در همین صفحه به صورت دیالوگ", DialogMessage.SankaDialog.Message_Type.Message, this,true);
                    break;
                case 3:
                    MessageDialog.ShowMessage("رفتن به صفحه ای دیگر و نمایش پیام در آن صفحه", DialogMessage.SankaDialog.Message_Type.Message,"/Default.aspx");
                    break;
                case 4:
                    MessageDialog.ShowMessage("رفتن به صفحه ای دیگر و نمایش پیام در آن صفحه به صورت دیالوگ", DialogMessage.SankaDialog.Message_Type.Message, "/Default.aspx", false, true);
                    break;
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {

        }
    }
}