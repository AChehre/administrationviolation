﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Configuration;

namespace SankaWebAppFramework
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            #region ثبت بازدید کننده به عنوان عضوی از گروه عمومی

            UserProfile profile = new UserProfile();
            profile.lstGroupIDs = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();
            profile.lstGroupIDs.Add(new DTO.Config.tblUserGroupMap.tblUserGroupMap
            {
                UserGroupID = Common.PublicGroupID
            });

            // اگر ترو باشد، یعنی زمان یوزر تمام شده است و تازه سشن ست شده است
            // در چک کردن پرمیژن صفحات در زمان پست بک ها بکار می آید
            profile.UserSessionIsNew = true;
            
            Session["UserProfile"] = profile;

            #endregion ثبت بازدید کننده به عنوان عضوی از گروه عمومی

            #region ثبت در جدول آمار سایت

            try
            {
                //این خط مشخص میکند که مثلا اگر در 10 دقیقه اخیر، کاربر جاری به سایت سر نزده بود، به عنوان بازدید کننده در جدول ثبت شود
                int MinDuration = int.Parse(WebConfigurationManager.AppSettings.Get("MinDurationForAmar"));

                DTO.Config.tblAmar.tblAmar Obj = new DTO.Config.tblAmar.tblAmar();

                Obj.RegDate = DateTime.Now;
                Obj.UserIP = Common.UserIP();
                Obj.LastTime = Obj.RegDate.Value.AddMinutes(-MinDuration);

                (new DAL.Config.tblAmar.Methods()).Insert(Obj);
            }
            catch
            {

            }

            #endregion ثبت در جدول آمار سایت
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}