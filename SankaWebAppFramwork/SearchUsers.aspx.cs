﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class SearchUsers : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Search1.UserID = UserID;
            Search1.LockByUserAccessLevel = true;

            Search1.FilterResultByOstanCode = UserOstanCode.HasValue ? UserOstanCode.Value : 0;
            Search1.FilterResultByRegionCode = UserRegionCode.HasValue ? UserRegionCode.Value : 0;
            Search1.FilterResultBySchoolCode = UserSchoolCode.HasValue ? UserSchoolCode.Value : 0;
            Search1.CanFilterResultByOstan = true;
            Search1.CanFilterResultByRegion = true;
            Search1.CanFilterResultBySchool = true;

            Search1.ParentFirstNameInputFieldID = "txtFName";
            Search1.ParentLastNameInputFieldID = "txtLName";
            Search1.ParentPersonnelCodeInputFieldID = "txtPersonnelCode";
            Search1.ParentFatherNameOutputFieldID = "txtFatherName";

            Search1.ParentFirstNameOutputFieldID = "txtFName";
            Search1.ParentLastNameOutputFieldID = "txtLName";
            Search1.ParentPersonnelCodeOutputFieldID = "txtPersonnelCode";
            Search1.ParentOstanCodeOutputFieldID = "ddlState";
            Search1.ParentRegionCodeOutputFieldID = "ddlRegion";
            Search1.ParentSchoolCodeOutputFieldID = "ddlSchool";
        }
    }
}