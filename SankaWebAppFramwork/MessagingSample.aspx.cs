﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class MessageTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MessageDialog.ShowMessage("تست نمایش پیام در یک پنجره و اجرای یک فانکشن بعد از بستن این پنجره ", DialogMessage.SankaDialog.Message_Type.Warning, this, true, "Test");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            MessageDialog.ShowMessage("نمایش پیغام در دیو", DialogMessage.SankaDialog.Message_Type.Warning, this);
        }
    }
}