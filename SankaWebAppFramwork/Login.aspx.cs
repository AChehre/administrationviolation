﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class Login : GeneralPage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (UserID > 0)
            {// اگر کاربر لاگین است، این صفحه لود نشود
                Response.Redirect("~/Default.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoginForm1.Login_Succeed += new controls.LoginForm.dlgAfterLogin(LoginForm1_Login_Succeed);
            LoginForm1.Error_Occured += new controls.LoginForm.dlgError(LoginForm1_Error_Occured);
        }

        /// <summary>
        /// ورود موفقیت آمیز
        /// </summary>
        /// <param name="profile"></param>
        void LoginForm1_Login_Succeed(UserProfile profile,bool blnAnotherLogonPerson)
        {
            //** پاک کردن کوکی نگهدارنده آدرس های بازگشت
            SankaWebAppFramework.Common.RemoveReturnUrlCookie();

            if (profile.UserID > 0)
            {
                // اگر کاربر جاری عضو گروه اعضا هیات بود
                // آیدی هیاتش را بدست می آوریم و به صفحه لیست پرونده هایش هدایتش میکنیم
                #region بدست آوردن هیات هایی که کاربر جاری در آنها عضو است

                List<DTO.Board.tblBoardMemberView.tblBoardMemberView> RList = new List<DTO.Board.tblBoardMemberView.tblBoardMemberView>();
                DTO.Board.tblBoardMemberView.tblBoardMemberView Obj = new DTO.Board.tblBoardMemberView.tblBoardMemberView();
                Obj.MemberUserID = UserID;
                Obj.BoardMemberMapStatus = (byte)AdministrationViolation.Admin.Enums.Board_Member_Status.Active;
                Obj.BoardStatus = (byte)AdministrationViolation.Admin.Enums.Board_Status.Active;
                // بدست آوردن تمام هیات هایی که کاربر در آنها عضو فعال است
                RList = (new DAL.Board.tblBoardMemberView.Methods()).SelectByMemberUserID(Obj);
                if (RList.Any())
                {
                    UserBoardID = RList[0].BoardID.Value;
                    UserBoardType = RList[0].BoardType.Value;
                    BoardOstanCode = RList[0].OstanCode;
                    BoardRegionCode = RList[0].RegionCode;
                    CheckCounterSetting();

                    // Login 1 , Logout = 2
                    (new Logger()).InsertLoginLogoutLog(profile.UserID, UserBoardID, Logger.LoginFlag.Login);

                #endregion بدست آوردن هیات هایی که کاربر جاری در آنها عضو است

                    string strDestPageUrl = getRedirectPage(profile.lstGroupIDs.Last().UserGroupID);

                    #region هدایت کاربر به صفحه مناسب
                    if (blnAnotherLogonPerson)
                    {
                        MessageDialog.ShowMessage("کاربر دیگری با نام کاربری شما هم اکنون وارد شده است یا خروج قبلی شما از سامانه به درستی صورت نگرفته است .", DialogMessage.SankaDialog.Message_Type.Warning, strDestPageUrl);
                    }
                    else
                    {
                        Response.Redirect(strDestPageUrl);
                    }
                    
                    #endregion  هدایت کاربر به صفحه مناسب
                }
                else if (profile.lstGroupIDs.Where(x => x.UserGroupID == 2).Any()) // اگر کاربر جز گروه اعضای هیات ها بود
                {
                    MessageDialog.ShowMessage("کاربر گرامی شما در هیچ هیاتی عضویت ندارید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    profile.UserID = 0;
                    Session["UserProfile"] = profile;
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        /// <summary>
        /// دریافت لینک صفحه پیش فرض بعد ار ورود به سیستم
        /// </summary>
        /// <param name="groupID">شناسه گروه</param>
        /// <returns></returns>
        private string getRedirectPage(int? groupID)
        {
            switch (groupID)
            {
                case 2:
                    {
                        return "~/Board/ViolationFiles.aspx";
                    }
                case 19:
                    {
                        return "~/Board/ViolationFiles.aspx";
                    }
                case 3:
                    {
                        return "~/Admin";
                    }
                case 21:
                    {
                        return "~/Config";
                    }
                default:
                    {
                        return "~/Default.aspx";
                    }

            }
        }

        /// <summary>
        /// چک کردن وجود تنظیمات شمارنده برای هیات کاربر وارد شده در سال عملیاتی انتخابی
        /// در صورت عدم یافتن رکورد ، برای اولین بار رکورد تنظیمات شمارنده را ایجاد و مقادیر آن را پیش فرض به 1 
        /// مقدار دهی میکند .
        /// </summary>
        private void CheckCounterSetting()
        {
            if (this.UserBoardID == 0) return;

            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting
            {
                BoardID = this.UserBoardID,
                Year = UserCurrentApplicationYear.Value

            };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            if (obj.RowNumber == 0)
            {
                var objSetting = new DTO.Board.tblCounterSetting.tblCounterSetting
                {
                    FileSerialCounter = 1,
                    VoteCounter = 1,
                    VoteNotificationCounter = 1,
                    ViolationNotificationCounter = 1,
                    EjectionNotificationCounter = 1,
                    ClosingCounter = 1,
                    BoardID = this.UserBoardID,
                    Year = UserCurrentApplicationYear.Value,
                    
                };
                bool bln = (new DAL.Board.tblCounterSetting.Methods()).Insert(objSetting);
            }
        }

        void LoginForm1_Error_Occured(string message)
        {
            MessageDialog.ShowMessage(message, DialogMessage.SankaDialog.Message_Type.Warning, this);
        }
    }
}
