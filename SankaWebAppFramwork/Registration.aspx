﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true"
    CodeBehind="Registration.aspx.cs" Inherits= "SankaWebAppFramework.Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div>
            <label>
                نام</label>
            <asp:TextBox ID="txtFName" MaxLength="20" runat="server"></asp:TextBox>
            <label>
                نام خانوادگی</label>
            <asp:TextBox ID="txtLName" MaxLength="30" runat="server"></asp:TextBox>
            <label>
                ایمیل</label>
            <asp:TextBox ID="txtMail" TextMode="Email" MaxLength="50" runat="server"></asp:TextBox>
            <label>
                نام کاربری</label>
            <asp:TextBox ID="txtUserName" MaxLength="20" runat="server"></asp:TextBox>
            <label>
                کلمه عبور</label>
            <asp:TextBox ID="txtPass" TextMode="Password" MaxLength="30" runat="server"></asp:TextBox>
            <label>
                تکرار کلمه عبور</label>
            <asp:TextBox ID="txtRePass" TextMode="Password" MaxLength="30" runat="server"></asp:TextBox>
        </div>
    </div>
</asp:Content>
