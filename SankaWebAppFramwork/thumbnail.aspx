﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<script runat="server">
    
    /// <summary>
    /// تغییر اندازه تصویر
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string file = Request.QueryString.Get("file");
            int width = Request.QueryString.Get("width").StringToInt();
            int height = Request.QueryString.Get("height").StringToInt();
            if (file != null)
            {
                string filePath = Server.MapPath(file);
                System.Drawing.Image image = System.Drawing.Image.FromFile(filePath);
                System.Drawing.Image thumbnailImage = image.GetThumbnailImage(width, height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

                thumbnailImage = SankaWebAppFramework.Thumnail.RoundCorners(thumbnailImage, 10, System.Drawing.Color.White);


                MemoryStream imageStream = new MemoryStream();
                thumbnailImage.Save(imageStream, ImageFormat.Jpeg);
                byte[] imageContent = new Byte[imageStream.Length];
                imageStream.Position = 0;
                imageStream.Read(imageContent, 0, (int)imageStream.Length);
                Response.ContentType = "image/jpeg";
                Response.BinaryWrite(imageContent);
                imageStream.Close();
                thumbnailImage.Dispose();
                image.Dispose();
            }
        }
        catch { }
    }

    public bool ThumbnailCallback()
    {
        return true;
    }
</script>
