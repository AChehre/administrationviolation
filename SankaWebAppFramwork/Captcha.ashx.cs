﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

namespace SankaWebAppFramework
{
    /// <summary>
    /// Summary description for Captcha
    /// </summary>
    public class Captcha : IHttpHandler, System.Web.SessionState.IRequiresSessionState 
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "image/jpeg";
                context.Response.Clear();
                context.Response.BufferOutput = true;

                Font font = new Font("Arial", 24, FontStyle.Bold);
                Bitmap bmp = new Bitmap(164, 64, PixelFormat.Format24bppRgb);
                Graphics g = Graphics.FromImage(bmp);
                g.Clear(Color.Lavender);

                Random rand = new Random();
                //------------------------------------------
                char[] chrCharacter = 
                {
                    '0' , '1' , '2' , 'r' , 'p' , '5' , '6' , 'd' , 'x' , '9' ,
                    'a' , 'b' , 'c' , 'd' , 'e' , 'f' , 'g' , 'h' , 'i' , 'j' ,
                    'k' , 'l' , '7' , 'n' , 'o' , '4' , 'q' , '3' , 's' , 't' ,
                    'u' , 'v' , 'w' , '8' , 'y' , 'z'
        
                };
                //------------------------------------------ 

                g.DrawRectangle(new Pen(new SolidBrush(Color.Gray)), new Rectangle(0, 0, 163, 63));

                int l = 15;
                string Capcha = string.Empty;
                for (int i = 0; i < 4; i++)
                {
                    string srtCharacter = Convert.ToString(chrCharacter[rand.Next(0, 35)]);
                    g.DrawString(srtCharacter, font, new SolidBrush(Color.Blue), new PointF(l, rand.Next(5, 25)));
                    l += 35;
                    Capcha = Capcha + srtCharacter;
                }
                Pen pen = new Pen(new SolidBrush(Color.Red), 1);
                for (int i = 0; i < 20; i++)
                {
                    g.DrawLine(pen, new Point(rand.Next(180), rand.Next(100)), new Point(rand.Next(180), rand.Next(100)));
                }
                bmp.Save(context.Response.OutputStream, ImageFormat.Jpeg);


                context.Session["Captcha"] = Capcha;
                context.Session.Timeout = 20;
                g.Dispose();
                bmp.Dispose();
                // Send the output to the client.
                context.Response.Flush();
            }
            catch
            {
                
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}