﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class NewsArchive : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFromDate.DateShamsi = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now) + "/01/01";
                txtToDate.DateShamsi = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                BindGrid();
            }
        }

        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (!txtFromDate.DateIsValid || !txtToDate.DateIsValid)
            {
                SankaDialog1.ShowMessage("درج تاریخ شروع و پایان الزامی است .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }
            if (txtFromDate.DateMiladi > txtToDate.DateMiladi)
            {
                SankaDialog1.ShowMessage("تاریخ شروع باید کوچکتر از تاریخ پایان باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }
            BindGrid();
        }

        /// <summary>
        /// نمایش رکورد های اخبار در گرید
        /// </summary>
        private void BindGrid()
        {
            var obj = new DTO.Admin.tblNews.tblNews
            {
                FromDate = txtFromDate.DateMiladi,
                ToDate = txtToDate.DateMiladi,
                GroupIDs = getUserGroupIds()
            };
            gvNews.DataSource = (new DAL.Admin.tblNews.Methods()).SelectByDate(obj);
            gvNews.DataBind();
        }

        /// <summary>
        /// دریافت لیست گرو هایی که کاربر در آنها عضویت دارد
        /// </summary>
        /// <returns></returns>
        private string getUserGroupIds()
        {
            string strGroupIDs = "";
            var lst = this.UserGroupIDs;
            foreach (var obj in lst)
            {
                strGroupIDs += obj.UserGroupID + ",";
            }
            return strGroupIDs.Remove(strGroupIDs.LastIndexOf(","));
        }
    }
}