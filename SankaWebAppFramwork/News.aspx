﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="News.aspx.cs" Inherits="SankaWebAppFramework.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Repeater ID="rptNews" runat="server">
        <ItemTemplate>
            <fieldset>
                <asp:Image ID="imgNews" ImageUrl='<%# String.Format("~/thumbnail.aspx?file={0}&width={1}&height={2}",Eval("ImageUrl"),75,65) %>'
                    runat="server" ToolTip='<%# Eval("Title") %>' />
                <strong>&nbsp;&nbsp;<span style="color: Blue"> <a style="color: #0000ff;" title="متن کامل اطلاعیه"
                    href='<%# Eval("ID","NewsDetails.aspx?ID={0}") %>'>
                    <%# Eval("Title").ToString() %></a></span></strong>&nbsp;&nbsp;<%# Utility.GetCustomDateWithoutTime(Eval("WriteDate"))%>
                <br />
                <strong style="float: right">شرح اطلاعیه :&nbsp;</strong>
                <div style="float: right">
                    <%# Eval("ShortDescription").ToString() + "   ..." %>
                </div>
                &nbsp;&nbsp;<a style="color: #0000ff;" title="متن کامل اطلاعیه" href='<%# Eval("ID","/NewsDetails.aspx?ID={0}") %>'><b>متن
                    کامل اطلاعیه</b></a>
            </fieldset>
        </ItemTemplate>
        <SeparatorTemplate>
            <br />
        </SeparatorTemplate>
    </asp:Repeater>
    <a href="NewsArchive.aspx">
        <img title="آرشیو اخبار" src="images/icon/text_loudspeaker.png" />
    </a>
</asp:Content>
