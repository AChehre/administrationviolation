﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework
{
    public partial class NewsDetails : GeneralPage
    {
        /// <summary>
        /// نمایش جزئیات خبر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(Request.QueryString.Count == 0) return;
                DTO.Admin.tblNews.tblNews obj = new DTO.Admin.tblNews.tblNews();
                obj.ID = Request.QueryString.Get("ID").StringToLong();
                obj = (new DAL.Admin.tblNews.Methods()).SelectByID(obj);
                lblTitle.Text = obj.Title;
                lblWriteDate.Text = Utility.GetCustomDateWithoutTime(obj.WriteDate);
                ltrNewsDetails.Text = obj.Comment;
            }
        }
    }
}