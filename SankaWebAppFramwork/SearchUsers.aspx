﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchUsers.aspx.cs" Inherits="SankaWebAppFramework.SearchUsers" %>

<%@ Register TagPrefix="sanka" TagName="UCSearch" Src="~/controls/UCSearch.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="/css/site.css" rel="stylesheet" type="text/css" />
    <link href="/css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/site.js" type="text/javascript"></script>
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <%--for search--%>
    <script src="/js/UCSearch.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <sanka:UCSearch FirstNameMinCharLengh="0" LastNameMinCharLengh="0" PersonnelCodeMinCharLengh="8"
            PerformSearchOnName="true" PerformSearchOnPersonnelCode="true" runat="server"
            ClientIDMode="Static" ID="Search1">
        </sanka:UCSearch>
    </div>
    </form>
</body>
</html>
