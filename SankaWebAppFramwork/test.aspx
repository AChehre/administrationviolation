﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="test.aspx.cs" Inherits="SankaWebAppFramework.test" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="Sanka" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script>
        function ShowPopupForConfirm(message, title, TargetID, OnCloseFunction, Arg) {
            $(function () {
                $("#" + TargetID).html(message);
                $("#" + TargetID).dialog({
                    title: title,
                    buttons: {
                        'بلی': function () {
                            $(this).dialog('close');
                        }
                        ,
                        'خیر': function () {

                        }
                    },
                    modal: true,
                    close: function (event, ui) {
                        if (OnCloseFunction !== undefined)
                            OnCloseFunction(Arg);
                    }
                });
            });
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hfFlag" runat="server" ClientIDMode="Static" Value="0" />
    <asp:Button ID="Button1" runat="server" Text="Button" OnClientClick="javascript:return ShowPopupForConfirm('AAAA','BBBB',this.id,'VVVV','GGGG');" />
</asp:Content>
