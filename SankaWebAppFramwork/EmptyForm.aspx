﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmptyForm.aspx.cs" Inherits="SankaWebAppFramework.EmptyForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <link href="css/graphic.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/site.js" type="text/javascript"></script>
</head>
<body style="width: 600px; margin: 20px auto">
    <form id="form1" runat="server">
    <div>
        <div class="_5p odd-row">
            <label class="form-field-label">
                نام :</label>
            <asp:TextBox ID="txtFName" data-required="true" runat="server"></asp:TextBox>
        </div>
        <div class="_5p even-row">
            <label class="form-field-label">
                نام خانوادگی :</label>
            <asp:TextBox ID="txtLName" data-required="true" runat="server"></asp:TextBox>
        </div>
        <div class="_5p odd-row">
            <label class="form-field-label">
                ایمیل :</label>
            <asp:TextBox ID="txtEMail" CssClass="leftAlign" runat="server"></asp:TextBox>
        </div>
        <div class="_5p even-row">
            <label class="form-field-label">
                وضیعت :</label>
            <asp:RadioButtonList RepeatLayout="Flow" CssClass="radio-container" RepeatDirection="Horizontal"
                ID="rbtnIsActive" runat="server">
                <asp:ListItem Value="0" Text="غیرفعال"></asp:ListItem>
                <asp:ListItem Selected="True" Value="1" Text="فعال"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="_5p odd-row">
            <asp:Button title="ذخیره" ID="Button4" OnClientClick="return ValidateRequiredField();"
                CssClass="btn btn-primary" runat="server" Text="ذخیره" 
                onclick="Button4_Click" />
        </div>
    </div>
    </form>
</body>
</html>
