﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsArchive.aspx.cs" Inherits="SankaWebAppFramework.NewsArchive" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCDatePicker.ascx" TagName="UCDatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server">
    </cc2:SankaDialog>
    <fieldset>
        <legend>&nbsp;جستجوی خبر&nbsp;</legend>
        <div class="_10bm">
            <label class="form-field-label">
                از تاریخ&nbsp; :</label>
            <uc1:UCDatePicker ID="txtFromDate" runat="server" />
            <label class="form-field-label _50rmargin">
                تا تاریخ&nbsp; :</label>
            <uc1:UCDatePicker ID="txtToDate" runat="server" />
            <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="~/images/icon/Search.png"
                Style="cursor: pointer" OnClick="btnSearch_Click" class="cp_vm" />
        </div>
    </fieldset>
    <div class="grid">
        <cc1:SankaGrid ID="gvNews" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ReadOnly="True" SortExpression="RowNumber"
                    Visible="True">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID"
                    Visible="False" />
                <asp:BoundField HeaderText="عنوان اطلاعیه" DataField="Title"></asp:BoundField>
                <asp:TemplateField HeaderText="تاریج درج">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("WriteDate")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="بروزرسانی">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("ModifyDate"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="انتشار">
                    <ItemTemplate>
                        <asp:Image ID="imgStatus" runat="server" ImageUrl='<%# Convert.ToBoolean(Eval("Published")) ? "~/images/icon/online.png" : "~/images/icon/offline.png"  %>'
                            ToolTip='<%# Convert.ToBoolean(Eval("Published")) ? "منتشر شده" : "منتشر نشده"  %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="لینک مشاهده">
                    <ItemTemplate>
                        <a href='<%# string.Format("/NewsDetails.aspx?ID={0}",Eval("ID")) %>' target="_blank">
                            لینک اطلاعیه</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="انقضاء">
                    <ItemTemplate>
                        <%# Utility.GetNuemericCustomDateWithoutTime(Eval("ExpirationDate"))%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <span>رکوردی جهت نمایش موجود نیست .</span>
            </EmptyDataTemplate>
        </cc1:SankaGrid>
    </div>
</asp:Content>
