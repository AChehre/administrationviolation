﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploadLabelConfig.aspx.cs"
    Inherits="SankaWebAppFramework.Config.FileUploadLabelConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body dir="rtl" style="overflow:scroll">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlRequest" ScrollBars="Both" Direction="RightToLeft">
        <asp:GridView ID="gvRequestTable" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            DataKeyNames="AttachmentTypeIndex" DataSourceID="SqlDataSource1" 
            CellPadding="4" ForeColor="#333333"
            PageSize="100" ShowFooter="True" AllowSorting="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="AttachmentTypeIndex" HeaderText="AttachmentTypeIndex"
                    ReadOnly="True" SortExpression="AttachmentTypeIndex" />
                <asp:BoundField DataField="DescriptionLabel" HeaderText="DescriptionLabel" 
                    SortExpression="DescriptionLabel"></asp:BoundField>
                <asp:BoundField DataField="DocumentDateLabel" HeaderText="DocumentDateLabel"
                    SortExpression="DocumentDateLabel" />
                <asp:BoundField DataField="DocumentCodeLabel" HeaderText="DocumentCodeLabel"
                    SortExpression="DocumentCodeLabel" />
                <asp:BoundField DataField="SelectFileLabel" HeaderText="SelectFileLabel" 
                    SortExpression="SelectFileLabel" />
                <asp:BoundField DataField="AttachmentListLabel" 
                    HeaderText="AttachmentListLabel" SortExpression="AttachmentListLabel" />
                <asp:CommandField ShowEditButton="True" ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </asp:Panel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
        ConnectionString="<%$ ConnectionStrings:UIConnectionString %>" DeleteCommand="DELETE FROM [tblAttachmentLabels] WHERE [AttachmentTypeIndex] = @original_AttachmentTypeIndex"
        InsertCommand="INSERT INTO [tblAttachmentLabels] ([AttachmentTypeIndex], [DescriptionLabel], [DocumentDateLabel], [DocumentCodeLabel], [SelectFileLabel], [AttachmentListLabel]) VALUES (@AttachmentTypeIndex, @DescriptionLabel, @DocumentDateLabel, @DocumentCodeLabel, @SelectFileLabel, @AttachmentListLabel)"
        OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [tblAttachmentLabels]"
        
        UpdateCommand="UPDATE [tblAttachmentLabels] SET [DescriptionLabel] = @DescriptionLabel, [DocumentDateLabel] = @DocumentDateLabel, [DocumentCodeLabel] = @DocumentCodeLabel, [SelectFileLabel] = @SelectFileLabel, [AttachmentListLabel] = @AttachmentListLabel WHERE [AttachmentTypeIndex] = @original_AttachmentTypeIndex">
        <DeleteParameters>
            <asp:Parameter Name="original_AttachmentTypeIndex" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="AttachmentTypeIndex" Type="Int32" />
            <asp:Parameter Name="DescriptionLabel" Type="String" />
            <asp:Parameter Name="DocumentDateLabel" Type="String" />
            <asp:Parameter Name="DocumentCodeLabel" Type="String" />
            <asp:Parameter Name="SelectFileLabel" Type="String" />
            <asp:Parameter Name="AttachmentListLabel" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DescriptionLabel" Type="String" />
            <asp:Parameter Name="DocumentDateLabel" Type="String" />
            <asp:Parameter Name="DocumentCodeLabel" Type="String" />
            <asp:Parameter Name="SelectFileLabel" Type="String" />
            <asp:Parameter Name="AttachmentListLabel" Type="String" />
            <asp:Parameter Name="original_AttachmentTypeIndex" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
