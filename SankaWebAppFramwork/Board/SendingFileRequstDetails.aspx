﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Solid.Master" AutoEventWireup="true"
    CodeBehind="SendingFileRequstDetails.aspx.cs" Inherits="SankaWebAppFramework.Board.SendingFileRequstDetails" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCFileSendingRequest.ascx" TagName="UCFileSendingRequest"
    TagPrefix="uc1" %>
<%@ Register Src="../controls/SankaFileUpload.ascx" TagName="SankaFileUpload" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel-body prevententer">
        <div class="_5p">
            <label class="form-field-label">
                هیات رسیدگی کننده مجدد :</label>
            <asp:Label ID="lblBoardName" runat="server"></asp:Label>
        </div>
        <div class="_5p" style="display:none">
            <label class="form-field-label">
                کلاسه پرونده درخواستی :</label>
            <asp:Label ID="lblSerialCode" runat="server"></asp:Label>
        </div>
        <div class="_5p">
            <label class="form-field-label">
                توضیحات :</label>
            <div style="border-style: inset; border-width: thin; height: 100px" runat="server"
                id="txtComment">
            </div>
            <asp:HiddenField ID="hfFileID" runat="server" />
            <asp:HiddenField ID="hfRelatedFileID" runat="server" />
        </div>
        <div class="_5p">
            <asp:Button CssClass="btn btn-info" ID="btnAccept" runat="server" Text="قبول درخواست"
                OnClick="btnAccept_Click" />
        &nbsp;<asp:Button CssClass="btn btn-info" ID="btnReject" runat="server" Text="رد درخواست"
                OnClick="btnReject_Click" />
        </div>
    </div>
    <asp:Panel class="_5p odd-row" ID="pnlAttachment" runat="server" Visible="false">
        <uc2:SankaFileUpload ID="FileUpload1" runat="server" />
    </asp:Panel>
    <cc1:SankaDialog ID="SankaDialog1" runat="server">
    </cc1:SankaDialog>
    <input id="hfParentReloadFlag" type="hidden" value="1" />
</asp:Content>
