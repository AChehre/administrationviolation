﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Solid.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="SankaWebAppFramework.Board.Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body {
            background: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Literal ID="ltWorkFlowReport" runat="server"></asp:Literal>
</asp:Content>
