﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board.print
{
    public partial class BadviFileClosing : System.Web.UI.Page
    {
        /// <summary>
        /// شناسه صورتجلسه مختومه
        /// </summary>
        private long ClosingID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("ClosingID")))
                {
                    return Request.QueryString.Get("ClosingID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// شناسه هیات
        /// </summary>
        private long BoardID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("BoardID")))
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardID = value;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        protected byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }

        /// <summary>
        /// اتفاق یا اکثریت آراء
        /// </summary>
        private long SodurType
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("SType")))
                {
                    return Request.QueryString.Get("SType").StringToLong();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                checkViewFilePermission();
                LoadAccusedInfo(this.FileID);
                LoadBoardName();
                LoadClosingInfo();
                LoadAccusitionSubject();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "key1", "window.print();", true);
            }
        }

        /// <summary>
        /// بارگذاری موضوع اتهام
        /// </summary>
        private void LoadAccusitionSubject()
        {
            string strAccusitionSubects = "";
            var obj = new DTO.Board.tblAccusation.tblAccusation
            {
                ViolationFileID = this.FileID
            };
            var lst = (new DAL.Board.tblAccusation.Methods()).SelectByFileID(obj);
            for (int i = 0; i < lst.Count; i++)
            {
                strAccusitionSubects += lst[i].AccusationSubjectID + ",";
            }
            if (strAccusitionSubects.Length > 1)
            {
                strAccusitionSubects = strAccusitionSubects.Remove(strAccusitionSubects.Length - 1);
                //lblAccusitionSubject.Text = strAccusitionSubects;
            }
        }

        /// <summary>
        /// بارگذاری جزئیات رای صادره
        /// </summary>
        private void LoadClosingInfo()
        {
            var obj = new DTO.Board.tblViolationFileClosing.tblViolationFileClosing
            {
                ClosingID = this.ClosingID
            };
            obj = (new DAL.Board.tblViolationFileClosing.Methods()).SelectByID(obj);
            lblDate.Text = Utility.GetNuemericCustomDateWithoutTime(obj.ClosingDate);
            lblSerialNumber.Text = obj.ClosingNumber.ToString();
            lblSodurType.InnerHtml = getSodurTypeLabel();
            if (obj.ClosingDate.HasValue)
            {
                lblClosingDate.Text = Utility.GetNuemericCustomDateWithoutTime(obj.ClosingDate.Value);
            }
        }

        private string getSodurTypeLabel()
        {
            try
            {
                return SankaWebAppFramework.Board.Enums.strSodourTypeLabel[this.SodurType];
            }
            catch { return ""; }
        }

        /// <summary>
        /// برگرداندن نام هیات
        /// </summary>
        private void LoadBoardName()
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = this.BoardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            lblBoardName.Text = obj.BoardTitle;
        }

        /// <summary>
        /// بارگذاری اطلاعات هویتی متهم
        /// </summary>
        /// <param name="fileID">شناسه فایل</param>
        private void LoadAccusedInfo(long fileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                long personnelCode = obj.PersonnelCode.Value;
                var objUser = new DTO.Board.tblPersonnelView.tblPersonnelView
                {
                    PersonnelCode = personnelCode
                };
                objUser = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(objUser);
                if (objUser.RowNumber > 0)
                {
                    lblGardesheKar.InnerHtml = obj.ClosingReport;
                    lblAccusedName.Text = obj.FirstName + " " + obj.LastName;
                    //lblWroteDate.Text = lblSodurDate.Text = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                    lblAccusedName.Text = objUser.FirstName + " " + objUser.LastName;
                    lblFatherName.Text = obj.FatherName;
                    lblShenasNo.Text = objUser.ShenasNo;
                    lblSodurPlace.Text = objUser.BirthPlace;
                    lblOrganizationGroup.Text = Common.getOtganizationGroupName(objUser.OrganizationGroup);
                    lblOrganizationPosition.Text = Common.getOrganizationPosition(objUser.OrganizationPosition);
                    lblOstan.Text = objUser.OstanName;
                    lblRegion.Text = objUser.RegionName;
                    lblBirthYear.Text = Common.getBirthYear(objUser.FarsiBirthDate);
                    lblActivityYear.Text = objUser.ActivityYears.ToString();
                    lblMostkhdemType.Text = Common.getMostakhdemTypeLabel(objUser.MostkhdemType) ;//(objUser.MostkhdemType == 1) ? "رسمی" : "پیمانی";
                    lblEstekhdamStatus.Text = Common.getEstekhdamStatusLabel(objUser.EstekhdamStatus);//Enums.strlblEstekhdamStatus[objUser.EstekhdamStatus];
                    lblEducationalGrade.Text = Common.getDegreeLabel(objUser.Education);
                    lblNationalCode.Text = objUser.NationalCode;
                    lblPersonnelNumber.Text = objUser.PersonnelCode.ToString();
                    lblMarried.Text = objUser.Married ? "متاهل" : "مجرد";
                    lblGenderTitle.Text = Common.getGenderTitleLabel(objUser.Gender);
                }
            }
        }

        /// <summary>
        /// چک کردن اینکه کاربر مجوز مشاهده جزئیات پرونده را دارد یا نه ؟
        /// </summary>
        private void checkViewFilePermission()
        {
            UserProfile profile = Session["UserProfile"] as UserProfile;
            if (profile.UserID == 0)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            
            ///! اگر دفتر هماهنگی نبود باید چک شود
            if (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                var objBoard = new DTO.Board.tblFileAccessMap.tblFileAccessMap
                {
                    FileID = this.FileID,
                    BoardID = this.UserBoardID
                };
                byte bytViewType = (new DAL.Board.tblFileAccessMap.Methods()).HasPermission(objBoard);
                if (bytViewType == 0)
                {
                    Response.End();
                }
            }
        }
    }
}