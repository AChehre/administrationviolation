﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board.print
{
    public partial class TajdidNazarInnocenceVote : System.Web.UI.Page
    {
        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardID = value;
            }
        }

        private long VoteID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("VoteID")))
                {
                    return Request.QueryString.Get("VoteID").StringToLong();
                }
                return 0;
            }
        }

        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        private long BoardID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("BoardID")))
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        protected byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                checkViewFilePermission();
                LoadAccusedInfo(this.FileID);
                LoadBoardMember();
                LoadBoardName();
                LoadVoteInfo();
                LoadPreviusVoteInfo();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "key1", "window.print();", true);
            }
        }

        private void LoadPreviusVoteInfo()
        {
            DTO.Board.tblFileRelationMap.tblFileRelationMap obj = new DTO.Board.tblFileRelationMap.tblFileRelationMap
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblFileRelationMap.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                var objPreviusVote = new DTO.Board.tblViolationFileVote.tblViolationFileVote
                {
                    ViolationFileID = obj.RelatedFileID
                };
                objPreviusVote = (new DAL.Board.tblViolationFileVote.Methods()).SelectByViolationFileID(objPreviusVote);
                if (objPreviusVote.RowNumber > 0)
                {
                    lblPreviousBoardName.Text = getPreviusBoardName(objPreviusVote.ViolationFileID);
                    lblPreviousVoteDate.Text = Utility.GetNuemericCustomDateWithoutTime(objPreviusVote.VoteDate);
                    lblPreviousVoteNumber.Text = objPreviusVote.VoteNumber.ToString();
                    lblPreviousVoteDescription.Text = objPreviusVote.PenaltyDetail;
                }
            }
        }

        private string getPreviusBoardName(long fileID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                DTO.Board.tblBoard.tblBoard objBoard = new DTO.Board.tblBoard.tblBoard
                {
                    BoardID = obj.BoardID.Value
                };
                objBoard = (new DAL.Board.tblBoard.Methods()).SelectByID(objBoard);
                if (objBoard.RowNumber > 0)
                {
                    return objBoard.BoardTitle;
                }
            }
            return string.Empty;
        }

        private string getAccusitionList()
        {
            string strAccusitionSubects = "";
            var obj = new DTO.Board.tblAccusation.tblAccusation
            {
                ViolationFileID = this.FileID,
                Status = 1 // محرز شده
            };
            var lst = (new DAL.Board.tblAccusation.Methods()).SelectByFileID(obj);
            for (int i = 0; i < lst.Count; i++)
            {
                strAccusitionSubects += lst[i].AccusationSubjectID + ",";
            }
            if (strAccusitionSubects.Length > 1)
            {
                strAccusitionSubects = strAccusitionSubects.Remove(strAccusitionSubects.Length - 1);
            }
            return strAccusitionSubects;
        }

        private void LoadVoteInfo()
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                VoteID = this.VoteID
            };
            obj = (new DAL.Board.tblViolationFileVote.Methods()).SelectByID(obj);
            lblDate.Text = Utility.GetNuemericCustomDateWithoutTime(obj.VoteDate);
            lblAccusitionList.Text = getAccusitionList();
            lblSerialNumber.Text = obj.VoteNumber.ToString();
            lblVoteDate.Text = Utility.GetNuemericCustomDateWithoutTime(obj.VoteDate);
            lblSodourType.Text = Enums.strSodourTypeLabel[obj.SodourType.Value];
        }

        private void LoadBoardName()
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = this.BoardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            lblBoardName.Text = obj.BoardTitle;
        }

        private void LoadBoardMember()
        {
            var lstObj = new List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers>();

            var Obj = new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers();
            Obj.ViolationFileID = this.FileID;

            lstObj = (new DAL.Board.tblViolationFileVoteMembers.Methods()).SelectByViolationFileID(Obj);
            if (lstObj.Count >= 3)
            {
                lblMember1.Text = lstObj[0].FullName;
                lblMember2.Text = lstObj[1].FullName;
                lblMember3.Text = lstObj[2].FullName;
            }
        }

        private void LoadAccusedInfo(long fileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                long personnelCode = obj.PersonnelCode.Value;
                var objUser = new DTO.Board.tblPersonnelView.tblPersonnelView
                {
                    PersonnelCode = personnelCode
                };
                objUser = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(objUser);
                if (objUser.RowNumber > 0)
                {
                    lblGardesheKar.Text = obj.WorkflowReport;
                    lblAccusedName.Text = obj.FirstName + " " + obj.LastName;
                    lblDate.Text = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                    lblPersonnelNumber.Text = objUser.PersonnelCode.ToString();
                    lblAccusedName.Text = objUser.FirstName + " " + objUser.LastName;
                    lblFatherName.Text = obj.FatherName;
                    lblShenasNo.Text = objUser.ShenasNo;
                    lblSodurPlace.Text = objUser.BirthPlace;
                    lblOrganizationGroup.Text = Common.getOtganizationGroupName(objUser.OrganizationGroup);
                    lblOrganizationPosition.Text = Common.getOrganizationPosition(objUser.OrganizationPosition);
                    lblOstan.Text = objUser.OstanName;
                    lblRegion.Text = objUser.RegionName;
                    lblBirthYear.Text = Common.getBirthYear(objUser.FarsiBirthDate);
                    lblActivityYear.Text = objUser.ActivityYears.ToString();
                    lblMarried.Text = objUser.Married ? "متاهل" : "مجرد";
                    lblMostkhdemType.Text = Common.getMostakhdemTypeLabel(objUser.MostkhdemType);//(objUser.MostkhdemType == 1) ? "رسمی" : "پیمانی";
                    lblEstekhdamStatus.Text = Common.getEstekhdamStatusLabel(objUser.EstekhdamStatus);//Enums.strlblEstekhdamStatus[objUser.EstekhdamStatus];
                    lblEducationalGrade.Text = Common.getDegreeLabel(objUser.Education);
                    lblNationalCode.Text = objUser.NationalCode;
                    lblPersonnelNumber.Text = objUser.PersonnelCode.ToString();
                    lblGenderTitle.Text = Common.getGenderTitleLabel(objUser.Gender);
                    lblAccusedTitle.Text = Common.getAccusedGenderTitleLabel(objUser.Gender);
                }
            }
        }

        private void checkViewFilePermission()
        {
            UserProfile profile = Session["UserProfile"] as UserProfile;
            if (profile.UserID == 0)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            if (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                var objBoard = new DTO.Board.tblFileAccessMap.tblFileAccessMap
                {
                    FileID = this.FileID,
                    BoardID = this.UserBoardID
                };
                byte bytViewType = (new DAL.Board.tblFileAccessMap.Methods()).HasPermission(objBoard);
                if (bytViewType == 0)
                {
                    Response.End();
                }
            }
        }
    }
}