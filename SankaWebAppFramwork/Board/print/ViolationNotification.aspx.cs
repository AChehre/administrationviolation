﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board.print
{
    public partial class ViolationNotification : System.Web.UI.Page
    {
        #region پروپرتی ها

        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardID = value;
            }
        }

        private long NotificationID
        {
            get
            {
                if(!string.IsNullOrEmpty(Request.QueryString.Get("ID")))
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        private long BoardID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("BoardID")))
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        private byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }

        private string returnPageUrl
        {
            get
            {
                return Request.QueryString.Get("returnPageUrl");
            }
        }

        #endregion  پروپرتی ها

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                checkViewFilePermission();
                BindAccusedInfo(this.FileID);
                BindBoardAddress(this.BoardID);
                BindAccusitionList(this.NotificationID);
                BindNotificationField(this.NotificationID);
                LoadBoardManagerName(this.BoardID);
                ScriptManager.RegisterClientScriptBlock(this, Page.GetType(), "key1", "window.print()", true);
            }
        }

        private void LoadBoardManagerName(long boardID)
        {
            var obj = new DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView
            {
                BoardID = boardID,
                MemberRole = (byte)AdministrationViolation.Admin.Enums.Board_Member_Role.BoardAdmin
            };
            obj = (new DAL.Admin.tblBoardMemberMapView.Methods()).SelectByMemberRole(obj);
            lblManagerName.Text = obj.FName + " " + obj.LName;
        }

        private void BindNotificationField(long notificationID)
        {
            var obj = new DTO.Board.tblViolationNotification.tblViolationNotification
            {
                NotificationID = notificationID
            };
            obj = (new DAL.Board.tblViolationNotification.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                lblGiverName.Text = obj.GiverName;
                lblNotificationAddress.Text =  obj.NotificationAddress;
                lblSerialNumber.Text = obj.LetterSerial;
                lblDate.Text = Utility.GetNuemericCustomDateWithoutTime(obj.NotificationDate);
                //lblDeliveryDate.Text = (obj.DeliveryDate.HasValue) ? Utility.GetCustomDateWithoutTime(obj.DeliveryDate) : string.Empty;
            }
        }

        private void BindBoardAddress(long boardID)
        {
            var obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = boardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                txtBoardAddress.Text = obj.Address;
            }
        }

        private void BindAccusitionList(long notificationID)
        {
            var objAccusition = new DTO.Board.tblAccusitionView.tblAccusitionView
            {
                NotificationID = NotificationID
            };
           
           rptAccusitionList.DataSource = (new DAL.Board.tblAccusitionView.Methods()).SelectByNotificationID(objAccusition);
           rptAccusitionList.DataBind();
        }

        private void BindAccusedInfo(long fileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                lblAccusedFullName.Text = obj.FirstName + " " + obj.LastName;
                lblFatherName.Text = obj.FatherName;
                lblNotifiedPerson.Text = obj.FirstName + " " + obj.LastName;
            }
        }

        private void checkViewFilePermission()
        {
            UserProfile profile = Session["UserProfile"] as UserProfile;
            if (profile.UserID == 0)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            if (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                var objBoard = new DTO.Board.tblFileAccessMap.tblFileAccessMap
                {
                    FileID = this.FileID,
                    BoardID = this.UserBoardID
                };
                byte bytViewType = (new DAL.Board.tblFileAccessMap.Methods()).HasPermission(objBoard);
                if (bytViewType == 0)
                {
                    Response.End();
                }
            }
        }
    }
}