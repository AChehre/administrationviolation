﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Proceedings.aspx.cs" Inherits="SankaWebAppFramework.Board.print.Proceedings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl">
<head id="Head1" runat="server">
    <title>صورت جلسه</title>
    <style>
        table
        {
            width: 100%; /*margin: 0px 0px 0px 0px;*/
        }
        body
        {
            margin: 0px;
            padding: 0px;
            top: 0px;
            left: 0px;
            height: 580px;
            font-family: 'B Nazanin';
            text-align: right;
        }
        td
        {
            direction: rtl;
        }
        p
        {
            direction: rtl;
        }
        .style2
        {
            font-size: 14pt;
        }
        .style3
        {
        }
        .style4
        {
            text-align: center;
            font-weight: bold;
            font-size: 14pt;
        }
        span
        {
            margin-right: 10px;
            margin-left: 10px;
            font-weight: 700;
            font-size: 16pt;
        }
        .style5
        {
            font-size: 11pt;
        }
    </style>
</head>
<body style="text-align: right">
    <form id="form1" runat="server">
    <div style="border-color: #000000; border-width: 1px; border-style: solid; padding-right: 5px;
        padding-left: 5px;">
        <table>
            <tr>
                <td style="text-align: center">
                    <strong><span>بسمه تعالی</span></strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <img alt="" src="/images/report/arm.png" style="height: 36px; width: 89px; top: 126px;
                        right: 508px;" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <strong><font size="5">صورت جلسه</font></strong>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <strong style="font-size: 14pt">شماره صورتجلسه :</strong>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <strong style="font-size: 14pt">تاریخ :</strong>
                    <asp:Label ID="lblWroteDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td dir="rtl">
                    <strong style="font-size: 14pt">آقاي / خانم :</strong>
                    <asp:Label ID="lblAccusedFullName" runat="server" />
                </td>
            </tr>
        </table>
        <p class="style2">
            دراجراي مـاده 24 قـانون رسيـدگي بـه تخلفــات اداري جلسـه
            <asp:Label ID="lblBoardName" runat="server"></asp:Label>
            &nbsp;در تـاريخ&nbsp;<asp:Label ID="lblSodurDate" runat="server"></asp:Label>
            &nbsp;با حضورسه نفراز اعضا جهت رسيدگي به درخواست <strong>
                <asp:Label runat="server" ID="lblGenderTitle" />
                <asp:Label ID="lblAccusedName" runat="server" />
            </strong>فرزند <strong>
                <asp:Label ID="lblFatherName" runat="server"></asp:Label>
            </strong>به شماره شناسنامه<strong>
                <asp:Label ID="lblShenasNo" runat="server"></asp:Label>
            </strong>صادره از <strong>
                <asp:Label ID="lblSodurPlace" runat="server"></asp:Label>
            </strong>متولد <strong>
                <asp:Label ID="lblBirthYear" runat="server"></asp:Label>
            </strong>،
            <asp:Label ID="lblMarrageStatus" runat="server"></asp:Label>
            ، مستخدم رسمی که با مدرک تحصیلی
            <asp:Label ID="lblEducationalGrade" runat="server"></asp:Label>
            دارای گروه <strong>
                <asp:Label ID="lblOrganizationGroup" runat="server"></asp:Label>
            </strong>و با <strong>
                <asp:Label ID="lblActivityYear" runat="server"></asp:Label>
            </strong>سال سابقه كاردر پست سازمانی <strong>
                <asp:Label ID="lblOrganizationPosition" runat="server"></asp:Label>
            </strong>دربخش/شهرستان/منطقه <strong>
                <asp:Label ID="lblRegion" runat="server"></asp:Label>
            </strong>استان <strong>
                <asp:Label ID="lblOstan" runat="server"></asp:Label>
            </strong>خدمت می نموده و براساس رأي قطعي شماره ..................مورخ هيأت بدوي
            / تجديدنظر به مجازات محكوم شده است،تشكيل گرديد.
        </p>
        <p class="paragraph">
            <strong>گردش كار : </strong>
            <br>
            <p class="paragraph">
                <p class="paragraph" id="lblGardesheKar" runat="server">
                </p>
                <br>
            </p>
        </p>
        <p style="border-style: solid; border-width: 1px;">
            <table>
                <tr>
                    <td class="style4">
                        <asp:Label ID="lblMember1" runat="server" CssClass="style5"></asp:Label>
                    </td>
                    <td class="style4">
                        <asp:Label ID="lblMember2" runat="server" CssClass="style5"></asp:Label>
                    </td>
                    <td class="style4">
                        <asp:Label ID="lblMember3" runat="server" CssClass="style5"></asp:Label>
                    </td>
                </tr>
            </table>
        </p>
    </div>
    </form>
</body>
</html>
