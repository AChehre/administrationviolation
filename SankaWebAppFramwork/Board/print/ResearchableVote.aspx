﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResearchableVote.aspx.cs"
    Inherits="SankaWebAppFramework.Board.print.ResearchableVote" %>

<html>
<head>
    <style type="text/css" media="print">
        #divPanel
        {
            display: none;
        }
    </style>
    <style type="text/css">
	@font-face {
		font-family: 'IranNastaliq';
		src: url('fonts/IranNastaliq.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'BNazanin';
		src: url('fonts/BNazanin.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	.page
	{
		width:850px;
		margin:0px auto;
		font-family:'BNazanin';
		font-size:13.5pt;
		text-align:right;
		direction:rtl;
		padding:50pt;
	}
	.top-position
	{
		margin-top:-70px;
		width:60%; 
		float:right;
		text-align:center;
	}
	.logo-center
	{
		width:80px;
		height:45px;
	}
	
	.logo-right
	{
		width:130px;
		height:80px;
	}
	
	.Nastaligh
	{
		font-family: IranNastaliq; 
	}
	.title1
	{
		font-size:20pt;
		line-height:1pt;
		text-align:center; !important;
	}
	
	.date-and-number-left
	{
		text-align:right;
		width:20%;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
		float:left;
	}
	
	.date-and-number-right
	{
		text-align:right;
		width:20%;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
		float:right;
	}
	
	.paragraph
	{
		text-align: justify;
		width:100%;
		float:right;
	}
	
	</style>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="EidehPardazanFarazgamArad Co .Ltd" />
    <meta name="keywords" />
    <meta name="description" />
    <title>رای قابل پژوهش </title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divPanel">
        <img id="btnPrint" alt="چاپ" src="/images/icon/printer.png" onclick="window.print();"
            style="cursor: pointer" title="چاپ" />
        <img src="/images/icon/back.png" onclick="history.back()" alt="بازگشت" title="بازگشت"
            style="cursor: pointer" />
    </div>
    <div align="center" class="page">
        <div align="right" style="display: none">
            <img class="logo-right" src="images/arm.png" />
        </div>
        <div style="width: 100%">
            <div class="date-and-number-right">
                <p>
                    <span class="Nastaligh">شماره پرسنلی:&nbsp;</span>
                    <asp:Label ID="lblPersonnelNumber" runat="server"></asp:Label>
                </p>
            </div>
            <div class="top-position">
                <p class="Nastaligh title1">
                    بسمه تعالی</p>
                <img src="images/logo.png" class="logo-center" />
                <p class="Nastaligh title1">
                    رای قابل پژوهش</p>
            </div>
            <div class="date-and-number-left">
                <p>
                    <span class="Nastaligh">شماره رای:&nbsp;</span>
                    <asp:Label ID="lblSerialNumber" runat="server"></asp:Label>
                </p>
                <p>
                    <span class="Nastaligh">تاریخ رای:&nbsp;</span>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div style="padding-right: 5px; padding-left: 5px">
            <div class="paragraph">
                جلسه هیات بدوی رسیدگی به تخلفات اداری کارمندان <strong>
                    <asp:Label ID="lblBoardName" runat="server"></asp:Label></strong>&nbsp; &nbsp;در
                تـاريخ&nbsp;<asp:Label runat="server" ID="lblVoteDate" Style="font-weight: 700" />
                با حضورسه نفراز اعضا جهت رسيدگي به اتهام / اتهام های &nbsp;
                <asp:Label runat="server" ID="lblGenderTitle" />&nbsp; <strong>
                    <asp:Label ID="lblAccusedName" runat="server"></asp:Label>
                </strong>مستخدم&nbsp;<strong><asp:Label ID="lblMostkhdemType" runat="server"></asp:Label></strong>&nbsp;
                متولد سال <strong>&nbsp;
                    <asp:Label ID="lblBirthYear" runat="server"></asp:Label>
                    &nbsp; </strong>شماره شناسنامه &nbsp; <strong>
                        <asp:Label ID="lblShenasNo" runat="server"></asp:Label>
                    </strong>&nbsp; صادره از &nbsp; <strong>
                        <asp:Label ID="lblSodurPlace" runat="server"></asp:Label>
                        &nbsp; </strong>&nbsp; شماره ملی &nbsp; <strong>
                            <asp:Label ID="lblNationalCode" runat="server"></asp:Label>
                            &nbsp;</strong>فرزند &nbsp; <strong>
                                <asp:Label ID="lblFatherName" runat="server"></asp:Label>
                            </strong>&nbsp; وضعیت تاهل: &nbsp; <strong>
                                <asp:Label ID="lblMarrage" runat="server"></asp:Label>
                            </strong>&nbsp; ، مدرک تحصیلی &nbsp; <strong>
                                <asp:Label ID="lblEducationalGrade" runat="server"></asp:Label>
                            </strong>&nbsp; دارای گروه/طبقه &nbsp; <strong>
                                <asp:Label ID="lblOrganizationGroup" runat="server"></asp:Label>
                            </strong>&nbsp; با &nbsp; <strong>
                                <asp:Label ID="lblActivityYear" runat="server"></asp:Label>
                            </strong>سال سابقه &nbsp; کار، آخرین پست سازمانی &nbsp; <strong>
                                <asp:Label ID="lblOrganizationPosition" runat="server"></asp:Label>
                            </strong>&nbsp; که در منطقه/بخش/شهرستان &nbsp; <strong>
                                <asp:Label ID="lblRegion" runat="server"></asp:Label>
                            </strong>&nbsp; استان &nbsp; <strong>
                                <asp:Label ID="lblOstan" runat="server"></asp:Label>
                            </strong>&nbsp; خدمت می نماید و در زمان صدور این رای در حالت استخدامی
                &nbsp; <strong>
                    <asp:Label ID="lblEstekhdamStatus" runat="server"></asp:Label>
                </strong>&nbsp; بوده ، تشکیل شد .<br>
                پس از اتمام رسیدگی و ملاحظه اسناد و مدارک موجود در پرونده و نیز با توجه کامل به
                مدافعات <strong>
                    <asp:Label ID="lblAccusedTitle" runat="server"></asp:Label>
                </strong>، اتهام/ اتهام های منتسب به وی که با بند / بندهای <strong>
                    <asp:Label ID="lblAccusitionList" runat="server"></asp:Label>
                </strong>ماده 8 قانون رسیدگی به تخلفات اداری مصوب 1372/9/7 مجلس شورای اسلامی 
                منطبق می باشد ، برای هیات محرز گردید . لذا هیات <strong>&nbsp;<asp:Label ID="lblSodourType" runat="server"></asp:Label>
                    &nbsp; </strong>نامبرده را به استناد &nbsp; <strong>
                        <asp:Label ID="lblMadde9" runat="server"></asp:Label>
                        &nbsp; </strong>&nbsp;قانون مزبور ، به مجازات &nbsp; <strong>
                            <asp:Label ID="lblPenaltyDescription" runat="server"></asp:Label>
                            &nbsp;</strong>محکوم<strong> </strong>می نماید .</p>
			</div>
            <div class="paragraph">
                این رای قابل تجدید نظر می باشد و <strong>
                    <asp:Label ID="lblMahkoomLabel" runat="server"></asp:Label>
                </strong>&nbsp;یا نماینده قانونی وی میتواند درخواست تجدید نظر خود را حداکثر ظرف
                <strong>سی روز</strong> از تاریخ ابلاغ رای به اداره کارگزینی مربوطه به نشانی &nbsp;
                <strong>
                    <asp:Label ID="lblKargoziniAddress" runat="server"></asp:Label>
                </strong>&nbsp; تسلیم نماید . در غیر اینصورت به استناد تبصره 1 ماده 4 قانون مذکور
                ، این رای از تاریخ انقضاء مهلت یاد شده لازم الاجراء خواهد بود .
			</div>
            <div class="paragraph">
				<br>
                <strong>گردش كار : </strong>
			</div>
             <div class="paragraph">
                    <p class="paragraph" id="lblGardesheKar" runat="server">
                    </p>
                    <br>
					</p>
            </div>
            <div style="width: 100%">
                <div style="width: 33%; float: right">
                    1.<span>نام و نام خانوادگی:</span> <strong>
                        <asp:Label ID="lblMember1" runat="server" Css></asp:Label>
                    </strong><span>امضاء</span>
                </div>
                <div style="width: 33%; float: right">
                    2.<span>نام و نام خانوادگی:</span> <strong>
                        <asp:Label ID="lblMember2" runat="server" Css></asp:Label>
                    </strong><span>امضاء</span>
                </div>
                <div style="width: 33%; float: right">
                    3.<span>نام و نام خانوادگی:</span> <strong>
                        <asp:Label ID="lblMember3" runat="server" Css></asp:Label>
                    </strong><span>امضاء</span>
                </div>
            </div>
        </div>
        <div style="border-style: solid; border-width: 1px; width: 100%; margin-top: 30px;
            height: 30px; float: right">
            <div style="width: 95%; float: right">
                <strong>* تذکر </strong>: بدون درج نام و نام خانوادگی و امضاء اعضا حاضر در جلسه
                در پایان گردش کار ، رای فاقد اعتبار است .
            </div>
            <div style="width: 5%; float: left; text-align: right">
                <strong>ف:3</strong>
            </div>
        </div>
        <div style="width: 100%; margin-top: 30px; float: right">
            <div style="width: 55%; float: right">
                نام و نام خانوادگی ابلاغ شونده : &nbsp; <strong>
                    <asp:Label ID="lblNotifiedPerson" runat="server"></asp:Label>
                    &nbsp; </strong>
            </div>
            <div style="width: 45%; float: left; text-align: right">
                امضاء
            </div>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            <div style="width: 55%; float: right">
                نام و نام خانوادگی مامور ابلاغ : &nbsp; <strong>
                    <asp:Label ID="lblGiverName" runat="server"></asp:Label>
                    &nbsp; </strong>
            </div>
            <div style="width: 45%; float: left; text-align: right">
                امضاء
            </div>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            محل ابلاغ (سكونت /كار): &nbsp; <strong>
                <asp:Label ID="lblNotificationAddress" runat="server"></asp:Label>
                &nbsp; </strong>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            تاريخ ابلاغ به تمام حروف: &nbsp; <strong>
                <asp:Label ID="lblDeliveryDate" runat="server" Visible="False"></asp:Label>
                &nbsp; </strong>
        </div>
    </div>
    </form>
</body>
</html>
