﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevisionFileClosing.aspx.cs"
    Inherits="SankaWebAppFramework.Board.print.RevisionFileClosing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <style type="text/css" media="print">
        #divPanel
        {
            display: none;
        }
    </style>
    <style type="text/css">
	@font-face {
		font-family: 'IranNastaliq';
		src: url('fonts/IranNastaliq.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'BNazanin';
		src: url('fonts/BNazanin.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	.page
	{
		width:850px;
		margin:0px auto;
		font-family:'BNazanin';
		font-size:13.5pt;
		text-align:right;
		direction:rtl;
		padding:50pt;
	}
	.top-position
	{
		margin-top:-40px;
		width:60%; 
		float:right;
		text-align:center;
            height: 94px;
        }
	.logo-center
	{
		width:80px;
		height:45px;
	}
	
	.logo-right
	{
		width:130px;
		height:80px;
	}
	
	.Nastaligh
	{
		font-family: IranNastaliq; 
	}
	.title1
	{
		font-size:20pt;
		line-height:1pt;
		text-align:center; !important;
	}
	
	.date-and-number-left
	{
		text-align:right;
		width:20%;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
		float:left;
	}
	
	.date-and-number-right
	{
		text-align:right;
		width:20%;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
		float:right;
	}
	
	.paragraph{
		text-align: justify;
		width:100%;
		float:right;
	}
	
	    .style1
        {
            text-align: right;
            direction: rtl;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: Calibri, sans-serif;
            margin-left: .5in;
            margin-right: 0in;
            margin-top: 0in;
            margin-bottom: 10.0pt;
        }
	
	</style>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="EidehPardazanFarazgamArad Co .Ltd" />
    <meta name="keywords" />
    <meta name="description" />
    <title>صورت جلسه مختومه</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divPanel">
        <img id="btnPrint" alt="چاپ" src="/images/icon/printer.png" onclick="window.print();"
            style="cursor: pointer" title="چاپ" />
        <img src="/images/icon/back.png" onclick="history.back()" alt="بازگشت" title="بازگشت"
            style="cursor: pointer" />
    </div>
    <div align="center" class="page">
        <div align="right" style="display: none">
            <img class="logo-right" src="images/arm.png" />
        </div>
        <div style="width: 100%">
            <div class="date-and-number-right">
                <p>
                    <span class="Nastaligh">شماره پرسنلی:&nbsp;</span>
                    <asp:Label ID="lblPersonnelNumber" runat="server"></asp:Label>
                </p>
            </div>
            <div class="top-position">
                <p class="Nastaligh title1">
                    بسمه تعالی</p>
                <img src="images/logo.png" class="logo-center" />
                <p class="Nastaligh title1">
                    صورت جلسه مختومه</p>
            </div>
            <div class="date-and-number-left">
                <p>
                    <span class="Nastaligh">شماره صورت جلسه:&nbsp;</span>
                    <asp:Label ID="lblSerialNumber" runat="server"></asp:Label>
                </p>
                <p>
                    <span class="Nastaligh">تاریخ صورت جلسه:&nbsp;</span>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div style="padding-right: 5px; padding-left: 5px">
            <p class="paragraph">
                &nbsp;جلسه هیات &nbsp;
                <asp:Label ID="lblBoardTypeName" runat="server" style="font-weight: 700"></asp:Label>
                رسیدگی به تخلفات اداری کارمندان &nbsp;<strong><asp:Label ID="lblBoardName" runat="server"></asp:Label></strong>&nbsp;&nbsp;در
                تـاريخ&nbsp;&nbsp;<asp:Label runat="server" ID="lblClosingDate" 
                    Style="font-weight: 700" />
                &nbsp;با حضورسه نفراز اعضا جهت رسيدگي به پرونده &nbsp;
                <asp:Label runat="server" ID="lblGenderTitle" style="font-weight: 700" />&nbsp; <strong>
                    <asp:Label ID="lblAccusedName" runat="server" />
                </strong>مستخدم&nbsp;<strong><asp:Label ID="lblMostkhdemType" runat="server"></asp:Label></strong>&nbsp;
                متولد سال <strong>&nbsp;
                    <asp:Label ID="lblBirthYear" runat="server"></asp:Label>
                    &nbsp; </strong>شماره شناسنامه &nbsp; <strong>
                        <asp:Label ID="lblShenasNo" runat="server">2323</asp:Label>
                    </strong>&nbsp; صادره از &nbsp; <strong>
                        <asp:Label ID="lblSodurPlace" runat="server"></asp:Label>
                        &nbsp; </strong>&nbsp; شماره ملی &nbsp; <strong>
                            <asp:Label ID="lblNationalCode" runat="server"></asp:Label>
                        </strong>فرزند &nbsp; <strong>
                            <asp:Label ID="lblFatherName" runat="server">محمد حسین</asp:Label>
                        </strong>&nbsp; وضعیت تاهل: &nbsp; <strong>
                            <asp:Label ID="lblMarried" runat="server"></asp:Label>
                        </strong>&nbsp; ، مدرک تحصیلی &nbsp; <strong>
                            <asp:Label ID="lblEducationalGrade" runat="server"></asp:Label>
                        </strong>&nbsp; داصورت جلسه گروه/طبقه &nbsp; <strong>
                            <asp:Label ID="lblOrganizationGroup" runat="server"></asp:Label>
                        </strong>&nbsp; با &nbsp; <strong>
                            <asp:Label ID="lblActivityYear" runat="server"></asp:Label>
                        </strong>سال سابقه &nbsp; کار، آخرین پست سازمانی &nbsp; <strong>
                            <asp:Label ID="lblOrganizationPosition" runat="server"></asp:Label>
                        </strong>&nbsp; که در منطقه/بخش/شهرستان &nbsp; <strong>
                            <asp:Label ID="lblRegion" runat="server">فارسان</asp:Label>
                        </strong>&nbsp; استان &nbsp; <strong>
                            <asp:Label ID="lblOstan" runat="server"></asp:Label>
                        </strong>&nbsp; خدمت می نماید و در زمان صدور این صورت جلسه در حالت استخدامی &nbsp;
                <strong>
                    <asp:Label ID="lblEstekhdamStatus" runat="server"></asp:Label>
                </strong>&nbsp; بوده&nbsp; که برا اساس صورت جلسه شماره
                <asp:Label ID="lblPreviousVoteNumber" runat="server" style="font-weight: 700"></asp:Label>&nbsp;مورخ
                <asp:Label ID="lblPreviousVoteDate" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;هیات بدوی <strong>
                    <asp:Label ID="lblPreviousBoardName" runat="server"></asp:Label>
                </strong>&nbsp;به مجازات <strong>
                    <asp:Label ID="lblPreviousVoteDescription" runat="server"></asp:Label>
                            &nbsp;محکوم گردیده است</strong>، تشکیل شد . </p>
            <p class="style1" dir="RTL" style="mso-add-space: auto; unicode-bidi: embed">
                <span style="font-size: 13pt; font-weight: 700" 
                    >
                با ملاحظه اسناد و مدارک موجود در پرونده، هیات <b>به</b> <b>اتفاق آراء</b><span 
                    style="mso-spacerun:yes">&nbsp; </span>پرونده مطروحه را <b>مختومه اعلام</b> 
                می­نماید.<o:p></o:p></span></p>
            <p class="paragraph">
                <strong>گردش كار : </strong>
                <br>
                <p class="paragraph" id="lblGardesheKar" runat="server">
                </p>
            </p>
        </div>
        <div style="border-style: solid; border-width: 1px; width: 100%; margin-top: 30px;
            height: 30px; float: right">
            <div style="width: 95%; float: right">
                <strong>* تذکر </strong>: بدون درج نام و نام خانوادگی و امضاء اعضا حاضر در جلسه
                در پایان گردش کار ، صورت جلسه فاقد اعتبار است .
            </div>
            <div style="width: 5%; float: left; text-align: right">
                <strong>ف:4</strong>
            </div>
        </div>
        <div style="width: 100%; margin-top: 30px; float: right">
            <div style="width: 55%; float: right">
                نام و نام خانوادگی ابلاغ شونده : &nbsp; <strong>
                    <asp:Label ID="lblNotifiedPerson" runat="server"></asp:Label>
                    &nbsp; </strong>
            </div>
            <div style="width: 45%; float: left; text-align: right">
                امضاء
            </div>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            <div style="width: 55%; float: right">
                نام و نام خانوادگی مامور ابلاغ : &nbsp; <strong>
                    <asp:Label ID="lblGiverName" runat="server"></asp:Label>
                    &nbsp; </strong>
            </div>
            <div style="width: 45%; float: left; text-align: right">
                امضاء
            </div>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            محل ابلاغ (سكونت /كار): &nbsp; <strong>
                <asp:Label ID="lblNotificationAddress" runat="server"></asp:Label>
                &nbsp; </strong>
        </div>
        <div style="width: 100%; margin-top: 10px; float: right">
            تاريخ ابلاغ به تمام حروف: &nbsp; <strong>
                <asp:Label ID="lblDeliveryDate" runat="server" Visible="False"></asp:Label>
                &nbsp; </strong>
        </div>
    </div>
    </form>
</body>
</html>
