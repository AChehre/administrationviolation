﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViolationNotification.aspx.cs"
    Inherits="SankaWebAppFramework.Board.print.ViolationNotification" %>

<html>
<head>
    <style type="text/css" media="print">
        #divPanel
        {
            display: none;
        }
    </style>
    <style type="text/css">
	@font-face {
		font-family: 'IranNastaliq';
		src: url('fonts/IranNastaliq.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'BNazanin';
		src: url('fonts/BNazanin.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	.page
	{
		width:980px;
		margin:0px auto;
		font-family:'BNazanin';
		font-size:13pt;
		text-align:right;
		direction:rtl;
	}
	.top-position
	{
		margin-top:-40px;
		width:100%;
		text-align:center;
	}
	.logo-center
	{
		width:80px;
		height:45px;
	}
	
	.logo-right
	{
		width:130px;
		height:80px;
	}
	
	.Nastaligh
	{
		font-family: IranNastaliq; 
	}
	.title1
	{
		font-size:20pt;
		line-height:1pt;
		text-align:center; !important;
	}
	
	.date-and-number
	{
		margin-top:-30px;
		text-align:right;
		width:200px;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
	}
	</style>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="EidehPardazanFarazgamArad Co .Ltd" />
    <meta name="keywords" />
    <meta name="description" />
    <title>ابلاغ اتهام </title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divPanel">
        <img id="btnPrint" alt="چاپ" src="/images/icon/printer.png" onclick="window.print();"
            style="cursor: pointer" title="چاپ" />
        <img src="/images/icon/back.png" onclick="history.back()" alt="بازگشت" title="بازگشت"
            style="cursor: pointer" />
    </div>
    <div align="center" class="page">
        <div align="right">
            <img class="logo-right" src="images/arm.png" />
        </div>
        <div class="top-position">
            <p class="Nastaligh title1">
                بسمه تعالی</p>
            <img src="images/logo.png" class="logo-center" />
            <p class="Nastaligh title1">
                برگ ابلاغ اتهام</p>
        </div>
        <div align="left">
            <div class="date-and-number">
                <p>
                    <span class="Nastaligh">شماره:&nbsp;</span>
                    <asp:Label ID="lblSerialNumber" runat="server"></asp:Label>
                </p>
                <p>
                    <span class="Nastaligh">تاریخ:&nbsp;</span>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div style="padding-right: 5px; padding-left: 5px">
            <table>
                <tbody>
                    <tr>
                        <td colspan="2" class="paragraph">
                            <span><strong>آقای / خانم :&nbsp;</strong> </span>&nbsp;<asp:Label ID="lblAccusedFullName"
                                runat="server" />&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; فرزند&nbsp;&nbsp;&nbsp;
                                </strong>
                            <asp:Label ID="lblFatherName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="paragraph">
                نظر به اينكه پرونده شما به اتهام/ اتهام هاي زير در اين هيأت تحت رسيدگي مي باشد،
                لذا مقتضي است حداكثر ظرف 10 روز از تاريخ رؤيت اين برگ، دفاعيه كتبي خود را به انضمام
                مدارك لازم به اين هيأت به نشاني
                <asp:Label ID="txtBoardAddress" runat="server">
                &nbsp;تسلیم نمایید .</asp:Label></p>
            <p style="text-align: left">
                <strong>رئیس هیات :
                    <asp:Label ID="lblManagerName" runat="server"></asp:Label>
                </strong>
            </p>
            <p class="paragraph">
                موارد اتهام:
            </p>
            <div style="height: 142px">
                <table>
                    <asp:Repeater runat="server" ID="rptAccusitionList">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;">
                                    <strong>
                                        <%# Eval("RowNumber") %>
                                        - </strong>
                                </td>
                                <td>
                                    <strong>
                                        <%# Eval("Description") %>
                                    </strong>
                                </td>
                                <td style="text-align: center;">
                                    <strong>كه با بند </strong>
                                </td>
                                <td style="text-align: center;">
                                    <strong>
                                        <%# Eval("AccusationSubjectID") %></strong>
                                </td>
                                <td>
                                    <strong>ماده 8 قانون رسيدگي به تخلفات اداري &nbsp;&nbsp;&nbsp; منطبق است. </strong>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
            <br>
            <br>
            <table>
                <tbody>
                    <tr>
                        <td class="style5">
                            نام و نام خانوادگی ابلاغ شونده :
                            <asp:Label ID="lblNotifiedPerson" runat="server" CssClass="style4">
                            </asp:Label>
                        </td>
                        <td class="style5">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            امضاء
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style5">
                            نام و نام خانوادگی مامور ابلاغ :
                            <asp:Label ID="lblGiverName" runat="server" CssClass="style4">
                            </asp:Label>
                        </td>
                        <td class="style5">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            امضاء
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style5" colspan="2">
                            محل ابلاغ (سكونت /كار) :
                            <asp:Label ID="lblNotificationAddress" runat="server" CssClass="style4">
                            </asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style5" colspan="2">
                            تاريخ ابلاغ به تمام حروف :
                            <%--<asp:Label ID="lblDeliveryDate" runat="server" CssClass="style4">
                            </asp:Label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style5" colspan="2">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="border-style: solid; border-width: 1px; width: 100%; margin-top: 30px;
            height: 30px">
            <div style="width: 95%; float: right">
                <strong>* تذکر </strong>: * چنانچه در مهلت ياد شده دفاعيه كتبي خود را تسليم ننمايند
                هيأت طبق ماده 18 آيين نامه اجرايي قانون رسيدگي به تخلفات اداري عمل خواهد كرد.
            </div>
            <div style="width: 5%; float: left; text-align: left">
                <strong>ف:1</strong>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
