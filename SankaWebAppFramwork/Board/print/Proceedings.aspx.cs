﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board.print
{
    /// <summary>
    /// تمامی متدها عینا شبیه  متدهای دو فرم قبلی
    /// </summary>
    public partial class Proceedings : System.Web.UI.Page
    {
        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        private long BoardID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("BoardID")))
                {
                    return Request.QueryString.Get("BoardID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی هئیتی که کاربر جاری در آن عضو است
        /// </summary>
        protected long UserBoardID
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardID;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardID = value;
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        protected byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                checkViewFilePermission();
                LoadAccusedInfo(this.FileID);
                LoadBoardMember();
                LoadBoardName();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "key1", "window.print();", true);
            }
        }

        private void LoadBoardName()
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = this.BoardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            lblBoardName.Text = obj.BoardTitle;
        }

        private void LoadBoardMember()
        {
            var lstObj = new List<DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers>();

            var Obj = new DTO.Board.tblViolationFileVoteMembers.tblViolationFileVoteMembers();
            Obj.ViolationFileID = this.FileID;

            lstObj = (new DAL.Board.tblViolationFileVoteMembers.Methods()).SelectByViolationFileID(Obj);
            if (lstObj.Count >= 3)
            {
                lblMember1.Text = lstObj[0].FullName;
                lblMember2.Text = lstObj[1].FullName;
                lblMember3.Text = lstObj[2].FullName;
            }
        }

        private void LoadAccusedInfo(long fileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                long personnelCode = obj.PersonnelCode.Value;
                var objUser = new DTO.Board.tblPersonnelView.tblPersonnelView
                {
                    PersonnelCode = personnelCode
                };
                objUser = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(objUser);
                if (objUser.RowNumber > 0)
                {
                    lblGardesheKar.InnerHtml = obj.WorkflowReport;
                    lblAccusedFullName.Text = obj.FirstName + " " + obj.LastName;
                    lblWroteDate.Text = lblSodurDate.Text = Utility.GetNuemericCustomDateWithoutTime(DateTime.Now);
                    lblAccusedName.Text = objUser.FirstName + " " + objUser.LastName;
                    lblFatherName.Text = obj.FatherName;
                    lblShenasNo.Text = objUser.ShenasNo;
                    lblSodurPlace.Text = objUser.BirthPlace;
                    lblOrganizationGroup.Text = Common.getOtganizationGroupName(objUser.OrganizationGroup);
                    lblOrganizationPosition.Text = Common.getOrganizationPosition(objUser.OrganizationPosition);
                    lblOstan.Text = objUser.OstanName;
                    lblRegion.Text = objUser.RegionName;
                    lblBirthYear.Text = Common.getBirthYear(objUser.FarsiBirthDate);
                    lblActivityYear.Text = objUser.ActivityYears.ToString();
                    lblEducationalGrade.Text = Common.getDegreeLabel(objUser.Education);
                    lblMarrageStatus.Text = objUser.Married ? "متاهل" : "مجرد";
                    lblGenderTitle.Text = Common.getGenderTitleLabel(objUser.Gender);
                }
            }
        }

        private void checkViewFilePermission()
        {
            
                UserProfile profile = Session["UserProfile"] as UserProfile;
                if (profile.UserID == 0)
                {
                    Response.Redirect("~/Login.aspx", true);
                }
            
            if (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                var objBoard = new DTO.Board.tblFileAccessMap.tblFileAccessMap
                {
                    FileID = this.FileID,
                    BoardID = this.UserBoardID
                };
                byte bytViewType = (new DAL.Board.tblFileAccessMap.Methods()).HasPermission(objBoard);
                if (bytViewType == 0)
                {
                    Response.End();
                }
            }
        }
    }
}