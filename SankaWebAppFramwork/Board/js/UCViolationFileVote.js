﻿
$(document).ready(function () {
    VoteType_change();
});

function VoteType_change() {
    if ($("[id$='ddlVoteType']").val() == 0) {
        $("#CondemnationDiv").hide();
    }
    else {
        $("#CondemnationDiv").show();
    }
}

// نمایش فرم چاپی
function ShowPrintForm() {
    $("[id$='btnPrintVote']").click();
    return false;
}

//** نمایش فرم چاپی یک ابلاغیه
function ShowNotificationPrint() {
    $("[id$='btnShowPrintNotification']").click();
    return false;
}