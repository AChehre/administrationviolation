﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Board.Master" AutoEventWireup="true" CodeBehind="UserBoardsList.aspx.cs" Inherits="SankaWebAppFramework.Board.UserBoardsList" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage" ClientIDMode="Static">
    </cc2:SankaDialog>
    <div class="grid">
        <cc1:SankaGrid ID="gvBoards" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف">
                    <HeaderStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="BoardID" HeaderText="BoardID" Visible="False" />
                <asp:BoundField DataField="BoardTitle" HeaderText="نام هیات" />
                <asp:TemplateField HeaderText="نوع هیات">
                    <ItemTemplate>
                        <%# AdministrationViolation.Admin.Enums.Board_Type_Label[Eval("BoardType").ToString().StringToInt()]%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="لیست پرونده ها">
                    <ItemTemplate>
                        <a data-save-return-url="true" href='<%# string.Format("ViolationFiles.aspx?BoardID={0}", Eval("BoardID")) %>' >لیست پرونده ها</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </cc1:SankaGrid>
    </div>
</asp:Content>
