﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Board.Master" AutoEventWireup="true"
    CodeBehind="SetCurrentApplicationYear.aspx.cs" Inherits="SankaWebAppFramework.Board.SetCurrentApplicationYear" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc2:SankaDialog>
    <fieldset>
        <legend>تنظیم سال عملیاتی </legend>
        <div>
            <div class="_10bm">
                <label class="form-field-label">
                    انتخاب سال :</label>
                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="_10bm" style="width: auto">
                <label class="form-field-label">
                    شروع شماره پرونده از :</label>
                <asp:TextBox ID="txtFileSerialCounter" MaxLength="9" runat="server" placeholder="مثال: 001002"></asp:TextBox>
            </div>
            <div class="_10bm" style="width: auto">
                <label class="form-field-label">
                    شروع نامه ابلاغیه اتهام از :</label>
                <asp:TextBox ID="txtViolationNotifivationCounter" MaxLength="9" runat="server" placeholder="مثال: 001002"></asp:TextBox>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    شروع شمارنده رای از:</label>
                <asp:TextBox ID="txtVoteSerialCounter" MaxLength="9" runat="server" placeholder="مثال: 001002"></asp:TextBox>
            </div>
            <div class="_10bm" style="display:none">
                <label class="form-field-label">
                    شروع شمارنده ابلاغیه رای از :</label>
                <asp:TextBox ID="txtVoteNotificationNumber" MaxLength="9" runat="server" placeholder="مثال: 001002"></asp:TextBox>
            </div>
            <div class="_10bm">
                <label class="form-field-label">
                    شروع شمارنده صورت جلسه مختومه از :</label>
                <asp:TextBox ID="txtClosingCounter" MaxLength="9" runat="server" placeholder="مثال: 001002"></asp:TextBox>
            </div>
        </div>
    </fieldset>
</asp:Content>
