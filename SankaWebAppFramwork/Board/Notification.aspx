﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Solid.Master" AutoEventWireup="true"
    CodeBehind="Notification.aspx.cs" Inherits="SankaWebAppFramework.Board.Notification" %>

<%@ Register Src="~/controls/UCAddNotification.ascx" TagName="UCAddNotification"
    TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css" />
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
    <%---------%>
    <link href="/css/UCSelectMultiItems.css" rel="stylesheet" />
    <script src="/js/UCSelectMultiItems.js"></script>
    <%---------%>
    <script type="text/javascript">
        function CloseModal() {
            window.parent.jQuery('#modal_content').dialog('close');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc5:UCAddNotification ID="AddNotification1" runat="server" />
</asp:Content>
