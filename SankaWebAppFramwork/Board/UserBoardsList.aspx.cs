﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class UserBoardsList : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageLabelTitle = "لیست هئیت ها";

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        /// <summary>
        /// نمایش لیست هیات هایی که کاربر جاری در آنها عضو فعال است
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DTO.Board.tblUserBoardsView.tblUserBoardsView obj = new DTO.Board.tblUserBoardsView.tblUserBoardsView();
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.MemberUserID = UserID;
                obj.BoardStatus = (byte)AdministrationViolation.Admin.Enums.Board_Status.Active;
                obj.BoardMemberMapStatus = (byte)AdministrationViolation.Admin.Enums.Board_Member_Status.Active;

                List<DTO.Board.tblUserBoardsView.tblUserBoardsView> RList = (new DAL.Board.tblUserBoardsView.Methods()).SelectAllByPaging(obj);
                gvBoards.CustomDataBind(RList, this.PageSize, this.PageNumber, (int)SankaWebAppFramework.Enums.PageLinkNumber.TenNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = RList.Any() ? RList[0].TotalRow.Value : 0;
            }
            catch (Exception ex)
            {
                LogError(ex,"خطا در نمایش لیست هیات های کاربر.");
            }
        }
    }
}