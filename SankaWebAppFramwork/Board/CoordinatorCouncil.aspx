﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Board.Master" AutoEventWireup="true"
    CodeBehind="CoordinatorCouncil.aspx.cs" Inherits="SankaWebAppFramework.Board.CoordinatorCouncil" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel-group" id="pnlSendFileRequest" runat="server">
        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse3" class="panel-heading pointer-cursor">
                <h4 runat="server" id="h2" class="panel-title">
                    لیست پرونده های ارسال شده توسط دفتر هماهنگی</h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <div class="grid">
                        <cc1:SankaGrid ID="gvFileSendRequest" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="ID" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" SortExpression="RowNumber"
                                    ItemStyle-Width="30px">
                                    <ItemStyle Width="30px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="30px" ReadOnly="True"
                                    Visible="false" SortExpression="ID">
                                    <ItemStyle Width="30px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="FromBoardID" HeaderText="FromBoardID" SortExpression="FromBoardID"
                                    Visible="False" />
                                <asp:BoundField DataField="ToBoardID" HeaderText="ToBoardID" SortExpression="ToBoardID"
                                    Visible="False" />
                                <asp:TemplateField HeaderText="تاریخ" SortExpression="RequestDate">
                                    <ItemTemplate>
                                        <asp:Label data-sankatooltip='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>'
                                            data-title='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>' data-duplicatetitle="false"
                                            style="cursor: pointer" ID="Label1"
                                            Text='<%# Utility.GetNuemericCustomDateWithoutTime(Eval("RequestDate")) %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RequestedFileSerialCode" HeaderText="کلاسه پرونده" SortExpression="RequestedFileSerialCode" />
                                <asp:BoundField DataField="FileID" HeaderText="FileID" Visible="False" />
                                <asp:BoundField DataField="RelatedFileID" HeaderText="RelatedFileID" Visible="False" />
                                <asp:TemplateField HeaderText="توضیحات">
                                    <ItemTemplate>
                                        <%# Eval("Comment") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RequestDataID" HeaderText="RequestDataID" Visible="False"
                                    SortExpression="RequestDataID" />
                                <asp:TemplateField HeaderText="هیات مبداء">
                                    <ItemTemplate>
                                        <%# Eval("FromBoardTitle") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ToBoardTitle" HeaderText="هیات مقصد" />
                                <asp:TemplateField HeaderText="وضعیت">
                                    <ItemTemplate>
                                        <img data-title='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatuslabel[Convert.ToByte(Eval("Status"))] %>'
                                            data-duplicatetitle="false"
                                            style="cursor: pointer" src='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatusIconSrc[Convert.ToByte(Eval("Status"))] %>'
                                            data-sankatooltip='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatuslabel[Convert.ToByte(Eval("Status"))] %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="مشاهده">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkDetails" runat="server" ImageUrl="/images/icon/index_add.png"
                                            NavigateUrl='<%# string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}",Eval("RelatedFileID"),Eval("ToBoardID")) %>'
                                            Target="_blank" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <span>رکوردی جهت نمایش موجود نیست .</span>
                            </EmptyDataTemplate>
                        </cc1:SankaGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
