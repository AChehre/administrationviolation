﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Board/Board.Master"
    AutoEventWireup="true" CodeBehind="ViolationFileDependencies.aspx.cs" Inherits="SankaWebAppFramework.Board.ViolationFileDependencies" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCAddViolationFile.ascx" TagName="UCAddViolation" TagPrefix="uc1" %>
<%@ Register Src="~/controls/UCAddAccusition.ascx" TagName="UCAddAccusition" TagPrefix="uc3" %>
<%@ Register Src="~/controls/UCAddNotification.ascx" TagName="UCAddNotification"
    TagPrefix="uc4" %>
<%@ Register Src="~/controls/UCAccusedInfo.ascx" TagName="UCAccusedInfo" TagPrefix="uc8" %>
<%@ Register Src="~/controls/UCOtherRequest.ascx" TagName="UCOtherRequest" TagPrefix="uc7" %>
<%@ Register Src="~/controls/UCViolationFileVote.ascx" TagName="UCViolationFileVote"
    TagPrefix="uc5" %>
<%@ Register Src="~/controls/UCAllocationMembers.ascx" TagName="UCAllocationMembers"
    TagPrefix="uc9" %>
<%@ Register Src="~/controls/UCPreviewVote.ascx" TagName="UCPreviewVote" TagPrefix="uc10" %>
<%@ Register Src="~/controls/UCFileSendingRequest.ascx" TagName="UCFileSendingRequest"
    TagPrefix="uc2" %>
<%@ Register Src="../controls/UCFurtherInvestigationOrder.ascx" TagName="UCFurtherInvestigationOrder"
    TagPrefix="uc6" %>
<%@ Register Src="../controls/UCVoteNotification.ascx" TagName="UCVoteNotification"
    TagPrefix="uc11" %>
<%@ Register Src="~/controls/UCChangeFileBoard.ascx" TagName="UCChangeFileBoard"
    TagPrefix="uc12" %>
<%@ Register Src="../controls/UCEjectionNotification.ascx" TagName="UCEjectionNotification"
    TagPrefix="uc13" %>
<%@ Register Src="../controls/UCCloseAllocationMembers.ascx" TagName="UCCloseAllocationMembers"
    TagPrefix="uc14" %>
<%@ Register Src="../controls/UCPreviewClose.ascx" TagName="UCPreviewClose" TagPrefix="uc15" %>
<%@ Register Src="../controls/UCViolationFileClosing.ascx" TagName="UCViolationFileClosing"
    TagPrefix="uc16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="/css/AutoComplete.css" rel="stylesheet" type="text/css" />
    <script src="/js/AutoComplete.js" type="text/javascript"></script>
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css" />
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
    <%---------%>
    <link href="/css/UCSelectMultiItems.css" rel="stylesheet" />
    <script src="/js/UCSelectMultiItems.js"></script>
    <%---------%>
    <script src="js/UCViolationFileVote.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet" />
   <%-- <script src="/sankaeditor/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/sankaeditor/ckeditor/adapters/jquery.js" type="text/javascript"></script>
    <script src="/sankaeditor/scripts/UCSankaEditor.js"></script>--%>
    <script src="/js/UCPreviewVote.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc2:SankaDialog>
    <div class="panel-group" id="pnlRequestsList" runat="server" visible="false">
        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse1" class="panel-heading pointer-cursor">
                <h4 runat="server" id="h1" class="panel-title">
                    لیست اقدامات مرتبط با این پرونده</h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <asp:Panel runat="server" ID="pnlMainRequest" Style="margin-bottom: 2px; padding-right: 0px">
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSubsidiaryRequest" Style="margin-bottom: 2px; padding-right: 0px">
                    </asp:Panel>
                    <span></span>
                    <asp:Panel runat="server" ID="pnlBasicRequest" Style="margin-bottom: 2px; padding-right: 0px">
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <div style="overflow: hidden">
        <asp:HyperLink class="btn btn-success" Style="float: left; margin: 5px; display: none"
            ID="lnkViolationDetail" runat="server">پرونده در یک نگاه</asp:HyperLink>
        <span id="sanka_basic_modal">
            <asp:HyperLink data-sankatooltip="راهنما" data-title="راهنما" data-duplicatetitle="false"
                data-width="800" data-height="600" class="btn btn-primary basic" Style="float: left;
                margin: 5px;" ID="lnkHelp" runat="server">راهنما</asp:HyperLink>
        </span>
        <asp:HyperLink data-sankatooltip="سایر پرونده های متهم" data-title="سایر پرونده های متهم"
            class="btn btn-primary basic" Style="float: left; margin: 5px;" ID="lnkAccusedOtherFile"
            Target="_blank" runat="server">سایر پرونده های متهم</asp:HyperLink>
        <%--<span id="sanka_basic_modal">
            <asp:HyperLink data-sankatooltip="فرم چاپی صدور رای" data-title="فرم چاپی صدور رای"
                data-duplicatetitle="false" data-width="920" data-height="600" class="btn btn-primary basic"
                Visible="false" Style="float: left; margin: 5px;" ID="lnkPrintVote" runat="server"
                ImageUrl="/images/icon/printer.png"></asp:HyperLink>
        </span>--%><asp:ImageButton runat="server" ID="btnUnlock" Visible="false" ImageUrl="~/images/icon/stock_lock_open.png" data-sankatooltip="فعال سازی قابلیت ویرایش"
            data-title="فعال سازی قابلیت ویرایش" class="btn btn-primary basic" Style="float: left;
            margin: 5px;" OnClick="btnUnlock_Click" />
    </div>
    <div class="panel-group" id="pnlRelatedFiles" runat="server">
        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse2" class="panel-heading pointer-cursor">
                <h4 runat="server" id="h3" class="panel-title">
                    لیست پرونده های مرتبط با این پرونده</h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <asp:PlaceHolder runat="server" ID="phRelatedFiles"></asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <asp:MultiView ID="MultiView1" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged"
        runat="server">
        <asp:View ID="View1" runat="server">
            <uc1:UCAddViolation ID="AddViolation1" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <uc8:UCAccusedInfo ID="UCAccusedInfo1" runat="server" />
        </asp:View>
        <asp:View ID="View3" runat="server">
            <uc3:UCAddAccusition ID="AddAccusition1" runat="server" />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <uc4:UCAddNotification ID="AddNotification1" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <uc7:UCOtherRequest ID="UCOtherRequest1" runat="server" />
        </asp:View>
        <asp:View ID="View6" runat="server">
            <uc9:UCAllocationMembers ID="UCAllocationMembers1" runat="server" />
        </asp:View>
        <asp:View ID="View7" runat="server">
            <uc10:UCPreviewVote ID="UCPreviewVote1" runat="server" />
        </asp:View>
        <asp:View ID="View8" runat="server">
            <uc5:UCViolationFileVote ID="UCViolationFileVote1" runat="server" />
        </asp:View>
        <asp:View ID="View9" runat="server">
            <uc2:UCFileSendingRequest ID="UCFileSendingRequest1" runat="server" />
        </asp:View>
        <asp:View ID="View10" runat="server">
            <uc6:UCFurtherInvestigationOrder ID="UCFurtherInvestigationOrder1" runat="server" />
        </asp:View>
        <asp:View ID="View11" runat="server">
            <uc11:UCVoteNotification ID="UCVoteNotification1" runat="server" />
        </asp:View>
        <asp:View ID="View12" runat="server">
            <uc12:UCChangeFileBoard ID="UCChangeFileBoard1" runat="server" />
        </asp:View>
        <asp:View ID="View13" runat="server">
            <uc13:UCEjectionNotification ID="UCEjectionNotification1" runat="server" />
        </asp:View>
        <asp:View ID="View14" runat="server">
            <uc14:UCCloseAllocationMembers ID="UCCloseAllocationMembers1" runat="server" />
        </asp:View>
        <asp:View ID="View15" runat="server">
            <uc15:UCPreviewClose ID="UCPreviewClose1" runat="server" />
        </asp:View>
        <asp:View ID="View16" runat="server">
            <uc16:UCViolationFileClosing ID="UCViolationFileClosing1" runat="server" />
        </asp:View>
    </asp:MultiView>
    <div class="panel-group" id="pnlDoneRequest" runat="server">
        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse3" class="panel-heading pointer-cursor">
                <h4 runat="server" id="hTitle" class="panel-title">
                    لیست اقدامات صورت گرفته بر رروی این پرونده</h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <div class="grid">
                        <cc1:SankaGrid ID="gvRequestDataList" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="RequestDataID">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ItemStyle-Width="30px" SortExpression="RowNumber">
                                    <ItemStyle Width="30px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="عنوان">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkbtnTitle" runat="server" CommandArgument="Title" CommandName="ASC"
                                            data-sankatooltip="مرتب سازی براساس عنوان" OnCommand="lnkbtnSort_Command" ToolTip="مرتب سازی براساس عنوان">عنوان</asp:LinkButton>
                                        <asp:Image ID="imgTitle" runat="server" Height="10" ImageUrl="/images/sort_up.png"
                                            Visible="false" Width="10" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Title") %>
                                    </ItemTemplate>
                                    <ItemStyle Width="180px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Note" HeaderText="توضیحات" SortExpression="Note" />
                                <asp:TemplateField HeaderText="وضعیت">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkbtnStatus" runat="server" CommandArgument="Status" CommandName="ASC"
                                            data-sankatooltip="مرتب سازی براساس وضعیت" OnCommand="lnkbtnSort_Command" ToolTip="مرتب سازی براساس وضعیت">وضعیت</asp:LinkButton>
                                        <asp:Image ID="imgStatus" runat="server" Height="10" ImageUrl="/images/sort_up.png"
                                            Visible="false" Width="10" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <img data-title='<%# SankaWebAppFramework.Board.Enums.RequestStatuslabel[Convert.ToByte(Eval("Status"))] %>'
                                            data-duplicatetitle="false" style="cursor: pointer" src='<%# SankaWebAppFramework.Board.Enums.RequestStatusIconSrc[Convert.ToByte(Eval("Status"))] %>'
                                            data-sankatooltip='<%# SankaWebAppFramework.Board.Enums.RequestStatuslabel[Convert.ToByte(Eval("Status"))] %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="FileID" HeaderText="FileID" SortExpression="FileID" Visible="False" />
                                <asp:BoundField DataField="RequestDataID" HeaderText="RequestDataID" ReadOnly="True"
                                    SortExpression="RequestDataID" Visible="False" />
                                <asp:BoundField DataField="RequestID" HeaderText="RequestID" SortExpression="RequestID"
                                    Visible="False" />
                                <asp:BoundField DataField="PrequesitRequestDataID" HeaderText="PrequesitRequestDataID"
                                    SortExpression="PrequesitRequestDataID" Visible="False" />
                                <asp:TemplateField HeaderText="تاریخ ثبت">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkbtnRequestDate" runat="server" CommandArgument="RequestDate"
                                            CommandName="ASC" data-sankatooltip="مرتب سازی براساس تاریخ ثبت" OnCommand="lnkbtnSort_Command"
                                            ToolTip="مرتب سازی براساس تاریخ ثبت">تاریخ ثبت</asp:LinkButton>
                                        <asp:Image ID="imgRequestDate" runat="server" Height="10" ImageUrl="/images/sort_up.png"
                                            Visible="false" Width="10" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" data-duplicatetitle="false" data-sankatooltip='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>'
                                            data-title='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>' Style="cursor: pointer"
                                            Text='<%# Utility.GetNuemericCustomDateWithoutTime(Eval("RequestDate")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="110px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="اقدام کننده">
                                    <ItemTemplate>
                                        <%# Eval("FName") + " " + Eval("LName") %>
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="جزئیات" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnAttachment" runat="server" CommandArgument='<%# Eval("RequestID") + "_" + Eval("RequestDataID") + "_" + Eval("RelatedAttachementTypes") %>'
                                            ImageUrl="~/images/icon/index_add.png" OnCommand="btnAttachment_Command" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="حذف">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDeleteRequest" runat="server" CommandArgument='<%# Eval("RequestID") + "_" + Eval("RequestDataID") %>'
                                            ImageUrl="~/images/icon/delete.png" OnClientClick="return ShowConfirm(this);"
                                            OnCommand="btnDeleteRequest_Command" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <center>
                                    رکوردی موجود نیست</center>
                            </EmptyDataTemplate>
                        </cc1:SankaGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group" id="pnlSendFileRequest" runat="server">
        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse4" class="panel-heading pointer-cursor">
                <h4 runat="server" id="h2" class="panel-title">
                    دستورات رسیدگی مجدد این پرونده</h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <div class="grid">
                        <cc1:SankaGrid ID="gvFileSendRequest" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="ID" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ItemStyle-Width="30px">
                                    <ItemStyle Width="30px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Comment" HeaderText="توضیحات" SortExpression="Comment" />
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                    SortExpression="ID" Visible="False" />
                                <asp:BoundField DataField="FromBoardID" HeaderText="FromBoardID" Visible="False" />
                                <asp:BoundField DataField="ToBoardID" HeaderText="ToBoardID" Visible="False" />
                                <asp:TemplateField HeaderText="وضعیت">
                                    <ItemTemplate>
                                        <img data-title='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatuslabel[Convert.ToByte(Eval("Status"))] %>'
                                            data-duplicatetitle="false" style="cursor: pointer" src='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatusIconSrc[Convert.ToByte(Eval("Status"))] %>'
                                            data-sankatooltip='<%# SankaWebAppFramework.Board.Enums.SendFileRequestStatuslabel[Convert.ToByte(Eval("Status"))] %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="تاریخ ثبت" SortExpression="RequestDate">
                                    <ItemTemplate>
                                        <asp:Label data-sankatooltip='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>'
                                            data-title='<%# Utility.GetCustomDateWithoutTime(Eval("RequestDate")) %>' data-duplicatetitle="false"
                                            Style="cursor: pointer" ID="Label1" Text='<%# Utility.GetNuemericCustomDateWithoutTime(Eval("RequestDate")) %>'
                                            runat="server" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="RequestedFileSerialCode" HeaderText="RequestedFileSerialCode"
                                    SortExpression="RequestedFileSerialCode" Visible="False" />
                                <asp:BoundField DataField="FileID" HeaderText="FileID" SortExpression="FileID" Visible="False" />
                                <asp:BoundField DataField="RelatedFileID" HeaderText="RelatedFileID" Visible="False" />
                                <asp:BoundField DataField="RequestDataID" HeaderText="RequestDataID" Visible="False" />
                                <asp:TemplateField HeaderText="جزئیات">
                                    <ItemTemplate>
                                        <span id="sanka_basic_modal"><a href='<%# string.Format("/Board/SendingFileRequstDetails.aspx?ID={0}", Eval("ID")) %>'
                                            data-sankatooltip="مشاهده جزئیات" data-title="دستور رسیدگی مجدد پرونده" data-duplicatetitle="false"
                                            style="cursor: pointer" class="basic" data-width="700" data-height="400" />
                                            <img id="img1" class="cp_vm" alt="" src="/images/icon/index_add.png" />
                                        </span>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <center>
                                    رکوردی موجود نیست</center>
                            </EmptyDataTemplate>
                        </cc1:SankaGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
