﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class CoordinatorCouncil : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        /// <summary>
        /// لیست پرونده ها ی ارسال شده توسط دفتر هماهنگی
        /// </summary>
        private void BindGrid()
        {
            var obj = new DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView
            {
                ViewType = (byte)Enums.SendingFileRequestViewType.COORDINATOR
            };
            gvFileSendRequest.DataSource = (new DAL.Board.tblSendingFileRequest_BoardView.Methods()).SelectByBoardID(obj);
            gvFileSendRequest.DataBind();
        }
    }
}