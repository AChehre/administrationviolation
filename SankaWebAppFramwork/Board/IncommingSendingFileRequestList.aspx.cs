﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class IncommingSendingFileRequestList : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        /// <summary>
        /// لیست درخواست های رسیده جهت ارسال پرونده از این هیات
        /// </summary>
        private void BindGrid()
        {
            var obj = new DTO.Board.tblSendingFileRequest_BoardView.tblSendingFileRequest_BoardView
            {
                BoardID = this.UserBoardID,
                ViewType = (byte)Enums.SendingFileRequestViewType.INCOMMING
            };
            gvFileSendRequest.DataSource = (new DAL.Board.tblSendingFileRequest_BoardView.Methods()).SelectByBoardID(obj);
            gvFileSendRequest.DataBind();
        }
    }
}