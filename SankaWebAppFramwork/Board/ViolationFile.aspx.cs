﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace SankaWebAppFramework.Board
{
    public partial class ViolationFile : GeneralPage
    {
        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long BoardID
        {
            get
            {
                return UserBoardID;
            }
        }

        /// <summary>
        /// آیا کاربر عضو  هیات هست یا نه
        /// </summary>
        protected bool IsCurrentUserBoardMember
        {
            get
            {
                if (ViewState["IsBoardMember"] != null)
                {
                    return ViewState["IsBoardMember"].ToString().StringToBool();
                }

                return false;
            }

            set
            {
                ViewState.Add("IsBoardMember", value);
            }
        }

        /// <summary>
        /// وضعیت پرونده
        /// </summary>
        private byte ViolationFileStatus
        {
            set
            {
                ViewState.Add("ViolationFileStatus", value);
            }
            get
            {
                return ViewState["ViolationFileStatus"].ToString().StringToByte();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                IsCurrentUserBoardMember = CheckUserToAccessToBoard(BoardID, "~/Board/UserBoardsList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View);

                #endregion چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                if (this.FileID > 0)
                {
                    AccusedInfo1.FileID = FileID;

                    LoadAccusitionRecoards(FileID);
                    LoadNotificationRecorad(FileID);
                    LoadFileAttachment(FileID);
                }

                #region عدم نمایش ستون های در گریدها عملیات، هنگامی که کاربر غیر عضو صفحه را میبیند

                bool VisibleStatus = IsCurrentUserBoardMember;

                if (gvAccusition.Columns.Count > 0)
                {
                    gvAccusition.Columns[gvAccusition.Columns.Count - 1].Visible = VisibleStatus;
                }
                if (gvUplodedFiles.Columns.Count > 0)
                {
                    gvUplodedFiles.Columns[gvUplodedFiles.Columns.Count - 1].Visible = VisibleStatus;
                }

                #endregion عدم نمایش ستون های در گریدها عملیات، هنگامی که کاربر غیر عضو صفحه را میبیند
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ViolationFileStatus = AccusedInfo1.ViolationFileStatus;
            PageLabelTitle = Common.getFileStatusLabel(this.FileID);
            BindActionMenu();
        }

        /// <summary>
        /// بارگذاری ضمائم در گردید مربوطه
        /// </summary>
        /// <param name="FileID"></param>
        private void LoadFileAttachment(long FileID)
        {
            try
            {
                DTO.Board.tblFileAttachment.tblFileAttachment obj
                    = new DTO.Board.tblFileAttachment.tblFileAttachment();
                obj.ViolationFileID = FileID;

                List<DTO.Board.tblFileAttachment.tblFileAttachment> lst =
                    (new DAL.Board.tblFileAttachment.Methods()).SelectAll(obj);

                gvUplodedFiles.DataSource = (lst.Count > 0) ? lst : null;
                gvUplodedFiles.DataBind();
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش لیست پیوست ها.");
            }
        }

        /// <summary>
        /// بارگذاری لیست ابلاغیه ها در گرید مربوطه
        /// </summary>
        /// <param name="FileID"></param>
        private void LoadNotificationRecorad(long FileID)
        {
            try
            {
                DTO.Board.tblViolationNotification.tblViolationNotification obj = new DTO.Board.tblViolationNotification.tblViolationNotification();
                obj.ViolationFileID = FileID;
                List<DTO.Board.tblViolationNotification.tblViolationNotification> RList = (new DAL.Board.tblViolationNotification.Methods()).SelectByViolationFileID(obj);
                gvNotification.DataSource = RList;
                gvNotification.DataBind();

            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش لیست ابلاغیه ها.");
            }
        }

        /// <summary>
        /// بارگذاری لیست اتهامات در گردید مربوطه
        /// </summary>
        /// <param name="FileID"></param>
        private void LoadAccusitionRecoards(long FileID)
        {
            try
            {
                DTO.Board.tblAccusitionView.tblAccusitionView obj = new DTO.Board.tblAccusitionView.tblAccusitionView();
                obj.ViolationFileID = FileID;
                List<DTO.Board.tblAccusitionView.tblAccusitionView> RList = (new DAL.Board.tblAccusitionView.Methods()).SelectByFileID(obj);
                gvAccusition.DataSource = RList;
                gvAccusition.DataBind();
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش لیست اتهامات.");
            }
        }

        /// <summary>
        /// باید کردن منو و دکمه ها
        /// </summary>
        private void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (!IsCurrentUserBoardMember)
            {//  اگر کاربر به نوعی ناظر بود که عضو هیات نبود، فقط دکمه بازگشت ساخته شود
                //  دکمه بازگشت
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnCancelView",
                    Title = "بازگشت",
                    Type = ActionMenuType.HyperLink,
                    Icon = "/images/icon/back.png",
                    OnClientClick = "GoBackUrl(this);",
                    NavigateUrl = "/board/ViolationFiles.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View + "&BoardID=" + BoardID
                });
            }
            else
            {
                //  دکمه بازگشت
                lstAction.Add(new ActionMenuModel()
                {
                    TargetID = "btnCancelView",
                    Title = "عملیات روی پرونده",
                    Type = ActionMenuType.HyperLink,
                    Icon = "/images/icon/back.png",
                    NavigateUrl = "/board/ViolationFileDependencies.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View + "&FileID=" + FileID
                });
            }

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();

            // چون تابع رو در لود صفحه فراخوانی نکردیم، باید دستی فانکشن ساخت منو را فراخوانی کنیم
            UCActionMenu1.CreateMenu();
        }
    }
}