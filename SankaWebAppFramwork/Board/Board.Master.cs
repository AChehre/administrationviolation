﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board
{
    public partial class Board : System.Web.UI.MasterPage
    {
        /// <summary>
        /// گرفتن شناسه های عضویت یک کاربر در گروههای کاربری
        /// </summary>
        protected List<DTO.Config.tblUserGroupMap.tblUserGroupMap> lstGroupIDs
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).lstGroupIDs;
                }
                catch
                {
                    return new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>(); ;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMenus.Controls.Add(CreateMenu());
            if (!IsPostBack)
            {
                UserProfile profile = (Session["UserProfile"] as UserProfile);
                if (profile != null)
                {
                    lblApplicationYear.Text = " سال عملیاتی" + profile.CurrentApplicationYear;
                }
            }
        }

        protected void ibtnLogOut_Click(object sender, EventArgs e)
        {
            ((GeneralPage)this.Page).Logout();

            Session.RemoveAll();

            #region ثبت بازدید کننده به عنوان عضوی از گروه عمومی

            UserProfile profile = new UserProfile();
            profile.lstGroupIDs = new List<DTO.Config.tblUserGroupMap.tblUserGroupMap>();
            profile.lstGroupIDs.Add(new DTO.Config.tblUserGroupMap.tblUserGroupMap
            {
                UserGroupID = SankaWebAppFramework.Common.PublicGroupID
            });

            // اگر ترو باشد، یعنی زمان یوزر تمام شده است و تازه سشن ست شده است
            // در چک کردن پرمیژن صفحات در زمان پست بک ها بکار می آید
            profile.UserSessionIsNew = true;

            Session["UserProfile"] = profile;

            #endregion ثبت بازدید کننده به عنوان عضوی از گروه عمومی

            //** پاک کردن کوکی نگهدارنده آدرس های بازگشت
            SankaWebAppFramework.Common.RemoveReturnUrlCookie();

            Session["Message"] = "شما با موفقیت از سامانه خارج شدید .";
            Response.Redirect("~");
        }

        /// <summary>
        /// ساخت منو به صورت داینامیک
        /// </summary>
        protected HtmlGenericControl CreateMenu()
        {
            #region بدست آوردن تمام منوهای قابل دسترس

            List<DTO.Config.tblMenu.tblMenu> lstMenu = new List<DTO.Config.tblMenu.tblMenu>();
            lstMenu = (new DAL.Config.tblMenu.Methods()).SelectAll_Published();

            #endregion بدست آوردن تمام منوهای قابل دسترس

            #region بدست آوردن تمام سطوح دسترسی منوها

            List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> lstPermission = new List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap>();
            lstPermission = (new DAL.Config.tblUserGroupMenuMap.Methods()).SelectAll();

            #endregion بدست آوردن تمام سطوح دسترسی منوها

            // ساخت زیرمنوهای ،منوهای پدر
            // قرارداد: اگر پرنت آیدی منو برابر منفی یک بود، یعنی منوی سطح یک و پدر است
            return CreateSubMenu(lstMenu, lstPermission, null, true);

        }

        /// <summary>
        /// ساخت زیر منوهای یک منو
        /// </summary>
        /// <param name="lstMenu">لیست تمام منوها</param>
        /// <param name="lstPermission">سطوح دسترسی منوها</param>
        /// <param name="ParentID">آیدی منویی که میخواهیم ساب منوهای آنرا بسازیم، هنگام صدازدن تابع برای اولین بار، مقدار منفی یک میدهیم</param>
        /// <param name="IsFirstCall">فقط وقتی که اولین بار این تابع فراخوانی میشود، باید ترو باشد ، تا کلاس تگ یو ال اصلی منو ست شود</param>
        protected HtmlGenericControl CreateSubMenu(List<DTO.Config.tblMenu.tblMenu> lstMenu, List<DTO.Config.tblUserGroupMenuMap.tblUserGroupMenuMap> lstPermission, int? ParentID, bool IsFirstCall)
        {
            // بدست آوردن تمام منوهایی که زیر منوی ، منوی ورودی تابع هستند و باید آنها را بسازیم
            var lstParentMenu = lstMenu.Where(x => x.ParentID == ParentID).ToList();

            HtmlGenericControl MasterUl = new HtmlGenericControl("ul");
            if (IsFirstCall)
            {//اگر اولین بار بود تابع  فراخوانی میشد،کلاس یو ال اصلی ست شود
                MasterUl.Attributes.Add("class", "sf-menu");
            }

            foreach (DTO.Config.tblMenu.tblMenu Menu in lstParentMenu)
            {// ساخت تگ های ال آی و لینک های ساب منوها

                if (lstPermission.Count(x => x.MenuID == Menu.MenuID && lstGroupIDs.Count(g => g.UserGroupID == x.UserGroupID) > 0) < 1)
                {// اگر سطح دسترسی منو را نداشت، منو ساخته نشود
                    continue;
                }

                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl Anchor = new HtmlGenericControl("a");
                li.Controls.Add(Anchor);
                li.Attributes.Add("title", Menu.Title);
                Anchor.InnerText = Menu.Title;
                Anchor.Attributes.Add("href", Menu.Link);
                Anchor.Attributes.Add("title", Menu.Title);

                if (lstMenu.Count(x => x.ParentID == Menu.MenuID) > 0)
                {// اگر زیر منو داشته باشد، این تابع به صورت بازگشتی دوباره فراخوانی میشود

                    var child = CreateSubMenu(lstMenu, lstPermission, Menu.MenuID.Value, false);
                    if (child != null)
                    {
                        li.Controls.Add(child);
                    }
                }

                // اضافه کردن به تگ یو ال پدر
                MasterUl.Controls.Add(li);
            }

            if (MasterUl.Controls.Count < 1)
            {
                return null;
            }

            return MasterUl;
        }
    }
}