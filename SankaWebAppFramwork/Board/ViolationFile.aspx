﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Board/Board.Master" AutoEventWireup="true" CodeBehind="ViolationFile.aspx.cs" Inherits="SankaWebAppFramework.Board.ViolationFile" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="sanka" TagName="AutoComplete" Src="~/controls/AutoComplete.ascx" %>
<%@ Register Src="~/controls/UCAccusedInfo.ascx" TagName="AccusedInfo" TagPrefix="ucAccused" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/screenLock/css/screenLock.css" rel="stylesheet" type="text/css" />
    <link href="/SankaModal/css/SankaModal.css" rel="stylesheet" type="text/css" />
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/screenLock/js/screenLock.js" type="text/javascript"></script>
    <script src="/SankaModal/js/SankaModal.js" type="text/javascript"></script>
    <link href="/css/bootstrap.css" rel="stylesheet" />
    <%--    <link href="/css/AutoComplete.css" rel="stylesheet" type="text/css" />
    <script src="/js/AutoComplete.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCActionMenu ID="UCActionMenu1" runat="server" />

    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage" ClientIDMode="Static">
    </cc2:SankaDialog>

    <div class="panel-group" id="accordion">

        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse1" class="panel-heading pointer-cursor">
                <h4 class="panel-title">مشخصات شخص مورد اتهام</h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <ucAccused:AccusedInfo ID="AccusedInfo1" runat="server" />
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse2" class="panel-heading pointer-cursor">
                <h4 class="panel-title">لیست اتهامات</h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="grid">
                        <cc1:SankaGrid ID="gvAccusition" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" SortExpression="RowNumber" />
                                <asp:TemplateField HeaderText="موضوع">
                                    <ItemTemplate>
                                        <span id="sanka_basic_modal">
                                            <a style="color: blue" href='<%# string.Format("/Board/Accusition.aspx?AccID={0}&FileID={1}&BoardID={2}",Eval("AccusationID"),Eval("ViolationFileID"),BoardID) %>'
                                                data-sankatooltip="مشاهده جزئیات اتهام" data-title="مشاهده جزئیات اتهام"
                                                data-duplicatetitle="false" data-width="900" data-height="570" class='basic'>
                                                <%# Eval("Title") %>
                                            </a>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AccusationID" HeaderText="AccusationID" Visible="False" />
                                <asp:TemplateField HeaderText="تاریخ اعلام گزارش" SortExpression="AccusationDate">
                                    <ItemTemplate>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("AccusationDate")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="تاریخ ثبت اتهام در سیستم" SortExpression="RegisterDate">
                                    <ItemTemplate>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("RegisterDate")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="وضعیت" SortExpression="Status">
                                    <ItemTemplate>
                                        <%# SankaWebAppFramework.Board.Enums.strAccusationStatusLabels[Eval("Status").ToString().StringToByte()] %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--                                <asp:TemplateField HeaderText="عملیات">
                                    <ItemTemplate>
                                        <asp:Button ID="btnConfirm" Enabled='<%# Eval("Status").ToString().StringToByte() == (byte)SankaWebAppFramework.Board.Enums.AccusationStatus.CONFIRMED ? false:true %>' OnClientClick="return ShowConfirm(this,'آیا از تایید این اتهام مطمئن هستید؟')" OnCommand="changeStatusCommand" CommandArgument='<%# Eval("AccusationID")%>' CommandName='<%# (byte)SankaWebAppFramework.Board.Enums.AccusationStatus.CONFIRMED %>' CssClass="btn btn-success" runat="server" Text="تایید اتهام" />
                                        <asp:Button ID="btnNotConfirm" Enabled='<%# Eval("Status").ToString().StringToByte() == (byte)SankaWebAppFramework.Board.Enums.AccusationStatus.NOT_CONFIRMED ? false:true %>' OnClientClick="return ShowConfirm(this,'آیا از رد این اتهام مطمئن هستید؟')" OnCommand="changeStatusCommand" CommandArgument='<%# Eval("AccusationID")%>' CommandName='<%# (byte)SankaWebAppFramework.Board.Enums.AccusationStatus.NOT_CONFIRMED %>' CssClass="btn btn-danger" runat="server" Text="رد اتهام" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <%--<asp:TemplateField HeaderText="حذف" ShowHeader="False">
                            <ItemTemplate>
                                <center>
                                    <asp:ImageButton ID="btnDelete" OnClientClick="return ShowConfirm(this);" runat="server"
                                        CausesValidation="false" CommandName="Delete"
                                        ImageUrl="~/images/icon/delete.png" Text="حذف" />
                                </center>
                            </ItemTemplate>
                            <HeaderStyle />
                        </asp:TemplateField>--%>
                            </Columns>
                            <EmptyDataTemplate>
                                رکوردی موجود نیست .
                            </EmptyDataTemplate>
                        </cc1:SankaGrid>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse3" class="panel-heading pointer-cursor">
                <h4 class="panel-title">ابلاغیه ها</h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="grid">
                        <cc1:SankaGrid ID="gvNotification" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" SortExpression="RowNumber" />
                                <asp:BoundField DataField="NotificationID" HeaderText="NotificationID" SortExpression="NotificationID"
                                    Visible="False" />
                                <asp:BoundField DataField="ViolationFileID" HeaderText="ViolationFileID" SortExpression="ViolationFileID"
                                    Visible="False" />
                                <asp:TemplateField HeaderText="تاریخ ابلاغیه" SortExpression="NotificationDate">
                                    <ItemTemplate>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("NotificationDate")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="نوع ابلاغ" SortExpression="NotificationType">
                                    <ItemTemplate>
                                        <%# SankaWebAppFramework.Board.Enums.strNotificationType[Eval("NotificationType").ToString().StringToByte()] %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="تاریخ ابلاغ به متهم" SortExpression="DeliveryDate">
                                    <ItemTemplate>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("DeliveryDate"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="GiverName" HeaderText="نام گیرنده" SortExpression="GiverName" />
                                <asp:BoundField DataField="RecipientName" HeaderText="نام مامور ابلاغ" SortExpression="RecipientName" />
                                <asp:BoundField DataField="RecipientRelationship" HeaderText="نسبت با متهم" SortExpression="RecipientRelationship"
                                    Visible="False" />
                                <asp:BoundField DataField="LetterSerial" HeaderText="شماره ابلاغیه" SortExpression="LetterSerial" />
                                <asp:TemplateField HeaderText="لایحه دفاعیه">
                                    <ItemTemplate>
                                        <span data-sankatooltip='<%# SankaWebAppFramework.Board.Enums.ReplyBillLabels[Eval("ReplyBill").ToString().StringToByte()] %>'>
                                                <%# SankaWebAppFramework.Board.Enums.ReplyBillLabels[Eval("ReplyBill").ToString().StringToByte()].ShowSummary(5) %>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="جزئیات">
                                    <ItemTemplate>
                                        <span id="sanka_basic_modal">
                                            <a href='<%# string.Format("/Board/Notification.aspx?NotifyID={0}&FileID={1}&BoardID={2}",Eval("NotificationID"),Eval("ViolationFileID"),BoardID) %>'
                                                target="_blank" data-sankatooltip="مشاهده جزئیات ابلاغیه" data-title="مشاهده جزئیات ابلاغیه"
                                                data-duplicatetitle="false" data-width="800" data-height="600" class='basic'>مشاهده</a>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                رکوردی موجود نیست .
                            </EmptyDataTemplate>
                        </cc1:SankaGrid>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div data-toggle="collapse" data-target="#collapse4" class="panel-heading pointer-cursor">
                <h4 class="panel-title">پیوست ها و مدارک</h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="grid">
                        <asp:GridView ID="gvUplodedFiles" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="AttachmentID">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" ReadOnly="True" />
                                <asp:BoundField DataField="AttachmentID" HeaderText="AttachmentID" ReadOnly="True"
                                    Visible="false" />

                                <asp:TemplateField HeaderText="نوع سند">
                                    <ItemTemplate>
                                        <center>
                        <%# SankaWebAppFramework.Board.Enums.strAttachmentFileType[Eval("AttachmentType").ToString().StringToByte()]%>
                    </center>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="توضیحات">
                                    <ItemTemplate>
                                        <center>
                        <span data-sankatooltip='<%# Eval("AttachmentTitle")%>' title='<%# Eval("AttachmentTitle")%>'><%# Eval("AttachmentTitle").ToString().ShowSummary(20)%></span>
                    </center>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>
                                <asp:BoundField DataField="DocumentCode" HeaderText="شماره سند" ReadOnly="True" />
                                <asp:TemplateField HeaderText="تاریخ سند">
                                    <ItemTemplate>
                                        <center>
                        <%# Utility.GetCustomDateWithoutTime(Eval("DocumentDate"))%>
                    </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="تاریخ ثبت">
                                    <ItemTemplate>
                                        <%# Utility.GetCustomDateWithoutTime(Eval("RegDate"))%>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="حجم فایل به بایت">
                                    <ItemTemplate>
                                        <center>
                        <%# SankaWebAppFramework.Board.Common.GetFileSize(Eval("AttachmentSource").ToString())%>
                    </center>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="مشاهده">
                                    <ItemTemplate>
                                        <center>
                        <asp:HyperLink ID="lnkURL" runat="server" NavigateUrl='<%# Eval("AttachmentSource") %>' 
                            Target="_blank">مشاهده</asp:HyperLink>
                    </center>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <center>
                                سندی پیوست نگردیده است</center>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField ID="hfRelatedPersonFullName" runat="server" ClientIDMode="Static" />
    

</asp:Content>
