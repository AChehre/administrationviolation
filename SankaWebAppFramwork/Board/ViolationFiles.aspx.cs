﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Configuration;

namespace SankaWebAppFramework.Board
{
    public partial class ViolationFiles : GeneralPage
    {
        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public string HintTagColor
        {
            get
            {
                if (ViewState["HintTagColor"] != null)
                {
                    return ViewState["HintTagColor"].ToString();
                }
                return "#FFFFFF";
            }
            set
            {
                ViewState.Add("HintTagColor", value);
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public bool OrderByReferType
        {
            get
            {
                if (ViewState["OrderByReferType"] != null)
                {
                   return ViewState["OrderByReferType"].ObjectToBool();
                }
                return false;
            }
            set
            {
                ViewState.Add("OrderByReferType", value);
            }
        }

        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long BoardID
        {
            get
            {
                return UserBoardID;
            }
        }

        /// <summary>
        /// آیا کاربر عضو  هیات هست یا نه
        /// </summary>
        private bool IsCurrentUserBoardMember
        {
            get
            {
                if (ViewState["IsBoardMember"] != null)
                {
                    return ViewState["IsBoardMember"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("IsBoardMember", value);
            }
        }

        /// <summary>
        /// استخراج متن مربوط به کد نتیجه نهایی بر اساس اندیس
        /// </summary>
        /// <param name="FinalResultCode"></param>
        /// <returns></returns>
        protected string getFinalResult(object FinalResultCode)
        {
            return Enums.strFinalStatus[FinalResultCode.ToString().StringToByte()];
        }

        /// <summary>
        ///  استخراج وضعیت پرونده
        /// </summary>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        protected string GetFileStatus(object statusCode)
        {
            return Common.getFileStatusLabel(statusCode);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.HintTagColor = WebConfigurationManager.AppSettings.Get("HintTagColor");
                }
                catch { }

                #region چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                IsCurrentUserBoardMember = CheckUserToAccessToBoard(BoardID, "~/Board/UserBoardsList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View);

                #endregion چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                Common.BindViolationFileFinalResult(ddlFinalResult);
                ddlFinalResult.Items.Insert(0, new ListItem("انتخاب کنید", "-1"));
                Common.BindViolationFileStatus(ddlStatus);
                ddlStatus.Items.Insert(0, new ListItem("انتخاب کنید", "-1"));
                if (this.UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    Common.BindBoards(ddlBoard);
                }
                else
                {
                    divBoardID.Hide();
                } 
                ddlBoard.Items.Insert(0, new ListItem("انتخاب کنید", "-1"));
                
                txtFromDate.Empty = true;
                txtToDate.Empty = true;

                LoadData();

                //** اگر صفحه پست بک نشده مقدار زیر را ترو میکنیم تا به صورت پیش فرض اطلاعات گرید ، نمایش داده شود
                GridViewPageChanged = true;
            }
            if (GridViewPageChanged)
            {
                GridViewPageChanged = false;
                BindGrid();
            }
            else
            { // زمانی که لود پیج به خاطر کنترل هایی غیر از کنترل های تغییر صفحه و اندازه صفحه گرید ، فراخوانی میشود، چون 
                // لیست دکمه های صفحه بندی در ویو استیت نمی ماند، دوباره آنها را میسازیم
                gvViolationFiles.CreatePageNumberLink(this.TotalRow, this.PageSize, this.PageNumber, (int)SankaWebAppFramework.Enums.PageLinkNumber.TenNumber);
            }
            if (UserBoardType != 2)
            {
                BindActionMenu();
            }
        }

        /// <summary>
        /// نمایش اطلاعات هیات ای که ایدیش در کوئری استرینگ هست
        /// </summary>
        protected void LoadData()
        {
            DTO.Admin.tblBoard.tblBoard obj = new DTO.Admin.tblBoard.tblBoard();
            try
            {
                obj.BoardID = BoardID;

                obj = (new DAL.Admin.tblBoard.Methods()).SelectByID(obj);
            }
            catch (Exception ex)
            {
                LogError(ex,"خطا در بدست آوردن اطلاعات هیات.");
            }

            if (obj.RowNumber > 0)
            {// مقداردهی عنوان صفحه در صورت وجود هیات درخواستی
                PageLabelTitle = obj.BoardTitle + "[لیست پرونده های تخلف]";
            }
            else
            {
                SankaDialog1.ShowMessage((new Translation()).MessageContent(Translation.MessageContentType.MSG_PAGE_ACCESS_DENIED), DialogMessage.SankaDialog.Message_Type.Warning, "~/Board/UserBoardsList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View, true);
            }
        }

        /// <summary>
        /// نمایش لیست پرونده های هیات
        /// </summary>
        private void BindGrid()
        {
            try
            {
                DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();

                //if (UserBoardType != 3)
                //{
                //    obj.BoardID = BoardID;
                //}
                //else
                //{
                //    if (UserBoardType == 3 && string.IsNullOrEmpty(Request.QueryString.Get("BoardID")))
                //    {
                //        obj.BoardID = null;
                //    }
                //    else
                //    {
                //        obj.BoardID = BoardID;
                //    }
                //}

                obj.SortOrder = SortOrder;
                obj.SortColumn = SortColumn;
                obj.PageNumber = this.PageNumber;
                obj.PageSize = this.PageSize;
                obj.OrderByReferType = this.OrderByReferType;
                // مقادیر جستجو

                if (txtFName.Text.Trim().Length > 0)
                {
                    obj.FirstName = txtFName.Text.Trim().CorrectArabicChars();
                }

                if (txtLName.Text.Trim().Length > 0)
                {
                    obj.LastName = txtLName.Text.Trim().CorrectArabicChars();
                }

                if (txtPersonnelCode.Text.Trim().Length > 0)
                {
                    obj.PersonnelCode = txtPersonnelCode.Text.StringToLong();
                }

                if (txtSerialCode.Text.Trim().Length > 0)
                {
                    obj.SerialCode = txtSerialCode.Text.Trim().CorrectArabicChars();
                }

                if (ddlStatus.SelectedIndex > 0)
                {
                    obj.Status = ddlStatus.SelectedValue.StringToInt();
                }

                if (ddlFinalResult.SelectedIndex > 0)
                {
                    obj.VoteTypeCode = ddlFinalResult.SelectedValue.StringToByte();
                }

                if (this.UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    if (ddlBoard.SelectedIndex > 0)
                    {
                        obj.BoardID = ddlBoard.SelectedValue.StringToLong();
                    }
                    else
                    {
                        if (Request.QueryString.Get("BoardID") != null)
                        {
                            obj.BoardID = Request.QueryString.Get("BoardID").StringToLong();
                        }
                    }
                }
                else
                {
                    obj.BoardID = this.UserBoardID;
                }

                #region از تاریخ
                // مقدار پیش فرض
                obj.FromDate = null;
                if (txtFromDate.DateIsValid)
                {// اگر انتخاب شده بود مقدار دهی شود
                    obj.FromDate = txtFromDate.DateMiladi;
                }

                #endregion از تاریخ

                #region تا تاریخ
                // مقدار پیش فرض
                obj.ToDate = null;
                if (txtToDate.DateIsValid)
                {// اگر انتخاب شده بود مقدار دهی شود
                    obj.ToDate = txtToDate.DateShamsi.ToLatinDateTimeFull("23:59:59");// همیشه تاریخ پایانی ، تا آخرین ساعت روز باشد
                }

                #endregion تا تاریخ

                List<DTO.Board.tblViolationFile.tblViolationFile> lstViolationFiles = (new DAL.Board.tblViolationFile.Methods()).SelectAllByPagingByBoardID(obj);
                gvViolationFiles.CustomDataBind(lstViolationFiles, this.PageSize, this.PageNumber, (int)SankaWebAppFramework.Enums.PageLinkNumber.TenNumber);
                // اگر در لیست رکوردی وجود داشته باشد، تعداد کل سطرها را در یک هیدن فیلد ذخیره میکنیم
                this.TotalRow = lstViolationFiles.Any() ? lstViolationFiles[0].TotalRow.Value : 0;

                gvViolationFiles.Columns[9].Visible = (this.UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora);

                SetSortStatus();
            }
            catch (Exception ex)
            {
                LogError(ex, "خطا در نمایش لیست پرونده های هیات");
            }
        }

        /// <summary>
        /// باید کردن منو
        /// </summary>
        private void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            if (!IsCurrentUserBoardMember)
            {//  اگر کاربر به نوعی ناظر بود که عضو هیات نبود، فقط دکمه بازگشت ساخته شود

                //  دکمه بازگشت
                lstAction.Add(new ActionMenuModel()
                {
                    Icon = "/images/icon/back.png",
                    TargetID = "btnBack",
                    Title = "بازگشت",
                    Type = ActionMenuType.HyperLink,
                    OnClientClick = "GoBackUrl(this);",
                    NavigateUrl = "/admin/AdminPanel.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View
                });
            }
            else
            {
                Dictionary<string, string> Attrs = new Dictionary<string, string>();

                Attrs.Add("data-save-return-url", "true");

                lstAction.Add(new ActionMenuModel()
               {
                   TargetID = "btnNewViolationFile",
                   Title = "پرونده جدید",
                   Type = ActionMenuType.HyperLink,
                   NavigateUrl = "~/Board/ViolationFileDependencies.aspx?BoardID=" + BoardID,
                   Icon = "/images/icon/document_add.png",
                   Attributes = Attrs
               });

                //  دکمه بازگشت
                lstAction.Add(new ActionMenuModel()
                {
                    Icon = "/images/icon/back.png",
                    TargetID = "btnBack",
                    Title = "بازگشت",
                    Type = ActionMenuType.HyperLink,
                    OnClientClick = "GoBackUrl(this);",
                    NavigateUrl = "/Board/UserBoardsList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View
                });
            }

            UCActionMenu1.DataSource = lstAction;
            UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            this.OrderByReferType = true;
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;

            BindGrid();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvViolationFiles.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvViolationFiles.HeaderRow.FindControl(ControlID) as LinkButton;
                    System.Web.UI.WebControls.Image img = gvViolationFiles.HeaderRow.FindControl("img" + SortColumn) as System.Web.UI.WebControls.Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            BindGrid();
        }

        /// <summary>
        /// نمایش تمامی پرونده ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            this.PageNumber = 1;
            // ست کردن مقدار کنترل ها به مقادیر پیش فرض
            ddlStatus.SelectedIndex = 0;
            ddlFinalResult.SelectedIndex = 0;
            txtFName.Clear();
            txtLName.Clear();
            txtPersonnelCode.Clear();
            txtFromDate.DateShamsi = "";
            txtToDate.DateShamsi = "";
            txtSerialCode.Clear();
            ddlBoard.SelectedIndex = 0;
            ddlFinalResult.SelectedIndex = 0;
            ddlStatus.SelectedIndex = 0;
            BindGrid();
        }

        protected void gvViolationFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HiddenField hfHintTag = (e.Row.FindControl("hfHintTag") as HiddenField);
            if (hfHintTag != null)
            {
                int hintTagValue = hfHintTag.Value.StringToByte();
                if (hintTagValue > 0)
                {
                    // e.Row.BackColor = this.HintTagColor;
                    e.Row.Style.Add(HtmlTextWriterStyle.BackgroundColor, this.HintTagColor);
                    e.Row.Attributes.Add("data-sankatooltip", getHintTagTitle(hintTagValue));
                }
            }
        }

        /// <summary>
        /// استخراج متن منتاظر با تگ برجسته سازی پرونده ها
        /// </summary>
        /// <param name="hintTagValue"></param>
        /// <returns></returns>
        private string getHintTagTitle(int hintTagValue)
        {
            try
            {
                return Enums.strFileHintTagTooltip[hintTagValue];
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}