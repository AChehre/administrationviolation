﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No7 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        private void LoadTableNo1()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long count = 0;
            long sum = 0;

            count = getCount(0,4, dt);
            sum += count;
            tblTable1.Rows[2].Cells[1].InnerHtml = "<center>" + count + "</center>";

            count = getCount(5,9, dt);
            sum += count;
            tblTable1.Rows[3].Cells[1].InnerHtml = "<center>" + count + "</center>";

            count = getCount(10,14, dt);
            sum += count;
            tblTable1.Rows[4].Cells[1].InnerHtml = "<center>" + count + "</center>";

            count = getCount(15,19, dt);
            sum += count;
            tblTable1.Rows[5].Cells[1].InnerHtml = "<center>" + count + "</center>";

            count = getCount(20,24, dt);
            sum += count;
            tblTable1.Rows[6].Cells[1].InnerHtml = "<center>" + count + "</center>";

            count = getCount(25, 255,dt);
            sum += count;
            tblTable1.Rows[7].Cells[1].InnerHtml = "<center>" + count + "</center>";

            tblTable1.Rows[8].Cells[1].InnerHtml = "<center>" + sum + "</center>";
        }

        /// <summary>
        /// فراوانی بر اساس استان و منطقه هیات
        /// </summary>
        /// <param name="fromYears">از سال</param>
        /// <param name="toYears">تا سال</param>
        /// <param name="dt">تاریخ</param>
        /// <returns></returns>
        private long getCount(byte fromYears , byte toYears , DateTime dt)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                FromYear = fromYears,
                ToYear = toYears,
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getVoteCountByActivityYear(obj);
        }

        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            tdBoardTitle.InnerHtml = string.Format(@"<strong>جدول (7) فراوانی آراء صادره هیات های بدوی بر حسب سابقه خدمت</strong>", ddlBoard.SelectedItem.Text);
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}