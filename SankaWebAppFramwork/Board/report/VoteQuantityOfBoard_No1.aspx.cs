﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No1 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        /// <summary>
        /// بارگذاری لیست سال
        /// </summary>
        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        /// <summary>
        /// بارگذاری اطلاعات در قالب جدول
        /// </summary>
        private void LoadTableNo1()
        {
            long sum =0;
            HtmlTableCell cell = null;
            var lst = (new DAL.Board.tblAccusationSubject.Methods()).SelectAll();
            int i = 1;
            //DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(1394,7,1, 0, 0, 0, 0);
            foreach (var obj in lst)
            {
                HtmlTableRow tr = new HtmlTableRow();
                cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>بند " + i + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>" + obj.Title + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                long count = getCount(obj.SubjectID.Value,dt);
                sum += count;
                cell.InnerHtml = "<strong>" + count + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                tblTable1.Rows.Add(tr);
                i++;
            }

            HtmlTableRow trTotal = new HtmlTableRow();
            cell = new HtmlTableCell();
            cell.InnerHtml = "<strong>جمع</strong>";
            cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
            cell.ColSpan = 2;
            trTotal.Cells.Add(cell);
           

            cell = new HtmlTableCell();
            cell.InnerHtml = "<strong>" + sum + "</strong>";
            cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
            trTotal.Cells.Add(cell);

            tblTable1.Rows.Add(trTotal);
        }

        /// <summary>
        /// تعداد فراوانی بر اساس موضوع در بازه زمانی سه ماه
        /// </summary>
        /// <param name="subjectID"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        private long getCount(long subjectID,DateTime dt)
        {
            var obj = new DTO.Board.tblBoardAccusation.tblBoardAccusation
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                AccusationSubjectID = subjectID,
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblBoardAccusation.Methods()).SelectCountByAccusationSubjectID(obj);
        }

        /// <summary>
        /// برگرداندان عدد اولین ماه براساس عدد فصل
        /// </summary>
        /// <returns></returns>
        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            string strBoardTitle = @" <strong>جدول (1) فراوانی آراء صادره هیات&nbsp; {0}&nbsp; بر حسب نوع تخلف </strong>";
            tdBoardTitle.InnerHtml = string.Format(strBoardTitle, ddlBoard.SelectedItem.Text);
            LoadTableNo1();
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}