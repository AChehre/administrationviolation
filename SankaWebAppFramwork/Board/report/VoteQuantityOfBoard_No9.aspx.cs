﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No9 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
            }
        }

        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        private void LoadTableNo1()
        {
            HtmlTableCell cell = null;
            var lst = (new DAL.Board.tblAccusationSubject.Methods()).SelectAll();
            int i = 1;
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            foreach (var obj in lst)
            {
                HtmlTableRow tr = new HtmlTableRow();
                cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>بند " + i + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>" + obj.Title + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>" + getCount(obj.SubjectID.Value,dt) + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                tblTable1.Rows.Add(tr);
                i++;
            }
        }

        /// <summary>
        ///بر اساس موضوع اتهام تعداد فراوانی
        /// </summary>
        /// <param name="subjectID">موضوع اتهام</param>
        /// <param name="dt"></param>
        /// <returns></returns>
        private long getCount(long subjectID,DateTime dt)
        {
            var obj = new DTO.Board.tblBoardAccusation.tblBoardAccusation
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1)
            };
            return (new DAL.Board.tblBoardAccusation.Methods()).SelectCountByAccusationSubjectID(obj);
        }

        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}