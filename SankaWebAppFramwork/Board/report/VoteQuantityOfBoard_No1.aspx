﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/report/Report.Master" AutoEventWireup="true"
    CodeBehind="VoteQuantityOfBoard_No1.aspx.cs" Inherits="SankaWebAppFramework.Board.report.VoteQuantityOfBoard_No1" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
        }
        td
        {
            vertical-align: top;
        }
    </style>
    <style type="text/css" media="print">
        .print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button CssClass="print" ID="btnReturn" runat="server" OnClick="btnReturn_Click"
        Text="بازگشت" />
    <asp:MultiView runat="server" ID="MultiView1" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <center>
                <div id="divPanel" style="font-family: B Nazanin; font-size: 14pt">
                    <table>
                        <tr>
                            <td>
                                سه ماهه :
                                <asp:DropDownList ID="ddlSession" runat="server">
                                    <asp:ListItem Value="1">اول</asp:ListItem>
                                    <asp:ListItem Value="2">دوم</asp:ListItem>
                                    <asp:ListItem Value="3">سوم</asp:ListItem>
                                    <asp:ListItem Value="4">چهارم</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                سال : &nbsp;
                                <asp:DropDownList ID="ddlYear" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                هیئت
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBoard" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnGenerateReport" runat="server" OnClick="btnGenerateReport_Click"
                                    Text="تولید گزارش" />
                            </td>
                        </tr>
                    </table>
                </div>
            </center>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="print">
                <asp:Button ID="btnPrint" UseSubmitBehavior="false" OnClientClick="window.print();return false"
                    runat="server" Text="چاپ" Style="cursor: pointer" />
                &nbsp;<asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="تولید گزارش مجدد" />
            </div>
            <center>
                <asp:Label runat="server" ID="lblTitle" Style="direction: rtl; font-weight: bold"></asp:Label></center>
            <table style="width: 100%" border="1" id="tblTable1" runat="server">
                <tr>
                    <td id="tdBoardTitle" runat="server" colspan="3" style="text-align: center">
                       
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="style2" colspan="2">
                        <b>عنوان تخلف</b>
                    </td>
                    <td align="center">
                        <b>فراوانی</b>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <cc1:SankaDialog ID="SankaDialog1" runat="server">
    </cc1:SankaDialog>
</asp:Content>
