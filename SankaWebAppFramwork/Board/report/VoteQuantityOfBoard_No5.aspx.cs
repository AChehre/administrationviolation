﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No5 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoardType(ddlBoardType);
            }
        }

        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        private void LoadTableNo1()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long count = 0;
            long sum = 0;

            var objOstans = (new DAL.Config.tblOstans.Methods()).SelectAll();
            int i = 0;

            foreach (var objOstan in objOstans)
            {
                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>" + objOstan.Name + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                count = getCount(objOstan.OstanCode.Value, dt);
                cell.InnerHtml = "<strong>" + count + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                sum += count;

                i++;

                tblTable1.Rows.Add(tr);
            }

            HtmlTableRow trTotal = new HtmlTableRow();
            HtmlTableCell cellTotal = new HtmlTableCell();
            cellTotal.InnerHtml = "<strong><center>جمع</center></strong>";
            cellTotal.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
            trTotal.Cells.Add(cellTotal);

            cellTotal = new HtmlTableCell();
          
            cellTotal.InnerHtml = "<strong>" + sum + "</strong>";
            cellTotal.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
            trTotal.Cells.Add(cellTotal);
            tblTable1.Rows.Add(trTotal);
        }

        /// <summary>
        /// فراوانی در هر استان
        /// </summary>
        /// <param name="ostanCode">کد استان</param>
        /// <param name="dt">تاریخ</param>
        /// <returns></returns>
        private long getCount(int ostanCode, DateTime dt)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                OstanCode = ostanCode,
                BoardType = ddlBoardType.SelectedValue.StringToByte(),
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getVoteCountByOstanCode(obj);
        }

        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            string.Format(@"<strong>جدول (5) فراوانی آراء صادره هیات های {0} بر حسب استان محل خدمت</strong>",ddlBoardType.SelectedItem.Text);

            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}