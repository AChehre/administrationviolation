﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/report/Report.Master" AutoEventWireup="true"
    CodeBehind="VoteReport.aspx.cs" Inherits="SankaWebAppFramework.Board.report.VoteReport" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css" media="print">
        #divPanel
        {
            display: none;
        }
    </style>
    <style type="text/css">
        @font-face
        {
            font-family: 'IranNastaliq';
            src: url('fonts/IranNastaliq.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face
        {
            font-family: 'BNazanin';
            src: url('fonts/BNazanin.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        body
        {
            font-family: 'BNazanin';
            text-align: center;
        }
        .header
        {
            font-weight: bold;
        }
        .header td
        {
            text-align: center;
        }
        .style1
        {
            height: 39px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <div id="divPanel" style="font-family: B Nazanin; font-size: 14pt">
                <table>
                    <tr>
                        <td>
                            لیست آرا صادره در
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMonth" runat="server" Width="100px">
                                <asp:ListItem Value="-1">انتخاب ماه</asp:ListItem>
                                <asp:ListItem Value="1">فروردین</asp:ListItem>
                                <asp:ListItem Value="1">اردیبهشت</asp:ListItem>
                                <asp:ListItem Value="3">خرداد</asp:ListItem>
                                <asp:ListItem Value="4">تیر</asp:ListItem>
                                <asp:ListItem Value="5">مرداد</asp:ListItem>
                                <asp:ListItem Value="6">شهریور</asp:ListItem>
                                <asp:ListItem Value="7">مهر</asp:ListItem>
                                <asp:ListItem Value="8">آبان</asp:ListItem>
                                <asp:ListItem Value="9">آذر</asp:ListItem>
                                <asp:ListItem Value="10">دی</asp:ListItem>
                                <asp:ListItem Value="11">بهمن</asp:ListItem>
                                <asp:ListItem Value="12">اسفند</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            ماه
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            هیات
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBoard" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            وزارت آموزش و پرورش
                        </td>
                        <td>
                            <asp:Button ID="btnGenerateReport" runat="server" OnClick="btnGenerateReport_Click"
                                Text="تولید گزارش" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="print">
                <asp:Button ID="btnPrint" UseSubmitBehavior="false" OnClientClick="window.print();return false"
                    runat="server" Text="چاپ" Style="cursor: pointer" />
                <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="بازگشت" />
                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="تولید گزارش مجدد" />
            </div>
            <div style="font-weight: bold; text-align: center">
                لیست آراء صادره در<asp:Label ID="lblMonth" runat="server"></asp:Label>
                &nbsp;ماه &nbsp;<asp:Label ID="lblYear" runat="server"></asp:Label>
                &nbsp;هیات<asp:Label ID="lblBoardName" runat="server"></asp:Label>
                &nbsp;رسیدگی به تخلفات اداری شعبه وزارت آموزش و پرورش</div>
            <table border="1" style="width: 100%" id="tblVoteReport" runat="server">
                <tr class="header">
                    <td class="style1">
                        ردیف
                    </td>
                    <td class="style1">
                        شماره رای
                    </td>
                    <td class="style1">
                        نام و نام خانوادگی
                    </td>
                    <td class="style1">
                        سابقه محکومیت
                    </td>
                    <td class="style1">
                        محل خدمت
                    </td>
                    <td class="style1">
                        سمت مستخدم
                    </td>
                    <td class="style1">
                        مدرک تحصیلی
                    </td>
                    <td class="style1">
                        سابقه خدمت
                    </td>
                    <td class="style1">
                        وضعیت تاهل
                    </td>
                    <td class="style1">
                        بند تخلف
                    </td>
                    <td class="style1">
                        موضوع تخلف
                    </td>
                    <td class="style1">
                        مجازات
                    </td>
                    <td class="style1">
                        تایید یا نقض
                    </td>
                </tr>
            </table>
            <div style="font-weight: bold; text-align: center">
                رئیس هیات&nbsp;
                <asp:Label ID="lblManagerName" runat="server"></asp:Label>
        </asp:View>
    </asp:MultiView>
    <div id="notification_dialog" style="display: none">
    </div>
    <cc1:SankaDialog ID="SankaDialog2" runat="server">
    </cc1:SankaDialog>
    <div id='modal_content'>
        <h3>
        </h3>
        <iframe frameborder="0" id="sanka_iframe" src=""></iframe>
        <br />
</asp:Content>
