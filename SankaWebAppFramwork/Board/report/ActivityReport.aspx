﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/report/Report.Master" AutoEventWireup="true"
    CodeBehind="ActivityReport.aspx.cs" Inherits="SankaWebAppFramework.Board.report.ActivityReport" %>

<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="Date" Src="~/controls/UCDatePicker.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css" media="print">
        #divPanel
        {
            display: none;
        }
    </style>
    <style type="text/css">
	@font-face {
		font-family: 'IranNastaliq';
		src: url('fonts/IranNastaliq.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'BNazanin';
		src: url('fonts/BNazanin.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	.page
	{
		width:980px;
		margin:0px auto;
		font-family:'BNazanin';
		font-size:13pt;
		text-align:right;
		direction:rtl;
	}
	.top-position
	{
		margin-top:-40px;
		width:100%;
		text-align:center;
            font-weight: 700;
        }
	.logo-center
	{
		width:80px;
		height:45px;
	}
	
	.logo-right
	{
		width:130px;
		height:80px;
	}
	
	.Nastaligh
	{
		font-family: B Nazanin; 
	}
	.title1
	{
		font-size:20pt;
		line-height:1pt;
		text-align:center; !important;
	}
	
	.date-and-number
	{
		margin-top:-30px;
		text-align:right;
		width:200px;
		font-size:16pt;
		direction:rtl;
		line-height:10pt;
	}
	.paragraph
    {
        font-weight: 700;
    }
    #tblActivity td
    {
        font-family:B Nazanin;
            text-align: center;
            font-weight: 700;
        }
	#tblActivity
    {
        width: 883px;
    }
	    .style1
        {
            text-align: center;
        }
        .style6
        {
            text-align: center;
            width: 109px;
        }
        .style7
        {
            width: 109px;
        }
        .style8
        {
            width: 106px;
        }
        .style9
        {
            width: 53px;
        }
        .style11
        {
            font-weight: bold;
        }
        .style12
        {
            width: 106px;
            font-weight: bold;
        }
        .style13
        {
            font-weight: bold;
            width: 99px;
        }
        .style14
        {
            width: 99px;
        }
	    .style15
        {
            font-size: 12pt;
        }
	    .style17
        {
            font-size: 12pt;
            width: 181px;
        }
        .style18
        {
            width: 181px;
        }
        .style19
        {
            width: 170px;
            height: 121px;
        }
	</style>
    <!-------------------------------------------------------------------------------------->
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css"
        media="all" />
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css"
        media="all" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------->
    <style type="text/css" media="print">
        .print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <center>
                <div id="divPanel" style="font-family: B Nazanin; font-size: 14pt">
                    <table>
                        <tr>
                            <td>
                                از تاریخ : &nbsp;
                                <uc1:Date ID="txtFromDate" runat="server" />
                            </td>
                            <td>
                                تا تاریخ : &nbsp;
                                <uc1:Date ID="txtToDate" runat="server" />
                            </td>
                            <td>
                                هیئت
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBoard" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnGenerateReport" runat="server" OnClick="btnGenerateReport_Click"
                                    Text="تولید گزارش" />
                            </td>
                        </tr>
                    </table>
                </div>
            </center>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="print">
                <asp:Button ID="btnPrint" UseSubmitBehavior="false" OnClientClick="window.print();return false"
                    runat="server" Text="چاپ" Style="cursor: pointer" />
                <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="بازگشت" />
                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="تولید گزارش مجدد" />
            </div>
            <div align="center" class="page">
                <div class="top-position">
                    <br />
                    <br />
                    <br />
                    <p class="Nastaligh title1">
                        بسمه تعالی</p>
                </div>
                <div style="padding-right: 5px; padding-left: 5px">
                    <p class="paragraph">
                        به : دفتر هماهنگی هیات های رسیدگی به تخلفات اداری
                    </p>
                    <p class="paragraph">
                        موضوع : گزارش فعالیت
                    </p>
                    <p class="paragraph">
                        سلام علیکم
                    </p>
                    <p class="paragraph">
                        <span style="font-family: B Nazanin; color: #000000; font-size: 12pt; font-weight: 400;
                            font-style: normal; text-decoration: none;">احتراما ، در اجرای صدور ماده 24 آیین
                            نامه اجرایی قانون رسیدگی به تخلفات اداری مصوب 73/07/27 هیات وزیران گزارش فعالیت
                            طبق جدول زیر اعلام میگردد .</span></p>
                    <div style="text-align: center">
                        <table border="1" id="tblActivity">
                            <tr>
                                <td rowspan="2" class="style18">
                                    <img alt="" class="style19" src="images/logo.gif" />
                                </td>
                                <td rowspan="2" class="style13">
                                    تعداد پرونده های موجود (اول ماه)
                                </td>
                                <td rowspan="2" class="style13">
                                    تعداد پرونده های واصله&nbsp;
                                </td>
                                <td colspan="4" class="style1">
                                    تعداد پرونده های رسیدگی شده
                                </td>
                                <td class="style6" rowspan="2">
                                    تعداد پرونده های موجود (پایان ماه)
                                </td>
                            </tr>
                            <tr>
                                <td class="style12">
                                    محکومیت قطعی&nbsp;
                                </td>
                                <td class="style9">
                                    <b style="text-align: center">محکومیت قابل پژوهش&nbsp; </b>
                                </td>
                                <td class="style11">
                                    برائت
                                </td>
                                <td class="style11">
                                    مختومه
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" class="style18">
                                    &nbsp; پرونده های جاری
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblFirstOfMonth_Jari" runat="server"></asp:Label>
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblVaseleh_Jari" runat="server"></asp:Label>
                                </td>
                                <td class="style8">
                                    <asp:Label ID="lblGhati_Jari" runat="server"></asp:Label>
                                </td>
                                <td class="style9">
                                    <asp:Label ID="lblResearchable_Jari" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblInocence_Jari" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClose_Jari" runat="server"></asp:Label>
                                </td>
                                <td class="style7">
                                    <asp:Label ID="lblLastOfMonth_Jari" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" class="style17">
                                    &nbsp; پرونده موضوع ماده 22 یا 24&nbsp; ق
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblFirstOfMonth_Madde_22_24" runat="server"></asp:Label>
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblVaseleh_Madde22_24" runat="server"></asp:Label>
                                </td>
                                <td class="style8">
                                    <asp:Label ID="lblGhati_Madde22_24" runat="server"></asp:Label>
                                </td>
                                <td class="style9">
                                    <asp:Label ID="lblResearchable_Madde22_24" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblInocence_Madde22_24" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClose_Madde22_24" runat="server"></asp:Label>
                                </td>
                                <td class="style7">
                                    <asp:Label ID="lblLastOfMonth_Madde22_24" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" class="style18">
                                    <span style="color: rgb(0, 0, 0); font-family: 'B Nazanin'; font-style: normal; font-variant: normal;
                                        font-weight: bold; letter-spacing: normal; line-height: normal; orphans: auto;
                                        text-align: center; text-indent: 0px; text-transform: none; white-space: normal;
                                        widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important;
                                        float: none;" class="style15">&nbsp; پرونده موضوع ماده 11&nbsp; ق</span>
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblFirstOfMonth_Madde_11" runat="server"></asp:Label>
                                </td>
                                <td class="style14">
                                    <asp:Label ID="lblVaseleh_Madde11" runat="server"></asp:Label>
                                </td>
                                <td class="style8">
                                    <asp:Label ID="lblGhati_Madde11" runat="server"></asp:Label>
                                </td>
                                <td class="style9">
                                    <asp:Label ID="lblResearchable_Madde11" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblInocence_Madde11" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClose_Madde11" runat="server"></asp:Label>
                                </td>
                                <td class="style7">
                                    <asp:Label ID="lblLastOfMonth_Madde11" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
    <div id="notification_dialog" style="display: none">
    </div>
    <cc2:SankaDialog ID="SankaDialog1" runat="server">
    </cc2:SankaDialog>
    <div id='modal_content'>
        <h3>
        </h3>
        <iframe frameborder="0" id="sanka_iframe" src=""></iframe>
    </div>
</asp:Content>
