﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No6 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        private void LoadTableNo1()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long count = 0;
            long sum = 0;

            var lstPosition = (new DAL.Board.tblOrganizationPosition.Methods()).SelectAll();
            int i = 0;

            foreach (var position in lstPosition)
            {
                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                cell.InnerHtml = "<strong>" + position.OrganizationPositionName + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                count = getCount(position.OrganizationPositionID, dt);
                cell.InnerHtml = "<strong>" + count + "</strong>";
                cell.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                tr.Cells.Add(cell);

                tblTable1.Rows.Add(tr);

                sum += count;

                i++;
            }

            HtmlTableRow trTotal = new HtmlTableRow();
            HtmlTableCell cellTotal = new HtmlTableCell();
            cellTotal.InnerHtml = "<strong><center>جمع</center></strong>";
            cellTotal.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
            trTotal.Cells.Add(cellTotal);

            cellTotal = new HtmlTableCell();

            cellTotal.InnerHtml = "<strong>" + sum + "</strong>";
            cellTotal.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
            trTotal.Cells.Add(cellTotal);

            tblTable1.Rows.Add(trTotal);
        }

        /// <summary>
        /// فراوانی بر اساس پست سازمانی
        /// </summary>
        /// <param name="position">کد سمت</param>
        /// <param name="dt">تاریخ</param>
        /// <returns></returns>
        private long getCount(int position, DateTime dt)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date, // اول ماه
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1), // آخر ماه ساعت یازده و پنجاه و نه دقیقه و پنجاه و نه ثانیه
                OrganizationPosition = position,
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getVoteCountByOrganizationPosition(obj);
        }

        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            tdBoardTitle.InnerHtml = string.Format(@"<strong>جدول (6) فراوانی آراء صادره هیات {0} بر حسب پست سازمانی</strong>", ddlBoard.SelectedItem.Text);
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}