﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteReport : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Header.Title = "گزارش فعالیت";
            if (!IsPostBack)
            {
                for (int i = 1394; i < 1500; i++)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlYear.Items.Insert(0,new ListItem("انتخاب سال","-1"));
                ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();

                for (int i = 1; i < 13; i++)
                {
                    ddlMonth.Items.Add(new ListItem((new System.Globalization.PersianCalendar()).GetMonthsInYear(i).ToString(),i.ToString()));
                }
                ddlMonth.SelectedValue = (new System.Globalization.PersianCalendar()).GetMonth(DateTime.Now).ToString();

                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        /// <summary>
        /// بار گذاری گزارش
        /// </summary>
        /// <returns></returns>
        private bool LoadReport()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), ddlMonth.SelectedValue.StringToInt(), 1, 0, 0, 0, 0);
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                FromDate = dt,
                ToDate = dt.AddMonths(1).AddSeconds(-1)
            };
            var lsrVotedViolationFiles = (new DAL.Board.tblViolationFile.Methods()).SelectAll_By_BoardID_For_VoteReport(obj);
            int i = 1;
            HtmlTableCell cell = null;
            foreach (var file in lsrVotedViolationFiles)
            {
                HtmlTableRow tr = new HtmlTableRow();
                cell = new HtmlTableCell();
                cell.InnerHtml = i++.ToString();
                tr.Cells.Add(cell);

                var objVote = new DTO.Board.tblViolationFileVote.tblViolationFileVote
                {
                    ViolationFileID = file.FileID
                };
                objVote = (new DAL.Board.tblViolationFileVote.Methods()).SelectByViolationFileID(objVote);

                cell = new HtmlTableCell();
                cell.InnerText = objVote.VoteNumber.ToString();
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = file.FirstName + " " + file.LastName;
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = getCondemnationHistory(file.PersonnelCode);
                tr.Cells.Add(cell);

                var objPersonnel = new DTO.Board.tblPersonnelView.tblPersonnelView
                {
                    PersonnelCode = file.PersonnelCode.Value
                };
                objPersonnel = (new DAL.Board.tblPersonnelView.Methods()).SelectByPersonnelCode(objPersonnel);

                cell = new HtmlTableCell();
                cell.InnerText = objPersonnel.OstanName + "-" + objPersonnel.RegionName;
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = Common.getOrganizationPosition(objPersonnel.OrganizationPosition);
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = Common.getDegreeLabel(objPersonnel.Education);
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = objPersonnel.ActivityYears.ToString();
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = objPersonnel.Married ? "متاهل" : "مجرد";
                tr.Cells.Add(cell);


                string strAccusitionTitle = "";
                string strAccusitionDescription = "";
                getAccusitionList(file.FileID, ref strAccusitionTitle, ref strAccusitionDescription);

                cell = new HtmlTableCell();
                cell.InnerText = strAccusitionTitle;
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = strAccusitionDescription;
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = objVote.PenaltyDetail;
                tr.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = getConfirmOrNaghzeRay(file.FileID);
                tr.Cells.Add(cell);

                tblVoteReport.Rows.Add(tr);
            }
            return true;
        }

        /// <summary>
        /// گرفتن متن تایید یا نقض رای
        /// </summary>
        /// <param name="fileID">شناسه پرونده</param>
        /// <returns></returns>
        private string getConfirmOrNaghzeRay(long fileID)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile { FileID = fileID };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            return obj.NaghzeRay > 0 ? "نقض" : "تایید";
        }

        /// <summary>
        /// گرفتن لیست اتهامات
        /// </summary>
        /// <param name="fileID">شناسه پرونده</param>
        /// <param name="strAccusitionTitle">عنوان اتهام</param>
        /// <param name="strAccusitionDescription">توضیحات</param>
        private void getAccusitionList(long fileID , ref string strAccusitionTitle , ref string strAccusitionDescription)
        {
            var obj = new DTO.Board.tblAccusation.tblAccusation
            {
                ViolationFileID = fileID,
                Status = 1 // محرز شده
            };
            var lst = (new DAL.Board.tblAccusation.Methods()).SelectByFileID(obj);
            for (int i = 0; i < lst.Count; i++)
            {
                var objAccusationSubject = (new DAL.Board.tblAccusationSubject.Methods()).SelectByID(new DTO.Board.tblAccusationSubject.tblAccusationSubject{ SubjectID = lst[i].AccusationSubjectID});
                strAccusitionTitle += objAccusationSubject.Title.Substring(0, objAccusationSubject.Title.IndexOf("8") + 1) + ",";
                strAccusitionDescription += objAccusationSubject.Description + ",";
            }
            if (strAccusitionTitle.Length > 1)
            {
                strAccusitionTitle = strAccusitionTitle.Remove(strAccusitionTitle.Length - 1);
            }
            if (strAccusitionDescription.Length > 1)
            {
                strAccusitionDescription = strAccusitionDescription.Remove(strAccusitionDescription.Length - 1);
            }
        }

        /// <summary>
        ///متهم گرفتن متن سابقه محکومیت پیشین
        /// </summary>
        /// <param name="personnelCode">کد پرسنلی</param>
        /// <returns></returns>
        private string getCondemnationHistory(long? personnelCode)
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                PersonnelCode = personnelCode.Value
            };
            var lst = (new DAL.Board.tblViolationFile.Methods()).SelectAllByPersonnelCode(obj);
            return lst.Count.ToString();
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            if (!ValidateData()) return;
            if (LoadReport())
            {
                LoadBoardManagerName(ddlBoard.SelectedValue.StringToLong());
                lblYear.Text = ddlYear.SelectedValue;
                lblMonth.Text = ddlMonth.SelectedItem.Text;
                lblBoardName.Text = ddlBoard.SelectedItem.Text;
                MultiView1.ActiveViewIndex = 1;
            }
        }

        /// <summary>
        /// گرفتن نام رئیس / مدیر هیات
        /// </summary>
        /// <param name="boardID">شناسه هیات</param>
        private void LoadBoardManagerName(long boardID)
        {
            var obj = new DTO.Admin.tblBoardMemberMapView.tblBoardMemberMapView
            {
                BoardID = boardID,
                MemberRole = (byte)AdministrationViolation.Admin.Enums.Board_Member_Role.BoardAdmin
            };
            obj = (new DAL.Admin.tblBoardMemberMapView.Methods()).SelectByMemberRole(obj);
            lblManagerName.Text = obj.FName + " " + obj.LName;
        }

        /// <summary>
        /// اعتبار سنجی داده های ورودی
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            if (ddlYear.SelectedIndex == 0)
            {
                SankaDialog2.ShowMessage("سال مورد نظر را انتخاب نمایید .",DialogMessage.SankaDialog.Message_Type.Warning,Page,true);
                return false;
            }
            if (ddlMonth.SelectedIndex == 0)
            {
                SankaDialog2.ShowMessage("ماه مورد نظر را انتخاب نمایید .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                return false;
            }
            return true;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }
    }
}