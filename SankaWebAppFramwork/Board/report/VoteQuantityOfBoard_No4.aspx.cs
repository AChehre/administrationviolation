﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No4 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        private void LoadTableNo1()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long countMale = 0;
            long countFemale = 0;
            long sum = 0;

            countMale = getCount(1, dt);
            sum += countMale;
            tblTable1.Rows[2].Cells[1].InnerHtml = "<center>" + countMale + "</center>";

            countFemale = getCount(2, dt);
            sum += countFemale;
            tblTable1.Rows[3].Cells[1].InnerHtml = "<center>" + countFemale + "</center>";

            tblTable1.Rows[4].Cells[1].InnerHtml = "<center>" + sum + "</center>";
        }

        /// <summary>
        /// فراوانی بر اساس جنسیت
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        private long getCount(byte gender, DateTime dt)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                Gender = gender,
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getVoteCountByGender(obj);
        }

        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            tdBoardTitle.InnerHtml = string.Format(@"جدول (4) فراوانی آراء صادره هیات {0} بر حسب جنسیت", ddlBoard.SelectedItem.Text);
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}