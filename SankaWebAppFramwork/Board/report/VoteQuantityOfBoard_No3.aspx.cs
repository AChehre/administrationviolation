﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No3 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        /// <summary>
        /// بارگذاری لیست سال
        /// </summary>
        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        /// <summary>
        /// بارگذاری گزارش در قالب جدول
        /// </summary>
        private void LoadTableNo1()
        {
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long count = 0;
            long sum = 0;

            var lst = (new DAL.Board.tblDegree.Methods()).SelectAll();
            foreach (var obj in lst)
            {
                HtmlTableRow tr = new HtmlTableRow();

                HtmlTableCell td = new HtmlTableCell();
                td.InnerHtml = obj.DegreeTitle;
                td.Align = "center";
                tr.Cells.Add(td);

                td = new HtmlTableCell();
                count = getCount(obj.DegreeID, dt);
                td.InnerHtml = count.ToString();
                td.Align = "center";
                tr.Cells.Add(td);
                sum += count;

                tblTable1.Rows.Add(tr);
            }

            HtmlTableRow trSum = new HtmlTableRow();

            HtmlTableCell tdSum = new HtmlTableCell();
            tdSum.InnerHtml = "جمع کل :";
            tdSum.Align = "center";
            trSum.Cells.Add(tdSum);

            tdSum = new HtmlTableCell();
            tdSum.InnerHtml = "<center>" + sum + "</center>";
            tdSum.Align = "center";
            trSum.Cells.Add(tdSum);

            tblTable1.Rows.Add(trSum);
        }

        /// <summary>
        /// تعداد فراوانی
        /// </summary>
        /// <param name="education">کد مدرک تحصیلی</param>
        /// <param name="dt">تاریخ</param>
        /// <returns>تعداد فراوانی</returns>
        private long getCount(byte education,DateTime dt)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                Education = education,
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getVoteCountByEducationalGrade(obj);
        }

        /// <summary>
        /// عدد ماه نخست هر فصل
        /// </summary>
        /// <returns></returns>
        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            string strBoardTitle = @"جدول (3) فراوانی آراء صادره هیات {0} بر حسب مدرک تحصیلی ";
            tdBoardTitle.InnerHtml = string.Format(strBoardTitle, ddlBoard.SelectedItem.Text);
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}