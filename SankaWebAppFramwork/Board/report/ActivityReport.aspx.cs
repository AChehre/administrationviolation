﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class ActivityReport : System.Web.UI.Page
    {
        #region پروپرتی ها

        /// <summary>
        /// ازتاریخ
        /// </summary>
        private DateTime FromDate
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FromDate")))
                {
                    return Request.QueryString.Get("FromDate").StringToDateTime();
                }
                return DateTime.Now.AddYears(1);
            }
        }

        /// <summary>
        /// تا تاریخ
        /// </summary>
        private DateTime ToDate
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("ToDate")))
                {
                    return Request.QueryString.Get("ToDate").StringToDateTime();
                }
                return DateTime.Now.AddYears(1);
            }
        }

        /// <summary>
        /// سال عملیاتی کاربر جاری
        /// </summary>
        private byte UserBoardType
        {
            get
            {
                try
                {
                    return (Session["UserProfile"] as UserProfile).UserBoardType;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                (Session["UserProfile"] as UserProfile).UserBoardType = value;
            }
        }

        /// <summary>
        /// آدرس صفحه بازگشت 
        /// </summary>
        private string returnPageUrl
        {
            get
            {
                return Request.QueryString.Get("returnPageUrl");
            }
        }

        #endregion  پروپرتی ها

        protected void Page_Load(object sLaster, EventArgs e)
        {
            Common.BindBoards(ddlBoard);
            if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                ddlBoard.Enabled = false;
            }
            this.Header.Title = "گزارش فعالیت";
        }

        /// <summary>
        /// بارگذاری جزئیات گزارش
        /// </summary>
        /// <returns></returns>
        private bool LoadReport()
        {
            if (!ValidateDate()) return false;
            var obj = new DTO.Report.tblActivityReport.tblActivityReport
            {
                FromDate = txtFromDate.DateMiladi,
                ToDate = txtToDate.DateMiladi.AddDays(1).AddSeconds(-1)
            };
            if (ddlBoard.SelectedIndex > 0)
            {
                obj.BoardID = ddlBoard.SelectedValue.StringToLong();
            }
            obj = (new DAL.Report.tblActivityReport.Methods()).SelectByDate(obj);
            if (obj.RowNumber > 0)
            {
                lblFirstOfMonth_Jari.Text = obj.Jari_First_Of_Month.ToString();
                lblVaseleh_Jari.Text = obj.Jari_Vaseleh.ToString();
                lblGhati_Jari.Text = obj.Jari_Ghatie.ToString();
                lblResearchable_Jari.Text = obj.Jari_Researchable.ToString();
                lblInocence_Jari.Text = obj.Jari_Innocence.ToString();
                lblClose_Jari.Text = obj.Jari_Closed.ToString();
                //lblLastOfMonth_Jari.Text = obj.Jari_Last_Of_Month.ToString();
                lblLastOfMonth_Jari.Text = GetLastOfMonthJari(obj);

                lblFirstOfMonth_Madde_22_24.Text = obj.Madde_22_24_First_Of_Month.ToString();
                lblVaseleh_Madde22_24.Text = obj.Madde_22_24_Vaseleh.ToString();
                lblGhati_Madde22_24.Text = obj.Madde_22_24_Ghatie.ToString();
                lblResearchable_Madde22_24.Text = obj.Madde_22_24_Researchable.ToString();
                lblInocence_Madde22_24.Text = obj.Madde_22_24_Innocence.ToString();
                lblClose_Madde22_24.Text = obj.Madde_22_24_Closed.ToString();
                //lblLastOfMonth_Madde22_24.Text = obj.Madde_22_24_Last_Of_Month.ToString();
                lblLastOfMonth_Madde22_24.Text = GetLastOfMonthMadde22_24(obj);

                lblFirstOfMonth_Madde_11.Text = obj.Madde_11_First_Of_Month.ToString();
                lblVaseleh_Madde11.Text = obj.Madde_11_Vaseleh.ToString();
                lblGhati_Madde11.Text = obj.Madde_11_Ghatie.ToString();
                lblResearchable_Madde11.Text = obj.Madde_11_Researchable.ToString();
                lblInocence_Madde11.Text = obj.Madde_11_Innocence.ToString();
                lblClose_Madde11.Text = obj.Madde_11_Closed.ToString();
                //lblLastOfMonth_Madde11.Text = obj.Madde_11_Last_Of_Month.ToString();
                lblLastOfMonth_Madde11.Text = GetLastOfMonthMadde11(obj);
                return true;
            }
            return false;
        }

        /// <summary>
        /// ماده 11 آخر ماه
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string GetLastOfMonthMadde11(DTO.Report.tblActivityReport.tblActivityReport obj)
        {
            return (obj.Madde_11_First_Of_Month + obj.Madde_11_Vaseleh - (obj.Madde_11_Ghatie + obj.Madde_11_Researchable + obj.Madde_11_Innocence + obj.Madde_11_Closed)).ToString();
        }

        /// <summary>
        /// ماده 24 آخر ماه
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string GetLastOfMonthMadde22_24(DTO.Report.tblActivityReport.tblActivityReport obj)
        {
            return (obj.Madde_22_24_First_Of_Month + obj.Madde_22_24_Vaseleh - (obj.Madde_22_24_Ghatie + obj.Madde_22_24_Researchable + obj.Madde_22_24_Innocence + obj.Madde_22_24_Closed)).ToString();
        }

        /// <summary>
        /// جاری های آخر ماه
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string GetLastOfMonthJari(DTO.Report.tblActivityReport.tblActivityReport obj)
        {
            return (obj.Jari_First_Of_Month + obj.Jari_Vaseleh - (obj.Jari_Ghatie + obj.Jari_Researchable + obj.Jari_Innocence + obj.Jari_Closed)).ToString();
        }

        /// <summary>
        /// اعتبار سنجی اطلاعات ورودی
        /// </summary>
        /// <returns></returns>
        private bool ValidateDate()
        {
            if (!txtFromDate.DateIsValid)
            {
                SankaDialog1.ShowMessage("تاریخ شروع را وارد نمایید .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                return false;
            }
            if (!txtToDate.DateIsValid)
            {
                SankaDialog1.ShowMessage("تاریخ پایان را وارد نمایید .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                return false;
            }
            if (txtToDate.DateMiladi.Date < txtFromDate.DateMiladi.Date)
            {
                SankaDialog1.ShowMessage("تاریخ شروع باید کوچکتر از تاریخ پایان باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
                return false;
            }
            return true;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            if (LoadReport())
            {
                MultiView1.ActiveViewIndex = 1;
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}