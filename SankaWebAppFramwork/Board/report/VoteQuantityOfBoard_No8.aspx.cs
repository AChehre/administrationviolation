﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SankaWebAppFramework.Board.report
{
    public partial class VoteQuantityOfBoard_No8 : GeneralPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                Common.BindBoards(ddlBoard);
                if (this.UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
                {
                    ddlBoard.SelectedValue = Common.getUserBoardID().ToString();
                    ddlBoard.Enabled = false;
                }
            }
        }

        /// <summary>
        /// بارگذاری سال
        /// </summary>
        private void BindYear()
        {
            for (int i = 1300; i < 1400; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                ddlYear.Items.Add(item);
            }
            ddlYear.SelectedValue = (new System.Globalization.PersianCalendar()).GetYear(DateTime.Now).ToString();
        }

        /// <summary>
        /// بارگذاری اطلاعات در قالب جدول
        /// </summary>
        private void LoadTableNo1()
        {
            long sum = 0;
            DateTime dt = (new System.Globalization.PersianCalendar()).ToDateTime(ddlYear.SelectedValue.StringToInt(), getSessionFirstMonth(), 1, 0, 0, 0, 0);
            long count = 0;
            for (byte i = 0; i < 11; i++)
            {
                count = getCount(i, dt, Enums.VoteType.CONDEMNATION);
                sum += count;
                tblTable1.Rows[i + 2].Cells[2].InnerHtml = "<center>" + count + "</center>";
            }

            count = getCount(13, dt, Enums.VoteType.INNOCENCE);
            sum += count;
            tblTable1.Rows[13].Cells[2].InnerHtml = "<center>" + count + "</center>";

            tblTable1.Rows[14].Cells[2].InnerHtml = "<center>" + sum + "</center>";
        }

        /// <summary>
        /// برگرداندن فراوانی بر اساس نوع جریمه و نوع رای در بازه زمانی مشخص
        /// </summary>
        /// <param name="penaltyType">نوع جریمه</param>
        /// <param name="dt">تاریخ</param>
        /// <param name="voteType">نوع رای برادت یا محکومیت</param>
        /// <returns>تعداد فراوانی</returns>
        private long getCount(byte penaltyType, DateTime dt, Enums.VoteType voteType)
        {
            var obj = new DTO.Board.tblViolationFileVote.tblViolationFileVote
            {
                FromDate = dt.Date,
                ToDate = dt.Date.AddMonths(3).AddMilliseconds(-1),
                BoardID = ddlBoard.SelectedValue.StringToLong(),
                VoteType = (byte)voteType,
                PenaltyType = penaltyType,
                OstanCode = this.BoardOstanCode,
                RegionCode = this.BoardRegionCode
            };
            return (new DAL.Board.tblViolationFileVote.Methods()).getPenaltyTypeCount(obj);
        }

        /// <summary>
        /// گرفتن ماه اول فصل
        /// </summary>
        /// <returns></returns>
        private int getSessionFirstMonth()
        {
            switch (ddlSession.SelectedValue)
            {
                case "1":
                    {
                        return 1;
                    }
                case "2":
                    {
                        return 4;
                    }
                case "3":
                    {
                        return 7;
                    }
                case "4":
                    {
                        return 10;
                    }
            }
            return 1;
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string strTitle = "آمار فراوانی آراء صادره هیات های رسیدگی به تخلفات اداری طی سه ماهه {0} سال {1}";
            this.Header.Title = lblTitle.Text = string.Format(strTitle, ddlSession.SelectedItem.Text, ddlYear.SelectedItem.Text);
            LoadTableNo1();
            string strBoardTitle = @"جدول (2) فراوانی آراء صادره هیات {0} بر حسب مجازات اعمال شده ";
            tdBoardTitle.InnerHtml = string.Format(strBoardTitle, ddlBoard.SelectedItem.Text);
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Board/ViolationFiles.aspx", true);
        }
    }
}