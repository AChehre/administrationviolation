﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Drawing;

namespace SankaWebAppFramework.Board
{
    public partial class ViolationFileDependencies : GeneralPage
    {
        private string DefaultDate
        {
            get
            {
                System.Globalization.PersianCalendar pc = new System.Globalization.PersianCalendar();
                return string.Format(ViolationFileYear.ToString() + "/{0}/{1}",pc.GetMonth(DateTime.Now),pc.GetDayOfMonth(DateTime.Now));
            }
        }

        #region پروپرتی ها

        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long AccusedPersonnelCode
        {
            set
            {
                ViewState.Add("AccusedPersonnelCode", value);
            }
            get
            {
                if (ViewState["AccusedPersonnelCode"] != null)
                {
                    return ViewState["AccusedPersonnelCode"].ToString().StringToInt();
                }
                return 0;
            }
        }


        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long BoardID
        {
            get
            {
                return UserBoardID;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long FileID
        {
            get
            {
                if (Request.QueryString.Get("FileID") != null)
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// وضعیت پرونده
        /// </summary>
        private int? ViolationFileStatus
        {
            set
            {
                ViewState.Add("ViolationFileStatus", value);
            }
            get
            {
                if (ViewState["ViolationFileStatus"] != null)
                {
                    return ViewState["ViolationFileStatus"].ToString().StringToInt();
                }
                return null;
            }
        }

        /// <summary>
        /// مجوزهای انجام عملیات برای کاربر جاری
        /// </summary>
        public List<int> PermitedRequestID
        {
            set
            {
                Session["PermitedRequestID"] = value;
            }
            get
            {
                if (Session["PermitedRequestID"] != null)
                {
                    return (List<int>)Session["PermitedRequestID"];
                }
                return new List<int>();
            }
        }

        /// <summary>
        /// سال عملیاتی پرونده
        /// </summary>
        private int? ViolationFileYear
        {
            set
            {
                ViewState.Add("ViolationFileYear", value);
            }
            get
            {
                if (ViewState["ViolationFileYear"] != null)
                {
                    return ViewState["ViolationFileYear"].ToString().StringToInt();
                }
                return null;
            }
        }

        private int CurrentRequestID
        {
            set
            {
                ViewState.Add("ReqID", value);
            }
            get
            {
                return ViewState["ReqID"].ToString().StringToInt();
            }
        }

        /// <summary>
        /// نحوه مرتب سازی
        /// </summary>
        private string SortOrder
        {
            get
            {
                return hfSortOrder.Value;
            }
            set
            {
                hfSortOrder.Value = value;
            }
        }

        /// <summary>
        /// ستون مرتب سازی
        /// </summary>
        private string SortColumn
        {
            get
            {
                return hfSortColumn.Value;
            }
            set
            {
                hfSortColumn.Value = value;
            }
        }

        private bool UserRequestThisFile
        {
            get
            {
                if (ViewState["UserRequestThisFile"] != null)
                {
                    return ViewState["UserRequestThisFile"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("UserRequestThisFile", value);
            }
        }

        #endregion پروپرتی ها

        protected void Page_Load(object sender, EventArgs e)
        {
            getFileStatus();
            if (ViolationFileStatus != (int)Enums.FileStatus.CLOSE_FILE)
            {
                LoadRequestsButtons();
            }
            //this.MaintainScrollPositionOnPostBack = true;
            if (!IsPostBack)
            {
                setHelpLink(1);
                if (FileID > 0)
                {
                    checkViewFilePermission();
                }
                // ضمائم پرونده بارگذاری میشود

                LoadRequestDataList();
                LoadSendingFileRequestList();
                LoadAccusedInfo();
                LoadRelatedFiles();

                getViolationFileYear();

                if (FileID == 0) // در حالت ثبت پرونده جدید
                {
                    if (!UserCurrentApplicationYear.HasValue) // اگر سال عملیاتی تنظیم نشده بود، به صفحه مربوطه هدایتش کنیم
                    {
                        SankaDialog1.ShowMessage("لطفا سال عملیاتی خود را تنظیم نمایید.", DialogMessage.SankaDialog.Message_Type.Warning, "/board/SetCurrentApplicationYear.aspx", true, true);
                    }
                    this.ViolationFileYear = this.UserCurrentApplicationYear.Value;
                    lnkViolationDetail.Visible = false;
                    MultiView1.ActiveViewIndex = 0;
                    CurrentRequestID = getAddViolationFileRequestID();
                }
                else // در حالتی که پرونده را ویرایش یا مشاهده میکنیم
                {
                    MultiView1.ActiveViewIndex = 1;
                }
            }

            #region مقدار دهی یوزرکنترل ها

            UCAccusedInfo1.FileID = this.FileID;
            UCAccusedInfo1.BoardID = this.BoardID;

            AddViolation1.CurrentApplicationYear = ViolationFileYear;
            AddViolation1.BoardID = this.BoardID;

            AddAccusition1.FileID = this.FileID;
            AddAccusition1.BoardID = this.BoardID;

            AddNotification1.BoardID = this.BoardID;
            AddNotification1.FileID = this.FileID;
            AddNotification1.CurrentApplicationYear = ViolationFileYear;

            UCOtherRequest1.BoardID = this.BoardID;
            UCOtherRequest1.FileID = this.FileID;

            UCAllocationMembers1.FileID = this.FileID;
            UCAllocationMembers1.BoardID = this.BoardID;
            UCAllocationMembers1.ViolationFileYear = ViolationFileYear.Value;

            UCPreviewVote1.BoardID = this.BoardID;
            UCPreviewVote1.FileID = this.FileID;

            UCViolationFileVote1.FileID = this.FileID;
            UCViolationFileVote1.BoardID = this.BoardID;

            UCFileSendingRequest1.FileID = this.FileID;
            UCFileSendingRequest1.BoardID = this.BoardID;
            UCFileSendingRequest1.LockToBoardDropDownList = true;

            UCFurtherInvestigationOrder1.FileID = this.FileID;
            UCFurtherInvestigationOrder1.BoardID = this.BoardID;
            UCFurtherInvestigationOrder1.LockToBoardDropDownList = true;

            UCVoteNotification1.FileID = this.FileID;
            UCVoteNotification1.BoardID = this.BoardID;
            UCVoteNotification1.CurrentApplicationYear = ViolationFileYear.Value;

            UCChangeFileBoard1.FileID = this.FileID;
            UCChangeFileBoard1.BoardID = this.BoardID;

            UCEjectionNotification1.FileID = this.FileID;
            UCEjectionNotification1.BoardID = this.BoardID;
            UCEjectionNotification1.CurrentApplicationYear = ViolationFileYear.Value;

            UCCloseAllocationMembers1.FileID = this.FileID;
            UCCloseAllocationMembers1.BoardID = this.BoardID;
            UCCloseAllocationMembers1.ViolationFileYear = ViolationFileYear.Value;

            UCPreviewClose1.BoardID = this.BoardID;
            UCPreviewClose1.FileID = this.FileID;

            UCViolationFileClosing1.FileID = this.FileID;
            UCViolationFileClosing1.BoardID = this.BoardID;

            #endregion مقدار دهی یوزرکنترل ها

            #region تعریف رویداد های یوزرکنتر ها

            AddViolation1.AddViolation_Completed += new controls.UCAddViolationFile.dlgAddViolation_Completed(AddViolation1_AddViolation_Completed);
            AddViolation1.Error_Occured += new controls.UCAddViolationFile.dlgAddViolation_Error(AddViolation1_Error_Occured);

            AddAccusition1.AddAccusition_Completed += new controls.UCAddAccusition.dlgAddAccusition_Completed(AddAccusition1_AddAccusition_Completed);
            AddAccusition1.Cancel += new controls.UCAddAccusition.dlgAddAccusition_Cancel(AddAccusition1_Cancel);
            AddAccusition1.Error_Occured += new controls.UCAddAccusition.dlgAddAccusition_Error(AddAccusition1_Error_Occured);

            AddNotification1.AddNotification_Completed += new controls.UCAddNotification.dlgAddNotification_Completed(AddNotification1_AddNotification_Completed);
            AddNotification1.Cancel_Click += new controls.UCAddNotification.dlgCancel_Click(AddNotification1_Cancel_Click);
            AddNotification1.Error_Occured += new controls.UCAddNotification.dlgError(AddNotification1_Error_Occured);

            UCOtherRequest1.AddRequest_Completed += new controls.UCOtherRequest.dlgAddRequest_Completed(UCOtherRequest1_AddRequest_Completed);
            UCOtherRequest1.Cancel += new controls.UCOtherRequest.dlgAddAccusition_Cancel(UCOtherRequest1_Cancel);

            UCAllocationMembers1.AllocationMembers_Completed += new controls.UCAllocationMembers.dlgAllocationMembers_Completed(UCAllocationMembers1_AllocationMembers_Completed);
            UCAllocationMembers1.Cancel_Click += new controls.UCAllocationMembers.dlgAddAccusition_CancelClick(UCAllocationMembers1_Cancel_Click);
            UCAllocationMembers1.Error += new controls.UCAllocationMembers.dlgAddAccusition_Error(UCAllocationMembers1_Error);

            UCPreviewVote1.Save_Completed += new controls.UCPreviewVote.dlSaveCompleted(UCPreviewVote1_Save_Completed);
            UCPreviewVote1.Cancel += new controls.UCPreviewVote.dlgAddAccusition_Cancel(UCPreviewVote1_Cancel);

            UCViolationFileVote1.Vote_Completed += new controls.UCViolationFileVote.dlgVote_Completed(UCViolationFileVote1_Vote_Completed);
            UCViolationFileVote1.Cancel += new controls.UCViolationFileVote.dlgVote_Cancel(UCViolationFileVote1_Cancel);
            UCViolationFileVote1.Error_Occured += new controls.UCViolationFileVote.dlgError(UCViolationFileVote1_Error_Occured);

            UCFileSendingRequest1.SendingFileRequest_Completed += new controls.UCFileSendingRequest.dlgSendingFileRequest_Completed(UCFileSendingRequest1_SendingFileRequest_Completed);
            UCFileSendingRequest1.Cancel_Click += new controls.UCFileSendingRequest.dlgSendingFileRequest_CancelClick(UCFileSendingRequest1_Cancel_Click);
            UCFileSendingRequest1.Error_Occured += new controls.UCFileSendingRequest.dlgSendingFileRequest_Error(UCFileSendingRequest1_Error_Occured);

            UCFurtherInvestigationOrder1.FurtherInvestigationOrder_Completed += new controls.UCFurtherInvestigationOrder.dlgFurtherInvestigationOrder_Completed(UCFurtherInvestigationOrder1_FurtherInvestigationOrder_Completed);
            UCFurtherInvestigationOrder1.Cancel_Click += new controls.UCFurtherInvestigationOrder.dlgFurtherInvestigationOrder_CancelClick(UCFurtherInvestigationOrder1_Cancel_Click);
            UCFurtherInvestigationOrder1.Error_Occured += new controls.UCFurtherInvestigationOrder.dlgFurtherInvestigationOrder_Error(UCFurtherInvestigationOrder1_Error_Occured);

            UCVoteNotification1.AddNotification_Completed += new controls.UCVoteNotification.dlgAddNotification_Completed(UCVoteNotification1_AddNotification_Completed);
            UCVoteNotification1.Error_Occured += new controls.UCVoteNotification.dlgError(UCVoteNotification1_Error_Occured);
            UCVoteNotification1.Cancel_Click += new controls.UCVoteNotification.dlgCancel_Click(UCVoteNotification1_Cancel_Click);

            UCChangeFileBoard1.ChangeFileBoard_Completed += new controls.UCChangeFileBoard.dlgChangeFileBoard_Completed(UCChangeFileBoard1_ChangeFileBoard_Completed);
            UCChangeFileBoard1.Cancel_Click += new controls.UCChangeFileBoard.dlgChangeFileBoard_CancelClick(UCChangeFileBoard1_Cancel_Click);
            UCChangeFileBoard1.Error_Occured += new controls.UCChangeFileBoard.dlgChangeFileBoard_Error(UCChangeFileBoard1_Error_Occured);

            UCEjectionNotification1.AddNotification_Completed += new controls.UCEjectionNotification.dlgAddNotification_Completed(UCEjectionNotification1_AddNotification_Completed);
            UCEjectionNotification1.Error_Occured += new controls.UCEjectionNotification.dlgError(UCEjectionNotification1_Error_Occured);
            UCEjectionNotification1.Cancel_Click += new controls.UCEjectionNotification.dlgCancel_Click(UCEjectionNotification1_Cancel_Click);

            UCCloseAllocationMembers1.AllocationMembers_Completed += new controls.UCCloseAllocationMembers.dlgAllocationMembers_Completed(UCCloseAllocationMembers1_AllocationMembers_Completed);
            UCCloseAllocationMembers1.Cancel_Click += new controls.UCCloseAllocationMembers.dlgAddAccusition_CancelClick(UCCloseAllocationMembers1_Cancel_Click);
            UCCloseAllocationMembers1.Error += new controls.UCCloseAllocationMembers.dlgAddAccusition_Error(UCCloseAllocationMembers1_Error);

            UCPreviewClose1.Save_Completed += new controls.UCPreviewClose.dlSaveCompleted(UCPreviewClose1_Save_Completed);
            UCPreviewClose1.Cancel += new controls.UCPreviewClose.dlgAddAccusition_Cancel(UCPreviewClose1_Cancel);

            UCViolationFileClosing1.Closing_Completed += new controls.UCViolationFileClosing.dlgClosing_Completed(UCViolationFileClosing1_Closing_Completed);
            UCViolationFileClosing1.Cancel += new controls.UCViolationFileClosing.dlgClosing_Cancel(UCViolationFileClosing1_Cancel);
            UCViolationFileClosing1.Error_Occured += new controls.UCViolationFileClosing.dlgError(UCViolationFileClosing1_Error_Occured);

            #endregion تعریف رویداد های یوزرکنتر ها

            //setHelpLink(1);

            if (this.UserRequestThisFile)
            {
                pnlRelatedFiles.Hide();
                pnlSendFileRequest.Hide();
                pnlRequestsList.Hide();
                gvRequestDataList.Columns[11].Visible = false;
            }
        }

        void AddViolation1_Error_Occured(List<string> lstMessages)
        {
            SankaDialog1.ShowMessage(GetWarrningList(lstMessages), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCViolationFileClosing1_Error_Occured(List<string> lstMessage)
        {
            SankaDialog1.ShowMessage(GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCViolationFileClosing1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCViolationFileClosing1_Closing_Completed(bool Status, bool isFinalsave)
        {
            if (Status)
            {
                string strMessage = "صورت جلسه مختومه با موفقیت ثبت شد .";
                //string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage(strMessage, DialogMessage.SankaDialog.Message_Type.Message, Page);
                // پس از ثبت نهایی موفقیت آمیز رای، به مرحله ابلاغ رای میرویم
                if (isFinalsave)
                {
                    // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
                    string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                    SankaDialog1.ShowMessage(strMessage, DialogMessage.SankaDialog.Message_Type.Message, strUrl, true);
                }
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت صورت جلسه مختومه خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void UCPreviewClose1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCPreviewClose1_Save_Completed(bool Status)
        {
            if (Status)
            {
                LoadRequestDataList();
                // پاک کردن مقدار کنترل ها
                UCPreviewClose1.LoadData(DefaultDate);

                SankaDialog1.ShowMessage("متن گردش کار با موفقیت ثبت شد.", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت متن گردش کار خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void UCCloseAllocationMembers1_Error(string errorMessage)
        {
            SankaDialog1.ShowMessage(errorMessage, DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCCloseAllocationMembers1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCCloseAllocationMembers1_AllocationMembers_Completed(bool Status)
        {
            if (Status)
            {
                // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("تخصیص اعضا با موفقیت انجام شد .", DialogMessage.SankaDialog.Message_Type.Message, strUrl, true);

            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه تعیین اعضاء خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page, true);
            }
        }

        void UCViolationFileVote1_Error_Occured(List<string> lstMessage)
        {
             SankaDialog1.ShowMessage(GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void AddAccusition1_Error_Occured(List<string> lstMessage)
        {
            SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCEjectionNotification1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID, false));
        }

        void UCEjectionNotification1_Error_Occured(List<string> lstMessage)
        {
            SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCEjectionNotification1_AddNotification_Completed(bool Status)
        {
            SankaDialog1.ShowMessage("ابلاغ حکم با موفقیت ثبت شد .", DialogMessage.SankaDialog.Message_Type.Message, Page);
        }

        void UCChangeFileBoard1_Error_Occured(List<string> lstMessage)
        {
            SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCChangeFileBoard1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCChangeFileBoard1_ChangeFileBoard_Completed(bool Status)
        {
            if (Status)
            {
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("پرونده با موفقیت انتقال یافت .", DialogMessage.SankaDialog.Message_Type.Message, strUrl,true);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه انتقال پرونده خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
            }
        }

        void UCVoteNotification1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID, false));
        }

        void UCVoteNotification1_AddNotification_Completed(bool Status)
        {
            SankaDialog1.ShowMessage("ابلاغ رای با موفقیت ثبت شد .", DialogMessage.SankaDialog.Message_Type.Message, Page);
        }

        void UCVoteNotification1_Error_Occured(List<string> lstMessage)
        {
            SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCCloseFile1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCCloseFile1_CloseFile_Completed(bool Status)
        {
            SankaDialog1.ShowMessage("پرونده بسته شد .", DialogMessage.SankaDialog.Message_Type.Message, Page, true);
        }

        /// <summary>
        /// چک کردن اینکه کاربر مجوز دسترسی به پرونده را دارد یا نه
        /// </summary>
        private void checkViewFilePermission()
        {
            UserProfile profile = Session["UserProfile"] as UserProfile;
            if (profile.UserID == 0)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            if (UserBoardType != (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora)
            {
                var objBoard = new DTO.Board.tblFileAccessMap.tblFileAccessMap
                {
                    FileID = this.FileID,
                    BoardID = this.BoardID
                };
                byte bytViewType = (new DAL.Board.tblFileAccessMap.Methods()).HasPermission(objBoard);
                if (bytViewType == 0 /* مجوز دسترسی ندارد */)
                {
                    SankaDialog1.ShowMessage("شما مجوز دسترسی به این پرونده را ندارید .", DialogMessage.SankaDialog.Message_Type.Warning, "/Board/ViolationFiles.aspx?action=0", true);
                }
                this.UserRequestThisFile = (bytViewType == 2 /* فقط قابل خواندن */);
            }
        }

        /// <summary>
        /// استخراج شناسه جدول درخواست ها مرتبط به درخواست ایجاد پرونده بر اساس نوع هیات
        /// </summary>
        /// <returns></returns>
        private int getAddViolationFileRequestID()
        {
            switch ((AdministrationViolation.Admin.Enums.Board_Type)this.UserBoardType)
            {
                case  AdministrationViolation.Admin.Enums.Board_Type.Initial:
                    {
                        return 1;
                    }
                case AdministrationViolation.Admin.Enums.Board_Type.Revision:
                    {
                        return 29;
                    }
                case AdministrationViolation.Admin.Enums.Board_Type.Shora:
                    {
                        return 56;
                    }
            }
            return 1;
        }

        /// <summary>
        /// بارگذاری سال عملیاتی
        /// </summary>
        private void getViolationFileYear()
        {
            if (this.FileID > 0)
            {
                var obj = new DTO.Board.tblViolationFile.tblViolationFile
                {
                    FileID = this.FileID
                };
                ViolationFileYear = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj).ActivityYear;
            }
        }

        /// <summary>
        ///  بدست آوردن وضعیت پرونده
        /// </summary>
        private void getFileStatus()
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile();
            obj.FileID = FileID;
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                ViolationFileStatus = obj.Status;
                btnUnlock.OnClientClick = string.Format("return ShowConfirm(this,'آیا میخواهید قابلیت اعمال تغییرات روی پرونده شماره {0} را فعال نمایید ؟ ')", obj.SerialCode);
                btnUnlock.Visible = 
                (
                    this.UserBoardType == (byte)AdministrationViolation.Admin.Enums.Board_Type.Shora
                    && !Common.IsUnlockedFile(this.FileID)
                    && obj.BoardID != this.UserBoardID
                );
            }
        }

        /// <summary>
        /// بار گذاری لیست درخواست های ارسال پرونده در گرید لیست درخواستها
        /// </summary>
        private void LoadSendingFileRequestList()
        {
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                RelatedFileID = this.FileID
            };
            var lst = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByRelatedFileID(obj);
            if (lst.Any())
            {
                gvFileSendRequest.DataSource = lst;
                gvFileSendRequest.DataBind();
            }
            else
            {
                pnlSendFileRequest.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
        }

        #region پیاده سازی بدنه رویدادهای کنترل ها

        void UCViolationFileVote1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCViolationFileVote1_Vote_Completed(bool Status)
        {
            if (Status)
            {
                //string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("رای  با موفقیت ثبت شد .", DialogMessage.SankaDialog.Message_Type.Message, Page);
                // پس از ثبت موفقیت آمیز رای، به مرحله ابلاغ رای میرویم
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت رای خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void UCPreviewVote1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCPreviewVote1_Save_Completed(bool Status)
        {
            if (Status)
            {
                LoadRequestDataList();
                // پاک کردن مقدار کنترل ها
                UCPreviewVote1.LoadData(DefaultDate);

                SankaDialog1.ShowMessage("متن گردش کار با موفقیت ثبت شد.", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت متن گردش کار خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void UCAllocationMembers1_Error(string errorMessage)
        {
            SankaDialog1.ShowMessage(errorMessage, DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCAllocationMembers1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID, false));
        }

        void UCAllocationMembers1_AllocationMembers_Completed(bool Status)
        {
            if (Status)
            {
                // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("تخصیص اعضا با موفقیت انجام شد .", DialogMessage.SankaDialog.Message_Type.Message, strUrl, true);

            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه تعیین اعضاء خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page, true);
            }
        }

        void UCOtherRequest1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCOtherRequest1_AddRequest_Completed(bool Status , long requestID)
        {
            if (Status)
            {
                switch (requestID)
                {
                    case 77:
                        {
                            SetTempCommandFlag(true);
                            break;
                        }
                    case 89:
                        {
                            SetTempCommandFlag(false);
                            break;
                        }
                }
                SankaDialog1.ShowMessage("اطلاعات با موفقیت ثبت گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page, true);
                UCOtherRequest1.LoadData();
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت درخواست خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        /// <summary>
        /// درج فلگ دستور موقت
        /// </summary>
        private void SetTempCommandFlag(bool hasTempCommand)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                HasTempCommand = hasTempCommand
            };
            (new DAL.Board.tblViolationFile.Methods()).SetTempCommandFlag(obj);
        }

        void AddNotification1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void AddNotification1_AddNotification_Completed(bool Status)
        {
            if (Status)
            {
                SankaDialog1.ShowMessage("ابلاغیه با موفقیت ذخیره شد .", DialogMessage.SankaDialog.Message_Type.Message, Page, true);
            }
            else
            {
                SankaDialog1.ShowMessage("خطا در ذخیره ابلاغیه .", DialogMessage.SankaDialog.Message_Type.Error, Page, true);
            }
        }

        void AddAccusition1_Cancel()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void AddAccusition1_AddAccusition_Completed(bool Status)
        {
            if (Status)
            {
                SankaDialog1.ShowMessage("اتهام با موفقیت ذخیره شد .", DialogMessage.SankaDialog.Message_Type.Message, Page);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ثبت اتهام خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void AddAccusition1_Cancel_Click()
        {
            MultiView1.ActiveViewIndex = 1;
        }

        void AddViolation1_AddViolation_Completed(long fileID,string creationDae)
        {
            string strMessage = string.Format("تشکیل پرونده صورت گرفت . تاریخ تشکیل : {0}", creationDae);
            if (Common.InsertRequestDataRecord(this.UserID, this.CurrentRequestID, fileID, strMessage) > 0)
            {
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", fileID, this.BoardID);
                SankaDialog1.ShowMessage("پرونده با موفقیت ایجاد شد .", DialogMessage.SankaDialog.Message_Type.Message, strUrl, true, true);
            }
        }

        void UCFileSendingRequest1_Error_Occured(string errorMessage)
        {
            SankaDialog1.ShowMessage(errorMessage, DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCFileSendingRequest1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCFileSendingRequest1_SendingFileRequest_Completed(bool Status)
        {
            if (Status)
            {
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("درخواست ارسال پرونده با موفقیت ذخیره گردید .", DialogMessage.SankaDialog.Message_Type.Message, strUrl);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه ذخیره درخواست ارسال پرونده خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        void AddNotification1_Error_Occured(List<string> lstMessage , bool closeView)
        {
            string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
            if (closeView)
            {
                SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, strUrl, true);
            }
            else
            {
                SankaDialog1.ShowMessage(this.GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning,Page);
            }
        }

        void UCFurtherInvestigationOrder1_Error_Occured(string errorMessage)
        {
            SankaDialog1.ShowMessage(errorMessage, DialogMessage.SankaDialog.Message_Type.Warning, Page);
        }

        void UCFurtherInvestigationOrder1_Cancel_Click()
        {
            // به صفحه ای که از این کنترل استفاده میکنید، یک ایونت میدهیم که دکمه کنسل کلیک شده است
            Response.Redirect(string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID));
        }

        void UCFurtherInvestigationOrder1_FurtherInvestigationOrder_Completed(bool Status)
        {
            if (Status)
            {
                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("دستور رسیدگی مجدد پرونده ارسال گردید .", DialogMessage.SankaDialog.Message_Type.Message, strUrl);
            }
            else
            {
                SankaDialog1.ShowMessage("در پروسه دستور رسیدگی مجدد پرونده خطایی رخ داده است .", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        #endregion پیاده سازی بدنه رویدادهای کنترل ها

        /// <summary>
        /// بار گذاری اطلاعات متهم
        /// </summary>
        private void LoadAccusedInfo()
        {
            UCAccusedInfo1.FileID = this.FileID;
            this.AccusedPersonnelCode = UCAccusedInfo1.LoadData();
            lnkAccusedOtherFile.NavigateUrl = string.Format("~/Board/AccisedOtherFiles.aspx?PCode={0}", this.AccusedPersonnelCode);
        }

        /// <summary>
        /// بارگذاری لیست درخواستهایی که تاکنون برای این پرونده ثبت گردیده است
        /// </summary>
        private void LoadRequestDataList()
        {
            if (this.FileID > 0)
            {
                DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData
                {
                    FileID = this.FileID,
                    SortColumn = this.SortColumn,
                    SortOrder = this.SortOrder,
                };
                gvRequestDataList.DataSource = (new DAL.Board.tblRequestData.Methods()).SelectByFileID(obj);
                gvRequestDataList.DataBind();
                SetSortStatus();
            }
        }

        /// <summary>
        /// بارگذاری لیست دکمه های عملیاتی برای انجام عملیات برروی پرونده
        /// </summary>
        private void LoadRequestsButtons()
        {
            pnlBasicRequest.Controls.Clear();
            pnlMainRequest.Controls.Clear();
            pnlSubsidiaryRequest.Controls.Clear();

            DTO.Config.tblRequest.tblRequest objRequest = new DTO.Config.tblRequest.tblRequest { BoardType = this.UserBoardType };
            List<DTO.Config.tblRequest.tblRequest> lstRequests = (new DAL.Config.tblRequest.Methods()).SelectAll(objRequest);

            setPermitedRequestIDs(lstRequests);

            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData { FileID = this.FileID };
            List<DTO.Board.tblRequestData.tblRequestData> lstRequestData = (new DAL.Board.tblRequestData.Methods()).SelectByFileID(obj);

            foreach (DTO.Config.tblRequest.tblRequest request in lstRequests)
            {
                if (request.Enabled.HasValue && !request.Enabled.Value) continue;
                if (ViolationFileStatus >= (int)Enums.FileStatus.CLOSE_FILE)
                {
                    continue;
                }
                
                if (ViolationFileStatus >= (int)Enums.FileStatus.SODOURE_RAY)
                {
                    if (request.RequestID >= 86 && request.RequestID <= 88) // صدور صورتجلسه مختومه
                    {
                        continue;
                    }
                }

                if (this.ViolationFileStatus >= (int)Enums.FileStatus.SODOURE_RAY)
                {
                    if (request.FileStatusIndex < (int)Enums.FileStatus.SODOURE_RAY)
                    {
                        continue;
                    }
                }

                if (request.PrequisitRequestIDs == null)
                {
                    AddButton(request);
                }
                else
                {
                    if (lstRequestData.Any())
                    {
                        foreach (DTO.Board.tblRequestData.tblRequestData reqData in lstRequestData)
                        {
                            string[] strPrequisitIDs = request.PrequisitRequestIDs.Split(',');
                            foreach (string strID in strPrequisitIDs)
                            {
                                if (strID.StringToInt() == reqData.RequestID)
                                {
                                    if (reqData.Status == 1)
                                    {
                                        AddButton(request);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (pnlBasicRequest.Controls.Count == 0 && pnlMainRequest.Controls.Count == 0 && pnlSubsidiaryRequest.Controls.Count == 0)
            {
                pnlRequestsList.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
        }

        /// <summary>
        /// درج لیست مجوزهای داده شده به کاربر جهت ثبت درخواست ها داخل سشن
        /// </summary>
        /// <param name="lstRequests"></param>
        private void setPermitedRequestIDs(List<DTO.Config.tblRequest.tblRequest> lstRequests)
        {
            List<int> lst = new List<int>();
            foreach (var obj in lstRequests)
            {
                lst.Add(obj.RequestID);
            }
            this.PermitedRequestID = lst;
        }

        /// <summary>
        /// دکمه های عملیاتی را تولید و برروی فرم نمایش میدهد
        /// </summary>
        /// <param name="request">شی درخواست عملیات</param>
        private void AddButton(DTO.Config.tblRequest.tblRequest request)
        {
            if (request.RequestType == 3)
            {
                if (!ValidateBasicRequestButton(request.RequestID))
                {
                    return;
                }
            }

            Button btn = new Button
            {
                ID = "btn_" + request.RequestID,
                Text = request.Title,
                CommandArgument = request.RequestID.ToString() + "_" + request.RelatedAttachementTypes + "_" + request.RequestType,
                Width = 183,
                ToolTip = request.Title,
            };
            btn.Attributes.Add("data-sankatooltip", btn.ToolTip);
            btn.Command += new CommandEventHandler(btn_Command);

            switch (request.RequestType)
            {
                case 1:
                    {
                        if (pnlMainRequest.FindControl("btn_" + request.RequestID) == null)
                        {
                            btn.CssClass = "btn btn-primary basic";
                            btn.Style.Add("margin-left", "2px");
                            if (!string.IsNullOrEmpty(request.BgColor))
                            {
                                btn.Style.Add("background-color", request.BgColor);
                            }
                            if (!string.IsNullOrEmpty(request.FontColor))
                            {
                                btn.Style.Add("color", request.FontColor);
                            }
                            pnlMainRequest.Controls.Add(btn);
                        }
                        break;
                    }
                case 2:
                    {
                        if (pnlSubsidiaryRequest.FindControl("btn_" + request.RequestID) == null)
                        {
                            btn.CssClass = "btn btn-primary basic";
                            btn.Style.Add("margin-left", "2px");
                            if (!string.IsNullOrEmpty(request.BgColor))
                            {
                                btn.Style.Add("background-color", request.BgColor);
                            }
                            if (!string.IsNullOrEmpty(request.FontColor))
                            {
                                btn.Style.Add("color", request.FontColor);
                            }
                            pnlSubsidiaryRequest.Controls.Add(btn);
                        }
                        break;
                    }
                case 3:
                    {
                        if (pnlBasicRequest.FindControl("btn_" + request.RequestID) == null)
                        {
                            btn.CssClass = "btn btn-primary basic";
                            btn.Style.Add("margin-left", "2px");
                            if (!string.IsNullOrEmpty(request.BgColor))
                            {
                                btn.Style.Add("background-color", request.BgColor);
                            }
                            if (!string.IsNullOrEmpty(request.FontColor))
                            {
                                btn.Style.Add("color", request.FontColor);
                            }
                            pnlBasicRequest.Controls.Add(btn);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// آیا درخواست داده شده از نوع درخواستهایی است که باید تمامی پیش نیاز های آنبرآورده شده باشد
        /// </summary>
        /// <param name="requestID">شناسه رخواست</param>
        /// <returns></returns>
        private bool ValidateBasicRequestButton(int requestID)
        {
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData
            {
                FileID = this.FileID,
                RequestID = requestID
            };
            bool bln = (new DAL.Board.tblRequestData.Methods()).tblRequestDataCheckAllPrequisit(obj);
            return bln;
        }

        /// <summary>
        /// ثبت فلگ دفاعیه برای پرونده
        /// </summary>
        /// <param name="hasDefenceBill"></param>
        private void SetDefenceBillFlag(bool hasDefenceBill)
        {
            var objFile = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID,
                HasDefenseBill = hasDefenceBill
            };
            (new DAL.Board.tblViolationFile.Methods()).SetDefenceBillFlag(objFile);

        }

        /// <summary>
        /// رویداد بازگشت به صفحه جزئیات پرونده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
            LoadRequestDataList();
            pnlRequestsList.Show();
        }

        /// <summary>
        /// حذف درخواست
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteRequest_Command(object sender, CommandEventArgs e)
        {
            string[] strArgument = e.CommandArgument.ToString().Split('_');
            int requestID = strArgument[0].StringToInt();
            long requestDataID = strArgument[1].StringToLong();

            if (!Common.IsUnlockedFile(this.FileID) || requestID == 1 || requestID == 29 || requestID == 56)
            {
                if (!this.PermitedRequestID.Contains(requestID))
                {
                    SankaDialog1.ShowMessage("شما مجاز به حذف این رکورد نمی باشید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
                if (!IsThisBoardOwnerTheFile())
                {
                    SankaDialog1.ShowMessage("این پرونده در اختیار این هیات نمیباشد . لذا مجاز به حذف درخواست های آن نمی باشید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
            }

            if (requestID == 2 || requestID == 30) //  ثبت گزارش تخلف
            {
                if (CheckAccusationHasBeedNotified(requestDataID))
                {
                    SankaDialog1.ShowMessage("این اتهام ابلاغ گردیده است و امکان حذف آن وجود ندارد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
            }

            if (requestID == 8 || requestID == 36) //  لایحه دفاعیه
            {
                SetDefenceBillFlag(false);
            }

            int DependedData = Common.CheckHasDependedRequestData(requestDataID, this.FileID);
            if (DependedData > 0)
            {
                if (DependedData == 255)
                {
                    SankaDialog1.ShowMessage("پس از صدور رای امکان حذف عملیات صورت گرفته برروی پرونده وجود ندارد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
                else if (DependedData == 254)
                {
                    SankaDialog1.ShowMessage("با درخواست ارسال پرونده موافقت شده است . لذا مجاز به حذف این رکورد نمی باشید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
                else if (DependedData == 253)
                {
                    if (!Common.IsUnlockedFile(this.FileID))
                    {
                        SankaDialog1.ShowMessage("پرونده مختومه اعلام گردیده است . لذا مجاز به حذف این رکورد نمی باشید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                        return;
                    }
                }
                else if (DependedData == 256)
                {
                    if (!Common.IsUnlockedFile(this.FileID))
                    {
                        SankaDialog1.ShowMessage("شما مجاز به حذف این درخواست نمی باشد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                        return;
                    }
                }
                else
                {
                    SankaDialog1.ShowMessage("به علت وابسته بودن عملیات تکمیل شده دیگری به این رکورد ، امکان حذف وجود ندارد .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                    return;
                }
            }
            if (!CheckAfterFurtherInvestigationOrder(requestDataID))
            {
                SankaDialog1.ShowMessage("شما مجوز حذف این رکورد را ندارید .", DialogMessage.SankaDialog.Message_Type.Warning, Page);
                return;
            }
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData()
            {
                RequestDataID = requestDataID,
                RequestID = requestID,
                FileID = this.FileID
            };
            bool bln = (new DAL.Board.tblRequestData.Methods()).Delete(obj);
            if (bln)
            {
                // لاگ حذف درخواست
                (new Logger()).InsertRequestDataModificationLog(requestDataID, Logger.RecardModificationFlag.DELETE);

                if (requestID == 77)
                {
                    SetTempCommandFlag(false);
                }

                string strUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("رکورد با موفقیت حذف گردید .", DialogMessage.SankaDialog.Message_Type.Message, strUrl, true);
            }
        }

        /// <summary>
        /// آیا هیات جاری مالک پرونده است
        /// </summary>
        /// <returns></returns>
        private bool IsThisBoardOwnerTheFile()
        {
            long userBoardID = (Session["UserProfile"] as UserProfile).UserBoardID;
            var obj = new DTO.Board.tblViolationFile.tblViolationFile { FileID = this.FileID };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            return userBoardID == obj.BoardID;

        }

        /// <summary>
        ///? آیا دستور رسیدگی مجدد پرونده صادر شده
        /// </summary>
        /// <param name="requestDataID">شناسه درخواست ثبت شده</param>
        /// <returns></returns>
        private bool CheckAfterFurtherInvestigationOrder(long requestDataID)
        {
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData
            {
                RequestID = 85,
                FileID = this.FileID,
            };
            obj = (new DAL.Board.tblRequestData.Methods()).SelectByRequestID(obj);
            return (requestDataID > obj.RequestDataID);
        }

        /// <summary>
        /// مقدار دهی لینک راهنمای درخواست
        /// </summary>
        /// <param name="RequestID"></param>
        private void setHelpLink(int RequestID)
        {
            lnkHelp.NavigateUrl = string.Format("~/Board/Help.aspx?ID={0}", RequestID);
            lnkHelp.Target = "_blank";
        }

        /// <summary>
        /// مرتب سازی براساس ستون ها
        /// هنگام کلیک روی هدر ستونها، جهت مرتب سازی ، این تابع فراخوانی میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnSort_Command(object sender, CommandEventArgs e)
        {
            SortColumn = e.CommandArgument.ToString();
            SortOrder = e.CommandName.ToString();

            // موقع کلیک روی دکمه، نتایج جدید برای صفحه اول بیاید
            PageNumber = 1;
            LoadRequestDataList();
        }

        /// <summary>
        /// تنظیم کردن حالت مرتب سازی گرید
        /// ستونی که روی آن کلیک شده را در گرید پیدا میکنیم، و اگر حالت فعلی مرتب سازی آن صعودی باشد
        /// به نزولی تبدیل میکنیم یا بالعکس
        /// </summary>
        protected void SetSortStatus()
        {
            try
            {
                if (gvRequestDataList.Rows.Count > 0)
                {
                    string ControlID = "lnkbtn" + SortColumn;
                    LinkButton lnkbtn = gvRequestDataList.HeaderRow.FindControl(ControlID) as LinkButton;
                    System.Web.UI.WebControls.Image img = gvRequestDataList.HeaderRow.FindControl("img" + SortColumn) as System.Web.UI.WebControls.Image;

                    if (lnkbtn != null)
                    {
                        if (SortOrder == "DESC")
                        {
                            lnkbtn.CommandName = "ASC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_down.png";
                                img.Visible = true;
                            }
                        }
                        else if (SortOrder == "ASC")
                        {
                            lnkbtn.CommandName = "DESC";
                            if (img != null)
                            {
                                img.ImageUrl = "/images/sort_up.png";
                                img.Visible = true;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// بار گذاری لیست پرونده های مرتبط با این پرونده
        /// </summary>
        private void LoadRelatedFiles()
        {
            DTO.Board.tblFileRelationMap.tblFileRelationMap obj = new DTO.Board.tblFileRelationMap.tblFileRelationMap
            {
                FileID = this.FileID
            };
            var lst = (new DAL.Board.tblFileRelationMap.Methods()).SelectAll(obj);
            if (lst.Any())
            {
                foreach (DTO.Board.tblFileRelationMap.tblFileRelationMap file in lst)
                {
                    phRelatedFiles.Controls.Add(CreateButton(file));
                }
            }
            else
            {
                pnlRelatedFiles.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
        }

        /// <summary>
        /// ایجاد هایپرلینک فایلهای مرتیط
        /// </summary>
        /// <param name="file">شی جدول فایلهای مرتبط</param>
        /// <returns></returns>
        private HyperLink CreateButton(DTO.Board.tblFileRelationMap.tblFileRelationMap file)
        {
            HyperLink btn = new HyperLink
            {
                ID = "btnRelatedFile" + file.FileID,
                Text = file.SerialCode,
                NavigateUrl = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", file.RelatedFileID,getBoardID(file.RelatedFileID)),
                Width = 183,
                ToolTip = " مشاهده پرونده تخلف شماره " + file.SerialCode,
                Target = "_blank"
            };
            btn.Attributes.Add("data-sankatooltip", btn.ToolTip);
            btn.CssClass = "btn btn-success";
            btn.Style.Add("margin-left", "2px");
            return btn;
        }

        /// <summary>
        /// استخراج شناسه هیات
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        private long getBoardID(long fileID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.BoardID.Value;
            }
            return 0;
        }

        /// <summary>
        /// کد های درخواست را دریافت و به تبع ان ویومرتبط با درخواست را نمایش میدهد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_Command(object sender, CommandEventArgs e)
        {
            string[] strArgument = e.CommandArgument.ToString().Split('_');
            this.CurrentRequestID = strArgument[0].StringToInt();
            string relatedAttachementTypes = strArgument[1];
            byte requestType = strArgument[2].StringToByte();

            //if (requestType == 3 && /* صورت جلسه مختومه نبود */ !CloseFileRequest(this.CurrentRequestID))
            //{
            //    CheckAllAccusitionForStatus();
            //}

            switch (this.CurrentRequestID)
            {
                case 1:
                    {
                        MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case 29 :
                    {
                        MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case 56:
                    {
                        MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                //-------------------------------------------------------
                case 2:
                    {
                        AddAccusition1.RequestID = this.CurrentRequestID;
                        AddAccusition1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                case 30:
                    {
                        AddAccusition1.RequestID = this.CurrentRequestID;
                        AddAccusition1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                case 57:
                    {
                        AddAccusition1.RequestID = this.CurrentRequestID;
                        AddAccusition1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                //-----------------------------------------------------
                case 3:
                    {
                        AddNotification1.RequestID = this.CurrentRequestID;
                        // پاک کردن مقدار کنترل ها
                        AddNotification1.ResetAllControls(DefaultDate);
                        AddNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                case 31:
                    {
                        AddNotification1.RequestID = this.CurrentRequestID;
                        // پاک کردن مقدار کنترل ها
                        AddNotification1.ResetAllControls(DefaultDate);
                        AddNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                case 58:
                    {
                        AddNotification1.RequestID = this.CurrentRequestID;
                        // پاک کردن مقدار کنترل ها
                        AddNotification1.ResetAllControls(DefaultDate);
                        AddNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                //-----------------------------------------------------
                case 14:
                    {
                        CheckAllAccusitionForStatus();
                        UCAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                case 42:
                    {
                        CheckAllAccusitionForStatus();
                        UCAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                case 69:
                    {
                        CheckAllAccusitionForStatus();
                        UCAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                //-----------------------------------------------------
                case 15:
                    {
                        UCPreviewVote1.RequestID = this.CurrentRequestID;
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                case 43:
                    {
                        UCPreviewVote1.RequestID = this.CurrentRequestID;
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                case 70:
                    {
                        UCPreviewVote1.RequestID = this.CurrentRequestID;
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                //-----------------------------------------------------
                case 16:
                    {
                        CheckAllAccusitionForStatus();
                        UCViolationFileVote1.RequestID = this.CurrentRequestID;
                        UCViolationFileVote1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                case 44:
                    {
                        CheckAllAccusitionForStatus();
                        UCViolationFileVote1.RequestID = this.CurrentRequestID;
                        UCViolationFileVote1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                case 71:
                    {
                        CheckAllAccusitionForStatus();
                        UCViolationFileVote1.RequestID = this.CurrentRequestID;
                        UCViolationFileVote1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                //-----------------------------------------------------
                case 28:
                    {
                        UCFileSendingRequest1.RequestID = this.CurrentRequestID;
                        UCFileSendingRequest1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                case 55:
                    {
                        UCFileSendingRequest1.RequestID = this.CurrentRequestID;
                        UCFileSendingRequest1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                case 82:
                    {
                        UCFileSendingRequest1.RequestID = this.CurrentRequestID;
                        UCFileSendingRequest1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                //----------------------------------------------------
                case 85:
                    {
                        UCFurtherInvestigationOrder1.RequestID = this.CurrentRequestID;
                        UCFurtherInvestigationOrder1.LoadData();
                        MultiView1.ActiveViewIndex = 9;
                        break;
                    }
                //----------------------------------------------------
                case 17:
                    {
                        UCVoteNotification1.RequestID = this.CurrentRequestID;
                        UCVoteNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                case 45:
                    {
                        UCVoteNotification1.RequestID = this.CurrentRequestID;
                        UCVoteNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                case 72:
                    {
                        UCVoteNotification1.RequestID = this.CurrentRequestID;
                        UCVoteNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                //----------------------------------------------------
                case 104:
                    {
                        UCChangeFileBoard1.RequestID = this.CurrentRequestID;
                        UCChangeFileBoard1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCChangeFileBoard1.LoadData();
                        MultiView1.ActiveViewIndex = 11;
                        break;
                    }
                //----------------------------------------------------
                case 102:
                    {
                        UCEjectionNotification1.RequestID = this.CurrentRequestID;
                        UCEjectionNotification1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCEjectionNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 12;
                        break;
                    }
                //-----------------------------------------------------
                case 127:
                    {
                        UCCloseAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCCloseAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 13;
                        break;
                    }
                case 129:
                    {
                        UCCloseAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCCloseAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 13;
                        break;
                    }
                case 131:
                    {
                        UCCloseAllocationMembers1.RequestID = this.CurrentRequestID;
                        UCCloseAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 13;
                        break;
                    }
                //-----------------------------------------------------
                case 128:
                    {
                        //if (HaveAnyNotNotifiedAcciusition()) return;
                        UCPreviewClose1.RequestID = this.CurrentRequestID;
                        UCPreviewClose1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 14;
                        break;
                    }
                case 130:
                    {
                        //if (HaveAnyNotNotifiedAcciusition()) return;
                        UCPreviewClose1.RequestID = this.CurrentRequestID;
                        UCPreviewClose1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 14;
                        break;
                    }
                case 132:
                    {
                        //if (HaveAnyNotNotifiedAcciusition()) return;
                        UCPreviewClose1.RequestID = this.CurrentRequestID;
                        UCPreviewClose1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 14;
                        break;
                    }
                //----------------------------------------------------------
                case 86:
                    {
                        UCViolationFileClosing1.RequestID = this.CurrentRequestID;
                        UCViolationFileClosing1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileClosing1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCViolationFileClosing1.LoadData();
                        MultiView1.ActiveViewIndex = 15;
                        break;
                    }
                case 87:
                    {
                        UCViolationFileClosing1.RequestID = this.CurrentRequestID;
                        UCViolationFileClosing1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileClosing1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCViolationFileClosing1.LoadData();
                        MultiView1.ActiveViewIndex = 15;
                        break;
                    }
                case 88:
                    {
                        UCViolationFileClosing1.RequestID = this.CurrentRequestID;
                        UCViolationFileClosing1.ActivityYear = this.UserCurrentApplicationYear.Value;
                        UCViolationFileClosing1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCViolationFileClosing1.LoadData();
                        MultiView1.ActiveViewIndex = 15;
                        break;
                    }
                //----------------------------------------------------------
                default:
                    {
                        UCOtherRequest1.RequestID = this.CurrentRequestID;
                        UCOtherRequest1.RelatedAttachementTypes = relatedAttachementTypes;
                        UCOtherRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 4;
                        break;
                    }
            }
            setHelpLink(this.CurrentRequestID);
        }

        /// <summary>
        /// برای هیات بدوی باید چک کنیم که پرونده ارجاعی نباشد بعد شرط لبلاغ را اجباری کنیم .
        /// </summary>
        private bool CheckIsRefredFile()
        {
            var obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = this.FileID
            };
            obj = (new DAL.Board.tblViolationFile.Methods()).SelectByID(obj);
            return (obj.FileReferType == 0 || obj.ReferHistoryCounter == 0);
        }

        /// <summary>
        /// درخواست از نوع صورت جلیه مختومه است یا نه ؟
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        private bool CloseFileRequest(int requestID)
        {
            return (requestID == 127 || requestID == 129 || requestID == 131);
        }

        /// <summary>
        /// چک کردن اینکه قبل از صدور رای باید ....
        /// </summary>
        private void CheckAllAccusitionForStatus()
        {
            if (CheckIsRefredFile())
            {
                var objAccusitionView = new DTO.Board.tblAccusitionView.tblAccusitionView();
                objAccusitionView.ViolationFileID = this.FileID;
                var RList = (new DAL.Board.tblAccusitionView.Methods()).SelectNotNotificated(objAccusitionView);
                if (RList.Any())
                {
                    string strPath = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                    SankaDialog1.ShowMessage("ابتدا لازم است تا تمامی اتهامات ابلاغ گردد .", DialogMessage.SankaDialog.Message_Type.Warning, strPath, true);
                    return;
                }
            }

            DTO.Board.tblAccusation.tblAccusation obj = new DTO.Board.tblAccusation.tblAccusation
            {
                ViolationFileID = this.FileID
            };
            if ((new DAL.Board.tblAccusation.Methods()).HasUnknownAccusitionStatus(obj))
            {
                string strPath = string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}", this.FileID, this.BoardID);
                SankaDialog1.ShowMessage("وضعیت برخی از اتهام های موجود در پرونده تعیین نگردیده است .", DialogMessage.SankaDialog.Message_Type.Warning, strPath, true);
                return;
            }
        }

        protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
        {
            if (MultiView1.ActiveViewIndex > 0)
            {
                if (MultiView1.ActiveViewIndex > 1)
                {
                    pnlRequestsList.Hide();
                    pnlSendFileRequest.Hide();
                    pnlDoneRequest.Hide();
                    pnlMainRequest.Hide();
                    pnlSubsidiaryRequest.Hide();
                    pnlBasicRequest.Hide();
                    pnlRelatedFiles.Hide();
                }
                else
                {
                    pnlRequestsList.Show();
                    pnlSendFileRequest.Show();
                    pnlDoneRequest.Show();
                    pnlMainRequest.Show();
                    pnlSubsidiaryRequest.Show();
                    pnlBasicRequest.Show();
                    pnlRelatedFiles.Show();
                }
            }

        }

        /// <summary>
        /// لیست ضمائم مرتبط با درخواست را براساس کد درخواست بارگذاری میکند
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAttachment_Command(object sender, CommandEventArgs e)
        {
            string[] strArgument = e.CommandArgument.ToString().Split('_');
            int RequestID = strArgument[0].StringToInt();
            long RequestDataID = strArgument[1].StringToLong();
            string strRelatedAttachementTypes = strArgument[2];

            setHelpLink(RequestID);

            switch (RequestID) // شناسه جدول   شناسه درخواست ها
            {
                //-----------------------------------------
                case 2:
                    {
                        AddAccusition1.FileID = this.FileID;
                        AddAccusition1.RequestID = RequestID;
                        AddAccusition1.RequestDataID = RequestDataID;
                        AddAccusition1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                case 30:
                    {
                        AddAccusition1.FileID = this.FileID;
                        AddAccusition1.RequestID = RequestID;
                        AddAccusition1.RequestDataID = RequestDataID;
                        AddAccusition1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                case 57:
                    {
                        AddAccusition1.FileID = this.FileID;
                        AddAccusition1.RequestID = RequestID;
                        AddAccusition1.RequestDataID = RequestDataID;
                        AddAccusition1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddAccusition1.LoadData();
                        MultiView1.ActiveViewIndex = 2;
                        break;
                    }
                //-----------------------------------------
                case 3:
                    {
                        AddNotification1.FileID = this.FileID;
                        AddNotification1.RequestID = RequestID;
                        AddNotification1.RequestDataID = RequestDataID;
                        AddNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                case 31:
                    {
                        AddNotification1.FileID = this.FileID;
                        AddNotification1.RequestID = RequestID;
                        AddNotification1.RequestDataID = RequestDataID;
                        AddNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                case 58:
                    {
                        AddNotification1.FileID = this.FileID;
                        AddNotification1.RequestID = RequestID;
                        AddNotification1.RequestDataID = RequestDataID;
                        AddNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        AddNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 3;
                        break;
                    }
                //-------------------------------------------
                case 14:
                    {
                        UCAllocationMembers1.RequestID = RequestID;
                        UCAllocationMembers1.RequestDataID = RequestDataID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                case 42:
                    {
                        UCAllocationMembers1.RequestID = RequestID;
                        UCAllocationMembers1.RequestDataID = RequestDataID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                case 69:
                    {
                        UCAllocationMembers1.RequestID = RequestID;
                        UCAllocationMembers1.RequestDataID = RequestDataID;
                        UCAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 5;
                        break;
                    }
                //-------------------------------------------
                case 15:
                    {
                        UCPreviewVote1.RequestID = RequestID;
                        UCPreviewVote1.RequestDataID = RequestDataID;
                        // پاک کردن مقدار کنترل ها
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                case 43:
                    {
                        UCPreviewVote1.RequestID = RequestID;
                        UCPreviewVote1.RequestDataID = RequestDataID;
                        // پاک کردن مقدار کنترل ها
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                case 70:
                    {
                        UCPreviewVote1.RequestID = RequestID;
                        UCPreviewVote1.RequestDataID = RequestDataID;
                        // پاک کردن مقدار کنترل ها
                        UCPreviewVote1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 6;
                        break;
                    }
                //-------------------------------------------
                case 16:
                    {
                        UCViolationFileVote1.RequestID = RequestID;
                        UCViolationFileVote1.RequestDataID = RequestDataID;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                case 44:
                    {
                        UCViolationFileVote1.RequestID = RequestID;
                        UCViolationFileVote1.RequestDataID = RequestDataID;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                case 71:
                    {
                        UCViolationFileVote1.RequestID = RequestID;
                        UCViolationFileVote1.RequestDataID = RequestDataID;
                        UCViolationFileVote1.LoadData();
                        MultiView1.ActiveViewIndex = 7;
                        break;
                    }
                //-------------------------------------------
                case 28:
                    {
                        UCFileSendingRequest1.RequestID = RequestID;
                        UCFileSendingRequest1.RequestDataID = RequestDataID;
                        UCFileSendingRequest1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                case 55:
                    {
                        UCFileSendingRequest1.RequestID = RequestID;
                        UCFileSendingRequest1.RequestDataID = RequestDataID;
                        UCFileSendingRequest1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                case 82:
                    {
                        UCFileSendingRequest1.RequestID = RequestID;
                        UCFileSendingRequest1.RequestDataID = RequestDataID;
                        UCFileSendingRequest1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCFileSendingRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 8;
                        break;
                    }
                //----------------------------------------------------
                case 85:
                    {
                        UCFurtherInvestigationOrder1.RequestID = RequestID;
                        UCFurtherInvestigationOrder1.RequestDataID = RequestDataID;
                        UCFurtherInvestigationOrder1.LoadData();
                        MultiView1.ActiveViewIndex = 9;
                        break;
                    }
                //----------------------------------------------------
                case 17:
                    {
                        UCVoteNotification1.RequestID = RequestID;
                        UCVoteNotification1.RequestDataID = RequestDataID;
                        UCVoteNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                case 45:
                    {
                        UCVoteNotification1.RequestID = RequestID;
                        UCVoteNotification1.RequestDataID = RequestDataID;
                        UCVoteNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                case 72:
                    {
                        UCVoteNotification1.RequestID = RequestID;
                        UCVoteNotification1.RequestDataID = RequestDataID;
                        UCVoteNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCVoteNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 10;
                        break;
                    }
                //----------------------------------------------------
                case 104:
                    {
                        UCChangeFileBoard1.RequestID = strArgument[0].StringToInt();
                        UCChangeFileBoard1.RequestDataID = strArgument[1].StringToLong();
                        UCChangeFileBoard1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCChangeFileBoard1.LoadData();
                        MultiView1.ActiveViewIndex = 11;
                        break;
                    }
                case 102:
                    {
                        UCEjectionNotification1.RequestID = strArgument[0].StringToInt();
                        UCEjectionNotification1.RequestDataID = strArgument[1].StringToLong();
                        UCEjectionNotification1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCEjectionNotification1.LoadData();
                        MultiView1.ActiveViewIndex = 12;
                        break;
                    }
                //----------------------------------------------------
                case 127:
                    {
                        UCCloseAllocationMembers1.RequestID = RequestID;
                        UCCloseAllocationMembers1.RequestDataID = RequestDataID;
                        UCCloseAllocationMembers1.LoadData();
                        MultiView1.ActiveViewIndex = 13;
                        break;
                    }
                //----------------------------------------------------
                case 128:
                    {
                        UCPreviewClose1.RequestID = RequestID;
                        UCPreviewClose1.RequestDataID = RequestDataID;
                        // پاک کردن مقدار کنترل ها
                        UCPreviewClose1.LoadData(DefaultDate);
                        MultiView1.ActiveViewIndex = 14;
                        break;
                    }
                //---------------------------------------------------
                case 86:
                    {
                        UCViolationFileClosing1.RequestID = RequestID;
                        UCViolationFileClosing1.RequestDataID = RequestDataID;
                        UCViolationFileClosing1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCViolationFileClosing1.LoadData();
                        MultiView1.ActiveViewIndex = 15;
                        break;
                    }
                //---------------------------------------------------
                default:
                    {
                        UCOtherRequest1.RequestID = strArgument[0].StringToInt();
                        UCOtherRequest1.RequestDataID = strArgument[1].StringToLong();
                        UCOtherRequest1.RelatedAttachementTypes = strRelatedAttachementTypes;
                        UCOtherRequest1.LoadData();
                        MultiView1.ActiveViewIndex = 4;
                        break;
                    }
            }
            pnlRequestsList.Hide();
        }

        /// <summary>
        /// آیا اتهام ابلاغ شده است ؟
        /// </summary>
        /// <param name="requestDataID"></param>
        /// <returns></returns>
        private bool CheckAccusationHasBeedNotified(long requestDataID)
        {
            var obj = new DTO.Board.tblAccusation.tblAccusation
            {
                RequestDataID = requestDataID
            };
            return (new DAL.Board.tblAccusation.Methods()).CheckDeletePernission(obj);
        }

        /// <summary>
        /// افزودن شناسه فایل به لیست فایلهایی که برای این کاربر باز هستند و قرار دادن لیست داخل سشن
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUnlock_Click(object sender, EventArgs e)
        {
            btnUnlock.Visible = false;
            List<long> lstFileIDs = null;
            if (Session["UnlockedFileList"] == null)
            {
                lstFileIDs = new List<long>();
            }
            else
            {
                lstFileIDs = Session["UnlockedFileList"] as List<long>;
            }
            lstFileIDs.Add(this.FileID);

            (new Logger()).InsertFileUnlockHistoryLog(this.FileID);

            Session["UnlockedFileList"] = lstFileIDs;
            SankaDialog1.ShowMessage("قابلیت ویرایش فایل برای شخص شما فعال گردد . <br/> توجه : این قابلیت با خروج شما از سیستم غیر فعال میگردد .", DialogMessage.SankaDialog.Message_Type.Warning, Page, true);
        }
    }
}