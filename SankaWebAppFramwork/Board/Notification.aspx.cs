﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class Notification : GeneralPage
    {
        /// <summary>
        /// شناسه ابلاغیه
        /// </summary>
        private long NotificationID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("NotifyID")))
                {
                    return Request.QueryString.Get("NotifyID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long BoardID
        {
            get
            {
                return UserBoardID;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long FileID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("FileID")))
                {
                    return Request.QueryString.Get("FileID").StringToLong();
                }

                return 0;
            }
        }

        /// <summary>
        /// آیا کاربر عضو  هیات هست یا نه
        /// </summary>
        private bool IsCurrentUserBoardMember
        {
            get
            {
                if (ViewState["IsBoardMember"] != null)
                {
                    return ViewState["IsBoardMember"].ToString().StringToBool();
                }

                return false;
            }
            set
            {
                ViewState.Add("IsBoardMember", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                IsCurrentUserBoardMember = CheckUserToAccessToBoard(BoardID, "~/Board/UserBoardsList.aspx?action=" + (byte)SankaWebAppFramework.Enums.Page_ActionCode.View);

                #endregion چک کردن اینکه کاربر جاری، میتواند هیات جاری را مشاهده کند یا خیر

                #region مقدار دهی یوزرکنترل ها
                // همیشه از این صفحه در حال غیرقابل ویرایش، اتهام نمایش داده شود
                //AddNotification1.IsCurrentUserBoardMember = false;
                //AddNotification1.BoardID = BoardID;
                //AddNotification1.NotificationID = NotificationID;
                
                //AddNotification1.LoadData();
                #endregion مقدار دهی یوزرکنترل ها

                
            }
        }

    }
}