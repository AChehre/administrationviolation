﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Board.Master" AutoEventWireup="true"
    CodeBehind="ViolationFiles.aspx.cs" Inherits="SankaWebAppFramework.Board.ViolationFiles" %>

<%@ Register Assembly="Utility" Namespace="SankaGridView" TagPrefix="cc1" %>
<%@ Register Src="~/controls/UCActionMenu.ascx" TagName="UCActionMenu" TagPrefix="uc1" %>
<%@ Register Assembly="SankaWebAppFramework" Namespace="DialogMessage" TagPrefix="cc2" %>
<%@ Register TagPrefix="sanka" TagName="Date" Src="~/controls/UCDatePicker.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/SankaGrid.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------->
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css"
        media="all" />
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css"
        media="all" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
    <!-------------------------------------------------------------------------------------->
    <link href="/css/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc2:SankaDialog ID="SankaDialog1" runat="server" MessageLocationID="divMessage"
        ClientIDMode="Static">
    </cc2:SankaDialog>
    <uc1:UCActionMenu ID="UCActionMenu1" runat="server" />
    <div class="panel-group" id="accordion">
        <div class="panel panel-primary" id="pnlRequestsList" runat="server">
            <div data-toggle="collapse" data-target="#collapse1" class="panel-heading pointer-cursor">
                <h4 runat="server" id="hTitle" class="panel-title">
                    <img src="/images/icon/Search.png" />
                    جستجوی پرونده</h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body prevententer">
                    <div class="_10bm">
                        <label class="form-field-label">
                            نام :</label>
                        <asp:TextBox ID="txtFName" placeholder="نام" data-enter-target="btnFilter" MaxLength="30"
                            runat="server"></asp:TextBox>
                        <label class="form-field-label _50rmargin">
                            نام خانوادگی :</label>
                        <asp:TextBox ID="txtLName" placeholder="نام خانوادگی" data-enter-target="btnFilter"
                            MaxLength="30" runat="server"></asp:TextBox>
                    </div>
                    <div class="_10bm">
                        <label class="form-field-label">
                            کد پرسنلی :</label>
                        <asp:TextBox ID="txtPersonnelCode" CssClass="MustBeNumber" data-enter-target="btnFilter"
                            placeholder="کد پرسنلی" MaxLength="15" runat="server"></asp:TextBox>
                        <label class="form-field-label _50rmargin">
                            وضعیت :</label>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="_10bm">
                        <label class="form-field-label">
                            نتیجه نهایی :</label>
                        <asp:DropDownList ID="ddlFinalResult" runat="server">
                        </asp:DropDownList>
                        <label class="form-field-label _50rmargin">
                            کلاسه پرونده :</label>
                        <asp:TextBox ID="txtSerialCode" MaxLength="20" placeholder="کلاسه پرونده"
                            runat="server" />
                    </div>
                    <div class="_10bm">
                        <label class="form-field-label">
                            از تاریخ :</label>
                        <sanka:Date ID="txtFromDate" DontValidate="true" runat="server">
                        </sanka:Date>
                        <label class="form-field-label _50rmargin">
                            تا تاریخ :</label>
                        <sanka:Date ID="txtToDate"  DontValidate="true" runat="server">
                        </sanka:Date>
                    </div>
                    <div class="_10bm" runat="server" id="divBoardID">
                        <label class="form-field-label">
                            نام هیات :</label>
                        <asp:DropDownList ID="ddlBoard" Width="250px"  runat="server" />
                    </div>
                    <div class="_10bm">
                        <asp:Button ID="btnFilter" CssClass="btn btn-info" OnClick="btnFilter_Click" runat="server"
                            Text="فیلتر" />
                        <asp:Button ID="btnShowAll" CssClass="btn btn-info" OnClick="btnShowAll_Click" runat="server"
                            Text="نمایش همه" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grid">
        <cc1:SankaGrid ID="gvViolationFiles" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvViolationFiles_RowDataBound">
            <Columns>
                <%--                <asp:TemplateField HeaderText="کد ملی">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="NationalCode" data-sankatooltip="مرتب سازی براساس کد ملی" ToolTip="مرتب سازی براساس کد ملی"
                            CommandName="ASC" ID="lnkbtnNationalCode" OnCommand="lnkbtnSort_Command" runat="server">کد ملی</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgNationalCode"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("NationalCode").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نام پدر">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="FatherName" data-sankatooltip="مرتب سازی براساس نام پدر" ToolTip="مرتب سازی براساس نام پدر"
                            CommandName="ASC" ID="lnkbtnFatherName" OnCommand="lnkbtnSort_Command" runat="server">نام پدر</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgFatherName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("FatherName").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField DataField="RowNumber" HeaderText="ردیف" />
                <asp:BoundField DataField="FileID" HeaderText="FileID" Visible="False" />
                <asp:TemplateField HeaderText="نام">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="FirstName" data-sankatooltip="مرتب سازی براساس نام"
                            ToolTip="مرتب سازی براساس نام" CommandName="ASC" ID="lnkbtnFirstName" OnCommand="lnkbtnSort_Command"
                            runat="server">نام</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgFirstName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("FirstName").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نام خانوادگی">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="LastName" data-sankatooltip="مرتب سازی براساس نام خانوادگی"
                            ToolTip="مرتب سازی براساس نام خانوادگی" CommandName="ASC" ID="lnkbtnLastName"
                            OnCommand="lnkbtnSort_Command" runat="server">نام خانوادگی</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgLastName"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("LastName").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="کد پرسنلی">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="PersonnelCode" data-sankatooltip="مرتب سازی براساس کد پرسنلی"
                            ToolTip="مرتب سازی براساس کد پرسنلی" CommandName="ASC" ID="lnkbtnPersonnelCode"
                            OnCommand="lnkbtnSort_Command" runat="server">کد پرسنلی</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgPersonnelCode"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("PersonnelCode").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="وضعیت" SortExpression="Status">
                    <ItemTemplate>
                        <%# GetFileStatus(Eval("Status")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="کلاسه پرونده">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="SerialCode" data-sankatooltip="مرتب سازی براساس کلاسه پرونده"
                            ToolTip="مرتب سازی براساس کلاسه پرونده" CommandName="ASC" ID="lnkbtnSerialCode"
                            OnCommand="lnkbtnSort_Command" runat="server">کلاسه پرونده</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgSerialCode"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("SerialCode").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="تاریخ ایجاد">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="CreationDate" data-sankatooltip="مرتب سازی براساس تاریخ"
                            ToolTip="مرتب سازی براساس تاریخ" CommandName="ASC" ID="lnkbtnCreationDate" OnCommand="lnkbtnSort_Command"
                            runat="server">تاریخ تشکیل پرونده</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgCreationDate"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Utility.GetCustomDateWithoutTime(Eval("CreationDate")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نتیجه نهایی">
                    <ItemTemplate>
                        <%# getFinalResult(Eval("FinalResult"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="نام هیات">
                    <HeaderTemplate>
                        <asp:LinkButton CommandArgument="BoardTitle" data-sankatooltip="مرتب سازی براساس نام هیات"
                            ToolTip="مرتب سازی براساس نام هیات" CommandName="ASC" ID="lnkbtnBoardTitle" OnCommand="lnkbtnSort_Command"
                            runat="server">نام هیات</asp:LinkButton>
                        <asp:Image Width="10" Height="10" ImageUrl="/images/sort_up.png" ID="imgBoardTitle"
                            runat="server" Visible="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("BoardTitle") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="دفعات ارجاع" Visible="False">
                    <ItemTemplate>
                        <%# Eval("ReferHistoryCounter") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="جزییات">
                    <ItemTemplate>
                        <asp:HyperLink data-save-return-url="true" ID="lnkDetails" runat="server" Text="مشاهده"
                            NavigateUrl='<%# string.Format("ViolationFileDependencies.aspx?FileID={0}&BoardID={1}",Eval("FileID"),Eval("BoardID")) %>' />
                        <asp:HiddenField runat="server" ID="hfHintTag" Value='<%# Eval("HintTag") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                رکوردی موجود نیست
            </EmptyDataTemplate>
            <PagerStyle BackColor="#CCCCCC" />
        </cc1:SankaGrid>
    </div>
    <asp:HiddenField ID="hfSortColumn" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSortOrder" runat="server" Value="" ClientIDMode="Static" />
</asp:Content>
