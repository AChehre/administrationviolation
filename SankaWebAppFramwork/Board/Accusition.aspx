﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Board/Solid.Master" AutoEventWireup="true"
    CodeBehind="Accusition.aspx.cs" Inherits="SankaWebAppFramework.Board.Accusition" %>

<%@ Register Src="~/controls/UCAddAccusition.ascx" TagName="UCAddAccusition" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/SankaGrid.css" rel="stylesheet" type="text/css" />
    <link href="/calendar/css/persianDatepicker-dark.css" rel="stylesheet" type="text/css" />
    <link href="/calendar/css/persianDatepicker-default.css" rel="stylesheet" type="text/css" />
    <script src="/calendar/js/persianDatepicker.js" type="text/javascript"></script>
    <script src="/calendar/js/persianDatepicker.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <uc3:UCAddAccusition ID="AddAccusition1" runat="server" />

</asp:Content>
