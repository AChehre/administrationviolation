﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class SetCurrentApplicationYear : GeneralPage
    {
        /// <summary>
        /// آیدی هیات
        /// </summary>
        protected long BoardID
        {
            get
            {
                return UserBoardID;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SankaDialog1.ShowMessage(string.Format("سال عملیاتی تنظیم شده {0}  میباشد . در صورت نیاز آن را تغییر دهید .", this.UserCurrentApplicationYear), DialogMessage.SankaDialog.Message_Type.Warning, Page, false);
                if (UserCurrentApplicationYear.HasValue)
                {
                    ddlYear.SelectedValue = UserCurrentApplicationYear.Value.ToString();
                }
                ddlYear_SelectedIndexChanged(sender, e);
            }
            BindActionMenu();
        }

        /// <summary>
        /// بارگذاری مقادیر فعلس شمارنده های نامه
        /// </summary>
        private void LoadSerialCounters()
        {
            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting
            {
                BoardID = this.UserBoardID,
                Year = ddlYear.SelectedValue.StringToInt(),

            };
            obj = (new DAL.Board.tblCounterSetting.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                txtFileSerialCounter.Text = obj.FileSerialCounter.ToString();
                txtViolationNotifivationCounter.Text = obj.ViolationNotificationCounter.ToString();
                txtVoteNotificationNumber.Text = obj.VoteNotificationCounter.ToString();
                txtVoteSerialCounter.Text = obj.VoteCounter.ToString();
                txtClosingCounter.Text = obj.ClosingCounter.ToString();
            }
            else
            {
                txtFileSerialCounter.Text = "1";
                txtViolationNotifivationCounter.Text = "1";
                txtVoteNotificationNumber.Text = "1";
                txtVoteSerialCounter.Text = "1";
                txtClosingCounter.Text = "1";
            }
        }

        /// <summary>
        /// ذخیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveClick(object sender, EventArgs e)
        {
            try
            {
                UserCurrentApplicationYear = ddlYear.SelectedValue.StringToInt();
                SaveSerialCounter();
                ////SankaDialog1.ShowMessage("سال عملیاتی با موفقیت ذخیره شد.", DialogMessage.SankaDialog.Message_Type.Message, "/board/ViolationFileDependencies.aspx");
            }
            catch
            {
                SankaDialog1.ShowMessage("خطا در ثبت سال عملیاتی", DialogMessage.SankaDialog.Message_Type.Error, Page);
            }
        }

        /// <summary>
        /// ذخیره مقدار جدید شمارنده های نامه
        /// </summary>
        private void SaveSerialCounter()
        {
            List<string> lstMessage = new List<string>();

            if (txtFileSerialCounter.Text.StringToLong() == 0)
            {
                lstMessage.Add("شمارنده پرونده وارد شده نامعتبر است .");
            }
            if (txtVoteSerialCounter.Text.StringToLong() == 0)
            {
                lstMessage.Add("شمارنده رای وارد شده نامعتبر است .");
            }
            if (txtVoteNotificationNumber.Text.StringToLong() == 0)
            {
                lstMessage.Add("شمارنده ابلاغیه رای وارد شده نامعتبر است .");
            }
            if (txtViolationNotifivationCounter.Text.StringToLong() == 0)
            {
                lstMessage.Add("شمارنده ابلاغیه  اتهام وارد شده نامعتبر است .");
            }
            if (CheckViolationFileCounterConfilict())
            {
                lstMessage.Add("پرونده ای با این شماره قبلا در سیستم ثبت گردید است یا اینکه شماره پرونده ای بزرگتر ار این شماره قبلا ثبت گردیده است .");
            }
            if (CheckViolationNotificationCounterConfilict())
            {
                lstMessage.Add("ابلاغیه ای با این شماره قبلا در سیستم ثبت گردید است یا اینکه شماره ابلاغیه ای بزرگتر از این شماره قبلا ثبت گردیده است .");
            }
            if (CheckVoteNotificationCounterConfilict())
            {
                lstMessage.Add("ابلاغیه رای با این شماره نامه قبلا در سیستم ثبت گردید است یا اینکه شماره نامه ای بزرگتر ار این شماره قبلا ثبت گردیده است .");
            }
            if (CheckVoteCounterConfilict())
            {
                lstMessage.Add("این شماره رای قبلا در سیستم استفاده شده است یا اینکه شماره رای بزرگتر ار این شماره قبلا ثبت گردیده است .");
            }
            if (CheckClosingCounterConfilict())
            {
                lstMessage.Add("این شماره صورت جلسه مختومه قبلا در سیستم استفاده شده است یا اینکه شماره صورت جلسه ای بزرگتر ار این شماره قبلا ثبت گردیده است .");
            }
            if (lstMessage.Any())
            {
                SankaDialog1.ShowMessage(GetWarrningList(lstMessage), DialogMessage.SankaDialog.Message_Type.Warning, Page, false);
                return;
            }
            var obj = new DTO.Board.tblCounterSetting.tblCounterSetting
            {
                FileSerialCounter = txtFileSerialCounter.Text.StringToLong(),
                VoteCounter = txtVoteSerialCounter.Text.StringToLong(),
                VoteNotificationCounter = txtVoteNotificationNumber.Text.StringToLong(),
                ViolationNotificationCounter = txtViolationNotifivationCounter.Text.StringToLong(),
                ClosingCounter = txtClosingCounter.Text.StringToLong(),
                BoardID = this.UserBoardID,
                Year = ddlYear.SelectedValue.StringToInt()
            };
            bool bln = (new DAL.Board.tblCounterSetting.Methods()).Insert(obj);
            if (bln)
            {
                LoadSerialCounters();
                SankaDialog1.ShowMessage("تغییرات ذخیره گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page, false);
            }
        }

        /// <summary>
        /// چک کردن تداخل شمارنده صورت جلسه مختومه
        /// </summary>
        /// <returns></returns>
        private bool CheckClosingCounterConfilict()
        {
            var obj = new DTO.Board.tblViolationFileClosing.tblViolationFileClosing()
            {
                ClosingNumber = txtClosingCounter.Text.StringToLong(),
                ActivityYear = ddlYear.SelectedValue.StringToInt(),
                BoardID = this.UserBoardID
            };
            return (new DAL.Board.tblViolationFileClosing.Methods()).CheckExistFileSerialCode(obj);
        }

        /// <summary>
        /// نمایش سال های شمسی از سال 50 تا سال جاری
        /// </summary>
        void BindYear()
        {
            int YearTo = DateTime.Now.GetPersianYear();

            int YearFrom = 1350;

            for (int i = YearTo; i >= YearFrom; i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        /// <summary>
        /// باید کردن منو و دکمه ها
        /// </summary>
        private void BindActionMenu()
        {
            List<ActionMenuModel> lstAction = new List<ActionMenuModel>();

            //  دکمه ذخیره
            lstAction.Add(new ActionMenuModel()
            {
                TargetID = "btnSave",
                Title = "ذخیره",
                button_click = btnSaveClick,
                Type=ActionMenuType.ImageButton,
                Icon = "/images/icon/save.png"
            });

            UCActionMenu1.DataSource = lstAction;
           UCActionMenu1.CreateMenu();
        }

        /// <summary>
        /// ساخت سریال نامه
        /// </summary>
        /// <param name="counter">عدد شمارنده</param>
        /// <returns>سریال تولید شده</returns>
        private string getLetterSerial(string counter)
        {
            string ShortYear = UserCurrentApplicationYear.ToString().Substring(2, 2);
            switch (counter.Length)
            {
                case 1:
                    {
                        return "00000" + counter;
                    }
                case 2:
                    {
                        return "0000" + counter;
                    }
                case 3:
                    {
                        return "000" + counter;
                    }
                case 4:
                    {
                        return "00" + counter;
                    }
                case 5:
                    {
                        return "0" + counter;
                    }
            }

            return ShortYear + "/" + getBoardCode() + "/" + counter;
        }

        /// <summary>
        /// استخراج مد هیات
        /// </summary>
        /// <returns></returns>
        private string getBoardCode()
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard()
            {
                BoardID = this.UserBoardID
            };
            return (new DAL.Board.tblBoard.Methods()).SelectByID(obj).BoardCode;
        }

        /// <summary>
        /// چک کردن تداخل شمارنده سریال پرونده
        /// </summary>
        /// <returns></returns>
        private bool CheckViolationFileCounterConfilict()
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                SerialCounter = txtFileSerialCounter.Text.StringToLong(),
                ActivityYear = ddlYear.SelectedValue.StringToInt(),
                BoardID = this.UserBoardID
            };
            return (new DAL.Board.tblViolationFile.Methods()).CheckExistFileSerialCode(obj);
        }

        /// <summary>
        /// چک کردن تداخل شمارنده ابلاغیه
        /// </summary>
        /// <returns></returns>
        private bool CheckViolationNotificationCounterConfilict()
        {
            var obj = new DTO.Board.tblViolationNotification.tblViolationNotification
            {
                SerialCounter = txtViolationNotifivationCounter.Text.StringToLong(),
                BoardID = this.UserBoardID,
                ActivityYear = ddlYear.SelectedValue.StringToInt()
            };
            return (new DAL.Board.tblViolationNotification.Methods()).CheckExistLetterSerialCode(obj);
        }

        /// <summary>
        /// چک کردن تداخل شمارنده رای صادره
        /// </summary>
        /// <returns></returns>
        private bool CheckVoteCounterConfilict()
        {
            var obj = new DTO.Board.tblViolationFileVoteView.tblViolationFileVoteView
            {
                VoteNumber = txtVoteSerialCounter.Text.StringToLong(),
                BoardID = this.UserBoardID,
                ActivityYear = ddlYear.SelectedValue.StringToInt()
            };
            return (new DAL.Board.tblViolationFileVoteView.Methods()).CheckVoteNumberExists(obj);
        }

        /// <summary>
        /// چک کردن تداخل شمارنده ابلاغیه رای صادره
        /// </summary>
        /// <returns></returns>
        private bool CheckVoteNotificationCounterConfilict()
        {
            var obj = new DTO.Board.tblVoteNotification.tblVoteNotification
            {
                SerialCounter = txtVoteNotificationNumber.Text.StringToLong(),
                BoardID = this.UserBoardID,
                ActivityYear = ddlYear.SelectedValue.StringToInt()
            };
            return (new DAL.Board.tblVoteNotification.Methods()).CheckCounterConfilict(obj);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSerialCounters();
        }
    }
}