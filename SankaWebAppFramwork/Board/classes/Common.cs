﻿
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System;

namespace SankaWebAppFramework.Board
{
    public class Common
    {
        /// <summary>
        /// بایند کردن کمبو نوع فایل پیوستی
        /// </summary>
        /// <param name="ddl">کمبوباکسی که میخواهیم لیست را نمایش دهد</param>
        public static void BindAttachmentFileType(DropDownList ddl ,string  strAllowedFileTypes)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.strAttachmentFileType.Length; i++)
            {
                if (strAllowedFileTypes.Length > 0)
                {
                    List<string> lstFileTypes = getIDs(strAllowedFileTypes);
                    if (!lstFileTypes.Contains(i.ToString()))
                    {
                        continue;
                    }
                }
                ListItem item = new ListItem(Enums.strAttachmentFileType[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
            ddl.DataBind();
        }

        /// <summary>
        /// بایند کردن کمبوباکس نوع ابلاغیه
        /// </summary>
        /// <param name="ddl">کمبوباکس موردنظر</param>
        public static void BindNotificationType(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.strNotificationType.Length; i++)
            {
                ListItem item = new ListItem(Enums.strNotificationType[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بایند کردن کمبوباکس لایحه دفاعیه
        /// </summary>
        /// <param name="ddl">کمبوباکس موردنظر</param>
        public static void BindReplyBill(DropDownList ddl)
        {
            for (int i = 0; i < Enums.ReplyBillLabels.Length; i++)
            {
                ListItem item = new ListItem(Enums.ReplyBillLabels[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بایند کردن کمبو نوع رای
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindVoteType(DropDownList ddl)
        {
            for (int i = 0; i < Enums.VoteTypeLabels.Length; i++)
            {
                ListItem item = new ListItem(Enums.VoteTypeLabels[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بایند کردن مجازات های ماده 9 قانون
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindPenaltyType(DropDownList ddl)
        {
            for (int i = 0; i < Enums.PenaltyTypeLabels.Length; i++)
            {
                ListItem item = new ListItem(Enums.PenaltyTypeLabels[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بدست آوردن حجم یک فایل
        /// </summary>
        /// <param name="url">آدرس فایل</param>
        /// <returns> اندازه فایل به اضافه کیوبایت</returns>
        public static string GetFileSize(string url)
        {
            try
            {
                return (new System.IO.FileInfo(HttpContext.Current.Server.MapPath(url)).Length / 1024) + " کیلو بایت ";
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// بایند کردن کمبوباکس مربوط به وضعیت حکم نهایی یک پرونده
        /// </summary>
        public static void BindViolationFileFinalResult(DropDownList ddl)
        {
            for (int i = 0; i < Enums.strFinalStatus.Length; i++)
            {
                ListItem item = new ListItem(Enums.strFinalStatus[i], i.ToString(), true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بایند کردن کمبوباکس محل ابلاغ
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindNotificationLocations(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.NotificationLocationLabels.Length; i++)
            {
                ListItem item = new ListItem(Enums.NotificationLocationLabels[i], Enums.NotificationLocationLabels[i], true);
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// بایند کردن کمبوباکس مربوط به وضعیت یک پرونده
        /// </summary>
        /// <param name="ddl">کموباکسی که میخواهیم مقادیر را در آن نمایش دهیم</param>
        public static void BindViolationFileStatus(DropDownList ddl)
        {
            ListItem item = new ListItem("درج اتهام","0");
            ddl.Items.Add(item);
            item = new ListItem("ابلاغ اتهام", "100");
            ddl.Items.Add(item);
            item = new ListItem("تکمیل مستندات", "200");
            ddl.Items.Add(item);
            item = new ListItem("صدور رای", "300");
            ddl.Items.Add(item);
            item = new ListItem("رسیدگی به اعتراض", "400");
            ddl.Items.Add(item);
        }

        /// <summary>
        /// برگرداندن آرایه ای از شناسه ها با مقسم کاما
        /// </summary>
        /// <param name="strIDs">رشته شامب شناسه های با کاما جدا شده</param>
        /// <returns></returns>
        public static List<string> getIDs(string strIDs)
        {
            if (strIDs == null) return null;
            return strIDs.Split(',').ToList();
        }

        /// <summary>
        /// ثبت یک رکورد درخواست در جدول درخواست ها
        /// </summary>
        /// <param name="userID">شناسه کاربر </param>
        /// <param name="requestID">شناسه درخواست</param>
        /// <param name="fileID">شناسه پرونده</param>
        /// <param name="note">توضیحات</param>
        /// <param name="updateFileStatus">وضعیت پرونده را تغییر بده یا نه</param>
        /// <param name="requestDataID">شناسه درخواست ثبت شده</param>
        /// <returns>شناسه درخواست ثبت شده</returns>
        public static long InsertRequestDataRecord(long userID , int requestID , long fileID , string note , bool updateFileStatus = true , long requestDataID = 0)
        {
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData
            {
                RequestDataID = requestDataID,
                UpdateFileStatus = updateFileStatus,
                FileID = fileID,
                RequestID = requestID,
                Note = note,
                UserID = userID
            };
            long result = (new DAL.Board.tblRequestData.Methods()).Insert(obj);
            if (result > 0)
            {
                // لاگ ثبت درخواست
                (new Logger()).InsertRequestDataModificationLog(result, Logger.RecardModificationFlag.INSERT);
            }
            return result;
        }

        /// <summary>
        /// وضعیت فلگ درخواست را تغییر میدهد
        /// درخواست ممکن است حذف شود یا مستندات جهت بستن آن درج گردد
        /// پس از هرکدام از این تغییرات این متد اجرا و بصورت خودکار وضعیت را تشخیص و فلگ را مقدار دهی یکند .
        /// </summary>
        /// <param name="requestDataID">شناسه رکورد درخواست</param>
        /// <returns>نتیجه اجرای متد</returns>
        public static bool UpdateRequestDataStatus(long requestDataID)
        {
            var obj = new DTO.Board.tblRequestData.tblRequestData 
            {
                RequestDataID = requestDataID
            };
            bool bln = (new DAL.Board.tblRequestData.Methods()).UpdateStatus(obj);
            if (bln)
            {
                // لاگ تغییر وضعیت درخواست
                (new Logger()).InsertRequestDataModificationLog(requestDataID, Logger.RecardModificationFlag.UPDATE);
            }
            return bln;
        }

        /// <summary>
        /// آیا این درخواست پیش نیاز درخواست دیگری است 
        /// یا وابستگی منطقی به درخواست دیگری دارد .
        /// </summary>
        /// <param name="requestDataID">شناسه درخواست</param>
        /// <param name="fileID">شناسه پرونده</param>
        /// <returns>عددی شامل کد نوع وابستگی جهت نمایش پیغام مناسب به کاربر</returns>
        public static int CheckHasDependedRequestData(long requestDataID , long fileID)
        {
            DTO.Board.tblRequestData.tblRequestData obj = new DTO.Board.tblRequestData.tblRequestData()
            {
                RequestDataID = requestDataID,
                FileID = fileID
            };
            return(new DAL.Board.tblRequestData.Methods()).CheckHasDependedRequestData(obj);
        }

        /// <summary>
        /// برگرداندن متن متناظر با وضعیت فعلی پرونده
        /// </summary>
        /// <param name="fileID">شناسه پرونده</param>
        /// <returns>متن وضعیت پرونده</returns>
        public static string getFileStatusLabel(long fileID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile
            {
                FileID = fileID
            };
            int index = (new DAL.Board.tblViolationFile.Methods()).GetFileStatus(obj);
            return Common.getFileStatusLabel((object)index);
        }

        /// <summary>
        /// برگرداندن متن متناظر با وضعیت فعلی پرونده
        /// </summary>
        /// <param name="statusIndex">اندیس وضعیت پرونده</param>
        /// <returns>متن وضعیت</returns>
        public static string getFileStatusLabel(object statusIndex)
        {
            if (statusIndex == null)
            {
                return "تشکیل پرونده - فاقد اتهام";
            }
            else
            {
                int status = statusIndex.ObjectToInt();
                if (status == 0)
                {
                    return "درج اتهام";
                }
                if (status == 100 || status < 200)
                {
                    return "ابلاغ اتهام";
                }
                if (status == 200 || status < 300)
                {
                    return "تکمیل مستندات";
                }
                if (status == 300 || status < 400)
                {
                    return "صدور رای";
                }
                if (status == 400 || status < 500)
                {
                    return "رسیدگی به اعتراض";
                }
                if (status == 500)
                {
                    return "مختومه";
                }
            }
            return "نا مشخص";
        }

        /// <summary>
        /// پرکردن لیست هیات ها در کمبو باکس
        /// </summary>
        /// <param name="ddl">شی کمبو باکس</param>
        public static void BindBoards(DropDownList ddl)
        {
            ddl.DataSource = (new DAL.Board.tblBoard.Methods()).SelectAll();
            ddl.DataTextField = "BoardTitle";
            ddl.DataValueField = "BoardID";
            ddl.DataBind();
        }

        /// <summary>
        /// پرکردن لیست هیات ها براساس نوع هیات
        /// </summary>
        /// <param name="ddl">کمبوباکس هیات</param>
        /// <param name="boardType">نوع هیات</param>
        public static void BindBoards(DropDownList ddl , byte boardType)
        {
            var obj = new DTO.Board.tblBoard.tblBoard { BoardType = boardType };
            ddl.DataSource = (new DAL.Board.tblBoard.Methods()).SelectByBoardType(obj);
            ddl.DataTextField = "BoardTitle";
            ddl.DataValueField = "BoardID";
            ddl.DataBind();
        }

        /// <summary>
        /// پرکردن لیست پرونده های تخلف یک هیات
        /// </summary>
        /// <param name="ddl">کمبو باکس</param>
        /// <param name="boardID">شناسه هیات</param>
        /// <param name="currentBoardID">شناسه هیات جاری</param>
        public static void BindViolationfile(DropDownList ddl, long boardID , long currentBoardID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile { BoardID = boardID, CurrentBoardID = currentBoardID };
            ddl.DataSource = (new DAL.Board.tblViolationFile.Methods()).SelectAllByBoardID(obj);
            ddl.DataTextField = "SerialCode";
            ddl.DataValueField = "FileID";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب", "-1"));
        }

        /// <summary>
        /// پرکردن لیست پرونده های ماده 17
        /// </summary>
        /// <param name="ddl">شی کمبو باکس</param>
        /// <param name="boardID">شناسه هیات</param>
        public static void BindViolationfileForDaftar_M17(DropDownList ddl, long boardID)
        {
            DTO.Board.tblViolationFile.tblViolationFile obj = new DTO.Board.tblViolationFile.tblViolationFile { BoardID = boardID };
            ddl.DataSource = (new DAL.Board.tblViolationFile.Methods()).SelectAllByBoardIDForDaftar(obj);
            ddl.DataTextField = "SerialCode";
            ddl.DataValueField = "FileID";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("انتخاب", "-1"));
        }

        /// <summary>
        /// نمایش سال های شمسی از سال 50 تا سال جاری
        /// </summary>
        public static void BindYear(DropDownList ddl)
        {
            int YearTo = DateTime.Now.GetPersianYear();

            int YearFrom = 1350;

            for (int i = YearTo; i >= YearFrom; i--)
            {
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        /// <summary>
        /// پرکردن لیست نوع شخص گزارش دهنده تخلف
        /// </summary>
        /// <param name="ddl">شی کمبو باکس</param>
        public static void BindReportSource(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.strAccusationReportSource.Length; i++)
            {
                ListItem item = new ListItem(Enums.strAccusationReportSource[i], i.ToString());
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// پرکردن لیست محل وقوع تخلف
        /// </summary>
        /// <param name="ddl">شی کمبو باکس</param>
        public static void BindEventPlace(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.strAccusationEventPlace.Length; i++)
            {
                ListItem item = new ListItem(Enums.strAccusationEventPlace[i], i.ToString());
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// پرکردن لیست وضعیت احراز  یا عم احزار اتهام
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindAccusitionStatus(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 0; i < Enums.strAccusationStatusLabels.Length; i++)
            {
                ListItem item = new ListItem(Enums.strAccusationStatusLabels[i], i.ToString());
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// نمایش موضوع های مختلف ثبت گزارش تخلف
        /// </summary>
        public static void BindSubject(DropDownList dd)
        {
            dd.Items.Clear();
            dd.DataSource = (new DAL.Board.tblAccusationSubject.Methods()).SelectAll();
            dd.DataTextField = "Title";
            dd.DataValueField = "SubjectID";
            dd.DataBind();
        }

        #region برچسب های مورد نیاز برای فرم های چاپی 

        //  دریافت برچسب مدرک تحصیلی متهم براساس شناسه تحصیلات
        public static string getDegreeLabel(byte? degreeID)
        {
            if (!degreeID.HasValue) return string.Empty;
            var obj = new DTO.Board.tblDegree.tblDegree
            {
                DegreeID = degreeID.Value
            };
            return (new DAL.Board.tblDegree.Methods()).SelectByID(obj).DegreeTitle;
        }

        /// <summary>
        /// دریافت سال تولد متهم
        /// </summary>
        /// <param name="farsiBirthDate"></param>
        /// <returns></returns>
        public static string getBirthYear(int? farsiBirthDate)
        {
            if (!farsiBirthDate.HasValue) return string.Empty;
            return farsiBirthDate.ToString().Substring(0, 4);
        }

        /// <summary>
        /// گرفتن متن متناظر با شناسه پست سازمانی
        /// </summary>
        /// <param name="positionID"></param>
        /// <returns></returns>
        public static string getOrganizationPosition(int positionID)
        {
            var obj = new DTO.Board.tblOrganizationPosition.tblOrganizationPosition
            {
                OrganizationPositionID = positionID
            };
            obj = (new DAL.Board.tblOrganizationPosition.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.OrganizationPositionName;
            }
            return string.Empty;
        }

        /// <summary>
        /// برگرداندن گروه سازمانی متهم
        /// </summary>
        /// <param name="groupID">شناسه گروه</param>
        /// <returns>برچسب گروه سازمانی</returns>
        public static string getOtganizationGroupName(int groupID)
        {
            var obj = new DTO.Board.tblOrganizationGroup.tblOrganizationGroup
            {
                OrganizationGroupID = groupID
            };
            obj = (new DAL.Board.tblOrganizationGroup.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.OrganizationGroupname;
            }
            return string.Empty;
        }

        /// <summary>
        /// گرفتن متن متناظر با شناسه پست سازمانی
        /// </summary>
        /// <param name="positionID"></param>
        /// <returns></returns>
        public static string getEducationLabel(byte degreeID)
        {
            var obj = new DTO.Board.tblDegree.tblDegree
             {
                 DegreeID = degreeID
             };
            obj = (new DAL.Board.tblDegree.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.DegreeTitle;
            }
            return string.Empty;
        }

        /// <summary>
        ///برچسب نوع هیات بدوی / تجدید نظر
        /// </summary>
        /// <param name="ddl"></param>
        public static void BindSodourType(DropDownList ddl)
        {
            ddl.Items.Clear();
            for (int i = 1; i < Enums.strSodourTypeLabel.Length; i++)
            {
                ListItem item = new ListItem(Enums.strSodourTypeLabel[i], i.ToString());
                ddl.Items.Add(item);
            }
        }

        /// <summary>
        /// برگرداندن برچسب وضعیت استخدامی
        /// </summary>
        /// <param name="estekhdamStatusID">شناسه وضعیت استخدام</param>
        /// <returns>متن وضعیت استخدام</returns>
        internal static string getEstekhdamStatusLabel(byte estekhdamStatusID)
        {
            var obj = new DTO.Board.tblEstekhdamStatus.tblEstekhdamStatus
            {
                EstekhdamStatusID = estekhdamStatusID
            };
            obj = (new DAL.Board.tblEstekhdamStatus.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.Title;
            }
            return string.Empty;
        }

        /// <summary>
        /// بر گرداندن برچسب وضعیت مستخدم
        /// </summary>
        /// <param name="mostakhdemTypeID">شناسه وضعیت مستخدم</param>
        /// <returns>متن وضعیت مستخدم</returns>
        internal static string getMostakhdemTypeLabel(byte mostakhdemTypeID)
        {
            var obj = new DTO.Board.tblMostakhdemType.tblMostakhdemType
            {
                MostkhdemTypeID = mostakhdemTypeID
            };
            obj = (new DAL.Board.tblMostakhdemType.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                return obj.Title;
            }
            return string.Empty;
        }

        /// <summary>
        /// برگرداندن برچست عنوان جنسیت متهم
        /// </summary>
        /// <param name="gender">شناسه نوع جنسیت</param>
        /// <returns></returns>
        internal static string getAccusedGenderTitleLabel(byte gender)
        {
            return gender == 1 ? "متهم" : "متهمه";
        }


        /// <summary>
        /// برگرداندن برچست  خانم یا آقا  برای عنوان متهم بر اساس جنسیت متهم
        /// </summary>
        /// <param name="gender">شناسه نوع جنسیت</param>
        /// <returns></returns>
        internal static string getGenderTitleLabel(byte gender)
        {
            return gender == 1 ? "آقای" : "خانم";
        }

        /// <summary>
        /// برگرداندن برچست عنوان جنسیت محکوم
        /// </summary>
        /// <param name="gender">شناسه نوع جنسیت</param>
        /// <returns></returns>
        internal static string getConvictGenderTitleLabel(byte gender)
        {
            return gender == 1 ? "محکوم علیه" : "محکوم علیها";
        }

        #endregion  برچسب های مورد نیاز برای فرم های چاپی

        /// <summary>
        /// چک کردن اینکه این که قفل فایل برداشته شده است ؟
        /// شناسه فایل داخل لیست پرونده های فاقد قفل موجود اسا یا نه ؟
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        internal static bool IsUnlockedFile(long fileID)
        {
            List<long> lstFileIDs = null;
            lstFileIDs = HttpContext.Current.Session["UnlockedFileList"] as List<long>;
            if (lstFileIDs != null)
            {
                return lstFileIDs.Contains(fileID);
            }
            return false;
        }

        internal static void BindBoardType(DropDownList ddlBoardType)
        {
            for (int i = 0; i < AdministrationViolation.Admin.Enums.Board_Type_Label.Length; i++)
            {
                ddlBoardType.Items.Add(new ListItem(AdministrationViolation.Admin.Enums.Board_Type_Label[i],(i+1).ToString()));
            }
        }

        /// <summary>
        /// استخراج شناسه هیات کاربر جاری
        /// </summary>
        public static long getUserBoardID()
        {
            UserProfile profile = HttpContext.Current.Session["UserProfile"] as UserProfile;
            if (profile != null)
            {
                return profile.UserBoardID;
            }
            return 0;
        }
    }
}