﻿
namespace SankaWebAppFramework.Board
{
    public class Enums
    {
        /// <summary>
        /// اینام مربوط به وضعیت پرونده
        /// </summary>
        public enum FileStatus
        {
            /// <summary>
            /// درج اتهام
            /// </summary>
            DAREJE_ETEHAM = 0,

            /// <summary>
            /// ابلاغ اتهام
            /// </summary>
            EBLAGHE_ETEHAM = 100,

            /// <summary>
            /// تکمیل مستندات
            /// </summary>
            TAKMILE_MOSTANADAT = 200,

            /// <summary>
            /// تعیین اعضای صدور رای
            /// </summary>
            TAINE_AZAYEYE_SODOURE_RAY = 210,

            /// <summary>
            /// صدور رای
            /// </summary>
            SODOURE_RAY = 300,

            /// <summary>
            /// رسیدگی به اعتراض دیوان
            /// </summary>
            RESIDEGI_BE_ETERAZ = 400,

             /// <summary>
            /// مختومه
            /// </summary>
            CLOSE_FILE = 500

        };

        /// <summary>
        /// مقادیر لایحه دفاعیه
        /// </summary>
        public enum ReplyBill
        {
            /// <summary>
            /// ندارد
            /// </summary>
            NOT_REPLIED,
            /// <summary>
            ///  دارد-درخواست حضور در جلسه به تشخیص هیات
            /// </summary>
            REPLIED_REQUEST_ATTENDANCE_BY_BOARD,
            /// <summary>
            ///  دارد-درخواست حضور در جلسه توسط متهم
            /// </summary>
            REPLIED_REQUEST_ATTENDANCE_BY_ACCUSER
        }

        /// <summary>
        /// آرایه انواع لایحه دفائیه
        /// </summary>
        public static string[] ReplyBillLabels =
        {
           "ندارد", "دارد -درخواست حضور در جلسه به تشخیص هیات","دارد -درخواست حضور در جلسه توسط متهم"
        };

        /// <summary>
        /// لیست انواع نتایج یک پرونده
        /// </summary>
        public static string[] strFinalStatus =
        {
            "نامشخص","مختومه","برائت","محکومیت (قطعی)","محکومیت (قابل پژوهش)"
        };

        /// <summary>
        /// نوع رای پرونده
        /// </summary>
        public enum VoteType
        {
            /// <summary>
            /// برائت
            /// </summary>
            INNOCENCE = 0,

            /// <summary>
            /// محکومیت 
            /// </summary>
            CONDEMNATION = 1,
        }

        public static string[] VoteTypeLabels = { "برائت", "محکومیت" };

        /// <summary>
        /// مجازات های پرونده
        /// </summary>
        public enum PenaltyType
        {//بند الف-ب-ج-د-ه-و-ز-ح-ط-ی-ک
            /// <summary>
            /// بند الف
            /// </summary>
            CLAUSE_A,
            /// <summary>
            /// بند ب
            /// </summary>
            CLAUSE_B,
            /// <summary>
            /// بند ج
            /// </summary>
            CLAUSE_C,
            /// <summary>
            /// بند د
            /// </summary>
            CLAUSE_D,
            /// <summary>
            /// بند ه
            /// </summary>
            CLAUSE_F,
            /// <summary>
            /// بند و
            /// </summary>
            CLAUSE_G,
            /// <summary>
            /// بند ز
            /// </summary>
            CLAUSE_H,
            /// <summary>
            /// بند ح
            /// </summary>
            CLAUSE_I,
            /// <summary>
            /// بند ط
            /// </summary>
            CLAUSE_J,
            /// <summary>
            /// بند ی
            /// </summary>
            CLAUSE_K,
            /// <summary>
            /// بند ک
            /// </summary>
            CLAUSE_L,
            //CLAUSE_M
        }

        /// <summary>
        /// عناوین عناوین مجازات های ماده 9
        /// </summary>
        public static string[] PenaltyTypeLabels = { "بند الف ماده 9 ", "بند ب ماده 9 ", "بند ج ماده 9 "
                                                   , "بند د ماده 9 ", "بند ه ماده 9 ", "بند و ماده 9 "
                                                   , "بند ز ماده 9 ", "بند ح ماده 9 ", "بند ط ماده 9 "
                                                   , "بند ی ماده 9 ", "بند ک ماده 9 "};

        /// <summary>
        /// عناوین انواع ابلاغیه
        /// </summary>
        public static string[] strNotificationType = { "نامشخص", "واقعی", "قانونی" };

        /// <summary>
        /// انواع ضمائم پرونده
        /// </summary>
        public static string[] strAttachmentFileType =
        {
            "گزارش تخلف",
            "ابلاغ اتهام",
            "گزارش کارشناسی",
            "گزارش گروه تحقیق",
            "استعلام از مراجع",
            "پاسخ استعلام",
            "لایحه دفاعیه متهم",
            "نامه دعوت از متهم به مصاحبه",
            "گزارش کارشناسی پس از دفاعیه",
            "گزارش گروه تحقیق پس از دفاعیه",
            "مدارک حقوقی",
            "فرم اظهار نظر انفرادی",
            "صدور رای",
            "ابلاغ رای",
            "حکم اجرای رای",
            "دادخواست دیوان محکوم علیه",
            "لایحه دفاعیه دفتر هماهنگی",
            "دادنامه صادره از دیوان(دستور موقت)",
            "دادنامه صادره از دیوان (تایید )",
            "دادنامه صادره از دیوان (نقض رای)",
            "نامه ارجاع به هیات",
            "نامه ارجاع دادنامه به ادارات مربوطه",
            "نامه تجدیدنظر خواهی از کارگزینی اداره مربوطه",
            "درخواست ارسال پرونده از هیات بدوی",
            "گردش کار",
            "اعتراض به رای",
            "صدور صورتجلسه مختومه",
            "لایحه تجدید نظر خواهی",
            "تقاضای نقض رای",
            "ارجاع جهت رسیدگی",
            "دادنامه تجدید نظر دیوان",
            "رفع اثر دستور موقت",
            "صورتجلسه هیات صادر کننده رای",
            "موافقت با نقض رای",
            "مخالفت با نقض رای",
            "اعلام ماده 24",
            "حکم کارگزینی(اخراج ماده17)",
            "ابلاغیه حکم اخراج",
            "نامه اعتراض به حکم",
            "نامه درخواست پرونده استخدامی",
            "تقاضای برقراری مقرری (ماده 11)",
            "صورتجلسه برقراری مقرری",
            "پاسخ به محکوم علیه",
            "اعلام ماده 22 از هیات عالی نظارت",
            "بررسی اعضای هیات"
        };

        /// <summary>
        /// اینام انواع وضعیت های اتهام
        /// </summary>
        public enum AccusationStatus
        {
            /// <summary>
            /// نامشخص
            /// </summary>
            NOUN,
            /// <summary>
            /// محزز شده
            /// </summary>
            CONFIRMED,
            /// <summary>
            /// محرز نشده
            /// </summary>
            NOT_CONFIRMED
        };

        /// <summary>
        /// عناوین مربوط به اینام اینام انواع وضعیت های اتهام
        /// </summary>

        public static string[] strAccusationReportSource = { "شخص حقیقی", "شخص حقوقی" };

        public static string[] strAccusationEventPlace = { "خارج از خدمت", "محل خدمت" , "هر دو"};

        public static string[] strAccusationStatusLabels = { "نامشخص" , "محرز شده", "محرز نشده"};

        public static string[] NotificationLocationLabels = {  "نامعین" , "محل سکونت", "محل کار", "درج در روزنامه" , };

        public static string[] RequestStatuslabel = { "تکمیل نشده","تکمیل شده" };

        public static string[] RequestStatusIconSrc = { "/images/icon/open_envelop.png", "/images/icon/close_envelop.png" };

        public static string[] SendFileRequestStatuslabel = { "در انتظار ارسال پرونده", "پرونده ارسال شد" , "درخواست ارسال رد شده است" };

        public static string[] SendFileRequestStatusIconSrc = { "/images/icon/wait.png", "/images/icon/check.png", "/images/icon/back.png" };

        /// <summary>
        /// درخواستهای ارسال فایل
        /// </summary>
        public enum SendingFileRequestViewType
        {
            /// <summary>
            ///  درخواست رسیده
            /// </summary>
            INCOMMING = 1, 
            /// <summary>
            ///  درخواست ارسال شده
            /// </summary>
            OUTGOING = 2,
            /// <summary>
            ///  دفتر هماهنگی
            /// </summary>
            COORDINATOR = 3
        }

        /// <summary>
        /// متن مرتبط با انواع وضعیت فلگ  جهت قابل توجه نمودن پرونده
        /// </summary>
        public static string[] strFileHintTagTooltip =
        {
            "وضعیت پرونده را عادی میکند","ابلاغ رای ثبت شده ، ولی حکم اجرای رای ثبت نشده و باید ثبت شود","ارجاع از سوی دفتر هماهنگی ، جهت رسیدگی در هیات","","اعلام ماده 24"
        };

        /// <summary>
        /// متن متناظر با اندیس وضعیت استخدامی
        /// </summary>
        public static string[] strlblEstekhdamStatus =
        {
            "نامشخص",
            "شاغل",
            "انفصال موقت",
            "انفصال دائم"
        };

        /// <summary>
        /// اینام مربوط به انواع رای صادره
        /// </summary>
        public enum VoteTypeCode
        {
            NOUN = 0,
            CLOSE,
            INNOCENCE,
            FINIT_CONDEMNATION,
            RESEARCHABLE_CONDEMNATION
        }

        /// <summary>
        /// متن مرتبط با هر اینان وضعیت پرونده
        /// </summary>
        public static string[] strVoteTypeCodeLabel =
        {
            "نامشخص",
            "مختومه",
            "برائت",
            "محکومیت (قطعی)",
            "محکومیت (قابل پژوهش)"
        };

        /// <summary>
        /// متن نوع صدور رای
        /// </summary>
        public static string[] strSodourTypeLabel =
        {
            "نامشخص",
            "به اتفاق آراء",
            "با اکثریت آراء",
        };
    }
}