﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class SendingFileRequstDetails : System.Web.UI.Page
    {
        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long SendingRequestID
        {
            get
            {
                if (Request.QueryString.Get("ID") != null)
                {
                    return Request.QueryString.Get("ID").StringToLong();
                }
                return 0;
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        public long? FileID
        {
            get
            {
                if (ViewState["FileID"] != null)
                {
                    return ViewState["FileID"].ToString().StringToLong();
                }

                return null;
            }
            set
            {
                ViewState.Add("FileID", value);
            }
        }

        /// <summary>
        /// شناسه پرونده مورد درخواست
        /// </summary>
        private long? RelatedFileID
        {
            get
            {
                if (ViewState["RelatedFileID"] != null)
                {
                    return ViewState["RelatedFileID"].ToString().StringToLong();
                }

                return null;
            }
            set
            {
                ViewState.Add("RelatedFileID", value);
            }
        }

        /// <summary>
        /// شناسه پرونده مورد درخواست
        /// </summary>
        private long ToBoardID
        {
            get
            {
                if (ViewState["ToBoardID"] != null)
                {
                    return ViewState["ToBoardID"].ToString().StringToLong();
                }

                return 0;
            }
            set
            {
                ViewState.Add("ToBoardID", value);
            }
        }

        /// <summary>
        /// شناسه پرونده مورد درخواست
        /// </summary>
        private string RequestedFileSerialCode
        {
            get
            {
                if (ViewState["RequestedFileSerialCode"] != null)
                {
                    return ViewState["RequestedFileSerialCode"].ToString();
                }

                return null;
            }
            set
            {
                ViewState.Add("RequestedFileSerialCode", value);
            }
        }

        /// <summary>
        /// شناسه پرونده
        /// </summary>
        private long RequestDataID
        {
            get
            {
                if (ViewState["RequestDataID"] != null)
                {
                    return ViewState["RequestDataID"].ToString().StringToLong();
                }
                return 0;
            }
            set
            {
                ViewState.Add("RequestDataID", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        /// <summary>
        /// جزئیات درخواست ارسال فایل
        /// </summary>
        private void LoadData()
        {
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                ID = this.SendingRequestID
            };
            obj = (new DAL.Board.tblSendingFileRequest.Methods()).SelectByID(obj);
            if (obj.RowNumber > 0)
            {
                lblBoardName.Text = getBoardName(obj.ToBoardID);
                lblSerialCode.Text = obj.RequestedFileSerialCode;
                hfFileID.Value = obj.FileID.ToString();
                hfRelatedFileID.Value = obj.RelatedFileID.ToString();
                txtComment.InnerText = obj.Comment;
                this.FileID = obj.FileID;
                this.RelatedFileID = obj.RelatedFileID;
                this.ToBoardID = obj.ToBoardID;
                this.RequestDataID = obj.RequestDataID;
                this.RequestedFileSerialCode = obj.RequestedFileSerialCode;

                FileUpload1.RequestDataID = this.RequestDataID;
                if (FileUpload1.Load())
                {
                    FileUpload1.LockAllControls();
                    //pnlAttachment.Show();
                }
                if (obj.Status > 0)
                {
                    btnAccept.Hide();
                    btnReject.Hide();
                }
            }
        }

        /// <summary>
        /// استخراج نام هیات
        /// </summary>
        /// <param name="boardID"></param>
        /// <returns></returns>
        private string getBoardName(long boardID)
        {
            DTO.Board.tblBoard.tblBoard obj = new DTO.Board.tblBoard.tblBoard
            {
                BoardID = boardID
            };
            obj = (new DAL.Board.tblBoard.Methods()).SelectByID(obj);
            return obj.BoardTitle;
        }

        /// <summary>
        /// قبول درخواست ارسال پرونده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            if (InsertAccessMap())
            {
                if (InsertFileRelationMap())
                {
                    DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
                    {
                        ID = this.SendingRequestID,
                        Status = 1
                    };
                    bool bln = (new DAL.Board.tblSendingFileRequest.Methods()).UpdateStatus(obj);
                    if (bln)
                    {
                        SankaDialog1.ShowMessage("پرونده ارسال گردید .", DialogMessage.SankaDialog.Message_Type.Message, Page, true);
                        btnAccept.Hide();
                        btnReject.Hide();
                    }
                }
            }
        }

        /// <summary>
        /// درج رکورد جهت ارتباط پرونده جدید به پرونده درخواست شده
        /// </summary>
        /// <returns></returns>
        private bool InsertFileRelationMap()
        {
            DTO.Board.tblFileRelationMap.tblFileRelationMap obj = new DTO.Board.tblFileRelationMap.tblFileRelationMap
            {
                FileID = this.FileID.Value,
                RelatedFileID = this.RelatedFileID.Value,
                SerialCode = this.RequestedFileSerialCode
            };
            return (new DAL.Board.tblFileRelationMap.Methods()).Insert(obj);
        }

        /// <summary>
        /// درج رکورد جهت دادن مجوز دسترسی  هیات مقصد جهت دسترسی به پرونده 
        /// </summary>
        /// <returns></returns>
        private bool InsertAccessMap()
        {
            DTO.Board.tblFileAccessMap.tblFileAccessMap obj = new DTO.Board.tblFileAccessMap.tblFileAccessMap
            {
                FileID = this.RelatedFileID.Value,
                BoardID = this.ToBoardID
            };
            return (new DAL.Board.tblFileAccessMap.Methods()).Insert(obj);
        }

        /// <summary>
        /// رد درخواست ارسال پرونده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReject_Click(object sender, EventArgs e)
        {
            DTO.Board.tblSendingFileRequest.tblSendingFileRequest obj = new DTO.Board.tblSendingFileRequest.tblSendingFileRequest
            {
                ID = this.SendingRequestID,
                Status = 2
            };
            bool bln = (new DAL.Board.tblSendingFileRequest.Methods()).UpdateStatus(obj);
            if (bln)
            {
                SankaDialog1.ShowMessage("با درخواست ارسال پرونده مخالفت شد .", DialogMessage.SankaDialog.Message_Type.Message, Page, true);
                btnAccept.Hide();
                btnReject.Hide();
            }
        }
    }
}