﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SankaWebAppFramework.Board
{
    public partial class Help : GeneralPage
    {
        /// <summary>
        /// شناسه راهنما
        /// </summary>
        private int RequestID
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString.Get("ID")))
                {
                    return Request.QueryString.Get("ID").StringToByte();
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        /// <summary>
        /// بدست آوردن اطلاعات یک راهنما
        /// </summary>
        void LoadData()
        {
            DTO.Config.tblHelp.tblHelp Obj = new DTO.Config.tblHelp.tblHelp();

            Obj.RequestID = this.RequestID;
            ltWorkFlowReport.Text = "عدم وجود راهنما";
            if (Obj.RequestID == 0)
            {
                return;
            }

            try
            {
                Obj = (new DAL.Config.tblHelp.Methods()).SelectByType(Obj);

                if (Obj.RowNumber > 0)
                {
                    ltWorkFlowReport.Text = Obj.HelpContent;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }
    }
}