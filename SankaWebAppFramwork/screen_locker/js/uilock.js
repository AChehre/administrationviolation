﻿
(function ($) {
    $.extend({
        uiLock: function (content) {
            if (content == 'undefined') content = '';
            $('<div></div>').attr('id', 'uiLockId').css({
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'z-index': 1000,
                'opacity': 0.5,
                'width': $(document).width() + 'px',
                'height': $(document).height() + 'px',
                'color': 'white',
                'background-color': 'black'
            }).html(content).appendTo('body');
        },
        uiUnlock: function () {
            $('#uiLockId').remove();
        }
    });
})(jQuery);

function lockScreen() {

    createCountDownBox();

    //load_url(blog_url);
    $('#countdown_box').show(); //countdown
    $.uiLock('');
}

//funciton to initialise a click event for the webpage buttons
$(document).ready(function () {
    $('#lock').click(function () {

        //show content
        $('#countdown_box').show(); //countdown

        //lock interface
        $.uiLock('');
    });

    //Initial settings
    $('#countdown_box').hide();
});

function createCountDownBox()
{
    var iDiv = document.createElement('div');
    iDiv.id = 'countdown_box';
    iDiv.innerHTML = "<table style='text-align:center' cellpadding='0px' cellspacing='0px'><tr><td>لطفا منتظر بمانید ...</td></tr><tr><td><img src='/screen_locker/img/loading.gif' /></td></tr></table>";
    document.getElementsByTagName('body')[0].appendChild(iDiv);

    $("#countdown_box").css('text-align','center');
    $("#countdown_box").css('vertical-align', 'middle');
    $("#countdown_box").css('background-color', '#000000');
    $("#countdown_box").css('color', '#ffffff');
    $("#countdown_box").css('position', 'fixed');
    $("#countdown_box").css('height', '50px');
    $("#countdown_box").css('width', '105px');
    $("#countdown_box").css('z-index','1000000');
    $("#countdown_box").css('left', (($(window).width() - $('#countdown_box').outerWidth()) / 2) - 10);
    $("#countdown_box").css('top', ($(window).height() - $('#countdown_box').outerHeight()) / 2);

   
}