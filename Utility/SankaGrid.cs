﻿using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Text;

namespace SankaGridView
{
    public class SankaGrid : GridView
    {

        /// <summary>
        /// بایند کردن اطلاعات به گرید ، همراه با نمایش دکمه های صفحه بندی
        /// </summary>
        /// <typeparam name="T">جنریک تایپ</typeparam>
        /// <param name="DataSource">لیست داده هایی که باید نمایش داده شود</param>
        /// <param name="PageSize">تعداد رکوردهای هر صفحه</param>
        /// <param name="PageIndex">شماره صفحه</param>
        /// <param name="PageLinkNo">تعداد دکمه های قابل نمایش جهت صفحه بندی</param>
        public void CustomDataBind<T>(List<T> DataSource, int PageSize, int PageIndex, int PageLinkNo = 10)
        {
            //بایند کردن گرید به دیتاسورس 
            this.DataSource = DataSource;
            this.DataBind();

            int TotalRow = 0;

            #region گرفتن تعداد کل سطرهای موجود در لیست

            if (DataSource.Count > 0)
            {
                PropertyInfo[] props = DataSource[0].GetType().GetProperties();

                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name == "TotalRow")
                    {
                        int.TryParse(prop.GetValue(DataSource[0], null).ToString(), out TotalRow);
                        break;
                    }
                }
            }

            #endregion گرفتن تعداد کل سطرهای موجود در لیست

            // اضافه کردن آیتم های شماره صفحه و تعداد رکورد هر صفحه ، به صفحه ای که گرید در آن قرار دارد
            Literal ltLinks = new Literal();

            ltLinks.Text = CreatePageNumberLink(TotalRow, PageSize, PageIndex, PageLinkNo);

            this.Parent.Controls.Add(ltLinks);
        }

        /// <summary>
        /// ساخت لینک های مربوط به شماره صفحات یک عملیات صفحه بندی
        /// </summary>
        /// <param name="TotalRow">تعداد کل سطرها</param>
        /// <param name="PageSize">تعداد سطر هر صفحه</param>
        /// <param name="PageIndex">شماره صفحه جاری</param>
        /// <param name="PageLinkNo">تعداد دکمه هایی که میخواهیم به عنوان لینک صفحات نمایش دهیم، همیشه زوج باشد</param>
        /// <returns></returns>
        public string CreatePageNumberLink(long TotalRow, int PageSize, int PageIndex, int PageLinkNo)
        {
            StringBuilder PagingItem = new StringBuilder();

            long NumberofPage = (TotalRow / PageSize);

            if ((TotalRow % PageSize) > 0)
            {// اگر تقسیم تعداد کل سطرها به تعداد سطر هر صفحه، باقیمانده داشت، یکی به تعداد صفحه اضافه بشود
                NumberofPage++;
            }

            //if (NumberofPage > 1)
            //{
            PagingItem.Append("<ul class='pagination'>");

            #region اضافه کردن کمبو انتخاب تعداد سطرهای صفحه

            PagingItem.Append("<li><select id='ddlPageRowsNo' style='width: 55px !important;' onchange='setPageSize(this.value)' >");

            PagingItem.Append("<option value='10' " + (PageSize == 10 ? "selected='selected'" : "") + " >10</option>");
            PagingItem.Append("<option value='20' " + (PageSize == 20 ? "selected='selected'" : "") + " >20</option>");
            PagingItem.Append("<option value='50' " + (PageSize == 50 ? "selected='selected'" : "") + " >50</option>");
            PagingItem.Append("<option value='100' " + (PageSize == 100 ? "selected='selected'" : "") + " >100</option>");

            PagingItem.Append("</select></li>");

            #endregion اضافه کردن کمبو انتخاب تعداد سطرهای صفحه

            if (PageIndex > 1)
            {// اگر شماره صفحه بزرگتر از یک بود، لینک صفحه نخست و صفحه قبلی اضافه شود
                #region دکمه صفحه نخست

                PagingItem.Append("<li><a title='1' onclick='ChangePage(1)' >");
                PagingItem.Append("صفحه نخست");
                PagingItem.Append("</a></li>");

                #endregion دکمه صفحه نخست

                #region دکمه صفحه قبلی

                PagingItem.Append("<li><a title='" + (PageIndex - 1).ToString() + "' onclick='ChangePage(" + (PageIndex - 1) + ")' >");
                PagingItem.Append("قبلی");
                PagingItem.Append("</a></li>");

                #endregion دکمه صفحه قبلی
            }

            #region سایر دکمه ها

            string SelectedClass = "active";
            // بدست آوردن شماره صفحه ابتدایی برای نمایش به کاربر
            long From = (PageIndex - (PageLinkNo / 2)) > 0 ? (PageIndex - (PageLinkNo / 2)) : 1;
            // بدست آوردن شماره صفحه انتهایی برای نمایش به کاربر
            long To = NumberofPage;
            if ((From + PageLinkNo - 1) <= NumberofPage)
            {
                To = (From + PageLinkNo - 1);
            }
            else
            {
                To = NumberofPage;
                From = (NumberofPage - PageLinkNo + 1) > 0 ? (NumberofPage - PageLinkNo + 1) : 1;
            }

            if (From > 1)
            {// اگر اولین دکمه لینک صفحات بزرگتر از صفر باشد، ی سه نقطه اول بگذاریم
                PagingItem.Append("<li><span> ... </span></li>");
            }

            string Action = "";
            for (long i = From; i <= To; i++)
            {
                Action = "onclick='ChangePage(" + i.ToString() + ")'";
                SelectedClass = "";
                if (PageIndex == i)
                {
                    SelectedClass = "active";
                    Action = "";
                }
                PagingItem.Append("<li class='" + SelectedClass + "'><a title='" + i.ToString() + "' " + Action + " >");
                PagingItem.Append(i.ToString());
                PagingItem.Append("</a></li>");
            }

            if (To < NumberofPage)
            {// اگر آخرین دکمه لینک صفحات کوچکتر از تعداد صفحات باشد، ی سه نقطه آخرش بگذاریم
                PagingItem.Append("<li><span> ... </span></li>");
            }

            #endregion سایر دکمه ها

            if (PageIndex < NumberofPage)
            {// اگر شماره صفحه کوچکتر از تعداد کل صفحات بود، لینک صفحه پایانی و صفحه بعدی اضافه شود

                #region دکمه صفحه بعدی

                PagingItem.Append("<li><a title='" + (PageIndex + 1).ToString() + "' onclick='ChangePage(" + (PageIndex + 1) + ")' >");
                PagingItem.Append("بعدی");
                PagingItem.Append("</a></li>");

                #endregion دکمه صفحه بعدی

                #region دکمه صفحه پایانی

                PagingItem.Append("<li><a title='" + NumberofPage + "' onclick='ChangePage(" + NumberofPage + ")' >");
                PagingItem.Append("صفحه پایانی");
                PagingItem.Append("</a></li>");

                #endregion دکمه صفحه پایانی
            }

            PagingItem.Append("</ul>");
            //}

            return PagingItem.ToString();
        }
    }
}

