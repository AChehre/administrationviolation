﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

public static class ExtensionMethods
{
    public static int CorrectShamsiDate(this string dt)
    {
        string[] strDateParts = dt.Split('/');
        string strYear = strDateParts[0];
        string strMonth = (int.Parse(strDateParts[1])) > 9 ? strDateParts[1] : "0" + (int.Parse(strDateParts[1]));
        string strDay = (int.Parse(strDateParts[2])) > 9 ? strDateParts[2] : "0" + (int.Parse(strDateParts[2]));
        return int.Parse(strYear + strMonth + strDay);
    }

    /// <summary>
    /// برای تبدیل تاریخ شمسی به تاریخ میلادی 
    /// </summary>
    /// <param name="dt">زشته تاریخ شمسی</param>
    /// <returns>تاریخ میلادی</returns>
    public static DateTime ToLatinDate(this string dt)
    {
        PersianCalendar pc = new PersianCalendar();
        string[] dateParts1 = dt.Split('/');
        return pc.ToDateTime(dateParts1[0].StringToInt(), dateParts1[1].StringToInt(), dateParts1[2].StringToInt(), 0, 0, 0, 0);
    }

    /// <summary>
    /// برای تبدیل تاریخ میلادی به تاریخ شمشی 
    /// </summary>
    /// <param name="dt">تاریخ میلادی</param>
    /// <returns>رشته تاریخ شمسی</returns>
    public static string ToPersianDate(this DateTime dt)
    {
        PersianCalendar pc = new PersianCalendar();

        return pc.GetYear(dt) + "/" + pc.GetMonth(dt) + "/" + pc.GetDayOfMonth(dt);
    }

    /// <summary>
    /// برای اینکه کنترل کنیم آیا رشته ما از نوع GUID میباشد یا خیر
    /// </summary>
    /// <param name="input">رشته GUID</param>
    /// <returns>بولین</returns>
    public static bool IsGuid(this String input)
    {
        try
        {
            new Guid(input);
            return true;
        }
        catch { }
        return false;
    }

    /// <summary>
    /// برای کنترل کردن اینکه بدانیم آیا رشته  ما از نوع اینت میباشد یا خیر
    /// </summary>
    /// <param name="input">رشته عددی</param>
    /// <returns>بولین</returns>
    public static bool IsInt(this String input)
    {
        try
        {
            Int32.Parse(input);
            return true;
        }
        catch (ArgumentNullException) { }
        catch (FormatException) { }
        catch (OverflowException) { }
        return false;
    }

    /// <summary>
    /// از بین بردن سمیکالون
    /// </summary>
    /// <param name="input">زشته متنی</param>
    /// <returns>رشته</returns>
    public static string RemoveSemicolon(this string input)
    {
        input = input.Replace(",", "");
        return input.Trim();
    }

    /// <summary>
    /// از بین بردن نقطه برای اعداد
    /// </summary>
    /// <param name="input">رشته متنی</param>
    /// <returns>رشته</returns>
    public static string Removedot(this string input)
    {
        input = input.Replace(".", "");
        return input.Trim();
    }

    /// <summary>
    /// عدد به رشته تبدیل
    /// </summary>
    /// <param name="txt">زشته عددی</param>
    /// <returns>عدد</returns>
    public static int StringToInt(this string txt)
    {
        txt = txt.Replace(".", "").Replace(",", "").Trim();
        int num;
        int.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// تبدیل رشته به جی یوآیدی
    /// </summary>
    /// <param name="txt">زشته عددی</param>
    /// <returns>guid</returns>
    public static Guid StringToGuid(this string txt)
    {
        txt = txt.Replace(".", "").Replace(",", "").Trim();
        Guid num;
        Guid.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// تبدیل رشته به بایت
    /// </summary>
    /// <param name="txt">رشته بایت</param>
    /// <returns>بایت</returns>
    public static byte StringToByte(this string txt)
    {
        byte result;
        byte.TryParse(txt, out result);
        return result;
    }

    /// <summary>
    /// تبدیل رشته به عدد
    /// </summary>
    /// <param name="txt">زشته عددی</param>
    /// <returns>عدد</returns>
    public static long StringToLong(this string txt)
    {
        txt = txt.Replace(".", "").Replace(",", "").Trim();
        long num;
        long.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// تبدیل رشته به عدد اعشاری
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns>عدد اعشاری</returns>
    public static double StringToDouble(this string txt)
    {
        double num;
        double.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// تبدیل اعداد داخل متن به فارسی
    /// </summary>
    /// <param name="num">رشته عددی</param>
    /// <param name="seprate">کارکتر جداکننده</param>
    /// <returns></returns>
    public static string ConvertNumToFa(this string num, bool seprate)
    {
        if (seprate)
        {
            try
            {
                num = long.Parse(num).ToString("N0");
            }
            catch { }
        }

        string[] strPart = num.Split('/');
        if (strPart.Length == 3)
        {
            Array array = strPart;
            Array.Reverse(array);
            num = string.Join("/", strPart);
        }

        string result = "";
        foreach (char c in num.ToCharArray())
        {
            switch (c)
            {
                case '0':
                    result += "٠";
                    break;
                case '1':
                    result += "١";
                    break;
                case '2':
                    result += "٢";
                    break;
                case '3':
                    result += "٣";
                    break;
                case '4':
                    result += "٤";
                    break;
                case '5':
                    result += "٥";
                    break;
                case '6':
                    result += "٦";
                    break;
                case '7':
                    result += "٧";
                    break;
                case '8':
                    result += "٨";
                    break;
                case '9':
                    result += "٩";
                    break;
                default:
                    result += c;
                    break;

            }
        }
        return result;
    }

    /// <summary>
    /// سه رقم سه رقم جداد میکند
    /// </summary>
    /// <param name="num">عدد</param>
    /// <returns>رشته عددی</returns>
    public static string Devide3Part(this long num)
    {
        return string.Format("{0:N0}", num);
    }

    /// <summary>
    /// سه رقم سه رقم جداد میکند
    /// </summary>
    /// <param name="num">عدد</param>
    /// <returns>زشته عددی</returns>
    public static string Devide3Part(this int num)
    {
        return string.Format("{0:N0}", num);
    }

    /// <summary>
    /// تبدیل رشته به تاریخ
    /// </summary>
    /// <param name="txt">رشته تاریخ</param>
    /// <returns>تاریخ</returns>
    public static DateTime StringToDateTime(this string txt)
    {
        txt = txt.Replace(".", "").Replace(",", "").Trim();
        DateTime dt;
        DateTime.TryParse(txt, out dt);
        return dt;
    }

    /// <summary>
    /// رشته منطقی را به بولین تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته منطقی</param>
    /// <returns></returns>
    public static bool StringToBool(this string txt)
    {
        txt = txt.Replace(".", "").Replace(",", "").Trim();
        bool num;
        bool.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// استخراخ MD5
    /// </summary>
    /// <param name="text">رشته ورودی</param>
    /// <returns>رشته MD5</returns>
    public static string getMD5(this string text)
    {
        StringBuilder sBuilder = new StringBuilder();
        using (MD5 md5Hash = MD5.Create())
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(text));
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
        }
        return sBuilder.ToString();
    }

    /// <summary>
    /// مخفی کردن کنترل
    /// </summary>
    /// <param name="ctrl">کنترل</param>
    public static void Hide(this System.Web.UI.HtmlControls.HtmlControl ctrl)
    {
        ctrl.Visible = false;
    }

    /// <summary>
    /// نمایان کردن کنترل
    /// </summary>
    /// <param name="ctrl">کنترل</param>
    public static void Show(this System.Web.UI.HtmlControls.HtmlControl ctrl)
    {
        ctrl.Visible = true;
    }

    /// <summary>
    /// مخفی کردن کنترل
    /// </summary>
    /// <param name="ctrl">کنترل</param>
    public static void Hide(this System.Web.UI.Control ctrl)
    {
        ctrl.Visible = false;
    }

    /// <summary>
    /// نمایان کردن کنترل
    /// </summary>
    /// <param name="ctrl">کنترل</param>
    public static void Show(this System.Web.UI.Control ctrl)
    {
        ctrl.Visible = true;
    }

    /// <summary>
    /// به هم چسباندن آیتمهای داخل لیست با کاما 
    /// </summary>
    /// <param name="?"></param>
    /// <returns></returns>
    public static string JoinByCamma(this List<long> lst)
    {
        string strOutpot = "";
        foreach (object str in lst)
        {
            strOutpot += (str + ",");
        }
        return strOutpot.Remove(strOutpot.Length - 1);
    }

    /// <summary>
    /// تعیین معتبر بودن کد ملی
    /// </summary>
    /// <param name="nationalCode">کد ملی وارد شده</param>
    /// <returns>
    /// در صورتی که کد ملی صحیح باشد خروجی <c>true</c> و در صورتی که کد ملی اشتباه باشد خروجی <c>false</c> خواهد بود
    /// </returns>
    public static bool IsValidNationalCode(this String nationalCode)
    {
        //در صورتی که کد ملی وارد شده تهی باشد

        if (String.IsNullOrEmpty(nationalCode))
            //throw new Exception("لطفا کد ملی را صحیح وارد نمایید");
            return false;


        //در صورتی که کد ملی وارد شده طولش کمتر از 10 رقم باشد
        if (nationalCode.Length != 10)
            //throw new Exception("طول کد ملی باید ده کاراکتر باشد");
            return false;

        //در صورتی که کد ملی ده رقم عددی نباشد
        var regex = new Regex(@"\d{10}");
        if (!regex.IsMatch(nationalCode))
            //throw new Exception("کد ملی تشکیل شده از ده رقم عددی می‌باشد؛ لطفا کد ملی را صحیح وارد نمایید");
            return false;

        //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
        var allDigitEqual = new[] { "0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999" };
        if (allDigitEqual.Contains(nationalCode)) return false;


        //عملیات شرح داده شده در بالا
        var chArray = nationalCode.ToCharArray();
        var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
        var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
        var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
        var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
        var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
        var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
        var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
        var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
        var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
        var a = Convert.ToInt32(chArray[9].ToString());

        var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
        var c = b % 11;

        return (((c < 2) && (a == c)) || ((c >= 2) && ((11 - c) == a)));
    }

    /// <summary>
    /// گرفتن سال شمسی
    /// </summary>
    /// <param name="MyDate"></param>
    /// <returns></returns>
    public static int GetPersianYear(this DateTime MyDate)
    {
        PersianCalendar PDate = new PersianCalendar();
        if (MyDate == null)
        {
            return PDate.GetYear(DateTime.Now);
        }
        return PDate.GetYear(MyDate);
    }

    /// <summary>
    /// گرفتن ماه شمسی
    /// </summary>
    /// <param name="MyDate"></param>
    /// <returns></returns>
    public static int GetPersianMonth(this DateTime MyDate)
    {
        PersianCalendar PDate = new PersianCalendar();
        if (MyDate == null)
        {
            return PDate.GetMonth(DateTime.Now);
        }
        return PDate.GetMonth(MyDate);
    }

    /// <summary>
    /// گرفتن روز شمسی
    /// </summary>
    /// <param name="MyDate"></param>
    /// <returns></returns>
    public static int GetPersianDay(this DateTime MyDate)
    {
        PersianCalendar PDate = new PersianCalendar();
        if (MyDate == null)
        {
            return PDate.GetDayOfMonth(DateTime.Now);
        }
        return PDate.GetDayOfMonth(MyDate);
    }

    /// <summary>
    /// بررسی درست بودن آدرس ایمیل
    /// </summary>
    /// <param name="sEmail"></param>
    /// <returns></returns>
    public static bool IsEmailAdress(this string sEmail)
    {
        var sRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        return sRegex.IsMatch(sEmail);
    }

    /// <summary>
    /// بررسی درست بودن آدرس ایمیل
    /// </summary>
    /// <param name="sEmail"></param>
    /// <returns></returns>
    public static bool IsMobileNumber(this string sMobile)
    {
        Regex sRegx = new Regex(@"^09[1-3][0-9]{8}$");
        return sRegx.IsMatch(sMobile);
    }

    /// <summary>
    /// بررسی صحیح بودن تاریخ شمسی ورودی
    /// </summary>
    /// <param name="date">تاریخ شمسی به صورت رشته</param>
    /// <returns>اگر تاریخ شمسی صحیح بود ، ترو میدهد</returns>
    public static bool IsValidShamsi(this string date)
    {
        int year;
        int month;
        int day;

        //سال و ماه و روز

        if (date.Split('/').Length != 3)
            return false;

        //عدد

        if (!int.TryParse(date.Split('/')[0], out year))
            return false;

        if (!int.TryParse(date.Split('/')[1], out month))
            return false;

        if (!int.TryParse(date.Split('/')[2], out day))
            return false;

        //طول اعداد

        if (date.Split('/')[0].Length != 4)
            return false;

        if (date.Split('/')[1].Length != 2)
            return false;

        if (date.Split('/')[2].Length != 2)
            return false;

        if (day > 31 || day <= 0)
            return false;

        if (month > 12 || month <= 0)
            return false;

        try
        {
            PersianCalendar persianCalendar = new PersianCalendar();
            DateTime miladiDate = persianCalendar.ToDateTime(year, month, day, 0, 0, 0, 0);
        }
        catch
        {
            return false;
        }

        return true;
    }

    public static bool isValidPostalCode(this string strPostalCode)
    {
        if (strPostalCode != "")
        {
            var sRegex = new Regex(@"^(1|[3-9]){10}$");
            return sRegex.IsMatch(strPostalCode) ? true : false;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// تاریخ و ساعت شمسی را میگیرد و به میلادی تبدیل میکند
    /// </summary>
    /// <param name="date">تاریخ</param>
    /// <param name="time">زمان</param>
    /// <returns>تاریخ میلادی</returns>
    public static DateTime ToLatinDateTimeFull(this string date, string time)
    {
        var timeSplited = time.Split(':');
        int hour, minute, second;
        var result = date.ToLatinDate();

        if (int.TryParse(timeSplited[0], out hour) &
            int.TryParse(timeSplited[1], out minute) &
            int.TryParse(timeSplited[2], out second))
        {
            result = result.AddHours(hour);
            result = result.AddMinutes(minute);
            result = result.AddSeconds(second);
        }

        return result;
    }

    /// <summary>
    ///باکس تکست کردن پاک 
    ///</summary>
    /// <param name="txt">شی جعبه متن</param>
    public static void Clear(this System.Web.UI.WebControls.TextBox txt)
    {
        txt.Text = string.Empty;
    }

    /// <summary>
    ///لیبل کردن خالی 
    ///</summary>
    /// <param name="lbl">شی لیبل</param>
    public static void Clear(this System.Web.UI.WebControls.Label lbl)
    {
        lbl.Text = string.Empty;
    }

    /// <summary>
    ///فیلد هیدن محتویات کردن پاک 
    ///</summary>
    /// <param name="hf">شی هیدن فیلد</param>
    public static void Clear(this System.Web.UI.WebControls.HiddenField hf)
    {
        hf.Value = string.Empty;
    }

    /// <summary>
    /// تبدیل یک رشته به ClientIDMode 
    /// </summary>
    /// <param name="data">رشته ورودی</param>
    /// <returns>ClientIDMode</returns>
    public static System.Web.UI.ClientIDMode StringToClientIDMode(this string data)
    {
        if (data == "AutoID")
        {
            return System.Web.UI.ClientIDMode.AutoID;
        }
        else if (data == "Predictable")
        {
            return System.Web.UI.ClientIDMode.Predictable;
        }
        else if (data == "Static")
        {
            return System.Web.UI.ClientIDMode.Static;
        }

        return System.Web.UI.ClientIDMode.Inherit;
    }

    /// <summary>
    ///فارسی به عربی "ک" و "ی" تبدیل 
    ///<para>رشته انتهای و ابتدا از فاصله حذف و </para>
    ///</summary>
    /// <param name="txt">رشته ورودی</param>
    /// <returns>رشته خروجی پس از اصلاح</returns>
    public static string CorrectArabicChars(this string txt)
    {
        return txt.Replace("ی", "ي").Replace("ک", "ك").Trim();
    }

    /// <summary>
    /// رشته عددی را به اعشاری تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static float StringToFloat(this string txt)
    {
        txt = txt.Replace(",", "").Trim();
        float num;
        float.TryParse(txt, out num);
        return num;
    }

    /// <summary>
    /// رشته عددی را به لانگ تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static long ObjectToLong(this object txt)
    {
        try
        {
            return Convert.ToInt64(txt);
        }
        catch
        {
            return 0;
        }
    }

    /// <summary>
    /// رشته عددی را به لانگ تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static DateTime? ObjectToDate(this object txt)
    {
        try
        {
            return Convert.ToDateTime(txt);
        }
        catch
        {
            return null;
        }
    }

    /// <summary>
    /// رشته عددی را به لانگ تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static int ObjectToInt(this object txt)
    {
        try
        {
            return Convert.ToInt32(txt);
        }
        catch
        {
            return 0;
        }
    }

    /// <summary>
    /// رشته عددی را به لانگ تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static byte ObjectToByte(this object txt)
    {
        try
        {
            return Convert.ToByte(txt);
        }
        catch
        {
            return 0;
        }
    }

    /// <summary>
    /// رشته عددی را به لانگ تبدیل میکند
    /// </summary>
    /// <param name="txt">رشته عددی</param>
    /// <returns></returns>
    public static bool ObjectToBool(this object txt)
    {
        try
        {
            return Convert.ToBoolean(txt);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// نمایش تعدادی از کاراکترهای ابتدایی یک متن
    /// </summary>
    /// <param name="text">متن موردنظر</param>
    /// <param name="Length">تعداد کاراکترهای نتیجه</param>
    /// <returns> </returns>
    public static string ShowSummary(this string text, int Length)
    {
        if (text.Length <= Length)
        {
            return text;
        }

        return text.Substring(0, Length) + " ... ";
    }


    public static bool IsEmpty(this TextBox txt)
    {
        return txt.Text.Trim().Length == 0;
    }
}


