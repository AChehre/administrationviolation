﻿
using System;
using System.Text;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class Utility
{
    /// <summary>
    ///بودن رقمی تک صورت در عدد پشت صفر رعایت با عدد به میلادی تاریخ تبدیل 
    ///</summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string GetNuemericCustomDate(object obj)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(obj);
            StringBuilder sb = new StringBuilder();
            System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();

            string Year = pcal.GetYear(dt).ToString();
            sb.Append(Year);
            sb.Append("/");
            string Month = (pcal.GetMonth(dt) < 10) ? "0" + pcal.GetMonth(dt) : pcal.GetMonth(dt).ToString();
            sb.Append(Month);
            sb.Append("/");
            string Day = (pcal.GetDayOfMonth(dt) < 10) ? "0" + pcal.GetDayOfMonth(dt) : pcal.GetDayOfMonth(dt).ToString();
            sb.Append(Day);
            return sb.ToString() + "  ساعت " + GetTime(dt);
        }
        catch
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// تبدیل تاریخ میلادی به شمسی به همراه ساعت
    /// </summary>
    /// <param name="obj">شی حاوی ساعت لاتین</param>
    /// <returns>رشته حاوی ساعت فارسی</returns>
    public static string GetCustomDate(object obj)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(obj);
            StringBuilder sb = new StringBuilder();
            System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();
            DayOfWeek Day = pcal.GetDayOfWeek(dt);
            switch (Day)
            {
                case DayOfWeek.Saturday:
                    {
                        sb.Append(" شنبه ");
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        sb.Append(" یکشنبه ");
                        break;
                    }
                case DayOfWeek.Monday:
                    {
                        sb.Append(" دوشنبه ");
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        sb.Append(" سه شنبه ");
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        sb.Append(" چهارشنبه ");
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        sb.Append(" پنجشنبه ");
                        break;
                    }
                case DayOfWeek.Friday:
                    {
                        sb.Append(" جمعه ");
                        break;
                    }
            }
            int DayOfMonth = pcal.GetDayOfMonth(dt);
            sb.Append(" " + DayOfMonth + " ");

            switch (pcal.GetMonth(dt))
            {
                case 1:
                    {
                        sb.Append("فروردین");
                        break;
                    }
                case 2:
                    {
                        sb.Append("اردیبهشت");
                        break;
                    }
                case 3:
                    {
                        sb.Append("خرداد");
                        break;
                    }
                case 4:
                    {
                        sb.Append("تیر");
                        break;
                    }
                case 5:
                    {
                        sb.Append("مرداد");
                        break;
                    }
                case 6:
                    {
                        sb.Append("شهریور");
                        break;
                    }
                case 7:
                    {
                        sb.Append("مهر");
                        break;
                    }
                case 8:
                    {
                        sb.Append("آبان");
                        break;
                    }
                case 9:
                    {
                        sb.Append("آذر");
                        break;
                    }
                case 10:
                    {
                        sb.Append("دی");
                        break;
                    }
                case 11:
                    {
                        sb.Append("بهمن");
                        break;
                    }
                case 12:
                    {
                        sb.Append("اسفند");
                        break;
                    }
            }
            sb.Append(" " + pcal.GetYear(dt));
            return sb.ToString() + "  ساعت : " + GetTime(dt);
        }
        catch
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// تبدیل تاریخ میلادی به شمسی بدون ساعت
    /// </summary>
    /// <param name="obj">شی حاوی ساعت لاتین</param>
    /// <returns>رشته حاوی ساعت فارسی</returns>
    public static string GetCustomDateWithoutTime(object obj)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(obj);
            StringBuilder sb = new StringBuilder();
            System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();
            DayOfWeek Day = pcal.GetDayOfWeek(dt);
            switch (Day)
            {
                case DayOfWeek.Saturday:
                    {
                        sb.Append(" شنبه ");
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        sb.Append(" یکشنبه ");
                        break;
                    }
                case DayOfWeek.Monday:
                    {
                        sb.Append(" دوشنبه ");
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        sb.Append(" سه شنبه ");
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        sb.Append(" چهارشنبه ");
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        sb.Append(" پنجشنبه ");
                        break;
                    }
                case DayOfWeek.Friday:
                    {
                        sb.Append(" جمعه ");
                        break;
                    }
            }
            int DayOfMonth = pcal.GetDayOfMonth(dt);
            sb.Append(" " + DayOfMonth + " ");

            switch (pcal.GetMonth(dt))
            {
                case 1:
                    {
                        sb.Append("فروردین");
                        break;
                    }
                case 2:
                    {
                        sb.Append("اردیبهشت");
                        break;
                    }
                case 3:
                    {
                        sb.Append("خرداد");
                        break;
                    }
                case 4:
                    {
                        sb.Append("تیر");
                        break;
                    }
                case 5:
                    {
                        sb.Append("مرداد");
                        break;
                    }
                case 6:
                    {
                        sb.Append("شهریور");
                        break;
                    }
                case 7:
                    {
                        sb.Append("مهر");
                        break;
                    }
                case 8:
                    {
                        sb.Append("آبان");
                        break;
                    }
                case 9:
                    {
                        sb.Append("آذر");
                        break;
                    }
                case 10:
                    {
                        sb.Append("دی");
                        break;
                    }
                case 11:
                    {
                        sb.Append("بهمن");
                        break;
                    }
                case 12:
                    {
                        sb.Append("اسفند");
                        break;
                    }
            }
            sb.Append(" " + pcal.GetYear(dt));
            return sb.ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// بازگرداندن ساعت
    /// </summary>
    /// <param name="dt">شی ساعت لاتین</param>
    /// <returns>رشته حاوی ساعت</returns>
    public static string GetTime(DateTime dt)
    {
        string Z = "";
        DateTime M = dt;
        if (M.Hour < 10) Z += "0";
        Z += M.Hour;
        Z += ":";
        if (M.Minute < 10) Z += "0";
        Z += M.Minute;
        Z += ":";
        if (M.Second < 10) Z += "0";
        Z += M.Second;
        return Z;
    }

    /// <summary>
    /// تبدیل تاریخ شمسی به میلادی
    /// </summary>
    /// <param name="year">سال شمسی</param>
    /// <param name="month">ماه شمسی</param>
    /// <param name="day">زوز شمسی</param>
    /// <param name="hour">ساعت شمسی</param>
    /// <param name="minute">دقیق شمسی</param>
    /// <param name="second">ثانیه شمسی</param>
    /// <returns>تاریخ لاتین</returns>
    public static DateTime getLatinDate(int year, int month, int day, int hour, int minute, int second)
    {
        PersianCalendar pc = new PersianCalendar();
        return pc.ToDateTime(year, month, day, hour, minute, second, 0);
    }

    /// <summary>
    /// تولید کد فعال سازی 16 حرفی
    /// </summary>
    /// <param name="SerialNumber">سریال نرم افزار</param>
    /// <param name="SystemCode">شناسه سخت افزاری سیستم</param>
    /// <returns>رشته حاوی 16 حرف</returns>
    internal static string GenerateActivationCode(string SerialNumber, string SystemCode)
    {
        string part1 = SerialNumber.getMD5();
        string part2 = SystemCode.getMD5();
        string result = (part1 + part2).getMD5().Substring(0, 16);
        StringBuilder sb = new StringBuilder(result);
        for (int i = 4; i < 16; i += 5)
        {
            sb.Insert(i, "-");
        }
        return sb.ToString();
    }

    /// <summary>
    ///بودن رقمی تک صورت در عدد پشت صفر رعایت با عدد به میلادی تاریخ تبدیل 
    ///</summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string GetNuemericCustomDateWithoutTime(object obj)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(obj);
            StringBuilder sb = new StringBuilder();
            System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();

            string Year = pcal.GetYear(dt).ToString();
            sb.Append(Year);
            sb.Append("/");
            string Month = (pcal.GetMonth(dt) < 10) ? "0" + pcal.GetMonth(dt) : pcal.GetMonth(dt).ToString();
            sb.Append(Month);
            sb.Append("/");
            string Day = (pcal.GetDayOfMonth(dt) < 10) ? "0" + pcal.GetDayOfMonth(dt) : pcal.GetDayOfMonth(dt).ToString();
            sb.Append(Day);
            return sb.ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    #region Extera

    /// <summary>
    /// رفتن به آدرس ورودی
    /// </summary>
    /// <param name="url">آدرس صفحه مقصد</param>
    /// <param name="EndResponse">وضعیت فلاش شدن، اگر فالس باشد، یعنی کدهای بعد از فراخوانی این تابع اجرا شود، اگر ترو باشد، یعنی کدهای بعد از این تابع اجرا نشود</param>
    public static void CustomRedirect(string url, bool EndResponse = false)
    {
        HttpContext.Current.Response.Redirect(url, EndResponse);
    }

    /// <summary>
    /// ساخت یک عدد هشت رقمی تصادفی
    /// </summary>
    /// <returns></returns>
    public static string Create8DigitString()
    {
        Random RNG = new Random();
        var builder = new StringBuilder();
        while (builder.Length < 8)
        {
            builder.Append(RNG.Next(10).ToString());
        }
        return builder.ToString();
    }

    /// <summary>
    /// ساخت لینک های مربوط به شماره صفحات یک عملیات صفحه بندی
    /// </summary>
    /// <param name="TotalRow">تعداد کل سطرها</param>
    /// <param name="PageSize">تعداد سطر هر صفحه</param>
    /// <param name="PageIndex">شماره صفحه جاری</param>
    /// <param name="MainUrl">آدرس صفحه ای که باید هر لینک به آن صفحه هدایت شود -- به عبارتی صفحه مقصد</param>
    /// <param name="PageLinkNo">تعداد دکمه هایی که میخواهیم به عنوان لینک صفحات نمایش دهیم، همیشه زوج باشد</param>
    /// <returns></returns>
    public static string CreatePageNumberLink(int TotalRow, int PageSize, int PageIndex, string MainUrl, int PageLinkNo)
    {
        StringBuilder PagingItem = new StringBuilder();

        int NumberofPage = (TotalRow / PageSize);

        if ((TotalRow % PageSize) > 0)
        {// اگر تقسیم تعداد کل سطرها به تعداد سطر هر صفحه، باقیمانده داشت، یکی به تعداد صفحه اضافه بشود
            NumberofPage++;
        }

        if (NumberofPage > 1)
        {
            PagingItem.Append("<div id='PageNoContainer'>");

            string Url = "#";
            if (PageIndex > 1)
            {// اگر شماره صفحه بزرگتر از یک بود، لینک صفحه نخست و صفحه قبلی اضافه شود
                #region دکمه صفحه نخست

                Url = MainUrl + "Page=1";
                PagingItem.Append("<a title='1' class='PageNo' href='" + Url + "'>");
                PagingItem.Append("صفحه نخست");
                PagingItem.Append("</a>");

                #endregion دکمه صفحه نخست

                #region دکمه صفحه قبلی

                Url = MainUrl + "Page=" + (PageIndex - 1).ToString();
                PagingItem.Append("<a title='" + (PageIndex - 1).ToString() + "' class='PageNo' href='" + Url + "'>");
                PagingItem.Append("قبلی");
                PagingItem.Append("</a>");

                #endregion دکمه صفحه قبلی
            }

            Url = "#";
            string SelectedClass = "SelectedPageNo";
            // بدست آوردن شماره صفحه ابتدایی برای نمایش به کاربر
            int From = (PageIndex - (PageLinkNo / 2)) > 0 ? (PageIndex - (PageLinkNo / 2)) : 1;
            // بدست آوردن شماره صفحه انتهایی برای نمایش به کاربر
            int To = (From + PageLinkNo - 1) <= NumberofPage ? (From + PageLinkNo - 1) : NumberofPage;

            if (From > 1)
            {// اگر اولین دکمه لینک صفحات بزرگتر از صفر باشد، ی سه نقطه اول بگذاریم
                PagingItem.Append("<span> ... </span>");
            }

            for (int i = From; i <= To; i++)
            {
                Url = MainUrl + "Page=" + i.ToString();
                SelectedClass = "";
                if (PageIndex == i)
                {
                    SelectedClass = "SelectedPageNo";
                    Url = "#";
                }
                PagingItem.Append("<a title='" + i.ToString() + "' class='PageNo " + SelectedClass + "' href='" + Url + "'>");
                PagingItem.Append(i.ToString());
                PagingItem.Append("</a>");
            }

            if (To < NumberofPage)
            {// اگر آخرین دکمه لینک صفحات کوچکتر از تعداد صفحات باشد، ی سه نقطه آخرش بگذاریم
                PagingItem.Append("<span> ... </span>");
            }

            if (PageIndex < NumberofPage)
            {// اگر شماره صفحه کوچکتر از تعداد کل صفحات بود، لینک صفحه پایانی و صفحه بعدی اضافه شود

                #region دکمه صفحه بعدی

                Url = MainUrl + "Page=" + (PageIndex + 1).ToString();
                PagingItem.Append("<a title='" + (PageIndex + 1).ToString() + "' class='PageNo' href='" + Url + "'>");
                PagingItem.Append("بعدی");
                PagingItem.Append("</a>");

                #endregion دکمه صفحه بعدی

                #region دکمه صفحه پایانی

                Url = MainUrl + "Page=" + NumberofPage;
                PagingItem.Append("<a title='" + NumberofPage + "' class='PageNo' href='" + Url + "'>");
                PagingItem.Append("صفحه پایانی");
                PagingItem.Append("</a>");

                #endregion دکمه صفحه پایانی
            }

            PagingItem.Append("</div>");
        }

        return PagingItem.ToString();
    }

    /// <summary>
    /// ساخت قالب نمایشی پیام
    /// </summary>
    /// <param name="Message">پیام</param>
    /// <param name="MessageType">نحوه ی نمایش پیام</param>
    /// <returns>متن پیام به همراه قالب نمایشی</returns>
    protected static string CreateMessageFormat(string Message, Message_Type MessageType)
    {
        if (MessageType == Message_Type.Info)
        {
            Message = string.Format("<div class=\"info\"><span data-icon=\"K\"></span>{0}</div>", Message);
        }
        else if (MessageType == Message_Type.Message)
        {
            Message = string.Format("<div class=\"message close\"><span data-icon=\"E\"></span>{0}</div>", Message);
        }
        else if (MessageType == Message_Type.Warning)
        {
            Message = string.Format("<div class=\"warning\"><span data-icon=\"C\"></span>{0}</div>", Message);
        }
        else if (MessageType == Message_Type.Error)
        {
            Message = string.Format("<div class=\"error\"><span data-icon=\"u\">{0}</div>", Message);
        }

        return Message;
    }

    /// <summary>
    /// نمایش یک متن به کاربران
    /// </summary>
    /// <param name="Message">متنی که باید نمایش داده شود</param>
    /// <param name="MessageType">نوع متن، مثلا متن خطا،اخطار و غیره ، از اینام استفاده شود</param>
    /// <param name="_master">مستر پیجی که قرار است پیغام در آن نمایش داده شود</param>
    /// <param name="ShowInDialog">اگر ترو باشد، پیام در دیالوگ به کاربر نمایش داده میشود،در غیر اینصورت در یک دیو نمایش داده میشود</param>
    protected static void ShowMessage(string Message, Message_Type MessageType, MasterPage _master, bool ShowInDialog = false)
    {
        #region ساخت قالب پیام با توجه به نوع نمایش پیام

        Message = CreateMessageFormat(Message, MessageType);

        #endregion ساخت قالب پیام با توجه به نوع نمایش پیام

        if (ShowInDialog)
        {
            #region نمایش پیام در دیالوگ

            _master.Page.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "ShowPopup('" + Message + "','" + Message_Type_Label[(byte)MessageType] + "');", true);

            #endregion نمایش پیام در دیالوگ
        }
        else
        {
            #region نمایش پیام در دیو

            System.Web.UI.HtmlControls.HtmlContainerControl divMessage = (_master.FindControl("divMessage")
                        as System.Web.UI.HtmlControls.HtmlContainerControl);
            divMessage.InnerHtml = Message;
            divMessage.EnableViewState = false;
            divMessage.Show();

            #endregion نمایش پیام در دیو
        }
    }

    /// <summary>
    /// نمایش یک متن به کاربران
    /// </summary>
    /// <param name="Message">متنی که باید نمایش داده شود</param>
    /// <param name="MessageType">نوع متن، مثلا متن خطا،اخطار و غیره ، از اینام استفاده شود</param>
    /// <param name="_master">مستر پیجی که قرار است پیغام در آن نمایش داده شود</param>
    /// <param name="RedirectUrl">آدرس مقصد</param>
    /// <param name="EndResponse">اگر ترو باشد، کدهایی که بعد از فراخوانی این تابع باشند، اجرا نمیشوند</param>
    /// <param name="ShowInDialog">اگر ترو باشد، پیام در دیالوگ به کاربر نمایش داده میشود،در غیر اینصورت در یک دیو نمایش داده میشود</param>
    protected static void ShowMessage(string Message, Message_Type MessageType, MasterPage _master, string RedirectUrl, bool EndResponse = false, bool ShowInDialog = false)
    {
        #region ذخیره پیام در سشن و هدایت کاربر به صفحه مورد نظر

        //ذخیره پیام در سشن
        _master.Page.Session.Add(MessageType.ToString(), Message);

        // ذخیره حالت نمایش در سشن، اینکه در دیالوگ نمایش داده شود یا دیو
        _master.Page.Session.Add(MessageType.ToString() + "_ShowInDialog", ShowInDialog);

        CustomRedirect(RedirectUrl, EndResponse);

        #endregion ذخیره پیام در سشن و هدایت کاربر به صفحه مورد نظر
    }

    /// <summary>
    /// نمایش پیام هایی که بعد از ریدایرکت باید نمایش داده شوند
    /// <param name="_master">مستر پیجی که قرار است پیام در آن نمایش داده شود</param>
    /// </summary>
    protected static void ShowMessageAfterRedirect(MasterPage _master)
    {
        // متن خطایی که داخل سشن بار شده را نمایش میدهد
        if (_master.Page.Session[Message_Type.Error.ToString()] != null)
        {
            ShowMessage(_master.Page.Session[Message_Type.Error.ToString()].ToString(), Utility.Message_Type.Error, _master, Convert.ToBoolean(_master.Page.Session[Message_Type.Error.ToString() + "_ShowInDialog"]));
            _master.Page.Session.Remove(Message_Type.Error.ToString());
        }
        // متن پیامی که داخل سشن بار شده را نمایش میدهد
        else if (_master.Page.Session[Message_Type.Message.ToString()] != null)
        {
            ShowMessage(_master.Page.Session[Message_Type.Message.ToString()].ToString(), Utility.Message_Type.Message, _master, Convert.ToBoolean(_master.Page.Session[Message_Type.Message.ToString() + "_ShowInDialog"]));
            _master.Page.Session.Remove(Message_Type.Message.ToString());
        }
        // متن اخطاری که داخل سشن بار شده را نمایش میدهد
        else if (_master.Page.Session[Message_Type.Warning.ToString()] != null)
        {
            ShowMessage(_master.Page.Session[Message_Type.Warning.ToString()].ToString(), Utility.Message_Type.Warning, _master, Convert.ToBoolean(_master.Page.Session[Message_Type.Warning.ToString() + "_ShowInDialog"]));
            _master.Page.Session.Remove(Message_Type.Warning.ToString());
        }
        else if (_master.Page.Session[Message_Type.Info.ToString()] != null)
        {
            ShowMessage(_master.Page.Session[Message_Type.Info.ToString()].ToString(), Utility.Message_Type.Info, _master, Convert.ToBoolean(_master.Page.Session[Message_Type.Info.ToString() + "_ShowInDialog"]));
            _master.Page.Session.Remove(Message_Type.Info.ToString());
        }
    }


    /// <summary>
    /// جهت مشخص کردن نوع پیام هایی که به کاربران نمایش میدهیم
    /// </summary>
    protected enum Message_Type
    {
        Info = 0,// اطلاع رسانی
        Message = 1,// پیام
        Warning = 2,// اخطار
        Error = 3// خطا
    }

    /// <summary>
    /// عنوان انواع پیام های نمایشی به کاربر
    /// </summary>
    protected static string[] Message_Type_Label =
    {
        "اطلاع رسانی",
        "پیام",
        "اخطار",
        "خطا"
    };

    /// <summary>
    /// بدست آوردن آی پی کاربر
    /// </summary>
    public static string getUserIP()
    {
        string ip = "";

        try
        {
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
        }
        catch
        {

        }
        return ip;
    }

    /// <summary>
    /// اضافه کردن یک فایل به هدر یک صفحه
    /// </summary>
    /// <param name="path">مسیر فایل</param>
    /// <param name="page">صفحه مورد نظر</param>
    /// <param name="FileType">نوع فایل</param>
    public static void AddFileToPageHeader(string path, Page page,AddedFileType FileType)
    {
        Literal TargetFile = new Literal();
        if (FileType == AddedFileType.Css)
        {
            TargetFile.Text = @"<link href=""" + page.ResolveUrl(path) + @""" type=""text/css"" rel=""stylesheet"" />";
            
        }
        else if (FileType == AddedFileType.Js)
        {
            TargetFile.Text = @"<script type='text/javascript' src=" + page.ResolveUrl(path) + "></script>";
        }

        page.Header.Controls.Add(TargetFile);
    }

    /// <summary>
    /// نوع فایلی که باید به هدر صفحه اضافه شود
    /// </summary>
    public enum AddedFileType
    {
        /// <summary>
        /// فایل سی اس اس
        /// </summary>
        Css = 0,
        /// <summary>
        /// فایل جاوااسکریپت
        /// </summary>
        Js = 1,
    }

    #endregion

    public static string getBroaserAgent()
    {
        HttpBrowserCapabilities cap = HttpContext.Current.Request.Browser;
        return cap.Platform + "/" + cap.Browser + " Ver " + cap.Version;
    }
}
